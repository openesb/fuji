require 'java'
require 'XPath'
require 'ESB'
include_class "javax.xml.namespace.QName"

@@service = QName.new "http://fuji.dev.java.net/application/[app]", "[service]"

def process(inMsg)
  # payload is a java org.w3c.dom.Node object
  payload = inMsg.getPayload

  # your code goes here

  # return message  
  inMsg
end