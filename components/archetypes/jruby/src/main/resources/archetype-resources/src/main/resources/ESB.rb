require 'java'
require 'XPath'
include_class "com.sun.jbi.fuji.components.jruby.ServiceConsumerFactory"

  class ESB    
    def ESB.invoke service, operation, payload
      consumer = ServiceConsumerFactory.newServiceConsumer(service)
      msg = consumer.getServiceMessageFactory.createMessage
      msg.setPayload(payload)
      consumer.invoke(msg, operation)
    end
    def ESB.invokeOneWay service, operation, payload
      consumer = ServiceConsumerFactory.newServiceConsumer(service)
      msg = consumer.getServiceMessageFactory.createMessage
      msg.setPayload(payload)
      consumer.invokeOneWay(msg, operation)
    end
  end
