/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JRubyComponent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.components.jruby;


import com.sun.jbi.framework.descriptor.Provides;
import com.sun.jbi.framework.result.ComponentTaskResult;
import com.sun.jbi.framework.result.TaskResultDetails.TaskId;

import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.fuji.component.base.BaseComponent;
import com.sun.jbi.fuji.component.base.MessageListener;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.management.DeploymentException;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jruby.Ruby;
import org.jruby.RubyException;
import org.jruby.RubyRuntimeAdapter;
import org.jruby.javasupport.JavaEmbedUtils;
import org.w3c.dom.Document;


/**
 * Initial version of a JRuby container.  Chances are that we will throw this
 * version out and use the Ruby container in GF v3 in the near future.
 *
 * @author Sun Microsystems, Inc.
 */
public class JRubyComponent extends BaseComponent implements MessageListener {
    
    
    // Pathetic ruby consts - these really should be dynamic, or at the very
    // least configurable
    private static final String RUBY_SERVICE_FILE = "service.rb";
    private static final String RUBY_METHOD = "process";
    
    private DocumentBuilder documentBuilder_;
    
    private Map<String, RubyService> rubyServices_ = 
            new HashMap<String, RubyService>();
    

    @Override
    public void init(ComponentContext context) throws JBIException {
        super.init(context);
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(false);
            documentBuilder_ = dbf.newDocumentBuilder();
        }
        catch (Exception ex) {
            throw new JBIException("Failed to initialze document builder", ex);
        }
        
        // seed the ComponentContext in ServiceConsumerFactory
        new ServiceConsumerFactory(context);
    }
    
    @Override
    public void init(String suName, String suPath) throws DeploymentException {
        super.init(suName, suPath);
        
        List<Provides> providedServices = 
                getServiceUnitDescriptor(suName).getServices().getProvides();
        
        for (Provides p : providedServices) {
            RubyService rs = new RubyService();
            String serviceName = p.getServiceName().getLocalPart();
            List<String> loadPaths = new ArrayList<String>();
            loadPaths.add(suPath + "/" + serviceName);
            rs.runtime = JavaEmbedUtils.initialize(loadPaths);
            try {
                rs.script = initRubyScript(rs.runtime, suPath + "/" + serviceName);
            } 
            catch (Exception ex) {
                ComponentTaskResult.inspectResult(ComponentTaskResult
                        .createExceptionMessage(
                                getComponentName(), TaskId.init, 
                                ex, "ESBCOMP-6007", 
                                "ESBCOMP-6007: Failed to initialize ruby for service " + serviceName, 
                                serviceName));
            }
            
            rubyServices_.put(serviceName, rs);
        }
    }

    @Override
    public void shutDown(String suName) throws DeploymentException {
        try {
            super.shutDown(suName);
            
            List<Provides> providedServices = 
                    getServiceUnitDescriptor(suName).getServices().getProvides();
            
            for (Provides p : providedServices) {
                RubyService rs = rubyServices_.get(p.getServiceName().getLocalPart());
                if (rs != null) {
                    rs.destroy();
                }
            }
        }
        catch (Exception ex) {
            ComponentTaskResult.inspectResult(ComponentTaskResult
                    .createExceptionMessage(
                            getComponentName(), TaskId.shutDown, 
                            ex, "ESBCOMP-6008", 
                            "ESBCOMP-6008: Failed to shutdown ruby services"+ ex.getMessage(), 
                            ex.getMessage()));
        }
    }

    @Override
    public void start() throws JBIException {
        super.start();
        setMessageListener(this);
    }
    
    @Override
    public void shutDown() throws JBIException {
        super.shutDown();
    }

    public ServiceMessage onMessage(
            ServiceEndpoint endpoint, ServiceMessage inMsg) throws Exception {
        Object result = null;
        
        try {
            // locate the ruby service
            RubyService rs = rubyServices_.get(
                    endpoint.getServiceName().getLocalPart());
            
            // no endpoint found - whine about it in the log
            if (rs == null) {
                log_.warning("Unable to locate JRuby service for endpoint: " + endpoint);
            }
            
            //Object payload = inMsg.getPayload();
            // XPath processor in ruby does not like default namespaces
            //if (payload instanceof Document) {
            //    inMsg.setPayload(stripNamespaces((Document)payload));
            //}
            result = JavaEmbedUtils.invokeMethod(
                    rs.runtime, rs.script, RUBY_METHOD, 
                    new Object[] {inMsg}, Object.class);
            
            if (result instanceof ServiceMessage) {
                return (ServiceMessage)result;
            }
            else {
                return null;
            }
        }
        catch (Exception ex) {
            log_.log(Level.WARNING, "Invocation of ruby service failed", ex);
            
            if (result instanceof RubyException) {
                ex = new Exception(((RubyException)result).message.toString());
            }
            
            throw ex;
        }
    }
    
    

    Object initRubyScript(Ruby rubyRuntime, String path) throws Exception {
        Object script = null;
        
        // Create runtime instance
        RubyRuntimeAdapter jruby = JavaEmbedUtils.newRuntimeAdapter();
        
        // Get the ruby file - we just use the first one we find now
        for (File f : new File(path).listFiles()) {
            if (f.getName().equals(RUBY_SERVICE_FILE)) {
                String str = fileAsString(f.getAbsolutePath());
                script = jruby.eval(rubyRuntime, str);
                break;
            }
        }
        
        return script;
    }
    
    String fileAsString(String filePath) throws IOException {
        String lineSep = System.getProperty("line.separator");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        StringBuffer strBuf = new StringBuffer();

        String currentLine = "";
        while ((currentLine = bufferedReader.readLine()) != null) {
            strBuf.append(currentLine);
            strBuf.append(lineSep);

        }
        return strBuf.toString();
    }
    
    // This is crazy - it's can't be this hard to shake a default namespace off
    // of a document.
    Document stripNamespaces(Document doc) throws Exception {
        Transformer t = TransformerFactory.newInstance().newTransformer();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        doc.getDocumentElement().removeAttribute("xmlns");
        t.transform(new DOMSource(doc), new StreamResult(bos));
        return documentBuilder_.parse(new ByteArrayInputStream(bos.toByteArray()));
    }
    
    class RubyService {
        Ruby    runtime;
        Object  script;
        
        void destroy() {
            if (runtime != null) {
                JavaEmbedUtils.terminate(runtime);
            }
            runtime = null;
            script = null;
        }
    }
    
}
