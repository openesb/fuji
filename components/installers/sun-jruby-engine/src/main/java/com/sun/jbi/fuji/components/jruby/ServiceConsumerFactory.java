/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceConsumerFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.components.jruby;

import org.glassfish.openesb.api.service.ServiceConsumer;
import org.glassfish.openesb.api.service.ServiceConsumerImpl;
import javax.jbi.component.ComponentContext;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;

/**
 * JRuby scripts can use this factory to manufacture ServiceConsmer instances
 * for services they would like to invoke.
 * @author kcbabo
 */
public class ServiceConsumerFactory {
    
    private static ComponentContext context_;
    
    ServiceConsumerFactory(ComponentContext context) {
        context_ = context;
    }
    
    /**
     * Create a new ServiceConsumer instance for the specified service.
     * @param service service that you want to consume 
     * @return new ServiceConsmer instance for specified service or null if 
     * the service was not found
     * @throws Exception failed to create ServiceConsumer instance
     */
    public static ServiceConsumer newServiceConsumer(QName service) 
            throws Exception {
        
        if (context_ == null) {
            throw new IllegalStateException("ComponentContext has not been set");
        }
        
        ServiceConsumer consumer = null;
        ServiceEndpoint[] ep = context_.getEndpointsForService(service);
        if (ep != null && ep.length > 0) {
            consumer = new ServiceConsumerImpl(context_.getDeliveryChannel(), ep[0]);
        }
        else {
            throw new Exception("Unable to locate service: " + service.toString());
        }
        
        return consumer;
    }
}
