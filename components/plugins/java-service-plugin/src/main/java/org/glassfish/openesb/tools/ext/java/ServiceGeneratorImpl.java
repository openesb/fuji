/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.tools.ext.java;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.descriptor.Template;
import org.glassfish.openesb.tools.common.service.DefaultServiceGenerator;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * @author gpatil (original)
 * @author chikkala (port)
 */
public class ServiceGeneratorImpl extends DefaultServiceGenerator {
    private static final Logger LOG = Logger.getLogger(ServiceGeneratorImpl.class.getName());
    private static final String POJO_DIR = "pojo"; //NOI18N
    
    public ServiceGeneratorImpl(ServiceDescriptor sd) {
        super(sd);
    }

    private String getJavaClassFileNameForService(String serviceName) {
        //TODO: this is what original code has. fix it to make a valid class name.
        return "Svc" + serviceName + ".java";
    }

    @Override
    protected List<File> generateServiceSources(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) {
        LOG.fine("Executing overridden method generateServiceSources in class Java ServiceGeneratorImpl");

        List<File> usedFiles = new ArrayList<File>();
        File srcDir = prjModel.getSourceDir();
        // webui generates the Service.java file under serviceName package. So, check if that
        // file exists and do nothing as webui already generated the source code.
        //TODO: change the webui to generate the source the same way as IDE or command line.
        File webUIGeneratedPath = new File(srcDir, serviceName + "/Service.java");
        if (webUIGeneratedPath.exists()) {
            LOG.fine("WebUI generated file exists. not generating the java source for " + serviceName);
            usedFiles.add(webUIGeneratedPath);
            return usedFiles;
        }
        // if we reach here. we are generating the source from IDE or commmand line.
        //
        // there is only one template Service.java. find it.
        String javaFileName = getJavaClassFileNameForService(serviceName);
        File serviceCodeFile = new File(srcDir, POJO_DIR + "/" + javaFileName);
        if (serviceCodeFile.exists()) {
            LOG.fine("Java Service source exists. Not generating the java source for " + serviceName);
            usedFiles.add(serviceCodeFile);
            return usedFiles;
        }
        // no source file. generate one from template.
        List<Template> templates = this.getServiceDescriptor().getTemplates();
        Template svcJavaTemplate = null;
        for ( Template t : templates) {
            if ( t.getAlias().endsWith("Service.java")) {
                svcJavaTemplate = t;
                break;
            }
        }
        if (svcJavaTemplate != null ) {
            if (!serviceCodeFile.getParentFile().exists()) {
                serviceCodeFile.getParentFile().mkdirs();
            }
            Properties templateData = new Properties();
            templateData.putAll(serviceConfig);
            templateData.setProperty(ServiceProjectModel.PROP_SERVICE_NAME, serviceName);
            try {
                createFileFromTemplate(svcJavaTemplate, serviceCodeFile, templateData);
                usedFiles.add(serviceCodeFile);
            } catch (IOException ex) {
                LOG.log(Level.FINE, "Unable to create file from template " + serviceCodeFile, ex);
            }           
        } else {
            LOG.fine("No Service java source template found in descriptor bundle");
        }
        return usedFiles;
    }

}
