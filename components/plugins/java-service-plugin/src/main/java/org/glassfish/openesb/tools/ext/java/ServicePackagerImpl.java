/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.tools.ext.java;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.service.DefaultServicePackager;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * @author gpatil (original)
 * @author chikkala (port)
 */
public class ServicePackagerImpl extends DefaultServicePackager {
    private static final Logger LOG = Logger.getLogger(ServicePackagerImpl.class.getName());
    private static final String SERVICE_TYPE = "java";
    private static final String POJO_DIR = "pojo"; //NOI18N
    public ServicePackagerImpl(ServiceDescriptor sd) {
        super(sd);
    }

    @Override
    public void beforeApplicationPackaging(ServiceProjectModel prjModel, List<String> services, ApplicationProjectModel appPrjModel) throws IOException {
        // Log a message to inform us that the overriden method is being executed.
        LOG.fine(" #### @@@@ Executing overridden method beforeApplicationPackaging in  Java ServicePackagerImpl");
        File appOutputDir = appPrjModel.getOutputDir();
        beforeApplicationPackagingWithJavaServices(appOutputDir, services);
    }

    @Override
    public void beforeServicePackaging(ServiceProjectModel prjModel, List<String> services) throws IOException {
        // Log a message to inform us that the overriden method is being executed.
        LOG.fine(" $$$$$$ Executing overridden method beforeServicePackaging in  Java ServicePackagerImpl");
        File outputDir = prjModel.getOutputDir();
        LOG.fine("Java output dir " + outputDir.getAbsolutePath());
        beforeJavaServicePackaging(outputDir, services);
    }

    private void beforeJavaServicePackaging(
            final File outputDir,  // .../target/classes/java
            final List<String> serviceNames) throws IOException {

        LOG.fine("beforeJavaServicePackaing... outputDir " + outputDir.getAbsolutePath());
        
        if ((serviceNames != null) && (serviceNames.size() > 0)){
            File classDir = outputDir.getParentFile(); //NOI18N
            File svcpkg = null;
            File destSvcpkg = null;

            cleanUpJavaResourcesOutput(outputDir, serviceNames);

            FileFilter noJava = new FileFilter() {
                public boolean accept(final File file) {
                    if ( file.getName().endsWith(".java")){ //NOI18N
                        return false;
                    }
                    return true;
                }
            };

            svcpkg = new File(classDir, POJO_DIR); //NOI18N
            destSvcpkg  = new File(classDir,  SERVICE_TYPE + "/" + POJO_DIR);
            destSvcpkg.mkdirs();
            LOG.fine("Copying files from " + svcpkg.getAbsolutePath() + " To " + destSvcpkg.getAbsolutePath());
            // Copy class files/"resources" to SU directory.
            copyDir(svcpkg, destSvcpkg, noJava);
            
        }
    }

    private void beforeApplicationPackagingWithJavaServices(
            File appOutputDir, // .../target/classes
            List<String> services) throws IOException{
        if (services.size() > 0){
            // Delete the POJO service package so that it won't get packaged into SA root.
            File pojoClassesDir = new File (appOutputDir, POJO_DIR);
            deleteDirectory(pojoClassesDir);
            pojoClassesDir  = new File(appOutputDir,  SERVICE_TYPE + "/" + POJO_DIR);
            deleteDirectory(pojoClassesDir);
        }
    }

    
    private void cleanUpJavaResourcesOutput(
                final File outputDir, // .../target/classes/java
                final List<String> serviceNames)  throws IOException {

        LOG.fine("Cleanup java resources Output " + outputDir.getAbsolutePath());
        // File suOutputDir = new File(outputDir, SERVICE_TYPE); // .../target/classes/java
        File suOutputDir = outputDir;

        if ((suOutputDir != null) && (suOutputDir.exists())){
            LOG.fine("Cleaning up the java resource output....");
            File[] files = suOutputDir.listFiles();
            for (File fl : files){
                if (serviceNames.contains(fl.getName())){
                    continue;
                } else if ("META-INF".equalsIgnoreCase(fl.getName())){ //NOI18N
                    continue;
                } else if (POJO_DIR.equals(fl.getName())){
                    FileFilter yesJava = new FileFilter() {
                        public boolean accept(final File file) {
                            if ( file.getName().endsWith(".java")){ //NOI18N
                                return true;
                            }
                            return false;
                        }
                    };
                    File pojoResDir  = new File(suOutputDir,  POJO_DIR);
                    File[] javas = pojoResDir.listFiles(yesJava);
                    for (File java: javas){
                        java.delete();
                    }
                } else {
                    if (fl.isDirectory()){
                        deleteDirectory(fl);
                    } else {
                        fl.delete();
                    }
                }
            }
        }

    }

    /**
     * Deletes the directory and its children.
     *
     * @param dir directory to be deleted.
     */
    private void deleteDirectory(
            final File directory) throws IOException {

        if (!directory.exists()) {
            LOG.fine("Trying to delete direcotry that does not exists : " + directory.getPath());
            return;
        }
        LOG.fine("Deleting directory " + directory.getAbsolutePath());

        File[] children = directory.listFiles();

        if ((children != null) && (children.length != 0)) {
            for (File child : children) {
                if (child.isDirectory()) {
                    deleteDirectory(child);
                } else {
                    if (!child.delete()) {
                        throw new IOException(
                                "Failed to delete file: " + child.getAbsolutePath());
                    }
                }
            }
        }

        if (!directory.delete()) {
            throw new IOException(
                    "Failed to delete directory: " + directory.getAbsolutePath());
        }
    }
    /**
     * //TODO: add this to common utilities. so that every one can use it. or
     * // expose the MavenUtil pacakages (com.sun.jbi.fuji.maven.util)
     * 
     * copied from MavenUtil.
     *
     * @param source
     * @param target
     * @param filter
     * @throws java.io.IOException
     */
    private void copyDir(
            final File source,
            final File target,
            final FileFilter filter) throws IOException {

        if (source.isDirectory()) {
            if (target.exists() && target.isFile()) {
                throw new IOException(
                        "An attempt was made to copy a directory into a file.");
            }

            if (!target.exists() && !target.mkdirs()) {
                throw new IOException(
                        "Failed to create directory structure " +
                        "for " + target + ".");
            }

            final File[] children = source.listFiles();

            if ((children != null) && (children.length > 0)) {
                for (File child : children) {
                    final File childTarget = new File(target, child.getName());

                    if (child.isFile()) {

                        if (filter.accept(child)) {
                            copyFile(child, childTarget);
                        }
                    } else {
                        copyDir(child, childTarget, filter);
                    }
                }
            }
        } else {
            // source is a file
            if (filter.accept(source)) {
                if (!target.exists() || target.isFile()) {
                    copyFile(source, target);
                } else {
                    copyFile(source, new File(target, source.getName()));
                }
            }
        }
    }

    private void copyFile(File src, File dest) throws IOException {
        Utils.copy(src, dest);
    }


}
