{
    id: "java-template",

    display_name: "POJO Service",
    description: "Basic template for POJO services.",
    icon: null,
    tags: ["template", "service", "java", "pojo"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10500,

    section: "templates",

    item_data: {
        node_type: "java",
        node_data: {
            properties: {
                code_name: "java",
                is_shared: false,

                java_code: null,

                noop: null
            },

            ui_properties: {
                display_name: "POJO ",
                description: "Executes arbitrary Java code.",
                icon: null,

                noop: null
            }
        }
    }
}