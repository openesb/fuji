package pojo;

import org.glassfish.openesb.pojose.api.ErrorMessage;
import org.glassfish.openesb.pojose.api.annotation.Provider;
import org.glassfish.openesb.pojose.api.annotation.Resource;
import org.glassfish.openesb.pojose.api.annotation.Operation;
import org.glassfish.openesb.pojose.api.res.Context;

@Provider(name="${serviceName}_endpoint", serviceQN="{http://fuji.dev.java.net/application/${applicationId}}${serviceName}",
        interfaceQN="{http://fuji.dev.java.net/application/${applicationId}}${applicationId}_interface")
public class Svc${serviceName} {
    // POJO Context
    @Resource
    private Context jbiCtx;

    public Svc${serviceName}() {
    }

    @Operation (outMessageTypeQN="{http://fuji.dev.java.net/application/${applicationId}}anyMsg")
    public String receive(String input) throws ErrorMessage {
        // Enter your code goes here.
        return input;
    }
}

