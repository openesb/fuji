{
    id: "rest-template",

    display_name: "REST Adapter",
    description: "Basic template for REST adapters.",
    icon: null,
    tags: ["template", "adapter", "rest"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10700,

    section: "templates",

    item_data: {
        node_type: "rest",
        node_data: {
            properties: {
                code_name: "rest-",
                is_shared: false,

                rest_http_resourceURL: null,
                rest_http_method: null,
                rest_http_acceptType: null,
                rest_http_contentType: null,
                rest_http_headers: null,
                rest_http_paramStyle: null,
                rest_http_params: null,
                rest_http_basicAuthUsername: null,
                rest_http_basicAuthPassword: null,

                noop: null
            },

            ui_properties: {
                display_name: "REST ",
                description: "Allows calling and provisioning REST web services.",
                icon: null,

                noop: null
            }
        }
    }
}