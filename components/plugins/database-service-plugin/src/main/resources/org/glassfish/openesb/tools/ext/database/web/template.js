{
    id: "database-template",

    display_name: "Database Adapter",
    description: "Basic template for database adapters.",
    icon: null,
    tags: ["template", "adapter", "database"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10100,

    section: "templates",

    item_data: {
        node_type: "database",
        node_data: {
            properties: {
                code_name: "database-",
                is_shared: false,

                database_jdbc_operationType: null,
                database_jdbc_jndiName: null,
                database_jdbc_sql: null,
                database_jdbc_paramOrder: null,
                database_jdbc_TableName: null,

                noop: null
            },

            ui_properties: {
                display_name: "Database ",
                description: "Allows polling from and writing to databases.",
                icon: null,

                noop: null
            }
        }
    }
}