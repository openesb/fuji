/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var JRUBY_SERVICE = "jruby"; // NOI18N

//----------------------------------------------------------------------------------------
var JRubyServiceNode = Class.create(ServiceNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(JRUBY_SERVICE, this);
        }

        Ws.Utils.apply_defaults(this.properties, {
            jruby_code:
                "require 'java'\n" +
                "require 'XPath'\n" +
                "require 'ESB'\n" +
                "include_class \"javax.xml.namespace.QName\"\n" +
                "\n" +
                "@@service = QName.new \"http://fuji.dev.java.net/application/[app]\", \"[service]\"\n" +
                "\n" +
                "def process(inMsg)\n" +
                "  # payload is a java org.w3c.dom.Node object\n" +
                "  payload = inMsg.getPayload\n" +
                "  \n" +
                "  # your code goes here\n" +
                "  \n" +
                "  # return message\n" +
                "  inMsg\n" +
                "end\n"
        });
    },

    pc_get_descriptors: function($super) {
        var container = $super();

        container.sections[0].properties.push({
            name: "properties_jruby_code",
            display_name: "Code",
            description: "JRuby code which will be executed",
            value: this.properties.jruby_code,
            type: Ws.PropertiesSheet.CODE,
            code_type: "ruby",

            auto_adjust_height: true,
            maximizable: true
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var JRubyServiceNodeUi = Class.create(ServiceNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(JRUBY_SERVICE, JRubyServiceNode);
Ws.Graph.NodeUiFactory.register_type(JRUBY_SERVICE, JRubyServiceNodeUi);
