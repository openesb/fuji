{
    id: "xslt-template",

    display_name: "XSLT Service",
    description: "Basic template for XSLT services.",
    icon: null,
    tags: ["template", "service", "xslt"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10900,

    section: "templates",

    item_data: {
        node_type: "xslt",
        node_data: {
            properties: {
                code_name: "xslt-",
                is_shared: false,

                xslt_code: null,

                noop: null
            },

            ui_properties: {
                display_name: "XSLT ",
                description: "Allows to perform XSLT transformations.",
                icon: null,

                noop: null
            }
        }
    }
}
