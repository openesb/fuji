/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.tools.ext.xslt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.jbi.descriptor.Connection;
import org.glassfish.openesb.tools.common.jbi.descriptor.Connection.Provider;
import org.glassfish.openesb.tools.common.jbi.descriptor.Jbi;
import org.glassfish.openesb.tools.common.service.DefaultServicePackager;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * Service packager for XsltSE.
 * <p>
 * <b>NOTE:</b> This class does not account for Invoke activities in the transformmap.

 * @author kcbabo
 * @author ksimpson
 * @author chikkala (just ported code from old interface to new)
 */
public class ServicePackagerImpl extends DefaultServicePackager {

    private static final Logger LOG = Logger.getLogger(ServicePackagerImpl.class.getName());
    // Base namespace uri for Fuji XsltSE services
    private static final String TMAP_NS =
            "http://fuji.dev.java.net/application/";

    // Service name expected for all XSLT SE provided endpoints
    private static final String XSLT_SE_SERVICE = "xsltse";
    // Path to jbi.xml
    private static final String JBI_XML_PATH = "META-INF/jbi.xml";

    // Name of the WSDL file containing a service definition
//    private static final String SERVICE_WSDL_FILE = "binding.wsdl";
    // Relative path to XSL source files in output directory
//    private static final String XSL_PATH = "classes/xslt";
    // Name of the XSL transformation mapping file
    private static final String TRANSFORM_MAP_NAME = "transformmap.xml";
    private Queue<String> mSrvcNames;

    public ServicePackagerImpl(ServiceDescriptor sd) {
        super(sd);
        mSrvcNames = new LinkedList<String>();
    }

    @Override
    public void beforeApplicationPackaging(ServiceProjectModel prjModel, List<String> services, ApplicationProjectModel appPrjModel) throws IOException {
        String applicationId = appPrjModel.getProperty(ApplicationProjectModel.PROP_APPLICATION_ID);
        File appOutputDir = appPrjModel.getOutputDir();
        beforeApplicationPackagingWithXSLTServices(applicationId, appOutputDir, services);
    }

    @Override
    public void beforeServicePackaging(ServiceProjectModel prjModel, List<String> services) throws IOException {
        File outputDir = prjModel.getOutputDir();
        beforeXSLTServicePackaging(outputDir, services);
    }

    /** Perform pre-processing for XSLT service artifacts.
     *
     * @param outputDir service unit output dir (/target/classes/xslt ) where processed/compiled files from source
     * are placed.
     * @throws java.lang.Exception
     */
    private void beforeXSLTServicePackaging(
            final File outputDir,
            final List<String> serviceNames) throws IOException {
        LOG.fine("Performing pre-processing for XSLT service artifacts.");
        try {
            // copy xsl files
            //File xslDir = new File(outputDir, XSL_PATH);
            //FileUtils.copyDirectoryStructure(xslDir, resourceDir);

            // merge transformmap.xml instances
            File rootMapFile = new File(outputDir, TRANSFORM_MAP_NAME);
            Document rootMapDoc = null;

            // iterate through all the services and add them to the map
            for (String service : serviceNames) {
                File serviceMapFile;
                Document serviceMapDoc;

                serviceMapFile = new File(outputDir,
                        service + File.separator + TRANSFORM_MAP_NAME);
                serviceMapDoc = parseXmlFile(serviceMapFile);

                // if we don't have a root doc yet, use the first service map
                // as a template
                if (rootMapDoc == null) {
                    rootMapDoc = serviceMapDoc;
                } else {
                    importServiceDefinition(rootMapDoc, serviceMapDoc);
                }
                serviceMapFile.delete();
            }

            mSrvcNames.addAll(serviceNames);
            writeTransformMap(rootMapDoc, rootMapFile);

            updateJbiXml(outputDir);
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw (IOException) new IOException().initCause(e);
        }
    }

    /**
     * We use this hook to add service connections for XSLT SE services.
     * @param applicationId applicationId of this project.
     * @param appOutputDir  app output directory (${basedir}/target/classes)
     * @param services
     * @throws IOException
     */
    private void beforeApplicationPackagingWithXSLTServices(
            String applicationId,
            final File appOutputDir,
            final List<String> services) throws IOException {
        LOG.fine("In XSLT Service Pacakger before application packaging...");
        if (true) {
            return;
        }
        try {
            /*
             * Previous code erroneously added connection to map Fuji default
             * endpoint for XsltSE service to itself. The implementer should
             * have taken more time to understand XsltSE endpoint naming
             * conventions, described in detail on the 'Endpoint-Configuration'
             * tab at: http://wiki.open-esb.java.net/Wiki.jsp?page=Transformmap
             */

            File saJbiXml = new File(appOutputDir, JBI_XML_PATH);
            Jbi jbiXml = Jbi.newJbi(new FileInputStream(saJbiXml));
            String appName = applicationId;
            List<Connection> cnxns = jbiXml.getServiceAssembly().getConnections();
            for (String serviceName : services) {
                for (Connection conn : cnxns) {
                    Provider prov = conn.getProvider();
                    if (prov.getEndpointName().equals(serviceName + "_endpoint")) {
//                        // only passed xslt service names, so this must be XsltSE
//                        prov.setServiceName(new QName(
//                                TMAP_NS + appName, XSLT_SE_SERVICE, "ns"+ appName));
//                        prov.setEndpointName(serviceName +"Service");
                        prov.setServiceName(new QName(
                                TMAP_NS + appName, XSLT_SE_SERVICE, "ns" + appName));
                    }
                }
            }

            FileOutputStream fos = new FileOutputStream(saJbiXml);
            jbiXml.writeTo(fos);
            fos.close();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw (IOException) new IOException().initCause(e);
        }
    }

    /**
     * Parse the given file into a DOM Document.
     * @param xmlFile file containing an XML document
     * @return
     * @throws java.lang.Exception
     */
    private Document parseXmlFile(
            final File xmlFile) throws Exception {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        return dbf.newDocumentBuilder().parse(xmlFile);
    }

    /**
     * Write the transformation map document to the specified file handle.
     * @param mapDoc
     * @param mapFile
     * @throws java.lang.Exception
     */
    private void writeTransformMap(
            final Document mapDoc,
            final File mapFile) throws Exception {

        FileOutputStream fos = new FileOutputStream(mapFile);
        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.transform(new DOMSource(mapDoc), new StreamResult(fos));
        fos.close();
    }

    /**
     * Grab the service definition in a service-specific transform map and add
     * it to the root mapping file.
     * @param rootMapDoc
     * @param serviceDoc
     */
    private void importServiceDefinition(
            final Document rootMapDoc,
            final Document serviceDoc) {

        // Add import definitions
        NodeList importDefs =
                serviceDoc.getDocumentElement().getElementsByTagName("import");
        for (int i = 0; i < importDefs.getLength(); i++) {
            rootMapDoc.getDocumentElement().appendChild(
                    rootMapDoc.importNode(importDefs.item(i), true));
        }

        // Add service definitions
        NodeList serviceDefs =
                serviceDoc.getDocumentElement().getElementsByTagName("service");
        for (int i = 0; i < serviceDefs.getLength(); i++) {
            rootMapDoc.getDocumentElement().appendChild(
                    rootMapDoc.importNode(serviceDefs.item(i), true));
        }
    }

    /**
     * XsltSE does not conform to Fuji's presumptuous endpoint naming strategy
     * and, therefore, we must alter the endpoint and service names.
     *
     * @param suDir service unit root directory
     * @throws java.lang.Exception
     */
    private void updateJbiXml(
            final File suDir) throws Exception {
        File jbiXmlFile = new File(suDir, JBI_XML_PATH);
        Jbi jbi = Jbi.newJbi(new FileInputStream(jbiXmlFile));
        // apparently framework doesn't bother to set this attribute...
        jbi.getServices().setBindingComponent(false);
        Logger log = Logger.getLogger("com.sun.jbi.interceptors.internal.flow");
//        for (Provides p : jbi.getServices().getProvides()) {
////            String newUri = p.getServiceName()  // api does not provide app name...sigh
////                    .getNamespaceURI().replace("application", "transformmap");
////            p.setServiceName(new QName(newUri, XSLT_SE_SERVICE, "nsTMap"));
////            p.setEndpointName(mSrvcNames.poll() +"_endpoint");
//            String uri = p.getServiceName().getNamespaceURI();
//            p.setServiceName(new QName(uri, XSLT_SE_SERVICE, "nsTMap"));
//            log.severe(p.getServiceName() +"  -  "+ p.getEndpointName());
//        }
        FileOutputStream fos = new FileOutputStream(jbiXmlFile);
        jbi.writeTo(fos);
        fos.close();
    }
}
