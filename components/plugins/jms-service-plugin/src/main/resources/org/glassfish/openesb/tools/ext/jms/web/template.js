{
    id: "jms-template",

    display_name: "JMS Adapter",
    description: "Basic template for JMS adapters.",
    icon: null,
    tags: ["template", "adapter", "jms"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10450,

    section: "templates",

    item_data: {
        node_type: "jms",
        node_data: {
            properties: {
                code_name: "jms-",
                is_shared: false,

                jms_destination: null,
                jms_destinationType: null,
                jms_messageType: null,
                jms_textPart: null,
                jms_connectionURL: null,

                noop: null
            },

            ui_properties: {
                display_name: "JMS ",
                description: "Provides JMS interoperability.",
                icon: null,

                noop: null
            }
        }
    }
}