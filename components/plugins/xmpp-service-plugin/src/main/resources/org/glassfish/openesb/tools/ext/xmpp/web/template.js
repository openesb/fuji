{
    id: "xmpp-template",

    display_name: "XMPP Adapter",
    description: "Basic template for XMPP adapters.",
    icon: null,
    tags: ["template", "adapter", "xmpp", "jabber"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10800,

    section: "templates",

    item_data: {
        node_type: "xmpp",
        node_data: {
            properties: {
                code_name: "xmpp-",
                is_shared: false,

                service_configuration_mode: null,
                
                xmpp_domain: "localhost",
                xmpp_port: 5222,
                xmpp_username: "username",
                xmpp_password: "password",
                xmpp_jabberId: "user@localhost/spark",

                noop: null
            },

            ui_properties: {
                display_name: "XMPP ",
                description: "Allows to send and receive messages over XMPP protocol.",
                icon: null,

                noop: null
            }
        }
    }
}
