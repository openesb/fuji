/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.ext.xmpp;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.DefaultToolsLookup;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;


/**
 *
 * @author chikkala
 */
public class ProviderImplTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ProviderImplTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ProviderImplTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testProviderImpl() throws Exception {
        // just instatiating it is the test :-)
        ProviderImpl provider = new ProviderImpl();
        URL url = provider.getServiceDescriptorURL();
        System.out.println("ServiceDescriptor URL " + url);
        assertNotNull(url);
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor("xmpp");
        System.out.println("Service Descriptor found for " + sd.getServiceType());
        assertEquals("xmpp", sd.getServiceType());
    }

    private URL getArchetypeURL(ServiceDescriptor sd) {
        Map<String, URL> webRes = sd.getWebResources();
        for (String key : webRes.keySet()) {
            if (key.endsWith("archetype.jar")) {
                return webRes.get(key);
            }
        }
        return null;
    }

    public void testArchetypeResource() throws Exception {
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor("xmpp");
        URL archetypeURL = getArchetypeURL(sd);
        assertNull("NULL Archetype URL expected", archetypeURL);
    }

    public void testConfigurations() throws Exception {
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor("xmpp");
        List<String> configNames = new ArrayList<String>();
        List<Configuration> list = sd.getConfigurations();
        Map<String, Configuration> configMap = new HashMap<String, Configuration>();
        Map<String, List<PropertyGroup>> configPGMap = new HashMap<String, List<PropertyGroup>>();
        for (Configuration config : list) {
            configNames.add(config.getName());
            configMap.put(config.getName(), config);
            List<PropertyGroup> propGroupList = config.getPropertyGroupList();
            configPGMap.put(config.getName(), propGroupList);
        }
        Configuration config = configMap.get("any-xmpp");
        assertNotNull("any-xmpp configuration expected", config);

        config = configMap.get("google-talk");
        assertNotNull("google talk configuration expected", config);

        assertEquals("two configurations expected ", 2, configMap.keySet().size());

        List<PropertyGroup> groupList = configPGMap.get("any-xmpp");
        assertEquals("two proprty groups expected ", 2, groupList.size());

        groupList = configPGMap.get("google-talk");
        assertEquals("two proprty groups expected ", 2, groupList.size());

        for (PropertyGroup group : groupList) {
            String name = group.getName();
            if (!("server-settings".equals(name) || "target-settings".equals(name))) {
                fail("server-settings or target-settings expected as group name " + name);
            }
            List<Property> props = group.getPropertyList();
            for (Property prop : props) {
                if ("xmpp.domain".equals(prop.getName())){
                    String defValue = prop.getValue();
                    assertEquals("talk.google.com expected as default value ", "talk.google.com", defValue);
                }
            }
        }

    }
}

