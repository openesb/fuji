/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServicePackager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.fuji.maven.plugins.java;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.maven.spi.ServiceToolsLocator;
import com.sun.jbi.fuji.maven.util.DefaultServicePackager;
import com.sun.jbi.fuji.maven.util.MavenUtils;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * 
 * @author gpatil
 */
public class JavaServicePackager extends DefaultServicePackager {
    private static String serviceType = "java"; //NOI18N
    private static String pojoDir = "pojo"; //NOI18N
    private static Logger logger = Logger.getLogger(JavaServicePackager.class.getName());
   
    public JavaServicePackager() {
        super(serviceType);
    }

    @Override
    public void generateServiceUnit(
            final ServiceToolsLocator locator,
            final IFLModel ifl,
            final File baseDirectory,
            final Properties props,
            final List usedFiles) throws IOException {

        // Log a message to inform us that the overriden method is being executed.
        logger.log(Level.FINE, "Executing overridden method generateServiceUnit in class JavaServicePackager");

        for (String serviceName: ifl.getServicesForType(serviceType)) {
            ZipInputStream zipInput = null;
            InputStream archetypeIS = null;
            try {
                URL archetypeURL = getServiceArchetypeURL(locator, serviceType);
                archetypeIS = archetypeURL.openStream();
                zipInput = new ZipInputStream(archetypeIS);

                ZipEntry entry = null;
                while ((entry = zipInput.getNextEntry()) != null) {
                    // We can't extract the archetype as is because it has a funky
                    // structure.  This code massages the directory naming to match
                    // the current app layout.
                    if (entry.getName().startsWith("archetype-resources/src/main") &&
                            !entry.isDirectory()) {

                        // need to mangle the name a bit - this is so hacky
                        String name = entry.getName();
                        name = name.replace("archetype-resources/", "");

                        // take care of the static Fuji directories
                        if (name.startsWith("src/main/config")) {
                            name = name.replace("src/main/config",
                                    "src/main/config/" + serviceType + "/" + serviceName);
                        } else if (name.startsWith("src/main/resources/META-INF")) {
                            name = name.replace("src/main/resources",
                                    "src/main/resources/" + serviceType);
                        } else if (name.startsWith("src/main/resources")) {
                            name = name.replace("src/main/resources",
                                    "src/main/resources/" + serviceType + "/" + serviceName);
                        } else if (name.startsWith("src/main/" + serviceType)) {
                            name = name.replace("src/main/" + serviceType + "/Service.java",
                                    "src/main/" + serviceType + "/" + pojoDir + "/Svc" + serviceName + ".java");
                        }

                        // Add the file to the used file list that is returned to the caller
                        final File file = new File(baseDirectory, name);
                        usedFiles.add(file);

                        // Do not overwrite existing artifacts
                        if (file.exists()) {
                            continue;
                        }

                        // New generation, need to create parent dir
                        if (!file.getParentFile().exists()) {
                            file.getParentFile().mkdirs();
                        }

                        MavenUtils.writeFile(zipInput, file, entry.getSize());
                        if (file.getName().endsWith(".java")){ //NOI18N
                            Properties newProps = new Properties();
                            newProps.putAll(props);
                            newProps.put("serviceName", serviceName);//NOI18N
                            MavenUtils.replaceTokensInFile(file, newProps);
                        }
                    }
                }
            } finally {
                try {
                    if (zipInput != null) {
                        zipInput.close();
                    }
                } catch (IOException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }

    private void cleanUpJavaResources(
                final File outputDir,   // .../target
                final File resourceDir, // .../target/classes/java
                final List<String> serviceNames)  throws IOException {

        if ((resourceDir != null) && (resourceDir.exists())){
            File[] files = resourceDir.listFiles();
            for (File fl : files){
                if (serviceNames.contains(fl.getName())){
                    continue;
                } else if ("META-INF".equalsIgnoreCase(fl.getName())){ //NOI18N
                    continue;
                } else if (pojoDir.equals(fl.getName())){
                    FileFilter yesJava = new FileFilter() {
                        public boolean accept(final File file) {
                            if ( file.getName().endsWith(".java")){ //NOI18N
                                return true;
                            }
                            return false;
                        }
                    };
                    File pojoResDir  = new File(resourceDir,  pojoDir);
                    File[] javas = pojoResDir.listFiles(yesJava);
                    for (File java: javas){
                        java.delete();
                    }
                } else {
                    if (fl.isDirectory()){
                        MavenUtils.deleteDirectory(fl);
                    } else {
                        fl.delete();
                    }
                }
            }
        }
        
    }

    @Override
    public void beforeServicePackaging(
            final File outputDir,  // .../target
            final File resourceDir, // .../target/classes/java
            final List<String> serviceNames) throws IOException {
        // Log a message to inform us that the overriden method is being executed.
        logger.log(Level.FINE, "Executing overridden method beforeServicePackaging in class JavaServicePackager");

        if ((serviceNames != null) && (serviceNames.size() > 0)){
            File classDir = new File(outputDir, "classes");//NOI18N
            File svcpkg = null;
            File destSvcpkg = null;

            cleanUpJavaResources(outputDir, resourceDir, serviceNames);

            FileFilter noJava = new FileFilter() {
                public boolean accept(final File file) {
                    if ( file.getName().endsWith(".java")){ //NOI18N
                        return false;
                    }
                    return true;
                }
            };

            svcpkg = new File(classDir, pojoDir); //NOI18N
            destSvcpkg  = new File(classDir,  serviceType + "/" + pojoDir);
            destSvcpkg.mkdirs();
            // Copy class files/"resources" to SU directory.
            MavenUtils.copy(svcpkg, destSvcpkg, noJava);
        }
    }

    @Override
    public void beforeApplicationPackaging(
            File appRoot,
            List<String> services) throws IOException{
        if (services.size() > 0){
            // Delete the POJO service package so that it won't get packaged into SA root.
            File pojoClassesDir = new File (appRoot, pojoDir);
            MavenUtils.deleteDirectory(pojoClassesDir);
            pojoClassesDir  = new File(appRoot,  serviceType + "/" + pojoDir);
            MavenUtils.deleteDirectory(pojoClassesDir);
        }
    }
}
