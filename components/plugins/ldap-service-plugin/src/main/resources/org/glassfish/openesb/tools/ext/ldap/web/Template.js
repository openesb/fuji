{
    id: "ldap-template",

    display_name: "LDAP Adapter",
    description: "Basic template for LDAP adapters.",
    icon: null,
    tags: ["template", "adapter", "ldap"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10800,

    section: "templates",

    item_data: {
        node_type: "ldap",
        node_data: {
            properties: {
                code_name: "ldap-",
                is_shared: false,

                ldap_location: "ldap://localhost:389",
                ldap_principal: "",
                ldap_credential: "",
                ldap_sslType: "None",
                ldap_authentication: "None",
                ldap_protocol: "",
                ldap_trustStore: "",
                ldap_trustStorePassword: "",
                ldap_trustStoreType: "",
                ldap_keyStore: "",
                ldap_keyStorePassword: "",
                ldap_keyStoreUsername: "",
                ldap_keyStoreType: "",
                ldap_tlsSecurity: "NO",

                noop: null
            },

            ui_properties: {
                display_name: "LDAP ",
                description: "Provides access to LDAP servers.",
                icon: null,

                noop: null
            }
        }
    }
}
