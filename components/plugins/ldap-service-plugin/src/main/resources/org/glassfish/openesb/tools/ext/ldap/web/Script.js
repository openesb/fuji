/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var LDAP_ADAPTER = "ldap";

//----------------------------------------------------------------------------------------
var LdapAdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    LDAP_ADAPTER, this);
        }

        Ws.Utils.apply_defaults(this.properties, {
            ldap_location: "ldap://localhost:389",
            ldap_authentication: "None",
            ldap_sslType: "None",
            ldap_tlsSecurity: "NO",
        });
    },

    pc_get_descriptors: function($super) {
        var container = $super();

        container.sections[0].properties.push({
            name: "properties_ldap_location",
            display_name: "Location",
            description: "Connection URL for the LDAP server",
            value: this.properties.ldap_location,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_principal",
            display_name: "Principal",
            description: "Principal (user name) for authentication",
            value: this.properties.ldap_principal,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_credential",
            display_name: "Credential",
            description: "Credential (Password) for authentication",
            value: this.properties.ldap_credential,
            type: Ws.PropertiesSheet.PASSWORD
        });

        container.sections[0].properties.push({
            name: "properties_ldap_sslType",
            display_name: "SSL Type",
            description: "Type of connection for communication with the LDAP server",
            value: this.properties.ldap_sslType,
            type: Ws.PropertiesSheet.ENUM,
                values:
                    [
                        {value: "None", display_name: "None"},
                        {value: "Enable SSL", display_name: "Enable SSL"},
                        {value: "TLS On Demand", display_name: "TLS On Demand"}
                    ]
        });

        container.sections[0].properties.push({
            name: "properties_ldap_authentication",
            display_name: "Authentication",
            description: "Authentication method to use for connection to LDAP server",
            value: this.properties.ldap_authentication,
            type: Ws.PropertiesSheet.ENUM,
                values:
                    [
                        {value: "None", display_name: "None"},
                        {value: "Simple", display_name: "Simple"}
                    ]
        });

        container.sections[0].properties.push({
            name: "properties_ldap_protocol",
            display_name: "Protocol",
            description: "Protocol to use for SSL connection to the LDAP server",
            value: this.properties.ldap_protocol,
            type: Ws.PropertiesSheet.ENUM,
                values:
                    [
                        {value: "SSL", display_name: "SSL"},
                        {value: "SSLv2", display_name: "SSLv2"},
                        {value: "SSLv3", display_name: "SSLv3"},
                        {value: "TLS", display_name: "TLS"},
                        {value: "TLSv1", display_name: "TLSv1"}
                    ]
        });

        container.sections[0].properties.push({
            name: "properties_ldap_trustStore",
            display_name: "TrustStore",
            description: "Full path to TrustStore file used for SSL connections",
            value: this.properties.ldap_trustStore,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_trustStorePassword",
            display_name: "TrustStore Password",
            description: "Password for accessing the TrustStore",
            value: this.properties.ldap_trustStorePassword,
            type: Ws.PropertiesSheet.PASSWORD
        });

        container.sections[0].properties.push({
            name: "properties_ldap_trustStoreType",
            display_name: "TrustStore Type",
            description: "Type of TrustStore (defaults to JKS)",
            value: this.properties.ldap_trustStoreType,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_keyStore",
            display_name: "KeyStore",
            description: "Full path to KeyStore file used for SSL connections",
            value: this.properties.ldap_keyStore,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_keyStorePassword",
            display_name: "KeyStore Password",
            description: "Password for accessing the KeyStore",
            value: this.properties.ldap_keyStorePassword,
            type: Ws.PropertiesSheet.PASSWORD
        });

        container.sections[0].properties.push({
            name: "properties_ldap_keyStoreUsername",
            display_name: "KeyStore Username",
            description: "Username for accessing the KeyStore",
            value: this.properties.ldap_keyStoreUsername,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_keyStoreType",
            display_name: "KeyStore Type",
            description: "Type of KeyStore (defaults to JKS)",
            value: this.properties.ldap_keyStoreType,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_ldap_tlsSecurity",
            display_name: "TLS Security",
            description: "Enable or disable TLS security",
            value: this.properties.ldap_tlsSecurity,
            type: Ws.PropertiesSheet.ENUM,
                values:
                    [
                        {value: "NO", display_name: "No"},
                        {value: "YES", display_name: "Yes"}
                    ]
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var LdapAdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(LDAP_ADAPTER, LdapAdapterNode);
Ws.Graph.NodeUiFactory.register_type(LDAP_ADAPTER, LdapAdapterNodeUi);
