{
    id: "scheduler-template",

    display_name: "Scheduler",
    description: "Basic template for schedulers.",
    icon: null,
    tags: ["template", "adapter", "scheduler"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10650,

    section: "templates",

    item_data: {
        node_type: "scheduler",
        node_data: {
            properties: {
                code_name: "scheduler-",
                is_shared: false,

                service_configuration_mode: null,
                
                scheduler_type: null,
                scheduler_message: null,
                scheduler_starting: null,
                scheduler_ending: null,
                scheduler_timezone: null,

                scheduler_interval: null,
                scheduler_repeat: null,

                scheduler_cron_expr: null,

                scheduler_duration: null,

                noop: null
            },

            ui_properties: {
                display_name: "Scheduler ",
                description: "Allows to send messages on a regular basis.",
                icon: null,

                noop: null
            }
        }
    }
}
