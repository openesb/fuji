{
    id: "rss-template",

    display_name: "RSS Adapter",
    description: "Basic template for RSS adapters.",
    icon: null,
    tags: ["template", "adapter", "rss", "gestalt"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10600,

    section: "templates",

    item_data: {
        node_type: "rss",
        node_data: {
            properties: {
                code_name: "rss-",
                is_shared: false,

                rss_address: "http://www.oreillynet.com/pub/feed/20",
                rss_pollingInterval: 20,
                rss_filterByType: "publishDate",
                rss_filterByValue: "now",
                rss_entriesPerMessage: 10,

                noop: null
            },

            ui_properties: {
                display_name: "RSS ",
                description: "Allows to poll from and publish to RSS feeds.",
                icon: null,

                noop: null
            }
        }
    }
}