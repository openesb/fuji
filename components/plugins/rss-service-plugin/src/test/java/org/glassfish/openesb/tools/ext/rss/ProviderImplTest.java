/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.ext.rss;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.DefaultToolsLookup;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
/**
 *
 * @author chikkala
 */
public class ProviderImplTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ProviderImplTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ProviderImplTest.class);
    }
    /**
     * Rigourous Test :-)
     */
    public void testProviderImpl() throws Exception {
        // just instatiating it is the test :-)
        ProviderImpl provider = new ProviderImpl();
        URL url = provider.getServiceDescriptorURL();
        System.out.println("ServiceDescriptor URL " + url);
        assertNotNull(url);
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor("rss");
        System.out.println("Service Descriptor found for " + sd.getServiceType());
        assertEquals("rss", sd.getServiceType());        
    }

    public void testConfigurations() throws Exception {
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor("rss");
        List<String> configNames = new ArrayList<String>();
        List<Configuration> list = sd.getConfigurations();
        Map<String, Configuration> configMap = new HashMap<String, Configuration>();
        Map<String, List<PropertyGroup>> configPGMap = new HashMap<String, List<PropertyGroup>>();
        for (Configuration config : list) {
            configNames.add(config.getName());
            configMap.put(config.getName(), config);
            List<PropertyGroup> propGroupList = config.getPropertyGroupList();
            configPGMap.put(config.getName(), propGroupList);
        }
        Configuration defConfig = sd.getDefaultConfiguration();
        if ( configMap.get(defConfig.getName()) == null) {
            // add default configuraiton.
            configNames.add(defConfig.getName());
            configMap.put(defConfig.getName(), defConfig);
            List<PropertyGroup> propGroupList = defConfig.getPropertyGroupList();
            assertNotNull("default config property group expected", propGroupList);
            System.out.println("Default group " + defConfig.getName() + " pg size " + propGroupList.size());
            configPGMap.put(defConfig.getName(), propGroupList);
        }

        assertEquals("zero configurations expected ", 1, configMap.keySet().size());

        Configuration config = configMap.get("default");
        assertNotNull("default configuration expected", config);
        System.out.println("### Config DN= " + config.getInfo().getDisplayName());
         System.out.println("### Config SD= " + config.getInfo().getShortDescription());

        config.getProperties().list(System.out);

        List<PropertyGroup> groupList = configPGMap.get("default");
        assertEquals("one property groups expected ", 1, groupList.size());

        for (PropertyGroup group : groupList) {
            String name = group.getName();
            if (!("".equals(name)) ) {
                fail("default property group expected as group name " + name);
            }
            System.out.println("### DN= " + group.getInfo().getDisplayName());
            System.out.println("### SD= " + group.getInfo().getShortDescription());

            List<Property> props = group.getPropertyList();
            System.out.println("Properties in default prop group " + props.size());
            assertEquals("5 Properties expected in default prop group ", 5, props.size());
            for (Property prop : props) {
                if ("rss.pollingInterval".equals(prop.getName())){
                    String defValue = prop.getValue();
                    assertEquals("20 expected as default value ", "20", defValue);
                }
            }
        }

    }
}

