{
    id: "file-template",

    display_name: "File Adapter",
    description: "Basic template for File adapters.",
    icon: null,
    tags: ["template", "adapter", "file"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10200,

    section: "templates",

    item_data: {
        node_type: "file",
        node_data: {
            properties: {
                code_name: "file-",

                noop: null
            },

            ui_properties: {
                display_name: "File ",
                description: "Used to poll or write data to a specified file on the filesystem.",
                icon: null,

                noop: null
            }
        }
    }
}