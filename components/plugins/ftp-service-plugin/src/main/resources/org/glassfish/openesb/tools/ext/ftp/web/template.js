{
    id: "ftp-template",

    display_name: "FTP Adapter",
    description: "Basic template for FTP adapters.",
    icon: null,
    tags: ["template", "adapter", "ftp"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10300,

    section: "templates",

    item_data: {
        node_type: "ftp",
        node_data: {
            properties: {
                code_name: "ftp-",
                is_shared: false,

                service_mode: null,
                ftp_use: null,
                ftp_encoding_style: null,
                ftp_charset: null,
                ftp_receive_from: null,
                ftp_receive_from_is_regex: null,
                ftp_send_to: null,
                ftp_send_to_has_pattern: null,
                ftp_pre_transfer_cmd: null,
                ftp_pre_transfer_target: null,
                ftp_pre_transfer_target_has_pattern: null,
                ftp_post_transfer_cmd: null,
                ftp_post_transfer_target: null,
                ftp_post_transfer_target_has_pattern: null,
                ftp_user: null,
                ftp_password: null,
                ftp_host: null,
                ftp_port: null,
                ftp_dir_list_style: null,
                ftp_mode: null,
                ftp_poll_interval_millis: null,

                noop: null
            },

            ui_properties: {
                display_name: "FTP ",
                description: "Allows polling from and writing to FTP locations.",
                icon: null,

                noop: null
            }
        }
    }
}