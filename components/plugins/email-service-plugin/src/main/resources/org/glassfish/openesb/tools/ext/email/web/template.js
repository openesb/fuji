{
    id: "email-template",

    display_name: "EMAIL Adapter",
    description: "Basic template for EMAIL adapters.",
    icon: null,
    tags: ["template", "adapter", "email", "mail"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10700,

    section: "templates",

    item_data: {
        node_type: "email",
        node_data: {
            properties: {
                code_name: "email-",
                is_shared: false,

                service_configuration_mode: null,
                
                direction: null,
                email_type: null,

                email_emailServer: null,
                email_useSSL: null,
                email_port: null,
                email_userName: null,
                email_password: null,
                
                email_smtp_location: null,
                email_smtp_charset: null,
                email_smtp_use: null,
                email_smtp_encodingStyle: null,

                email_pop3_or_imap_mailFolder: null,
                email_pop3_or_imap_maxMessageCount: null,
                email_pop3_or_imap_messageAckMode: null,
                email_pop3_or_imap_messageAckOperation: null,
                email_pop3_or_imap_pollingInterval: null,

                noop: null
            },

            ui_properties: {
                display_name: "EMAIL ",
                description: "Allows to send and receive email messages.",
                icon: null,

                noop: null
            }
        }
    }
}