/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BaseComponent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.component.base;

import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.InstallationContext;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.DeploymentException;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.management.ObjectName;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.service.ServiceMessageImpl;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.descriptor.Provides;
import com.sun.jbi.framework.result.ComponentTaskResult;
import com.sun.jbi.framework.result.ExceptionInfo;
import com.sun.jbi.framework.result.JbiTask;
import com.sun.jbi.framework.result.TaskResultDetails.MessageType;
import com.sun.jbi.framework.result.TaskResultDetails.TaskId;
import com.sun.jbi.framework.result.TaskResultDetails.TaskResult;


/**
 * This is a <b>temporary</b> solution for getting new components bootstraped
 * in the fuji codeline.  Once we have the annotation-based component model
 * up and running, this class is headed to the dust bin!
 *
 * @author Sun Microsystems, Inc.
 */
public class BaseComponent 
        implements javax.jbi.component.Component,
                   javax.jbi.component.Bootstrap,
                   javax.jbi.component.ServiceUnitManager,
                   javax.jbi.component.ComponentLifeCycle {
    
    protected ComponentContext context_;
    
    protected Logger log_;
    
    private AcceptThread acceptThread_;
    
    private MessageListener listener_;
    
    private Map<String, Jbi> descriptors_ = new HashMap<String, Jbi>();
    
    private Map<String, List<ServiceEndpoint>> endpoints_ = 
            new HashMap<String, List<ServiceEndpoint>>();
    
    /*************** javax.jbi.component.Component **************/
    
    public ComponentLifeCycle getLifeCycle() {
        return this;
    }

    public Document getServiceDescription(ServiceEndpoint enpt) {
        return null;
    }

    public ServiceUnitManager getServiceUnitManager() {
        return this;
    }

    public boolean isExchangeWithConsumerOkay(ServiceEndpoint enpt, MessageExchange mex) {
        return true;
    }

    public boolean isExchangeWithProviderOkay(ServiceEndpoint enpt, MessageExchange mex) {
        return true;
    }

    public ServiceEndpoint resolveEndpointReference(DocumentFragment frag) {
        return null;
    }

    
    /*************** javax.jbi.component.Bootstrap **************/
    
    public void cleanUp() throws JBIException {
        // this space intentionally left blank
    }

    public ObjectName getExtensionMBeanName() {
        return null;
    }

    public void init(InstallationContext ctx) throws JBIException {
        // this space intentionally left blank
    }

    public void onInstall() throws JBIException {
        // this space intentionally left blank
    }

    public void onUninstall() throws JBIException {
        // this space intentionally left blank
    }

    /*************** javax.jbi.component.ServiceUnitManager **************/
    
    public String deploy(String suName, String suPath) throws DeploymentException {
        return ComponentTaskResult.createSuccessMessage(getComponentName(), TaskId.deploy);
    }

    /** The default implementation activates all the provider endpoints found
     *  in the service unit jbi.xml.
     * @param suName
     * @param suPath
     * @throws javax.jbi.management.DeploymentException
     */
    public void init(String suName, String suPath) throws DeploymentException {
        ArrayList<ServiceEndpoint> activated = new ArrayList<ServiceEndpoint>();
        try {
            Jbi jbi = Jbi.newJbi(new FileInputStream(suPath + "/META-INF/jbi.xml"));
            descriptors_.put(suName, jbi);
            for (Provides service : jbi.getServices().getProvides()) {
                ServiceEndpoint ep = context_.activateEndpoint(
                        service.getServiceName(), service.getEndpointName());
                activated.add(ep);
            }
            endpoints_.put(suName, activated);
        }
        catch (Exception ex) {
            log_.log(Level.WARNING, "Failed to deploy SU " + suName, ex);
        }
    }

    public void shutDown(String suName) throws DeploymentException {
        List<ServiceEndpoint> epList = endpoints_.get(suName);
        if (epList != null) {
            for (ServiceEndpoint ep : epList) {
                try {
                    context_.deactivateEndpoint(ep);
                }
                catch (Exception ex) {
                    log_.log(Level.WARNING, "Failed to deactivate endpoint " + ep, ex);
                }
            }
        }
    }

    public void start(String suName) throws DeploymentException {
        acceptThread_ = new AcceptThread();
        acceptThread_.start();
    }

    public void stop(String suName) throws DeploymentException {
        if (acceptThread_ != null) {
            acceptThread_.interrupt();
            acceptThread_ = null;
        }
    }

    public String undeploy(String suName, String suPath) throws DeploymentException {
        return ComponentTaskResult.createSuccessMessage(getComponentName(), TaskId.undeploy);
    }

    public void init(ComponentContext context) throws JBIException {
        context_ = context;
        log_ = context_.getLogger(BaseComponent.class.getPackage().getName(), null);        
    }

    public void shutDown() throws JBIException {
        // this space intentionally left blank
    }

    public void start() throws JBIException {
        // this space intentionally left blank
    }

    public void stop() throws JBIException {
        // this space intentionally left blank
    }

    protected String getComponentName() {
        return context_ == null ? "" : context_.getComponentName();
    }
    
    protected synchronized void setMessageListener(MessageListener listener) {
        listener_ = listener;
    }
    
    protected Jbi getServiceUnitDescriptor(String suName) {
        return descriptors_.get(suName);
    }
    
    class AcceptThread extends Thread {
        public void run() {
            
            while (true) {
                try {
                    final MessageExchange me = context_.getDeliveryChannel().accept();

                    // channel closed or the component was stopped
                    if (me == null) {
                        return;
                    }
                    
                    // we don't do anything with completed exchanges
                    if (me.getStatus() != ExchangeStatus.ACTIVE) {
                        continue;
                    }

                    // this code only handles the case where an in message is being
                    // delivered on an exchange
                    new Thread() {
                        
                        ServiceMessage replyMsg = null;
                        
                        public void run() {
                            try {
                                if (listener_ != null) {
                                    replyMsg = listener_.onMessage(me.getEndpoint(),
                                            new ServiceMessageImpl(me.getMessage("in")));
                                }

                                // move to the appropriate state
                                if (me instanceof InOnly) {
                                    me.setStatus(ExchangeStatus.DONE);
                                }
                                else {
                                    me.setMessage(replyMsg, "out");
                                }
                                
                                
                            }
                            catch (Exception ex) {
                                me.setError(ex);
                            }

                            // fire away!
                            try {
                                context_.getDeliveryChannel().send(me);
                            }
                            catch (Exception ex) {
                                log_.log(Level.WARNING, 
                                        "Unable to return message exchange", ex);
                            }
                        }
                    }.start();

                }
                catch (Exception ex) {
                    if (ex.getCause() instanceof InterruptedException) {
                        // accept thread terminated, nothing to worry about
                        return;
                    }
                    log_.log(Level.WARNING, "Error while accepting exchange", ex);
                }
            }
        }
    }

}
