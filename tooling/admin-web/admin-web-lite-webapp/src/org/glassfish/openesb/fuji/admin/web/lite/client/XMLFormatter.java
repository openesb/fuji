/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.xml.client.Attr;
import com.google.gwt.xml.client.CDATASection;
import com.google.gwt.xml.client.CharacterData;
import com.google.gwt.xml.client.Comment;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.NamedNodeMap;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.ProcessingInstruction;
import com.google.gwt.xml.client.Text;
import com.google.gwt.xml.client.XMLParser;

/**
*
* @author chikkala
*/
public class XMLFormatter {
	public static final String XML_PI_CSS = "xml-pi";
	public static final String XML_TAG_CSS = "xml-tag";
	public static final String XML_ATTR_CSS = "xml-attribute";
	public static final String XML_VALUE_CSS = "xml-value";
	public static final String XML_COMMENT_CSS = "xml-comment";

	public static final String XML_LT = "&lt;";
	public static final String XML_GT = "&gt;";
	public static final String XML_QT = "&quot;";
	
	public static final String INDENT = "  ";

	private static String escapeXMLChars(String text) {
		String escapedStr = text.replaceAll("<", "&lt;");
		escapedStr = escapedStr.replaceAll(">", "&gt;");
		escapedStr = escapedStr.replaceAll("\"", "&quot;");
		return escapedStr;
	}

	private static String fmtIndent(int level, boolean isAttr) {
	    StringBuilder fmt = new StringBuilder();
        fmt.append("\n");
        for (int i = 0; i < level; ++i) {
            fmt.append(INDENT);
        }
        if ( isAttr ) {
            fmt.append(INDENT);
        }
        return fmt.toString();
	}
	
	private static String beginFmt() {
		return "<pre class=\"xml-view-pre\">";
	}

	private static String endFmt() {
		return "</pre>";
	}

	private static String fmtProcessingInstrcutions() {
		return "<span class=\"xml-pi\">&lt;?</span><span class=\"xml-pi\">xml</span> version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;";
	}

	private static String fmtComments(String comments, String indent) {
		return "<span class=\"" + XML_COMMENT_CSS + "\">" + indent + XML_LT
				+ "!--" + comments + "--" + XML_GT + "</span>";
	}

	@SuppressWarnings("unused")
	private static String getNameWithPrefix(String name, String prefix) {
		if (prefix != null && prefix.trim().length() > 0) {
			return prefix.trim() + ":" + name;
		} else {
			return name;
		}
	}

	private static String fmtAttribute(String attrName, String value,
			String prefix) {
		String fmtAttr = "<span class=\"" + XML_ATTR_CSS + "\">" + attrName
				+ "</span>=<span class=\"" + XML_VALUE_CSS + "\">&quot;"
				+ value + "&quot;</span>";
		return fmtAttr;
	}

	private static String fmtBeginTag(String tagName,
			Map<String, String> attrMap, int level, boolean hasChildren) {

		StringBuilder fmt = new StringBuilder();

		fmt.append("<span class=\"").append(XML_TAG_CSS).append("\">").append(
				XML_LT).append(tagName).append("</span>");

		String attrFmt = "";
		String attrIndent = fmtIndent(level, true);
		for (String attr : attrMap.keySet()) {
			String value = attrMap.get(attr);
			attrFmt = fmtAttribute(attr, value, null);
			fmt.append(attrIndent).append(attrFmt);
		}
		String tagEnd = XML_GT;
		if (!hasChildren) {
		    tagEnd = "/" + XML_GT;
		}
		fmt.append("<span class=\"").append(XML_TAG_CSS).append("\">").append(
		        tagEnd).append("</span>");

		return fmt.toString();
	}

	private static String fmtEndTag(String tagName) {
		StringBuilder fmt = new StringBuilder();

		fmt.append("<span class=\"").append(XML_TAG_CSS).append("\">").append(
				XML_LT).append("/").append(tagName).append(
				"</span><span class=\"").append(XML_TAG_CSS).append("\">")
				.append(XML_GT).append("</span>");

		return fmt.toString();
	}

	private static void appendElement(StringBuilder fmt, Element el, int level) {
		// String prefix = el.getPrefix();
		String tagName = el.getTagName();

		NamedNodeMap nnMap = el.getAttributes();
		Map<String, String> attrMap = new HashMap<String, String>();
		int attrSize = nnMap.getLength();
		for (int i = 0; i < attrSize; ++i) {
			Node attrNode = nnMap.item(i);
			Attr attr = el.getAttributeNode(attrNode.getNodeName());
			if (attr != null && attr.getSpecified()) {
				attrMap.put(attr.getName(), attr.getValue());
			}
		}
		fmt.append("\n");
		for (int i = 0; i < level; ++i) {
			fmt.append(INDENT);
		}
		
		boolean hasChildElements = false;
		NodeList nList = el.getChildNodes();
		int nSize = nList.getLength();
		if ( nSize <= 0 ) {
		    fmt.append(fmtBeginTag(tagName, attrMap, level, false));
		    return;
		} else {
		    fmt.append(fmtBeginTag(tagName, attrMap, level, true));
		}
		for (int i = 0; i < nSize; ++i) {
			Node item = nList.item(i);
			if (item instanceof Element) {
				hasChildElements = true;
				appendElement(fmt, (Element) item, level + 1);
			} else if (item instanceof CharacterData) {
				if (item instanceof Comment) {
					String comment = ((Comment) item).getData();
					comment = escapeXMLChars(comment);
					String cInd = "";
					for (int ix = 0; ix < level + 1; ++ix) {
						cInd += INDENT;
					}
					fmt.append("\n").append(fmtComments(comment, cInd));
				} else if (item instanceof Text) {
					fmt.append(((Text) item).getData());
				} else if (item instanceof CDATASection) {
					fmt.append(((CDATASection) item).getData());
				} else {
					fmt.append(((CharacterData) item).getData());
				}
			}
		}
		
		if (hasChildElements) {
			fmt.append("\n");
			for (int i = 0; i < level; ++i) {
				fmt.append(INDENT);
			}
		}
		fmt.append(fmtEndTag(tagName));
		
	}

	public static String fmtXMLText(String xmlTxt) {
		StringBuilder fmt = new StringBuilder();
		Document doc = null;
		fmt.append(beginFmt());
		try {
			doc = XMLParser.parse(xmlTxt);
		} catch (Exception ex) {
			fmt.append(xmlTxt);
			fmt.append(endFmt());
			return fmt.toString();
		}
		try {
			doc.normalize();
		} catch (Exception ex) {
			// ignore
		}
		try {
			XMLParser.removeWhitespace(doc);
		} catch (Exception ex) {
			// ignore
		}

		NodeList nList = doc.getChildNodes();
		int size = nList.getLength();
		for (int i = 0; i < size; ++i) {
			Node item = nList.item(i);
			if (item instanceof Element) {
				Element el = (Element) item;
				appendElement(fmt, el, 0);
			} else if (item instanceof CharacterData) {
				if (item instanceof Comment) {
					String comment = ((Comment) item).getData();
					comment = escapeXMLChars(comment);
					fmt.append(fmtComments(comment, "")).append("\n");
				} else if (item instanceof Text) {
					fmt.append(((Text) item).getData());
				} else if (item instanceof CDATASection) {
					fmt.append(((CDATASection) item).getData());
				} else {
					fmt.append(((CharacterData) item).getData());
				}

			} else if (item instanceof ProcessingInstruction) {
				fmt.append(fmtProcessingInstrcutions()).append("\n");
			}
		}
		fmt.append(endFmt());
		return fmt.toString();
	}
}
