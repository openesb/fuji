/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;
import java.util.Map;

/**
 * Bean that represents the osgi configuraiton info and has a
 * serialization/deserialization code to/from jmx open types that represent the
 * data.
 * 
 * @author chikkala
 */
public class OSGiConfigInfo implements IsSerializable {
	// property names
	public static final String PROP_PID = "pid";
	public static final String PROP_FACTORY_PID = "factoryPid";
	public static final String PROP_BUNDLE_LOCATION = "bundleLocation";
	public static final String PROP_PROPERTIES = "properties";
	// property values
	private String bundleLocation;
	private String pid;
	private String factoryPid;
	private OSGiPropertyInfoMap properties;

	public OSGiConfigInfo() {
		this.properties = new OSGiPropertyInfoMap();
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getFactoryPid() {
		return factoryPid;
	}

	public void setFactoryPid(String factoryPid) {
		this.factoryPid = factoryPid;
	}

	public String getBundleLocation() {
		return bundleLocation;
	}

	public void setBundleLocation(String bundleLocation) {
		this.bundleLocation = bundleLocation;
	}

	public List<OSGiPropertyInfo> getProperties() {
		return this.properties.getProperties();
	}

	public void setProperties(List<OSGiPropertyInfo> properties) {
		this.properties.setProperties(properties);
	}

	public Map<String, OSGiPropertyInfo> getPropertiesMap() {
		return this.properties.getPropertiesMap();
	}

	public OSGiPropertyInfo getProperty(String name) {
		return this.properties.getProperty(name);
	}

	public String getPropertyValue(String name) {
		return this.properties.getPropertyValue(name);
	}
}
