/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 * Bean that represents the osgi bundle info and has a
 * serialization/deserialization code to/from jmx open types that represent the
 * data.
 * 
 * @author chikkala
 */
public class OSGiBundleInfo implements IsSerializable {

	// bundle properties
	public static final String PROP_BUNDLE_ID = "bundleID";
	public static final String PROP_SYMBOLIC_NAME = "symbolicName";
	public static final String PROP_STATE = "state";
	public static final String PROP_JBI_ARTIFACT_TYPE = "jbiArtifactType";
	public static final String PROP_LAST_MODIFIED = "lastModified";
	public static final String PROP_LOCATION = "location";
	public static final String PROP_HEADERS = "headers";
	public static final String PROP_JBI_DESCRIPTOR = "jbiDescriptor";
	// OSGi States
	public static final String STATE_ACTIVE = "Active";
	public static final String STATE_RESOLVED = "Resolved";
	public static final String STATE_INSTALLED = "Installed";
	public static final String STATE_STARTING = "Starting";
	public static final String STATE_STOPPING = "Stopping";
	public static final String STATE_UNINSTALLED = "Uninstalled";
	public static final String STATE_UNKNOWN = "Unknown"; // Not a OSGi state.
	// JBI Types
	public static final String JBI_BC = "binding-component";
	public static final String JBI_SE = "service-engine";
	public static final String JBI_SA = "service-assembly";
	public static final String JBI_SL = "shared-library";
	public static final String JBI_UNKNOWN = "Unknown";

	// properties
	private String bundleId;
	private String symbolicName;
	private String state;
	private String jbiArtifactType;
	private String location;
	private String lastModified;
	private OSGiPropertyInfoMap headers;
	private String jbiDescriptor;
	private String fmtJbiDescriptor;

	/**
	 * constructor
	 */
	public OSGiBundleInfo() {
		headers = new OSGiPropertyInfoMap();
	}

	/**
	 * Get the value of lastModified
	 * 
	 * @return
	 */
	public String getLastModified() {
		return lastModified;
	}

	/**
	 * 
	 * @param lastModified
	 */
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	/**
	 * 
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * 
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Get the value of jbiArtifactType
	 * 
	 * @return the value of jbiArtifactType
	 */
	public String getJbiArtifactType() {
		return jbiArtifactType;
	}

	/**
	 * Set the value of jbiArtifactType
	 * 
	 * @param jbiArtifactType
	 *            new value of jbiArtifactType
	 */
	public void setJbiArtifactType(String jbiArtifactType) {
		this.jbiArtifactType = jbiArtifactType;
	}

	public String getJbiDescriptor() {
		return jbiDescriptor;
	}

	public void setJbiDescriptor(String jbiDescriptor) {
		this.jbiDescriptor = jbiDescriptor;
	}

	public String getFormattedJbiDescriptor() {
		return fmtJbiDescriptor;
	}

	public void setFormattedJbiDescriptor(String fmtJbiDescriptor) {
		this.fmtJbiDescriptor = fmtJbiDescriptor;
	}

	/**
	 * Get the value of symbolicName
	 * 
	 * @return the value of symbolicName
	 */
	public String getSymbolicName() {
		return symbolicName;
	}

	/**
	 * Set the value of symbolicName
	 * 
	 * @param symbolicName
	 *            new value of symbolicName
	 */
	public void setSymbolicName(String symbolicName) {
		this.symbolicName = symbolicName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public List<OSGiPropertyInfo> getHeaders() {
		return headers.getProperties();
	}

	public void setHeaders(List<OSGiPropertyInfo> headers) {
		this.headers.setProperties(headers);
	}

	public String getHeader(String name) {
		return this.headers.getPropertyValue(name);
	}
}
