/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.Arrays;

/**
 * 
 * @author chikkala
 */
public class OSGiPropertyInfo implements IsSerializable {
	// properties
	public static final String PROP_NAME = "name";
	public static final String PROP_VALUE = "value";
	public static final String PROP_TYPE = "type";
	// properties
	private String name;
	private String value;
	private String type;

	/**
	 * constructor
	 */
	protected OSGiPropertyInfo() {
	}

	public OSGiPropertyInfo(String name, Object value) {
		this.name = name;
		if (value != null) {
			Class clazz = value.getClass();
			this.type = clazz.getName();
			if (clazz.isArray()) {
				this.value = Arrays.toString((Object[]) value);
			} else {
				this.value = value.toString();
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
