/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.server.jmx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiServiceInfo;

/**
 * Bean that represents the osgi service info and has a serialization/deserialization 
 * code to/from jmx open types that represent the data.
 * 
 * @author chikkala
 */
public class OSGiServiceInfoJMXSerializer {

    // types
    public static final String[] ITEM_NAMES = {
        OSGiServiceInfo.PROP_BUNDLE,
        OSGiServiceInfo.PROP_ID,
        OSGiServiceInfo.PROP_DESCRIPTION,
        OSGiServiceInfo.PROP_OBJECT_CLASSES,
        OSGiServiceInfo.PROP_USING_BUNDLES,
        OSGiServiceInfo.PROP_PROPERTIES
    };

    public OSGiServiceInfoJMXSerializer() {

    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiServiceInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemDescs = ITEM_NAMES;
        ArrayType objectClassesType = new ArrayType(1, SimpleType.STRING);
        ArrayType usingBundlesType = new ArrayType(1, SimpleType.STRING);
        TabularType propertiesType = OSGiPropertyInfoJMXSerializer.getTabularType();

        OpenType[] itemTypes = {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            objectClassesType,
            usingBundlesType,
            propertiesType
        };
        
        type = new CompositeType(typeName, typeDesc, ITEM_NAMES, itemDescs, itemTypes);
        return type;
    }

    public static CompositeData toCompositeData(OSGiServiceInfo info) throws OpenDataException {
        CompositeData data = null;
        CompositeType type = getCompositeType();
        TabularData propertiesData = OSGiPropertyInfoJMXSerializer.toTabularData(info.getProperties());
        Object[] itemValues = {
            info.getBundle(),
            info.getId(),
            info.getDescription(),
            info.getObjectClasses(),
            info.getUsingBundles(),
            propertiesData
        };
        data = new CompositeDataSupport(type, ITEM_NAMES, itemValues);
        return data;
    }

    public static OSGiServiceInfo toOSGiServiceInfo(CompositeData data) throws OpenDataException {
        OSGiServiceInfo info = new OSGiServiceInfo();
        info.setBundle((String) data.get(OSGiServiceInfo.PROP_BUNDLE));
        info.setId((String) data.get(OSGiServiceInfo.PROP_ID));
        info.setDescription((String) data.get(OSGiServiceInfo.PROP_DESCRIPTION));
        info.setObjectClasses((String[]) data.get(OSGiServiceInfo.PROP_OBJECT_CLASSES));
        info.setUsingBundles((String[]) data.get(OSGiServiceInfo.PROP_USING_BUNDLES));

        TabularData propData = (TabularData) data.get(OSGiServiceInfo.PROP_PROPERTIES);
        List<OSGiPropertyInfo> propList = new ArrayList<OSGiPropertyInfo>();
        if (propData != null) {
            propList = OSGiPropertyInfoJMXSerializer.toOSGiPropertyInfoList(propData);
        }
        info.setProperties(propList);

        return info;
    }

    public static List<OSGiServiceInfo> toOSGiServiceInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiServiceInfo> list = new ArrayList<OSGiServiceInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>)tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiServiceInfo info = OSGiServiceInfoJMXSerializer.toOSGiServiceInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiServiceInfo> list) throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiServiceInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiServiceInfoJMXSerializer.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiServiceInfoJMXSerializer.ITEM_NAMES);
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiServiceInfo info : list) {
            tdata.put(OSGiServiceInfoJMXSerializer.toCompositeData(info));
        }
        return tdata;
    }
}
