/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.DecoratedTabPanel;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * 
 * @author chikkala
 */
public class PropertySheet extends SimplePanel {
	private DecoratedTabPanel tabPanel;
	private ArrayList<PropertySheetSet> tabs;
	private ArrayList<String> tabIndex;

	public PropertySheet() {
		super();
		initComponents();
	}

	private void initComponents() {
		tabs = new ArrayList<PropertySheetSet>();
		tabIndex = new ArrayList<String>();
		tabPanel = new DecoratedTabPanel();
		tabPanel.setAnimationEnabled(true);
		this.setWidget(tabPanel);
	}

	public void addSheetSet(PropertySheetSet set) {
		tabPanel.add(set, set.getName());
		tabs.add(set);
		tabIndex.add(set.getId());

	}

	public void clearSheetSets() {
		tabPanel.clear();
		tabs.clear();
		tabIndex.clear();
	}

	public void selectSheetSet(String sheetSetId) {
		int i = tabIndex.indexOf(sheetSetId);
		tabPanel.selectTab(i);
	}
}
