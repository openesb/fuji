/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.ImageBundle;

/**
 *
 * @author chikkala
 */
public interface FujiAdminImages extends ImageBundle {

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/locale.png")
    AbstractImagePrototype locale();
    
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/loading.gif")
    AbstractImagePrototype loading();    

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/fuji-logo.png")
    AbstractImagePrototype fujiLogo();

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/fuji-server.png")
    AbstractImagePrototype instanceNodeIcon();

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/folder.png")
    AbstractImagePrototype folderNodeIcon();

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/ServiceEngine.png")
    AbstractImagePrototype serviceEngineNodeIcon();
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/BindingComponent.png")
    AbstractImagePrototype bindingComponentNodeIcon();
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/SharedLibrary.png")
    AbstractImagePrototype sharedLibraryNodeIcon();
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/ServiceAssembly.png")
    AbstractImagePrototype serviceAssemblyNodeIcon();
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/osgi-service.png")
    AbstractImagePrototype interceptorNodeIcon();    
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/osgi-bundle.png")
    AbstractImagePrototype osgiBundleNodeIcon();
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/osgi-service.png")
    AbstractImagePrototype osgiServiceNodeIcon();
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/osgi-config.png")
    AbstractImagePrototype osgiConfigNodeIcon();
    
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/se-started.png")
    AbstractImagePrototype engineStarted(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/se-stopped.png")
    AbstractImagePrototype engineStopped(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/se-shutdown.png")
    AbstractImagePrototype engineShutdown();

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/bc-started.png")
    AbstractImagePrototype bindingStarted(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/bc-stopped.png")
    AbstractImagePrototype bindingStopped(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/bc-shutdown.png")
    AbstractImagePrototype bindingShutdown();    
    
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/sl-started.png")
    AbstractImagePrototype slibStarted(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/sl-stopped.png")
    AbstractImagePrototype slibStopped(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/sl-shutdown.png")
    AbstractImagePrototype slibShutdown();  

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/sa-started.png")
    AbstractImagePrototype assemblyStarted(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/sa-stopped.png")
    AbstractImagePrototype assemblyStopped(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/sa-shutdown.png")
    AbstractImagePrototype assemblyShutdown();  

    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/bundle-active.png")
    AbstractImagePrototype bundleActive(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/bundle-stopped.png")
    AbstractImagePrototype bundleResolved(); 
    @Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/bundle-installed.png")
    AbstractImagePrototype bundleInstalled();      
}
