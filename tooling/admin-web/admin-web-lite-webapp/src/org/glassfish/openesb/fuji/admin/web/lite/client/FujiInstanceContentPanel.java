/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInstanceInfo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
/**
*
* @author chikkala
*/
public class FujiInstanceContentPanel extends ContentPanel {
	
	public FujiInstanceContentPanel() {
		super();
	}

	@Override
	protected DetailTabPanel createDetailTabPanel() {
		return new FujiInstanceDetailTabPanel();
	}

	@Override
	protected OverviewPanel createOverviewPanel() {
		return new FujiInstanceOverviewPanel();
	}
	
	protected static FujiInstanceInfo getFujiInstanceInfo(Object userObj) {
		FujiInstanceInfo info = null;
		if ( userObj != null && userObj instanceof FujiInstanceInfo) {
			info = (FujiInstanceInfo)userObj;
		}
		return info;
	}
	
	public static List<Property> createOverviewProperties(FujiInstanceInfo info) {
		List<Property> propList = new ArrayList<Property>();
//    	propList.add(new Property.SimpleProperty("AdminHost", info.getAdminHost(), "Host", "Admin Host"));
//    	propList.add(new Property.SimpleProperty("AdminPort", info.getAdminPort(), "Port", "Admin Port"));
//    	propList.add(new Property.SimpleProperty("AdminUser", info.getAdminUser(), "User", "Admin User"));    	
    	return propList;
	}
	
	public static class FujiInstanceOverviewPanel extends OverviewPanel {
		public FujiInstanceOverviewPanel() {
			super();
		}
		
		private void refreshInstance() {
			if ( FujiAdmin.getFujiAdmin() != null ) {
				FujiAdmin.getFujiAdmin().loadFujiInstance(null);
			}
		}
		
		@Override
		protected Button[] createActions() {
	        Button refreshButton = new Button("Refresh", new ClickHandler() {
	            public void onClick(ClickEvent event) {
	              // Window.alert("Start Bundle");
	              refreshInstance();
	            }
	          });
	        Button[] actions = new Button[1];
	        actions[0] = refreshButton;
	        return actions;
		}
		
		@Override
		public void updateContent(Object userObj) {
			
			FujiInstanceInfo info = FujiInstanceContentPanel.getFujiInstanceInfo(userObj);
	    	List<Property> propList = new ArrayList<Property>();
	    	if ( info != null) {
	    		propList = createOverviewProperties(info);
	    	}
	    	setProperties(propList);
		}		
	}
	
	public static class FujiInstanceDetailTabPanel extends DetailTabPanel {

		public FujiInstanceDetailTabPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			FujiInstanceInfo info = FujiInstanceContentPanel.getFujiInstanceInfo(userObj);
			this.clearTabs();
			if ( info == null ) {
				return;
			}	    	
			
			TabContentPanel tab1 = new TabContentPanel(new InstallBundlePanel(), "InstallBundle", "Install Bundles", "Install Bundles");
			TabContentPanel tab2 = new TabContentPanel(new SimplePanel(), "LoggerSettings", "Set Loggers", "Logger Settings");
			TabContentPanel tab3 = new TabContentPanel(new SimplePanel(), "ServerLog", "View Server Log", "View Server Log");
			
	    	this.addTab(tab1);
	    	this.addTab(tab2);
	    	this.addTab(tab3);
	    	
	    	this.selectTab(tab1.getTabId());			
		}
		
	}
	
	public static class InstallBundlePanel extends FormPanel {
		public InstallBundlePanel() {
			super();
			initForm();
			initComponents();
			initFormHandlers();
		}
		private void initComponents() {
			
			VerticalPanel layout = new VerticalPanel();
			this.add(layout);
			layout.add(new HTML("Select Bundle file"));
			final FileUpload bundleFileUpload = new FileUpload();
			bundleFileUpload.setName("bundleFile");
			layout.add(bundleFileUpload);
			Button installButton = new Button("Install");
			installButton.addClickHandler(new ClickHandler() {
		        public void onClick(ClickEvent event) {
		          String filename = bundleFileUpload.getFilename();
		          if (filename.length() == 0) {
		            Window.alert("Please select the bundle file");
		          } else {
//		        	  Window.alert("Installing: " + filename);
		              installBundle(filename);
		          }
		        }
		      });
			layout.add(new HTML("<br>"));
			layout.add(installButton);	
			
		}
		
		private void initForm() {
		    // point the FormPanel at a service.
		    String installServiceURL = GWT.getModuleBaseURL() + "install-bundle-service";
		    // this.setAction("/fuji/admin/lite/install-bundle-service");
		    this.setAction(installServiceURL);

		    // Because we're going to add a FileUpload widget, we'll need to set the
		    // form to use the POST method, and multipart MIME encoding.
		    this.setEncoding(FormPanel.ENCODING_MULTIPART);
		    this.setMethod(FormPanel.METHOD_POST);			
		}
		
		private void initFormHandlers() {
		    // Add an event handler to the form.
		    this.addSubmitHandler(new FormPanel.SubmitHandler() {
		      public void onSubmit(SubmitEvent event) {
		        // This event is fired just before the form is submitted. We can take
		        // this opportunity to perform validation.
//		          String filename = bundleFileUpload.getFilename();
//		          if (filename.length() == 0) {
//		            Window.alert("Please select the bundle file");
//		            event.cancel();
//		          }		    	  
		      }
		    });
		    
		    this.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
		      public void onSubmitComplete(SubmitCompleteEvent event) {
		        // When the form submission is successfully completed, this event is
		        // fired. Assuming the service returned a response of type text/html,
		        // we can get the result text here (see the FormPanel documentation for
		        // further explanation).
		        // Window.alert(event.getResults());
		        selectBundle(event.getResults());
		      }
		    });

			
		}
		
		private void installBundle(String bundlePath) {
//			AsyncCallback<String> callback = new AsyncCallback<String>() {
//
//				public void onFailure(Throwable caught) {
//					// do something with errors
//					Window.alert("Error installing" + caught.getMessage());
//				}
//
//				public void onSuccess(String result) {
//					// update the watch list FlexTable
//					Window.alert("install Bundle Result:" + result);
//				}
//			};		
//			FujiAdmin.getFujiAdminService().installBundle(bundlePath, callback);	
			this.submit();
			
		}
		protected void selectBundle(String result) {
			if ( FujiAdmin.getFujiAdmin() != null ) {
//				Long id = Long.valueOf(result);
//				String bundleId = id.toString();
//				String tkn = "OSGiBundleInfo_" + bundleId;
				String bundleId = "";
				if ( result != null ) {
					bundleId = result.trim();
				}
				String tkn = "OSGiBundleInfo_" + bundleId;
				// Window.alert("Selecting Bundle: " + tkn);
				FujiAdmin.getFujiAdmin().loadFujiInstance(tkn);
			} else {
				Window.alert("Fuji Admin reference is NULL");
			}		
		}		
	}
}
