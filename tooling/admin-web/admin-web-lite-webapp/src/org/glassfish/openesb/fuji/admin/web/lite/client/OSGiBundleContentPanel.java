/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiBundleInfo;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
/**
*
* @author chikkala
*/
public class OSGiBundleContentPanel extends ContentPanel {
	
	public OSGiBundleContentPanel() {
		super();
	}

	@Override
	protected DetailTabPanel createDetailTabPanel() {
		return new OSGiBundleDetailTabPanel();
	}

	@Override
	protected OverviewPanel createOverviewPanel() {
		return new OSGiBundleOverviewPanel();
	}
	
	protected static OSGiBundleInfo getOSGiBundleInfo(Object userObj) {
		OSGiBundleInfo info = null;
		if ( userObj != null && userObj instanceof OSGiBundleInfo) {
			info = (OSGiBundleInfo)userObj;
		}
		return info;
	}
	private static String getTimeFormat(String time) {
		String timeFmt = time;
		try {
			Date t = new Date(Long.parseLong(time));
			timeFmt = DateTimeFormat.getMediumDateTimeFormat().format(t);
		} catch (Exception ex) {
			//ignore.
		}
		return timeFmt;
	}
	public static List<Property> createOverviewProperties(OSGiBundleInfo info) {
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("SymbolicName", info.getSymbolicName(), "Symbolic Name", "Symbolic Name"));
    	propList.add(new Property.SimpleProperty("BundleId", info.getBundleId(), "Bundle Id", "Bundle Id"));
    	propList.add(new Property.SimpleProperty("State", info.getState(), "State", "State"));
    	propList.add(new Property.SimpleProperty("Location", info.getLocation(), "Location", "Location"));
    	propList.add(new Property.SimpleProperty("LastModified", getTimeFormat(info.getLastModified()), "Last Modified", "Last Modified"));
    	return propList;
	}	
	
    public static PropertySheetSet createOSGiBundleHeaderSet(OSGiBundleInfo info) {
    	PropertySheetSet sheetSet = new PropertySheetSet("headers", "OSGi Bundle Headers", "Header Decription");
    	sheetSet.addOSGiProperties(info.getHeaders());
    	return sheetSet;
    }
    
    public static PropertySheetSet createOSGiBundleInfoSet(OSGiBundleInfo info) {
    	PropertySheetSet sheetSet = new PropertySheetSet("osgiBundleOverview", "OSGi Bundle Details", "OSGi Bundle Details");
    	List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("SymbolicName", info.getSymbolicName(), "Symbolic Name", "Symbolic Name"));
    	propList.add(new Property.SimpleProperty("BundleId", info.getBundleId(), "Bundle Id", "Bundle Id"));
    	propList.add(new Property.SimpleProperty("State", info.getState(), "State", "State"));
    	propList.add(new Property.SimpleProperty("Location", info.getLocation(), "Location", "Location"));
    	propList.add(new Property.SimpleProperty("LastModified", getTimeFormat(info.getLastModified()), "Last Modified", "Last Modified"));
    	sheetSet.addProperties(propList);
    	return sheetSet;
    }	
    
	public static class OSGiBundleOverviewPanel extends OverviewPanel {
		
		private OSGiBundleLifecycleActions actions;
		public OSGiBundleOverviewPanel() {
			super();
		}
		private OSGiBundleLifecycleActions getLifecycleActions() {
			if ( this.actions == null ) {
				this.actions = new OSGiBundleLifecycleActions();
			}
			return this.actions;
		}
		@Override
		protected Button[] createActions() {
			
			return getLifecycleActions().createActions();
		}

		@Override
		public void updateContent(Object userObj) {
			
			OSGiBundleInfo info = getOSGiBundleInfo(userObj);
			getLifecycleActions().setOSGiBundleInfo(info);
	    	List<Property> propList = new ArrayList<Property>();
	    	if ( info != null ) {
	    		propList = OSGiBundleContentPanel.createOverviewProperties(info);
	    	}
	    	setProperties(propList);
		}		
	}
	
	public static class OSGiBundleDetailTabPanel extends DetailTabPanel {

		public OSGiBundleDetailTabPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			OSGiBundleInfo info = getOSGiBundleInfo(userObj);
			this.clearTabs();
			if ( info == null ) {
				return;
			}	    	
	    	PropertySheetSet set1 = OSGiBundleContentPanel.createOSGiBundleInfoSet(info);
	    	PropertySheetSet set2 = OSGiBundleContentPanel.createOSGiBundleHeaderSet(info);	    	
	    	this.addTab(new TabContentPanel(set1));
	    	this.addTab(new TabContentPanel(set2));
	    	this.selectTab(set1.getId());			
		}
		
	}

}
