/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author chikkala
 */
public class FujiInterceptorInfo implements IsSerializable {

	public static final String INTERCEPTOR_CLASS = "com.sun.jbi.interceptors.Interceptor";
	public static final String INTERCEPTOR_NAME_PROP = "name";
	private OSGiServiceInfo serviceInfo;
	private OSGiConfigInfo configInfo;

	public FujiInterceptorInfo() {
	}

	public FujiInterceptorInfo(OSGiServiceInfo serviceInfo,
			OSGiConfigInfo configInfo) {
		this.serviceInfo = serviceInfo;
		this.configInfo = configInfo;
	}

	public OSGiConfigInfo getConfigInfo() {
		return configInfo;
	}

	public void setConfigInfo(OSGiConfigInfo configInfo) {
		this.configInfo = configInfo;
	}

	public OSGiServiceInfo getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(OSGiServiceInfo serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public String getDisplayName() {
		String name = this.serviceInfo.getPropertyValue(INTERCEPTOR_NAME_PROP);
		if (name == null) {
			String[] objClasses = this.serviceInfo.getObjectClasses();
			if (objClasses != null && objClasses.length > 0) {
				name = objClasses[0];
			} else {
				name = this.serviceInfo.getId();
			}
		}
		return name;
	}

	public static List<FujiInterceptorInfo> createFujiInterceptorInfoList(
			List<OSGiServiceInfo> serviceList, List<OSGiConfigInfo> configList) {
		List<FujiInterceptorInfo> list = new ArrayList<FujiInterceptorInfo>();
		Map<String, OSGiConfigInfo> configMap = new HashMap<String, OSGiConfigInfo>();
		for (OSGiConfigInfo cfgInfo : configList) {
			String name = cfgInfo.getPropertyValue(INTERCEPTOR_NAME_PROP);
			if (name != null) {
				configMap.put(name, cfgInfo);
			}
		}
		for (OSGiServiceInfo srvInfo : serviceList) {
			String[] objClasses = srvInfo.getObjectClasses();
			for (String objClass : objClasses) {
				if (INTERCEPTOR_CLASS.equals(objClass)) {
					String name = srvInfo
							.getPropertyValue(INTERCEPTOR_NAME_PROP);
					OSGiConfigInfo cfgInfo = configMap.get(name);
					list.add(new FujiInterceptorInfo(srvInfo, cfgInfo));
					break;
				}
			}
		}
		return list;
	}
}
