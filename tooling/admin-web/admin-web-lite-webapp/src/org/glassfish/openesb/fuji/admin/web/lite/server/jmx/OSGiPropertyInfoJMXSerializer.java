/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.server.jmx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;

/**
 *
 * @author chikkala
 */
public class OSGiPropertyInfoJMXSerializer {

    // jmx tabular data names
    public static final String[] ITEM_NAMES = {
        OSGiPropertyInfo.PROP_NAME,
        OSGiPropertyInfo.PROP_VALUE,
        OSGiPropertyInfo.PROP_TYPE
    };
    // jmx tabular data types
    public static final OpenType[] ITEM_TYPES = {
        SimpleType.STRING,
        SimpleType.STRING,
        SimpleType.STRING
    };

    /**
     * constructor
     */
    protected OSGiPropertyInfoJMXSerializer() {
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiPropertyInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemNames = ITEM_NAMES;
        String[] itemDescs = ITEM_NAMES;
        OpenType[] itemTypes = ITEM_TYPES;
        type = new CompositeType(typeName, typeDesc, itemNames, itemDescs, itemTypes);
        return type;
    }

    public static TabularType getTabularType() throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiPropertyInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiPropertyInfoJMXSerializer.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiPropertyInfoJMXSerializer.ITEM_NAMES);
        return ttype;
    }

    public static CompositeData toCompositeData(OSGiPropertyInfo info) throws OpenDataException {
        CompositeData data = null;
        CompositeType compositeType = getCompositeType();
        String[] itemNames = ITEM_NAMES;
        Object[] itemValues = {
            info.getName(),
            info.getValue(),
            info.getType()
        };
        data = new CompositeDataSupport(compositeType, itemNames, itemValues);
        return data;
    }

    public static OSGiPropertyInfo toOSGiPropertyInfo(CompositeData data) throws OpenDataException {
        
//        if (!data.getCompositeType().equals(getCompositeType())) {
//            throw new OpenDataException("Can not convert ");
//        }
        OSGiPropertyInfo info = new OSGiPropertyInfo((String) data.get(OSGiPropertyInfo.PROP_NAME),
        (String) data.get(OSGiPropertyInfo.PROP_VALUE));
        info.setType((String) data.get(OSGiPropertyInfo.PROP_TYPE));
        return info;
    }

    public static List<OSGiPropertyInfo> toOSGiPropertyInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiPropertyInfo> list = new ArrayList<OSGiPropertyInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>) tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiPropertyInfo info = OSGiPropertyInfoJMXSerializer.toOSGiPropertyInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiPropertyInfo> list) throws OpenDataException {
        TabularType ttype = getTabularType();
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiPropertyInfo info : list) {
            tdata.put(OSGiPropertyInfoJMXSerializer.toCompositeData(info));
        }
        return tdata;
    }
}
