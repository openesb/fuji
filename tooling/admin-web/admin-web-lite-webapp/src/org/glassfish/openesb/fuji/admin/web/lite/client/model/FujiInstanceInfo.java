/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author chikkala
 */
public class FujiInstanceInfo implements IsSerializable {

    private String mAdminHost = "localhost";
    private String mAdminPort = "8699";
    private String mAdminUser = "";
    private String mAdminPassword = "";

    private List<OSGiBundleInfo> mBundleList;
    private List<OSGiServiceInfo> mServiceList;
    private List<OSGiConfigInfo> mConfigList;

    private List<OSGiBundleInfo> mSEList;
    private List<OSGiBundleInfo> mBCList;
    private List<OSGiBundleInfo> mSLibList;
    private List<OSGiBundleInfo> mSAList;

    private List<FujiInterceptorInfo> mInterceptorList;

    public FujiInstanceInfo() {
        this.mBundleList = new ArrayList<OSGiBundleInfo>();
        this.mServiceList = new ArrayList<OSGiServiceInfo>();
        this.mConfigList = new ArrayList<OSGiConfigInfo>();

        this.mSEList = new ArrayList<OSGiBundleInfo>();
        this.mBCList = new ArrayList<OSGiBundleInfo>();
        this.mSLibList = new ArrayList<OSGiBundleInfo>();
        this.mSAList = new ArrayList<OSGiBundleInfo>();

        this.mInterceptorList = new ArrayList<FujiInterceptorInfo>();

    }

    public String getName() {
        if (mAdminHost == null || mAdminHost.trim().length() == 0 ) {
            return "Fuji Instnace";
        } else {
            return "Fuji Instance[" + this.mAdminHost + ":" + this.mAdminPort + "]";
        }
    }

    public String getAdminHost() {
        return mAdminHost;
    }

    public void setAdminHost(String adminHost) {
        this.mAdminHost = adminHost;
    }

    /**
     * Get the value of adminPort
     * 
     * @return the value of adminPort
     */
    public String getAdminPort() {
        return mAdminPort;
    }

    /**
     * Set the value of adminPort
     * 
     * @param adminPort new value of adminPort
     */
    public void setAdminPort(String adminPort) {
        this.mAdminPort = adminPort;
    }

    public String getAdminPassword() {
        return mAdminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.mAdminPassword = adminPassword;
    }

    public String getAdminUser() {
        return mAdminUser;
    }

    public void setAdminUser(String adminUser) {
        this.mAdminUser = adminUser;
    }

    public List<OSGiBundleInfo> getBundleList() {
        return mBundleList;
    }

    public void setBundleList(List<OSGiBundleInfo> bundleList) {
        this.mBundleList.clear();
        this.mBundleList.addAll(bundleList);
    }

    public List<OSGiConfigInfo> getConfigList() {
        return mConfigList;
    }

    public void setConfigList(List<OSGiConfigInfo> configList) {
        this.mConfigList.clear();
        this.mConfigList.addAll(configList);
    }

    public List<OSGiServiceInfo> getServiceList() {
        return mServiceList;
    }

    public void setServiceList(List<OSGiServiceInfo> serviceList) {
        this.mServiceList.clear();
        this.mServiceList.addAll(serviceList);
    }

    public List<OSGiBundleInfo> getEngineList() {
        return mSEList;
    }

    public void setEngineList(List<OSGiBundleInfo> bundleList) {
        this.mSEList.clear();
        this.mSEList.addAll(bundleList);
    }

    public List<OSGiBundleInfo> getBindingList() {
        return mBCList;
    }

    public void setBindingList(List<OSGiBundleInfo> bundleList) {
        this.mBCList.clear();
        this.mBCList.addAll(bundleList);
    }

    public List<OSGiBundleInfo> getSLibList() {
        return mSLibList;
    }

    public void setSLibList(List<OSGiBundleInfo> bundleList) {
        this.mSLibList.clear();
        this.mSLibList.addAll(bundleList);
    }

    public List<OSGiBundleInfo> getAssemblyList() {
        return mSAList;
    }

    public void setAssemblyList(List<OSGiBundleInfo> bundleList) {
        this.mSAList.clear();
        this.mSAList.addAll(bundleList);
    }

    public List<FujiInterceptorInfo> getInterceptorList() {
        return mInterceptorList;
    }

    public void setInterceptorList(List<FujiInterceptorInfo> interceptorList) {
        this.mInterceptorList.clear();
        this.mInterceptorList.addAll(interceptorList);
    }

    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("<b>" + getName() + "</b><br>");
        buf.append("<b><i>&nbsp;&nbsp;- " + "OSGi Bundles" + "</i></b><br>");
        int i = 0;
        for (OSGiBundleInfo info : this.mBundleList) {

            buf.append("&nbsp;&nbsp;&nbsp;- " + info.getSymbolicName() + "<br>");
            if (++i > 1000) {
                buf.append("&nbsp;&nbsp;&nbsp;- " + "..." + "<br>");
                break;
            }
        }
        buf.append("<b><i>&nbsp;&nbsp;-" + "OSGi Services" + "</i></b><br>");
        buf.append("&nbsp;&nbsp;&nbsp;- " + "..." + "<br>");

        buf.append("<b><i>&nbsp;&nbsp;-" + "OSGi Configurations"
                + "</i></b><br>");
        buf.append("&nbsp;&nbsp;&nbsp;- " + "..." + "<br>");

        return buf.toString();
    }
}
