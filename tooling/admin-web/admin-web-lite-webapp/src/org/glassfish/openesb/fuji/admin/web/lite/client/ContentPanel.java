/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.user.client.ui.VerticalPanel;
/**
*
* @author chikkala
*/
public class ContentPanel extends VerticalPanel {
	
	/**
	 * The base style name.
	 */
	public static final String DEFAULT_STYLE_NAME = "Application";
	
	private Object userObject;
	private OverviewPanel overviewPanel;
	private DetailTabPanel detailPanel;
	
	public ContentPanel() {
		super();
		initComponents();
	}
	private void initComponents() {
//		Grid layout = new Grid(2, 1);
//		layout.setCellPadding(0);
//		layout.setCellSpacing(0);
//		// layout.setSize("100%", "600px");
//		this.setWidget(layout);
//		
//		CellFormatter formatter = layout.getCellFormatter();
//		formatter.setStyleName(0, 0, FujiAdminFrame.DEFAULT_STYLE_NAME + "-content-wrapper");
//		formatter.setStyleName(1, 0, FujiAdminFrame.DEFAULT_STYLE_NAME + "-content-wrapper");
//		
//		layout.getRowFormatter().setVerticalAlign(0,HasVerticalAlignment.ALIGN_TOP);
//		
//		initOverviewPanel();
//		layout.setWidget(0, 0, overviewPanel);
//		
//		formatter.setHeight(1,0, "100%");
//		detailPanel.setWidget(new Label("Details"));
//		layout.setWidget(1, 0, detailPanel);		
//		
//		initOverviewPanel();
//		initDetailPanel();
		
//		DockPanel layout = new DockPanel();
//		this.setWidget(layout);
		// layout.setStyleName(FujiAdminFrame.DEFAULT_STYLE_NAME + "-content-wrapper");
		// layout.setVerticalAlignment(HasVerticalAlignment.ALIGN_TOP);
//		layout.add(overviewPanel, DockPanel.NORTH);
//		layout.add(detailPanel, DockPanel.CENTER);
//		DecoratorPanel overviewDecorator = new DecoratorPanel();
//		overviewDecorator.setWidget(overviewPanel);
//		overviewDecorator.setWidth("100%");
		
		this.overviewPanel = createOverviewPanel();
		this.detailPanel = createDetailTabPanel();
		if ( this.overviewPanel != null ) {
//			this.add(overviewPanel, DockPanel.NORTH);
			this.add(overviewPanel);
		}
		if ( this.detailPanel != null ) {
//			this.add(detailPanel, DockPanel.CENTER);
			this.add(detailPanel);
			this.setCellHeight(this.detailPanel, "100%");
			this.setCellWidth(this.detailPanel, "100%");
		}
		this.setStyleName(DEFAULT_STYLE_NAME + "-content-wrapper");
		
		// this.setHeight("600px");
		
	}
	
    public Object getUserObject() {
        return userObject;
    }

    public void setUserObject(Object userObject) {
        this.userObject = userObject;
        updateContent();
    }	

    protected OverviewPanel createOverviewPanel() {
    	// return new OverviewPanel();
    	return null;
    }
    
    protected DetailTabPanel createDetailTabPanel() {
    	// return new DetailTabPanel();
    	return null;
    }
    
    private void updateContent() {
    	Object userObj = this.getUserObject();
    	if ( this.overviewPanel != null ) {
    		this.overviewPanel.updateContent(userObj);
    	}
    	if ( this.detailPanel != null ) {
    		this.detailPanel.updateContent(userObj);    	
    	}
    }
    
    public void adjustSize() {
//    	if ( this.detailPanel != null ) {
//    		this.detailPanel.adjustSize();
//    	}
    }
 
}
