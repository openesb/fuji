/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
/**
*
* @author chikkala
*/
public class OverviewPanel  extends Composite {
	/**
	 * The base style name.
	 */
	public static final String DEFAULT_STYLE_NAME = "Application";
    // private DecoratorPanel overviewDecorator = new DecoratorPanel();
	private VerticalPanel layout = new VerticalPanel();
	private HorizontalPanel actionPanel = new HorizontalPanel();
	private Grid overviewGrid = new Grid(1,1);
	private List<Property> propList;
	
	public OverviewPanel() {
		
        actionPanel.setSpacing(10);
        initActions();
        
        overviewGrid.setCellSpacing(4);
        propList = new ArrayList<Property>();
        setProperties(propList);
        
		layout.add(actionPanel);
		layout.add(overviewGrid);		
		// overviewDecorator.setWidget(layout);
		
		// All composites must call initWidget() in their constructors.
		initWidget(layout);
        // initWidget(overviewDecorator);        
        // Give the overall composite a style name.
        setStyleName(DEFAULT_STYLE_NAME);		
	}
	
	private void initActions() {
		actionPanel.clear();
		Button[] actions = createActions();
		if ( actions == null) {
			return;
		}
		for ( Button action : actions) {
			actionPanel.add(action);
		}
	}
	
	protected Button[] createActions() {
 		return new Button[0];
	}
	
	protected void setProperties(List<Property> propList) {
		this.propList.clear();
		this.propList.addAll(propList);
		updateOverviewGrid();
	}
	
	private void updateOverviewGrid() {

        if (this.propList == null) {
        	overviewGrid.clear();
        	overviewGrid.resize(1, 1);
        	overviewGrid.setText(0, 0, "");
        } else {
        	overviewGrid.clear();
        	overviewGrid.resize(propList.size(), 2);
            for (int i = 0; i < propList.size(); ++i) {
                Property info = propList.get(i);
                overviewGrid.setWidget(i, 0, new Label(info.getDisplayName() + ":"));
                overviewGrid.setWidget(i, 1, new MyLabel(info.getValue(), 40));
            }
        }		
	}
	protected void clearContent() {
		setProperties(new ArrayList<Property>());
	}
	
	public void updateContent(Object userObj) {
		clearContent();    	
	}
}
