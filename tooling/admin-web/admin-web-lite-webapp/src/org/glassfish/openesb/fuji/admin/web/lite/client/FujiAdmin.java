/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInstanceInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInterceptorInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiBundleInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiConfigInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiServiceInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.rpc.FujiAdminService;
import org.glassfish.openesb.fuji.admin.web.lite.client.rpc.FujiAdminServiceAsync;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.Node;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 * @author chikkala
 */
public class FujiAdmin implements EntryPoint {

	/**
	 * A special version of the ToggleButton that cannot be clicked if down. If
	 * one theme button is pressed, all of the others are depressed.
	 */
	private static class ThemeButton extends ToggleButton {
		private static List<ThemeButton> allButtons = null;

		private String theme;

		public ThemeButton(String theme) {
			super();
			this.theme = theme;
			addStyleName("sc-ThemeButton-" + theme);

			// Add this button to the static list
			if (allButtons == null) {
				allButtons = new ArrayList<ThemeButton>();
				setDown(true);
			}
			allButtons.add(this);
		}

		public String getTheme() {
			return theme;
		}

		@Override
		protected void onClick() {
			if (!isDown()) {
				// Raise all of the other buttons
				for (ThemeButton button : allButtons) {
					if (button != this) {
						button.setDown(false);
					}
				}

				// Fire the click handlers
				super.onClick();
			}
		}
	}

	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	/**
	 * The static images used throughout the FujiAdmin.
	 */
	public static final FujiAdminImages images = (FujiAdminImages) GWT
			.create(FujiAdminImages.class);
	/**
	 * The current style theme.
	 */
	public static String CUR_THEME = FujiAdminConstants.STYLE_THEMES[0];

	/**
	 * Get the URL of the page, without an hash of query string.
	 * 
	 * @return the location of the page
	 */
	private static native String getHostPageLocation()
	/*-{
		var s = $doc.location.href;

		// Pull off any hash.
		var i = s.indexOf('#');
		if (i != -1)
		  s = s.substring(0, i);

		// Pull off any query string.
		i = s.indexOf('?');
		if (i != -1)
		  s = s.substring(0, i);

		// Ensure a final slash if non-empty.
		return s;
	}-*/;

	/**
	 * The {@link FujiAdminFrame}. UI Main Frame
	 */
//	private FujiAdminFrame app = new FujiAdminFrame();
	 private FujiAdminPane app = new FujiAdminPane();

	/**
	 * Create a remote service proxy to talk to the server-side fuji admin
	 * service.
	 */
	private static FujiAdminServiceAsync fujiAdminService;
	private static FujiAdmin admin;
	
	private String loadingImage;
	
	private TreeItem instanceNode;
	private TreeItem bundleFolder;
	private TreeItem serviceFolder;
	private TreeItem configFolder;
	
	private TreeItem engineFolder;
	private TreeItem bindingFolder;
	private TreeItem slibFolder;
	private TreeItem assemblyFolder;
	private TreeItem interceptorFolder;
	
	private ContentPanel defaultContent;
	private OSGiBundleContentPanel osgiBundleContent;
	private OSGiServiceContentPanel osgiServiceContent;
	private OSGiConfigContentPanel osgiConfigContent;
	private JBIBundleContentPanel jbiBundleContent;
	private FujiInterceptorContentPanel fujiInterceptorContent;
	
	private FujiInstanceContentPanel fujiInstanceContent;
	
	/**
	 * A mapping of history tokens to their associated explorer nodes.
	 */
	private Map<String, TreeItem> itemTokens = new HashMap<String, TreeItem>();

	/**
	 * A mapping of menu items to the widget display when the item is selected.
	 */
	private Map<TreeItem, ContentPanel> itemWidgets = new HashMap<TreeItem, ContentPanel>();
	    
	private FujiAdminConstants constants;    
	    
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		// Create the constants
		constants = (FujiAdminConstants) GWT
				.create(FujiAdminConstants.class);
		admin = this;

		updateStyleSheets();

		// Create the application
		setupTitlePane(constants);
		setupMainLinks(constants);
		setupOptionsPanel();
		setupExplorerView(constants);
		// setupDetailView(constants);
		
	    // Setup a history handler to reselect the associate menu item
	    final ValueChangeHandler<String> historyHandler = new ValueChangeHandler<String>() {
	      public void onValueChange(ValueChangeEvent<String> event) {
	    	TreeItem item = null;
	    	item = itemTokens.get(event.getValue());
	        if (item == null) {
	          try {
	        	  item = app.getExplorer().getItem(0).getChild(0);
	          } catch (Exception ex) {
	        	  //ignore. log exception for debug.
	          }
	        }
	        // Select the associated TreeItem
	        app.getExplorer().setSelectedItem(item, false);
	        app.getExplorer().ensureSelectedItemVisible();
	        // Show the associated ContentWidget
	        displayContentWidget(item);
	      }
	    };
	    History.addValueChangeHandler(historyHandler);

	    // Add a handler that sets the content widget when a menu item is selected
	    app.addSelectionHandler(new SelectionHandler<TreeItem>() {
	      public void onSelection(SelectionEvent<TreeItem> event) {
	        TreeItem item = event.getSelectedItem();
	        updateHistory(item);
	        displayContentWidget(item);
	      }
	    });

	    // Show the initial content
	    if (History.getToken().length() > 0) {
	      History.fireCurrentHistoryState();
	    } else {
	      // Use the first token available
	      TreeItem firstItem = null;
	      try {
	      firstItem = app.getExplorer().getItem(0).getChild(0);
	      app.getExplorer().setSelectedItem(firstItem, false);
	      app.getExplorer().ensureSelectedItemVisible();
	      } catch (Exception ex) {
	    	  // ignore.
	      }
	      displayContentWidget(firstItem);
	    }		

	}
	
	public static FujiAdmin getFujiAdmin() {
		return admin;
	}
	
	public static FujiAdminServiceAsync getFujiAdminService() {
		if (fujiAdminService == null) {
			fujiAdminService = GWT.create(FujiAdminService.class);
		}
		return fujiAdminService;
	}
	
	 private String getContentTitle(TreeItem node) {
		 Object userObj = node.getUserObject();
		 if ( userObj == null ) {
			 return node.getText();
		 }
		 if ( userObj instanceof FujiInstanceInfo ) {
			 return constants.fujiInstanceName();
		 }
		 if ( userObj instanceof OSGiBundleInfo) {
			 OSGiBundleInfo info = (OSGiBundleInfo)userObj;
			 String type = info.getJbiArtifactType();
			 if ( OSGiBundleInfo.JBI_BC.equals(type)) {
				 return constants.bindingTitle();
			 } else if ( OSGiBundleInfo.JBI_SE.equals(type)) {
				 return constants.engineTitle();
			 } else if ( OSGiBundleInfo.JBI_SL.equals(type)) {
				 return constants.slibTitle();
			 } else if ( OSGiBundleInfo.JBI_SA.equals(type)) {
				 return constants.assemblyTitle();
			 } else {
				 return constants.osgiBundleTitle();
			 }
		 } else if (userObj instanceof OSGiServiceInfo) {
			 return constants.osgiServiceTitle();
		 } else if ( userObj instanceof OSGiConfigInfo) {
			 return constants.osgiConfigTitle();
		 } else if ( userObj instanceof FujiInterceptorInfo) {
			 return constants.interceptorTitle();
		 }
		 return node.getText();
	 }
	
	  /**
	   * Set the content to the {@link ContentWidget}.
	   * 
	   * @param content the {@link ContentWidget} to display
	   */
	  private void displayContentWidget(TreeItem node) {
		ContentPanel content = itemWidgets.get(node);
	    if (content != null) {
	      content.setUserObject(node.getUserObject());
	      app.setContent(content);
	      app.setContentTitle(getContentTitle(node));
	    }
	  }
	  
	  private void updateHistory(TreeItem node) {
//	        ContentPanel content = itemWidgets.get(node);
//	        if (content != null && !content.equals(app.getContent())) {
//	          History.newItem(getContentWidgetToken(node));
//	        }		
		  if (!itemTokens.containsValue(node)) {
			  History.newItem(getContentWidgetToken(node));
		  }
	  }
	  /**
	   * Get the token for a given content widget.
	   * 
	   * @return the content widget token.
	   */	  
	  private String getContentWidgetToken(Object obj) {
		  String tkn = null;
		  Object userObj = null;

		  if (obj != null && obj instanceof TreeItem ) {
			  userObj = ((TreeItem)obj).getUserObject();
			  if (userObj == null) {
				  userObj = obj;
			  }
		  }
		  if (userObj == null ) {
			  return null;
		  }		  
		  if (userObj instanceof OSGiBundleInfo) {
			  tkn = "OSGiBundleInfo_" + ((OSGiBundleInfo)userObj).getBundleId();
		  } else if (userObj instanceof FujiInterceptorInfo) {
			  tkn = "FujiInterceptorInfo_" + ((FujiInterceptorInfo)userObj).getDisplayName();
		  } else if (userObj instanceof OSGiServiceInfo) {
			  tkn = "OSGiServiceInfo_" + ((OSGiServiceInfo)userObj).getId();
		  } else if (userObj instanceof OSGiConfigInfo) {
			  tkn = "OSGiConfigInfo_" + ((OSGiConfigInfo)userObj).getPid();
		  } else {
			  tkn = Integer.toString(userObj.hashCode());
		  }		  
		  return tkn;
	  }
	  
	private void setupTitlePane(FujiAdminConstants constants) {
		// Get the title from the internationalized constants
		String pageTitle = "<h1>" + constants.mainTitle() + "</h1><h2>"
				+ constants.mainSubTitle() + "</h2>";

		// Add the title and some images to the title bar
		HorizontalPanel titlePanel = new HorizontalPanel();
		titlePanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		titlePanel.add(images.fujiLogo().createImage());
		titlePanel.add(new HTML(pageTitle));
//		app.setTitleWidget(titlePanel);
		app.getTitlePane().setTitleWidget(titlePanel);
	}

	private void setupMainLinks(FujiAdminConstants constants) {
		// Link to Fuji Homepage
		app.addLink(new HTML("<a href=\"" + FujiAdminConstants.FUJI_HOMEPAGE
				+ "\">" + constants.mainLinkHomepage() + "</a>"));

		// Link to Fuji Wiki
		app.addLink(new HTML("<a href=\"" + FujiAdminConstants.FUJI_WIKI
				+ "\">" + constants.mainLinkWiki() + "</a>"));
	}

	/**
	 * Create the options that appear next to the title.
	 */
	private void setupOptionsPanel() {
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		if (LocaleInfo.getCurrentLocale().isRTL()) {
			vPanel.getElement().setAttribute("align", "left");
		} else {
			vPanel.getElement().setAttribute("align", "right");
		}
		app.setOptionsWidget(vPanel);

		// Add the option to change the locale
//		final ListBox localeBox = new ListBox();
//		String currentLocale = LocaleInfo.getCurrentLocale().getLocaleName();
//		if (currentLocale.equals("default")) {
//			currentLocale = "en";
//		}
//		String[] localeNames = LocaleInfo.getAvailableLocaleNames();
//		for (String localeName : localeNames) {
//			if (!localeName.equals("default")) {
//				String nativeName = LocaleInfo
//						.getLocaleNativeDisplayName(localeName);
//				localeBox.addItem(nativeName, localeName);
//				if (localeName.equals(currentLocale)) {
//					localeBox.setSelectedIndex(localeBox.getItemCount() - 1);
//				}
//			}
//		}
//		localeBox.addChangeHandler(new ChangeHandler() {
//			public void onChange(ChangeEvent event) {
//				String localeName = localeBox.getValue(localeBox
//						.getSelectedIndex());
//				Window.open(getHostPageLocation() + "?locale=" + localeName,
//						"_self", "");
//			}
//		});
		HorizontalPanel localeWrapper = new HorizontalPanel();
//		localeWrapper.add(images.locale().createImage());
//		localeWrapper.add(localeBox);
		vPanel.add(localeWrapper);

		// Add the option to change the style
		final HorizontalPanel styleWrapper = new HorizontalPanel();
		vPanel.add(styleWrapper);
		for (int i = 0; i < FujiAdminConstants.STYLE_THEMES.length; i++) {
			final ThemeButton button = new ThemeButton(
					FujiAdminConstants.STYLE_THEMES[i]);
			styleWrapper.add(button);
			button.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					// Update the current theme
					CUR_THEME = button.getTheme();

					// Reload the current tab, loading the new theme if
					// necessary
//					TabBar bar = ((TabBar) app.getContentTitle());
//					bar.selectTab(bar.getSelectedTab());

					// Load the new style sheets
					updateStyleSheets();
				}
			});
		}
	}
    private String getLoadingImageHTML() {
        if (loadingImage == null) {
            loadingImage = "<img src=\"" + GWT.getModuleBaseURL()
                + "images/loading.gif\">";
          }
        return loadingImage;
    }
    
//    private String fomratedItemTokens() {
//    	StringBuilder buf = new StringBuilder();
//    	for (String key : itemTokens.keySet()) {
//    		if (key.startsWith("OSGiBundleInfo")) {
//    			buf.append(key + ",\n");
//    		}
//    	}
//    	return buf.toString();
//    }
    
    public void selectExplorerNode(String itemTkn) {
    	TreeItem node = null;
    	if ( itemTkn == null ) {
    		node = instanceNode;
    	} else {
    		String key = itemTkn.trim();
    		node = itemTokens.get(itemTkn);
    		if (node == null) {
    			// Window.alert("Node not found for Item Token " + itemTkn + "<br>" + fomratedItemTokens());
    			node = instanceNode;
    		}
    	}
    	if ( node == null) {
    		// Window.alert("Node not found for Item Token. Not selecting any" + itemTkn);
    		return;
    	}
        app.getExplorer().setSelectedItem(node, false);
        app.getExplorer().ensureSelectedItemVisible();   	
    	displayContentWidget(node);
    }
    
	private void clearExplorer() {
		engineFolder.removeItems();
		bindingFolder.removeItems();
		slibFolder.removeItems();
		assemblyFolder.removeItems();
		interceptorFolder.removeItems();
		bundleFolder.removeItems();
		serviceFolder.removeItems();
		configFolder.removeItems();
		
		itemTokens.clear();
		itemWidgets.clear();
		
		// add the item tokens and widgets for instance node and its folders
		itemWidgets.put(instanceNode, fujiInstanceContent); 
		itemTokens.put(getContentWidgetToken(instanceNode), instanceNode);		
		
		itemWidgets.put(engineFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(engineFolder), engineFolder);	
		
		itemWidgets.put(bindingFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(bindingFolder), bindingFolder);
		
		itemWidgets.put(slibFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(slibFolder), slibFolder);		
		
		itemWidgets.put(assemblyFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(assemblyFolder), assemblyFolder);		
		
		itemWidgets.put(interceptorFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(interceptorFolder), interceptorFolder);
		
		itemWidgets.put(bundleFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(bundleFolder), bundleFolder);
		
		itemWidgets.put(serviceFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(serviceFolder), serviceFolder);
		
		itemWidgets.put(configFolder, defaultContent); 
		itemTokens.put(getContentWidgetToken(configFolder), configFolder);		
		
	}
	
	public void loadFujiInstance(final String selectTkn) {
		
		AsyncCallback<FujiInstanceInfo> callback = new AsyncCallback<FujiInstanceInfo>() {

			public void onFailure(Throwable caught) {
				// do something with errors
				loadDefaultFujiInstance(constants);
			}

			public void onSuccess(FujiInstanceInfo result) {
				// update the watch list FlexTable
				loadFujiInstance(constants, result, selectTkn);
			}
		};
//		loadFujiInstance(constants, new FujiInstanceInfo(), selectTkn);
		 getFujiAdminService().getFujiInstanceInfo(callback);		
	}
	
	private void setupExplorerView(final FujiAdminConstants constants) {
        defaultContent = new ContentPanel();
        osgiBundleContent = new OSGiBundleContentPanel();
        osgiServiceContent = new OSGiServiceContentPanel();
        osgiConfigContent = new OSGiConfigContentPanel();
        jbiBundleContent = new JBIBundleContentPanel();
        fujiInterceptorContent = new FujiInterceptorContentPanel();
        fujiInstanceContent = new FujiInstanceContentPanel();
        
		// instance root
        FujiInstanceInfo info = null;
		instanceNode = addExplorerNode(info);
		// engine folder
        engineFolder = addExplorerNodeForInstanceFolder(constants.engineFolder());		
		// binding folder
        bindingFolder = addExplorerNodeForInstanceFolder(constants.bindingFolder());	
		// slib folder
        slibFolder = addExplorerNodeForInstanceFolder(constants.slibFolder());
		// assembly folder
        assemblyFolder = addExplorerNodeForInstanceFolder(constants.assemblyFolder());	        
		// interceptor folder
        interceptorFolder = addExplorerNodeForInstanceFolder(constants.interceptorFolder());	
		
		// bundle folder
        bundleFolder = addExplorerNodeForInstanceFolder(constants.osgiBundleFolder());		
        // service folder
        serviceFolder = addExplorerNodeForInstanceFolder(constants.osgiServiceFolder());
        // config folder
        configFolder = addExplorerNodeForInstanceFolder(constants.osgiConfigFolder());     
        
        loadFujiInstance(getContentWidgetToken(instanceNode));
	}


	
	private void loadDefaultFujiInstance(FujiAdminConstants constants) {
		//TODO:  show error icon or not connected icon with error in tooltip
		clearExplorer();
		instanceNode.setHTML(images.instanceNodeIcon().getHTML() + " " + constants.fujiInstanceName());
	}
	
    private void loadFujiInstance(FujiAdminConstants constants, FujiInstanceInfo info, String selectTkn) {

    	clearExplorer();
    	updateInstanceNode(instanceNode, info);		
        // engine childern
        for ( OSGiBundleInfo bInfo : info.getEngineList()) {
            TreeItem bInfoItem = addExplorerNode(bInfo);
        }

        // binding childern
        for ( OSGiBundleInfo bInfo : info.getBindingList()) {
            TreeItem bInfoItem = addExplorerNode(bInfo);
        }

        // slib childern
        for ( OSGiBundleInfo bInfo : info.getSLibList()) {
            TreeItem bInfoItem = addExplorerNode(bInfo);
        }
        
        // assembly childern
        for ( OSGiBundleInfo bInfo : info.getAssemblyList()) {
            TreeItem bInfoItem = addExplorerNode(bInfo);
        }
        
        // interceptor childern
        for ( FujiInterceptorInfo iInfo : info.getInterceptorList()) {
            TreeItem bInfoItem = addExplorerNode(iInfo);
        }       
        
        // bundle childern
        for ( OSGiBundleInfo bInfo : info.getBundleList()) {
            TreeItem bInfoItem = addExplorerNode(bInfo);
        }
        
        // service children
        for ( OSGiServiceInfo sInfo : info.getServiceList()) {
            TreeItem sInfoItem = addExplorerNode(sInfo);
        }
        
        // config children
        for ( OSGiConfigInfo cInfo : info.getConfigList()) {
            TreeItem cInfoItem = addExplorerNode(cInfo);
        }
        selectExplorerNode(selectTkn);
    }
	
	private TreeItem addExplorerNode(TreeItem parent,
			AbstractImagePrototype image, String name, ContentPanel content, Object userObject) {
		TreeItem node = null;
		node = parent.addItem(image.getHTML() + " " + name);
		node.setUserObject(userObject);
		itemWidgets.put(node, content); 
		itemTokens.put(getContentWidgetToken(node), node);			
		return node;
	}
	
	private TreeItem updateInstanceNode(TreeItem node, FujiInstanceInfo info) {
		ContentPanel content = fujiInstanceContent;			
		AbstractImagePrototype image = null;
		String name = null;
		String itemName = null;
		if ( info == null ) {
			image = images.instanceNodeIcon();
			name = constants.fujiInstanceName();	
			itemName = image.getHTML() + "" + name + "[" + getLoadingImageHTML() + "loading...]";
		} else {
			image = images.instanceNodeIcon();
			name = info.getName();
			itemName = image.getHTML() + "" + name;		
		}
		node.setHTML(itemName);
		node.setUserObject(info);
		itemWidgets.put(node, content); 
		itemTokens.put(getContentWidgetToken(node), node);		
		return node;
	}
	
	private TreeItem addExplorerNode(FujiInstanceInfo info) {
		Tree explorer = app.getExplorer();
		TreeItem node = explorer.addItem(constants.fujiInstanceName());
		updateInstanceNode(node, info);
		return node;
	}
	
	private TreeItem addExplorerNodeForInstanceFolder(String folderName) {
		TreeItem node = null;
		TreeItem parent = instanceNode;
		ContentPanel content = defaultContent; //TODO add folderContent panel
		AbstractImagePrototype image = images.folderNodeIcon();
		node = addExplorerNode(parent,image,folderName, content, null);	
		return node;
	}		
	
	private TreeItem addExplorerNode(OSGiBundleInfo info) {
		String type = info.getJbiArtifactType();
		TreeItem node = null;
		if ( OSGiBundleInfo.JBI_BC.equals(type)) {
			node = addExplorerNodeForBinding(info);
		}  else if ( OSGiBundleInfo.JBI_SE.equals(type)) {
			node = addExplorerNodeForEngine(info);
		}  else if ( OSGiBundleInfo.JBI_SL.equals(type)) {
			node = addExplorerNodeForSLib(info);
		}  else if ( OSGiBundleInfo.JBI_SA.equals(type)) {
			node = addExplorerNodeForAssembly(info);
		}  else  {
			node = addExplorerNodeForBundle(info);
		} 			
		return node;
	}
	
	private TreeItem addExplorerNodeForBinding(OSGiBundleInfo info) {
		TreeItem node = null;
		TreeItem parent = bindingFolder;
		
		AbstractImagePrototype image = null;
		String name = info.getSymbolicName();
		
		ContentPanel content = jbiBundleContent;
		
		String state = info.getState();
		
		if ( OSGiBundleInfo.STATE_ACTIVE.equals(state)) {
			// active
			image = images.bindingStarted();
		} else if (OSGiBundleInfo.STATE_RESOLVED.equals(state)) {
			// stopped
			image = images.bindingStopped();
		} else {
			// installed
			image = images.bindingShutdown();
		}			
		
		node = addExplorerNode(parent, image, name, content, info);
		return node;
	}	
	
	private TreeItem addExplorerNodeForEngine(OSGiBundleInfo info) {
		TreeItem node = null;
		TreeItem parent = engineFolder;
		ContentPanel content = jbiBundleContent;
		AbstractImagePrototype image = null;
		String name = info.getSymbolicName();
		
		String state = info.getState();
		
		if ( OSGiBundleInfo.STATE_ACTIVE.equals(state)) {
			// active
			image = images.engineStarted();
		} else if (OSGiBundleInfo.STATE_RESOLVED.equals(state)) {
			// stopped
			image = images.engineStopped();
		} else {
			// installed
			image = images.engineShutdown();
		}			
		node = addExplorerNode(parent, image, name, content, info);		
		return node;
	}		
	
	private TreeItem addExplorerNodeForSLib(OSGiBundleInfo info) {
		TreeItem node = null;
		TreeItem parent = slibFolder;
		ContentPanel content = jbiBundleContent;
		
		String name = info.getSymbolicName();
		AbstractImagePrototype image = null;
		String state = info.getState();
		
		if ( OSGiBundleInfo.STATE_ACTIVE.equals(state)) {
			// active
			image = images.slibStarted();
		} else if (OSGiBundleInfo.STATE_RESOLVED.equals(state)) {
			// stopped
			image = images.slibStopped();
		} else {
			// installed
			image = images.slibShutdown();
		}			
		node = addExplorerNode(parent, image, name, content, info);		
		return node;
	}	
	
	private TreeItem addExplorerNodeForAssembly(OSGiBundleInfo info) {
		TreeItem node = null;
		TreeItem parent = assemblyFolder;
		ContentPanel content = jbiBundleContent;
		
		String name = info.getSymbolicName();
		AbstractImagePrototype image = null;
		String state = info.getState();
		
		if ( OSGiBundleInfo.STATE_ACTIVE.equals(state)) {
			// active
			image = images.assemblyStarted();
		} else if (OSGiBundleInfo.STATE_RESOLVED.equals(state)) {
			// stopped
			image = images.assemblyStopped();
		} else {
			// installed
			image = images.assemblyShutdown();
		}			
		node = addExplorerNode(parent, image, name, content, info);		
		return node;
	}
	

	private TreeItem addExplorerNodeForBundle(OSGiBundleInfo info) {
		TreeItem node = null;
		TreeItem parent = bundleFolder;
		ContentPanel content = osgiBundleContent;
		
		String name = info.getSymbolicName();
		AbstractImagePrototype image = null;
		String state = info.getState();
		
		if ( OSGiBundleInfo.STATE_ACTIVE.equals(state)) {
			// active
			image = images.bundleActive();
		} else if (OSGiBundleInfo.STATE_RESOLVED.equals(state)) {
			// stopped
			image = images.bundleResolved();
		} else {
			// installed
			image = images.bundleInstalled();
		}			
		node = addExplorerNode(parent, image, name, content, info);	
		return node;
	}	
	
	private TreeItem addExplorerNode(FujiInterceptorInfo info) {
		TreeItem node = null;
		TreeItem parent = interceptorFolder;
		ContentPanel content = fujiInterceptorContent;
		
		String name = info.getDisplayName();
		AbstractImagePrototype image = null;
		image = images.interceptorNodeIcon();
		node = addExplorerNode(parent, image, name, content, info);		
		return node;
	}			
	
	private TreeItem addExplorerNode(OSGiServiceInfo info) {
		TreeItem node = null;
		TreeItem parent = serviceFolder;
		ContentPanel content = osgiServiceContent;
		
		String name = info.getObjectClasses()[0];
		AbstractImagePrototype image = null;
		image = images.osgiServiceNodeIcon();
		node = addExplorerNode(parent, image, name, content, info);		
		return node;
	}
	
	private TreeItem addExplorerNode(OSGiConfigInfo info) {
		TreeItem node = null;
		TreeItem parent = configFolder;
		ContentPanel content = osgiConfigContent;
		
		String name = info.getPid();
		AbstractImagePrototype image = null;
		image = images.osgiConfigNodeIcon();
		node = addExplorerNode(parent, image, name, content, info);		
		return node;
	}	
	/**
	 * Get the style name of the reference element defined in the current GWT
	 * theme style sheet.
	 * 
	 * @param prefix
	 *            the prefix of the reference style name
	 * @return the style name
	 */
	private String getCurrentReferenceStyleName(String prefix) {
		String gwtRef = prefix + "-Reference-" + CUR_THEME;
		if (LocaleInfo.getCurrentLocale().isRTL()) {
			gwtRef += "-rtl";
		}
		return gwtRef;
	}

	/**
	 * Update the style sheets to reflect the current theme and direction.
	 */
	private void updateStyleSheets() {
		// Generate the names of the style sheets to include
		String gwtStyleSheet = "gwt/" + CUR_THEME + "/" + CUR_THEME + ".css";
		String fujiAdminStyleSheet = CUR_THEME + "/FujiAdmin.css";

		// Find existing style sheets that need to be removed
		boolean styleSheetsFound = false;
		final HeadElement headElem = StyleSheetLoader.getHeadElement();
		final List<Element> toRemove = new ArrayList<Element>();
		NodeList<Node> children = headElem.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.getItem(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = Element.as(node);
				if (elem.getTagName().equalsIgnoreCase("link")
						&& elem.getPropertyString("rel").equalsIgnoreCase(
								"stylesheet")) {
					styleSheetsFound = true;
					String href = elem.getPropertyString("href");
					// If the correct style sheets are already loaded, then we
					// should have
					// nothing to remove.
					if (!href.contains(gwtStyleSheet)
							&& !href.contains(fujiAdminStyleSheet)) {
						toRemove.add(elem);
					}
				}
			}
		}

		// Return if we already have the correct style sheets
//		if (styleSheetsFound && toRemove.size() == 0) {
//			return;
//		}

		// Detach the app while we manipulate the styles to avoid rendering
		// issues
		RootPanel.get().remove(app);

		// Remove the old style sheets
		for (Element elem : toRemove) {
			headElem.removeChild(elem);
		}

		// Load the GWT theme style sheet
		String modulePath = GWT.getModuleBaseURL();
		Command callback = new Command() {
			/**
			 * The number of style sheets that have been loaded and executed
			 * this command.
			 */
			private int numStyleSheetsLoaded = 0;

			public void execute() {
				// Wait until all style sheets have loaded before re-attaching
				// the app
				numStyleSheetsLoaded++;
				if (numStyleSheetsLoaded < 2) {
					return;
				}

				// Different themes use different background colors for the body
				// element, but IE only changes the background of the visible
				// content
				// on the page instead of changing the background color of the
				// entire
				// page. By changing the display style on the body element, we
				// force
				// IE to redraw the background correctly.
				RootPanel.getBodyElement().getStyle().setProperty("display",
						"none");
				RootPanel.getBodyElement().getStyle()
						.setProperty("display", "");
				RootPanel.get().add(app);
			}
		};
		StyleSheetLoader.loadStyleSheet(modulePath + gwtStyleSheet,
				getCurrentReferenceStyleName("gwt"), callback);

		// Load the showcase specific style sheet after the GWT theme style
		// sheet so
		// that custom styles supercede the theme styles.
		StyleSheetLoader.loadStyleSheet(modulePath + fujiAdminStyleSheet,
				getCurrentReferenceStyleName("Application"), callback);
	}
}
