/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
/**
*
* @author chikkala
*/
public class FujiAdminPane extends Composite implements ResizeHandler,
		HasSelectionHandlers<TreeItem> {
	
	/**
	 * The base style name.
	 */
	public static final String DEFAULT_STYLE_NAME = "Application";
	/**
	 * The pane that contains the title widget and links.
	 */
	private TitlePane titlePane;

	/**
	 * The pane that contains the explore and content.
	 */
	private BodyPane bodyPane;

	/**
	 * The last known width of the window.
	 */
	private int windowWidth = -1;

	public FujiAdminPane() {
		// Setup the main layout widget
		FlowPanel layout = new FlowPanel();
		initWidget(layout);

		// Setup the title pane with the title and links
		titlePane = new TitlePane();
		layout.add(titlePane);

		// Setup content pane.
		bodyPane = new BodyPane();
		layout.add(bodyPane);

		// bodyPane.setHeight("600px");
		// Add a window resize handler
		Window.addResizeHandler(this);
	}

	public TitlePane getTitlePane() {
		return titlePane;
	}
	
	public ExplorerPane getExplorerPane() {
		return bodyPane.getExplorerPane();
	}
	
//	public ContentPane getContentPane() {
//		return bodyPane.getContentPane();
//	}
	/**
	 * Add a link to the top of the page.
	 * 
	 * @param link
	 *            the widget to add to the mainLinks
	 */
	public void addLink(Widget link) {
		getTitlePane().addLink(link);
	}

	/**
	 * Set the {@link Widget} to use as options, which appear to the right of
	 * the title bar.
	 * 
	 * @param options
	 *            the options widget
	 */
	public void setOptionsWidget(Widget options) {
		getTitlePane().setOptionsWidget(options);
	}

	/**
	 * @return the main menu.
	 */
	public Tree getExplorer() {
		return getExplorerPane().getExplorer();
	}

	/**
	 * @return the {@link Widget} in the content area
	 */
	public Widget getContent() {
//		return getContentPane().getContent();
		return this.bodyPane.getContent();
	}

	/**
	 * Set the {@link Widget} to display in the content area.
	 * 
	 * @param content
	 *            the content widget
	 */
	public void setContent(Widget content) {
//		getContentPane().setContent(content);
		this.bodyPane.setContent(content);
	}

	/**
	 * Set the title of the content area.
	 * 
	 * @param title
	 *            the content area title
	 */
	public void setContentTitle(String title) {
//		getContentPane().setContentTitle(title);
		this.bodyPane.setContentTitle(title);
	}

	@Override
	protected void onLoad() {
		super.onLoad();
		onWindowResized(Window.getClientWidth(), Window.getClientHeight());
	}

	@Override
	protected void onUnload() {
		super.onUnload();
		windowWidth = -1;
	}

	public void onResize(ResizeEvent event) {
		onWindowResized(event.getWidth(), event.getHeight());
	}

	public void onWindowResized(int width, int height) {
		if (width == windowWidth || width < 1) {
			return;
		}
		windowWidth = width;
		onWindowResizedImpl(width);
	}

	protected void onWindowResizedImpl(int width) {
		bodyPane.onWindowResizedImpl(width);
	}

	public HandlerRegistration addSelectionHandler(
			SelectionHandler<TreeItem> handler) {
		return getExplorerPane().addSelectionHandler(handler);
	}
}
