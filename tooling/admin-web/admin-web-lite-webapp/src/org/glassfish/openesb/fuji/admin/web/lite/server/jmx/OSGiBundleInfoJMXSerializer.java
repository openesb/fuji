/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.admin.web.lite.server.jmx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiBundleInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;


/**
 * Bean that represents the osgi bundle info and has a serialization/deserialization 
 * code to/from jmx open types that represent the data.
 * 
 * @author chikkala
 */
public class OSGiBundleInfoJMXSerializer {

    // jmx tabular data names
    public static final String[] ITEM_NAMES = {
        OSGiBundleInfo.PROP_BUNDLE_ID,
        OSGiBundleInfo.PROP_SYMBOLIC_NAME,
        OSGiBundleInfo.PROP_STATE,
        OSGiBundleInfo.PROP_JBI_ARTIFACT_TYPE,
        OSGiBundleInfo.PROP_LAST_MODIFIED,
        OSGiBundleInfo.PROP_LOCATION,
        OSGiBundleInfo.PROP_HEADERS,
        OSGiBundleInfo.PROP_JBI_DESCRIPTOR
    };


    /**
     * constructor
     */
    public OSGiBundleInfoJMXSerializer() {
    }


    public static OpenType[] getItemTypes() throws OpenDataException {
        TabularType headersType = OSGiPropertyInfoJMXSerializer.getTabularType();
        OpenType[] itemTypes = {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            headersType,
            SimpleType.STRING
        };
        return itemTypes;
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiBundleInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemNames = ITEM_NAMES;
        String[] itemDescs = ITEM_NAMES;
        OpenType[] itemTypes = getItemTypes();
        type = new CompositeType(typeName, typeDesc, itemNames, itemDescs, itemTypes);
        return type;
    }

    public static CompositeData toCompositeData(OSGiBundleInfo info) throws OpenDataException {
        CompositeData data = null;
        CompositeType type = getCompositeType();
        String[] itemNames = ITEM_NAMES;
        TabularData headerData = OSGiPropertyInfoJMXSerializer.toTabularData(info.getHeaders());
        Object[] itemValues = {
            info.getBundleId(),
            info.getSymbolicName(),
            info.getState(),
            info.getJbiArtifactType(),
            info.getLastModified(),
            info.getLocation(),
            headerData,
            info.getJbiDescriptor()
        };
        data = new CompositeDataSupport(type, itemNames, itemValues);
        return data;
    }

    public static OSGiBundleInfo toOSGiBundleInfo(CompositeData data) throws OpenDataException {
        OSGiBundleInfo info = new OSGiBundleInfo();
//        if (!data.getCompositeType().equals(getCompositeType())) {
//            throw new OpenDataException("Can not convert ");
//        }
        info.setBundleId((String) data.get(OSGiBundleInfo.PROP_BUNDLE_ID));
        info.setSymbolicName((String) data.get(OSGiBundleInfo.PROP_SYMBOLIC_NAME));
        info.setState((String) data.get(OSGiBundleInfo.PROP_STATE));
        info.setJbiArtifactType((String) data.get(OSGiBundleInfo.PROP_JBI_ARTIFACT_TYPE));
        info.setLastModified((String) data.get(OSGiBundleInfo.PROP_LAST_MODIFIED));
        info.setLocation((String) data.get(OSGiBundleInfo.PROP_LOCATION));
        TabularData headersData = (TabularData) data.get(OSGiBundleInfo.PROP_HEADERS);
        List<OSGiPropertyInfo> headersList = new ArrayList<OSGiPropertyInfo>();
        if (headersData != null) {
            headersList = OSGiPropertyInfoJMXSerializer.toOSGiPropertyInfoList(headersData);
        }
        info.setHeaders(headersList);
        info.setJbiDescriptor((String) data.get(OSGiBundleInfo.PROP_JBI_DESCRIPTOR));
        return info;
    }

    public static List<OSGiBundleInfo> toOSGiBundleInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>) tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiBundleInfo info = OSGiBundleInfoJMXSerializer.toOSGiBundleInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiBundleInfo> list) throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiBundleInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiBundleInfoJMXSerializer.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiBundleInfoJMXSerializer.ITEM_NAMES);
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiBundleInfo info : list) {
            tdata.put(OSGiBundleInfoJMXSerializer.toCompositeData(info));
        }
        return tdata;
    }
}
