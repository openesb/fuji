/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.i18n.client.Constants;

/**
 *
 * @author chikkala
 */
public interface FujiAdminConstants extends Constants {
  /**
   * Link to FUJI homepage.
   */
  String FUJI_HOMEPAGE = "https://fuji.dev.java.net/";

  /**
   * Link to FUJI Wiki page.
   */
  String FUJI_WIKI = "http://wiki.open-esb.java.net/Wiki.jsp?page=FujiGettingStarted";

  /**
   * The available style themes that the user can select.
   */
  String[] STYLE_THEMES = {"standard", "chrome", "dark"};
  
  /**
   * @return text for the link to the Fuji Wiki
   */
  String mainLinkWiki();

  /**
   * @return text for the link to the Fuji homepage
   */
  String mainLinkHomepage();

  /**
   * @return the title of the main menu
   */
  String mainMenuTitle();

  /**
   * @return the sub title of the application
   */
  String mainSubTitle();

  /**
   * @return the title of the application
   */
  String mainTitle();

  @Key("fuji.instance.name")
  String fujiInstanceName();
  
  String engineFolder();
  String bindingFolder();
  String slibFolder();
  String assemblyFolder();
  String interceptorFolder();
  
  String osgiBundleFolder();
  String osgiServiceFolder();
  String osgiConfigFolder();
  
  String engineTitle();
  String bindingTitle();
  String slibTitle();
  String assemblyTitle();
  String interceptorTitle();
  
  String osgiBundleTitle();
  String osgiServiceTitle();
  String osgiConfigTitle();
  
  String installBtn();
  String stopBtn();
  String startBtn();
  String uninstallBtn();
  
  
}
