/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeImages;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.ImageBundle.Resource;
/**
*
* @author chikkala
*/
public class ExplorerPane extends Composite implements HasSelectionHandlers<TreeItem> {
	
	public interface FujiAdminTreeImages extends TreeImages {
		/**
		 * An image indicating a leaf.
		 * 
		 * @return a prototype of this image
		 */
		@Resource("org/glassfish/openesb/fuji/admin/web/lite/client/images/noimage.png")
		AbstractImagePrototype treeLeaf();
	}
	/**
	 * The base style name.
	 */
	public static final String DEFAULT_STYLE_NAME = "Application";

	private Tree explorer;
	private ScrollPanel explorerView;
	// private Grid explorerViewLayout;
	private DecoratorPanel layout;

	public ExplorerPane() {

		// Setup explorer view
		initExplorerPane();
		initWidget(layout);
	}

	/**
	 * @return the main menu.
	 */
	public Tree getExplorer() {
		return explorer;
	}

	public HandlerRegistration addSelectionHandler(
			SelectionHandler<TreeItem> handler) {
		return explorer.addSelectionHandler(handler);
	}

	private void initExplorerPane() {
		FujiAdminTreeImages treeImages = GWT.create(FujiAdminTreeImages.class);
		explorer = new Tree(treeImages);
		explorer.setAnimationEnabled(true);
		explorer.addStyleName(DEFAULT_STYLE_NAME + "-menu");
		explorerView = new ScrollPanel(explorer);
		explorerView.setSize("300px", "600px");

		layout = new DecoratorPanel();
		layout.setWidget(explorerView);
	}
}
