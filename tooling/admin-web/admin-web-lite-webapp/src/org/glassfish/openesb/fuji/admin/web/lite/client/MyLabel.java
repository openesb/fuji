/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
/**
*
* @author chikkala
*/
public class MyLabel extends Label {
	private String mText;
	private int visibleLength;
	private Tooltip tooltip;
	private boolean needTooltip;

	public MyLabel(String text, boolean wordWrap, int visibleLength) {
		super();
		initLabel(text, wordWrap, visibleLength);
	}

	public MyLabel(String text, int visibleLength) {
		this(text, false, visibleLength);
	}

	public MyLabel(String text) {
		this(text, false, -1);
	}

	private void initLabel(String text, boolean wordWrap, int visibleLength) {
		this.mText = text;
		this.setVisibleLength(visibleLength);
		this.setWordWrap(wordWrap);
		String visibleText = text;
		if (text != null && this.visibleLength >= 0
				&& text.length() > this.visibleLength) {
			visibleText = text.substring(0, this.visibleLength)
					+ "...";
			needTooltip = true;
			this.tooltip = new Tooltip(this);
		}
		this.setText(visibleText);		

		this.addMouseOverHandler(new MouseOverHandler() {

			public void onMouseOver(MouseOverEvent event) {
				showTooltip();
			}

		});

		this.addMouseOutHandler(new MouseOutHandler() {

			public void onMouseOut(MouseOutEvent event) {
				if (!isTargetTooltip(event)) {
					hideTooltip();
				}
			}

		});
	}

	protected boolean isTargetTooltip(MouseOutEvent event) {
		if ( tooltip == null || !this.tooltip.isShowing()) {
			return false;
		}
		
		int left = this.tooltip.getPopupLeft();
		int top = this.tooltip.getAbsoluteTop();
		int width = this.tooltip.getOffsetWidth();
		int height = this.tooltip.getOffsetHeight();
		int x = event.getClientX();
		int y = event.getClientY();
		return ( x >= left && x <= left + width && y >= top && y <= top + height);
	}
	
	public int getVisibleLength() {
		return visibleLength;
	}

	public void setVisibleLength(int visibleLength) {
		this.visibleLength = visibleLength;
	}

	protected String getTooltipText() {
		return this.mText;
	}

	private void showTooltip() {
		if ( tooltip != null ) {
			tooltip.showRelativeTo(this);
		}
	}

	private void hideTooltip() {
		if ( tooltip != null) {
			tooltip.hide();
		}
	}

	public static class Tooltip extends PopupPanel {
		private ScrollPanel layout;
		private MyLabel widget;
		private HTML tip;

		public Tooltip(MyLabel widget) {
			super(true);
			this.widget = widget;
			this.layout = new ScrollPanel();
			tip = new HTML(widget.getTooltipText());
			this.layout.setWidget(tip);
			// add(tip);
			this.setWidget(this.layout);
			this.layout.setSize("500px", "70px");
			
			this.addDomHandler(new MouseOutHandler() {

				public void onMouseOut(MouseOutEvent event) {
						hideTooltip();
				}

			}, MouseOutEvent.getType());			
		}
		
		private void hideTooltip() {
				this.hide();
		}
	}
}
