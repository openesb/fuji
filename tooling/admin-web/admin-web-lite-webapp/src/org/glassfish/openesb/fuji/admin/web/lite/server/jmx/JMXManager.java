/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.server.jmx;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 *
 * @author chikkala
 */
public class JMXManager implements PropertyChangeListener {

    private static Logger LOG = Logger.getLogger(JMXManager.class.getName());
    public static final String FUJI_ADMIN_OBJECT_NAME = "con.sun.jbi.fuji:runtime=OSGi,type=admin,service=admin";
    private static ObjectName sAdminMBeanOName;
    private MBeanServerConnection mMBS;
    private JMXConnector mConnector;
    private String mHost;
    private String mPort;
    private String mUsername;
    private String mPassword;
    private boolean usePlatformServer = false;

    public JMXManager() {
        this("localhost", "8699", "", "");
    }

    public JMXManager(String port) {
        this("localhost", port, "", "");
    }

    public JMXManager(String port, String username, String password) {
        this("localhost", port, username, password);
    }

    public JMXManager(String host, String port, String username, String password) {
        this.mHost = host;
        this.mPort = port;
        this.mUsername = username;
        this.mPassword = password;
        this.usePlatformServer = false;
    }

    public boolean isUsePlatformServer() {
        return usePlatformServer;
    }

    public void setUsePlatformServer(boolean usePlatformServer) {
        this.usePlatformServer = usePlatformServer;
    }

    public String getHost() {
        return mHost;
    }

    public void setHost(String host) {
        this.mHost = host;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    public String getPort() {
        return mPort;
    }

    public void setPort(String port) {
        this.mPort = port;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public MBeanServerConnection getMBeanServerConnection() {
        return mMBS;
    }
    
    public void setMBeanServerConnection(MBeanServerConnection mbs) {
        this.mMBS = mbs;
    }

    protected String getURL() {

        String host = getHost();
        String port = getPort();


        if (host == null) {
            host = "localhost";
        }
        if (port == null) {
            port = "8699";
        }

        String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
        return url;
    }

    protected Map<String, Object> getEnvironment() {
        Map<String, Object> env = new HashMap<String, Object>();
        String username = getUsername();
        String password = getPassword();
        if (username == null) {
            username = "";
        }
        if (password == null) {
            password = "";
        }
        // LOG.info("Connection Credentials user:" + username + " password:" + password.length());
        env.put(JMXConnector.CREDENTIALS, new String[]{username, password});
        return env;
    }

    /**
     * JMX Agent connection
     * 
     */
    protected void connect() throws Exception {
        if (isUsePlatformServer()) {
//            // Get the Platform MBean Server
//            MBeanServer pServer = ManagementFactory.getPlatformMBeanServer();
////             mMBS = pServer;
//            // pServer.
            LOG.fine("Conneting to FUJI Server Platform JMX Server....");
        } else {
            // Create JMX Agent URL
            String url = getURL();
            LOG.info("Conneting to FUJI Server:  JMX Connetion URL " + url);
            JMXServiceURL jmxServiceURL = new JMXServiceURL(url);
            // Connect the JMXConnector
            mConnector = JMXConnectorFactory.connect(jmxServiceURL, getEnvironment());
            // Get the MBeanServerConnection
            mMBS = mConnector.getMBeanServerConnection();
        }
    }

    protected void close() throws Exception {

        //Close the connection
        try {
            if ( mConnector != null ) {
                mConnector.close();
            }
        } finally {
            mConnector = null;
            if (!this.usePlatformServer) {
//                LOG.info("Closing Remote MBS...");
                mMBS = null;
            }
        }
    }

    public static ObjectName getAdminMBeanOjectName() {

        try {
            if (sAdminMBeanOName == null) {
                sAdminMBeanOName = new ObjectName(FUJI_ADMIN_OBJECT_NAME);
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return sAdminMBeanOName;
    }

    public boolean isJMXAdminAvailable() {
        boolean available = false;
        try {
            this.connect();
        } catch (Exception ex) {
            return false; // can not connect.

        }
        try {
            available = this.getMBeanServerConnection().isRegistered(JMXManager.getAdminMBeanOjectName());
        } catch (Exception ex) {
            // ignore
        } finally {
            try {
                this.close();
            } catch (Exception ex) {
                // ingore.
            }
        }
        return available;
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName,
            Object[] params, String[] signature) throws Exception {
        Object resultObject = null;
        this.connect();
        try {
            resultObject = getMBeanServerConnection().invoke(objName, operationName, params, signature);
        } catch (MBeanException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            Exception targetEx = ex.getTargetException();
            if (targetEx == null) {
                targetEx = ex;
            }
            throw targetEx;
        } finally {
            try {
                this.close();
            } catch (Exception ex) {
                // ingore.
            }
        }
        return resultObject;
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName) throws Exception {
        Object[] params = new Object[0];
        String[] signature = new String[0];
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, String param) throws Exception {
        Object[] params = {param};
        String[] signature = {"java.lang.String"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, long param) throws Exception {
        Object[] params = {Long.valueOf(param)};
        String[] signature = {"long"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2) throws Exception {
        Object[] params = {param1, Boolean.valueOf(param2)};
        String[] signature = {"java.lang.String", "boolean"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2, boolean param3) throws Exception {
        Object[] params = {param1, Boolean.valueOf(param2), Boolean.valueOf(param2)};
        String[] signature = {"java.lang.String", "boolean", "boolean"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
//    public void testMBeanServer() {
//        MBeanServerConnection mbs = this.mMBS;
//        LOG.info("############  testing MBean Server....");
//        try {
//            if (mbs == null) {
//                LOG.info("No MBeanServer Connection");
//            }
//            String[] domains = mbs.getDomains();
//            for ( String domain : domains ) {
//                LOG.info("MBS domain: " + domain);
//            }
//            Set mbeans = mbs.queryMBeans(null, null);
//            for ( Object mbean : mbeans ) {
//                javax.management.ObjectInstance objInst = (javax.management.ObjectInstance)mbean;
//                LOG.info("MBean=" + objInst.getObjectName());
//            }
//        } catch (IOException ex) {
//            // Logger.getLogger(AdminWebLiteActivator.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
//    }   
    
}
