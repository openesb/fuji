/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.server.jmx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiConfigInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;

/**
 * Bean that represents the osgi configuraiton info and has a serialization/deserialization 
 * code to/from jmx open types that represent the data.
 * 
 * @author chikkala
 */
public class OSGiConfigInfoJMXSerializer {
    // types
    public static final String[] ITEM_NAMES = {
        OSGiConfigInfo.PROP_PID,
        OSGiConfigInfo.PROP_FACTORY_PID,
        OSGiConfigInfo.PROP_BUNDLE_LOCATION,
        OSGiConfigInfo.PROP_PROPERTIES
    };

    public OSGiConfigInfoJMXSerializer() {
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiConfigInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemDescs = ITEM_NAMES;
        TabularType propertiesType = OSGiPropertyInfoJMXSerializer.getTabularType();

        OpenType[] itemTypes = {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            propertiesType
        };

        type = new CompositeType(typeName, typeDesc, ITEM_NAMES, itemDescs, itemTypes);
        return type;
    }

    public static CompositeData toCompositeData(OSGiConfigInfo info) throws OpenDataException {
        CompositeData data = null;
        CompositeType type = getCompositeType();
        TabularData propertiesData = OSGiPropertyInfoJMXSerializer.toTabularData(info.getProperties());
        Object[] itemValues = {
            info.getPid(),
            info.getFactoryPid(),
            info.getBundleLocation(),
            propertiesData
        };
        data = new CompositeDataSupport(type, ITEM_NAMES, itemValues);
        return data;
    }

    public static OSGiConfigInfo toOSGiConfigInfo(CompositeData data) throws OpenDataException {
        OSGiConfigInfo info = new OSGiConfigInfo();

        info.setPid((String) data.get(OSGiConfigInfo.PROP_PID));
        info.setFactoryPid((String) data.get(OSGiConfigInfo.PROP_FACTORY_PID));
        info.setBundleLocation((String) data.get(OSGiConfigInfo.PROP_BUNDLE_LOCATION));

        TabularData propData = (TabularData) data.get(OSGiConfigInfo.PROP_PROPERTIES);
        List<OSGiPropertyInfo> propList = new ArrayList<OSGiPropertyInfo>();
        if (propData != null) {
            propList = OSGiPropertyInfoJMXSerializer.toOSGiPropertyInfoList(propData);
        }
        info.setProperties(propList);

        return info;
    }

    public static List<OSGiConfigInfo> toOSGiConfigInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiConfigInfo> list = new ArrayList<OSGiConfigInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>) tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiConfigInfo info = OSGiConfigInfoJMXSerializer.toOSGiConfigInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiConfigInfo> list) throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiConfigInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiConfigInfoJMXSerializer.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiConfigInfoJMXSerializer.ITEM_NAMES);
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiConfigInfo info : list) {
            tdata.put(OSGiConfigInfoJMXSerializer.toCompositeData(info));
        }
        return tdata;
    }
}
