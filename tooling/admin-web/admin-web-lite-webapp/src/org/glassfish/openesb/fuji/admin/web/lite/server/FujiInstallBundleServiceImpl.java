/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
*
* @author chikkala
*/
public class FujiInstallBundleServiceImpl extends HttpServlet {
	
	private File writeToFile(FileItem item) {
//		if ( item instanceof DiskFileItem) {
//			File uploadFile = ((DiskFileItem)item).getStoreLocation();
//			if ( uploadFile != null ) {
//				return uploadFile;
//			}
//		}
		File tempDir = new File(System.getProperty("java.io.tmpdir", "/tmp"));
		File uploadedFile = new File(item.getName());
		String tempName = uploadedFile.getName();
		String name = tempName;
		String ext = null;		
		if (tempName != null && tempName.trim().length() > 0 ) {
			tempName = tempName.trim();
			int idx = tempName.lastIndexOf(".");
			if ( idx >= 0 ) {
				name = tempName.substring(0, idx);
				ext = tempName.substring(idx, tempName.length());
			}
		}
		if ( name == null) {
			name = "";
		}
		try {
			uploadedFile = File.createTempFile(name, ext, tempDir);
			item.write(uploadedFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			uploadedFile = null;
		}
		
		return uploadedFile;
	}
	
	private FujiInstanceMgr getFujiInstanceMgr() {
		FujiInstanceMgr instMgr = (FujiInstanceMgr) this.getServletConfig().getServletContext().getAttribute(FujiAdminServiceConstants.FUJI_INSTANCE_MGR_ID);
		return instMgr;
	}
	
	private File processUploadFile(HttpServletRequest request)throws ServletException, IOException {
		// Check that we have a file upload request
		StringBuilder result = new StringBuilder();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
			throw new ServletException("InstallBundle Servlet Request is not a Multipart Conent");
		}
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// Set factory constraints
//		int maxMemorySize = 1;
//		File tempDir = new File(System.getProperty("java.io.tmpdir", "/tmp")); 
//		factory.setSizeThreshold(maxMemorySize);
//		factory.setRepository(tempDir);
//		factory.setFileCleaningTracker(null); //disable file cleaning tracker

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		File uploadFile = null;
		// Parse the request
		try {
			List /* FileItem */ items = upload.parseRequest(request);
			
			if ( items == null || items.size() == 0) {
				throw new IOException("InstallBundle Servlet Request does not contain file upload items");
			}

			for ( Object item : items ) {
				if ( item instanceof FileItem) {
					FileItem fileItem = (FileItem)item;
					String fileUploadFieldName = fileItem.getFieldName();
					if (!fileItem.isFormField()) {
						// filePath = fileItem.getName();
						uploadFile = writeToFile(fileItem);
						break;
					}
				}
			}
		} catch (FileUploadException e) {
			throw new ServletException(e);
		}
		return uploadFile;
	}
	
    private String installBundle(HttpServletRequest request)
    throws ServletException, IOException {
    	StringBuilder result = new StringBuilder();
    	File uploadFile = processUploadFile(request);
    	if ( uploadFile == null ) {
    		result.append("Install Bundle File Path is NULL");
    	} else {
    		FujiInstanceMgr mgr = getFujiInstanceMgr();
    		if ( mgr == null ) {
    			result.append("Fuji Instance Manager is not found in application context");
    		} else {
    			try {
					String installResult = mgr.installBundle(uploadFile.getAbsolutePath());
					// result.append("Bundle Installed: " + installResult);
					result.append(installResult);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					throw new ServletException(e);
				}
    		}
    	}
    	return result.toString();
    }
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String result = installBundle(request);
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Install Bundle</title>");  
            out.println("</head>");
            out.println("<body>");
//            out.println("<h1>Install Bundle Servlet at " + request.getContextPath () + "</h1>");
//            out.println("<h1>Installing  " + result + "</h1>");
            out.println(result);
            out.println("</body>");
            out.println("</html>");
            /* */
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
