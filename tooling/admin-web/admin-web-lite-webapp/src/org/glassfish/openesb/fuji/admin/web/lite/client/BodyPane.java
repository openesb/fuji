/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
/**
*
* @author chikkala
*/
public class BodyPane extends Composite {
	/**
	 * The base style name.
	 */
	public static final String DEFAULT_STYLE_NAME = "Application";

	/**
	 * The panel that contains the menu and content.
	 */
	private HorizontalPanel layout;
	private ExplorerPane explorerPane;
	// private ContentPane contentPane;
	
	/**
	 * The decorator around the detail view.
	 */
	private DecoratorPanel contentPaneLayout;
	/**
	 * The main wrapper around the detail view.
	 */
	// private Grid contentLayout;
	private VerticalPanel contentLayout;
	/**
	 * The wrapper around the detail view.
	 */
	private SimplePanel contentWrapper;
	
	private Label contentTitle;

	public BodyPane() {

		layout = new HorizontalPanel();
		layout.setWidth("100%");
		layout.setHeight("600px");
		layout.setSpacing(0);
		layout.setVerticalAlignment(HasVerticalAlignment.ALIGN_TOP);

		// Setup explorer view
		explorerPane = new ExplorerPane();
		layout.add(explorerPane);
		layout.setCellHeight(explorerPane, "100%");

		// Setup detail view
//		contentPane = new ContentPane();
		initContentPane();
		layout.add(contentPaneLayout);
		layout.setCellHeight(contentPaneLayout, "100%");

		layout.setCellHorizontalAlignment(contentPaneLayout,
				HasHorizontalAlignment.ALIGN_RIGHT);

		layout.setHeight("600px");

		initWidget(layout);

	}

	/**
	 * @return the explorer Pane.
	 */
	public ExplorerPane getExplorerPane() {
		return explorerPane;
		// return null;
	}

	/**
	 * @return the explorer Pane.
	 */
//	public ContentPane getContentPane() {
//		return contentPane;
//	}

	protected void onWindowResizedImpl(int width) {
		int explorerWidth = this.getExplorerPane().getOffsetWidth();
		int contentWidth = Math.max(width - explorerWidth, 1);
		layout.setCellWidth(explorerPane, explorerWidth + "px");
//		layout.setCellWidth(contentPane, contentWidth + "px");
		layout.setCellWidth(contentPaneLayout, contentWidth + "px");
		onContentPaneWindowResizedImpl(contentWidth);
	}
	
//	public static class ContentPane extends Composite {
//
//		/**
//		 * The base style name.
//		 */
//		private static final String DEFAULT_STYLE_NAME = "Application";
//
//		/**
//		 * The decorator around the detail view.
//		 */
//		private DecoratorPanel contentPaneLayout;
//		/**
//		 * The main wrapper around the detail view.
//		 */
//		private Grid contentLayout;
//		// private VerticalPanel contentLayout;
//		/**
//		 * The wrapper around the detail view.
//		 */
//		private SimplePanel contentWrapper;
//
//		public ContentPane() {
//
//			// Setup detail view
//			initContentPane();
//			initWidget(contentPaneLayout);
//		}

		/**
		 * @return the {@link Widget} in the content area
		 */
		public Widget getContent() {
			return contentWrapper.getWidget();
		}

		/**
		 * Set the {@link Widget} to display in the content area.
		 * 
		 * @param content
		 *            the content widget
		 */
		public void setContent(Widget content) {
			Widget myContent = content;
			if (content == null) {
				myContent = new HTML("&nbsp;");
			}
			contentWrapper.setWidget(myContent);
		}
		/**
		 * Set the title of the content area.
		 * 
		 * @param title
		 *            the content area title
		 */
		public void setContentTitle(String title) {
			contentTitle.setText(title);
		}

//		private void initContentPane() {
//			// Setup the content layout
//			contentLayout = new Grid(2, 1);
//			contentLayout.setCellPadding(0);
//			contentLayout.setCellSpacing(0);
//			// detailViewLayout.setSize("100%", "600px");
//			contentLayout.setHeight("600px");
//
//			contentPaneLayout = new DecoratorPanel();
//			contentPaneLayout.setWidget(contentLayout);
//			contentPaneLayout.addStyleName(DEFAULT_STYLE_NAME + "-content-decorator");
//			contentPaneLayout.getElement().setAttribute("align", "RIGHT");
//
//			CellFormatter formatter = contentLayout.getCellFormatter();
//
//			// Add the content title
//			// setContentTitle(new HTML("Content"));
//			contentTitle = new Label("Content");
//			contentLayout.setWidget(0, 0, contentTitle);
//			formatter.setStyleName(0, 0, DEFAULT_STYLE_NAME + "-content-title");
//
//			formatter.setHeight(1, 0, "100%");
//			// Add the content wrapper
//			contentWrapper = new SimplePanel();
//			contentWrapper.setHeight("100%");
//			contentLayout.setWidget(1, 0, contentWrapper);
//			formatter.setStyleName(1, 0, DEFAULT_STYLE_NAME
//					+ "-content-wrapper");
//
//			// detailViewLayout.getWidget(0, 0)
//			// formatter.setHeight(1,0,"100%");
//			ContentPanel content = new ContentPanel();
//			setContent(content);
//		}
		
		private void initContentPane() {
			// Setup the content layout
			contentLayout = new VerticalPanel();
			// detailViewLayout.setSize("100%", "600px");
			contentLayout.setHeight("600px");

			contentPaneLayout = new DecoratorPanel();
			contentPaneLayout.setWidget(contentLayout);
			contentPaneLayout.addStyleName(DEFAULT_STYLE_NAME + "-content-decorator");
			contentPaneLayout.getElement().setAttribute("align", "RIGHT");

			// Add the content title
			// setContentTitle(new HTML("Content"));
			contentTitle = new Label("Content");
			contentLayout.add(contentTitle);
			contentTitle.setStyleName(DEFAULT_STYLE_NAME + "-content-title");
			contentLayout.setCellWidth(contentTitle, "100%");	
			
			// Add the content wrapper
			contentWrapper = new SimplePanel();
			contentWrapper.setHeight("100%");
			contentLayout.add(contentWrapper);
			contentWrapper.setStyleName(DEFAULT_STYLE_NAME + "-content-wrapper");
			
			contentLayout.setCellHeight(contentWrapper, "100%");
			contentLayout.setCellWidth(contentWrapper, "100%");			

			// detailViewLayout.getWidget(0, 0)
			// formatter.setHeight(1,0,"100%");
			ContentPanel content = new ContentPanel();
			setContent(content);
		}
		
		
//		public void onContentPaneWindowResizedImpl(int contentWidth) {
//			int contentWidthInner = Math.max(contentWidth - 10, 1);
//			contentLayout.getCellFormatter().setWidth(0, 0,
//					contentWidthInner + "px");
//			contentLayout.getCellFormatter().setWidth(1, 0,
//					contentWidthInner + "px");
//		}
		
		public void onContentPaneWindowResizedImpl(int contentWidth) {
			int contentWidthInner = Math.max(contentWidth-10, 1);
			contentLayout.setCellWidth(contentWrapper, contentWidthInner + "px");
			contentLayout.setCellWidth(contentTitle, contentWidthInner + "px");
			Widget panel = this.getContent();
			if ( panel != null && panel instanceof ContentPanel) {
				ContentPanel cPanel = (ContentPanel) panel;
				int height = 600;
				cPanel.adjustSize();
			}
		}		
		
//	}

}
