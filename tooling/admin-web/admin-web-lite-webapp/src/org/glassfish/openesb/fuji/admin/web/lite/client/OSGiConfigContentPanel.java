/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiConfigInfo;

/**
*
* @author chikkala
*/
public class OSGiConfigContentPanel extends ContentPanel {
	
	public OSGiConfigContentPanel() {
		super();
	}

	@Override
	protected DetailTabPanel createDetailTabPanel() {
		return new OSGiConfigDetailTabPanel();
	}

	@Override
	protected OverviewPanel createOverviewPanel() {
		return new OSGiConfigOverviewPanel();
	}
	
	protected static OSGiConfigInfo getOSGiConfigInfo(Object userObj) {
		OSGiConfigInfo info = null;
		if ( userObj != null && userObj instanceof OSGiConfigInfo) {
			info = (OSGiConfigInfo)userObj;
		}
		return info;
	}
	
	public static List<Property> createOverviewProperties(OSGiConfigInfo info) {
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("Pid", info.getPid(), "PID", "Configuration PID"));
    	propList.add(new Property.SimpleProperty("Bundle", info.getFactoryPid(), "Factory PID", "Factory PID"));
    	propList.add(new Property.SimpleProperty("BundleLocation", info.getBundleLocation(), "Bundle Location", "Bundle Location"));
		return propList;
	}

    public static PropertySheetSet createOSGiConfigInfoSet(OSGiConfigInfo info) {
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("Pid", info.getPid(), "PID", "Configuration PID"));
    	propList.add(new Property.SimpleProperty("Bundle", info.getFactoryPid(), "Factory PID", "Factory PID"));
    	propList.add(new Property.SimpleProperty("BundleLocation", info.getBundleLocation(), "Bundle Location", "Bundle Location"));
    	
    	PropertySheetSet sheetSet = new PropertySheetSet("osgiConfigInfo", "OSGi Configuration", "OSGi Configuration");
    	sheetSet.addOSGiProperties(info.getProperties());
    	return sheetSet;
    }
    
    public static PropertySheetSet createConfigurationPropertiesSet(OSGiConfigInfo info) {
    	PropertySheetSet sheetSet = new PropertySheetSet("osgiConfigProperties", "Configuration", "Configuration");
    	sheetSet.addOSGiProperties(info.getProperties());
    	return sheetSet;
    }
    
	public static class OSGiConfigOverviewPanel extends OverviewPanel {
		public OSGiConfigOverviewPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			
			OSGiConfigInfo info = OSGiConfigContentPanel.getOSGiConfigInfo(userObj);
	    	List<Property> propList = new ArrayList<Property>();
	    	if ( info != null) {
	    		propList = createOverviewProperties(info);
	    	}
	    	setProperties(propList);
		}		
	}
	
	public static class OSGiConfigDetailTabPanel extends DetailTabPanel {

		public OSGiConfigDetailTabPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			OSGiConfigInfo info = OSGiConfigContentPanel.getOSGiConfigInfo(userObj);
			this.clearTabs();
			if ( info == null ) {
				return;
			}	    	
	    	PropertySheetSet set1 = OSGiConfigContentPanel.createConfigurationPropertiesSet(info);  	
	    	this.addTab(new TabContentPanel(set1));
	    	this.selectTab(set1.getId());			
		}
		
	}

}
