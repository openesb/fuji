/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiServiceInfo;
/**
*
* @author chikkala
*/
public class OSGiServiceContentPanel extends ContentPanel {
	
	public OSGiServiceContentPanel() {
		super();
	}

	@Override
	protected DetailTabPanel createDetailTabPanel() {
		return new OSGiServiceDetailTabPanel();
	}

	@Override
	protected OverviewPanel createOverviewPanel() {
		return new OSGiServiceOverviewPanel();
	}
	
	protected static OSGiServiceInfo getOSGiServiceInfo(Object userObj) {
		OSGiServiceInfo info = null;
		if ( userObj != null && userObj instanceof OSGiServiceInfo) {
			info = (OSGiServiceInfo)userObj;
		}
		return info;
	}
	
	public static List<Property> createOverviewProperties(OSGiServiceInfo info) {
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("Id", info.getId(), "Service ID", "Service ID"));
    	propList.add(new Property.SimpleProperty("Bundle", info.getBundle(), "Bundle", "Bundle"));
    	propList.add(new Property.SimpleProperty("Description", info.getDescription(), "Description", "Description"));
    	propList.add(new Property.SimpleProperty("ObjectClasses", info.getObjectClasses(), "Service Objects", "Service Objects"));
    	propList.add(new Property.SimpleProperty("UsingBundles", info.getUsingBundles(), "Using Bundles", "Using Bundles"));		
		return propList;
	}

    public static PropertySheetSet createOSGiServiceInfoSet(OSGiServiceInfo info) {
    	
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("Id", info.getId(), "Service ID", "Service ID"));
    	propList.add(new Property.SimpleProperty("Bundle", info.getBundle(), "Bundle", "Bundle"));
    	propList.add(new Property.SimpleProperty("Description", info.getDescription(), "Description", "Description"));
    	propList.add(new Property.SimpleProperty("ObjectClasses", info.getObjectClasses(), "Service Objects", "Service Objects"));
    	propList.add(new Property.SimpleProperty("UsingBundles", info.getUsingBundles(), "Using Bundles", "Using Bundles"));		
   	
    	PropertySheetSet sheetSet = new PropertySheetSet("osgiService", "OSGi Service", "OSGi Service");
    	sheetSet.addProperties(propList);
    	return sheetSet;
    }
    
    public static PropertySheetSet createOSGiServicePropertiesSet(OSGiServiceInfo info) {
    	PropertySheetSet sheetSet = new PropertySheetSet("osgiServiceProperties", "OSGi Service Properties", "OSGi Service Properties");
    	sheetSet.addOSGiProperties(info.getProperties());
    	return sheetSet;
    }
    
	public static class OSGiServiceOverviewPanel extends OverviewPanel {
		public OSGiServiceOverviewPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			
			OSGiServiceInfo info = getOSGiServiceInfo(userObj);
	    	List<Property> propList = new ArrayList<Property>();
	    	if ( info != null) {
	    		propList = createOverviewProperties(info);
	    	}
	    	setProperties(propList);
		}		
	}
	
	public static class OSGiServiceDetailTabPanel extends DetailTabPanel {

		public OSGiServiceDetailTabPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			OSGiServiceInfo info = OSGiServiceContentPanel.getOSGiServiceInfo(userObj);
			this.clearTabs();
			if ( info == null ) {
				return;
			}	    	
	    	PropertySheetSet set1 = OSGiServiceContentPanel.createOSGiServicePropertiesSet(info);  	
	    	this.addTab(new TabContentPanel(set1));
	    	this.selectTab(set1.getId());			
		}
		
	}

}
