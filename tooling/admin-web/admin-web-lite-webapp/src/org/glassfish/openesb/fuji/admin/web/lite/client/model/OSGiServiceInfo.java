/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client.model;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;
import java.util.Map;

/**
 * Bean that represents the osgi service info and has a
 * serialization/deserialization code to/from jmx open types that represent the
 * data.
 * 
 * @author chikkala
 */
public class OSGiServiceInfo implements IsSerializable {
	// property names
	public static final String PROP_BUNDLE = "bundle";
	public static final String PROP_ID = "id";
	public static final String PROP_DESCRIPTION = "description";
	public static final String PROP_OBJECT_CLASSES = "objectClasses";
	public static final String PROP_USING_BUNDLES = "usingBundles";
	public static final String PROP_PROPERTIES = "properties";
	// property values
	private String bundle;
	private String id;
	private String description;
	private String[] objectClasses;
	private String[] usingBundles;
	private OSGiPropertyInfoMap properties;

	public OSGiServiceInfo() {
		this.objectClasses = new String[0];
		this.usingBundles = new String[0];
		this.properties = new OSGiPropertyInfoMap();
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String[] getObjectClasses() {
		return objectClasses;
	}

	public void setObjectClasses(String[] objectClasses) {
		if (objectClasses != null) {
			this.objectClasses = objectClasses;
		}
	}

	public String[] getUsingBundles() {

		return usingBundles;
	}

	public void setUsingBundles(String[] usingBundles) {
		if (usingBundles != null) {
			this.usingBundles = usingBundles;
		}
	}

	public List<OSGiPropertyInfo> getProperties() {
		return this.properties.getProperties();
	}

	public void setProperties(List<OSGiPropertyInfo> properties) {
		this.properties.setProperties(properties);
	}

	public Map<String, OSGiPropertyInfo> getPropertiesMap() {
		return this.properties.getPropertiesMap();
	}

	public OSGiPropertyInfo getProperty(String name) {
		return this.properties.getProperty(name);
	}

	public String getPropertyValue(String name) {
		return this.properties.getPropertyValue(name);
	}
}
