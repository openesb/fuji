/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiBundleInfo;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
/**
*
* @author chikkala
*/
public class JBIBundleContentPanel extends ContentPanel {
	
	public JBIBundleContentPanel() {
		super();
	}

	@Override
	protected DetailTabPanel createDetailTabPanel() {
		return new JBIBundleDetailTabPanel();
	}

	@Override
	protected OverviewPanel createOverviewPanel() {
		return new JBIBundleOverviewPanel();
	}
	
	protected static OSGiBundleInfo getOSGiBundleInfo(Object userObj) {
		OSGiBundleInfo info = null;
		if ( userObj != null && userObj instanceof OSGiBundleInfo) {
			info = (OSGiBundleInfo)userObj;
		}
		return info;
	}
	public static List<Property> createOverviewProperties(OSGiBundleInfo info) {
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("JbiArtifactType", info.getJbiArtifactType(), "JBI Artifact", "JBI Artifact"));
    	propList.add(new Property.SimpleProperty("SymbolicName", info.getSymbolicName(), "Name", "Name"));
    	propList.add(new Property.SimpleProperty("State", info.getState(), "State", "State"));
//    	propList.add(new Property.SimpleProperty("JbiDescription", info.getJbiDescription(), "Description", "Description"));
    	return propList;
	}	
	
	public static class JBIBundleOverviewPanel extends OverviewPanel {
		private OSGiBundleLifecycleActions actions;
		
		public JBIBundleOverviewPanel() {
			super();
		}
		private OSGiBundleLifecycleActions getLifecycleActions() {
			if ( this.actions == null ) {
				this.actions = new OSGiBundleLifecycleActions();
			}
			return this.actions;
		}
		
		@Override
		protected Button[] createActions() {
			return getLifecycleActions().createActions();
		}

		@Override
		public void updateContent(Object userObj) {
			
			OSGiBundleInfo info = getOSGiBundleInfo(userObj);
			getLifecycleActions().setOSGiBundleInfo(info);
	    	List<Property> propList = new ArrayList<Property>();
	    	if ( info != null ) {
	    		propList = JBIBundleContentPanel.createOverviewProperties(info);
	    	}
	    	setProperties(propList);
		}		
	}
	
	public static class JBIBundleDetailTabPanel extends DetailTabPanel {

		public JBIBundleDetailTabPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			OSGiBundleInfo info = getOSGiBundleInfo(userObj);
			this.clearTabs();
			if ( info == null ) {
				return;
			}	    	
	    	PropertySheetSet set1 = OSGiBundleContentPanel.createOSGiBundleInfoSet(info);
	    	PropertySheetSet set2 = OSGiBundleContentPanel.createOSGiBundleHeaderSet(info);
	    	
	    	
//	    	ScrollPanel jbiDescSP = new ScrollPanel();
//	    	Label jbiDescLBL = new Label(info.getJbiDescriptor());
//	    	HTML jbiDescLBL = new HTML(info.getJbiDescriptor());
	    	String fmtJbiDesc = info.getFormattedJbiDescriptor();
	    	if ( fmtJbiDesc == null ) {
	    		fmtJbiDesc = XMLFormatter.fmtXMLText(info.getJbiDescriptor());
	    		info.setFormattedJbiDescriptor(fmtJbiDesc);
	    	}
	    	
//	    	HTML jbiDescLBL = new HTML(fmtJbiDesc);
//	    	SimplePanel jbiDescPanel = new SimplePanel();
//	    	jbiDescPanel.setWidget(jbiDescLBL);
	    	
	    	HTMLPanel jbiDescPanel = new HTMLPanel(fmtJbiDesc);
	    	
//	    	jbiDescSP.setWidget(jbiDescLBL);
//	    	jbiDescLBL.setStyleName("xml-view");
	    	
	    	TabContentPanel jbiDescTab = new JBIDescriptorTabContentPanel(jbiDescPanel);
	    	
	    	this.addTab(jbiDescTab);
	    	this.addTab(new TabContentPanel(set1));
	    	this.addTab(new TabContentPanel(set2));
	    	
	    	this.selectTab(set1.getId());		
	    	// this.selectTab(jbiDescTab.getTabId());
		}
		
	}
	
	public static class JBIDescriptorTabContentPanel extends TabContentPanel {
		public JBIDescriptorTabContentPanel(Widget widget) {
			super(widget, "JBIDescriptor", "JBI Descriptor", "JBI Descriptor");
		}
	}
	
	

}
