/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.server;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.TabularData;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInstanceInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInterceptorInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiBundleInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiConfigInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiServiceInfo;
import org.glassfish.openesb.fuji.admin.web.lite.server.jmx.JMXManager;
import org.glassfish.openesb.fuji.admin.web.lite.server.jmx.OSGiBundleInfoJMXSerializer;
import org.glassfish.openesb.fuji.admin.web.lite.server.jmx.OSGiConfigInfoJMXSerializer;
import org.glassfish.openesb.fuji.admin.web.lite.server.jmx.OSGiPropertyInfoJMXSerializer;
import org.glassfish.openesb.fuji.admin.web.lite.server.jmx.OSGiServiceInfoJMXSerializer;

/**
 * 
 * @author chikkala
 */
public class FujiInstanceMgr {

    protected static final Logger LOG = Logger.getLogger(FujiInstanceMgr.class
            .getName());

    // private PropertyChangeSupport mPropSupport;

    private JMXManager mJmxMgr;

    // private FujiInstanceInfo mFujiInstanceInfo;

    public FujiInstanceMgr() {
        this.mJmxMgr = new JMXManager();
        // this.mFujiInstanceInfo = new FujiInstanceInfo();
    }

    // public FujiInstanceInfo getFujiInstanceInfo() {
    // return mFujiInstanceInfo;
    // }

    public JMXManager getJMXManager() {
        return this.mJmxMgr;
    }

    public void setPlatformMBeanServer(MBeanServerConnection mbs) {
        this.mJmxMgr.setUsePlatformServer(true);
        this.mJmxMgr.setMBeanServerConnection(mbs);

    }

    public void setRemoteMBeanServer(String host, String port, String username,
            String password) {
        this.mJmxMgr.setUsePlatformServer(false);
        this.mJmxMgr.setHost(host);
        this.mJmxMgr.setPort(port);
        this.mJmxMgr.setUsername(username);
        this.mJmxMgr.setPassword(password);
    }

    // public FujiInstanceInfo loadInstance(String host, String port,
    // String username, String password) {
    //
    // FujiInstanceInfo info = new FujiInstanceInfo();
    // info.setAdminHost(host);
    // info.setAdminPort(port);
    // info.setAdminUser(username);
    // info.setAdminPassword(password);
    //
    // this.mJmxMgr.setHost(host);
    // this.mJmxMgr.setPort(port);
    // this.mJmxMgr.setUsername(username);
    // this.mJmxMgr.setPassword(password);
    //
    // updateFujiInstanceInfo(info);
    //
    // return info;
    // }

    public FujiInstanceInfo loadInstance() {
        // this.mJmxMgr.testMBeanServer();
        // if (!this.mJmxMgr.isJMXAdminAvailable()) {
        // LOG.info("### FUJI JMX ADMIN NOT AVAILABLE....");
        // }
        FujiInstanceInfo info = new FujiInstanceInfo();
        if (this.mJmxMgr.isUsePlatformServer()) {
            info.setAdminHost(null);
            info.setAdminPort(null);
            info.setAdminUser(null);
            info.setAdminPassword(null);
        } else {
            info.setAdminHost(this.mJmxMgr.getHost());
            info.setAdminPort(this.mJmxMgr.getPort());
            info.setAdminUser(this.mJmxMgr.getUsername());
            info.setAdminPassword(this.mJmxMgr.getPassword());
        }
        updateFujiInstanceInfo(info);
        return info;
    }

    private void updateFujiInstanceInfo(FujiInstanceInfo info) {
        List<OSGiBundleInfo> allBundles = getBundleList();
        List<OSGiServiceInfo> allServices = getServiceRefs();
        List<OSGiConfigInfo> allConfigs = getConfigurations();

        List<OSGiBundleInfo> engineList = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> bindingList = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> slibList = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> assemblyList = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> otherList = new ArrayList<OSGiBundleInfo>();
        List<FujiInterceptorInfo> interceptorList = new ArrayList<FujiInterceptorInfo>();

        splitBundleList(allBundles, engineList, bindingList, slibList,
                assemblyList, otherList);
        interceptorList = listInterceptors(allServices, allConfigs);

        info.setEngineList(engineList);
        info.setBindingList(bindingList);
        info.setSLibList(slibList);
        info.setAssemblyList(assemblyList);
        info.setBundleList(otherList);
        info.setInterceptorList(interceptorList);

        info.setServiceList(allServices);
        info.setConfigList(allConfigs);
    }

    private List<OSGiBundleInfo> getBundleList() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        TabularData data = null;
        try {
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName,
                    "listBundles");
            data = (TabularData) result;
        } catch (Exception ex) {
            logException(ex);
        }
        if (data != null) {
            try {
                list = OSGiBundleInfoJMXSerializer.toOSGiBundleInfoList(data);
            } catch (OpenDataException ex) {
                logException(ex);
            }
        }
        return list;
    }

    private List<OSGiServiceInfo> getServiceRefs() {
        List<OSGiServiceInfo> list = new ArrayList<OSGiServiceInfo>();
        TabularData data = null;
        try {
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName,
                    "listServiceRefs");
            data = (TabularData) result;
        } catch (Exception ex) {
//            LOG.info(ex.getMessage());
            logException(ex);
        }
        if (data != null) {
            try {
                list = OSGiServiceInfoJMXSerializer.toOSGiServiceInfoList(data);
            } catch (OpenDataException ex) {
                ex.printStackTrace();
            }
        }
        return list;
    }

    private List<OSGiConfigInfo> getConfigurations() {
        List<OSGiConfigInfo> list = new ArrayList<OSGiConfigInfo>();
        TabularData data = null;
        try {
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName,
                    "listConfigurations");
            data = (TabularData) result;
        } catch (Exception ex) {
//            LOG.info(ex.getMessage());
            logException(ex);
        }
        if (data != null) {
            try {
                list = OSGiConfigInfoJMXSerializer.toOSGiConfigInfoList(data);
            } catch (OpenDataException ex) {
                ex.printStackTrace();
            }
        }
        return list;
    }

    public void splitBundleList(List<OSGiBundleInfo> all,
            List<OSGiBundleInfo> engineList, List<OSGiBundleInfo> bindingList,
            List<OSGiBundleInfo> slibList, List<OSGiBundleInfo> assemblyList,
            List<OSGiBundleInfo> otherList) {
        engineList.clear();
        bindingList.clear();
        slibList.clear();
        assemblyList.clear();
        otherList.clear();
        for (OSGiBundleInfo info : all) {

            if (OSGiBundleInfo.JBI_UNKNOWN.equals(info.getJbiArtifactType())) {
                otherList.add(info);
            } else if (OSGiBundleInfo.JBI_SE.equals(info.getJbiArtifactType())) {
                engineList.add(info);
            } else if (OSGiBundleInfo.JBI_BC.equals(info.getJbiArtifactType())) {
                bindingList.add(info);
            } else if (OSGiBundleInfo.JBI_SL.equals(info.getJbiArtifactType())) {
                slibList.add(info);
            } else if (OSGiBundleInfo.JBI_SA.equals(info.getJbiArtifactType())) {
                assemblyList.add(info);
            } else {
                otherList.add(info);
            }
        }
    }

    public List<OSGiBundleInfo> listBundles(List<OSGiBundleInfo> all,
            String type) {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        for (OSGiBundleInfo info : all) {
            if (type != null && type.equals(info.getJbiArtifactType())) {
                list.add(info);
            }
        }
        return list;
    }

    public List<FujiInterceptorInfo> listInterceptors(
            List<OSGiServiceInfo> serviceList, List<OSGiConfigInfo> configList) {
        List<FujiInterceptorInfo> list = new ArrayList<FujiInterceptorInfo>();
        list = FujiInterceptorInfo.createFujiInterceptorInfoList(serviceList,
                configList);
        return list;
    }

    public String installBundle(String bundlePath) throws Exception {
        return installBundle(bundlePath, true, true);
    }

    public String installBundle(String bundlePath, boolean start,
            boolean reInstall) throws Exception {
        LOG.info("Installing Bundle " + bundlePath + "With start= " + start
                + " reInstall=" + reInstall);
        Object result = null;

        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "installBundle",
                bundlePath, start, reInstall);

        return (String) result;
    }

    public String startBundle(Long bundleId) throws Exception {
        // Long bundleId = Long.valueOf(info.getBundleId());
        LOG.info("Starting Bundle " + bundleId);
        Object result = null;

        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "startBundle",
                bundleId);

        return (String) result;
    }

    public String stopBundle(Long bundleId) throws Exception {
        // Long bundleId = Long.valueOf(info.getBundleId());
        LOG.info("Stoping Bundle " + bundleId);
        Object result = null;

        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "stopBundle",
                bundleId);

        return (String) result;
    }

    public String uninstallBundle(Long bundleId) throws Exception {
        // Long bundleId = Long.valueOf(info.getBundleId());
        LOG.info("Uninstall Bundle " + bundleId);
        Object result = null;

        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName,
                "uninstallBundle", bundleId);

        return (String) result;
    }

    public boolean isJMXAdminAvailable() {
        return this.mJmxMgr.isJMXAdminAvailable();
    }

    public boolean shutdownRemoteInstance() {
        boolean shutdown = false;
        if (isJMXAdminAvailable()) {
            try {
                ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
                this.mJmxMgr.invokeMBeanOperation(mbeanOName, "shutdown");
                shutdown = true; // wait for some time.

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return shutdown;
    }

    public String updateConfiguration(String pid, List<OSGiPropertyInfo> props)
            throws Exception {
        LOG.info("updating OSGi Cofiguration...");
        javax.management.openmbean.TabularData data = null;

        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        data = OSGiPropertyInfoJMXSerializer.toTabularData(props);
        Object[] params = { pid, data };
        String[] signature = { "java.lang.String",
                "javax.management.openmbean.TabularData" };
        Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName,
                "updateConfiguration", params, signature);

        return pid;
    }

    public String updateConfigProperty(String pid, String name, String value)
            throws Exception {

        String result = null;
        OSGiPropertyInfo updateProp = new OSGiPropertyInfo(name, value);
        List<OSGiPropertyInfo> updatePropList = new ArrayList<OSGiPropertyInfo>();
        updatePropList.add(updateProp);
        LOG.info("Updating config prop name=" + updateProp.getName()
                + " value=" + updateProp.getValue());
        try {
            result = updateConfiguration(pid, updatePropList);
        } catch (Exception ex) {
            logException(ex);
        }
        return result;
    }
    
    private void logException(Exception ex) {
        LOG.log(Level.FINE, ex.getMessage(), ex);
    }
}
