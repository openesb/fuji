/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInterceptorInfo;
/**
*
* @author chikkala
*/
public class FujiInterceptorContentPanel extends ContentPanel {
	
	public FujiInterceptorContentPanel() {
		super();
	}

	@Override
	protected DetailTabPanel createDetailTabPanel() {
		return new FujiInterceptorDetailTabPanel();
	}

	@Override
	protected OverviewPanel createOverviewPanel() {
		return new FujiInterceptorOverviewPanel();
	}
	
	protected static FujiInterceptorInfo getFujiInterceptorInfo(Object userObj) {
		FujiInterceptorInfo info = null;
		if ( userObj != null && userObj instanceof FujiInterceptorInfo) {
			info = (FujiInterceptorInfo)userObj;
		}
		return info;
	}
	
	public static List<Property> createOverviewProperties(FujiInterceptorInfo info) {
		List<Property> propList = new ArrayList<Property>();
    	propList.add(new Property.SimpleProperty("DisplayName", info.getDisplayName(), "Name", "Display Name"));
    	return propList;
	}
	
    public static PropertySheetSet createFujiInterceptorConfigurationSet(FujiInterceptorInfo info) {
    	PropertySheetSet sheetSet = new PropertySheetSet("interceptorConfiguration", "Configuration", "Interceptor Configuration");
    	sheetSet.addOSGiProperties(info.getConfigInfo().getProperties());
    	return sheetSet;
    }
    
	public static class FujiInterceptorOverviewPanel extends OverviewPanel {
		public FujiInterceptorOverviewPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			
			FujiInterceptorInfo info = FujiInterceptorContentPanel.getFujiInterceptorInfo(userObj);
	    	List<Property> propList = new ArrayList<Property>();
	    	if ( info != null) {
	    		propList = createOverviewProperties(info);
	    	}
	    	setProperties(propList);
		}		
	}
	
	public static class FujiInterceptorDetailTabPanel extends DetailTabPanel {

		public FujiInterceptorDetailTabPanel() {
			super();
		}

		@Override
		public void updateContent(Object userObj) {
			FujiInterceptorInfo info = FujiInterceptorContentPanel.getFujiInterceptorInfo(userObj);
			this.clearTabs();
			if ( info == null ) {
				return;
			}	    	
	    	PropertySheetSet set1 = FujiInterceptorContentPanel.createFujiInterceptorConfigurationSet(info);  	
	    	PropertySheetSet set2 = OSGiServiceContentPanel.createOSGiServiceInfoSet(info.getServiceInfo());
	    	PropertySheetSet set3 = OSGiServiceContentPanel.createOSGiServicePropertiesSet(info.getServiceInfo());
	    	this.addTab(new TabContentPanel(set1));
	    	this.selectTab(set1.getId());			
		}
		
	}

}
