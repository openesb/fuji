/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.Arrays;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;
/**
*
* @author chikkala
*/
public class Property {
	private String name;
	private String displayName;
	private String shortDescription;
	
	@SuppressWarnings("unused")
	private Property() {}
	
	protected Property(String name, String displayName, String shortDescription) {
		this.name = name;
		if ( displayName == null || displayName.trim().length() == 0) {
			this.displayName = name;
		} else {
			this.displayName = displayName;
		}
		if ( shortDescription == null || shortDescription.trim().length() == 0) {
			this.shortDescription = name;
		} else {
			this.shortDescription = shortDescription;
		}
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return "";
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public static class OSGiProperty extends Property {
		private OSGiPropertyInfo info;
		public OSGiProperty(OSGiPropertyInfo info) {
			this(info, info.getName(), info.getName());
		}
		public OSGiProperty(OSGiPropertyInfo info, String displayName, String shortDescription) {
			super(info.getName(), displayName, shortDescription);
			this.info = info;
		}
		public String getValue() {
			return info.getValue();
		}
	}
	
	public static class SimpleProperty extends Property {
		private String value;
		public SimpleProperty(String name, Object value) {
			this(name, value, name, name);
		}
		public SimpleProperty(String name, Object value, String displayName, String shortDescription) {
			super(name, displayName, shortDescription);
			this.value = "";
	        if (value != null) {
	            Class clazz = value.getClass();
	            if (clazz.isArray()) {
	                this.value = Arrays.toString((Object[]) value);
	            } else {
	                this.value = value.toString();
	            }
	        }			
		}
		public String getValue() {
			return value;
		}
	}	
	
}
