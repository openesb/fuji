/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiBundleInfo;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
/**
*
* @author chikkala
*/
public class OSGiBundleLifecycleActions {
	private OSGiBundleInfo info;
	
	protected void selectBundle() {
		if ( FujiAdmin.getFujiAdmin() != null ) {
			String bundleId = info.getBundleId();
			String tkn = "OSGiBundleInfo_" + bundleId;
			FujiAdmin.getFujiAdmin().loadFujiInstance(tkn);
		} else {
			Window.alert("Fuji Admin reference is NULL");
		}		
	}
	protected void startBundle() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do something with errors
				Window.alert("Error Starting " + caught.getMessage());
			}

			public void onSuccess(String result) {
				// update the watch list FlexTable
//				Window.alert("Start Bundle Result:" + result);
				selectBundle();
			}
		};		
		FujiAdmin.getFujiAdminService().startBundle(info.getBundleId(), callback);
	}
	
	protected void stopBundle() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				//TODO: do something with errors
				Window.alert("Error Stopping " + caught.getMessage());
			}

			public void onSuccess(String result) {
				// update the watch list FlexTable
//				Window.alert("Stop Bundle Result:" + result);
				selectBundle();
			}
		};		
		FujiAdmin.getFujiAdminService().stopBundle(info.getBundleId(), callback);			
	}
	
	protected void uninstallBundle() {
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do something with errors
				Window.alert("Error Uninstalling " + caught.getMessage());
			}

			public void onSuccess(String result) {
				// update the watch list FlexTable
//				Window.alert("Uninstall Bundle Result:" + result);
				selectBundle();
			}
		};		
		FujiAdmin.getFujiAdminService().uninstallBundle(info.getBundleId(), callback);			
	}	
	
	public Button[] createActions() {
        Button startButton = new Button("Start", new ClickHandler() {
            public void onClick(ClickEvent event) {
              // Window.alert("Start Bundle");
              startBundle();
            }
          });
          
        Button stopButton = new Button("Stop", new ClickHandler() {
            public void onClick(ClickEvent event) {
              // Window.alert("Stop Bundle");
              stopBundle();
            }
          });
        Button uninstallButton = new Button("Uninstall", new ClickHandler() {
            public void onClick(ClickEvent event) {
              // Window.alert("Uninstall Bundle");
              uninstallBundle();
            }
          });
        Button[] actions = new Button[3];
        actions[0] = startButton;
        actions[1] = stopButton;
        actions[2] = uninstallButton;
        
        return actions;
	}
	
	public OSGiBundleInfo getOSGiBundleInfo() {
		return this.info;
	}
	
	public void setOSGiBundleInfo(OSGiBundleInfo info) {
		this.info = info;
	}

}
