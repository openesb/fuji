/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;

import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DecoratedTabPanel;
import com.google.gwt.user.client.ui.SimplePanel;
/**
*
* @author chikkala
*/
public class DetailTabPanel extends SimplePanel {
	
	public static final String DEFAULT_STYLE_NAME = "Application";
	
	private DecoratedTabPanel tabPanel; 
	private ArrayList<TabContentPanel> tabs;
	private ArrayList<String> tabIndex;
 	public DetailTabPanel() {
		super();
		initComponents();
	}
	
	private void initComponents() {
		tabs = new ArrayList<TabContentPanel>();
		tabIndex = new ArrayList<String>();
		tabPanel = new DecoratedTabPanel();
		// tabPanel.setAnimationEnabled(true);
		
		
		this.setWidget(tabPanel);
		
		this.setStyleName(DEFAULT_STYLE_NAME + "-content-wrapper");
		
		tabPanel.setStyleName("Application-detail-tab-panel");
		tabPanel.setHeight("100%");
		tabPanel.setWidth("100%");
		tabPanel.getDeckPanel().setHeight("100%");
		tabPanel.getDeckPanel().setWidth("100%");
		
//		this.tabPanel.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
//
//			public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {
//				adjustTabContentSize(event.getItem());
//			}			
//		});
		
	}
	
//	public void adjustTabContentSize(int idx) {
//		// Window.alert("Before select: adjustSize " + idx);
//    	int width = Math.max(tabPanel.getDeckPanel().getOffsetWidth() - 10, 1);
//    	int height = Math.max(tabPanel.getDeckPanel().getOffsetHeight() - 10, 1);
//    	// Window.alert("DetailTabPanel:adjustSize width:" + width  + " Height: " + height);		
//		TabContentPanel tab = (TabContentPanel)tabPanel.getWidget(idx);
////		tab.adjustSize(width, height);
//		tab.adjustSize();
//	}
	
//	public void adjustSize() {
//	   	int width = Math.max(tabPanel.getDeckPanel().getOffsetWidth() - 10, 1);
//    	int height = Math.max(tabPanel.getDeckPanel().getOffsetHeight() - 10, 1);		
//		int count = tabPanel.getWidgetCount();
//		for ( int i=0; i < count; ++i) {
//			TabContentPanel tab = (TabContentPanel)tabPanel.getWidget(i);
////			tab.adjustSize(Math.max(width-10,1), Math.max(height-10,1));	
//			tab.adjustSize();
//		}
//	}
	
	public void addTab(TabContentPanel tab) {
		tabPanel.add(tab, tab.getTabName());
		tabs.add(tab);
		tabIndex.add(tab.getTabId());
		// tab.adjustSize();
	}
	public void clearTabs() {
		tabPanel.clear();
		tabs.clear();
		tabIndex.clear();
	}
	public void selectTab(String tabId) {
		int i = tabIndex.indexOf(tabId);
		tabPanel.selectTab(i);
	}
	
	public void updateContent(Object userObj) {
		this.clearTabs();
	}
}
