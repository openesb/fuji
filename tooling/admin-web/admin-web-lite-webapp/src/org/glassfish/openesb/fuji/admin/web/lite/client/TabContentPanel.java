/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
*
* @author chikkala
*/
public class TabContentPanel extends Composite {

    /**
     * The default style name.
     */
	public static final String DEFAULT_STYLE_NAME = "Application";
    /**
     * The static loading image displayed when loading CSS or source code.
     */
    private ScrollPanel panel = null;
//    private VerticalPanel layout = null;

    private String tabId;
    private String tabName;
    private String tabDescription;

    /**
     * Constructor.
     *
     * @param constants the constants
     */
    public TabContentPanel() {
        this(new HTML("&nbsp;"), "tabConent", "Tab Content", "Tab Content");
    }
    public TabContentPanel(PropertySheetSet set) {
    	this(set, set.getId(), set.getName(), set.getDescription());
    }
    public TabContentPanel(Widget content, String id, String name, String description) {
        this.tabId = id;
        this.tabName = name;
        this.tabDescription = description;
        panel = new ScrollPanel();
//        layout = new VerticalPanel();
        // layout.setWidget(panel);
//        layout.add(panel);
        
        // panel.setHeight("600px");
        // layout.setSize("100%", "100%");
//        layout.setCellHeight(panel, "100%");
//        layout.setCellWidth(panel, "100%");
        // panel.setAlwaysShowScrollBars(true);
        // All composites must call initWidget() in their constructors.
//        initWidget(layout);
        initWidget(panel);
        
        // panel.setSize("300px", "300px");
        // Give the overall composite a style name.
        // setStyleName(DEFAULT_STYLE_NAME);
        this.setStyleName(DEFAULT_STYLE_NAME + "-content-wrapper");
        panel.setWidget(content);
        
    }
    public String getTabDescription() {
        return tabDescription;
    }

    public void setTabDescription(String description) {
        this.tabDescription = description;
    }

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String id) {
        this.tabId = id;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String name) {
        this.tabName = name;
    }

    public void setContent(Widget content) {
    	this.panel.setWidget(content);
    }
    
    public void adjustSize() {
//    	int width = layout.getOffsetWidth();
//    	int height = layout.getOffsetHeight();
////    	if ( width <= 0 ) {
////    		width = 600;
////    	}  
////    	if ( height <= 0 ) {
////    		height = 400;
////    	}   	
//    	if ( width > 0 && height > 0 ) {
////    		panel.setSize(width + "px", height + "px");
//    	}
    }
    
//    public void adjustSize(int width, int height) {
//    	// Window.alert("TabContentPanel:adjustSize width:" + width  + " Height: " + height);
//    	if ( width <= 0 ) {
//    		width = 600;
//    	}  
//    	if ( height <= 0 ) {
//    		height = 400;
//    	}   	
//    	panel.setSize(width + "px", height + "px");
//    }
    
}
