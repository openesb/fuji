/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import java.util.ArrayList;
import java.util.List;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.OSGiPropertyInfo;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;

/**
*
* @author chikkala
*/
public class PropertySheetSet extends Composite {

    /**
     * The default style name.
     */
    private static final String DEFAULT_STYLE_NAME = "property-sheet";
    private Grid grid = null;
    private List<Property> propList;

    private String id;
    private String name;
    private String description;

    /**
     * Constructor.
     *
     * @param constants the constants
     */
    public PropertySheetSet() {
        this("properties", "Properties", "Properties");
    }
    
    public PropertySheetSet(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.propList = new ArrayList<Property>();
        grid = new Grid(1, 1);
        grid.setBorderWidth(1);
//        grid.setCellPadding(1);
//        panel.add(grid);

        initialize();

        // All composites must call initWidget() in their constructors.
//        initWidget(panel);
        initWidget(grid);
        // Give the overall composite a style name.
        // setStyleName(DEFAULT_STYLE_NAME);
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addOSGiProperties(List<OSGiPropertyInfo> osgiPropList) {
    	List<Property> propList = new ArrayList<Property>();
    	
        for (OSGiPropertyInfo info : osgiPropList) {
        	propList.add(new Property.OSGiProperty(info));
        }
        addProperties(propList);
    }
    
    public void addProperties(List<Property> propList) {
    	this.propList.clear();
    	this.propList.addAll(propList);
    	initialize();
    }

    /**
     * Initialize this widget by creating the elements that should be added to the
     * page.
     */
    public final void initialize() {
        if (this.propList == null) {
            grid.clear();
            grid.resize(1, 1);
            grid.setText(0, 0, "No Properties");
        } else {
            grid.clear();
            grid.resize(propList.size(), 2);
            for (int i = 0; i < propList.size(); ++i) {
                Property info = propList.get(i);
                grid.setWidget(i, 0, new Label(info.getDisplayName()));
                grid.setWidget(i, 1, new MyLabel(info.getValue(), false, 40));
            }
        }
    }
}
