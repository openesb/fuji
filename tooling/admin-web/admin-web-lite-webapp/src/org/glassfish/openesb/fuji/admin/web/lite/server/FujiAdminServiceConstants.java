package org.glassfish.openesb.fuji.admin.web.lite.server;

public interface FujiAdminServiceConstants {
    String FUJI_INSTANCE_MGR_ID = "fuji.admin.web.lite.instance.mgr";
    String FUJI_PLATFORM_MBS_ID = "fuji.admin.web.lite.platform.mbs";
    String FUJI_ADMIN_HOST_ID = "fuji.admin.web.lite.admin.host";
    String FUJI_ADMIN_PORT_ID = "fuji.admin.web.lite.admin.port";
    String FUJI_ADMIN_USER_ID = "fuji.admin.web.lite.admin.user";
    String FUJI_ADMIN_PASSWORD_ID = "fuji.admin.web.lite.admin.password";
}
