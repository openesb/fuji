/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.server;

import java.util.logging.Logger;

import javax.management.MBeanServerConnection;

import org.glassfish.openesb.fuji.admin.web.lite.client.model.FujiInstanceInfo;
import org.glassfish.openesb.fuji.admin.web.lite.client.rpc.FujiAdminService;
import org.glassfish.openesb.fuji.admin.web.lite.server.FujiInstanceMgr;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 * 
 * @author chikkala
 */
@SuppressWarnings("serial")
public class FujiAdminServiceImpl extends RemoteServiceServlet implements
		FujiAdminService {
    protected static final Logger LOG = Logger.getLogger(FujiAdminServiceImpl.class.getName());
	// FujiInstanceMgr instanceMgr = null;

	private FujiInstanceMgr createFujiInstanceMgr() {
	    LOG.info("creating Fuji Instance Mgr...");
	    FujiInstanceMgr instanceMgr = new FujiInstanceMgr();
	    
	    Object mbsObj = this.getServletConfig().getServletContext().getAttribute(FujiAdminServiceConstants.FUJI_PLATFORM_MBS_ID);
        if (mbsObj != null && mbsObj instanceof MBeanServerConnection) {
            LOG.info("Found Platform MBean server. setting up the instanceMgr with PLATFORM mbean server");
            MBeanServerConnection mbs = (MBeanServerConnection)mbsObj;
            instanceMgr.setPlatformMBeanServer(mbs);
        } else {
            LOG.info("Platform MBean server NOT FOUND. setting up the instanceMgr with REMOTE mbean server");
            instanceMgr.setRemoteMBeanServer("localhost", "8699", "", "");
        }
	    this.getServletConfig().getServletContext().setAttribute(
                FujiAdminServiceConstants.FUJI_INSTANCE_MGR_ID, instanceMgr);	    
	    return instanceMgr;
	}
	
	
	private FujiInstanceMgr getFujiInstanceMgr() {
        FujiInstanceMgr instMgr = (FujiInstanceMgr) this.getServletConfig().getServletContext().getAttribute(FujiAdminServiceConstants.FUJI_INSTANCE_MGR_ID);
        if (instMgr == null ) {
            LOG.info(" Fuji Instance Mgr Not found in context...");
            instMgr = createFujiInstanceMgr();
        }
        return instMgr;
	}
	
//	private FujiInstanceInfo getFujiInstanceInfo(String host, String port,
//			String username, String password) {
//	    FujiInstanceMgr instanceMgr = getFujiInstanceMgr();
//		return instanceMgr.loadInstance(host, port, username, password);
//	}
	
    public FujiInstanceInfo getFujiInstanceInfo() {
//        LOG.info("getting fuji instance info...");
        FujiInstanceMgr instanceMgr = getFujiInstanceMgr();
        return instanceMgr.loadInstance();
        // return getFujiInstanceInfo("localhost", "8699", "", "");
    }
    
	public String installBundle(String bundlePath) {
		try {
			return getFujiInstanceMgr().installBundle(bundlePath);
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
	}

	public String startBundle(String bundleId) {
		try {
			return getFujiInstanceMgr().startBundle(Long.valueOf(bundleId));
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
	}

	public String stopBundle(String bundleId) {
		try {
			return getFujiInstanceMgr().stopBundle(Long.valueOf(bundleId));
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
	}

	public String uninstallBundle(String bundleId) {
		try {
			return getFujiInstanceMgr().uninstallBundle(Long.valueOf(bundleId));
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
	}
}
