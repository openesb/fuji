/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.admin.web.lite.client;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;

/**
*
* @author chikkala
*/
public class TitlePane extends Composite  {

	  /**
	   * The base style name.
	   */
	public static final String DEFAULT_STYLE_NAME = "Application";
	/**
	 * The panel that holds the main links.
	 */
	private HorizontalPanel linksPanel;
	/**
	 * The panel that contains the title widget and links.
	 */
	private FlexTable layout;

	public TitlePane() {
		// Setup the title pane with the title and links
		createTitlePane();
		initWidget(layout);
	}

	/**
	 * Add a link to the top of the page.
	 * 
	 * @param link
	 *            the widget to add to the mainLinks
	 */
	public void addLink(Widget link) {
		if (linksPanel.getWidgetCount() > 0) {
			linksPanel.add(new HTML("&nbsp;|&nbsp;"));
		}
		linksPanel.add(link);
	}

	/**
	 * Set the {@link Widget} to use as options, which appear to the right of
	 * the title bar.
	 * 
	 * @param options
	 *            the options widget
	 */
	public void setOptionsWidget(Widget options) {
		layout.setWidget(1, 1, options);
	}

	/**
	 * @return the {@link Widget} used as the title
	 */
	public Widget getTitleWidget() {
		return layout.getWidget(0, 0);
	}

	/**
	 * Set the {@link Widget} to use as the title bar.
	 * 
	 * @param title
	 *            the title widget
	 */
	public void setTitleWidget(Widget title) {
		layout.setWidget(1, 0, title);
	}

	/**
	 * Create the panel at the top of the page that contains the title and
	 * links.
	 */
	private void createTitlePane() {
		boolean isRTL = LocaleInfo.getCurrentLocale().isRTL(); // not yet used.

		layout = new FlexTable();
		layout.setCellPadding(0);
		layout.setCellSpacing(0);
		layout.setStyleName(DEFAULT_STYLE_NAME + "-top");
		FlexCellFormatter formatter = layout.getFlexCellFormatter();

		// Setup the links cell
		linksPanel = new HorizontalPanel();
		layout.setWidget(0, 0, linksPanel);
		formatter.setStyleName(0, 0, DEFAULT_STYLE_NAME + "-links");
		formatter.setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_RIGHT);
		formatter.setColSpan(0, 0, 2);

		// Setup the title cell
		setTitleWidget(null);
		formatter.setStyleName(1, 0, DEFAULT_STYLE_NAME + "-title");

		// Setup the options cell
		setOptionsWidget(null);
		formatter.setStyleName(1, 1, DEFAULT_STYLE_NAME + "-options");
		formatter.setHorizontalAlignment(1, 1, HasHorizontalAlignment.ALIGN_RIGHT);
		
		// Align the content to the top
		layout.getRowFormatter().setVerticalAlign(0,
				HasVerticalAlignment.ALIGN_TOP);
		layout.getRowFormatter().setVerticalAlign(1,
				HasVerticalAlignment.ALIGN_TOP);
	}
}
