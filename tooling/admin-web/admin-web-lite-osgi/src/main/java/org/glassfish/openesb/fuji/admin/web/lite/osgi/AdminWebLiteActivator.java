/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.admin.web.lite.osgi;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author chikkala
 */
public class AdminWebLiteActivator implements BundleActivator {

    private static final Logger LOG = Logger.getLogger(AdminWebLiteActivator.class.getName());

    private static final String FUJI_PLATFORM_MBS_ID = "fuji.admin.web.lite.platform.mbs";
    private static final String FUJI_ADMIN_HOST_ID = "fuji.admin.web.lite.admin.host";
    private static final String FUJI_ADMIN_PORT_ID = "fuji.admin.web.lite.admin.port";
    private static final String FUJI_ADMIN_USER_ID = "fuji.admin.web.lite.admin.user";
    private static final String FUJI_ADMIN_PASSWORD_ID = "fuji.admin.web.lite.admin.password";

    private static final String FUJI_ADMIN_ROOT_URL = "/fuji/admin";
    private static final String FUJI_ADMIN_ROOT_URL_PREFIX = "war";
    
    private static final String FUJI_ADMIN_LITE_URL = "/fuji/admin/lite";
    private static final String FUJI_ADMIN_LITE_URL_PREFIX = "war/lite";

    private static final String FUJI_ADMIN_SERVLET_URL = "/fuji/admin/lite/admin-service";
    private static final String FUJI_ADMIN_SERVLET_CLASS = "org.glassfish.openesb.fuji.admin.web.lite.server.FujiAdminServiceImpl";

    private static final String FUJI_ADMIN_INSTALL_BUNDLE_SERVLET_URL = "/fuji/admin/lite/install-bundle-service";
    private static final String FUJI_ADMIN_INSTALL_BUNDLE_SERVLET_CLASS = "org.glassfish.openesb.fuji.admin.web.lite.server.FujiInstallBundleServiceImpl";

    private static final String FUJI_ADMIN_MAIN_SERVLET_URL = "/fuji/admin/main";
    private static final String FUJI_ADMIN_MAIN_SERVLET_CLASS = "org.glassfish.openesb.fuji.admin.web.lite.server.FujiAdminMainServlet";

    private ServiceTracker httpTracker;

    public void start(BundleContext ctx) throws Exception {
        httpTracker = new ServiceTracker(ctx, HttpService.class.getName(), null);
        httpTracker.open();
        //TODO: start a thread to register the services.
        HttpService httpService = (HttpService) httpTracker.waitForService(60000);
        
        HttpContext defCtx = httpService.createDefaultHttpContext();
        HttpContext myCtx = new MyHttpContext(defCtx);

        httpService.registerResources(
                FUJI_ADMIN_LITE_URL, FUJI_ADMIN_LITE_URL_PREFIX, myCtx);

        httpService.registerResources(
                FUJI_ADMIN_ROOT_URL, FUJI_ADMIN_ROOT_URL_PREFIX, myCtx);


        Properties initParams = new Properties();
        MBeanServerConnection mbs = getPlatformMBeanServer();
//        testMBeanServer(mbs);
        try {
            Class adminServiceServletClass = AdminWebLiteActivator.class.forName(FUJI_ADMIN_SERVLET_CLASS);
            Object adminServiceServletInstance = adminServiceServletClass.newInstance();
            javax.servlet.Servlet adminServlet = (javax.servlet.Servlet) adminServiceServletInstance;
            httpService.registerServlet(FUJI_ADMIN_SERVLET_URL, adminServlet, initParams, myCtx);
            if ( mbs != null) {
                LOG.info("Setting Platform MBean server in admin servlet context");
                adminServlet.getServletConfig().getServletContext().setAttribute(FUJI_PLATFORM_MBS_ID, mbs);
            }
        } catch (Exception ex) {
           LOG.log(Level.INFO, ex.getMessage(), ex);
        }

        try {
            Class installServiceServletClass = AdminWebLiteActivator.class.forName(FUJI_ADMIN_INSTALL_BUNDLE_SERVLET_CLASS);
            Object installServiceServletInstance = installServiceServletClass.newInstance();
            javax.servlet.Servlet installServlet = (javax.servlet.Servlet) installServiceServletInstance;
            httpService.registerServlet(FUJI_ADMIN_INSTALL_BUNDLE_SERVLET_URL, installServlet, new Properties(), myCtx);
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }

        try {
            Class mainServletClass = AdminWebLiteActivator.class.forName(FUJI_ADMIN_MAIN_SERVLET_CLASS);
            Object mainServletInstance = mainServletClass.newInstance();
            javax.servlet.Servlet mainServlet = (javax.servlet.Servlet) mainServletInstance;
            httpService.registerServlet(FUJI_ADMIN_MAIN_SERVLET_URL, mainServlet, new Properties(), myCtx);
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    public void stop(BundleContext ctx) throws Exception {
        HttpService httpService = (HttpService) httpTracker.getService();
        
        httpService.unregister(FUJI_ADMIN_MAIN_SERVLET_URL);
        httpService.unregister(FUJI_ADMIN_INSTALL_BUNDLE_SERVLET_URL);
        httpService.unregister(FUJI_ADMIN_SERVLET_URL);

        httpService.unregister(FUJI_ADMIN_ROOT_URL);
        httpService.unregister(FUJI_ADMIN_LITE_URL);

        httpTracker.close();
    }

    private MBeanServerConnection getPlatformMBeanServer() {
        MBeanServer pServer = ManagementFactory.getPlatformMBeanServer();
        return pServer;
    }

//    private void testMBeanServer(MBeanServerConnection mbs) {
//        try {
//            if (mbs == null) {
//                System.out.println("No MBeanServer Connection");
//            }
//            String[] domains = mbs.getDomains();
//            for ( String domain : domains ) {
//                System.out.println("MBS domain: " + domain);
//            }
//            Set mbeans = mbs.queryMBeans(null, null);
//            for ( Object mbean : mbeans ) {
//                javax.management.ObjectInstance objInst = (javax.management.ObjectInstance)mbean;
//                System.out.println("MBean=" + objInst.getObjectName());
//            }
//        } catch (IOException ex) {
//            // Logger.getLogger(AdminWebLiteActivator.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
//    }

    public static class MyHttpContext implements HttpContext {
        HttpContext defCtx;
        public MyHttpContext(HttpContext defaultCtx) {
            defCtx = defaultCtx;
        }
        public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
            return defCtx.handleSecurity(request, response);
        }

//        public URL getResource(String resource) {
//            String name = resource;
//            if (name.startsWith("/fuji/admin/lite")) {
//                LOG.fine("Looking up for resource alias " + name);
//                name = "war/lite" + resource.substring("/fuji/admin/lite".length());
//            }
//            LOG.fine("Looking up for the http resources " + name);
//            URL resourceURL = defCtx.getResource(name);
//            if (resource == null ) {
//                LOG.fine("Resource not found on def http ctx: " + resource);
//                resourceURL = this.getClass().getResource(name);
//                if ( resource == null ) {
//                    LOG.fine("Resource not found: on my http ctx" + name);
//                }
//            }
//            return resourceURL;
//        }

        public URL getResource(String resName) {
            String name = resName;
            String resPrefix = "/fuji/admin/lite";
            String resMapPrefix = "war/lite";

            if (resName.startsWith(resPrefix)) {
                LOG.fine("Mapping resource alias " + resName);
                name = resMapPrefix + resName.substring(resPrefix.length());
            }
            // LOG.fine("Looking up for the http resources " + name);
            URL resourceURL = defCtx.getResource(name);
            if (resourceURL == null ) {
                LOG.fine("Resource not found on def http ctx: " + name);
                resourceURL = this.getClass().getResource(name);
                if ( resourceURL == null ) {
                    LOG.fine("Resource not found: on my http ctx" + name);
                }
            }
            return resourceURL;
        }

        public String getMimeType(String name) {
            return defCtx.getMimeType(name);
        }

    }
}
