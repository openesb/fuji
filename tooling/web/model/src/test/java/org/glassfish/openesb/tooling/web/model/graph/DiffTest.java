package org.glassfish.openesb.tooling.web.model.graph;

import org.glassfish.openesb.tooling.web.model.graph.comparison.GraphComparator;
import java.util.Date;
import java.util.List;
import junit.framework.TestCase;
import org.glassfish.openesb.tooling.web.model.graph.comparison.GraphComparator.DefaultGraphComparator;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkAdditionDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkRemovalDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeAdditionDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeRemovalDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.UpdateDescriptor;

/**
 *
 * @author ksorokin
 */
public class DiffTest extends TestCase {
    
    public DiffTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /*
     * 10 -> 20
     *
     * 10 -> 30
     */
    public void testDiff_AB_AC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30
     *
     * 10 -> 30
     */
    public void testDiff_ABC_AC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30 -> 40 -> 50
     *
     * 10 -> 30 -> 20 -> 50
     */
    public void testDiff_ABCDE_ACBE() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                50, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                300, "default",
                graph1.getNodeById(30), 0,
                graph1.getNodeById(40), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                400, "default",
                graph1.getNodeById(40), 0,
                graph1.getNodeById(50), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                50, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(30), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(50), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30 -> 40 -> 50 -> 60 -> 70 -> 80
     *
     * 10 -> 30 -> 80 -> 70 -> 20 -> 50
     * 40
     * 60
     */
    public void testDiff_ABCDEFGH_ACHGBE_D_F() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                50, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                60, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                70, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                80, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                300, "default",
                graph1.getNodeById(30), 0,
                graph1.getNodeById(40), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                400, "default",
                graph1.getNodeById(40), 0,
                graph1.getNodeById(50), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                500, "default",
                graph1.getNodeById(50), 0,
                graph1.getNodeById(60), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                600, "default",
                graph1.getNodeById(60), 0,
                graph1.getNodeById(70), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                700, "default",
                graph1.getNodeById(70), 0,
                graph1.getNodeById(80), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                50, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                60, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                70, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                80, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(30), 0,
                graph2.getNodeById(80), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                300, "default",
                graph2.getNodeById(80), 0,
                graph2.getNodeById(70), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                400, "default",
                graph2.getNodeById(70), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                500, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(50), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10
     *
     * 10 -> 20 -> 30
     */
    public void testDiff_A_ABC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30 -> 40
     *
     * 20 -> 30
     */
    public void testDiff_ABCD_BC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                300, "default",
                graph1.getNodeById(30), 0,
                graph1.getNodeById(40), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30
     *
     * .
     */
    public void testDiff_ABC___() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * .
     *
     * 10 -> 20 -> 30
     */
    public void testDiff___ABC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10
     * 20
     * 30
     * 
     * 10 -> 20 -> 30
     */
    public void testDiff_A_B_C_ABC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30
     *
     * 10 -> 20 -> 30
     */
    public void testDiff_ABC_ABC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20
     *    -> 30
     *
     * 10 -> 20 -> 30
     *          -> 40
     */
    public void testDiff_AB_C_ABC() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(10), 1,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                300, "default",
                graph2.getNodeById(20), 1,
                graph2.getNodeById(40), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        //printoutGraph(graph1);
        
        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /*
     * 10 -> 20 -> 30
     *    -> 40 -> 50
     *          -> 60 -> 70 -> 80
     * 
     * 20 -> 30
     *    -> 40 -> 50
     *          -> 60 -> 70 -> 80
     * 
     */
    public void testDiff_Tree1() {
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);
        graph1.add(new Node(
                10, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                50, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                60, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                70, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Node(
                80, "default",
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                200, "default",
                graph1.getNodeById(20), 0,
                graph1.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                300, "default",
                graph1.getNodeById(10), 1,
                graph1.getNodeById(40), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                400, "default",
                graph1.getNodeById(40), 0,
                graph1.getNodeById(50), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                500, "default",
                graph1.getNodeById(40), 1,
                graph1.getNodeById(60), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                600, "default",
                graph1.getNodeById(60), 0,
                graph1.getNodeById(70), 0,
                new PropertySet(), new PropertySet()));
        graph1.add(new Link(
                700, "default",
                graph1.getNodeById(70), 0,
                graph1.getNodeById(80), 0,
                new PropertySet(), new PropertySet()));

        Graph graph2 = new Graph("graph2", "", "", "", new Date(), null);
        graph2.add(new Node(
                20, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                30, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                40, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                50, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                60, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                70, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Node(
                80, "default",
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                200, "default",
                graph2.getNodeById(20), 1,
                graph2.getNodeById(40), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                300, "default",
                graph2.getNodeById(40), 0,
                graph2.getNodeById(50), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                400, "default",
                graph2.getNodeById(40), 1,
                graph2.getNodeById(60), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                500, "default",
                graph2.getNodeById(60), 0,
                graph2.getNodeById(70), 0,
                new PropertySet(), new PropertySet()));
        graph2.add(new Link(
                600, "default",
                graph2.getNodeById(70), 0,
                graph2.getNodeById(80), 0,
                new PropertySet(), new PropertySet()));

        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2);
        //printoutDiff(diff);

        graph1.update(diff);
        //printoutGraph(graph1);

        assertTrue(new GraphComparator.DefaultGraphComparator().compare(graph1, graph2));
    }

    /**
     * "file-1" -> "file-2"
     *
     * "file-1" -> "jruby-1" -> "jms-1"
     */
    public void testDiff_CodenameComparator1() {
        PropertySet properties;

        // -------------------------------------------------------------------------------
        Graph graph1 = new Graph("graph1", "", "", "", new Date(), null);

        properties = new PropertySet();
        properties.set(Graph.PROPERTY_CODE_NAME, "file-1");
        graph1.add(new Node(10, "default", properties, new PropertySet()));

        properties = new PropertySet();
        properties.set(Graph.PROPERTY_CODE_NAME, "file-2");
        graph1.add(new Node(20, "default", properties, new PropertySet()));

        graph1.add(new Link(
                100, "default",
                graph1.getNodeById(10), 0,
                graph1.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));

        // -------------------------------------------------------------------------------
        Graph graph2 = new Graph("graph1", "", "", "", new Date(), null);

        properties = new PropertySet();
        properties.set(Graph.PROPERTY_CODE_NAME, "file-1");
        graph2.add(new Node(10, "default", properties, new PropertySet()));

        properties = new PropertySet();
        properties.set(Graph.PROPERTY_CODE_NAME, "jruby-1");
        graph2.add(new Node(20, "default", properties, new PropertySet()));

        properties = new PropertySet();
        properties.set(Graph.PROPERTY_CODE_NAME, "jms-1");
        graph2.add(new Node(30, "default", properties, new PropertySet()));

        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(10), 0,
                graph2.getNodeById(20), 0,
                new PropertySet(), new PropertySet()));

        graph2.add(new Link(
                100, "default",
                graph2.getNodeById(20), 0,
                graph2.getNodeById(30), 0,
                new PropertySet(), new PropertySet()));

        // -------------------------------------------------------------------------------
        //printoutSeparator();
        //printoutGraph(graph1);
        //printoutGraph(graph2);

        List<UpdateDescriptor> diff = graph1.diffTo(graph2, new CodenameComparator());
        //printoutDiff(diff);

        graph1.update(diff);
        //printoutGraph(graph1);

        assertTrue(new CodenameComparator().compare(graph1, graph2));
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private void printoutDiff(final List<UpdateDescriptor> diff) {
        System.out.println("");
        
        for (UpdateDescriptor descriptor: diff) {
            if (descriptor instanceof NodeRemovalDescriptor) {
                System.out.println("- " + ((NodeRemovalDescriptor) descriptor).getNode().getId());
            }

            if (descriptor instanceof NodeAdditionDescriptor) {
                System.out.println("+ " + ((NodeAdditionDescriptor) descriptor).getId());
            }

            if (descriptor instanceof LinkRemovalDescriptor) {
                System.out.println("- --> [" + ((LinkRemovalDescriptor) descriptor).getLink().getId() + "]");
            }

            if (descriptor instanceof LinkAdditionDescriptor) {
                LinkAdditionDescriptor lad = (LinkAdditionDescriptor) descriptor;
                System.out.println("+ " + lad.getStartNodeId() + " -[" + lad.getId() + "]-> " + lad.getEndNodeId());
            }
        }

        System.out.println("");
        System.out.println("");
    }

    private void printoutGraph(final Graph graph) {
        for (Node node: graph.getStartingPoints()) {
            System.out.println("");
            printoutNode(node, "");
        }

        System.out.println("");
    }

    private void printoutNode(final Node node, final String prefix) {
        final String nodeString = "" + node.getId() + " ";
        System.out.print(nodeString);

        List<Link> links = node.getOutboundLinks();

        String newPrefix = prefix + padWithSpaces(nodeString.length());
        for (int i = 0; i < links.size(); i++) {
            final Link link = links.get(i);

            if (i != 0) {
                System.out.println("");
                System.out.print(newPrefix);
            }

            String linkString = "-" + link.getStartNodeConnector() + "[" + link.getId() + "]-> ";
            System.out.print(linkString);

            if (link.getEndNode() != null) {
                printoutNode(link.getEndNode(), newPrefix + padWithSpaces(linkString.length()));
            }
        }
    }

    private String padWithSpaces(final int length) {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(" ");
        }
        return builder.toString();
    }

    private void printoutSeparator() {
        System.out.println("-----------------------------------------------------------");
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Inner classes
    private static class CodenameComparator extends DefaultGraphComparator {
        @Override
        public boolean compare(
                final Node nodeA,
                final Node nodeB) {

            if ((nodeA == null) && (nodeB == null)) {
                return true;
            }

            if (!checkForNulls(nodeA, nodeB)) {
                return false;
            }

            final String codeNameA =
                    nodeA.getProperties().getValue(Graph.PROPERTY_CODE_NAME);
            final String codeNameB =
                    nodeB.getProperties().getValue(Graph.PROPERTY_CODE_NAME);

            if ((codeNameA == null) && (codeNameB == null)) {
                return true;
            }

            if (!checkForNulls(codeNameA, codeNameB)) {
                return false;
            }

            return codeNameA.equals(codeNameB);
        }
    }
}
