/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph;

import org.glassfish.openesb.tooling.web.model.Entity;

/**
 *
 * @author ksorokin
 */
public class Link extends Entity {

    private int id;
    private String type;
    private Node startNode;
    private int startNodeConnector;
    private Node endNode;
    private int endNodeConnector;
    private PropertySet properties = new PropertySet();
    private PropertySet uiProperties = new PropertySet();
    private Graph graph;

    public Link() {
        // Does nothing
    }

    public Link(
            final int id,
            final String type,
            final Node startNode,
            final int startNodeConnector,
            final Node endNode,
            final int endNodeConnector,
            final PropertySet properties,
            final PropertySet uiProperties) {

        this.id = id;
        this.type = type;

        this.startNode = startNode;
        this.startNodeConnector = startNodeConnector;

        this.endNode = endNode;
        this.endNodeConnector = endNodeConnector;

        this.properties = properties;
        this.uiProperties = uiProperties;
    }

    public Link(
            final int id,
            final String type,
            final Node startNode,
            final int startNodeConnector,
            final Node endNode,
            final int endNodeConnector,
            final PropertySet properties,
            final PropertySet uiProperties,
            final Graph graph) {

        this(id, type, startNode, startNodeConnector,
                endNode, endNodeConnector, properties, uiProperties);

        this.graph = graph;
    }

    // Properties ////////////////////////////////////////////////////////////////////////
    public Object getProperty(final String name) {
        return properties.get(name);
    }

    public String getPropertyType(final String name) {
        return properties.getType(name);
    }

    public void setProperty(
            final String name, final String value, final String propertyType) {

        properties.set(name, value, type);
    }

    public Object getUiProperty(final String name) {
        return uiProperties.get(name);
    }

    public String getUiPropertyType(final String name) {
        return uiProperties.getType(name);
    }

    public void setUiProperty(
            final String name, final String value, final String propertyType) {

        uiProperties.set(name, value, type);
    }

    // Getters and setters ///////////////////////////////////////////////////////////////
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Node getStartNode() {
        return startNode;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }

    public int getStartNodeConnector() {
        return startNodeConnector;
    }

    public void setStartNodeConnector(int startNodeConnector) {
        this.startNodeConnector = startNodeConnector;
    }

    public Node getEndNode() {
        return endNode;
    }

    public void setEndNode(Node endNode) {
        this.endNode = endNode;
    }

    public int getEndNodeConnector() {
        return endNodeConnector;
    }

    public void setEndNodeConnector(int endNodeConnector) {
        this.endNodeConnector = endNodeConnector;
    }

    public PropertySet getProperties() {
        return properties;
    }

    public void setProperties(PropertySet properties) {
        this.properties = properties;
    }

    public PropertySet getUiProperties() {
        return uiProperties;
    }

    public void setUiProperties(PropertySet uiProperties) {
        this.uiProperties = uiProperties;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }
}
