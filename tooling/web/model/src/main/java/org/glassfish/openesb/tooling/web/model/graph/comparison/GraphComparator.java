/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.comparison;

import java.util.LinkedList;
import org.glassfish.openesb.tooling.web.model.graph.*;
import java.util.List;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkUpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeUpdateDescriptor;

/**
 *
 * @author ksorokin
 */
public interface GraphComparator {
    boolean compare(
            final Graph graphA,
            final Graph graphB);

    boolean compare(
            final Node nodeA,
            final Node nodeB);

    boolean compare(
            final Link linkA,
            final Link linkB);

    NodeUpdateDescriptor buildUpdate(
            final Node nodeA,
            final Node nodeB,
            final boolean overrideUi);

    LinkUpdateDescriptor buildUpdate(
            final Link linkA,
            final Link linkB,
            final boolean overrideUi);

    public static class DefaultGraphComparator implements GraphComparator {
        public boolean compare(
                final Graph graphA,
                final Graph graphB) {

            final List<Node> nodesA = new LinkedList<Node>(graphA.getNodes());
            final List<Node> nodesB = new LinkedList<Node>(graphB.getNodes());

            final List<Link> linksA = new LinkedList<Link>(graphA.getLinks());
            final List<Link> linksB = new LinkedList<Link>(graphB.getLinks());

            // First, a couple of basic checks.
            if (nodesA.size() != nodesB.size()) {
                return false;
            }

            if (linksA.size() != linksB.size()) {
                return false;
            }

            // Check that there is a 1-1 correlation between nodesA and nodesB.
            for (int i = 0; i < nodesA.size(); i++) {
                for (int j = 0; j < nodesB.size(); j++) {
                    if (compare(nodesA.get(i), nodesB.get(j))) {
                        nodesB.remove(j);
                        break;
                    }
                }
            }

            if (nodesB.size() > 0) {
                return false;
            }

            // Check that there is a 1-1 correlation between linksA and linksB.
            for (int i = 0; i < linksA.size(); i++) {
                for (int j = 0; j < linksB.size(); j++) {
                    if (compare(linksA.get(i), linksB.get(j))) {
                        linksB.remove(j);
                        break;
                    }
                }
            }

            if (linksB.size() > 0) {
                return false;
            }

            return true;
        }

        public boolean compare(
                final Node nodeA,
                final Node nodeB) {

            if ((nodeA == null) && (nodeB == null)) {
                return true;
            }

            if (!checkForNulls(nodeA, nodeB)) {
                return false;
            }

            return nodeA.getId() == nodeB.getId();
        }

        public boolean compare(
                final Link linkA,
                final Link linkB) {

            if ((linkA == null) && (linkB == null)) {
                return true;
            }

            if (!checkForNulls(linkA, linkB)) {
                return false;
            }

            return compare(linkA.getStartNode(), linkB.getStartNode()) &&
                    compare(linkA.getEndNode(), linkB.getEndNode()) &&
                    (linkA.getStartNodeConnector() == linkB.getStartNodeConnector()) &&
                    (linkA.getEndNodeConnector() == linkB.getEndNodeConnector());
        }

        public NodeUpdateDescriptor buildUpdate(
                final Node nodeA,
                final Node nodeB,
                final boolean overrideUi) {

            boolean thereIsNeedToUpdate = false;

            if (nodeA.getId() != nodeB.getId()) {
                thereIsNeedToUpdate = true;
            }

            if (!nodeA.getType().equals(nodeB.getType())) {
                thereIsNeedToUpdate = true;
            }

            if (!compare(nodeA.getShortcutTo(), nodeB.getShortcutTo())) {
                thereIsNeedToUpdate = true;
            }

            if (!compare(nodeA.getParent(), nodeB.getParent())) {
                thereIsNeedToUpdate = true;
            }

            if (!nodeA.getProperties().equals(nodeB.getProperties())) {
                thereIsNeedToUpdate = true;
            }

            if (!nodeA.getUiProperties().equals(nodeB.getUiProperties())) {
                thereIsNeedToUpdate = true;
            }

            if (thereIsNeedToUpdate) {
                final PropertySet updatedProperties = new PropertySet();
                final List<String> removedProperties = new LinkedList<String>();
                final PropertySet updatedUiProperties = new PropertySet();
                final List<String> removedUiProperties = new LinkedList<String>();

                for (Property propertyA: nodeA.getProperties()) {
                    Property propertyB =
                            nodeB.getProperties().getProperty(propertyA.getName());

                    if (propertyB == null) {
                        removedProperties.add(propertyA.getName());
                    } else if (!propertyA.getType().equals(propertyB.getType()) ||
                            !propertyA.getValue().equals(propertyB.getValue())) {
                        updatedProperties.setProperty(propertyB);
                    }
                }
                for (Property propertyB: nodeB.getProperties()) {
                    if (nodeA.getProperties().getProperty(propertyB.getName()) == null) {
                        updatedProperties.setProperty(propertyB);
                    }
                }
                for (Property uiPropertyA: nodeA.getUiProperties()) {
                    Property uiPropertyB =
                            nodeB.getUiProperties().getProperty(uiPropertyA.getName());

                    if (uiPropertyB == null) {
                        if (overrideUi) {
                            removedUiProperties.add(uiPropertyA.getName());
                        }
                    } else if (!uiPropertyA.getType().equals(uiPropertyB.getType()) ||
                            !uiPropertyA.getValue().equals(uiPropertyB.getValue())) {
                        if (overrideUi) {
                            updatedUiProperties.setProperty(uiPropertyB);
                        }
                    }
                }
                for (Property uiPropertyB: nodeB.getUiProperties()) {
                    if (nodeA.getUiProperties().getProperty(uiPropertyB.getName()) == null) {
                        updatedUiProperties.setProperty(uiPropertyB);
                    }
                }

                return new NodeUpdateDescriptor(
                        nodeA,
                        nodeB.getId(),
                        nodeB.getType(),
                        nodeB.getShortcutTo(),
                        nodeB.getParent(),
                        updatedProperties,
                        removedProperties,
                        updatedUiProperties,
                        removedUiProperties);
            }
            
            return null;
        }

        public LinkUpdateDescriptor buildUpdate(
                final Link linkA,
                final Link linkB,
                final boolean overrideUi) {

            return null;
        }

        protected boolean compare(
                final Object objectA,
                final Object objectB) {

            if ((objectA == null) && (objectB == null)) {
                return true;
            }

            if (!checkForNulls(objectA, objectB)) {
                return false;
            }

            return objectA.equals(objectB);
        }

        protected boolean checkForNulls(
                final Object objectA,
                final Object objectB) {

            if (((objectA == null) && (objectB != null)) ||
                    ((objectA != null) && (objectB == null))) {

                return false;
            }

            return true;
        }
    }
}
