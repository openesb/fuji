/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.update;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.Property;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;
import java.util.List;

/**
 *
 * @author ksorokin
 */
public class NodeUpdateDescriptor implements UpdateDescriptor {
    
    private int id;
    private int newId;
    private Node node;
    private String type;
    private Node shortcutTo;
    private Node parent;
    
    private PropertySet updatedProperties;
    private List<String> removedProperties;
    
    private PropertySet updatedUiProperties;
    private List<String> removedUiProperties;
    
    public NodeUpdateDescriptor(
            final Node node,
            final String type,
            final Node shortcutTo,
            final Node parent,
            final PropertySet updatedProperties,
            final List<String> removedProperties,
            final PropertySet updatedUiProperties,
            final List<String> removedUiProperties) {

        this.id = -1;
        this.newId = -1;
        this.node = node;
        this.type = type;

        this.shortcutTo = shortcutTo;
        this.parent = parent;

        this.updatedProperties = updatedProperties;
        this.removedProperties = removedProperties;

        this.updatedUiProperties = updatedUiProperties;
        this.removedUiProperties = removedUiProperties;
    }

    public NodeUpdateDescriptor(
            final Node node,
            final int newId,
            final String type,
            final Node shortcutTo,
            final Node parent,
            final PropertySet updatedProperties,
            final List<String> removedProperties,
            final PropertySet updatedUiProperties,
            final List<String> removedUiProperties) {

        this(node,
                type,
                shortcutTo,
                parent,
                updatedProperties,
                removedProperties,
                updatedUiProperties,
                removedUiProperties);

        this.newId = newId;
    }
    
    public NodeUpdateDescriptor(
            final int id,
            final String type,
            final Node shortcutTo,
            final Node parent,
            final PropertySet updatedProperties,
            final List<String> removedProperties,
            final PropertySet updatedUiProperties,
            final List<String> removedUiProperties) {

        this(null,
                type,
                shortcutTo,
                parent,
                updatedProperties,
                removedProperties,
                updatedUiProperties,
                removedUiProperties);

        this.id = id;
    }

    public UpdateResult apply(final Graph graph) {
        Node existingNode = node;

        if (existingNode == null) {
            existingNode = graph.getNodeById(id);

            if (existingNode == null) {
                Node newNode = new Node(id, type, updatedProperties, updatedUiProperties);
                graph.add(newNode);
                newNode.setShortcutTo(shortcutTo);
                newNode.setParent(parent);

                return new UpdateResult(true, true);
            }
        }

        boolean semanticsUpdated = false;
        boolean uiUpdated = false;

        if ((newId != -1) && (newId != existingNode.getId())) {
            existingNode.setId(newId);
            semanticsUpdated = true;
        }

        final String currType = existingNode.getType();
        if (((type != null) && !type.equals(currType)) ||
                ((currType != null) && !currType.equals(type))) {

            existingNode.setType(type);
            semanticsUpdated = uiUpdated = true;
        }

        final Node currShortcutTo = existingNode.getShortcutTo();
        if (((currShortcutTo != null) && !currShortcutTo.equals(shortcutTo)) ||
                ((shortcutTo != null) && !shortcutTo.equals(currShortcutTo))) {

            existingNode.setShortcutTo(shortcutTo);
            semanticsUpdated = uiUpdated = true;
        }

        final Node currParent = existingNode.getParent();
        if (((currParent != null) && !currParent.equals(parent)) ||
                ((parent != null) && !parent.equals(currParent))) {

            existingNode.setParent(parent);
            semanticsUpdated = uiUpdated = true;
        }

        for (Property property: updatedProperties) {
            existingNode.getProperties().setProperty(property);
            semanticsUpdated = true;
        }
        for (String name: removedProperties) {
            existingNode.getProperties().remove(name);
            semanticsUpdated = true;
        }

        for (Property property: updatedUiProperties) {
            existingNode.getUiProperties().setProperty(property);
            uiUpdated = true;
        }
        for (String name: removedUiProperties) {
            existingNode.getUiProperties().remove(name);
            uiUpdated = true;
        }

        existingNode.updateArtifacts();

        return new UpdateResult(semanticsUpdated, uiUpdated);
    }

    public Node getNode() {
        return node;
    }

    public int getId() {
        return id;
    }
}
