/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.update;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;

/**
 *
 * @author ksorokin
 */
public class LinkAdditionDescriptor implements UpdateDescriptor {

    private int id;
    private String type;
    private int startNodeId;
    private int startNodeConnector;
    private int endNodeId;
    private int endNodeConnector;
    private PropertySet properties;
    private PropertySet uiProperties;

    public LinkAdditionDescriptor(
            final int id,
            final String type,
            final Node startNode,
            final int startNodeConnector,
            final Node endNode,
            final int endNodeConnector,
            final PropertySet properties,
            final PropertySet uiProperties) {

        this.id = id;
        this.type = type;
        this.startNodeId = startNode == null ? -1 : startNode.getId();
        this.startNodeConnector = startNodeConnector;
        this.endNodeId = endNode == null ? -1 : endNode.getId();
        this.endNodeConnector = endNodeConnector;
        this.properties = properties;
        this.uiProperties = uiProperties;
    }

    public LinkAdditionDescriptor(
            final int id,
            final String type,
            final int startNodeId,
            final int startNodeConnector,
            final int endNodeId,
            final int endNodeConnector,
            final PropertySet properties,
            final PropertySet uiProperties) {

        this.id = id;
        this.type = type;
        this.startNodeId = startNodeId;
        this.startNodeConnector = startNodeConnector;
        this.endNodeId = endNodeId;
        this.endNodeConnector = endNodeConnector;
        this.properties = properties;
        this.uiProperties = uiProperties;
    }

    public LinkAdditionDescriptor(
            final Link link) {

        this(link.getId(),
                link.getType(),
                link.getStartNode().getId(),
                link.getStartNodeConnector(),
                link.getEndNode().getId(),
                link.getEndNodeConnector(),
                link.getProperties(),
                link.getUiProperties());
    }

    public LinkAdditionDescriptor(
            final int id,
            final Link link) {

        this(id,
                link.getType(),
                link.getStartNode().getId(),
                link.getStartNodeConnector(),
                link.getEndNode().getId(),
                link.getEndNodeConnector(),
                link.getProperties(),
                link.getUiProperties());
    }

    public UpdateResult apply(Graph graph) {
        final Node startNode = graph.getNodeById(startNodeId);
        final Node endNode = graph.getNodeById(endNodeId);

        graph.add(new Link(id, type,
                startNode, startNodeConnector,
                endNode, endNodeConnector,
                properties, uiProperties));

        return new UpdateResult(true, true);
    }

    public int getId() {
        return id;
    }

    public int getStartNodeId() {
        return startNodeId;
    }

    public int getEndNodeId() {
        return endNodeId;
    }
}
