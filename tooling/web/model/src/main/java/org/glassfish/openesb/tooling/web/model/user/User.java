/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.user;

import org.glassfish.openesb.tooling.web.model.Entity;

/**
 *
 * @author ksorokin
 */
public class User extends Entity {
    private String username;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private String company;
    private String email;
    private UserSkinProperties properties;

    public User(
            final String username,
            final String passwordHash) {
        this.username = username;
        this.passwordHash = passwordHash;
    }

    public User(
            final String username,
            final String passwordHash,
            final String firstName,
            final String lastName,
            final String company,
            final String email,
            UserSkinProperties properties) {

        this(username, passwordHash);

        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.email = email;
        this.properties = properties;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(
            final String username) {

        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(
            final String passwordHash) {

        this.passwordHash = passwordHash;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(
            final String firstName) {

        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(
            final String lastName) {

        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(
            final String company) {

        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(
            final String email) {

        this.email = email;
    }

    /**
     * @return the properties
     */
    public UserSkinProperties getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(UserSkinProperties properties) {
        this.properties = properties;
    }
}
