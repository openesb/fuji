/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.glassfish.openesb.tooling.web.model.Entity;

/**
 *
 * @author ksorokin
 */
public class PropertySet extends Entity implements Iterable<Property> {

    private List<Property> properties = new LinkedList<Property>();
    private List<String> updatedProperties = new LinkedList<String>();

    // Properties ////////////////////////////////////////////////////////////////////////
    public Object get(final String name) {
        for (Property property : properties) {
            if (property.getName().equals(name)) {
                final String propertyType = property.getType();

                if (propertyType.equals(INTEGER)) {
                    return new Integer(property.getValue());
                }

                if (propertyType.equals(BOOLEAN)) {
                    return new Boolean(property.getValue());
                }

                return property.getValue();
            }
        }

        return null;
    }

    public String getValue(final String name) {
        for (Property property : properties) {
            if (property.getName().equals(name)) {
                return property.getValue();
            }
        }

        return null;
    }

    public String getValue(final String name, final String defaultValue) {
        for (Property property : properties) {
            if (property.getName().equals(name)) {
                return property.getValue();
            }
        }

        return defaultValue;
    }

    public String getType(final String name) {
        for (Property property : properties) {
            if (property.getName().equals(name)) {
                return property.getType();
            }
        }

        return null;
    }

    public Property getProperty(final String name) {
        for (Property property : properties) {
            if (property.getName().equals(name)) {
                return property;
            }
        }

        return null;
    }

    public void set(final String name, final String value, final String type) {
        if (!updatedProperties.contains(name)) {
            updatedProperties.add(name);
        }

        for (Property property : properties) {
            if (property.getName().equals(name)) {
                property.setValue(value);
                property.setType(type);
                return;
            }
        }

        properties.add(new Property(name, value, type));
    }

    public void set(final String name, final Object value) {
        if (!updatedProperties.contains(name)) {
            updatedProperties.add(name);
        }

        if (value == null) {
            set(name, "", NULL);
        } else {
            String type = STRING;

            if (value instanceof Integer) {
                type = INTEGER;
            } else if (value instanceof Boolean) {
                type = BOOLEAN;
            } else if (value instanceof Double) {
                type = DOUBLE;
            }

            set(name, value.toString(), type);
        }
    }

    public void setProperty(final Property property) {
        final String name = property.getName();

        if (!updatedProperties.contains(name)) {
            updatedProperties.add(name);
        }

        for (Property temp : properties) {
            if (temp.getName().equals(name)) {
                temp.setValue(property.getValue());
                temp.setType(property.getType());
                return;
            }
        }

        properties.add(property);
    }

    public void remove(final String name) {
        for (int i = 0; i < properties.size(); i++) {
            if (properties.get(i).getName().equals(name)) {
                properties.remove(i);
                return;
            }
        }
    }

    // Iterable //////////////////////////////////////////////////////////////////////////
    public Iterator<Property> iterator() {
        return getProperties().iterator();
    }

    // Getters and setters ///////////////////////////////////////////////////////////////
    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(final List<Property> properties) {
        this.properties = properties;
    }

    // Updates tracking //////////////////////////////////////////////////////////////////
    public List<String> getUpdatedProperties() {
        return updatedProperties;
    }

    public void resetUpdates() {
        updatedProperties = new LinkedList<String>();
    }

    // Generic ///////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        final int size = properties.size();
        final StringBuilder builder = new StringBuilder();

        builder.append("[ ");
        for (int i = 0; i < size; i++) {
            builder.append(properties.get(i).toString());

            if (i < size - 1) {
                builder.append(", ");
            }
        }
        builder.append(" ]");

        return builder.toString();
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final String STRING = "string";
    public static final String INTEGER = "integer";
    public static final String DOUBLE = "double";
    public static final String BOOLEAN = "boolean";
    public static final String NULL = "null";
}
