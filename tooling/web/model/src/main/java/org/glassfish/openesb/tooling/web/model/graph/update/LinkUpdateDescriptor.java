/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.update;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.Property;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;
import java.util.List;

/**
 *
 * @author ksorokin
 */
public class LinkUpdateDescriptor implements UpdateDescriptor {
    
    private Link link;
    private int id;
    private String type;
    
    private Node startNode;
    private Integer startConnector;
    private Node endNode;
    private Integer endConnector;
    
    private PropertySet updatedProperties;
    private List<String> removedProperties;
    
    private PropertySet updatedUiProperties;
    private List<String> removedUiProperties;
    
    public LinkUpdateDescriptor(
            final Link link,
            final String type,
            final Node startNode,
            final Integer startNodeConnector,
            final Node endNode,
            final Integer endNodeConnector,
            final PropertySet updatedProperties,
            final List<String> removedProperties,
            final PropertySet updatedUiProperties,
            final List<String> removedUiProperties) {

        this.id = -1;
        this.link = link;
        this.type = type;
        
        this.startNode = startNode;
        this.startConnector = startNodeConnector;
        this.endNode = endNode;
        this.endConnector = endNodeConnector;
        
        this.updatedProperties = updatedProperties;
        this.removedProperties = removedProperties;
        
        this.updatedUiProperties = updatedUiProperties;
        this.removedUiProperties = removedUiProperties;
    }
    
    public LinkUpdateDescriptor(
            final int id,
            final String type,
            final Node startNode,
            final Integer startNodeConnector,
            final Node endNode,
            final Integer endNodeConnector,
            final PropertySet updatedProperties,
            final List<String> removedProperties,
            final PropertySet updatedUiProperties,
            final List<String> removedUiProperties) {

        this(null,
                type,
                startNode,
                startNodeConnector,
                endNode,
                endNodeConnector,
                updatedProperties,
                removedProperties,
                updatedUiProperties,
                removedUiProperties);

        this.id = id;
        this.type = type;

        this.startNode = startNode;
        this.startConnector = startNodeConnector;
        this.endNode = endNode;
        this.endConnector = endNodeConnector;

        this.updatedProperties = updatedProperties;
        this.removedProperties = removedProperties;

        this.updatedUiProperties = updatedUiProperties;
        this.removedUiProperties = removedUiProperties;
    }

    public UpdateResult apply(final Graph graph) {
        Link existingLink = link;

        if (existingLink == null) {
            existingLink = graph.getLinkById(id);

            if (existingLink == null) {
                Link newLink = new Link(
                        id,
                        type,
                        startNode,
                        startConnector,
                        endNode,
                        endConnector,
                        updatedProperties,
                        updatedUiProperties);

                graph.add(newLink);

                return new UpdateResult(true, true);
            }
        }

        boolean semanticsUpdated = false;
        boolean uiUpdated = false;

        String currType = existingLink.getType();
        if (((type != null) && !type.equals(currType)) ||
                ((currType != null) && !currType.equals(type))) {

            existingLink.setType(type);
            semanticsUpdated = uiUpdated = true;
        }

        Node currNode = existingLink.getStartNode();
        if (((startNode != null) && !startNode.equals(currNode)) ||
                ((currNode != null) && !currNode.equals(startNode))) {

            existingLink.setStartNode(startNode);
            semanticsUpdated = uiUpdated = true;
        }

        Integer currConnector = existingLink.getStartNodeConnector();
        if (((startConnector != null) && !startConnector.equals(currConnector)) ||
                ((currConnector != null) && !currConnector.equals(startConnector))) {

            existingLink.setStartNodeConnector(startConnector);
            semanticsUpdated = uiUpdated = true;
        }

        currNode = existingLink.getEndNode();
        if (((endNode != null) && !endNode.equals(currNode)) ||
                ((currNode != null) && !currNode.equals(endNode))) {

            existingLink.setEndNode(endNode);
            semanticsUpdated = uiUpdated = true;
        }

        currConnector = existingLink.getEndNodeConnector();
        if (((endConnector != null) && !endConnector.equals(currConnector)) ||
                ((currConnector != null) && !currConnector.equals(endConnector))) {

            existingLink.setEndNodeConnector(endConnector);
            semanticsUpdated = uiUpdated = true;
        }

        for (Property property: updatedProperties) {
            existingLink.getProperties().setProperty(property);
            semanticsUpdated = true;
        }
        for (String name: removedProperties) {
            existingLink.getProperties().remove(name);
            semanticsUpdated = true;
        }

        for (Property property: updatedUiProperties) {
            existingLink.getUiProperties().setProperty(property);
            uiUpdated = true;
        }
        for (String name: removedUiProperties) {
            existingLink.getUiProperties().remove(name);
            uiUpdated = true;
        }

        return new UpdateResult(semanticsUpdated, uiUpdated);
    }

    public Link getLink() {
        return link;
    }

    public int getId() {
        return id;
    }
}
