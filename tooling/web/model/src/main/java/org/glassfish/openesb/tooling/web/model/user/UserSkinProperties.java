/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tooling.web.model.user;

import org.glassfish.openesb.tooling.web.model.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author AlexanderPermyacov
 */
public class UserSkinProperties extends Entity {
    private int userId;

    private Map<String, String> properties;

    public UserSkinProperties(final Integer propertyId, int userId) {
        this.setEntityId(propertyId);
        this.userId = userId;
        this.properties = new HashMap<String, String>();
    }

    public UserSkinProperties(int propertyId, int userId,
            final Map<String, String> properties) {
        this.setEntityId(propertyId);
        this.userId = userId;
        this.properties = new HashMap<String, String>(properties);
    }

    public String getPropertyValue(String name) {
        return properties.get(name);
    }

    public Set<String> getPropertiesName() {
        return properties.keySet();
    }

    public String putProperty(String name, String value) {
        return properties.put(name, value);
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    public boolean isEmpty() {
        return properties.isEmpty();
    }

    public static final List<String> USER_SKIN_PROPERTIES = Arrays.asList(
            new String[]{
            "COLOR",
            "SIZE_FONT",
            "WINDOW"
    });
}
