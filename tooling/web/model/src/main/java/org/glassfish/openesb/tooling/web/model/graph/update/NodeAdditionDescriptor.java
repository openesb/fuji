/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.update;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;

/**
 *
 * @author ksorokin
 */
public class NodeAdditionDescriptor implements UpdateDescriptor {

    private int id;
    private String type;
    private Node shortcutTo;
    private Node parent;
    private PropertySet properties;
    private PropertySet uiProperties;

    public NodeAdditionDescriptor(
            final int id,
            final String type,
            final Node shortcutTo,
            final Node parent,
            final PropertySet properties,
            final PropertySet uiProperties) {

        this.id = id;
        this.type = type;
        this.shortcutTo = shortcutTo;
        this.parent = parent;
        this.properties = properties;
        this.uiProperties = uiProperties;
    }

    public NodeAdditionDescriptor(
            final Node node) {

        this(node.getId(),
                node.getType(),
                node.getShortcutTo(),
                node.getParent(),
                node.getProperties(),
                node.getUiProperties());
    }

    public UpdateResult apply(Graph graph) {
        graph.add(new Node(id, type, shortcutTo, parent, properties, uiProperties));

        return new UpdateResult(true, true);
    }

    public int getId() {
        return id;
    }
}
