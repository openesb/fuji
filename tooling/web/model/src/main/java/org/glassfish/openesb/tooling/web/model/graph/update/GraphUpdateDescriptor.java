/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.update;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Node;

/**
 *
 * @author ksorokin
 */
public class GraphUpdateDescriptor implements UpdateDescriptor {
    private String codeName;
    private String displayName;
    private String description;

    public GraphUpdateDescriptor(
            final String codeName,
            final String displayName,
            final String description) {

        this.codeName = codeName;
        this.displayName = displayName;
        this.description = description;
    }

    public UpdateResult apply(
            final Graph graph) {

        String oldCodeName = null, oldDisplayName = null, oldDescription = null;

        boolean semanticsUpdated = false;
        boolean uiUpdated = false;

        if ((codeName != null) && !codeName.equals(graph.getCodeName())) {
            semanticsUpdated = true;

            oldCodeName = graph.getCodeName();
            graph.setCodeName(codeName);
        }

        if ((displayName != null) && !displayName.equals(graph.getDisplayName())) {
            uiUpdated = true;

            oldDisplayName = graph.getDisplayName();
            graph.setDisplayName(displayName);
        }

        if ((description != null) && !description.equals(graph.getDescription())) {
            uiUpdated = true;

            oldDescription = graph.getDescription();
            graph.setDescription(description);
        }

        for (Node node: graph.getNodes()) {
            node.graphChanged(oldCodeName, codeName);
        }

        return new UpdateResult(semanticsUpdated, uiUpdated);
    }
}
