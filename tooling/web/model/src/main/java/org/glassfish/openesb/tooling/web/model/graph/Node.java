/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tooling.web.model.Entity;

/**
 *
 * @author ksorokin
 */
public class Node extends Entity {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static final Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private int id;
    private String type;
    private PropertySet properties = new PropertySet();
    private PropertySet uiProperties = new PropertySet();
    private Map<String, String> artifacts = new HashMap<String, String>();
    private Graph graph;
    private Node shortcutTo;
    private Node parent;

    public Node() {
        // Does nothing
    }

    public Node(
            final Integer id,
            final String type,
            final PropertySet properties,
            final PropertySet uiProperties) {

        this.id = id;
        this.type = type;

        this.properties = properties;
        this.uiProperties = uiProperties;
    }

    public Node(
            final Integer id,
            final String type,
            final Node shortcutTo,
            final Node parent,
            final PropertySet properties,
            final PropertySet uiProperties) {

        this(id, type, properties, uiProperties);

        this.shortcutTo = shortcutTo;
        this.parent = parent;
    }

    public Node(
            final Integer id,
            final String type,
            final Node shortcutTo,
            final Node parent,
            final PropertySet properties,
            final PropertySet uiProperties,
            final Graph graph) {

        this(id, type, properties, uiProperties);

        this.shortcutTo = shortcutTo;
        this.parent = parent;
        this.graph = graph;
    }

    // Queries ///////////////////////////////////////////////////////////////////////////
    public List<Link> getOutboundLinks() {
        if (graph != null) {
            return graph.getOutboundLinks(this);
        }

        return new LinkedList<Link>();
    }

    public List<Link> getInboundLinks() {
        if (graph != null) {
            return graph.getInboundLinks(this);
        }

        return new LinkedList<Link>();
    }

    public List<Link> getLinks() {
        final List<Link> links = new LinkedList<Link>();

        links.addAll(getInboundLinks());
        links.addAll(getOutboundLinks());

        return links;
    }

    // Properties ////////////////////////////////////////////////////////////////////////
    public Object getProperty(final String name) {
        return properties.get(name);
    }

    public String getPropertyType(final String name) {
        return properties.getType(name);
    }

    public void setProperty(
            final String name, final String value, final String propertyType) {

        properties.set(name, value, propertyType);
    }

    public void setProperty(
            final String name, final Object value) {

        properties.set(name, value);
    }

    public Object getUiProperty(final String name) {
        return uiProperties.get(name);
    }

    public String getUiPropertyType(final String name) {
        return uiProperties.getType(name);
    }

    public void setUiProperty(
            final String name, final String value, final String propertyType) {

        uiProperties.set(name, value, type);
    }

    public void setUiProperty(
            final String name, final Object value) {

        uiProperties.set(name, value);
    }

    // Artifacts /////////////////////////////////////////////////////////////////////////
    public String getArtifact(final String artifactPath) {
        return artifacts.get(artifactPath);
    }

    public void setArtifact(final String artifactPath, final String artifactCode) {
        artifacts.put(artifactPath, artifactCode);
    }

    public Map<String, String> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(Map<String, String> artifacts) {
        this.artifacts = artifacts;
    }

    public void updateArtifacts() {
        // Check whether we need to update any artifacts.
        //
        // A good example is the aggregate construct with 'java' aggregation -- we
        // need to refactor the java source code whenever the packagename or
        // classname properties change. If the 'use_external' property changes, we
        // need to load the external configuration file and get the properties values
        // from there. Ugh..
        //
        // TODO: this is an ugly hack, we need to externalize this behavior going
        // forward, it does not belong here.
        if ("aggregate".equals(getType())) {
            final String expressionType =
                    getProperties().getValue("expression_type");
            final boolean useExternal = Boolean.parseBoolean(
                    getProperties().getValue("use_external_config"));
            String pakkage =
                    getProperties().getValue("packagename");
            String clazz =
                    getProperties().getValue("classname");

            String javaSource = getArtifact("java-source");

            if ("java".equals(expressionType) && (javaSource != null)) {
                if (useExternal) {
                    final String eConfig = getArtifact("external-config");
                    final Properties eProperties = new Properties();

                    if (eConfig != null) {
                        try {
                            eProperties.load(new ByteArrayInputStream(
                                    eConfig.getBytes("UTF-8")));

                            if (eProperties.getProperty("packagename") != null) {
                                pakkage = eProperties.getProperty("packagename");
                            }

                            if (eProperties.getProperty("classname") != null) {
                                clazz = eProperties.getProperty("classname");
                            }
                        } catch (IOException ex) {
                            logger.log(
                                    Level.SEVERE,
                                    "Failed to read from a ByteArrayInputStream. " +
                                            "Weird...",
                                    ex);
                        }
                    }
                }

                javaSource = javaSource.replaceFirst("package.+;", "package " + pakkage + ";");
                javaSource = javaSource.replaceFirst("public\\s+class\\s+\\S+\\s+(.*\\s+)?implements(.+)Aggregate", "public class " + clazz + " $1implements$2Aggregate");

                setArtifact("java-source", javaSource);
            }
        }
    }

    public void graphChanged(
            final String oldCodeName,
            final String newCodeName) {

        // If we're a POJO, we need to updated our java code, as it includes refrences to
        // graph code name.
        // TODO: this should eventually go into the type descriptor.
        if (type.equals("java")) {
            String code = properties.getValue("java_code");

            code = code.replace(oldCodeName + "}", newCodeName + "}");
            code = code.replace(oldCodeName + "_interface", newCodeName + "_interface");

            properties.set("java_code", code);
        }
    }

    // Getters and setters ///////////////////////////////////////////////////////////////
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PropertySet getProperties() {
        return properties;
    }

    public void setProperties(PropertySet properties) {
        this.properties = properties;
    }

    public PropertySet getUiProperties() {
        return uiProperties;
    }

    public void setUiProperties(PropertySet uiProperties) {
        this.uiProperties = uiProperties;
    }

    public Node getShortcutTo() {
        return shortcutTo;
    }

    public void setShortcutTo(final Node shortcutTo) {
        this.shortcutTo = shortcutTo;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(final Node parent) {
        this.parent = parent;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    // Generic ///////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        builder.append("[ ");
        builder.append("id: " + id + ", ");
        builder.append("type: " + type + ", ");
        builder.append("parent: " + (parent  == null ? "null" : parent.getId()) + ", ");
        builder.append("shortcutTo: " + (shortcutTo  == null ? "null" : shortcutTo.getId()) + ", ");
        builder.append("properties: " + properties.toString() + ", ");
        builder.append("uiProperties: " + uiProperties.toString() + "");
        builder.append(" ]");

        return builder.toString();
    }
}
