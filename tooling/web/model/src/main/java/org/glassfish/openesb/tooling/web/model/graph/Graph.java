/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph;

import org.glassfish.openesb.tooling.web.model.graph.comparison.GraphComparator;
import java.util.Date;
import org.glassfish.openesb.tooling.web.model.graph.update.UpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.UpdateResult;
import org.glassfish.openesb.tooling.web.model.graph.validation.Validator;
import org.glassfish.openesb.tooling.web.model.graph.validation.ValidationResult;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkAdditionDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkRemovalDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkUpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeAdditionDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeRemovalDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeUpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.user.User;

/**
 *
 * @author ksorokin
 */
public class Graph extends GraphDescriptor {
    private List<Node> nodes = new LinkedList<Node>();
    private List<Link> links = new LinkedList<Link>();
    private Map<String, String> artifacts = new HashMap<String, String>();

    public Graph(
            final String codeName,
            final String displayName,
            final String description,
            final String revision,
            final Date createdOn,
            final User createdBy) {

        super(codeName, displayName, description, revision,
                createdOn, createdBy);
    }

    public Graph(
            final String codeName,
            final String displayName,
            final String description,
            final String revision,
            final Date createdOn,
            final User createdBy,
            final Date updatedOn,
            final User updatedBy) {

        super(codeName, displayName, description, revision,
                createdOn, createdBy, updatedOn, updatedBy);
    }

    // Add and remove ////////////////////////////////////////////////////////////////////
    public void add(
            final Node node) {
        node.setGraph(this);
        getNodes().add(node);
    }

    public void remove(
            final Node node) {

        // First we need to remove all the links which are attached to this node
        for (Link link : getLinksByNode(node)) {
            remove(link);
        }

        // Then  remove the node itself
        nodes.remove(node);
        node.setGraph(null);
    }

    public void add(
            final Link link) {

        link.setGraph(this);
        links.add(link);
    }

    public void remove(
            final Link link) {

        links.remove(link);
        link.setGraph(null);
    }

    // Queries ///////////////////////////////////////////////////////////////////////////
    public Node getNodeById(
            final int id) {

        for (Node node : getNodes()) {
            if (node.getId() == id) {
                return node;
            }
        }

        return null;
    }

    public Link getLinkById(
            final int id) {

        for (Link link : getLinks()) {
            if (link.getId() == id) {
                return link;
            }
        }

        return null;
    }

    public List<Link> getLinksByNode(
            final Node node) {

        final List<Link> filtered;

        filtered = getInboundLinks(node);
        filtered.addAll(getOutboundLinks(node));

        return filtered;
    }

    public List<Link> getInboundLinks(
            final Node node) {

        final List<Link> filtered = new LinkedList<Link>();

        for (Link link : getLinks()) {
            if (node.equals(link.getEndNode())) {
                filtered.add(link);
            }
        }

        return filtered;
    }

    public List<Link> getOutboundLinks(
            final Node node) {

        final List<Link> filtered = new LinkedList<Link>();

        for (Link link : getLinks()) {
            if (node.equals(link.getStartNode())) {
                filtered.add(link);
            }
        }

        return filtered;
    }

    public List<Node> getStartingPoints() {
        final List<Node> filtered = new LinkedList<Node>();

        for (Node node : nodes) {
            if (getInboundLinks(node).size() == 0) {
                filtered.add(node);
            }
        }

        return filtered;
    }

    public List<Node> getShortcutsTo(
            final Node node) {

        final List<Node> shortcuts = new LinkedList<Node>();

        for (Node candidate: nodes) {
            if (node.equals(candidate.getShortcutTo())) {
                shortcuts.add(candidate);
            }
        }

        return shortcuts;
    }

    public List<String> getNodeTypes() {

        final List<String> types = new LinkedList<String>();

        for (Node node : nodes) {
            if (!types.contains(node.getType())) {
                types.add(node.getType());
            }
        }

        return types;
    }

    public List<Node> getNodesByType(
            final String type) {

        final List<Node> filtered = new LinkedList<Node>();

        for (Node node : nodes) {
            if (node.getType().equals(type)) {
                filtered.add(node);
            }
        }

        return filtered;
    }

    public Node getNodeByPropertyValue(
            final String propertyName,
            final Object value) {

        if (value == null) {
            return null;
        }

        for (Node node : getNodes()) {
            if (value.equals(node.getProperty(propertyName))) {
                return node;
            }
        }

        return null;
    }

    public List<Node> getNodesByPropertyValue(
            final String propertyName,
            final Object value) {

        final List<Node> result = new LinkedList<Node>();

        if (value != null) {
            for (Node node : getNodes()) {
                if (value.equals(node.getProperty(propertyName))) {
                    result.add(node);
                }
            }
        }

        return result;
    }

    public Node getNodeByCodeName(
            final String codeName) {

        return getNodeByPropertyValue(PROPERTY_CODE_NAME, codeName);
    }

    // Diff //////////////////////////////////////////////////////////////////////////////
    public List<UpdateDescriptor> diffTo(
            final Graph graph) {

        return diffTo(graph, new GraphComparator.DefaultGraphComparator());
    }

    public List<UpdateDescriptor> diffTo(
            final Graph graphB,
            final GraphComparator comparator) {

        final Graph graphA = this;

        // First we need to find different nodes.
        List<NodeUpdateDescriptor> nodeUpdates =
                new LinkedList<NodeUpdateDescriptor>();
        List<Node> nodesToRemove = new LinkedList<Node>(); // Nodes from graphA.
        List<Node> nodesToAdd = new LinkedList<Node>(); // Nodes from graphB.
        for (Node nodeA: graphA.getNodes()) {
            Node matchingB = null;

            boolean bFound = false;
            for (Node nodeB: graphB.getNodes()) {
                if (comparator.compare(nodeA, nodeB)) {
                    matchingB = nodeB;
                    break;
                }
            }

            if (matchingB == null) {
                nodesToRemove.add(nodeA);
            } else {
                final NodeUpdateDescriptor update =
                        comparator.buildUpdate(nodeA, matchingB, false);
                if (update != null) {
                    nodeUpdates.add(update);
                }
            }
        }
        for (Node nodeB: graphB.getNodes()) {
            boolean aFound = false;
            for (Node nodeA: graphA.getNodes()) {
                if (comparator.compare(nodeA, nodeB)) {
                    aFound = true;
                    break;
                }
            }

            if (!aFound) {
                nodesToAdd.add(nodeB);
            }
        }

        // Then we should examine the links. Basically this means:
        // - all links that are connected to nodesToRemove should be removed
        // - all links thet are connected to nodesToAdd should be added
        // - all links connected to the intersection of nodes should be examined for
        //    their presence in both graphs: keep them, add them or remove them.
        final List<LinkUpdateDescriptor> linkUpdates =
                new LinkedList<LinkUpdateDescriptor>();
        final List<Link> linksToRemove = new LinkedList<Link>(); // Links from graphA
        final List<Link> linksToAdd = new LinkedList<Link>(); // Links from graphB
        for (Link linkA: graphA.getLinks()) {
            final Node startNodeA = linkA.getStartNode();
            final Node endNodeA = linkA.getEndNode();

            if (nodesToRemove.contains(startNodeA) ||
                    nodesToRemove.contains(endNodeA)) {

                linksToRemove.add(linkA);
                continue;
            }

            // So, the link is in the intersection of nodes from both graphs. Check
            // whether it is also present in the other graph. So, let's find the
            // corresponding start node, check its links and decide. Of course this
            // would work if the link does have a start node. Otherwise -- do the same
            // with an end node.
            final Link linkB = findMatchingLink(linkA, graphB, comparator);
            if (linkB == null) {
                linksToRemove.add(linkA);
            } else {
                final LinkUpdateDescriptor update = 
                        comparator.buildUpdate(linkA, linkB, false);
                if (update != null) {
                    linkUpdates.add(update);
                }
            }
        }

        for (Link linkB: graphB.getLinks()) {
            final Node startNodeB = linkB.getStartNode();
            final Node endNodeB = linkB.getEndNode();

            if (nodesToAdd.contains(startNodeB) ||
                    nodesToAdd.contains(endNodeB)) {

                linksToAdd.add(linkB);
                continue;
            }

            // So, the link is in the intersection of nodes from both graphs. Check
            // whether it is also present in the other graph. So, let's find the
            // corresponding start node, check its links and decide. Of course this
            // would work if the link does have a start node. Otherwise -- do the same
            // with an end node.
            if (findMatchingLink(linkB, graphA, comparator) == null) {
                linksToAdd.add(linkB);
            }
        }

        // Building the diff now is really simple:
        // 1. Remove links.
        // 2. Remove nodes.
        // 3. Add nodes.
        // 4. Add links.
        // 5. Update nodes.
        // 6. Update links.
        final List<UpdateDescriptor> diff = new LinkedList<UpdateDescriptor>();

        for (Link link: linksToRemove) {
            diff.add(new LinkRemovalDescriptor(link));
        }
        for (Node node: nodesToRemove) {
            diff.add(new NodeRemovalDescriptor(node));
        }
        for (NodeUpdateDescriptor update: nodeUpdates) {
            diff.add(update);
        }
        for (Node node: nodesToAdd) {
            diff.add(new NodeAdditionDescriptor(node));
        }
        for (LinkUpdateDescriptor update: linkUpdates) {
            diff.add(update);
        }
        for (Link link: linksToAdd) {
            diff.add(new LinkAdditionDescriptor(link));
        }

        return diff;
    }

    private Link findMatchingLink(
            final Link linkA,
            final Graph graphB,
            final GraphComparator comparator) {

        final Node startNodeA = linkA.getStartNode();
        final int startConnectorA = linkA.getStartNodeConnector();

        final Node endNodeA = linkA.getEndNode();
        final int endConnectorA = linkA.getEndNodeConnector();

        if (startNodeA != null) {
            Node startNodeB = null;
            for (Node nodeB: graphB.getNodes()) {
                if (comparator.compare(startNodeA, nodeB)) {
                    startNodeB = nodeB;
                    break;
                }
            }

            if (startNodeB != null) {
                for (Link linkB: startNodeB.getOutboundLinks()) {
                    final Node endNodeB = linkB.getEndNode();

                    if (comparator.compare(endNodeA, endNodeB) &&
                            (startConnectorA == linkB.getStartNodeConnector()) &&
                            (endConnectorA == linkB.getEndNodeConnector())) {

                        return linkB;
                    }
                }
            }
        } else if (endNodeA != null) {
            Node endNodeB = null;
            for (Node nodeB: graphB.getNodes()) {
                if (comparator.compare(endNodeA, nodeB)) {
                    endNodeB = nodeB;
                    break;
                }
            }

            if (endNodeB != null) {
                for (Link linkB: endNodeB.getInboundLinks()) {
                    final Node startNodeB = linkB.getStartNode();

                    if (comparator.compare(startNodeA, startNodeB) &&
                            (startConnectorA == linkB.getStartNodeConnector()) &&
                            (endConnectorA == linkB.getEndNodeConnector())) {

                        return linkB;
                    }
                }
            }
        }

        // Stale links (no start node and no end node) are of no real interest.

        return null;
    }

    // Updates ///////////////////////////////////////////////////////////////////////////
    public UpdateResult update(
            final UpdateDescriptor update) {

        return update.apply(this);
    }

    public UpdateResult update(
            final List<UpdateDescriptor> updates) {

        final UpdateResult result = new UpdateResult(false, false);

        for (UpdateDescriptor update : updates) {
            result.merge(update.apply(this));
        }

        return result;
    }

    // Validation ////////////////////////////////////////////////////////////////////////
    public List<ValidationResult> applyValidator(
            final Validator validator) {

        final List<ValidationResult> results = new LinkedList<ValidationResult>();

        ValidationResult result;

        // First validate the graph as a whole
        result = validator.validate(this);
        if (result != null) {
            results.add(result);
        }

        // Then validate individual nodes
        for (Node node : getNodes()) {
            result = validator.validate(node);
            if (result != null) {
                results.add(result);
            }
        }

        // Then validate individual links
        for (Link link : getLinks()) {
            result = validator.validate(link);
            if (result != null) {
                results.add(result);
            }
        }

        return results;
    }

    // Artifacts /////////////////////////////////////////////////////////////////////////
    public String getArtifact(
            final String artifactPath) {

        return artifacts.get(artifactPath);
    }

    public void setArtifact(
            final String artifactPath,
            final String artifactCode) {

        artifacts.put(artifactPath, artifactCode);
    }

    public Map<String, String> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(
            final Map<String, String> artifacts) {

        this.artifacts = artifacts;
    }

    // Getters and setters ///////////////////////////////////////////////////////////////
    public List<Node> getNodes() {

        return nodes;
    }

    public void setNodes(
            final List<Node> nodes) {

        this.nodes = nodes;
    }

    public List<Link> getLinks() {

        return links;
    }

    public void setLinks(
            final List<Link> links) {

        this.links = links;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    /**
     * Special property: code name (String).
     */
    public static final String PROPERTY_CODE_NAME = "code_name";
    public static final String UI_PROPERTY_X = "x";
    public static final String UI_PROPERTY_Y = "y";
    public static final String UI_PROPERTY_DISPLAY_NAME = "display_name";
    public static final String UI_PROPERTY_DESCRIPTION = "description";
}
