/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.monitoring;

import java.util.Date;

/**
 *
 * @author ksorokin
 */
public class MonitoringMessage {
    private String content;
    private Integer service;
    private Boolean inbound;
    private Date time;

    public MonitoringMessage() {
        this.content = "";
        this.service = 0;
        this.inbound = false;
        this.time = new Date();
    }

    public MonitoringMessage(
            final String content,
            final Integer service,
            final Boolean inbound) {

        this();

        this.content = content;
        this.service = service;
        this.inbound = inbound;
    }

    public MonitoringMessage(
            final String content,
            final Integer service,
            final Boolean inbound,
            final Date time) {
        
        this.content = content;
        this.service = service;
        this.inbound = inbound;
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }

    public Boolean getInbound() {
        return inbound;
    }

    public Boolean isInbound() {
        return inbound;
    }

    public void setInbound(Boolean inbound) {
        this.inbound = inbound;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
