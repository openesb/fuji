package org.glassfish.openesb.tooling.web.model.graph.comparison;

import java.util.Arrays;
import java.util.List;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.comparison.GraphComparator.DefaultGraphComparator;

/**
 *
 * @author ksorokin
 */
public class FujiGraphComparator extends DefaultGraphComparator {
    /**
     * {@inheritDoc}
     *
     * <p>
     * In this implementation two nodes are considered to be the same, if:
     * - they are both null,
     * - they are not EIPs and their code names are equal,
     * - they are EIPs and their first non-EIP neighbours are the same.
     */
    @Override
    public boolean compare(
            final Node nodeA,
            final Node nodeB) {

        if ((nodeA == null) && (nodeB == null)) {
            return true;
        }

        if (!checkForNulls(nodeA, nodeB)) {
            return false;
        }

        if (!nodeA.getType().equals(nodeB.getType())) {
            return false;
        }

        if (!SPECIAL_TYPES.contains(nodeA.getType())) {
            final String codeNameA =
                    nodeA.getProperties().getValue(Graph.PROPERTY_CODE_NAME);
            final String codeNameB =
                    nodeB.getProperties().getValue(Graph.PROPERTY_CODE_NAME);

            if ((codeNameA == null) && (codeNameB == null)) {
                return true;
            }

            if (!checkForNulls(codeNameA, codeNameB)) {
                return false;
            }

            return codeNameA.equals(codeNameB);
        } else {
            final Node previousNeighbourA = getPreviousNonEipNeighbour(nodeA);
            final Node nextNeighbourA = getNextNonEipNeighbour(nodeA);
            final Node previousNeighbourB = getPreviousNonEipNeighbour(nodeB);
            final Node nextNeighbourB = getNextNonEipNeighbour(nodeB);

            if (compare(previousNeighbourA, previousNeighbourB) &&
                    (compare(nextNeighbourA, nextNeighbourB))) {

                return true;
            }

            return false;
        }
    }

    private Node getPreviousNonEipNeighbour(
            final Node eipNode) {

        assert (SPECIAL_TYPES.contains(eipNode.getType()));

        Node neighbour = eipNode;
        while ((neighbour != null) && 
                SPECIAL_TYPES.contains(neighbour.getType())) {

            Node candidate = null;
            for (Link link: neighbour.getInboundLinks()) {
                if (link.getStartNode() != null) {
                    candidate = link.getStartNode();
                    break;
                }
            }

            neighbour = candidate;
        }

        return neighbour;
    }

    private Node getNextNonEipNeighbour(
            final Node eipNode) {

        assert (SPECIAL_TYPES.contains(eipNode.getType()));

        Node neighbour = eipNode;
        while ((neighbour != null) && 
                SPECIAL_TYPES.contains(neighbour.getType())) {

            Node candidate = null;
            for (Link link: neighbour.getOutboundLinks()) {
                if (link.getEndNode() != null) {
                    candidate = link.getEndNode();
                    break;
                }
            }

            neighbour = candidate;
        }

        return neighbour;
    }
    
    public static final String BROADCAST =
            "broadcast"; // NOI18N
    public static final String SPLIT =
            "split"; // NOI18N
    public static final String AGGREGATE =
            "aggregate"; // NOI18N
    public static final String FILTER =
            "filter"; // NOI18N
    public static final String TEE =
            "tee"; // NOI18N
    public static final String SELECT =
            "select"; // NOI18N
    public static final String ROUTE =
            "route"; // NOI18N
    public static final List<String> SPECIAL_TYPES = Arrays.asList(
            BROADCAST,
            SPLIT,
            AGGREGATE,
            FILTER,
            TEE,
            SELECT,
            ROUTE);
}
