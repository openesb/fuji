/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph.update;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;

/**
 *
 * @author ksorokin
 */
public class LinkRemovalDescriptor implements UpdateDescriptor {
    
    private Link link;
    
    public LinkRemovalDescriptor(final Link link) {
        this.link = link;
    }
    
    public UpdateResult apply(final Graph graph) {
        if ((link != null) && graph.getLinks().contains(link)) {
            graph.remove(link);
            
            return new UpdateResult(true, true);
        }
        
        return new UpdateResult(false, false);
    }

    public Link getLink() {
        return link;
    }
}
