/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.model.graph;

import java.util.Date;
import org.glassfish.openesb.tooling.web.model.Entity;
import org.glassfish.openesb.tooling.web.model.user.User;

/**
 *
 * @author ksorokin
 * @author vbychkov
 */
public class GraphDescriptor extends Entity {
    private String codeName;
    private String displayName;
    private String description;
    private String revision;
    private Date createdOn;
    private User createdBy;
    private Date updatedOn;
    private User updatedBy;
    private boolean isTracingEnabled;

    public GraphDescriptor(
            final String codeName,
            final String displayName,
            final String description,
            final String revision,
            final Date createdOn,
            final User createdBy) {

        this(codeName, displayName, description, revision,
                createdOn, createdBy, createdOn, createdBy);
    }

    public GraphDescriptor(
            final String codeName,
            final String displayName,
            final String description,
            final String revision,
            final Date createdOn,
            final User createdBy,
            final Date updatedOn,
            final User updatedBy) {

        this.codeName = codeName;
        this.displayName = displayName;
        this.description = description;
        this.revision = revision;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        isTracingEnabled = false;
    }

    public String getCodeName() {

        return codeName;
    }

    public void setCodeName(
            final String codeName) {

        this.codeName = codeName;
    }

    public String getDisplayName() {

        return displayName;
    }

    public void setDisplayName(
            final String displayName) {

        this.displayName = displayName;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(
            final String description) {

        this.description = description;
    }
    
    public String getRevision() {

        return revision;
    }

    public void setRevision(
            final String revision) {

        this.revision = revision;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(
            final User createdBy) {

        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(
            final Date createdOn) {

        this.createdOn = createdOn;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(
            final User updatedBy) {

        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(
            final Date updatedOn) {

        this.updatedOn = updatedOn;
    }

    /**
     * @return the isTracingEnabled
     */
    public boolean isTracingEnabled() {
        return isTracingEnabled;
    }

    /**
     * @param isTracingEnabled the isTracingEnabled to set
     */
    public void setTracingEnabled(boolean isTracingEnabled) {
        this.isTracingEnabled = isTracingEnabled;
    }
}
