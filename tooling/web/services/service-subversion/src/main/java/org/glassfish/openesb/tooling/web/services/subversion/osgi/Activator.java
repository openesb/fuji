/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.subversion.osgi;

import org.glassfish.openesb.tooling.web.services.VersioningService;
import org.glassfish.openesb.tooling.web.services.subversion.SubversionVersioningService;
import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;

/**
 *
 * @author ksorokin
 */
public class Activator implements BundleActivator, Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static final Logger logger = 
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ServiceRegistration registration;
    private File workdir;

    public void start(
            final BundleContext context) throws Exception {

        // Initialize configuration ------------------------------------------------------
        final String svnUrl = System.getProperty(SYSTEM_PROP_URL);
        final String svnUser = System.getProperty(SYSTEM_PROP_USER);
        final String svnPassword = System.getProperty(SYSTEM_PROP_PASSWORD);

        String workdirPath = System.getProperty(SYSTEM_PROP_WORKDIR);
        if (workdirPath == null) {
            final File dataDir = context.getDataFile(DEFAULT_WORKDIR_NAME);

            if (dataDir == null) {
                workdirPath = DEFAULT_WORKDIR;
            } else {
                workdirPath = dataDir.getCanonicalPath();
            }
        }

        workdir = new File(workdirPath);
        workdir.mkdirs();

        // Initialize repository ---------------------------------------------------------
        FSRepositoryFactory.setup();
        DAVRepositoryFactory.setup();
        SVNRepositoryFactoryImpl.setup();

        // Calculate the factual repository URL tobe supplied to the service -------------
        String urlBase;
        if (svnUrl != null) {
            urlBase = svnUrl;
        } else {
            urlBase = "file://" + 
                    workdir.getAbsoluteFile().toURI().toString().substring(5);

            try {
                SVNRepositoryFactory.createLocalRepository(workdir, false, false);
            } catch (SVNException e) {
                // Knowingly ignore. In 99% of cases this means that a repository
                // already exists at the given location. For the remaining 1% we'll
                // fail later anyway.
                logger.log(
                        Level.WARNING,
                        "Failed to initialize repository factories.",
                        e);
            }
        }

        // Strip the possible trailing slash.
        if (urlBase.endsWith("/")) {
            urlBase = urlBase.substring(0, urlBase.length() - 1);
        }

        // Register services -------------------------------------------------------------
        registration = context.registerService(
                VersioningService.class.getName(),
                new SubversionVersioningService(urlBase, svnUser, svnPassword),
                new Properties());
    }

    public void stop(
            final BundleContext context) throws Exception {

        registration.unregister();
    }
}
