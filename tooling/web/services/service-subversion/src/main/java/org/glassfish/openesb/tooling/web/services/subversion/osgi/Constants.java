/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.subversion.osgi;

/**
 *
 * @author ksorokin
 */
public interface Constants {
    public static final String SYSTEM_PROP_URL =
            "fuji.tooling.web.svn.url"; // NOI18N

    public static final String SYSTEM_PROP_USER =
            "fuji.tooling.web.svn.user"; // NOI18N

    public static final String SYSTEM_PROP_PASSWORD =
            "fuji.tooling.web.svn.password"; // NOI18N

    public static final String SYSTEM_PROP_WORKDIR =
            "fuji.tooling.web.svn.workdir"; // NOI18N

    public static final String DEFAULT_WORKDIR_NAME =
            "svn"; // NOI18N

    public static final String DEFAULT_WORKDIR =
            System.getProperty("user.home") + "/.fuji/" + DEFAULT_WORKDIR_NAME; // NOI18N
}
