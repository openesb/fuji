/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.subversion;

import org.glassfish.openesb.tooling.web.services.VersioningService;
import org.glassfish.openesb.tooling.web.services.VersioningException;
import java.io.File;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.ISVNStatusHandler;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusClient;
import org.tmatesoft.svn.core.wc.SVNStatusType;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 *
 * @author ksorokin
 */
public final class SubversionVersioningService implements VersioningService {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private String urlBase;
    private String user;
    private String password;

    public SubversionVersioningService(
            final String urlBase,
            final String user,
            final String password) {

        this.urlBase = urlBase;
        this.user = user;
        this.password = password;
    }

    public String update(
            final String codeName,
            final File directory) throws VersioningException {

        try {
            final SVNClientManager manager = createManager();

            final SVNURL url = SVNURL.parseURIDecoded(urlBase + "/" + codeName);
            final File localDir = new File(directory, codeName);

            if (isUnderControl(localDir)) {
                return new Long(manager.getUpdateClient().doUpdate(
                        localDir,
                        SVNRevision.HEAD,
                        true)).toString();
            } else {
                return new Long(manager.getUpdateClient().doCheckout(
                        url,
                        localDir,
                        null,
                        SVNRevision.HEAD,
                        true)).toString();
            }
        } catch (SVNException e) {
            throw new VersioningException(e);
        }
    }

    public String commit(
            final String codeName,
            final File directory) throws VersioningException {

        try {
            final SVNClientManager manager = createManager();

            final SVNWCClient wcClient = manager.getWCClient();
            final SVNCommitClient commitClient = manager.getCommitClient();
            final SVNUpdateClient updateClient = manager.getUpdateClient();
            final SVNStatusClient statusClient = manager.getStatusClient();
            final SVNWCClient wsClient = manager.getWCClient();

            final SVNURL url = SVNURL.parseURIDecoded(urlBase + "/" + codeName);
            final File localDir = new File(directory, codeName);

            // First we need to find out whether the local directory is under version
            // control. If it is not, we need to import everything except the 'target'
            // subdirectory. Otherwise we'll scan the working copy for changes and 
            // commit them.

            long revision;
            if (isUnderControl(localDir)) {
                statusClient.doStatus(
                        localDir, true, false, false, false, new ISVNStatusHandler() {

                    public void handleStatus(
                            final SVNStatus st) throws SVNException {

                        if (st.getContentsStatus() == SVNStatusType.STATUS_UNVERSIONED) {
                            wcClient.doAdd(st.getFile(), false, false, false, true);
                        }

                        if (st.getContentsStatus() == SVNStatusType.STATUS_MISSING) {
                            wcClient.doDelete(st.getFile(), false, false, false);
                        }
                    }
                });

                revision = commitClient.doCommit(
                        new File[]{localDir},
                        true,
                        MessageFormat.format(CM_COMMIT, codeName),
                        false,
                        true).getNewRevision();

                updateClient.doUpdate(
                        localDir, SVNRevision.HEAD, true);

                // If the commit procedure returns -1, this means that the revision has
                // not changed. Use the one from status, i.e. the current one.
                if (revision == -1) {
                    SVNInfo info = wcClient.doInfo(localDir, SVNRevision.HEAD);
                    revision = info.getRevision().getNumber();
                }
            } else {
                revision = commitClient.doImport(
                        localDir,
                        url,
                        MessageFormat.format(CM_IMPORT, codeName),
                        true).getNewRevision();

                // After we've imported the project, we need to tranform it into a 
                // working copy. We'll do this by first deleting the just-imported
                // directory structure and then checking it out.
                delete(localDir);
                localDir.mkdirs();

                updateClient.doCheckout(
                        url,
                        localDir,
                        SVNRevision.HEAD,
                        SVNRevision.HEAD,
                        true);
            }

            return new Long(revision).toString();
        } catch (SVNException e) {
            throw new VersioningException(e);
        }
    }

    public void delete(
            final String codeName,
            final File directory) throws VersioningException {

        try {
            final SVNClientManager manager = createManager();

            final SVNCommitClient commitClient = manager.getCommitClient();

            final SVNURL url = SVNURL.parseURIDecoded(urlBase + "/" + codeName);
            final File localDir = new File(directory, codeName);

            if (isUnderControl(localDir)) {
                commitClient.doDelete(
                        new SVNURL[]{url},
                        MessageFormat.format(CM_DELETE, codeName));
            }

            delete(localDir);
        } catch (SVNException e) {
            throw new VersioningException(e);
        }
    }

    public String getLastModifiedRevision(
            final String codeName) throws VersioningException {

        try {
            final SVNClientManager manager = createManager();
            
            final SVNWCClient wcClient = manager.getWCClient();
            final SVNURL url = SVNURL.parseURIDecoded(urlBase + "/" + codeName);

            final SVNInfo info = wcClient.doInfo(
                    url, SVNRevision.UNDEFINED, SVNRevision.HEAD);

            return new Long(info.getCommittedRevision().getNumber()).toString();
        } catch (SVNException e) {
            throw new VersioningException(e);
        }
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private SVNClientManager createManager() {
        final SVNClientManager manager;

        if (user != null) {
            final File configDir =
                    SVNWCUtil.getDefaultConfigurationDirectory();
            final ISVNAuthenticationManager authManager =
                    SVNWCUtil.createDefaultAuthenticationManager(configDir, user, password);
            final ISVNOptions options =
                    SVNWCUtil.createDefaultOptions(configDir, false);

            manager = SVNClientManager.newInstance(options, authManager);
        } else {
            manager = SVNClientManager.newInstance();
        }

        return manager;
    }

    private void delete(
            final File file) {
        
        if (!file.exists()) {
            return;
        }

        if (file.isDirectory()) {
            final File[] children = file.listFiles();

            if (children != null) {
                for (File child : children) {
                    delete(child);
                }
            }
        }

        file.delete();
    }

    private boolean isUnderControl(
            final File file) {

        final SVNClientManager manager = SVNClientManager.newInstance();
        final SVNWCClient wcClient = manager.getWCClient();

        boolean isUnderControl;
        try {
            wcClient.doInfo(file, SVNRevision.HEAD);
            isUnderControl = true;
        } catch (SVNException e) {
            logger.log(
                    Level.FINE,
                    "Failed to get versioning information about a " +
                    "directory. Is it under version control at all?", e);
            
            // An exception here in most cases means that the given directory is not 
            // under control. Otherwise it could mean an error elsewhere, which we 
            // ignore, as in this case we'll fail elsewhere anyway.

            isUnderControl = false;
        }

        return isUnderControl;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String CM_IMPORT =
            "Initial import of {0}."; // NOI18N
    private static final String CM_COMMIT =
            "Updating {0}."; // NOI18N
    private static final String CM_DELETE =
            "Deleting {0}."; // NOI18N
}
