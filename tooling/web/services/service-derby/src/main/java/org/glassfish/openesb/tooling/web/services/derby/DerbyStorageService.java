/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.derby;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tooling.web.model.Entity;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.model.user.UserSkinProperties;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.GraphDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.Property;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;
import org.glassfish.openesb.tooling.web.services.StorageException;
import org.glassfish.openesb.tooling.web.services.StorageService;

/**
 *
 * @author ksorokin
 */
public class DerbyStorageService implements StorageService {

    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private Connection connection;
    private DerbyUtils utils;

    public DerbyStorageService(
            final Connection connection,
            final DerbyUtils utils) {

        this.connection = connection;
        this.utils = utils;
    }

    //user graph skin
    public void storeUserGraphSkin(int userId, int graphId,
            final UserSkinProperties properties) throws StorageException {
        
        try {
            String sql = "";
            final Statement statement = getStatement();
            Integer propertyId = null;

            sql = "select * from USER_GRAPH_PROPERTIES " +
                    "where USER_ID=" + escape(properties.getUserId().toString()) +
                    " and GRAPH_ID=" + graphId;

            logger.fine(sql);

            final ResultSet result = statement.executeQuery(sql);
            propertyId = result.getInt("PROPERTIES_ID");

            if (propertyId == null) {
                sql = "insert into " + "  SKIN_PROPERTIES (";
                String names = "";
                String values = "";
                for (String name : properties.getPropertiesName()) {
                    names = names + name + " , ";
                    values = values + escape(properties.getPropertyValue(name)) + " , ";
                }
                names = names.substring(0, names.length() - 3);
                values = values.substring(0, values.length() - 3);

                sql = sql + names + " ) ";
                sql = sql + " values ( " + values + " )";

                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                final ResultSet result_ = statement.getGeneratedKeys();

                if ((result_ != null) && result_.next()) {
                    propertyId = result_.getInt(1);

                    sql = "insert into " + "  SKIN_PROPERTIES" +
                            "(PROPERTIES_ID, USER_ID, GRAPH_ID)" +
                            "values (" +
                            " " + escape(propertyId.toString()) + ", " +
                            " " + escape(properties.getUserId().toString()) + ", " +
                            " " + graphId + " " +
                            ")";
                }
            } else {
                sql = "update " +
                        "  SKIN_PROPERTIES " +
                        "set ";
                for (String name : properties.getPropertiesName()) {
                    sql = sql + " " + name + "=" +
                            escape(properties.getPropertyValue(name)) + " , ";
                }
                sql = sql.substring(0, sql.length() - 3);
                sql = sql + "where PROPERTIES_ID=" + propertyId;

                logger.fine(sql);

                statement.executeUpdate(sql);
            }

            statement.close();

        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    // Graphs ----------------------------------------------------------------------------
    public void storeGraph(
            final Graph graph) throws StorageException {

        try {
            final String sql;
            final Statement statement;

            if (graph.getEntityId() != null) {
                sql =
                        "update " +
                        "  GRAPHS " +
                        "set " +
                        "  CODE_NAME=" + escape(graph.getCodeName()) + ", " +
                        "  DISPLAY_NAME=" + escape(graph.getDisplayName()) + ", " +
                        "  DESCRIPTION=" + escape(graph.getDescription()) + ", " +
                        "  REVISION=" + escape(graph.getRevision()) + ", " +
                        "  UPDATED_ON=" + escape(graph.getUpdatedOn()) + ", " +
                        "  UPDATED_BY=" + graph.getUpdatedBy().getEntityId() + " " +
                        "where ENTITY_ID=" + graph.getEntityId();

                logger.fine(sql);

                statement = getStatement();
                statement.executeUpdate(sql);
                statement.close();
            } else {
                sql =
                        "insert into " +
                        "  GRAPHS (" +
                        "    CODE_NAME, DISPLAY_NAME, DESCRIPTION, REVISION, " +
                        "    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) " +
                        "values (" +
                        "  " + escape(graph.getCodeName()) + ", " +
                        "  " + escape(graph.getDisplayName()) + ", " +
                        "  " + escape(graph.getDescription()) + ", " +
                        "  " + escape(graph.getRevision()) + ", " +
                        "  " + escape(graph.getCreatedOn()) + ", " +
                        "  " + graph.getCreatedBy().getEntityId() + ", " +
                        "  " + escape(graph.getUpdatedOn()) + ", " +
                        "  " + graph.getUpdatedBy().getEntityId() + ")";

                logger.fine(sql);

                statement = getStatement();
                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                final ResultSet result = statement.getGeneratedKeys();

                if ((result != null) && result.next()) {
                    graph.setEntityId(result.getInt(1));
                }

                statement.close();
            }

            storeNodes(graph);
            storeLinks(graph);
            storeArtifacts(graph.getEntityId(), -1, graph.getArtifacts());
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public Graph retrieveGraph(
            final String codeName) throws StorageException {

        try {
            final String sql =
                    "select * from GRAPHS " +
                    "where CODE_NAME=" + escape(codeName);

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final Graph graph;

            if ((result != null) && result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");

                final String displayName = result.getString("DISPLAY_NAME");
                final String description = result.getString("DESCRIPTION");
                final String revision = result.getString("REVISION");
                final Date createdOn = result.getDate("CREATED_ON");
                final Integer createdById = result.getInt("CREATED_BY");
                final Date updatedOn = result.getDate("UPDATED_ON");
                final Integer updatedById = result.getInt("UPDATED_BY");

                final User createdBy = retrieveUser(createdById);
                final User updatedBy;
                if (createdById == updatedById) {
                    updatedBy = createdBy;
                } else {
                    updatedBy = retrieveUser(updatedById);
                }

                graph = new Graph(codeName, displayName, description, revision,
                        createdOn, createdBy, updatedOn, updatedBy);

                graph.setEntityId(entityId);

                graph.setNodes(retrieveNodes(graph));
                graph.setLinks(retrieveLinks(graph));
                graph.setArtifacts(retrieveArtifacts(graph.getEntityId(), -1));
            } else {
                graph = null;
            }

            statement.close();

            return graph;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public void deleteGraph(
            final Graph graph) throws StorageException {

        try {
            if (graph.getEntityId() == null) {
                return;
            }

            final String sql = "delete from GRAPHS " +
                    "where ENTITY_ID=" + graph.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            statement.executeUpdate(sql);
            statement.close();

            deleteNodes(graph.getNodes());
            deleteLinks(graph.getLinks());
            deleteArtifacts(graph.getEntityId(), -1);
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public List<GraphDescriptor> retrieveGraphDescriptors() throws StorageException {
        try {
            final String sql =
                    "select " +
                    "  ENTITY_ID, " +
                    "  CODE_NAME, " +
                    "  DISPLAY_NAME, " +
                    "  DESCRIPTION, " +
                    "  REVISION, " +
                    "  CREATED_ON, " +
                    "  CREATED_BY, " +
                    "  UPDATED_ON, " +
                    "  UPDATED_BY " +
                    "from " +
                    "  GRAPHS";

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);
            final List<GraphDescriptor> descriptors = new ArrayList<GraphDescriptor>();

            if (result != null) {
                while (result.next()) {
                    final Integer entityId = result.getInt("ENTITY_ID");
                    
                    final String codeName = result.getString("CODE_NAME");
                    final String displayName = result.getString("DISPLAY_NAME");
                    final String description = result.getString("DESCRIPTION");
                    final String revision = result.getString("REVISION");
                    final Date createdOn = result.getDate("CREATED_ON");
                    final Integer createdById = result.getInt("CREATED_BY");
                    final Date updatedOn = result.getDate("UPDATED_ON");
                    final Integer updatedById = result.getInt("UPDATED_BY");

                    final User createdBy = retrieveUser(createdById);
                    final User updatedBy;
                    if (createdById == updatedById) {
                        updatedBy = createdBy;
                    } else {
                        updatedBy = retrieveUser(updatedById);
                    }

                    GraphDescriptor descriptor = null;
                    if (codeName != null) {
                        descriptor = new GraphDescriptor(
                                codeName,
                                displayName,
                                description,
                                revision,
                                createdOn,
                                createdBy,
                                updatedOn,
                                updatedBy);
                        descriptor.setEntityId(entityId);

                        descriptors.add(descriptor);
                    }
                }
            }

            statement.close();

            return descriptors;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public boolean graphExists(
            final String codeName) throws StorageException {

        try {
            final String sql =
                    "select count(*) as CNT from GRAPHS " +
                    "where CODE_NAME=" + escape(codeName);

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final boolean exists;

            if ((result != null) && result.next()) {
                exists = result.getInt("CNT") > 0;
            } else {
                exists = false;
            }

            statement.close();

            return exists;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    // Users -----------------------------------------------------------------------------
    public void storeUser(
            final User user) throws StorageException {

        try {
            String sql;
            final Statement statement;

            if (user.getEntityId() != null) {
                sql =
                        "update " +
                        "  USERS " +
                        "set " +
                        "  USERNAME=" + escape(user.getUsername()) + ", " +
                        "  PASSWORD=" + escape(user.getPasswordHash()) + " " +
                        "  FIRST_NAME=" + escape(user.getFirstName()) + " " +
                        "  LAST_NAME=" + escape(user.getLastName()) + " " +
                        "  COMPANY=" + escape(user.getCompany()) + " " +
                        "  EMAIL=" + escape(user.getEmail()) + " " ;
                if (user.getProperties() != null) {
                    UserSkinProperties property = saveUserSkinProperties(user.getProperties());
                    user.setProperties(property);
                    sql = sql + "  PROPERTIES_ID=" + property.getEntityId() + " " ;
                }        
                sql = sql + "where " + "  ENTITY_ID=" + user.getEntityId();

                logger.fine(sql);

                statement = getStatement();
                statement.executeUpdate(sql);
                statement.close();
            } else {
                sql =
                        "insert into " +
                        "  USERS (USERNAME, PASSWORD, FIRST_NAME, " +
                        "      LAST_NAME, COMPANY, EMAIL) " +
                        "values (" +
                        "  " + escape(user.getUsername()) + ", " +
                        "  " + escape(user.getPasswordHash()) + ", " +
                        "  " + escape(user.getFirstName()) + ", " +
                        "  " + escape(user.getLastName()) + ", " +
                        "  " + escape(user.getCompany()) + ", " +
                        "  " + escape(user.getEmail()) + " " +
                        ")";

                logger.fine(sql);

                statement = getStatement();
                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                final ResultSet result = statement.getGeneratedKeys();

                if ((result != null) && result.next()) {
                    user.setEntityId(result.getInt(1));
                }

                statement.close();
            }
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public User retrieveUser(
            final String username,
            final String passwordHash) throws StorageException {

        try {
            final String sql =
                    "select " +
                    "  * " +
                    "from " +
                    "  USERS " +
                    "where " +
                    "  USERNAME=" + escape(username) + " and " +
                    "  PASSWORD=" + escape(passwordHash);

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final User user;

            if ((result != null) && result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");

                final String firstName = result.getString("FIRST_NAME");
                final String lastName = result.getString("LAST_NAME");
                final String company = result.getString("COMPANY");
                final String email = result.getString("EMAIL");

                user = new User(
                        username, passwordHash, firstName, lastName, 
                        company, email, null);
                final Integer propertyId = result.getInt("PROPERTIES_ID");
                if (propertyId != null) {
                    user.setProperties(getUserProperties(propertyId, entityId));
                }

                user.setEntityId(entityId);
            } else {
                user = null;
            }

            statement.close();

            return user;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public User retrieveUser(
            final String username) throws StorageException {

        try {
            final String sql =
                    "select " +
                    "  * " +
                    "from " +
                    "  USERS " +
                    "where " +
                    "  USERNAME=" + escape(username);

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final User user;

            if ((result != null) && result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");
                final String passwordHash = result.getString("PASSWORD");

                final String firstName = result.getString("FIRST_NAME");
                final String lastName = result.getString("LAST_NAME");
                final String company = result.getString("COMPANY");
                final String email = result.getString("EMAIL");

                user = new User(
                        username, passwordHash, firstName, lastName, company, 
                        email, null);
                user.setEntityId(entityId);
                final Integer propertieId = result.getInt("PROPERTIES_ID");
                if (propertieId != null) {
                    user.setProperties(getUserProperties(propertieId, entityId));
                }
            } else {
                user = null;
            }

            statement.close();

            return user;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public User retrieveUser(
            final Integer entityId) throws StorageException {
        try {
            final String sql =
                    "select " +
                    "  * " +
                    "from " +
                    "  USERS " +
                    "where " +
                    "  ENTITY_ID=" + entityId;

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final User user;

            if ((result != null) && result.next()) {
                final String username = result.getString("USERNAME");
                final String passwordHash = result.getString("PASSWORD");

                final String firstName = result.getString("FIRST_NAME");
                final String lastName = result.getString("LAST_NAME");
                final String company = result.getString("COMPANY");
                final String email = result.getString("EMAIL");

                user = new User(
                        username, passwordHash, firstName, lastName, company, 
                        email, null);
                final Integer propertyId = result.getInt("PROPERTIES_ID");
                if (propertyId != null) {
                    user.setProperties(getUserProperties(propertyId, entityId));
                }
                user.setEntityId(entityId);
            } else {
                user = null;
            }

            statement.close();

            return user;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public void deleteUser(
            final User user) throws StorageException {

        try {
            if (user.getEntityId() == null) {
                return;
            }

            final String sql =
                    "delete from " +
                    "  USERS " +
                    "where " +
                    "  ENTITY_ID=" + user.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            statement.executeUpdate(sql);
            statement.close();
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public User retrieveNobody() throws StorageException {
        return retrieveUser(DEFAULT_USER_ENTITY_ID);
    }

    public boolean userExists(
            final String username) throws StorageException {

        return retrieveUser(username) != null;
    }

    // Settings --------------------------------------------------------------------------
    public String getSetting(
            final String name,
            final User user) throws StorageException {

        // Check that the user is a valid one.
        if (user.getEntityId() == null) {
            throw new StorageException("The supplied user is not a valid one, " +
                    "it does not have an entity id.");
        }

        try {
            final String sql =
                    "select * from SETTINGS " +
                    "where NAME=" + escape(name) + " and USER_ID=" + user.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final String value;
            if ((result != null) && result.next()) {
                value = result.getString("VALUE");
            } else {
                value = null;
            }

            statement.close();

            return value;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    public void setSetting(
            final String name,
            final String value,
            final User user) throws StorageException {

        // Check that the user is a valid one.
        if (user.getEntityId() == null) {
            throw new StorageException("The supplied user is not a valid one, " +
                    "it does not have an entity id.");
        }

        try {
            String sql =
                    "select count(*) as CNT from SETTINGS " +
                    "where NAME=" + escape(name) + " and USER_ID=" + user.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            final Statement updateStatement;
            final ResultSet result = statement.executeQuery(sql);

            if (result.next() && (result.getInt("CNT") != 0)) {
                sql =
                        "update " +
                        "  SETTINGS " +
                        "set " +
                        "  VALUE=" + escape(value) + " " +
                        "where " +
                        "  NAME=" + escape(name) + " and " +
                        "  USER_ID=" + user.getEntityId();
            } else {
                sql =
                        "insert into " +
                        "  SETTINGS (NAME, VALUE, USER_ID) " +
                        "values " +
                        "  (" + escape(name) + ", " +
                        "   " + escape(value) + ", " +
                        "   " + user.getEntityId() + ")";
            }

            statement.close();

            updateStatement = getStatement();
            updateStatement.executeUpdate(sql);
            updateStatement.close();
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    // Logging ---------------------------------------------------------------------------
    public void log(
            final String sessionId,
            final User user,
            final String action,
            final String param1,
            final String param2,
            final String param3) {

        try {
            final Integer userId = (user != null) ? user.getEntityId() : -1;

            final String sql =
                    "insert into " +
                    "  LOG (SESSION_ID, USER_ID, ACTION, PARAM_1, PARAM_2, PARAM_3, TIME) " +
                    "values ( " +
                    "  " + escape(sessionId) + ", " +
                    "  " + userId + ", " +
                    "  " + escape(action) + ", " +
                    "  " + escape(param1) + ", " +
                    "  " + escape(param2) + ", " +
                    "  " + escape(param3) + ", " +
                    "  " + escape(new Date()) + " " +
                    ")";

            Statement statement = getStatement();
            statement.executeUpdate(sql);
            statement.close();
        } catch (SQLException e) {
            logger.log(
                    Level.WARNING,
                    "Failed to add a log entry.", e);
        }
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private List<Node> retrieveNodes(
            final Graph graph) throws StorageException {

        try {
            final String sql = "select * from NODES " +
                    "where GRAPH_ID=" + graph.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final List<Node> nodes = new LinkedList<Node>();
            final Map<Node, Integer> shortcutsMap = new HashMap<Node, Integer>();
            final Map<Node, Integer> parentsMap = new HashMap<Node, Integer>();

            while ((result != null) && result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");

                final Integer id = result.getInt("ID");
                final String type = result.getString("TYPE");
                final Integer shortcutToId = result.getObject("SHORTCUT_TO_ID") != null ? result.getInt("SHORTCUT_TO_ID") : null;
                final Integer parentId = result.getObject("PARENT_ID") != null ? result.getInt("PARENT_ID") : null;

                final PropertySet properties =
                        retrievePropertySet(result.getInt("PROPERTIES_ID"));
                final PropertySet uiProperties =
                        retrievePropertySet(result.getInt("UI_PROPERTIES_ID"));

                final Node node = new Node(id, type, properties, uiProperties);
                node.setEntityId(entityId);
                node.setGraph(graph);
                node.setArtifacts(retrieveArtifacts(graph.getEntityId(), id));

                nodes.add(node);

                if (shortcutToId != null) {
                    shortcutsMap.put(node, shortcutToId);
                }

                if (parentId != null) {
                    parentsMap.put(node, parentId);
                }
            }

            statement.close();

            for (Node shortcut : shortcutsMap.keySet()) {
                final int shortcutToId = shortcutsMap.get(shortcut).intValue();

                for (int i = 0; i < nodes.size(); i++) {
                    if (nodes.get(i).getId() == shortcutToId) {
                        shortcut.setShortcutTo(nodes.get(i));
                    }
                }
            }

            for (Node child : parentsMap.keySet()) {
                final int parentId = parentsMap.get(child).intValue();

                for (int i = 0; i < nodes.size(); i++) {
                    if (nodes.get(i).getId() == parentId) {
                        child.setParent(nodes.get(i));
                    }
                }
            }

            return nodes;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private List<Link> retrieveLinks(
            final Graph graph) throws StorageException {

        try {
            final String sql = "select * from LINKS " +
                    "where GRAPH_ID=" + graph.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            final List<Link> links = new LinkedList<Link>();

            while ((result != null) && result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");

                final Integer id = result.getInt("ID");
                final String type = result.getString("TYPE");

                final PropertySet properties =
                        retrievePropertySet(result.getInt("PROPERTIES_ID"));
                final PropertySet uiProperties =
                        retrievePropertySet(result.getInt("UI_PROPERTIES_ID"));

                final Integer startNodeId = result.getInt("START_NODE_ID");
                final Integer startNodeConnector = result.getInt("START_NODE_CONNECTOR");

                final Integer endNodeId = result.getInt("END_NODE_ID");
                final Integer endNodeConnector = result.getInt("END_NODE_CONNECTOR");

                final Link link = new Link(
                        id,
                        type,
                        findEntity(startNodeId, graph.getNodes()),
                        startNodeConnector,
                        findEntity(endNodeId, graph.getNodes()),
                        endNodeConnector,
                        properties,
                        uiProperties,
                        graph);
                link.setEntityId(entityId);

                links.add(link);
            }

            statement.close();

            return links;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private PropertySet retrievePropertySet(
            final Integer setId) throws StorageException {

        try {
            final PropertySet set = new PropertySet();
            set.setEntityId(setId);

            final String sql =
                    "select * from PROPERTIES where PROPERTY_SET_ID=" + setId;

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            while ((result != null) && result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");

                final String name = result.getString("NAME");
                final String value = result.getString("VALUE");
                final String type = result.getString("TYPE");

                final Property property = new Property(name, value, type);
                property.setEntityId(entityId);

                set.setProperty(property);
            }

            statement.close();

            set.resetUpdates();
            return set;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private Map<String, String> retrieveArtifacts(
            final int graphId,
            final int nodeId) throws StorageException {

        try {
            final String sql =
                    "select * from ARTIFACTS " +
                    "where GRAPH_ID=" + graphId +
                    "  and NODE_ID=" + nodeId;

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);
            final Map<String, String> artifacts = new HashMap<String, String>();

            while ((result != null) && result.next()) {
                artifacts.put(result.getString("PATH"), result.getString("CODE"));
            }

            statement.close();
            return artifacts;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private void storeNodes(
            final Graph graph) throws StorageException {

        try {
            for (Node node : graph.getNodes()) {
                final PropertySet properties = node.getProperties();
                final PropertySet uiProperties = node.getUiProperties();

                storePropertySet(properties);
                storePropertySet(uiProperties);

                final String sql;
                final Statement statement;

                if (node.getEntityId() != null) {
                    sql = "update NODES set " +
                            "ID=" + node.getId() + ", " +
                            "GRAPH_ID=" + graph.getEntityId() + ", " +
                            "TYPE=" + escape(node.getType()) + ", " +
                            "SHORTCUT_TO_ID=" + (node.getShortcutTo() != null ? node.getShortcutTo().getId() : "NULL") + ", " +
                            "PARENT_ID=" + (node.getParent() != null ? node.getParent().getId() : "NULL") + ", " +
                            "PROPERTIES_ID=" + properties.getEntityId() + ", " +
                            "UI_PROPERTIES_ID=" + uiProperties.getEntityId() + " " +
                            "where ENTITY_ID=" + node.getEntityId();

                    logger.fine(sql);

                    statement = getStatement();
                    statement.executeUpdate(sql);
                    statement.close();
                } else {
                    sql = "insert into NODES (" +
                            "ID, " +
                            "GRAPH_ID, " +
                            "TYPE, " +
                            "SHORTCUT_TO_ID, " +
                            "PARENT_ID, " +
                            "PROPERTIES_ID, " +
                            "UI_PROPERTIES_ID) values (" +
                            node.getId() + ", " +
                            graph.getEntityId() + ", " +
                            escape(node.getType()) + ", " +
                            (node.getShortcutTo() != null ? node.getShortcutTo().getId() : "NULL") + ", " +
                            (node.getParent() != null ? node.getParent().getId() : "NULL") + ", " +
                            properties.getEntityId() + ", " +
                            uiProperties.getEntityId() + ")";

                    logger.fine(sql);

                    statement = getStatement();
                    statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                    final ResultSet result = statement.getGeneratedKeys();

                    if ((result != null) && result.next()) {
                        node.setEntityId(result.getInt(1));
                    }

                    statement.close();
                }

                storeArtifacts(graph.getEntityId(), node.getId(), node.getArtifacts());
            }

            // The last thing we need to do is to find out which nodes were deleted, i.e.
            // which nodes are present in the database, but not present in the supplied
            // list of nodes.
            final String sql =
                    "select ENTITY_ID, PROPERTIES_ID, UI_PROPERTIES_ID from NODES " +
                    "where GRAPH_ID=" + graph.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");
                final Integer propertiesId = result.getInt("PROPERTIES_ID");
                final Integer uiPropertiesId = result.getInt("UI_PROPERTIES_ID");

                if (findEntity(entityId, graph.getNodes()) == null) {
                    final Statement deleteStatement = getStatement();

                    deleteStatement.executeUpdate(
                            "delete from NODES where ENTITY_ID=" + entityId);
                    deleteStatement.close();

                    deletePropertySet(propertiesId);
                    deletePropertySet(uiPropertiesId);
                    deleteArtifacts(graph.getEntityId(), entityId);
                }
            }

            statement.close();
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private void storeLinks(
            final Graph graph) throws StorageException {

        try {
            for (Link link : graph.getLinks()) {
                final Node startNode = link.getStartNode();
                final Node endNode = link.getEndNode();
                final PropertySet properties = link.getProperties();
                final PropertySet uiProperties = link.getUiProperties();

                storePropertySet(properties);
                storePropertySet(uiProperties);

                final String sql;
                final Statement statement;

                if (link.getEntityId() != null) {
                    sql = "update LINKS set " +
                            "ID=" + link.getId() + ", " +
                            "GRAPH_ID=" + graph.getEntityId() + ", " +
                            "TYPE=" + escape(link.getType()) + ", " +
                            "START_NODE_ID=" + getId(startNode) + ", " +
                            "START_NODE_CONNECTOR=" + link.getStartNodeConnector() + ", " +
                            "END_NODE_ID=" + getId(endNode) + ", " +
                            "END_NODE_CONNECTOR=" + link.getEndNodeConnector() + ", " +
                            "PROPERTIES_ID=" + properties.getEntityId() + ", " +
                            "UI_PROPERTIES_ID=" + uiProperties.getEntityId() + " " +
                            "where ENTITY_ID=" + link.getEntityId();

                    logger.fine(sql);

                    statement = getStatement();
                    statement.executeUpdate(sql);
                    statement.close();
                } else {
                    sql = "insert into LINKS (" +
                            "ID, " +
                            "GRAPH_ID, " +
                            "TYPE, " +
                            "START_NODE_ID," +
                            "START_NODE_CONNECTOR," +
                            "END_NODE_ID," +
                            "END_NODE_CONNECTOR," +
                            "PROPERTIES_ID, " +
                            "UI_PROPERTIES_ID) values (" +
                            link.getId() + ", " +
                            graph.getEntityId() + ", " +
                            escape(link.getType()) + ", " +
                            getId(startNode) + ", " +
                            link.getStartNodeConnector() + ", " +
                            getId(endNode) + ", " +
                            link.getEndNodeConnector() + ", " +
                            properties.getEntityId() + ", " +
                            uiProperties.getEntityId() + ")";

                    logger.fine(sql);

                    statement = getStatement();
                    statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                    final ResultSet result = statement.getGeneratedKeys();

                    if ((result != null) && result.next()) {
                        link.setEntityId(result.getInt(1));
                    }

                    statement.close();
                }
            }

            // The last thing we need to do is to find out which links were deleted, i.e.
            // which links are present in the database, but not present in the supplied
            // list of links.
            final String sql =
                    "select ENTITY_ID, PROPERTIES_ID, UI_PROPERTIES_ID from LINKS " +
                    "where GRAPH_ID=" + graph.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);

            while (result.next()) {
                final Integer entityId = result.getInt("ENTITY_ID");
                final Integer propertiesId = result.getInt("PROPERTIES_ID");
                final Integer uiPropertiesId = result.getInt("UI_PROPERTIES_ID");

                if (findEntity(entityId, graph.getLinks()) == null) {
                    final Statement deleteStatement = getStatement();

                    deleteStatement.executeUpdate(
                            "delete from LINKS where ENTITY_ID=" + entityId);
                    deleteStatement.close();

                    deletePropertySet(propertiesId);
                    deletePropertySet(uiPropertiesId);
                }
            }

            statement.close();
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private void storePropertySet(
            final PropertySet set) throws StorageException {

        try {
            if (set.getEntityId() == null) {
                final String sql = "insert into PROPERTY_SETS (DUMMY) values (0)";

                logger.fine(sql);

                final Statement statement = getStatement();
                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                final ResultSet result = statement.getGeneratedKeys();

                if ((result != null) && result.next()) {
                    set.setEntityId(result.getInt(1));
                }

                statement.close();
            }

            for (String propertyName : set.getUpdatedProperties()) {
                final Property property = set.getProperty(propertyName);

                if (property == null) {
                    continue;
                }

                final String sql;
                final Statement statement;

                if (property.getEntityId() != null) {
                    sql = "update PROPERTIES set " +
                            "TYPE=" + escape(property.getType()) + ", " +
                            "VALUE=" + escape(property.getValue()) + " " +
                            "where PROPERTY_SET_ID=" + set.getEntityId() + " and " +
                            "NAME=" + escape(property.getName());

                    logger.fine(sql);

                    statement = getStatement();
                    statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                    statement.close();
                } else {
                    sql = "insert into PROPERTIES (" +
                            "PROPERTY_SET_ID, " +
                            "NAME, " +
                            "TYPE, " +
                            "VALUE) values (" +
                            set.getEntityId() + ", " +
                            escape(property.getName()) + ", " +
                            escape(property.getType()) + ", " +
                            escape(property.getValue()) + ")";

                    logger.fine(sql);

                    statement = getStatement();
                    statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                    final ResultSet result = statement.getGeneratedKeys();

                    if ((result != null) && result.next()) {
                        property.setEntityId(result.getInt(1));
                    }

                    statement.close();
                }
            }

            set.resetUpdates();
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private void storeArtifacts(
            final int graphId,
            final int nodeId,
            final Map<String, String> artifacts) throws StorageException {

        try {
            String sql;
            Statement statement, pathStatement;
            ResultSet result;

            for (String path : artifacts.keySet()) {
                sql =
                        "select count(*) as CNT " +
                        "from ARTIFACTS " +
                        "where GRAPH_ID=" + graphId +
                        "  and NODE_ID=" + nodeId;

                statement = getStatement();
                result = statement.executeQuery(sql);

                if (result.next() && (result.getInt("CNT") != 0)) {
                    sql =
                            "update ARTIFACTS " +
                            "set CODE=" + escape(artifacts.get(path)) + " " +
                            "where GRAPH_ID=" + graphId + " " +
                            "  and NODE_ID=" + nodeId + " " +
                            "  and PATH=" + escape(path) + "";
                } else {
                    sql =
                            "insert into ARTIFACTS (GRAPH_ID, NODE_ID, PATH, CODE) " +
                            "values (" +
                            "  " + graphId + ", " +
                            "  " + nodeId + ", " +
                            "  " + escape(path) + ", " +
                            "  " + escape(artifacts.get(path)) + ")";
                }

                statement.close();

                pathStatement = getStatement();
                pathStatement.executeUpdate(sql);
                pathStatement.close();
            }
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private void deleteNodes(
            final List<Node> nodes) throws SQLException {

        for (Node node : nodes) {
            if (node.getEntityId() == null) {
                continue;
            }

            final String sql = "delete from NODES " +
                    "where ENTITY_ID=" + node.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            statement.executeUpdate(sql);
            statement.close();

            deletePropertySet(node.getProperties());
            deletePropertySet(node.getUiProperties());
        }
    }

    private void deleteLinks(
            final List<Link> links) throws SQLException {

        for (Link link : links) {
            if (link.getEntityId() == null) {
                continue;
            }

            final String sql = "delete from LINKS " +
                    "where ENTITY_ID=" + link.getEntityId();

            logger.fine(sql);

            final Statement statement = getStatement();
            statement.executeUpdate(sql);
            statement.close();

            deletePropertySet(link.getProperties());
            deletePropertySet(link.getUiProperties());
        }
    }

    private void deletePropertySet(
            final PropertySet set) throws SQLException {
        deletePropertySet(set.getEntityId());
    }

    private void deletePropertySet(
            final Integer setId) throws SQLException {

        if (setId == null) {
            return;
        }

        String sql;
        Statement statement;

        sql = "delete from PROPERTIES " +
                "where PROPERTY_SET_ID=" + setId;

        logger.fine(sql);

        statement = getStatement();
        statement.executeUpdate(sql);
        statement.close();

        sql = "delete from PROPERTY_SETS " +
                "where ENTITY_ID=" + setId;

        logger.fine(sql);

        statement = getStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    private void deleteArtifacts(
            final int graphId,
            final int nodeId) throws SQLException {

        String sql;
        Statement statement;

        sql =
                "delete from ARTIFACTS " +
                "where GRAPH_ID=" + graphId + "" +
                "  and NODE_ID=" + nodeId;

        logger.fine(sql);

        statement = getStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    private UserSkinProperties saveUserSkinProperties(UserSkinProperties property)
            throws SQLException {
        
        Integer propertyId = property.getEntityId();
        Statement statement = getStatement();
        if (propertyId != null) {
            String sql = "update SKIN_PROPERTIES set ";
            for (String name : property.getPropertiesName()) {
                sql = sql + name + "=" + escape(property.getPropertyValue(name)) + " , ";
            }
            sql = sql.substring(0, sql.length() - 3);
            sql += " where PROPERTIES_ID=" + propertyId;

            logger.fine(sql);
            statement.executeUpdate(sql);
            statement.execute(sql);
        } else {
            String sql = "insert into " + "  SKIN_PROPERTIES (";
            String names = "";
            String values = "";

            for (String name : property.getPropertiesName()) {
                names = names + name + " , ";
                values = values + escape(property.getPropertyValue(name)) + " , ";
            }
            names = names.substring(0, names.length() - 3);
            values = values.substring(0, values.length() - 3);

            sql = sql + names + " ) ";
            sql = sql + " values ( " + values + " )";
            
            logger.fine(sql);
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

            final ResultSet result_ = statement.getGeneratedKeys();

            if ((result_ != null) && result_.next()) {
                propertyId = result_.getInt(1);
                property.setEntityId(propertyId);
            }
        }
        statement.close();
        return property;
    }

    private UserSkinProperties getUserProperties(int propertyId, int userId) {
        String sql = "select * from USER_SKIN_PROPERTIES " +
                "where PROPERTIES_ID=" + propertyId;
        logger.fine(sql);
        UserSkinProperties property = new UserSkinProperties(propertyId, userId);
        final Statement statement;
        try {
            statement = getStatement();
            final ResultSet result = statement.executeQuery(sql);
            for (String key : UserSkinProperties.USER_SKIN_PROPERTIES) {
                String value = result.getString(key);
                property.putProperty(key, value);
            }
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DerbyStorageService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return property;
    }

    // Utils -----------------------------------------------------------------------------
    private String escape(
            final String string) {

        if (string == null) {
            return "NULL";
        } else {
            return "'" + string.replace("'", "''") + "'";
        }
    }

    private String escape(
            final Date date) {

        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (date == null) {
            return "NULL";
        } else {
            return "'" + format.format(date) + "'";
        }
    }

    private <T extends Entity> T findEntity(
            final Integer entityId,
            final List<T> entities) {

        for (T node : entities) {
            if (node.getEntityId() == null) {
                continue;
            }

            if (node.getEntityId().equals(entityId)) {
                return node;
            }
        }

        return null;
    }

    private <T extends Entity> String getId(
            final T entity) {

        if (entity == null) {
            return "NULL";
        } else {
            return entity.getEntityId().toString();
        }
    }

    private synchronized Statement getStatement() throws SQLException {
        try {
            return connection.createStatement();
        } catch (SQLException e) {
            logger.log(
                    Level.FINE,
                    "Failed to create a SQL statement.", e);

            // If we got an exception while creating the statement, then it is likely
            // that the connection has expired. We'll try to reconnect and if that fails,
            // this would mean that the error is somewhere else.
            connection = utils.createConnection();
            return connection.createStatement();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final Integer DEFAULT_USER_ENTITY_ID = 0;
}
