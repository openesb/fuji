/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.derby.osgi;

/**
 *
 * @author ksorokin
 */
public interface Constants {
    public static final String SYSTEM_PROP_HOST =
            "fuji.tooling.web.derby.host"; // NOI18N
    public static final String SYSTEM_PROP_PORT =
            "fuji.tooling.web.derby.port"; // NOI18N
    public static final String SYSTEM_PROP_DBNAME =
            "fuji.tooling.web.derby.dbname"; // NOI18N
    public static final String SYSTEM_PROP_DBUSER =
            "fuji.tooling.web.derby.user"; // NOI18N
    public static final String SYSTEM_PROP_DBPASSWORD =
            "fuji.tooling.web.derby.password"; // NOI18N
    public static final String DEFAULT_HOST =
            "localhost"; // NOI18N
    public static final String DEFAULT_PORT =
            "1600"; // NOI18N
    public static final String DEFAULT_DBNAME =
            "db-web-tooling"; // NOI18N
    public static final String DEFAULT_DBUSER =
            "app"; // NOI18N
    public static final String DEFAULT_DBPASSWORD =
            "app"; // NOI18N
}
