/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.osgi;

import org.glassfish.openesb.tooling.web.services.DeployerService;
import org.glassfish.openesb.tooling.web.services.DeployerException;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.service.startlevel.StartLevel;

/**
 *
 * @author ksorokin
 */
public class OsgiDeployerService implements DeployerService {

    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private BundleContext context;

    public OsgiDeployerService(final BundleContext context) {
        this.context = context;
    }

    public void deploy(
            final String codeName,
            final File entity) throws DeployerException {

        long start = System.currentTimeMillis();

        try {
            StartLevel slService = (StartLevel) context.getService(
                    context.getServiceReference(StartLevel.class.getName()));

            for (Bundle existing : context.getBundles()) {
                if (codeName.equals(existing.getSymbolicName())) {
                    existing.uninstall();
                    break;
                }
            }

            Bundle bundle = context.installBundle(entity.toURL().toString());

            slService.setBundleStartLevel(bundle, 1);
            bundle.start();

            if (!codeName.equals(bundle.getSymbolicName())) {
                // TODO We need a more sensible approach to reporting this
                throw new DeployerException();
            }
        } catch (BundleException e) {
            throw new DeployerException(e);
        } catch (MalformedURLException e) {
            throw new DeployerException(e);
        } finally {
            // Benchmark
            logger.log(Level.FINE, "OsgiDeployerService.deploy: " +
                    (System.currentTimeMillis() - start) + " ms.");
        }
    }

    public void undeploy(
            final String codeName) throws DeployerException {

        if (codeName == null) {
            throw new DeployerException("Bundle code name cannot be null.");
        }

        try {
            for (Bundle bundle : context.getBundles()) {
                if (codeName.equals(bundle.getSymbolicName())) {
                    bundle.uninstall();
                    return;
                }
            }
        } catch (BundleException e) {
            throw new DeployerException(e);
        }
    }
}
