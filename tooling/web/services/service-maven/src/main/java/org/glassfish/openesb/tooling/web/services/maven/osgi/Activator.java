/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.maven.osgi;

import com.sun.jbi.fuji.maven.util.MavenUtils;
import java.io.File;
import org.glassfish.openesb.tooling.web.services.BuilderService;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.services.SerializerService;
import org.glassfish.openesb.tooling.web.services.maven.MavenBuilderService;
import org.glassfish.openesb.tooling.web.services.maven.MavenSerializerService;
import java.util.Properties;
import org.glassfish.openesb.tooling.web.services.maven.Constants;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public class Activator implements BundleActivator, Constants {
    private ServiceTracker toolsLookupTracker;
    private ServiceTracker registryTracker;
    
    private ServiceRegistration serializerRegistration;
    private ServiceRegistration builderRegistration;

    public void start(BundleContext context) throws Exception {
        toolsLookupTracker = new ServiceTracker(
                context,
                ToolsLookup.class.getName(),
                null);
        toolsLookupTracker.open();
        // Get hold of the types manager service -----------------------------------------
        registryTracker = new ServiceTracker(
                context,
                RegistryService.class.getName(),
                null);
        registryTracker.open();
        
        // Register services -------------------------------------------------------------
        serializerRegistration = context.registerService(
                SerializerService.class.getName(),
                new MavenSerializerService(registryTracker, toolsLookupTracker),
                new Properties());
        builderRegistration = context.registerService(
                BuilderService.class.getName(),
                new MavenBuilderService(context, registryTracker, toolsLookupTracker),
                new Properties());

        // Create POJO SE classpath jars -------------------------------------------------
        final File dataDir = context.getDataFile(POJO_SE_CLASSPATH_DIRNAME);
        dataDir.mkdirs();

        MavenUtils.writeURLToFile(
                new File(dataDir, POJO_SE_CLASSPATH_JBI_FRAMEWORK),
                getClass().getClassLoader().getResource(POJO_SE_CLASSPATH_JBI_FRAMEWORK));
        MavenUtils.writeURLToFile(
                new File(dataDir, POJO_SE_CLASSPATH_FUJI_API),
                getClass().getClassLoader().getResource(POJO_SE_CLASSPATH_FUJI_API));
        MavenUtils.writeURLToFile(
                new File(dataDir, POJO_SE_CLASSPATH_POJO_API),
                getClass().getClassLoader().getResource(POJO_SE_CLASSPATH_POJO_API));
    }

    public void stop(BundleContext context) throws Exception {
        builderRegistration.unregister();
        serializerRegistration.unregister();

        registryTracker.close();
        toolsLookupTracker.close();
    }

}
