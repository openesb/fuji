/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.maven;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.maven.spi.ServiceToolsLocator;
import com.sun.jbi.fuji.maven.util.AppBuildManager;
import com.sun.jbi.fuji.maven.util.ApplicationUtils;
import com.sun.jbi.fuji.maven.util.MavenUtils;
import com.sun.jbi.fuji.maven.util.ServiceDescriptorWrapper.ServiceSPIWrapper;
import java.util.Set;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.comparison.FujiGraphComparator;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.services.SerializerService;
import org.glassfish.openesb.tooling.web.services.SerializerException;
import org.glassfish.openesb.tooling.web.utils.Utils;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public final class MavenSerializerService implements SerializerService {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ServiceTracker registryTracker;
    private ServiceTracker toolsLookupTracker;

    public MavenSerializerService(
            final ServiceTracker registryTracker, ServiceTracker toolsLookupTracker) {
        this.registryTracker = registryTracker;
        this.toolsLookupTracker = toolsLookupTracker;
    }

    public void serialize(
            final Graph graph,
            final File directory) throws SerializerException {

        long start = System.currentTimeMillis();
        
        try {
            // Initialize the project.
            if (!directory.exists()) {
                directory.mkdirs();
            }

            final File projectDirectory = new File(directory, graph.getCodeName());

            // If pom.xml exists, we think that the project has been already 
            // initialized, so we skip the next step
            final File pomXml = new File(projectDirectory, "pom.xml");
            if (!pomXml.exists()) {
                final InputStream archetypeStream = getClass().
                        getClassLoader().getResourceAsStream("app-archetype.jar");

                final String symbolicName = graph.getCodeName();

                // Define the bundle name, it should be equal to graph's display name, 
                // but it is empty or null, we will apply a default pattern
                String bundleName = graph.getDisplayName();
                if ((bundleName == null) || bundleName.trim().equals("")) {
                    bundleName = MessageFormat.format(
                            DEFAULT_BUNDLE_NAME_PATTERN, symbolicName);
                }

                ApplicationUtils.generateProject(
                        projectDirectory,
                        archetypeStream,
                        "open-esb.fuji.applications",
                        symbolicName,
                        "1.0-SNAPSHOT",
                        bundleName);

                archetypeStream.close();
            }

            // Populate the IFL file
            final File iflFile = new File(projectDirectory, "src/main/ifl/app.ifl");
            SerializerUtils.generateIfl(graph, iflFile);

            final WebServiceToolsLocator locator = new WebServiceToolsLocator(
                    (RegistryService) registryTracker.getService(),
                    (ToolsLookup)toolsLookupTracker.getService());

            SerializerUtils.writeApplicationArtifacts(graph, projectDirectory);
            Properties prop = new Properties();
            prop.put("applicationId", graph.getCodeName()); // NOI18N
            prop.put("artifactId", graph.getCodeName()); // NOI18N

            final AppBuildManager appBuilder = new AppBuildManager(projectDirectory, prop);
            final ArrayList usedFiles = new ArrayList();
            
            // Generate the artifacts for the components.
            final IFLModel ifl =
                    new IFLReader().read(new File(projectDirectory, "src/main/ifl"));
            
            // Get the service types defined in the ifl. to call the generate artifacts
            // for only those service types.
            final Set<String> iflServiceTypes = ifl.getServiceTypes();

            // Apply the components' settings
            for (Node node: graph.getNodes()) {
                if (!SerializerUtils.SPECIAL_TYPES.contains(node.getType())) {
                    SerializerUtils.writeComponentConfiguration(node, projectDirectory);
                }
                
                final String serviceType = node.getType();
                final String serviceName = node.getProperties().getValue("code_name");
                
                // Call generate artifacts for only service types that are in current ifl.
                if (iflServiceTypes.contains(serviceType)) {
                    logger.fine("#### Generating service artifacts for " + serviceName + "[" + serviceType + "]");
                    ApplicationUtils.generateServiceArtifacts(locator, appBuilder, ifl, usedFiles, serviceType, serviceName);
                } else if ("aws.s3".equals(serviceType)) {
                    logger.fine("### generating aws.s3 service artifacts");
                    generateAWSS3ServiceArtifacts(locator, appBuilder, ifl, usedFiles, node);
                } else {
                    logger.fine("#### Can not generate service artifacts. IFL does not contain the service declaration " + serviceName + "[" + serviceType + "]");
                }

                // Write other artifacts that user edited. This means overwriting the
                // default artifacts generated by the system with the ones edited
                // directly from the web.
                SerializerUtils.writeComponentArtifacts(node, projectDirectory);
            }

            // Generate service unit artifacts for the service types that does not
            // support per service artifacts generator. java and xslt so far uses the
            // deprecated interface.
            // TODO: remove it when the deprecated support is removed.
            generateServiceUnitsWithDeprecatedPackager(locator, appBuilder, ifl, usedFiles);

            // Now generate application minus the service units.
            ApplicationUtils.generateFlowConstructs (ifl, projectDirectory, usedFiles);
            ApplicationUtils.generateFlowTypeExtensions (ifl, projectDirectory, usedFiles);

            // Serialize the UI model of the message flow, so we can read it back later
            // if we need it.
            SerializerUtils.writeGraphUiModel(graph, projectDirectory);
        } catch (Exception e) {
            throw new SerializerException(e);
        } finally {
            // Benchmark
            logger.log(Level.FINE, "MavenSerializerService.serialize: " +
                    (System.currentTimeMillis() - start) + " ms.");
        }
    }

    public Graph deserialize(
            final String graphCodeName,
            final File directory) throws SerializerException {

        long start = System.currentTimeMillis();

        try {
            final File projectDirectory = new File(directory, graphCodeName);

            if (!projectDirectory.exists()) {
                throw new SerializerException("The project directory does not exist.");
            }

            final IFLModel ifl =
                    new IFLReader().read(new File(projectDirectory, "src/main/ifl"));

            final Graph iflGraph =
                    SerializerUtils.parseGraphFromIfl(graphCodeName, ifl, projectDirectory);
            final Graph uiModelGraph =
                    SerializerUtils.parseGraphFromUiModel(projectDirectory);
            
            if (uiModelGraph != null) {
                uiModelGraph.update(
                        uiModelGraph.diffTo(iflGraph, new FujiGraphComparator()));
                Utils.normalize(uiModelGraph);
                Utils.layoutGraph(uiModelGraph);
                return uiModelGraph;
            } else {
                Utils.normalize(iflGraph);
                Utils.layoutGraph(iflGraph);
                return iflGraph;
            }
        } catch (Exception e) {
            throw new SerializerException(e);
        } finally {
            // Benchmark
            logger.log(Level.FINE, "MavenSerializerService.deserialize: " +
                    (System.currentTimeMillis() - start) + " ms.");
        }
    }

    public Graph deserialize(
            final String codeName,
            final String source) throws SerializerException {
        
        try {
            final IFLModel ifl =
                    new IFLReader().read(new StringReader(source));

            return SerializerUtils.parseGraphFromIfl(codeName, ifl, null);
        } catch (IOException e) {
            throw new SerializerException(e);
        } catch (IFLParseException e) {
            throw new SerializerException(e);
        }
    }

    public String getArtifact(
            final Graph graph,
            final File directory,
            final List<String> artifactPaths) throws SerializerException {

        final File projectDirectory = new File(directory, graph.getCodeName());

        try {
            // First we serialize the graph into that directory. If the directory already
            // exists we consider it safe to clean it.
            if (projectDirectory.exists()) {
                MavenUtils.deleteDirectory(projectDirectory);
            }

            serialize(graph, directory);

            for (String path: artifactPaths) {
                final File artifactFile = new File(projectDirectory, path);

                if (artifactFile.exists() && artifactFile.isFile()) {
                    return MavenUtils.readFile(artifactFile);
                }
            }

            return null;
        } catch (IOException e) {
            throw new SerializerException(e);
        }
    }

    public List<String> getArtifactPaths(
            final Node node,
            final String artifact) {

        return SerializerUtils.getArtifactPaths(node, artifact);
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    /**
     * generate the aws as3 artifacts which involves generating artifacts for
     * multiple services ( 2 jruby and 1 rest ).
     */
    private void generateAWSS3ServiceArtifacts(
            ServiceToolsLocator locator,
            AppBuildManager appBuilder,
            IFLModel ifl,
            List usedFiles,
            Node node) throws IOException {

        String serviceName = node.getProperties().getValue("code_name");
        //COMPOSITE TYPE. need to generate service artifacts for multiple services that constitue the composite type.
        String restType = "rest";
        String jrubyType = "jruby";
        String awsS3PreProcess = serviceName + "-preprocess"; //jruby service
        String awsS3PostProcess = serviceName + "-postprocess"; // jruny service
        String awsS3Invoke = serviceName + "-invoke"; // rest service
        List<String> rubyServiceList = ifl.getServicesForType(jrubyType);
        List<String> restServiceList = ifl.getServicesForType(restType);
        if (rubyServiceList.contains(awsS3PreProcess)) {
            ApplicationUtils.generateServiceArtifacts(locator, appBuilder, ifl, usedFiles, jrubyType, awsS3PreProcess);
        }
        if (rubyServiceList.contains(awsS3PostProcess)) {
            ApplicationUtils.generateServiceArtifacts(locator, appBuilder, ifl, usedFiles, jrubyType, awsS3PostProcess);
        }
        if (restServiceList.contains(awsS3Invoke)) {
            ApplicationUtils.generateServiceArtifacts(locator, appBuilder, ifl, usedFiles, restType, awsS3Invoke);
        }
    }

    /**
     * Since the generate service artifacts per service is not supported by the
     * deprecated service pacakger, we need to call generate service units at the
     * end of per service artifacts generation to generate the artifacts for the
     * services that support only deprecated pacakger.
     *
     */
    private static void generateServiceUnitsWithDeprecatedPackager(
            final ServiceToolsLocator locator,
            final AppBuildManager appBuilder,
            final IFLModel ifl,
            final List usedFiles) throws IOException {

        for (String serviceType : ifl.getServiceTypes()) {
            ServiceProjectModel prjModel = appBuilder.getServiceProjectModel(serviceType);
            // generate the artifacts
            ServiceProjectModel svcPrjModel = appBuilder.getServiceProjectModel(serviceType);
            ServiceGenerator serviceGen = locator.getServiceGenerator(serviceType);
            if (serviceGen instanceof ServiceSPIWrapper) {
                logger.fine("#### Generating service unit with deprecated service packager for " + serviceType);
                serviceGen.generateServiceUnit(prjModel, ifl);
                List<String> services = ifl.getServicesForType(serviceType);
                for (String serviceName : services ) {
                    usedFiles.addAll(appBuilder.findUsedServiceArtifacts(svcPrjModel, serviceName));
                }
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String DEFAULT_BUNDLE_NAME_PATTERN =
            "Integration Application -- {0}"; // NOI18N
}
