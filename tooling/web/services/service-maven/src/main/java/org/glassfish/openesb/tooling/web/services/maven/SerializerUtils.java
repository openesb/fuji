/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.maven;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.maven.util.MavenUtils;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.glassfish.openesb.tooling.web.model.graph.Property;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author ksorokin
 */
public final class SerializerUtils {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static final Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    public static void generateIfl(
            final Graph graph,
            final File file) throws IOException {

        final PrintWriter writer = new PrintWriter(new FileWriter(file));

        try {
            // First we list all participating components
            for (Node node : graph.getNodes()) {
                // If the node is an EIP or a shortcut, we skip it.
                if (!SPECIAL_TYPES.contains(node.getType()) &&
                        !COMPOSITE_TYPES.contains(node.getType()) &&
                        (node.getShortcutTo() == null)) {

                    writer.println(node.getType() + " \"" +
                            node.getProperties().get("code_name") + "\"");
                }

                // For the aws.s3 we need to list two services: the jruby signature
                // generating thingie and the actual REST call.
                if ("aws.s3".equals(node.getType()) && (node.getShortcutTo() == null)) {
                    writer.println("jruby \"" +
                            node.getProperties().get("code_name") + "-preprocess\"");
                    writer.println("rest \"" +
                            node.getProperties().get("code_name") + "-invoke\"");

                    // We don't really need the post-process step for PUT or DELETE, as
                    // these are not expected to return any value.
                    if ("GET".equals(node.getProperties().getValue("awss3_operation"))) {
                        writer.println("jruby \"" +
                                node.getProperties().get("code_name") + "-postprocess\"");
                    }
                }
            }

            writer.println();

            // Then we find the starting points and start building routes from each of 
            // them.
            for (Node node : graph.getStartingPoints()) {
                if (node.getParent() == null) {
                    printRoute(node, writer, null);
                    writer.println();
                }
            }

            // Then we print all the named routes.
            for (Node node : graph.getStartingPoints()) {
                if (node.getParent() != null) {
                    printRoute(
                            node,
                            writer,
                            node.getParent().getProperties().getValue("code_name"));
                    writer.println();
                }
            }

            // Then we print the named routes for the "compound" components.
            for (Node node : graph.getNodes()) {
                if ("aws.s3".equals(node.getType()) && (node.getShortcutTo() == null)) {
                    final String codeName = node.getProperties().getValue("code_name");

                    writer.println("route \"" + codeName + "\" do");
                    writer.println("  to \"" + codeName + "-preprocess\"");
                    writer.println("  to \"" + codeName + "-invoke\"");

                    if ("GET".equals(node.getProperties().getValue("awss3_operation"))) {
                        writer.println("  to \"" + codeName + "-postprocess\"");
                    }

                    writer.println("end");
                    writer.println();
                }
            }
        } finally {
            writer.close();
        }
    }

    public static void printRoute(
            final Node node,
            final PrintWriter writer,
            final String codeName) {

        // If we're not a named route, we will not print anything if the route consists 
        // of a single service (no outbound links, or they lead nowhere) -- it does not 
        // make sense. Again, if not a named route we do not print anythign beginning 
        // with a special (EIP) node.
        if (codeName == null) {
            final List<Link> links = node.getOutboundLinks();
            if (links.size() == 0) {
                return;
            } else {
                boolean proceed = false;
                for (Link link : links) {
                    if (link.getEndNode() != null) {
                        proceed = true;
                        break;
                    }
                }

                if (!proceed) {
                    return;
                }
            }

            if (SPECIAL_TYPES.contains(node.getType())) {
                return;
            }
        }

        if (codeName == null) {
            writer.println("route do");
            writer.println("  from \"" + node.getProperties().get("code_name") + "\"");
        } else {
            writer.println("route \"" + codeName + "\" do");
            printNode(node, writer, "  ");
        }

        printMidpoint(node, writer, "  ");

        writer.println("end");
    }

    public static void printMidpoint(
            final Node startNode,
            final PrintWriter writer,
            final String indent) {

        if (startNode.getOutboundLinks().size() > 0) {
            // TODO Here we assume that there is only one outbound link, unless the 
            // node is a 'broadcast' or a 'tee' control
            if (BROADCAST.equals(startNode.getType())) {
                writer.println("" + indent + "broadcast do");

                for (Link link : startNode.getOutboundLinks()) {
                    final Node endNode = link.getEndNode();

                    // If the node is null, this means that the link attached to a 
                    // broadcast leads nowhere and we do not need to handle this.
                    if (endNode != null) {
                        writer.println("" + indent + "  route do");

                        printNode(endNode, writer, indent + "    ");
                        printMidpoint(endNode, writer, indent + "    ");

                        writer.println("" + indent + "  end");
                    }
                }

                writer.println("" + indent + "end");
            } else if (TEE.equals(startNode.getType())) {
                if (startNode.getOutboundLinks().size() > 1) {
                    final Node teeEndNode =
                            startNode.getOutboundLinks().get(1).getEndNode();

                    if (teeEndNode != null) {
                        writer.print("" + indent + "tee ");
                        printNode(teeEndNode, writer, "");
                    }
                }

                final Node endNode = startNode.getOutboundLinks().get(0).getEndNode();

                if (endNode != null) {
                    printNode(endNode, writer, indent);
                    printMidpoint(endNode, writer, indent);
                }
            } else if (SELECT.equals(startNode.getType())) {
                Link elseLink = null;

                final String exprType =
                        startNode.getProperties().getValue("expression_type");
                final String expression =
                        startNode.getProperties().getValue("expression");
                final boolean useExternalConfig = Boolean.parseBoolean(
                        startNode.getProperties().getValue("use_external_config"));
                final String externalConfigName =
                        startNode.getProperties().getValue("external_config_name");

                if (!useExternalConfig) {
                    writer.println("" + indent +
                            "select " + exprType + " (\"" + expression + "\") do");
                } else {
                    writer.println("" + indent +
                            "select " + exprType + " \"" + externalConfigName + "\" do");
                }
                if (startNode.getOutboundLinks().size() > 0) {
                    for (int i = 0; i < startNode.getOutboundLinks().size(); i++) {
                        final Link link = startNode.getOutboundLinks().get(i);

                        if (link.getStartNodeConnector() == 0) {
                            elseLink = link;
                            continue;
                        }

                        final Node endNode = link.getEndNode();

                        if (endNode != null) {
                            final String when = startNode.getProperty(
                                    "case_" + link.getStartNodeConnector()).toString();

                            writer.print("" + indent +
                                    "  when \"" + when + "\" ");
                            printNode(endNode, writer, "");
                        }
                    }

                    if (elseLink != null) {
                        final Node endNode = elseLink.getEndNode();

                        if (endNode != null) {
                            writer.print("" + indent + "  else ");
                            printNode(endNode, writer, "");
                        }
                    }
                }
                writer.println("" + indent + "end");
            } else {
                final Node endNode = startNode.getOutboundLinks().get(0).getEndNode();

                // If the node is null this means that the link is going nowhere and 
                // we're at the end already.
                if (endNode != null) {
                    printNode(endNode, writer, indent);
                    printMidpoint(endNode, writer, indent);
                }
            }
        }
    }

    private static void printNode(
            final Node node,
            final PrintWriter writer,
            final String indent) {

        // If the end node is a broadcast control, we should skip
        // outputting it.
        if (BROADCAST.equals(node.getType())) {
            // Do nothing for broadcasts.
        } else if (TEE.equals(node.getType())) {
            // Do nothing for tees.
        } else if (SELECT.equals(node.getType())) {
            // Do nothing for selects.
        } else if (SPLIT.equals(node.getType())) {
            final String exprType =
                    node.getProperties().getValue("expression_type");
            final String expression =
                    node.getProperties().getValue("expression");
            final boolean useExternalConfig = Boolean.parseBoolean(
                    node.getProperties().getValue("use_external_config"));
            final String externalConfigName =
                    node.getProperties().getValue("external_config_name");

            if (!useExternalConfig) {
                writer.println("" + indent +
                        "split " + exprType + " (\"" + expression + "\")");
            } else {
                writer.println("" + indent +
                        "split " + exprType + " \"" + externalConfigName + "\"");
            }
        } else if (AGGREGATE.equals(node.getType())) {
            final String exprType =
                    node.getProperties().getValue("expression_type");
            final String expression =
                    node.getProperties().getValue("expression");
            final String clazz =
                    node.getProperties().getValue("classname");
            final String pakkage =
                    node.getProperties().getValue("packagename");
            final boolean useExternalConfig = Boolean.parseBoolean(
                    node.getProperties().getValue("use_external_config"));
            final String externalConfigName =
                    node.getProperties().getValue("external_config_name");

            if (!useExternalConfig) {
                if ("set".equals(exprType)) {
                    writer.println("" + indent +
                            "aggregate " + exprType + " (\"" + expression + "\")");
                } else if ("java".equals(exprType)) {
                    writer.println("" + indent +
                            "aggregate " + exprType + " (\"packagename=" + pakkage +
                            ",classname=" + clazz + "\")");
                } else {
                    writer.println("" + indent +
                            "aggregate " + exprType + "");
                }
            } else {
                writer.println("" + indent +
                        "aggregate " + exprType + " \"" + externalConfigName + "\"");
            }
        } else if (FILTER.equals(node.getType())) {
            final String exprType =
                    node.getProperties().getValue("expression_type");
            final String expression =
                    node.getProperties().getValue("expression");
            final boolean useExternalConfig = Boolean.parseBoolean(
                    node.getProperties().getValue("use_external_config"));
            final String externalConfigName =
                    node.getProperties().getValue("external_config_name");

            if (!useExternalConfig) {
                writer.println("" + indent +
                        "filter " + exprType + " (\"" + expression + "\")");
            } else {
                writer.println("" + indent +
                        "filter " + exprType + " \"" + externalConfigName + "\"");
            }
        } else {
            Node realNode = node;
            String codeName = realNode.getProperties().getValue("code_name");
            while ((codeName == null) && (realNode.getShortcutTo() != null)) {
                realNode = realNode.getShortcutTo();
                codeName = realNode.getProperties().getValue("code_name");
            }

            writer.println("" + indent + "to \"" + codeName + "\"");
        }
    }

    public static void writeComponentConfiguration(
            final Node node,
            final File mavenProject) throws IOException {

        // There is no need to print configuration for shortcuts.
        if (node.getShortcutTo() != null) {
            return;
        }

        final File[] configs;
        if ("jruby".equals(node.getType())) {
            configs = new File[]{new File(mavenProject, "src/main/jruby/" +
                        node.getProperties().getValue("code_name") + "/service.rb")};
        } else if ("xslt".equals(node.getType())) {
            configs = new File[]{new File(mavenProject, "src/main/xslt/" +
                        node.getProperties().getValue("code_name") + "/transform.xsl")};
        } else if ("java".equals(node.getType())) {
            configs = new File[]{new File(mavenProject, "src/main/java/pojo/" +
                         "/Svc" + node.getProperties().getValue("code_name") + ".java")};
        } else if ("aws.s3".equals(node.getType())) {
            final String codeName = node.getProperties().getValue("code_name");

            if ("GET".equals(node.getProperties().getValue("awss3_operation"))) {
                configs = new File[]{
                            new File(mavenProject,
                            "src/main/jruby/" + codeName + "-preprocess/service.rb"),
                            new File(mavenProject,
                            "src/main/config/rest/" + codeName + "-invoke/service.properties"),
                            new File(mavenProject,
                            "src/main/jruby/" + codeName + "-postprocess/service.rb")
                        };
            } else {
                configs = new File[]{
                            new File(mavenProject,
                            "src/main/jruby/" + codeName + "-preprocess/service.rb"),
                            new File(mavenProject,
                            "src/main/config/rest/" + codeName + "-invoke/service.properties")
                        };
            }
        } else {
            configs = new File[]{new File(mavenProject, "src/main/config/" +
                        node.getType() + "/" + node.getProperties().getValue("code_name") +
                        "/service.properties")};
        }

        final PrintWriter[] writers = new PrintWriter[configs.length];
        for (int i = 0; i < configs.length; i++) {
            if (!configs[i].exists()) {
                configs[i].getParentFile().mkdirs();
            }

            writers[i] = new PrintWriter(new FileWriter(configs[i]));
        }

        try {
            if ("java".equals(node.getType())) {
                writers[0].print(node.getProperties().getValue("java_code"));
            } else

            if ("jruby".equals(node.getType())) {
                writers[0].print(node.getProperties().getValue("jruby_code"));
            } else

            if ("xslt".equals(node.getType())) {
                writers[0].print(node.getProperties().getValue("xslt_code"));
            } else

            if ("aws.s3".equals(node.getType())) {
                final String preprocessCode = AWS_S3_JRUBY_PREPROCESS_TEMPLATE.
                        replace("${-----bucket}", "" + node.getProperty("awss3_bucket")).
                        replace("${-----path}", "" + node.getProperty("awss3_path")).
                        replace("${-----operation}", "" + node.getProperty("awss3_operation")).
                        replace("${-----accesskey}", "" + node.getProperty("awss3_accesskey")).
                        replace("${-----secretkey}", "" + node.getProperty("awss3_secretkey")).
                        replace("${-----graph-code-name}", "" + node.getGraph().getCodeName());

                writers[0].println(preprocessCode);

                writers[1].println("rest.http.resourceURL=");
                writers[1].println("rest.http.method=" +
                        node.getProperties().getValue("awss3_operation"));
                writers[1].println("rest.http.acceptTypes=[\"*/*\"]");
                writers[1].println("rest.http.contentType=application/octet-stream");
                writers[1].println("rest.http.headers={}");
                writers[1].println("rest.http.paramStyle=QUERY");
                writers[1].println("rest.http.params={}");

                if ("GET".equals(node.getProperties().getValue("awss3_operation"))) {
                    final String postprocessCode = AWS_S3_JRUBY_POSTPROCESS_TEMPLATE.
                            replace("${-----bucket}", "" + node.getProperty("awss3_bucket")).
                            replace("${-----path}", "" + node.getProperty("awss3_path")).
                            replace("${-----operation}", "" + node.getProperty("awss3_operation")).
                            replace("${-----accesskey}", "" + node.getProperty("awss3_accesskey")).
                            replace("${-----secretkey}", "" + node.getProperty("awss3_secretkey")).
                            replace("${-----graph-code-name}", "" + node.getGraph().getCodeName());

                    writers[2].println(postprocessCode);
                }
            } else

            {
                // Write any other component types configuration which are not hardcoded
                // above. The configuraton of these types will be handled by the
                // type-specific plugin.
                writeComponentConfiguration(node, writers[0]);
            }
        } finally {
            for (int i = 0; i < writers.length; i++) {
                writers[i].close();
            }
        }
    }

    /**
     * Writes out the node configuration properties after filtering the
     * type specific properties and converting the '_' to '.' in the property names.
     */
    public static void writeComponentConfiguration(
            final Node node,
            final PrintWriter out) throws IOException {

        // Configuration properties definition convention. It is simple. By convention,
        // the separator character in the properties files is '.', while in web tooling,
        // due to javascript peculiarities it is -- '_'. We need to replace on with the
        // other.
        // Additionally, we need to filter out properties which are not related to the
        // service. We're interested in two types of them: thos which start with <type>
        // and those which start with 'service'.

        final String typePrefix = node.getType() + "_";
        final String servicePrefix = "service_";
        for (Property property : node.getProperties().getProperties()) {
            String propName = property.getName();
            String propValue;
            if (propName.startsWith(typePrefix) || propName.startsWith(servicePrefix)) {
                propName = propName.replace("_", ".");
                
                propValue = property.getValue();
                if (propValue == null) {
                    propValue = "";
                }
                propValue = propValue.replace("\n", "\\n");

                out.println(propName + "=" + propValue);
                logger.fine("Config prop " + propName + "=" + propValue);
            } else {
                // Not a config property. May be it is a node ui specific
                // property. Skip it.
            }
        }
    }

    public static Properties readComponentConfiguration(
            final String codeName,
            final String type,
            final File mavenProject) throws IOException {

        // No, we won't read EIPs configuration here.
        if (SPECIAL_TYPES.contains(type)) {
            return new Properties();
        }

        File[] configs;
        Properties tempProperties = new Properties();
        Properties resultProperties = new Properties();

        if ("jruby".equals(type)) {
            configs = new File[]{new File(mavenProject, "src/main/jruby/" +
                        codeName + "/service.rb")};
        } else if ("xslt".equals(type)) {
            configs = new File[]{new File(mavenProject, "src/main/xslt/" +
                        codeName + "/transform.xsl")};
        } else if ("java".equals(type)) {
            configs = new File[]{new File(mavenProject, "src/main/java/pojo" +
                        "/Svc" + codeName + ".java")};
        } else {
            configs = new File[]{new File(mavenProject, "src/main/config/" +
                        type + "/" + codeName + "/service.properties")};
        }

        final InputStream[] inputs = new InputStream[configs.length];
        for (int i = 0; i < configs.length; i++) {
            inputs[i] = new FileInputStream(configs[i]);
        }

        try {
            if ("database".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("jdbc_operation_type",
                        tempProperties.getProperty("jdbc.operationType", ""));
                resultProperties.setProperty("jdbc_jndi_name",
                        tempProperties.getProperty("jdbc.jndiName", ""));
                resultProperties.setProperty("jdbc_sql",
                        tempProperties.getProperty("jdbc.sql", ""));
                resultProperties.setProperty("jdbc_param_order",
                        tempProperties.getProperty("jdbc.paramOrder", ""));
                resultProperties.setProperty("jdbc_table_name",
                        tempProperties.getProperty("jdbc.TableName", ""));
            }

            if ("email".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("email_type",
                        tempProperties.getProperty("email.type", ""));

                // Common.
                resultProperties.setProperty("email_emailServer",
                        tempProperties.getProperty("email.emailServer", ""));
                resultProperties.setProperty("email_useSSL",
                        tempProperties.getProperty("email.useSSL", ""));
                resultProperties.setProperty("email_port",
                        tempProperties.getProperty("email.port", ""));
                resultProperties.setProperty("email_userName",
                        tempProperties.getProperty("email.userName", ""));
                resultProperties.setProperty("email_password",
                        tempProperties.getProperty("email.password", ""));

                // SMTP.
                resultProperties.setProperty("email_location",
                        tempProperties.getProperty("smtp.location", ""));
                resultProperties.setProperty("email_charset",
                        tempProperties.getProperty("smtp.charset", ""));
                resultProperties.setProperty("email_use",
                        tempProperties.getProperty("smtp.use", ""));
                resultProperties.setProperty("email_encodingStyle",
                        tempProperties.getProperty("smtp.encodingStyle", ""));

                // IMAP.
                resultProperties.setProperty("email_mailFolder",
                        tempProperties.getProperty("pop3.or.imap.mailFolder", ""));

                // POP3 or IMAP.
                resultProperties.setProperty("email_maxMessageCount",
                        tempProperties.getProperty("pop3.or.imap.maxMessageCount", ""));
                resultProperties.setProperty("email_messageAckMode",
                        tempProperties.getProperty("pop3.or.imap.messageAckMode", ""));
                resultProperties.setProperty("email_messageAckOperation",
                        tempProperties.getProperty("pop3.or.imap.messageAckOperation", ""));
                resultProperties.setProperty("email_pollingInterval",
                        tempProperties.getProperty("pop3.or.imap.pollingInterval", ""));
            }

            if ("file".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("file_use",
                        tempProperties.getProperty("file.use", ""));
                resultProperties.setProperty("file_file_name",
                        tempProperties.getProperty("file.fileName", ""));
                resultProperties.setProperty("file_polling_interval",
                        tempProperties.getProperty("file.pollingInterval", ""));
                resultProperties.setProperty("file_file_directory",
                        tempProperties.getProperty("file.fileDirectory", ""));
                resultProperties.setProperty("file_file_name_is_pattern",
                        tempProperties.getProperty("file.fileNameIsPattern", ""));
                resultProperties.setProperty("file_multiple_records_per_file",
                        tempProperties.getProperty("file.multipleRecordsPerFile", ""));
                resultProperties.setProperty("file_record_delimiter",
                        tempProperties.getProperty("file.recordDelimiter", ""));
            }

            if ("ftp".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("ftp_receive_from",
                        tempProperties.getProperty("ftp.receiveFrom", ""));
                resultProperties.setProperty("ftp_send_to",
                        tempProperties.getProperty("ftp.sendTo", ""));
                resultProperties.setProperty("ftp_user",
                        tempProperties.getProperty("ftp.user", ""));
                resultProperties.setProperty("ftp_password",
                        tempProperties.getProperty("ftp.password", ""));
                resultProperties.setProperty("ftp_host",
                        tempProperties.getProperty("ftp.host", ""));
                resultProperties.setProperty("ftp_port",
                        tempProperties.getProperty("ftp.port", ""));
                resultProperties.setProperty("ftp_dir_list_style",
                        tempProperties.getProperty("ftp.dirListStyle", ""));
                resultProperties.setProperty("ftp_mode",
                        tempProperties.getProperty("ftp.mode", ""));
                resultProperties.setProperty("ftp_poll_interval_millis",
                        tempProperties.getProperty("ftp.pollIntervalMillis", ""));
            }

            if ("http".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("http_location",
                        tempProperties.getProperty("http.location", ""));
            }

            if ("java".equals(type)) {
                resultProperties.setProperty("java_code",
                        readReader(new BufferedReader(new InputStreamReader(inputs[0], "UTF-8"))));
            }

            if ("jms".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("jms_destination",
                        tempProperties.getProperty("jms.destination", ""));
                resultProperties.setProperty("jms_destination_type",
                        tempProperties.getProperty("jms.destinationType", ""));
                resultProperties.setProperty("jms_message_type",
                        tempProperties.getProperty("jms.messageType", ""));
                resultProperties.setProperty("jms_text_part",
                        tempProperties.getProperty("jms.textPart", ""));
                resultProperties.setProperty("jms_connection_url",
                        tempProperties.getProperty("jms.connectionURL", ""));
            }

            if ("jruby".equals(type)) {
                resultProperties.setProperty("jruby_code",
                        readReader(new BufferedReader(new InputStreamReader(inputs[0], "UTF-8"))));
            }

            if ("rest".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("http_resource_url",
                        tempProperties.getProperty("http.resource-url", ""));
                resultProperties.setProperty("http_method",
                        tempProperties.getProperty("http.method", ""));
                resultProperties.setProperty("http_accept_types",
                        tempProperties.getProperty("http.accept-types", "").replace("\\\n", "\n"));
                resultProperties.setProperty("http_content_type",
                        tempProperties.getProperty("http.content-type", ""));
                resultProperties.setProperty("http_headers",
                        tempProperties.getProperty("http.headers", "").replace("\\\n", "\n"));
                resultProperties.setProperty("http_param_style",
                        tempProperties.getProperty("http.param-style", ""));
                resultProperties.setProperty("http_params",
                        tempProperties.getProperty("http.params", "").replace("\\\n", "\n"));
                resultProperties.setProperty("http_basic_auth_username",
                        tempProperties.getProperty("http.basic-auth-username", ""));
                resultProperties.setProperty("http_basic_auth_password",
                        tempProperties.getProperty("http.basic-auth-password", ""));
            }

            if ("rss".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("rss_address",
                        tempProperties.getProperty("rss.address", ""));
                resultProperties.setProperty("rss_polling_interval",
                        tempProperties.getProperty("rss.pollingInterval", ""));
                resultProperties.setProperty("rss_filter_by_type",
                        tempProperties.getProperty("rss.filterByType", ""));
                resultProperties.setProperty("rss_filter_by_value",
                        tempProperties.getProperty("rss.filterByValue", ""));
                resultProperties.setProperty("rss_entries_per_message",
                        tempProperties.getProperty("rss.entriesPerMessage", ""));
            }

            if ("scheduler".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("scheduler_type",
                        tempProperties.getProperty("scheduler.type", ""));
                resultProperties.setProperty("scheduler_message",
                        tempProperties.getProperty("scheduler.message", ""));
                resultProperties.setProperty("scheduler_starting",
                        tempProperties.getProperty("scheduler.starting", ""));
                resultProperties.setProperty("scheduler_ending",
                        tempProperties.getProperty("scheduler.ending", ""));
                resultProperties.setProperty("scheduler_timezone",
                        tempProperties.getProperty("scheduler.timezone", ""));

                resultProperties.setProperty("scheduler_interval",
                        tempProperties.getProperty("scheduler.interval", ""));
                resultProperties.setProperty("scheduler_repeat",
                        tempProperties.getProperty("scheduler.repeat", ""));

                resultProperties.setProperty("scheduler_cron_expr",
                        tempProperties.getProperty("scheduler.cron-expr", ""));

                resultProperties.setProperty("scheduler_duration",
                        tempProperties.getProperty("scheduler.duration", ""));
            }

            if ("xmpp".equals(type)) {
                tempProperties.load(inputs[0]);

                resultProperties.setProperty("xmpp_domain",
                        tempProperties.getProperty("xmpp.domain", ""));
                resultProperties.setProperty("xmpp_port",
                        tempProperties.getProperty("xmpp.port", ""));
                resultProperties.setProperty("xmpp_username",
                        tempProperties.getProperty("xmpp.username", ""));
                resultProperties.setProperty("xmpp_password",
                        tempProperties.getProperty("xmpp.password", ""));
                resultProperties.setProperty("xmpp_jabber_id",
                        tempProperties.getProperty("xmpp.jabberId", ""));
            }

            if ("xslt".equals(type)) {
                resultProperties.setProperty("xslt_code",
                        readReader(new BufferedReader(new InputStreamReader(inputs[0], "UTF-8"))));
            }

            return resultProperties;
        } finally {
            for (int i = 0; i < inputs.length; i++) {
                inputs[i].close();
            }
        }
    }

    public static void writeComponentArtifacts(
            final Node node,
            final File mavenProject) throws IOException {

        // There is no need to generate artifacts for shortcuts.
        if (node.getShortcutTo() != null) {
            return;
        }

        writeArtifacts(node, mavenProject);
    }

    public static void writeApplicationArtifacts(
            final Graph graph,
            final File mavenProject) throws IOException {

        writeArtifacts(graph, mavenProject);
    }

    public static List<String> getArtifactPaths(
            final Node node,
            final String artifact) {

        if (node == null) {
            if (IFL_SOURCE.equals(artifact)) {
                return Arrays.asList(new String[]{
                            "src/main/ifl/app.ifl"
                        });
            }

            return new LinkedList<String>();
        } else {
            final String name = node.getProperties().getValue("code_name");
            final String type = node.getType();

            if (BROADCAST.equals(node.getType())) {
                // There are no external artifacts for broadcast.
                return new LinkedList<String>();
            } else if (SPLIT.equals(node.getType())) {
                if (EXTERNAL_CONFIG.equals(artifact)) {
                    final String configName =
                            node.getProperties().getValue("external_config_name");

                    return Arrays.asList(new String[]{
                                "src/main/split/" + configName + "/flow.properties"
                            });
                } else {
                    return new LinkedList<String>();
                }
            } else if (AGGREGATE.equals(node.getType())) {
                if (EXTERNAL_CONFIG.equals(artifact)) {
                    final String configName =
                            node.getProperties().getValue("external_config_name");

                    return Arrays.asList(new String[]{
                                "src/main/aggregate/" + configName + "/flow.properties"
                            });
                } else if (JAVA_SOURCE.equals(artifact)) {
                    final boolean useExternal = Boolean.parseBoolean(
                            node.getProperties().getValue("use_external_config"));
                    final String externalName =
                            node.getProperties().getValue("external_config_name");

                    String pakkage = node.getProperties().getValue("packagename");
                    String clazz = node.getProperties().getValue("classname");

                    if (useExternal) {
                        final String eConfig = node.getArtifact(EXTERNAL_CONFIG);
                        final Properties eProperties = new Properties();

                        if (eConfig != null) {
                            try {
                                eProperties.load(new ByteArrayInputStream(
                                        eConfig.getBytes("UTF-8")));

                                if (eProperties.getProperty("packagename") != null) {
                                    pakkage = eProperties.getProperty("packagename");
                                }

                                if (eProperties.getProperty("classname") != null) {
                                    clazz = eProperties.getProperty("classname");
                                }
                            } catch (IOException e) {
                                logger.log(
                                        Level.SEVERE,
                                        "Failed to read from a ByteArrayInputStream. " +
                                                "Weird...",
                                        e);
                            }
                        } else {
                            // If we've been set up to use external configuration, but
                            // the corresponding artifact is not present -- apply the
                            // default values.
                            clazz = "Aggregator";
                            pakkage = "aggregate." + externalName;

                            pakkage = pakkage.replaceAll("[\\W&&[^\\x2E]]", "_");
                        }
                    }

                    return Arrays.asList(new String[]{
                                "src/main/java/" +
                                pakkage.replace(".", "/") + "/" + clazz + ".java"
                            });
                } else {
                    return new LinkedList<String>();
                }
            } else if (FILTER.equals(node.getType())) {
                if (EXTERNAL_CONFIG.equals(artifact)) {
                    final String configName =
                            node.getProperties().getValue("external_config_name");

                    return Arrays.asList(new String[]{
                                "src/main/filter/" + configName + "/flow.properties"
                            });
                } else {
                    return new LinkedList<String>();
                }
            } else if (TEE.equals(node.getType())) {
                return new LinkedList<String>();
            } else if (SELECT.equals(node.getType())) {
                if (EXTERNAL_CONFIG.equals(artifact)) {
                    final String configName =
                            node.getProperties().getValue("external_config_name");

                    return Arrays.asList(new String[]{
                                "src/main/cbr/" + configName + "/flow.properties"
                            });
                } else {
                    return new LinkedList<String>();
                }
            } else {
                if (MESSAGE_XSD.equals(artifact)) {
                    return Arrays.asList(new String[]{
                                "src/main/resources/" + type + "/" + name + "/message.xsd",
                                "src/main/resources/wsdl/message.xsd"
                            });
                } else if (INTERFACE_WSDL.equals(artifact)) {
                    return Arrays.asList(new String[]{
                                "src/main/resources/" + type + "/" + name + "/interface.wsdl",
                                "src/main/resources/wsdl/interface.wsdl"
                            });
                } else if (BINDING_WSDL.equals(artifact)) {
                    return Arrays.asList(new String[]{
                                "src/main/resources/" + type + "/" + name + "/binding.wsdl",
                                "src/main/resources/wsdl/binding.wsdl"
                            });
                } else {
                    return new LinkedList<String>();
                }
            }
        }
    }

    public static PropertySet toPropertySet(
            final Properties properties) {

        final PropertySet set = new PropertySet();

        if (properties != null) {
            for (Object key : properties.keySet()) {
                set.set(key.toString(), properties.get(key));
            }
        }

        return set;
    }

    public static void writeGraphUiModel(
            final Graph graph,
            final File mavenProject) throws IOException {

        final File serializedModelFile =
                new File(mavenProject, "web-tooling-model.xml");
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(serializedModelFile, "UTF-8");

            writer.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            writer.println("<graph " +
                    "code-name=\"" + escapeForXml(graph.getCodeName()) + "\" " +
                    "display-name=\"" + escapeForXml(graph.getDisplayName()) + "\">");
            writer.println("  <description>" + escapeForXml(graph.getDescription()) + "</description>");
            writer.println("  ");

            writer.println("  <nodes>");
            for (Node node: graph.getNodes()) {
                Node parent = node.getParent();
                Node shortcutTo = node.getShortcutTo();

                writer.println("    <node " +
                        "id=\"" + node.getId() + "\" " +
                        "code-name=\"" + escapeForXml(node.getProperties().getValue("code_name")) + "\" " +
                        "type=\"" + escapeForXml(node.getType()) + "\" " +
                        "parent=\"" + (parent == null ? "" : parent.getProperties().getValue("code_name")) + "\" " +
                        "shortcut-to=\"" + (shortcutTo == null ? "" : shortcutTo.getProperties().getValue("code_name")) + "\">");
                writer.println("      <properties>");
                for (Property property: node.getProperties()) {
                    if ("code_name".equals(property.getName())) {
                        continue;
                    }

                    writer.println("        <property " +
                            "name=\"" + escapeForXml(property.getName()) + "\" " +
                            "type=\"" + escapeForXml(property.getType()) + "\">" + escapeForXml(property.getValue()) + "</property>");
                }
                writer.println("      </properties>");
                writer.println("      <ui-properties>");
                for (Property property: node.getUiProperties()) {
                    writer.println("        <property " +
                            "name=\"" + escapeForXml(property.getName()) + "\" " +
                            "type=\"" + escapeForXml(property.getType()) + "\">" + escapeForXml(property.getValue()) + "</property>");
                }
                writer.println("      </ui-properties>");
                writer.println("    </node>");
            }
            writer.println("  </nodes>");

            writer.println("  <links>");
            for (Link link: graph.getLinks()) {
                writer.print("    <link " +
                        "id=\"" + link.getId() + "\" " +
                        "type=\"" + escapeForXml(link.getType()) + "\"");
                if (link.getStartNode() != null) {
                    writer.print(" start-node=\"" + link.getStartNode().getId() + "\"");
                    writer.print(" start-node-connector=\"" + link.getStartNodeConnector() + "\"");
                }
                if (link.getEndNode() != null) {
                    writer.print(" end-node=\"" + link.getEndNode().getId() + "\"");
                    writer.print(" end-node-connector=\"" + link.getEndNodeConnector() + "\"");
                }
                writer.println(">");

                writer.println("      <properties>");
                for (Property property: link.getProperties()) {
                    writer.println("        <property " +
                            "name=\"" + escapeForXml(property.getName()) + "\" " +
                            "type=\"" + escapeForXml(property.getType()) + "\">" + escapeForXml(property.getValue()) + "</property>");
                }
                writer.println("      </properties>");
                writer.println("      <ui-properties>");
                for (Property property: link.getUiProperties()) {
                    writer.println("        <property " +
                            "name=\"" + escapeForXml(property.getName()) + "\" " +
                            "type=\"" + escapeForXml(property.getType()) + "\">" + escapeForXml(property.getValue()) + "</property>");
                }
                writer.println("      </ui-properties>");
                writer.println("    </link>");
            }
            writer.println("  </links>");

            writer.println("</graph>");

            writer.flush();
        } finally {
            writer.close();
        }
    }

    public static Graph parseGraphFromUiModel(
            final File mavenProject) {

        final File serializedModelFile =
                new File(mavenProject, "web-tooling-model.xml");

        if (!serializedModelFile.exists()) {
            return null;
        }

        try {
            final Graph graph = new Graph("", "", "", "0", new Date(), null);

            final DocumentBuilderFactory domFactory =
                    DocumentBuilderFactory.newInstance();
            final DocumentBuilder domBuilder =
                    domFactory.newDocumentBuilder();
            final Document dom =
                    domBuilder.parse(serializedModelFile);


            final Element graphElement = dom.getDocumentElement();

            final String codeName = graphElement.getAttribute("code-name");
            final String displayName = graphElement.getAttribute("display-name");

            graph.setCodeName(codeName);
            graph.setDisplayName(displayName);

            org.w3c.dom.Node nodesElement = null;
            org.w3c.dom.Node linksElement = null;
            final NodeList graphChildElements = graphElement.getChildNodes();
            for (int i = 0; i < graphChildElements.getLength(); i++) {
                org.w3c.dom.Node node = graphChildElements.item(i);

                if ("description".equals(node.getNodeName())) {
                    graph.setDescription(node.getTextContent());
                }

                if ("nodes".equals(node.getNodeName())) {
                    nodesElement = node;
                }

                if ("links".equals(node.getNodeName())) {
                    linksElement = node;
                }
            }

            if ((nodesElement != null) && (linksElement != null)) {
                parseNodesAndLinks(graph, nodesElement, linksElement);
            }
            
            return graph;
        } catch (ParserConfigurationException e) {
            logger.log(Level.WARNING,
                    "Failed to parse the graph serialized UI model.", e);
        } catch (SAXException e) {
            logger.log(Level.WARNING,
                    "Failed to parse the graph serialized UI model.", e);
        } catch (DOMException e) {
            logger.log(Level.WARNING,
                    "Failed to parse the graph serialized UI model.", e);
        } catch (IOException e) {
            logger.log(Level.WARNING,
                    "Failed to parse the graph serialized UI model.", e);
        } catch (Exception e) {
            // Not the best thing to do, but we'll end up here if something goes
            // too wrong. A malformed XML is one of these cases.
            logger.log(Level.WARNING,
                    "Failed to parse the graph serialized UI model.", e);
        }

        return null;
    }

    public static Graph parseGraphFromIfl(
            final String graphCodeName,
            final IFLModel ifl,
            final File mavenProject) throws IOException {
        
        final Graph graph =
                new Graph(graphCodeName, "", "", "0", new Date(), null);

        // We run through all declared routes (this does not exactly correspond to
        // the routes explicitly defined by the user in the IFL file), creating
        // nodes and links as appropriate. This would give us an initial set of
        // nodes and links. It will not however create links from broadcasts,
        // tees and selects, as these tend to lead to "artificial" routes, and
        // are generally defined in a non-standard manner.
        final Map<String, Map.Entry<String, List<String>>> routes = ifl.getRoutes();
        for (String routeName: routes.keySet()) {
            final Map.Entry<String, List<String>> routeDef = routes.get(routeName);

            final String fromService =
                    routeDef.getKey();
            final List<String> routeServices =
                    new ArrayList<String>(routeDef.getValue());

            if (fromService != null) {
                routeServices.add(0, fromService);
            }

            Node currentNode = checkAndCreateNode(
                    routeServices.get(0), graph, ifl, mavenProject);
            Node nextNode;

            for (int i = 1; i < routeServices.size(); i++) {
                nextNode = checkAndCreateNode(
                        routeServices.get(i), graph, ifl, mavenProject);

                final Link link = new Link(
                        0, "default",
                        currentNode, 0,
                        nextNode, 0,
                        new PropertySet(), new PropertySet());
                graph.add(link);

                currentNode = nextNode;
            }
        }

        // Now a second run. Through broadcasts, tees and selects in order to create
        // links that were not present in the routes set. Hackish, if you ask me..
        final Map<String, Map<String, Object>> broadcasts = ifl.getBroadcasts();
        final Map<String, Map<String, Object>> selects = ifl.getSelects();
        final Map<String, Map<String, Object>> tees = ifl.getTees();

        for (String codeName: broadcasts.keySet()) {
            Map<String, Object> broadcastDef = broadcasts.get(codeName);
            Node broadcast = graph.getNodeByCodeName(codeName);

            if (broadcast == null) {
                // This is in fact an epic fail, the node should exist.
                continue;
            }

            // Look at the code names of the broadcast connections. For each --
            // check whether a node with such code name exists. If it does not, it
            // means that the broadcast refers to an implicit route.
            List<String> toList = (List<String>) broadcastDef.get("TO");
            for (int i = 0; i < toList.size(); i++) {
                String to = toList.get(i);
                Node toNode = graph.getNodeByCodeName(to);

                if (toNode == null) {
                    Map.Entry<String, List<String>> route = routes.get(to);
                    if (route == null) {
                        // In this case tee is referencing a node which we haven't
                        // encountered before. Weird as it is, but true.
                        toNode = createNode(
                                to, getServiceType(to, ifl), graph, mavenProject);
                    } else {
                        List<String> routeDef = routes.get(to).getValue();

                        to = routes.get(to).getValue().get(0);
                        toNode = graph.getNodeByCodeName(to);

                        // Now we need to find a node with the proper set of links 
                        // (that correspond to the route referred to by the
                        // broadcast).
                        if (!checkNodeForRoute(toNode, routeDef, 1)) {
                            List<Node> shortcutsToNode = graph.getShortcutsTo(toNode);
                            for (Node shortcut: shortcutsToNode) {
                                if (checkNodeForRoute(shortcut, routeDef, 1)) {
                                    toNode = shortcut;
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    // So, the broadcast refers directly to a node. In this case we
                    // need to find the node (or a shortcut to it) which does not
                    // have any links.
                    //
                    // Now check whether the node we selected already has an incoming
                    // (or an outgoing) link. If it does we should search for a vacant
                    // shortcut. After this operation we should have a node that is
                    // ready to be linked with the broadcast.
                    if (graph.getLinksByNode(toNode).size() > 0) {
                        List<Node> shortcutsToNode = graph.getShortcutsTo(toNode);
                        for (Node shortcut: shortcutsToNode) {
                            if (graph.getLinksByNode(shortcut).size() == 0) {
                                toNode = shortcut;
                                break;
                            }
                        }
                    }
                }

                if (toNode == null) {
                    // We're in deep trouble. This should not happen.
                } else {
                    Link link = new Link(0, "default",
                            broadcast, i,
                            toNode, 0,
                            new PropertySet(), new PropertySet());

                    graph.add(link);
                }
            }
        }

        for (String codeName: tees.keySet()) {
            Map<String, Object> teeDef = tees.get(codeName);
            Node tee = graph.getNodeByCodeName(codeName);

            String to = (String) teeDef.get("TO");

            Node toNode = graph.getNodeByCodeName(to);
            if (toNode == null) {
                Map.Entry<String, List<String>> route = routes.get(to);
                if (route == null) {
                    // In this case tee is referencing a node which we haven't
                    // encountered before. Weird as it is, but true.
                    toNode = createNode(
                            to, getServiceType(to, ifl), graph, mavenProject);
                } else {
                    List<String> routeDef = routes.get(to).getValue();

                    to = routes.get(to).getValue().get(0);
                    toNode = graph.getNodeByCodeName(to);

                    // So, the tee refers to an implicit route. So we need to
                    // find a node with the proper set of links (that correspond to
                    // the route referred to by the tee).
                    if (!checkNodeForRoute(toNode, routeDef, 1)) {
                        List<Node> shortcutsToNode = graph.getShortcutsTo(toNode);
                        for (Node shortcut: shortcutsToNode) {
                            if (checkNodeForRoute(shortcut, routeDef, 1)) {
                                toNode = shortcut;
                                break;
                            }
                        }
                    }
                }
            } else {
                // So, the tee refers directly to a node. In this case we
                // need to find the node (or a shortcut to it) which does not
                // have any links.
                // Now check whether the node we selected already has an incoming
                // (or an outgoing) link. If it does we should search for a vacant
                // shortcut. After this operation we should have a node that is
                // ready to be linked with the broadcast.
                if (graph.getLinksByNode(toNode).size() > 0) {
                    List<Node> shortcutsToNode = graph.getShortcutsTo(toNode);
                    for (Node shortcut: shortcutsToNode) {
                        if (graph.getLinksByNode(shortcut).size() == 0) {
                            toNode = shortcut;
                            break;
                        }
                    }
                }
            }

            if (toNode == null) {
                // We're in deep trouble. This should not happen.
            } else {
                Link link = new Link(0, "default",
                        tee, 1,
                        toNode, 0,
                        new PropertySet(), new PropertySet());

                graph.add(link);
            }
        }

        // TODO: We need to begin handling selects properly. !!!!!!!
        
        return graph;
    }

    // Private ---------------------------------------------------------------------------
    /**
     * Checks whether the given node is a good candidate for the given route, i.e. this
     * method analyzes the links of this node and tries to correlate them with the
     * codenames list present in the route definition. If the match is successful, the
     * node is considered to be a proper member of the route.
     *
     * @param node node to check for being part of the route.
     * @param routeDef route definition (list of services code names).
     * @param index position of the node in the route definition, all checks will be
     *      performed for services after this position.
     * @return Whether the node is part of the route or not.
     */
    private static boolean checkNodeForRoute(
            final Node node,
            final List<String> routeDef,
            final int index) {

        // If we're not at the end of the route yet, but the node has no outgoing
        // connections.
        if ((index < routeDef.size()) && (node.getOutboundLinks().size() == 0)) {
            return false;
        }

        // If we're at the end of the route, but the node still has outgoing conections.
        if ((index == routeDef.size()) && (node.getOutboundLinks().size() > 0)) {
            return false;
        }

        // If we're at the end of the route and the node has no outgoing connections.
        if ((index == routeDef.size()) && (node.getOutboundLinks().size() == 0)) {
            return true;
        }

        // If we're not at the end of the route and the node has outgoign connections,
        // check whether at least one of the "next" nodes has the code name which is
        // defined next in the route definition. After that -- continue checks for the
        // "next" node.
        for (Link link: node.getOutboundLinks()) {
            if ((link.getEndNode() != null) &&
                    routeDef.get(index).equals(getCodeName(link.getEndNode()))) {

                return checkNodeForRoute(link.getEndNode(), routeDef, index + 1);
            }
        }

        // If there were no matches in the previous loop -- the node does not belong to
        // the route.
        return false;
    }

    /**
     * Returns the code name of a node. If the node does not have a code name of its own,
     * but is instead a shortcut, the "target" node's code name is returned.
     *
     * @param node node whose code name should be returned.
     * @return Code name of the given node.
     */
    private static String getCodeName(
            final Node node) {

        String codeName = node.getProperties().getValue("code_name");

        if ((codeName == null) && (node.getShortcutTo() != null)) {
            return getCodeName(node.getShortcutTo());
        }

        return codeName;
    }

    /**
     * Returns the type of the given service (identified by its code name). This method
     * checks all available "services" sources, be it proper services, or collections of
     * EIPs.
     *
     * @param codeName code name of the service whose type should be retrieved.
     * @param ifl {@link IFLModel} of the message flow.
     * @return Type of the given service, or <kbd>null</kbd> if this service cannot be
     *      found in the message flow.
     */
    private static String getServiceType(
            final String codeName,
            final IFLModel ifl) {

        Set<String> types = ifl.getServiceTypes();

        Map<String, Map.Entry<String, List<String>>> routes = ifl.getRoutes();
        Map<String, Map<String, Object>> aggregates = ifl.getAggregates();
        Map<String, Map<String, Object>> broadcasts = ifl.getBroadcasts();
        Map<String, Map<String, Object>> filters = ifl.getFilters();
        Map<String, Map<String, Object>> selects = ifl.getSelects();
        Map<String, Map<String, Object>> splits = ifl.getSplits();
        Map<String, Map<String, Object>> tees = ifl.getTees();

        for (String type: types) {
            if (ifl.getServicesForType(type).contains(codeName)) {
                return type;
            }
        }

        if (routes.keySet().contains(codeName)) {
            return "route";
        }

        if (aggregates.keySet().contains(codeName)) {
            return "aggregate";
        }

        if (broadcasts.keySet().contains(codeName)) {
            return "broadcast";
        }

        if (filters.keySet().contains(codeName)) {
            return "filter";
        }

        if (selects.keySet().contains(codeName)) {
            return "select";
        }

        if (splits.keySet().contains(codeName)) {
            return "split";
        }

        if (tees.keySet().contains(codeName)) {
            return "tee";
        }

        return null;
    }

    /**
     * Creates a new node or a shortcut to an existing one.
     *
     * @param codeName code name of the service which should be represented by the node
     *      being created.
     * @param graph graph to which the new node should be added.
     * @param ifl
     * @param mavenProject
     * @return
     * @throws IOException
     */
    private static Node checkAndCreateNode(
            final String codeName,
            final Graph graph,
            final IFLModel ifl,
            final File mavenProject) throws IOException {

        Node node = graph.getNodeByCodeName(codeName);

        if (node == null) {
            final String type = getServiceType(codeName, ifl);

            if ("route".equals(type)) {
                // Unless there is a dollar sign in the code name, we create it.
                // Otherwise we simply ignore. For now.
                if (!codeName.contains("$")) {
                    node = createNode(codeName, type, graph, mavenProject);
                }
            } else {
                node = createNode(codeName, type, graph, mavenProject);
            }

            return node;
        } else {
            Node shortcut = new Node(
                    0, node.getType(), new PropertySet(), new PropertySet());
            shortcut.setShortcutTo(node);
            graph.add(shortcut);

            return shortcut;
        }
    }

    private static Node createNode(
            final String codeName,
            final String type,
            final Graph graph,
            final File mavenProject) throws IOException {

        Properties properties = null;

        if (mavenProject != null) {
            properties = SerializerUtils.readComponentConfiguration(
                    codeName, type, mavenProject);
        }

        Node node = new Node(0, type,
                SerializerUtils.toPropertySet(properties), new PropertySet());
        node.getProperties().set("code_name", codeName);

        graph.add(node);

        return node;
    }
    
    private static void parseNodesAndLinks(
            final Graph graph,
            final org.w3c.dom.Node nodesElement,
            final org.w3c.dom.Node linksElement) {

        // Tpp -- To Post Process
        final List<Node> shortcutsTpp = new LinkedList<Node>();
        final List<String> shortcutsTppTargets = new LinkedList<String>();
        final List<Node> childrenTpp = new LinkedList<Node>();
        final List<String> childrenTppParents = new LinkedList<String>();

        final NodeList nodesList = nodesElement.getChildNodes();
        for (int i = 0; i < nodesList.getLength(); i++) {
            final org.w3c.dom.Node nodeElement = nodesList.item(i);

            if ("node".equals(nodeElement.getNodeName())) {
                final Integer id = new Integer(nodeElement.
                        getAttributes().getNamedItem("id").getTextContent());
                final String codeName = nodeElement.
                        getAttributes().getNamedItem("code-name").getTextContent();
                final String type = nodeElement.
                        getAttributes().getNamedItem("type").getTextContent();
                final String shortcutTo = nodeElement.
                        getAttributes().getNamedItem("shortcut-to").getTextContent();
                final String parent = nodeElement.
                        getAttributes().getNamedItem("parent").getTextContent();

                final Node node = 
                        new Node(id, type, new PropertySet(), new PropertySet());
                node.getProperties().set("code_name", codeName);

                final NodeList nodeChildren = nodeElement.getChildNodes();
                for (int j = 0; j < nodeChildren.getLength(); j++) {
                    final org.w3c.dom.Node nodeChildElement = nodeChildren.item(j);

                    if ("properties".equals(nodeChildElement.getNodeName())) {
                        parseProperties(node.getProperties(), nodeChildElement);
                    }

                    if ("ui-properties".equals(nodeChildElement.getNodeName())) {
                        parseProperties(node.getUiProperties(), nodeChildElement);
                    }
                }

                graph.add(node);

                if (!"".equals(shortcutTo)) {
                    Node target = graph.getNodeByCodeName(shortcutTo);
                    if (target == null) {
                        shortcutsTpp.add(node);
                        shortcutsTppTargets.add(shortcutTo);
                    } else {
                        node.setShortcutTo(target);
                    }
                }

                if (!"".equals(parent)) {
                    Node target = graph.getNodeByCodeName(parent);
                    if (target == null) {
                        childrenTpp.add(node);
                        childrenTppParents.add(parent);
                    } else {
                        node.setParent(target);
                    }
                }
            }
        }

        for (int i = 0; i < shortcutsTpp.size(); i++) {
            final Node target = graph.getNodeByCodeName(shortcutsTppTargets.get(i));

            if (target != null) {
                shortcutsTpp.get(i).setShortcutTo(target);
            } else {
                // Ooooops. This should not happen, really.
            }
        }

        for (int i = 0; i < childrenTpp.size(); i++) {
            final Node target = graph.getNodeByCodeName(childrenTppParents.get(i));

            if (target != null) {
                childrenTpp.get(i).setParent(target);
            } else {
                // Ooooops. This should not happen, really.
            }
        }

        final NodeList linksList = linksElement.getChildNodes();
        for (int i = 0; i < linksList.getLength(); i++) {
            final org.w3c.dom.Node linkElement = linksList.item(i);

            if ("link".equals(linkElement.getNodeName())) {
                final Integer id = new Integer(linkElement.
                        getAttributes().getNamedItem("id").getTextContent());
                final String type = linkElement.
                        getAttributes().getNamedItem("type").getTextContent();

                Node startNode = null;
                int startNodeConnector = 0;
                if (linkElement.getAttributes().getNamedItem("start-node") != null) {
                    startNode = graph.getNodeById(new Integer(linkElement.
                        getAttributes().getNamedItem("start-node").getTextContent()));
                    startNodeConnector = Integer.parseInt(linkElement.
                        getAttributes().getNamedItem("start-node-connector").getTextContent());
                }

                Node endNode = null;
                int endNodeConnector = 0;
                if (linkElement.getAttributes().getNamedItem("end-node") != null) {
                    endNode = graph.getNodeById(new Integer(linkElement.
                        getAttributes().getNamedItem("end-node").getTextContent()));
                    endNodeConnector = Integer.parseInt(linkElement.
                        getAttributes().getNamedItem("end-node-connector").getTextContent());
                }

                final Link link = new Link(id, type,
                        startNode, startNodeConnector,
                        endNode, endNodeConnector,
                        new PropertySet(), new PropertySet());

                final NodeList linkChildren = linkElement.getChildNodes();
                for (int j = 0; j < linkChildren.getLength(); j++) {
                    final org.w3c.dom.Node nodeChildElement = linkChildren.item(j);

                    if ("properties".equals(nodeChildElement.getNodeName())) {
                        parseProperties(link.getProperties(), nodeChildElement);
                    }

                    if ("ui-properties".equals(nodeChildElement.getNodeName())) {
                        parseProperties(link.getUiProperties(), nodeChildElement);
                    }
                }

                graph.add(link);
            }
        }

    }

    private static void parseProperties(
            final PropertySet properties,
            final org.w3c.dom.Node propertiesElement) {

        final NodeList propertiesList = propertiesElement.getChildNodes();
        for (int i = 0; i < propertiesList.getLength(); i++) {
            final org.w3c.dom.Node node = propertiesList.item(i);

            if ("property".equals(node.getNodeName())) {
                final String name =
                        node.getAttributes().getNamedItem("name").getTextContent();
                final String type =
                        node.getAttributes().getNamedItem("type").getTextContent();
                final String value =
                        node.getTextContent();

                properties.set(name, value, type);
            }
        }
    }

    private static void writeArtifacts(
            final Graph graph,
            final File mavenProject) throws IOException {

        final Map<String, String> artifacts = graph.getArtifacts();

        for (String name : artifacts.keySet()) {
            final String path = getArtifactPaths(null, name).get(0);
            final File artifactFile = new File(mavenProject, path);

            if (!artifactFile.exists()) {
                artifactFile.getParentFile().mkdirs();
            }

            MavenUtils.writeFile(artifacts.get(name), artifactFile);
        }
    }

    private static void writeArtifacts(
            final Node node,
            final File mavenProject) throws IOException {

        final Map<String, String> artifacts = node.getArtifacts();

        for (String name : artifacts.keySet()) {
            final String path = getArtifactPaths(node, name).get(0);
            final File artifactFile = new File(mavenProject, path);

            if (!artifactFile.exists()) {
                artifactFile.getParentFile().mkdirs();
            }

            MavenUtils.writeFile(artifacts.get(name), artifactFile);
        }
    }

    private static String readReader(
            final BufferedReader reader) throws IOException {

        StringBuilder result = new StringBuilder();
        String line = null;

        while ((line = reader.readLine()) != null) {
            result.append(line).append("\n");
        }

        return result.toString();
    }

    private static String escapeForXml(
            final String unescaped) {

        if (unescaped == null) {
            return "";
        }

        String escaped = unescaped;
        escaped = escaped.replace("&", "&amp;");
        escaped = escaped.replace("<", "&lt;");
        escaped = escaped.replace(">", "&gt;");
        escaped = escaped.replace("\"", "&quot;");

        return escaped;
    }

    private static String unescapeFromXml(
            final String escaped) {

        if (escaped == null) {
            return "";
        }

        String unescaped = escaped;
        unescaped = unescaped.replace("&quot;", "\"");
        unescaped = unescaped.replace("&gt;", ">");
        unescaped = unescaped.replace("&lt;", "<");
        unescaped = unescaped.replace("&amp;", "&");

        return unescaped;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private SerializerUtils() {
        // Does nothing
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final String BROADCAST =
            "broadcast"; // NOI18N
    public static final String SPLIT =
            "split"; // NOI18N
    public static final String AGGREGATE =
            "aggregate"; // NOI18N
    public static final String FILTER =
            "filter"; // NOI18N
    public static final String TEE =
            "tee"; // NOI18N
    public static final String SELECT =
            "select"; // NOI18N
    public static final String ROUTE =
            "route"; // NOI18N
    public static final List<String> SPECIAL_TYPES = Arrays.asList(
            BROADCAST,
            SPLIT,
            AGGREGATE,
            FILTER,
            TEE,
            SELECT,
            ROUTE);
    private static final List<String> COMPOSITE_TYPES = Arrays.asList(
            "aws.s3");
    public static final String IFL_SOURCE =
            "ifl-source";
    public static final String EXTERNAL_CONFIG =
            "external-config";
    public static final String JAVA_SOURCE =
            "java-source";
    public static final String MESSAGE_XSD =
            "message-xsd";
    public static final String INTERFACE_WSDL =
            "interface-wsdl";
    public static final String BINDING_WSDL =
            "binding-wsdl";
    public static final String INDENT =
            "  ";
    private static final String AWS_S3_JRUBY_PREPROCESS_TEMPLATE =
            "require 'java'\n" +
            "require 'XPath'\n" +
            "require 'ESB'\n" +
            "\n" +
            "include_class \"java.util.logging.Level\"\n" +
            "include_class \"java.util.logging.Logger\"\n" +
            "include_class \"javax.crypto.Mac\"\n" +
            "include_class \"javax.crypto.spec.SecretKeySpec\"\n" +
            "include_class \"javax.xml.namespace.QName\"\n" +
            "include_class \"sun.misc.BASE64Encoder\"\n" +
            "include_class \"java.io.StringWriter\"\n" +
            "include_class \"javax.xml.transform.TransformerFactory\"\n" +
            "include_class \"javax.xml.transform.Transformer\"\n" +
            "include_class \"javax.xml.transform.dom.DOMSource\"\n" +
            "include_class \"javax.xml.transform.stream.StreamResult\"\n" +
            "\n" +
            "MyLogger = Logger.getLogger \"JRuby S3 Signature Generator\"\n" +
            "\n" +
            "def process(inMsg)\n" +
            "  payload = transform_to_string inMsg.getPayload\n" +
            "\n" +
            "  dynamic_path = inMsg.getProperty(\"org.glassfish.openesb.aws.s3.path\");\n" +
            "\n" +
            "  bucket_name = \"${-----bucket}\"\n" +
            "  path = dynamic_path != nil ? dynamic_path : \"${-----path}\"\n" +
            "\n" +
            "  MyLogger.log Level::INFO, \"AWS S3: Bucket: \" + bucket_name\n" +
            "  MyLogger.log Level::INFO, \"AWS S3: Path: \" + path\n" +
            "\n" +
            "  http_verb = \"${-----operation}\"\n" +
            "  content_md5 = \"\"\n" +
            "  content_type = \"application/octet-stream\"\n" +
            "  date = httpdate Time.now.utc\n" +
            "  canonicalized_resource = \"/\" + bucket_name + path\n" +
            "\n" +
            "  access_key = \"${-----accesskey}\"\n" +
            "  secret_key = \"${-----secretkey}\"\n" +
            "\n" +
            "  string_to_sign = http_verb + \"\\n\" + content_md5 + \"\\n\" +\n" +
            "    content_type + \"\\n\" + date + \"\\n\" + canonicalized_resource\n" +
            "\n" +
            "  signature = sign string_to_sign, secret_key\n" +
            "\n" +
            "  host = \"#{bucket_name}.s3.amazonaws.com\"\n" +
            "  url = \"http://#{host}\" + path\n" +
            "\n" +
            "  headers =\n" +
            "    \"{ \" +\n" +
            "      \"\\\"Host\\\": \\\"#{host}\\\", \" +\n" +
            "      \"\\\"Authorization\\\": \\\"AWS #{access_key}:#{signature}\\\", \" +\n" +
            "      \"\\\"User-Agent\\\": \\\"OpenESB REST BC\\\" \" +\n" +
            "    \"}\"\n" +
            "  MyLogger.log Level::INFO, headers\n" +
            "\n" +
            "  inMsg.setProperty \"org.glassfish.openesb.rest.url\", url\n" +
            "  inMsg.setProperty \"org.glassfish.openesb.rest.date\", date\n" +
            "  inMsg.setProperty \"org.glassfish.openesb.rest.headers\", headers\n" +
            "  inMsg.setProperty \"org.glassfish.openesb.rest.entity\", payload\n" +
            "\n" +
            "  # return message\n" +
            "  inMsg\n" +
            "end\n" +
            "\n" +
            "RFC2822_DAY_NAME = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\n" +
            "RFC2822_MONTH_NAME = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',\n" +
            "  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']\n" +
            "\n" +
            "def httpdate(date)\n" +
            "  sprintf('%s, %02d %s %d %02d:%02d:%02d GMT',\n" +
            "    RFC2822_DAY_NAME[date.wday],\n" +
            "    date.day,\n" +
            "    RFC2822_MONTH_NAME[date.mon-1],\n" +
            "    date.year,\n" +
            "    date.hour,\n" +
            "    date.min,\n" +
            "    date.sec)\n" +
            "end\n" +
            "\n" +
            "def sign(data, secret_key)\n" +
            "  data_bytes = java.lang.String.new(data).getBytes \"UTF8\"\n" +
            "  secret_key_bytes = java.lang.String.new(secret_key).getBytes \"UTF8\"\n" +
            "\n" +
            "  mac = Mac.getInstance \"HmacSHA1\"\n" +
            "  signing_key = SecretKeySpec.new secret_key_bytes, \"HmacSHA1\"\n" +
            "  mac.init signing_key\n" +
            "\n" +
            "  encoder = BASE64Encoder.new\n" +
            "\n" +
            "  signed_bytes = mac.doFinal data_bytes\n" +
            "\n" +
            "  encoder.encode signed_bytes\n" +
            "end\n" +
            "\n" +
            "def transform_to_string(payload)\n" +
            "  transformer = TransformerFactory.newInstance.newTransformer\n" +
            "  dom_source = DOMSource.new(payload)\n" +
            "  writer = StringWriter.new\n" +
            "  stream_result = StreamResult.new(writer)\n" +
            "  transformer.transform(dom_source, stream_result)\n" +
            "\n" +
            "  java.lang.String.new(writer.toString())\n" +
            "end\n";
    private static final String AWS_S3_JRUBY_POSTPROCESS_TEMPLATE =
            "require 'java'\n" +
            "require 'XPath'\n" +
            "require 'ESB'\n" +
            "include_class \"javax.xml.transform.stream.StreamSource\"\n" +
            "include_class \"java.io.ByteArrayInputStream\"\n" +
            "include_class \"java.io.BufferedReader\"\n" +
            "include_class \"java.io.InputStreamReader\"\n" +
            "\n" +
            "def process(inMsg)\n" +
            "  content = \"<jbi:message version=\\\"1.0\\\" type=\\\"msgns:anyMsg\\\" xmlns:jbi=\\\"http://java.sun.com/xml/ns/jbi/wsdl-11-wrapper\\\" xmlns:msgns=\\\"http://fuji.dev.java.net/application/${-----graph-code-name}\\\" xmlns:xop=\\\"http://www.w3.org/2004/08/xop/include\\\"><jbi:part><xop:Include href=\\\"org.glassfish.openesb.rest.attachment\\\"/></jbi:part></jbi:message>\"\n" +
            "  inMsg.setContent(StreamSource.new(ByteArrayInputStream.new(java.lang.String.new(content).getBytes())))\n" +
            "  \n" +
            "  inMsg\n" +
            "end\n";
}
