/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.maven;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.maven.util.ApplicationUtils;
import com.sun.jbi.fuji.maven.util.MavenUtils;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
// This will make Fuji build JDK 1.6 and above dependent.
//import javax.tools.DiagnosticCollector;
//import javax.tools.JavaCompiler;
//import javax.tools.JavaFileObject;
//import javax.tools.StandardJavaFileManager;
//import javax.tools.ToolProvider;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.glassfish.openesb.tooling.web.services.BuilderException;
import org.glassfish.openesb.tooling.web.services.BuilderService;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public final class MavenBuilderService implements BuilderService, Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private BundleContext context;
    private ServiceTracker registryTracker;
    private ServiceTracker toolsLookupTracker;

    public MavenBuilderService(
            final BundleContext context,
            final ServiceTracker registryTracker,
            ServiceTracker toolsLookupTracker) {

        this.context = context;
        this.registryTracker = registryTracker;
        this.toolsLookupTracker = toolsLookupTracker;
    }

    public void clean(
            final String codeName,
            final File directory) throws BuilderException {

        long start = System.currentTimeMillis();

        try {
            final File projectDirectory = new File(directory, codeName);
            final File targetDirectory = new File(projectDirectory, "target");

            MavenUtils.deleteDirectory(targetDirectory);
        } catch (IOException e) {
            throw new BuilderException(e);
        } finally {
            // Benchmark
            logger.log(Level.FINE, "MavenBuilderService.clean: " +
                    (System.currentTimeMillis() - start) + " ms.");
        }
    }

//================= JDK 1.5 code. ========
    private void compileUsingJDK5(File subdir, final String classpath, final File classesDirectory)
            throws IllegalArgumentException, ClassNotFoundException,
            SecurityException, InvocationTargetException,
            IOException, IllegalAccessException {
        boolean compiledClassPresent = false;

        for (File serviceJava : subdir.listFiles()) {
            if ((serviceJava.exists() && serviceJava.getName().endsWith(".java"))) {
                compiledClassPresent = true;
                Class javac = Class.forName("com.sun.tools.javac.Main");
                Method[] methods = javac.getMethods();
                for (Method method : methods) {
                    if (method.getName().equals("compile") && (method.getParameterTypes().length == 1)) {
                        method.invoke(null, (Object) new String[]{
                                            "-classpath",
                                            classpath,
                                            serviceJava.getAbsolutePath()
                                        });
                    }
                }
            }
        }
        if (compiledClassPresent) {
            moveClassFiles(subdir, new File(classesDirectory, "pojo"));
        }
    }
//========================= JDK 1.5 code - End ========

//============ JDK 1.6 code - Start. ========
    private void compileUsingJDK6(File subdir, final String classpath, final File classesDirectory)
            throws IllegalArgumentException, ClassNotFoundException,
            SecurityException, InvocationTargetException, IOException, IllegalAccessException, NoSuchMethodException, InstantiationException {
        boolean compiledClassPresent = false;

        // Classes needed
        final Class tp = Class.forName("javax.tools.ToolProvider");
        final Class jc = Class.forName("javax.tools.JavaCompiler");
        final Class dl = Class.forName("javax.tools.DiagnosticListener");
        final Class dc = Class.forName("javax.tools.DiagnosticCollector");
        final Class jfm = Class.forName("javax.tools.JavaFileManager");
        final Class sjfm = Class.forName("javax.tools.StandardJavaFileManager");
        final Class jc_ct = Class.forName("javax.tools.JavaCompiler$CompilationTask");

        //Methods needed
        final Method getSystemJavaCompiler = tp.getMethod("getSystemJavaCompiler");
        final Method getStandardFileManager = jc.getMethod("getStandardFileManager",
                dl, Locale.class, Charset.class);
        final Method getJavaFileObjects = sjfm.getMethod("getJavaFileObjects", File[].class);
        final Method getTask = jc.getMethod("getTask", java.io.Writer.class, jfm, dl,
                Iterable.class, Iterable.class, Iterable.class);
        final Method call = jc_ct.getMethod("call");

        final Object compiler = getSystemJavaCompiler.invoke(null, (Object[])null);
        final Object fileManager = getStandardFileManager.invoke(compiler, null, null, null);
        Object diagnostics = dc.newInstance();

        final List<String> options = new LinkedList<String>(Arrays.asList(new String[] {
            "-classpath",
            classpath
        }));

        for (File serviceJava : subdir.listFiles()) {
            if ((serviceJava.exists() && serviceJava.getName().endsWith(".java"))) {
                compiledClassPresent = true;
                Object[] files = new File[] {
                    serviceJava
                };
                final Object compilationUnits = getJavaFileObjects.invoke(fileManager, new Object[] {files});
                final Object compTask = getTask.invoke(compiler, null, fileManager,
                        diagnostics, options, null, compilationUnits);
               call.invoke(compTask);
            }
        }

        if (compiledClassPresent){
            moveClassFiles(subdir, new File(classesDirectory, "pojo"));
        }
    }
//============ JDK 1.6 code - End ========

//============ JDK 1.6 code - Start. uncomment below code once build is switched to JDK 1.6 ========
//    private void compileUsingJDK6(File subdir, final String classpath, final File classesDirectory)
//            throws IllegalArgumentException, ClassNotFoundException,
//            SecurityException, InvocationTargetException, IOException, IllegalAccessException {
//        boolean compiledClassPresent = false;
//                final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
//                final StandardJavaFileManager fileManager =
//                        compiler.getStandardFileManager(null, null, null);
//                final DiagnosticCollector<JavaFileObject> diagnostics =
//                        new DiagnosticCollector<JavaFileObject>();
//
//                final List<String> options = new LinkedList<String>(Arrays.asList(new String[] {
//                    "-classpath",
//                    classpath
//                }));
//
//                for (File serviceJava : javaSubdirs) {
//                    if (serviceJava.exists()) {
//                        compiledClassPresent = true;
//                        final Iterable<? extends JavaFileObject> compilationUnits =
//                                fileManager.getJavaFileObjects(serviceJava);
//
//                        compiler.getTask(
//                                null,
//                                fileManager,
//                                diagnostics,
//                                options,
//                                null,
//                                compilationUnits).call();
//                    }
//                }
//                if (compiledClassPresent){
//                    moveClassFiles(subdir, new File(classesDirectory, "pojo"));
//                }
//    }
//============ JDK 1.6 build dependednt code - End ========
    
    private void moveClassFiles(File pkgDir, File targetDir) throws IOException{
        if (pkgDir.exists() && pkgDir.isDirectory()){
            File[] classes = pkgDir.listFiles();
            File targetCls = null;

            for (File cls: classes){
                if (cls.isFile() && cls.getName().endsWith(".class")){
                    targetCls = new File(targetDir, cls.getName());
                    MavenUtils.moveFile(cls, targetCls);
                }
            }
        }
    }
    
    public File build(
            final String codeName,
            final File directory) throws BuilderException {

        long start = System.currentTimeMillis();
        
        try {
            final File projectDirectory = new File(directory, codeName);
            final File sourcesDirectory = new File(projectDirectory, "src/main");
            final File targetDirectory = new File(projectDirectory, "target");
            final File classesDirectory = new File(targetDirectory, "classes");
            final File metainfDirectory = new File(classesDirectory, "META-INF");

            targetDirectory.mkdirs();

            // Copy the required stuff.
            final File[] subdirs = sourcesDirectory.listFiles();
            if ((subdirs != null) && (subdirs.length > 0)) {
                for (File subdir : subdirs) {
                    if (subdir.getName().equals("resources")) {
                        MavenUtils.copy(subdir, classesDirectory);
                        continue;
                    }

                    if (subdir.getName().equals("config")) {
                        continue;
                    }

                    if (subdir.getName().equals("ifl")) {
                        MavenUtils.copy(subdir, metainfDirectory);
                        continue;
                    }

                    MavenUtils.copy(
                            subdir, new File(classesDirectory, subdir.getName()));
                }
            }

            // Replace some tokens before actually applying the packaging logic.
            final Properties properties = new Properties();
            properties.put("project.build.directory", targetDirectory.getAbsolutePath());

            MavenUtils.replaceTokensInDirectory(classesDirectory, properties, MavenUtils.DIRS_TO_SKIP);

            // Before actually packaging the application, we need to compile the classes
            // therein. Currently this is only done for POJO SE (that is the 'java/pojo/'
            // subdirectory within 'src/main'.

            File subdir = new File(sourcesDirectory, "java/pojo/");

            final File[] javaSubdirs = subdir.listFiles();

            if ((javaSubdirs != null) && (javaSubdirs.length > 0)) {
                final String classpathPrefix = context.getDataFile(
                        POJO_SE_CLASSPATH_DIRNAME).getAbsolutePath() + "/";
                final String classpath =
                        classpathPrefix + POJO_SE_CLASSPATH_JBI_FRAMEWORK + PATH_SEP +
                        classpathPrefix + POJO_SE_CLASSPATH_FUJI_API + PATH_SEP +
                        classpathPrefix + POJO_SE_CLASSPATH_POJO_API + PATH_SEP;

                String javaVersion = System.getProperty("java.version");
                Pattern vp = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)(\\.*)");
                Matcher m = vp.matcher(javaVersion);
                boolean foundVersion = false;
                int mv = 0;
                if (m.find()){
                    foundVersion = true;
                    mv = Integer.parseInt(m.group(2));
                }

                if (foundVersion){
                    if (mv == 5){ //JDK 1.5
                        compileUsingJDK5(subdir, classpath, classesDirectory);
                    }else if (mv == 6){ //JDK 1.6
                        compileUsingJDK6(subdir, classpath, classesDirectory);
                    }
                }
            }
            
            // Do actually package the application.
            final WebServiceToolsLocator locator = new WebServiceToolsLocator(
                    (RegistryService) registryTracker.getService(),
                    (ToolsLookup)toolsLookupTracker.getService());
            final IFLModel ifl =
                    new IFLReader().read(new File(projectDirectory, "src/main/ifl"));

            String applicationJar = ApplicationUtils.packageApplication(
                    locator, ifl, codeName, null, projectDirectory, targetDirectory);

            return new File(applicationJar);
        } catch (IOException e) {
            throw new BuilderException(e);
        } catch (IFLParseException e) {
            throw new BuilderException(e);
        } catch (Exception e) {
            throw new BuilderException(e);
        }finally {
            // Benchmark
            logger.log(Level.FINE, "MavenBuilderService.build: " +
                    (System.currentTimeMillis() - start) + " ms.");
        }
    }
}
