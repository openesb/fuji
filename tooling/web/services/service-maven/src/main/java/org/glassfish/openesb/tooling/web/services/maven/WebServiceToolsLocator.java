/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.maven;

import com.sun.jbi.fuji.maven.util.AbstractServiceToolsLocator;
import java.util.List;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ToolsLookup;

/**
 *
 * @author ksorokin
 * @author chikkala
 */
public class WebServiceToolsLocator extends AbstractServiceToolsLocator {

    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static final Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private RegistryService registry;

    public WebServiceToolsLocator(
            final RegistryService registry, ToolsLookup toolsLookup) {
        super(toolsLookup);
        this.registry = registry;
    }

    protected URL locateServiceArchetype(String serviceType) throws IOException {
        return registry.getArchetype(serviceType);
    }

    protected URL locateServicePlugin(String serviceType) throws IOException {
        return registry.getPackager(serviceType);
    }

    protected URL locateServiceInstaller(String componentName) throws IOException {
        List<URL> list = registry.getInstallers(componentName);
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
