/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.maven;

/**
 *
 * @author ksorokin
 */
public interface Constants {
    String POJO_SE_CLASSPATH_DIRNAME = "pojo-se-classpath";
    String POJO_SE_CLASSPATH_JBI_FRAMEWORK = "jbi-framework.jar";
    String POJO_SE_CLASSPATH_FUJI_API = "api.jar";
    String POJO_SE_CLASSPATH_POJO_API = "sun-pojo-engine-api.jar";

    // Just a shorthand for the ystem property.
    String PATH_SEP = System.getProperty("path.separator");
}
