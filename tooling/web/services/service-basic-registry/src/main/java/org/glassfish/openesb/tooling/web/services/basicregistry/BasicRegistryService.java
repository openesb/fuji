/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.basicregistry;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.services.registry.TypeDescriptor;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.NamespaceException;
import org.osgi.service.startlevel.StartLevel;

/**
 *
 * @author ksorokin
 */
public class BasicRegistryService implements RegistryService {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private BundleContext context;
    private List<String> availableTypes;
    private Map<String, String> iconUrls;
    private Map<String, String> shadowUrls;
    private Map<String, String> scriptUrls;
    private Map<String, String> scripts;
    private Map<String, List<String>> templates;
    private Map<String, URL> archetypes;
    private Map<String, List<URL>> installers;
    private Map<String, URL> packagers;
    private Map<String, URL> nameToUrl;

    public BasicRegistryService(
            final BundleContext context) throws NamespaceException {

        this.context = context;

        this.availableTypes = new LinkedList<String>();

        this.iconUrls = new HashMap<String, String>();
        this.shadowUrls = new HashMap<String, String>();
        this.scriptUrls = new HashMap<String, String>();

        this.scripts = new HashMap<String, String>();
        this.templates = new HashMap<String, List<String>>();

        this.archetypes = new HashMap<String, URL>();
        this.installers = new HashMap<String, List<URL>>();
        this.packagers = new HashMap<String, URL>();

        this.nameToUrl = new HashMap<String, URL>();
    }

    public List<String> getAvailableTypes() {
        return availableTypes;
    }

    public void installType(
            final TypeDescriptor descriptor) throws Exception {

        final String type = descriptor.getType();

        // Then "register" the web resources URLs
        final List<URL> urls = descriptor.getIcons();
        for (int i = 0; i < urls.size(); i++) {
            final String iconUrl = "img/component-types/" + type + "/icon-" + i + ".png";
            iconUrls.put(type + "-" + i, iconUrl);
            nameToUrl.put(iconUrl, urls.get(i));
        }

        final String scriptUrl = "js/component-types/" + type + "/script.js";
        scriptUrls.put(type, scriptUrl);
        scripts.put(type, descriptor.getScript());

        final String shadowUrl = "img/component-types/" + type + "/shadow.png";
        shadowUrls.put(type, shadowUrl);
        nameToUrl.put(shadowUrl, descriptor.getDragShadow());

        // Then "register" the templates
        templates.put(type, descriptor.getTemplates());

        // Then "register" the archetypes, installers and packagers
        archetypes.put(type, descriptor.getArchetype());
        installers.put(type, descriptor.getInstallers());
        packagers.put(type, descriptor.getPackager());

        // Lastly install the bundles contained in the descriptor
        for (URL url : descriptor.getInstallers()) {
            final StartLevel slService = (StartLevel) context.getService(
                    context.getServiceReference(StartLevel.class.getName()));

            final Bundle bundle = context.installBundle(url.toString(), url.openStream());

            slService.setBundleStartLevel(bundle, 1);
            bundle.start();
        }

        availableTypes.add(type);
    }

    public void uninstallType(
            final TypeDescriptor descriptor) {
    }

    public List<String> getIconUrls(
            final String type) {

        final List<String> urls = new LinkedList<String>();

        int i = 0;
        while (iconUrls.get(type + "-" + i) != null) {
            urls.add(iconUrls.get(type + "-" + i));
            i++;
        }

        return urls;
    }

    public String getDragShadowUrl(
            final String type) {

        return shadowUrls.get(type);
    }

    public String getScriptUrl(
            final String type) {

        return scriptUrls.get(type);
    }

    public String getScript(
            final String type) {

        return scripts.get(type);
    }

    public List<String> getTemplates(
            final String type) {

        return templates.get(type);
    }

    public URL getArchetype(
            final String type) {

        return archetypes.get(type);
    }

    public List<URL> getInstallers(
            final String type) {

        return installers.get(type);
    }

    public URL getPackager(
            final String type) {

        return packagers.get(type);
    }

    public URL resolveToUrl(
            final String name) {

        return nameToUrl.get(name);
    }

    public InputStream resolveToInputStream(
            final String name) {

        for (Map.Entry<String, String> entry: scriptUrls.entrySet()) {
            if (entry.getValue().equals(name)) {
                final String type = entry.getKey();

                try {
                    return new ByteArrayInputStream(scripts.get(type).getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    // Will never happen. Should never happen. We'll return null anyway.
                    // :-)
                }
            }
        }

        return null;
    }
}
