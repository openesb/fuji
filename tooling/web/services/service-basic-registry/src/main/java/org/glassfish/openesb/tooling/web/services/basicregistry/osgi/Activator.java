/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.basicregistry.osgi;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.services.basicregistry.BasicRegistryService;
import org.glassfish.openesb.tooling.web.services.registry.ClasspathTypeDescriptor;
import java.util.Properties;
import org.glassfish.openesb.tooling.web.services.registry.ServiceTypeDescriptor;
import org.glassfish.openesb.tooling.web.services.registry.TypeDescriptor;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 *
 * @author ksorokin
 * @author chikkala
 */
public class Activator implements BundleActivator {

    private static final Logger LOG = Logger.getLogger(Activator.class.getName());
    private ServiceRegistration registration;
    private BundleContext context;

    public void start(
            final BundleContext context) throws Exception {
        this.context = context;
        final RegistryService service = new BasicRegistryService(context);

        registration = context.registerService(
                RegistryService.class.getName(),
                service,
                new Properties());

//        service.installType(new ClasspathTypeDescriptor(
//                "/types/database", "database", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/http", "http", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/java", "java", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/jms", "jms", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/jruby", "jruby", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/rest", "rest", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/scheduler", "scheduler", getClass().getClassLoader()));
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/xslt", "xslt", getClass().getClassLoader()));

        // Install synthetic composite types. E.g. an AWS S3 service is actually a set
        // of three services: jruby -> rest -> jruby.
//        service.installType(new ClasspathTypeDescriptor(
//                "/types/aws.s3", "aws.s3", getClass().getClassLoader()));
        
        try {
            // Install types from already loaded service descriptor bundles.
            installTypesFromServiceDescriptorBundles();
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
        
        // Register the service listeners for service descriptor bundles getting loaded
        // after this bundle.
        registerToolsLookupServiceListener();
    }

    public void stop(
            final BundleContext context) throws Exception {

        registration.unregister();
    }

    /**
     * register the ToolsLookup service listener for any changes in the
     * service descriptor bundle addition or removal.
     */
    private void registerToolsLookupServiceListener() {
        if (context == null) {
            LOG.info("Cannot register ToolsLookupServiceListener: Bundle Context is NULL.");
            return;
        }

        final String filter =
                "(" + Constants.OBJECTCLASS + "=" + ToolsLookup.class.getName() + ")";
        try {
            context.addServiceListener(new ServiceListener() {
                public void serviceChanged(ServiceEvent evt) {
                    if (ServiceEvent.MODIFIED == evt.getType()) {
                        // TODO: log it at fine level
                        LOG.info("Service Modified " + evt.getServiceReference().getProperty(ToolsLookup.PROP_AVAILABLE_SERVICE_TYPES));
                        // TODO: update web ui for deteted service type or the new service type added.
                        try {
                            installTypesFromServiceDescriptorBundles();
                        } catch (Exception ex) {
                            LOG.log(Level.INFO, ex.getMessage(), ex);
                        }
                    }
                }
            }, filter);
        } catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * install types from the service descriptors.
     */
    private void installTypesFromServiceDescriptorBundles() {
        if (context == null) {
            LOG.warning("Cannot register ToolsLookupServiceListener: Bundle Context is NULL.");
            return;
        }
        if (registration == null) {
            LOG.warning("Type Service Registry Service not initialized for installing types");
            return;
        }

        final RegistryService service = (RegistryService)
                context.getService(registration.getReference());
        final List<String> availableTypes = service.getAvailableTypes();

        final ToolsLookup lookup = (ToolsLookup) this.context.getService(
                context.getServiceReference(ToolsLookup.class.getName()));

        for (ServiceDescriptor sd : lookup.getAvailableServiceDescriptors()) {
            String serviceType = sd.getServiceType();
            if (!availableTypes.contains(serviceType)) {
                final TypeDescriptor typeDesc = new ServiceTypeDescriptor(sd);

                try {
                    service.installType(typeDesc);
                } catch (Exception e) {
                    LOG.log(Level.WARNING, null, e);
                }
            }
        }
    }
}
