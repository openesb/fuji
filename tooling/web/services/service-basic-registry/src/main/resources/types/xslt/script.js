/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var XSLT_SERVICE = "xslt"; // NOI18N

//----------------------------------------------------------------------------------------
var XsltServiceNode = Class.create(ServiceNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(XSLT_SERVICE, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            xslt_code:
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">\n" +
                    "    <!-- Identify transform used to copy input to output. -->\n" +
                    "    <xsl:template match=\"@*|node()\">\n" +
                    "        <xsl:copy>\n" +
                    "            <xsl:apply-templates select=\"@*|node()\"/>\n" +
                    "        </xsl:copy>\n" +
                    "    </xsl:template>\n" +
                    "    \n" +
                    "</xsl:stylesheet>\n"
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_xslt_code",
            display_name: "Code",
            description: "XSLT code which will be executed",
            value: this.properties.xslt_code,
            code_type: "xml",
            type: Ws.PropertiesSheet.CODE,

            auto_adjust_height: true,
            maximizable: true
        });
        
        return container;
    }
});

//----------------------------------------------------------------------------------------
var XsltServiceNodeUi = Class.create(ServiceNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(XSLT_SERVICE, XsltServiceNode);
Ws.Graph.NodeUiFactory.register_type(XSLT_SERVICE, XsltServiceNodeUi);
