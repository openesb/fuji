/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//-----------------------------------------------------------------------------------
var JMS_ADAPTER = "jms"; // NOI18N

//----------------------------------------------------------------------------------------
var JmsAdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    JMS_ADAPTER, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            jms_destination: "DefaultQueue",
            jms_destination_type: "Queue",
            jms_message_type: "TextMessage",
            jms_text_part: "change me",
            jms_connection_url: "mq://localhost:7676"
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_jms_destination",
            display_name: "Destination",
            description: "",
            value: this.properties.jms_destination,
            type: Ws.PropertiesSheet.STRING
        });
        container.sections[0].properties.push({
            name: "properties_jms_destination_type",
            display_name: "Destination Type",
            description: "",
            value: this.properties.jms_destination_type,
            type: Ws.PropertiesSheet.ENUM,
            values: [
                {
                    value: "Queue",
                    display_name: "Queue",
                    description: ""
                }
            ]
        });
        container.sections[0].properties.push({
            name: "properties_jms_message_type",
            display_name: "Message Type",
            description: "",
            value: this.properties.jms_message_type,
            type: Ws.PropertiesSheet.ENUM,
            values: [
                {
                    value: "TextMessage",
                    display_name: "Text Message",
                    description: ""
                }
            ]
        });
        container.sections[0].properties.push({
            name: "properties_jms_text_part",
            display_name: "Text Part",
            description: "",
            value: this.properties.jms_text_part,
            type: Ws.PropertiesSheet.STRING
        });
        container.sections[0].properties.push({
            name: "properties_jms_connection_url",
            display_name: "URL",
            description: "",
            value: this.properties.jms_connection_url,
            type: Ws.PropertiesSheet.STRING
        });
        
        return container;
    }
});

//----------------------------------------------------------------------------------------
var JmsAdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(JMS_ADAPTER, JmsAdapterNode);
Ws.Graph.NodeUiFactory.register_type(JMS_ADAPTER, JmsAdapterNodeUi);
