/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var JAVA_SERVICE = "java"; // NOI18N

//----------------------------------------------------------------------------------------
var JavaServiceNode = Class.create(ServiceNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(JAVA_SERVICE, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            java_code:
                "package pojo;\n" +
                "\n" +
                "import org.glassfish.openesb.pojose.api.ErrorMessage;\n" +
                "import org.glassfish.openesb.pojose.api.annotation.Provider;\n" +
                "import org.glassfish.openesb.pojose.api.annotation.Resource;\n" +
                "import org.glassfish.openesb.pojose.api.annotation.Operation;\n" +
                "import org.glassfish.openesb.pojose.api.res.Context;\n" +
                "\n" +
                "@Provider(name=\"${serviceName}_endpoint\", serviceQN=\"{http://fuji.dev.java.net/application/${applicationId}}${serviceName}\",\n" +
                "        interfaceQN=\"{http://fuji.dev.java.net/application/${applicationId}}${applicationId}_interface\")\n" +
                "public class Svc${serviceName}{\n" +
                "    // POJO Context\n" +
                "    @Resource\n" +
                "    private Context jbiCtx;\n" +
                "    \n" +
                "    public Svc${serviceName}(){\n" +
                "    }\n" +
                "\n" +
                "    @Operation (outMessageTypeQN=\"{http://fuji.dev.java.net/application/${applicationId}}anyMsg\")\n" +
                "    public String receive(String input) throws ErrorMessage {\n" +
                "        // Enter your code goes here.\n" +
                "        return input;\n" +
                "    }\n" +
                "}\n"
        });

        // We need to adjust the pre_add_hook, so it also calls the stuff we need.
        if (this.pre_add_hook) {
            this.__pre_add_hook = this.pre_add_hook;

            this.pre_add_hook = function() {
                this.__pre_add_hook();

                var java_code = this.properties.java_code;
                java_code = this.__adjust_java_code_graph(
                        "${applicationId}",
                        current_graph.code_name,
                 //       graph_properties_container.get_code_name(),
                        java_code);
                java_code = this.__adjust_java_code_node(
                        "${serviceName}",
                        this.properties.code_name,
                        java_code);

                this.properties.java_code = java_code;
            }
        }
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_java_code",
            display_name: "Code",
            description: "Java code which will be executed",
            value: this.properties.java_code,
            type: Ws.PropertiesSheet.CODE,
            code_type: "java",

            auto_adjust_height: true,
            maximizable: true
        });
        
        return container;
    },

    pc_set_properties: function($super, properties) {
        var old_name = this.properties.code_name;
        var new_name = properties["properties_code_name"];
        
        
        properties["properties_java_code"] = this.__adjust_java_code_node(
                old_name, new_name, properties["properties_java_code"]);

        $super(properties);
    },

    __adjust_java_code_node: function(old_name, new_name, java_code) {
        var updated_code = java_code;

        updated_code = updated_code.replace(
                "public class Svc" + old_name, "public class Svc" + new_name, "g");
        updated_code = updated_code.replace(
                "public Svc" + old_name, "public Svc" + new_name, "g");
        updated_code = updated_code.replace(
                "}" + old_name, "}" + new_name, "g");
        updated_code = updated_code.replace(
                old_name + "_endpoint", new_name + "_endpoint", "g");

        return updated_code;
    },

    __adjust_java_code_graph: function(old_name, new_name, java_code) {
        var updated_code = java_code;

        updated_code = updated_code.replace(
                old_name + "}", new_name + "}", "g");
        updated_code = updated_code.replace(
                old_name + "_interface", new_name + "_interface", "g");

        return updated_code;
    }
});

//----------------------------------------------------------------------------------------
var JavaServiceNodeUi = Class.create(ServiceNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(JAVA_SERVICE, JavaServiceNode);
Ws.Graph.NodeUiFactory.register_type(JAVA_SERVICE, JavaServiceNodeUi);
