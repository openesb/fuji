{
    id: "aws.s3-template",

    display_name: "AWS S3 Adapter",
    description: "Basic template for AWS S3 adapters.",
    icon: null,
    tags: ["template", "adapter", "amazon", "aws", "s3"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10700,

    section: "templates",

    item_data: {
        node_type: "aws.s3",
        node_data: {
            properties: {
                code_name: "aws.s3-",
                is_shared: false,

                awss3_accesskey: null,
                awss3_secretkey: null,
                awss3_operation: null,
                awss3_bucket: null,
                awss3_path: null,
                
                noop: null
            },

            ui_properties: {
                display_name: "S3 ",
                description: "Allows accessing and working with Amazon Simple Storage Service.",
                icon: null,

                noop: null
            }
        }
    }
}