{
    id: "rest-template",

    display_name: "REST Adapter",
    description: "Basic template for REST adapters.",
    icon: null,
    tags: ["template", "adapter", "rest"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10700,

    section: "templates",

    item_data: {
        node_type: "rest",
        node_data: {
            properties: {
                code_name: "rest-",
                is_shared: false,

                http_resource_url: null,
                http_method: null,
                http_accept_type: null,
                http_content_type: null,
                http_headers: null,
                http_param_style: null,
                http_params: null,
                http_basic_auth_username: null,
                http_basic_auth_password: null,
                
                noop: null
            },

            ui_properties: {
                display_name: "REST ",
                description: "Allows calling and provisioning REST web services.",
                icon: null,

                noop: null
            }
        }
    }
}