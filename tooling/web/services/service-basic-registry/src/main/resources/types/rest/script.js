/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var REST_ADAPTER = "rest"; // NOI18N

//----------------------------------------------------------------------------------------
var RESTAdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    REST_ADAPTER, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            http_resource_url: "http://www.mycompany.com/",
            http_method: "GET",
            http_accept_types: "[\n  \"application/json\"\n]",
            http_content_type: "application/json",
            http_headers: "{\n  \"authorization\" : \"257984234\",\n  \"last-modifed\" : \"2009-04-23:12:00:00\"\n}",
            http_param_style: "QUERY",
            http_params: "{\n  \"userid\" : \"abc\",\n  \"userpassword\" : \"123\"\n}",
            http_basic_auth_username: "",
            http_basic_auth_password: ""
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_http_resource_url",
            type: Ws.PropertiesSheet.STRING,

            display_name: "URL",
            description: "URL of the external resource.",
            value: this.properties.http_resource_url
        });
        
        container.sections[0].properties.push({
            name: "properties_http_method",
            type: Ws.PropertiesSheet.ENUM,

            display_name: "Method",
            description: "HTTP verb to access the resource.",
            value: this.properties.http_method,
            values: [
                {value: "GET", display_name: "GET"},
                {value: "POST", display_name: "POST"},
                {value: "PUT", display_name: "PUT"},
                {value: "DELETE", display_name: "DELETE"}
            ]
        });
                
        container.sections[0].properties.push({
            name: "properties_http_accept_types",
            type: Ws.PropertiesSheet.CODE,

            display_name: "Accept Types",
            description: "Acceptable content types of the incoming payload. This should be an array in JSON format.",
            value: this.properties.http_accept_types
        });

        container.sections[0].properties.push({
            name: "properties_http_content_type",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Content Type",
            description: "Content type of the outgoing payload.",
            value: this.properties.http_content_type
        });

        container.sections[0].properties.push({
            name: "properties_http_headers",
            type: Ws.PropertiesSheet.CODE,

            display_name: "HTTP Headers",
            description: "Additional custom HTTP headers as pairs in JSON format.",
            value: this.properties.http_headers
        });

        container.sections[0].properties.push({
            name: "properties_http_param_style",
            type: Ws.PropertiesSheet.ENUM,

            display_name: "Param Style",
            description: "Style for the parameters.",
            value: this.properties.http_param_style,
            values: [
                {value: "QUERY", display_name: "QUERY"},
                {value: "MATRIX", display_name: "MATRIX"}
            ]
        });

        container.sections[0].properties.push({
            name: "properties_http_params",
            type: Ws.PropertiesSheet.CODE,

            display_name: "HTTP Params",
            description: "Additional custom HTTP parameters as pairs in JSON format.",
            value: this.properties.http_params
        });

        container.sections[0].properties.push({
            name: "properties_http_basic_auth_username",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Basic Auth Username",
            description: "If you specify the username it will be used in the Base64-encoded Authorization header.",
            value: this.properties.http_basic_auth_username
        });

        container.sections[0].properties.push({
            name: "properties_http_basic_auth_password",
            type: Ws.PropertiesSheet.PASSWORD,

            display_name: "Basic Auth Password",
            description: "If you specify the password it will be used in the Base64-encoded Authorization header.",
            value: this.properties.http_basic_auth_password
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var RESTAdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(REST_ADAPTER, RESTAdapterNode);
Ws.Graph.NodeUiFactory.register_type(REST_ADAPTER, RESTAdapterNodeUi);
