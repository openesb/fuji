{
    id: "database-template",

    display_name: "Database Adapter",
    description: "Basic template for database adapters.",
    icon: null,
    tags: ["template", "adapter", "database"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10100,

    section: "templates",

    item_data: {
        node_type: "database",
        node_data: {
            properties: {
                code_name: "database-",
                is_shared: false,

                jdbc_operation_type: null,
                jdbc_jndi_name: null,
                jdbc_sql: null,
                jdbc_param_order: null,
                jdbc_table_name: null,

                noop: null
            },

            ui_properties: {
                display_name: "Database ",
                description: "Allows polling from and writing to databases.",
                icon: null,
                
                noop: null
            }
        }
    }
}