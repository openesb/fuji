/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var DATABASE_ADAPTER = "database"; // NOI18N

//----------------------------------------------------------------------------------------
var DatabaseAdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    DATABASE_ADAPTER, this, ui_options);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            jdbc_operation_type: "insert",
            jdbc_jndi_name: "jdbc/__defaultDS",
            jdbc_sql: "INSERT INTO TABLE (COLUMN1, COLUMN2) VALUES (?,?)",
            jdbc_param_order: "COLUMN1, COLUMN2",
            jdbc_table_name: "TABLE"
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_jdbc_operation_type",
            display_name: "Operation Type",
            description: "Type of the database operation.",
            value: this.properties.jdbc_operation_type,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_jdbc_jndi_name",
            display_name: "JNDI Name",
            description: "JNDI name of the database connection.",
            value: this.properties.jdbc_jndi_name,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_jdbc_sql",
            display_name: "SQL",
            description: "The SQL query to be executed.",
            value: this.properties.jdbc_sql,
            code_type : "sql",
            type: Ws.PropertiesSheet.CODE,
            auto_adjust_height: true,
        });
        
        container.sections[0].properties.push({
            name: "properties_jdbc_param_order",
            display_name: "Param Order",
            description: "Order of parameters as received in the incoming message.",
            value: this.properties.jdbc_param_order,
            type: Ws.PropertiesSheet.STRING
        });
        
        container.sections[0].properties.push({
            name: "properties_jdbc_table_name",
            display_name: "Table Name",
            description: "Name of the database table.",
            value: this.properties.jdbc_table_name,
            type: Ws.PropertiesSheet.STRING
        });
        
        return container;
    }
});

//----------------------------------------------------------------------------------------
var DatabaseAdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(DATABASE_ADAPTER, DatabaseAdapterNode);
Ws.Graph.NodeUiFactory.register_type(DATABASE_ADAPTER, DatabaseAdapterNodeUi);
