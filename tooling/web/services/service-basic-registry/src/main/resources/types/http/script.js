/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var HTTP_ADAPTER = "http"; // NOI18N

//----------------------------------------------------------------------------------------
var HttpAdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    HTTP_ADAPTER, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            http_location: "http://localhost:18182"
        })
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_http_location",
            display_name: "HTTP Location",
            description: "URL prefix of the HTTP/SOAP endpoint.",
            value: this.properties.http_location,
            type: Ws.PropertiesSheet.STRING
        });
        
        return container;
    }
});

//----------------------------------------------------------------------------------------
var HttpAdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(HTTP_ADAPTER, HttpAdapterNode);
Ws.Graph.NodeUiFactory.register_type(HTTP_ADAPTER, HttpAdapterNodeUi);
