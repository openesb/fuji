{
    id: "http-template",

    display_name: "HTTP/SOAP Adapter",
    description: "Basic template for HTTP/SOAP adapters.",
    icon: null,
    tags: ["template", "adapter", "http", "soap"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10400,

    section: "templates",

    item_data: {
        node_type: "http",
        node_data: {
            properties: {
                code_name: "http-",
                is_shared: false,

                http_location: null,

                noop: null
            },

            ui_properties: {
                display_name: "HTTP ",
                description: "Allows to receive and send SOAP requests via " +
                        "HTTP protocol.",
                icon: null,

                noop: null
            }
        }
    }
}
