/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var AWSS3_ADAPTER = "aws.s3"; // NOI18N

//----------------------------------------------------------------------------------------
var AWSS3AdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    AWSS3_ADAPTER, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            awss3_accesskey: "",
            awss3_secretkey: "",
            awss3_operation: "GET",
            awss3_bucket: "",
            awss3_path: "/testfile"
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_awss3_accesskey",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Access Key",
            description: "Access key used to authenticate with AWS. It should have been provided by Amazon.",
            value: this.properties.awss3_accesskey
        });
        
        container.sections[0].properties.push({
            name: "properties_awss3_secretkey",
            type: Ws.PropertiesSheet.PASSWORD,

            display_name: "Secret Key",
            description: "Secret key used to authenticate with AWS. It should have been provided by Amazon.",
            value: this.properties.awss3_secretkey
        });

        container.sections[0].properties.push({
            name: "properties_awss3_operation",
            type: Ws.PropertiesSheet.ENUM,

            display_name: "Operation",
            description: "HTTP verb describing the operation whichs hould be performed on the resource.",
            value: this.properties.awss3_operation,
            values: [
                {value: "GET", display_name: "GET"},
                {value: "PUT", display_name: "PUT"},
                {value: "DELETE", display_name: "DELETE"}
            ]
        });
        
        container.sections[0].properties.push({
            name: "properties_awss3_bucket",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Bucket",
            description: "Name of the S3 bucket you will be working with.",
            value: this.properties.awss3_bucket
        });

        container.sections[0].properties.push({
            name: "properties_awss3_path",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Path",
            description: "Path to the file within the bucket you're going to work with.",
            value: this.properties.awss3_path
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var AWSS3AdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(AWSS3_ADAPTER, AWSS3AdapterNode);
Ws.Graph.NodeUiFactory.register_type(AWSS3_ADAPTER, AWSS3AdapterNodeUi);
