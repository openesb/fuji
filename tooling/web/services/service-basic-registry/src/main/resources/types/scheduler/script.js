/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var SCHEDULER_ADAPTER = "scheduler";

//----------------------------------------------------------------------------------------
var SchedulerAdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);
        
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    SCHEDULER_ADAPTER, this);
        }
        
        Ws.Utils.apply_defaults(this.properties, {
            scheduler_type: "simple",
            scheduler_message: "Trigger message",
            scheduler_starting: "now",
            scheduler_ending: "never",
            scheduler_timezone: "America/Los_Angeles",

            scheduler_interval: 60000,
            scheduler_repeat: "indefinite",

            scheduler_cron_expr: "0 * * * * ?",

            scheduler_duration: 600000
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        
        container.sections[0].properties.push({
            name: "properties_scheduler_type",
            display_name: "Type",
            description: "Valid trigger types are: Simple, Cron, Hybrid.",
            value: this.properties.scheduler_type,
            type: Ws.PropertiesSheet.ENUM,
            values: [
                {
                    value: "simple",
                    display_name: "Simple",
                    description: "A time interval-based trigger"
                },
                {
                    value: "cron",
                    display_name: "Cron",
                    description: "A calendar-based trigger"
                },
                {
                    value: "hybrid",
                    display_name: "Hybrid",
                    description: "Essentially a Cron trigger starting a Simple trigger"
                }
            ]
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_message",
            display_name: "Message",
            description: "Message that will be sent to the endpoint being triggered.",
            value: this.properties.scheduler_message,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_starting",
            display_name: "Starting",
            description: "Date and time when the triggers defined here should start once the respective service assembly has been deployed and started.  Enter either keyword 'now' or date in SimpleDateFormat: yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            value: this.properties.scheduler_starting,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_ending",
            display_name: "Ending",
            description: "Date and time when the triggers defined here should end once the respective service assembly is running.  Enter either keyword 'never' or date in SimpleDateFormat: yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            value: this.properties.scheduler_ending,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_timezone",
            display_name: "Time Zone",
            description: "Time zone ID for the environment where the trigger will be run.",
            value: this.properties.scheduler_timezone,
            type: Ws.PropertiesSheet.ENUM,

            style: {
                width: "180px"
            },

            values: [
                {
                    value: "ACT",
                    display_name: "ACT",
                    description: "ACT time zone"
                },
                {
                    value: "AET",
                    display_name: "AET",
                    description: "AET time zone"
                },
                {
                    value: "AGT",
                    display_name: "AGT",
                    description: "AGT time zone"
                },
                {
                    value: "ART",
                    display_name: "ART",
                    description: "ART time zone"
                },
                {
                    value: "AST",
                    display_name: "AST",
                    description: "AST time zone"
                },
                {
                    value: "Africa/Abidjan",
                    display_name: "Africa/Abidjan",
                    description: "Africa/Abidjan time zone"
                },
                {
                    value: "Africa/Accra",
                    display_name: "Africa/Accra",
                    description: "Africa/Accra time zone"
                },
                {
                    value: "Africa/Addis_Ababa",
                    display_name: "Africa/Addis Ababa",
                    description: "Africa/Addis_Ababa time zone"
                },
                {
                    value: "Africa/Algiers",
                    display_name: "Africa/Algiers",
                    description: "Africa/Algiers time zone"
                },
                {
                    value: "Africa/Asmara",
                    display_name: "Africa/Asmara",
                    description: "Africa/Asmara time zone"
                },
                {
                    value: "Africa/Asmera",
                    display_name: "Africa/Asmera",
                    description: "Africa/Asmera time zone"
                },
                {
                    value: "Africa/Bamako",
                    display_name: "Africa/Bamako",
                    description: "Africa/Bamako time zone"
                },
                {
                    value: "Africa/Bangui",
                    display_name: "Africa/Bangui",
                    description: "Africa/Bangui time zone"
                },
                {
                    value: "Africa/Banjul",
                    display_name: "Africa/Banjul",
                    description: "Africa/Banjul time zone"
                },
                {
                    value: "Africa/Bissau",
                    display_name: "Africa/Bissau",
                    description: "Africa/Bissau time zone"
                },
                {
                    value: "Africa/Blantyre",
                    display_name: "Africa/Blantyre",
                    description: "Africa/Blantyre time zone"
                },
                {
                    value: "Africa/Brazzaville",
                    display_name: "Africa/Brazzaville",
                    description: "Africa/Brazzaville time zone"
                },
                {
                    value: "Africa/Bujumbura",
                    display_name: "Africa/Bujumbura",
                    description: "Africa/Bujumbura time zone"
                },
                {
                    value: "Africa/Cairo",
                    display_name: "Africa/Cairo",
                    description: "Africa/Cairo time zone"
                },
                {
                    value: "Africa/Casablanca",
                    display_name: "Africa/Casablanca",
                    description: "Africa/Casablanca time zone"
                },
                {
                    value: "Africa/Ceuta",
                    display_name: "Africa/Ceuta",
                    description: "Africa/Ceuta time zone"
                },
                {
                    value: "Africa/Conakry",
                    display_name: "Africa/Conakry",
                    description: "Africa/Conakry time zone"
                },
                {
                    value: "Africa/Dakar",
                    display_name: "Africa/Dakar",
                    description: "Africa/Dakar time zone"
                },
                {
                    value: "Africa/Dar_es_Salaam",
                    display_name: "Africa/Dar es Salaam",
                    description: "Africa/Dar_es_Salaam time zone"
                },
                {
                    value: "Africa/Djibouti",
                    display_name: "Africa/Djibouti",
                    description: "Africa/Djibouti time zone"
                },
                {
                    value: "Africa/Douala",
                    display_name: "Africa/Douala",
                    description: "Africa/Douala time zone"
                },
                {
                    value: "Africa/El_Aaiun",
                    display_name: "Africa/El Aaiun",
                    description: "Africa/El_Aaiun time zone"
                },
                {
                    value: "Africa/Freetown",
                    display_name: "Africa/Freetown",
                    description: "Africa/Freetown time zone"
                },
                {
                    value: "Africa/Gaborone",
                    display_name: "Africa/Gaborone",
                    description: "Africa/Gaborone time zone"
                },
                {
                    value: "Africa/Harare",
                    display_name: "Africa/Harare",
                    description: "Africa/Harare time zone"
                },
                {
                    value: "Africa/Johannesburg",
                    display_name: "Africa/Johannesburg",
                    description: "Africa/Johannesburg time zone"
                },
                {
                    value: "Africa/Kampala",
                    display_name: "Africa/Kampala",
                    description: "Africa/Kampala time zone"
                },
                {
                    value: "Africa/Khartoum",
                    display_name: "Africa/Khartoum",
                    description: "Africa/Khartoum time zone"
                },
                {
                    value: "Africa/Kigali",
                    display_name: "Africa/Kigali",
                    description: "Africa/Kigali time zone"
                },
                {
                    value: "Africa/Kinshasa",
                    display_name: "Africa/Kinshasa",
                    description: "Africa/Kinshasa time zone"
                },
                {
                    value: "Africa/Lagos",
                    display_name: "Africa/Lagos",
                    description: "Africa/Lagos time zone"
                },
                {
                    value: "Africa/Libreville",
                    display_name: "Africa/Libreville",
                    description: "Africa/Libreville time zone"
                },
                {
                    value: "Africa/Lome",
                    display_name: "Africa/Lome",
                    description: "Africa/Lome time zone"
                },
                {
                    value: "Africa/Luanda",
                    display_name: "Africa/Luanda",
                    description: "Africa/Luanda time zone"
                },
                {
                    value: "Africa/Lubumbashi",
                    display_name: "Africa/Lubumbashi",
                    description: "Africa/Lubumbashi time zone"
                },
                {
                    value: "Africa/Lusaka",
                    display_name: "Africa/Lusaka",
                    description: "Africa/Lusaka time zone"
                },
                {
                    value: "Africa/Malabo",
                    display_name: "Africa/Malabo",
                    description: "Africa/Malabo time zone"
                },
                {
                    value: "Africa/Maputo",
                    display_name: "Africa/Maputo",
                    description: "Africa/Maputo time zone"
                },
                {
                    value: "Africa/Maseru",
                    display_name: "Africa/Maseru",
                    description: "Africa/Maseru time zone"
                },
                {
                    value: "Africa/Mbabane",
                    display_name: "Africa/Mbabane",
                    description: "Africa/Mbabane time zone"
                },
                {
                    value: "Africa/Mogadishu",
                    display_name: "Africa/Mogadishu",
                    description: "Africa/Mogadishu time zone"
                },
                {
                    value: "Africa/Monrovia",
                    display_name: "Africa/Monrovia",
                    description: "Africa/Monrovia time zone"
                },
                {
                    value: "Africa/Nairobi",
                    display_name: "Africa/Nairobi",
                    description: "Africa/Nairobi time zone"
                },
                {
                    value: "Africa/Ndjamena",
                    display_name: "Africa/Ndjamena",
                    description: "Africa/Ndjamena time zone"
                },
                {
                    value: "Africa/Niamey",
                    display_name: "Africa/Niamey",
                    description: "Africa/Niamey time zone"
                },
                {
                    value: "Africa/Nouakchott",
                    display_name: "Africa/Nouakchott",
                    description: "Africa/Nouakchott time zone"
                },
                {
                    value: "Africa/Ouagadougou",
                    display_name: "Africa/Ouagadougou",
                    description: "Africa/Ouagadougou time zone"
                },
                {
                    value: "Africa/Porto-Novo",
                    display_name: "Africa/Porto-Novo",
                    description: "Africa/Porto-Novo time zone"
                },
                {
                    value: "Africa/Sao_Tome",
                    display_name: "Africa/Sao Tome",
                    description: "Africa/Sao_Tome time zone"
                },
                {
                    value: "Africa/Timbuktu",
                    display_name: "Africa/Timbuktu",
                    description: "Africa/Timbuktu time zone"
                },
                {
                    value: "Africa/Tripoli",
                    display_name: "Africa/Tripoli",
                    description: "Africa/Tripoli time zone"
                },
                {
                    value: "Africa/Tunis",
                    display_name: "Africa/Tunis",
                    description: "Africa/Tunis time zone"
                },
                {
                    value: "Africa/Windhoek",
                    display_name: "Africa/Windhoek",
                    description: "Africa/Windhoek time zone"
                },
                {
                    value: "America/Adak",
                    display_name: "America/Adak",
                    description: "America/Adak time zone"
                },
                {
                    value: "America/Anchorage",
                    display_name: "America/Anchorage",
                    description: "America/Anchorage time zone"
                },
                {
                    value: "America/Anguilla",
                    display_name: "America/Anguilla",
                    description: "America/Anguilla time zone"
                },
                {
                    value: "America/Antigua",
                    display_name: "America/Antigua",
                    description: "America/Antigua time zone"
                },
                {
                    value: "America/Araguaina",
                    display_name: "America/Araguaina",
                    description: "America/Araguaina time zone"
                },
                {
                    value: "America/Argentina/Buenos_Aires",
                    display_name: "America/Argentina/Buenos Aires",
                    description: "America/Argentina/Buenos_Aires time zone"
                },
                {
                    value: "America/Argentina/Catamarca",
                    display_name: "America/Argentina/Catamarca",
                    description: "America/Argentina/Catamarca time zone"
                },
                {
                    value: "America/Argentina/ComodRivadavia",
                    display_name: "America/Argentina/ComodRivadavia",
                    description: "America/Argentina/ComodRivadavia time zone"
                },
                {
                    value: "America/Argentina/Cordoba",
                    display_name: "America/Argentina/Cordoba",
                    description: "America/Argentina/Cordoba time zone"
                },
                {
                    value: "America/Argentina/Jujuy",
                    display_name: "America/Argentina/Jujuy",
                    description: "America/Argentina/Jujuy time zone"
                },
                {
                    value: "America/Argentina/La_Rioja",
                    display_name: "America/Argentina/La Rioja",
                    description: "America/Argentina/La_Rioja time zone"
                },
                {
                    value: "America/Argentina/Mendoza",
                    display_name: "America/Argentina/Mendoza",
                    description: "America/Argentina/Mendoza time zone"
                },
                {
                    value: "America/Argentina/Rio_Gallegos",
                    display_name: "America/Argentina/Rio Gallegos",
                    description: "America/Argentina/Rio_Gallegos time zone"
                },
                {
                    value: "America/Argentina/San_Juan",
                    display_name: "America/Argentina/San Juan",
                    description: "America/Argentina/San_Juan time zone"
                },
                {
                    value: "America/Argentina/Tucuman",
                    display_name: "America/Argentina/Tucuman",
                    description: "America/Argentina/Tucuman time zone"
                },
                {
                    value: "America/Argentina/Ushuaia",
                    display_name: "America/Argentina/Ushuaia",
                    description: "America/Argentina/Ushuaia time zone"
                },
                {
                    value: "America/Aruba",
                    display_name: "America/Aruba",
                    description: "America/Aruba time zone"
                },
                {
                    value: "America/Asuncion",
                    display_name: "America/Asuncion",
                    description: "America/Asuncion time zone"
                },
                {
                    value: "America/Atikokan",
                    display_name: "America/Atikokan",
                    description: "America/Atikokan time zone"
                },
                {
                    value: "America/Atka",
                    display_name: "America/Atka",
                    description: "America/Atka time zone"
                },
                {
                    value: "America/Bahia",
                    display_name: "America/Bahia",
                    description: "America/Bahia time zone"
                },
                {
                    value: "America/Barbados",
                    display_name: "America/Barbados",
                    description: "America/Barbados time zone"
                },
                {
                    value: "America/Belem",
                    display_name: "America/Belem",
                    description: "America/Belem time zone"
                },
                {
                    value: "America/Belize",
                    display_name: "America/Belize",
                    description: "America/Belize time zone"
                },
                {
                    value: "America/Blanc-Sablon",
                    display_name: "America/Blanc-Sablon",
                    description: "America/Blanc-Sablon time zone"
                },
                {
                    value: "America/Boa_Vista",
                    display_name: "America/Boa Vista",
                    description: "America/Boa_Vista time zone"
                },
                {
                    value: "America/Bogota",
                    display_name: "America/Bogota",
                    description: "America/Bogota time zone"
                },
                {
                    value: "America/Boise",
                    display_name: "America/Boise",
                    description: "America/Boise time zone"
                },
                {
                    value: "America/Buenos_Aires",
                    display_name: "America/Buenos Aires",
                    description: "America/Buenos_Aires time zone"
                },
                {
                    value: "America/Cambridge_Bay",
                    display_name: "America/Cambridge Bay",
                    description: "America/Cambridge_Bay time zone"
                },
                {
                    value: "America/Campo_Grande",
                    display_name: "America/Campo Grande",
                    description: "America/Campo_Grande time zone"
                },
                {
                    value: "America/Cancun",
                    display_name: "America/Cancun",
                    description: "America/Cancun time zone"
                },
                {
                    value: "America/Caracas",
                    display_name: "America/Caracas",
                    description: "America/Caracas time zone"
                },
                {
                    value: "America/Catamarca",
                    display_name: "America/Catamarca",
                    description: "America/Catamarca time zone"
                },
                {
                    value: "America/Cayenne",
                    display_name: "America/Cayenne",
                    description: "America/Cayenne time zone"
                },
                {
                    value: "America/Cayman",
                    display_name: "America/Cayman",
                    description: "America/Cayman time zone"
                },
                {
                    value: "America/Chicago",
                    display_name: "America/Chicago",
                    description: "America/Chicago time zone"
                },
                {
                    value: "America/Chihuahua",
                    display_name: "America/Chihuahua",
                    description: "America/Chihuahua time zone"
                },
                {
                    value: "America/Coral_Harbour",
                    display_name: "America/Coral Harbour",
                    description: "America/Coral_Harbour time zone"
                },
                {
                    value: "America/Cordoba",
                    display_name: "America/Cordoba",
                    description: "America/Cordoba time zone"
                },
                {
                    value: "America/Costa_Rica",
                    display_name: "America/Costa Rica",
                    description: "America/Costa_Rica time zone"
                },
                {
                    value: "America/Cuiaba",
                    display_name: "America/Cuiaba",
                    description: "America/Cuiaba time zone"
                },
                {
                    value: "America/Curacao",
                    display_name: "America/Curacao",
                    description: "America/Curacao time zone"
                },
                {
                    value: "America/Danmarkshavn",
                    display_name: "America/Danmarkshavn",
                    description: "America/Danmarkshavn time zone"
                },
                {
                    value: "America/Dawson",
                    display_name: "America/Dawson",
                    description: "America/Dawson time zone"
                },
                {
                    value: "America/Dawson_Creek",
                    display_name: "America/Dawson Creek",
                    description: "America/Dawson_Creek time zone"
                },
                {
                    value: "America/Denver",
                    display_name: "America/Denver",
                    description: "America/Denver time zone"
                },
                {
                    value: "America/Detroit",
                    display_name: "America/Detroit",
                    description: "America/Detroit time zone"
                },
                {
                    value: "America/Dominica",
                    display_name: "America/Dominica",
                    description: "America/Dominica time zone"
                },
                {
                    value: "America/Edmonton",
                    display_name: "America/Edmonton",
                    description: "America/Edmonton time zone"
                },
                {
                    value: "America/Eirunepe",
                    display_name: "America/Eirunepe",
                    description: "America/Eirunepe time zone"
                },
                {
                    value: "America/El_Salvador",
                    display_name: "America/El Salvador",
                    description: "America/El_Salvador time zone"
                },
                {
                    value: "America/Ensenada",
                    display_name: "America/Ensenada",
                    description: "America/Ensenada time zone"
                },
                {
                    value: "America/Fort_Wayne",
                    display_name: "America/Fort Wayne",
                    description: "America/Fort_Wayne time zone"
                },
                {
                    value: "America/Fortaleza",
                    display_name: "America/Fortaleza",
                    description: "America/Fortaleza time zone"
                },
                {
                    value: "America/Glace_Bay",
                    display_name: "America/Glace Bay",
                    description: "America/Glace_Bay time zone"
                },
                {
                    value: "America/Godthab",
                    display_name: "America/Godthab",
                    description: "America/Godthab time zone"
                },
                {
                    value: "America/Goose_Bay",
                    display_name: "America/Goose Bay",
                    description: "America/Goose_Bay time zone"
                },
                {
                    value: "America/Grand_Turk",
                    display_name: "America/Grand Turk",
                    description: "America/Grand_Turk time zone"
                },
                {
                    value: "America/Grenada",
                    display_name: "America/Grenada",
                    description: "America/Grenada time zone"
                },
                {
                    value: "America/Guadeloupe",
                    display_name: "America/Guadeloupe",
                    description: "America/Guadeloupe time zone"
                },
                {
                    value: "America/Guatemala",
                    display_name: "America/Guatemala",
                    description: "America/Guatemala time zone"
                },
                {
                    value: "America/Guayaquil",
                    display_name: "America/Guayaquil",
                    description: "America/Guayaquil time zone"
                },
                {
                    value: "America/Guyana",
                    display_name: "America/Guyana",
                    description: "America/Guyana time zone"
                },
                {
                    value: "America/Halifax",
                    display_name: "America/Halifax",
                    description: "America/Halifax time zone"
                },
                {
                    value: "America/Havana",
                    display_name: "America/Havana",
                    description: "America/Havana time zone"
                },
                {
                    value: "America/Hermosillo",
                    display_name: "America/Hermosillo",
                    description: "America/Hermosillo time zone"
                },
                {
                    value: "America/Indiana/Indianapolis",
                    display_name: "America/Indiana/Indianapolis",
                    description: "America/Indiana/Indianapolis time zone"
                },
                {
                    value: "America/Indiana/Knox",
                    display_name: "America/Indiana/Knox",
                    description: "America/Indiana/Knox time zone"
                },
                {
                    value: "America/Indiana/Marengo",
                    display_name: "America/Indiana/Marengo",
                    description: "America/Indiana/Marengo time zone"
                },
                {
                    value: "America/Indiana/Petersburg",
                    display_name: "America/Indiana/Petersburg",
                    description: "America/Indiana/Petersburg time zone"
                },
                {
                    value: "America/Indiana/Tell_City",
                    display_name: "America/Indiana/Tell City",
                    description: "America/Indiana/Tell_City time zone"
                },
                {
                    value: "America/Indiana/Vevay",
                    display_name: "America/Indiana/Vevay",
                    description: "America/Indiana/Vevay time zone"
                },
                {
                    value: "America/Indiana/Vincennes",
                    display_name: "America/Indiana/Vincennes",
                    description: "America/Indiana/Vincennes time zone"
                },
                {
                    value: "America/Indiana/Winamac",
                    display_name: "America/Indiana/Winamac",
                    description: "America/Indiana/Winamac time zone"
                },
                {
                    value: "America/Indianapolis",
                    display_name: "America/Indianapolis",
                    description: "America/Indianapolis time zone"
                },
                {
                    value: "America/Inuvik",
                    display_name: "America/Inuvik",
                    description: "America/Inuvik time zone"
                },
                {
                    value: "America/Iqaluit",
                    display_name: "America/Iqaluit",
                    description: "America/Iqaluit time zone"
                },
                {
                    value: "America/Jamaica",
                    display_name: "America/Jamaica",
                    description: "America/Jamaica time zone"
                },
                {
                    value: "America/Jujuy",
                    display_name: "America/Jujuy",
                    description: "America/Jujuy time zone"
                },
                {
                    value: "America/Juneau",
                    display_name: "America/Juneau",
                    description: "America/Juneau time zone"
                },
                {
                    value: "America/Kentucky/Louisville",
                    display_name: "America/Kentucky/Louisville",
                    description: "America/Kentucky/Louisville time zone"
                },
                {
                    value: "America/Kentucky/Monticello",
                    display_name: "America/Kentucky/Monticello",
                    description: "America/Kentucky/Monticello time zone"
                },
                {
                    value: "America/Knox_IN",
                    display_name: "America/Knox IN",
                    description: "America/Knox_IN time zone"
                },
                {
                    value: "America/La_Paz",
                    display_name: "America/La Paz",
                    description: "America/La_Paz time zone"
                },
                {
                    value: "America/Lima",
                    display_name: "America/Lima",
                    description: "America/Lima time zone"
                },
                {
                    value: "America/Los_Angeles",
                    display_name: "America/Los Angeles",
                    description: "America/Los_Angeles time zone"
                },
                {
                    value: "America/Louisville",
                    display_name: "America/Louisville",
                    description: "America/Louisville time zone"
                },
                {
                    value: "America/Maceio",
                    display_name: "America/Maceio",
                    description: "America/Maceio time zone"
                },
                {
                    value: "America/Managua",
                    display_name: "America/Managua",
                    description: "America/Managua time zone"
                },
                {
                    value: "America/Manaus",
                    display_name: "America/Manaus",
                    description: "America/Manaus time zone"
                },
                {
                    value: "America/Martinique",
                    display_name: "America/Martinique",
                    description: "America/Martinique time zone"
                },
                {
                    value: "America/Mazatlan",
                    display_name: "America/Mazatlan",
                    description: "America/Mazatlan time zone"
                },
                {
                    value: "America/Mendoza",
                    display_name: "America/Mendoza",
                    description: "America/Mendoza time zone"
                },
                {
                    value: "America/Menominee",
                    display_name: "America/Menominee",
                    description: "America/Menominee time zone"
                },
                {
                    value: "America/Merida",
                    display_name: "America/Merida",
                    description: "America/Merida time zone"
                },
                {
                    value: "America/Mexico_City",
                    display_name: "America/Mexico City",
                    description: "America/Mexico_City time zone"
                },
                {
                    value: "America/Miquelon",
                    display_name: "America/Miquelon",
                    description: "America/Miquelon time zone"
                },
                {
                    value: "America/Moncton",
                    display_name: "America/Moncton",
                    description: "America/Moncton time zone"
                },
                {
                    value: "America/Monterrey",
                    display_name: "America/Monterrey",
                    description: "America/Monterrey time zone"
                },
                {
                    value: "America/Montevideo",
                    display_name: "America/Montevideo",
                    description: "America/Montevideo time zone"
                },
                {
                    value: "America/Montreal",
                    display_name: "America/Montreal",
                    description: "America/Montreal time zone"
                },
                {
                    value: "America/Montserrat",
                    display_name: "America/Montserrat",
                    description: "America/Montserrat time zone"
                },
                {
                    value: "America/Nassau",
                    display_name: "America/Nassau",
                    description: "America/Nassau time zone"
                },
                {
                    value: "America/New_York",
                    display_name: "America/New York",
                    description: "America/New_York time zone"
                },
                {
                    value: "America/Nipigon",
                    display_name: "America/Nipigon",
                    description: "America/Nipigon time zone"
                },
                {
                    value: "America/Nome",
                    display_name: "America/Nome",
                    description: "America/Nome time zone"
                },
                {
                    value: "America/Noronha",
                    display_name: "America/Noronha",
                    description: "America/Noronha time zone"
                },
                {
                    value: "America/North_Dakota/Center",
                    display_name: "America/North Dakota/Center",
                    description: "America/North_Dakota/Center time zone"
                },
                {
                    value: "America/North_Dakota/New_Salem",
                    display_name: "America/North Dakota/New Salem",
                    description: "America/North_Dakota/New_Salem time zone"
                },
                {
                    value: "America/Panama",
                    display_name: "America/Panama",
                    description: "America/Panama time zone"
                },
                {
                    value: "America/Pangnirtung",
                    display_name: "America/Pangnirtung",
                    description: "America/Pangnirtung time zone"
                },
                {
                    value: "America/Paramaribo",
                    display_name: "America/Paramaribo",
                    description: "America/Paramaribo time zone"
                },
                {
                    value: "America/Phoenix",
                    display_name: "America/Phoenix",
                    description: "America/Phoenix time zone"
                },
                {
                    value: "America/Port-au-Prince",
                    display_name: "America/Port-au-Prince",
                    description: "America/Port-au-Prince time zone"
                },
                {
                    value: "America/Port_of_Spain",
                    display_name: "America/Port of Spain",
                    description: "America/Port_of_Spain time zone"
                },
                {
                    value: "America/Porto_Acre",
                    display_name: "America/Porto Acre",
                    description: "America/Porto_Acre time zone"
                },
                {
                    value: "America/Porto_Velho",
                    display_name: "America/Porto Velho",
                    description: "America/Porto_Velho time zone"
                },
                {
                    value: "America/Puerto_Rico",
                    display_name: "America/Puerto Rico",
                    description: "America/Puerto_Rico time zone"
                },
                {
                    value: "America/Rainy_River",
                    display_name: "America/Rainy River",
                    description: "America/Rainy_River time zone"
                },
                {
                    value: "America/Rankin_Inlet",
                    display_name: "America/Rankin Inlet",
                    description: "America/Rankin_Inlet time zone"
                },
                {
                    value: "America/Recife",
                    display_name: "America/Recife",
                    description: "America/Recife time zone"
                },
                {
                    value: "America/Regina",
                    display_name: "America/Regina",
                    description: "America/Regina time zone"
                },
                {
                    value: "America/Resolute",
                    display_name: "America/Resolute",
                    description: "America/Resolute time zone"
                },
                {
                    value: "America/Rio_Branco",
                    display_name: "America/Rio Branco",
                    description: "America/Rio_Branco time zone"
                },
                {
                    value: "America/Rosario",
                    display_name: "America/Rosario",
                    description: "America/Rosario time zone"
                },
                {
                    value: "America/Santiago",
                    display_name: "America/Santiago",
                    description: "America/Santiago time zone"
                },
                {
                    value: "America/Santo_Domingo",
                    display_name: "America/Santo Domingo",
                    description: "America/Santo_Domingo time zone"
                },
                {
                    value: "America/Sao_Paulo",
                    display_name: "America/Sao Paulo",
                    description: "America/Sao_Paulo time zone"
                },
                {
                    value: "America/Scoresbysund",
                    display_name: "America/Scoresbysund",
                    description: "America/Scoresbysund time zone"
                },
                {
                    value: "America/Shiprock",
                    display_name: "America/Shiprock",
                    description: "America/Shiprock time zone"
                },
                {
                    value: "America/St_Johns",
                    display_name: "America/St Johns",
                    description: "America/St_Johns time zone"
                },
                {
                    value: "America/St_Kitts",
                    display_name: "America/St Kitts",
                    description: "America/St_Kitts time zone"
                },
                {
                    value: "America/St_Lucia",
                    display_name: "America/St Lucia",
                    description: "America/St_Lucia time zone"
                },
                {
                    value: "America/St_Thomas",
                    display_name: "America/St Thomas",
                    description: "America/St_Thomas time zone"
                },
                {
                    value: "America/St_Vincent",
                    display_name: "America/St Vincent",
                    description: "America/St_Vincent time zone"
                },
                {
                    value: "America/Swift_Current",
                    display_name: "America/Swift Current",
                    description: "America/Swift_Current time zone"
                },
                {
                    value: "America/Tegucigalpa",
                    display_name: "America/Tegucigalpa",
                    description: "America/Tegucigalpa time zone"
                },
                {
                    value: "America/Thule",
                    display_name: "America/Thule",
                    description: "America/Thule time zone"
                },
                {
                    value: "America/Thunder_Bay",
                    display_name: "America/Thunder Bay",
                    description: "America/Thunder_Bay time zone"
                },
                {
                    value: "America/Tijuana",
                    display_name: "America/Tijuana",
                    description: "America/Tijuana time zone"
                },
                {
                    value: "America/Toronto",
                    display_name: "America/Toronto",
                    description: "America/Toronto time zone"
                },
                {
                    value: "America/Tortola",
                    display_name: "America/Tortola",
                    description: "America/Tortola time zone"
                },
                {
                    value: "America/Vancouver",
                    display_name: "America/Vancouver",
                    description: "America/Vancouver time zone"
                },
                {
                    value: "America/Virgin",
                    display_name: "America/Virgin",
                    description: "America/Virgin time zone"
                },
                {
                    value: "America/Whitehorse",
                    display_name: "America/Whitehorse",
                    description: "America/Whitehorse time zone"
                },
                {
                    value: "America/Winnipeg",
                    display_name: "America/Winnipeg",
                    description: "America/Winnipeg time zone"
                },
                {
                    value: "America/Yakutat",
                    display_name: "America/Yakutat",
                    description: "America/Yakutat time zone"
                },
                {
                    value: "America/Yellowknife",
                    display_name: "America/Yellowknife",
                    description: "America/Yellowknife time zone"
                },
                {
                    value: "Antarctica/Casey",
                    display_name: "Antarctica/Casey",
                    description: "Antarctica/Casey time zone"
                },
                {
                    value: "Antarctica/Davis",
                    display_name: "Antarctica/Davis",
                    description: "Antarctica/Davis time zone"
                },
                {
                    value: "Antarctica/DumontDUrville",
                    display_name: "Antarctica/DumontDUrville",
                    description: "Antarctica/DumontDUrville time zone"
                },
                {
                    value: "Antarctica/Mawson",
                    display_name: "Antarctica/Mawson",
                    description: "Antarctica/Mawson time zone"
                },
                {
                    value: "Antarctica/McMurdo",
                    display_name: "Antarctica/McMurdo",
                    description: "Antarctica/McMurdo time zone"
                },
                {
                    value: "Antarctica/Palmer",
                    display_name: "Antarctica/Palmer",
                    description: "Antarctica/Palmer time zone"
                },
                {
                    value: "Antarctica/Rothera",
                    display_name: "Antarctica/Rothera",
                    description: "Antarctica/Rothera time zone"
                },
                {
                    value: "Antarctica/South_Pole",
                    display_name: "Antarctica/South Pole",
                    description: "Antarctica/South_Pole time zone"
                },
                {
                    value: "Antarctica/Syowa",
                    display_name: "Antarctica/Syowa",
                    description: "Antarctica/Syowa time zone"
                },
                {
                    value: "Antarctica/Vostok",
                    display_name: "Antarctica/Vostok",
                    description: "Antarctica/Vostok time zone"
                },
                {
                    value: "Arctic/Longyearbyen",
                    display_name: "Arctic/Longyearbyen",
                    description: "Arctic/Longyearbyen time zone"
                },
                {
                    value: "Asia/Aden",
                    display_name: "Asia/Aden",
                    description: "Asia/Aden time zone"
                },
                {
                    value: "Asia/Almaty",
                    display_name: "Asia/Almaty",
                    description: "Asia/Almaty time zone"
                },
                {
                    value: "Asia/Amman",
                    display_name: "Asia/Amman",
                    description: "Asia/Amman time zone"
                },
                {
                    value: "Asia/Anadyr",
                    display_name: "Asia/Anadyr",
                    description: "Asia/Anadyr time zone"
                },
                {
                    value: "Asia/Aqtau",
                    display_name: "Asia/Aqtau",
                    description: "Asia/Aqtau time zone"
                },
                {
                    value: "Asia/Aqtobe",
                    display_name: "Asia/Aqtobe",
                    description: "Asia/Aqtobe time zone"
                },
                {
                    value: "Asia/Ashgabat",
                    display_name: "Asia/Ashgabat",
                    description: "Asia/Ashgabat time zone"
                },
                {
                    value: "Asia/Ashkhabad",
                    display_name: "Asia/Ashkhabad",
                    description: "Asia/Ashkhabad time zone"
                },
                {
                    value: "Asia/Baghdad",
                    display_name: "Asia/Baghdad",
                    description: "Asia/Baghdad time zone"
                },
                {
                    value: "Asia/Bahrain",
                    display_name: "Asia/Bahrain",
                    description: "Asia/Bahrain time zone"
                },
                {
                    value: "Asia/Baku",
                    display_name: "Asia/Baku",
                    description: "Asia/Baku time zone"
                },
                {
                    value: "Asia/Bangkok",
                    display_name: "Asia/Bangkok",
                    description: "Asia/Bangkok time zone"
                },
                {
                    value: "Asia/Beirut",
                    display_name: "Asia/Beirut",
                    description: "Asia/Beirut time zone"
                },
                {
                    value: "Asia/Bishkek",
                    display_name: "Asia/Bishkek",
                    description: "Asia/Bishkek time zone"
                },
                {
                    value: "Asia/Brunei",
                    display_name: "Asia/Brunei",
                    description: "Asia/Brunei time zone"
                },
                {
                    value: "Asia/Calcutta",
                    display_name: "Asia/Calcutta",
                    description: "Asia/Calcutta time zone"
                },
                {
                    value: "Asia/Choibalsan",
                    display_name: "Asia/Choibalsan",
                    description: "Asia/Choibalsan time zone"
                },
                {
                    value: "Asia/Chongqing",
                    display_name: "Asia/Chongqing",
                    description: "Asia/Chongqing time zone"
                },
                {
                    value: "Asia/Chungking",
                    display_name: "Asia/Chungking",
                    description: "Asia/Chungking time zone"
                },
                {
                    value: "Asia/Colombo",
                    display_name: "Asia/Colombo",
                    description: "Asia/Colombo time zone"
                },
                {
                    value: "Asia/Dacca",
                    display_name: "Asia/Dacca",
                    description: "Asia/Dacca time zone"
                },
                {
                    value: "Asia/Damascus",
                    display_name: "Asia/Damascus",
                    description: "Asia/Damascus time zone"
                },
                {
                    value: "Asia/Dhaka",
                    display_name: "Asia/Dhaka",
                    description: "Asia/Dhaka time zone"
                },
                {
                    value: "Asia/Dili",
                    display_name: "Asia/Dili",
                    description: "Asia/Dili time zone"
                },
                {
                    value: "Asia/Dubai",
                    display_name: "Asia/Dubai",
                    description: "Asia/Dubai time zone"
                },
                {
                    value: "Asia/Dushanbe",
                    display_name: "Asia/Dushanbe",
                    description: "Asia/Dushanbe time zone"
                },
                {
                    value: "Asia/Gaza",
                    display_name: "Asia/Gaza",
                    description: "Asia/Gaza time zone"
                },
                {
                    value: "Asia/Harbin",
                    display_name: "Asia/Harbin",
                    description: "Asia/Harbin time zone"
                },
                {
                    value: "Asia/Hong_Kong",
                    display_name: "Asia/Hong Kong",
                    description: "Asia/Hong_Kong time zone"
                },
                {
                    value: "Asia/Hovd",
                    display_name: "Asia/Hovd",
                    description: "Asia/Hovd time zone"
                },
                {
                    value: "Asia/Irkutsk",
                    display_name: "Asia/Irkutsk",
                    description: "Asia/Irkutsk time zone"
                },
                {
                    value: "Asia/Istanbul",
                    display_name: "Asia/Istanbul",
                    description: "Asia/Istanbul time zone"
                },
                {
                    value: "Asia/Jakarta",
                    display_name: "Asia/Jakarta",
                    description: "Asia/Jakarta time zone"
                },
                {
                    value: "Asia/Jayapura",
                    display_name: "Asia/Jayapura",
                    description: "Asia/Jayapura time zone"
                },
                {
                    value: "Asia/Jerusalem",
                    display_name: "Asia/Jerusalem",
                    description: "Asia/Jerusalem time zone"
                },
                {
                    value: "Asia/Kabul",
                    display_name: "Asia/Kabul",
                    description: "Asia/Kabul time zone"
                },
                {
                    value: "Asia/Kamchatka",
                    display_name: "Asia/Kamchatka",
                    description: "Asia/Kamchatka time zone"
                },
                {
                    value: "Asia/Karachi",
                    display_name: "Asia/Karachi",
                    description: "Asia/Karachi time zone"
                },
                {
                    value: "Asia/Kashgar",
                    display_name: "Asia/Kashgar",
                    description: "Asia/Kashgar time zone"
                },
                {
                    value: "Asia/Katmandu",
                    display_name: "Asia/Katmandu",
                    description: "Asia/Katmandu time zone"
                },
                {
                    value: "Asia/Krasnoyarsk",
                    display_name: "Asia/Krasnoyarsk",
                    description: "Asia/Krasnoyarsk time zone"
                },
                {
                    value: "Asia/Kuala_Lumpur",
                    display_name: "Asia/Kuala Lumpur",
                    description: "Asia/Kuala_Lumpur time zone"
                },
                {
                    value: "Asia/Kuching",
                    display_name: "Asia/Kuching",
                    description: "Asia/Kuching time zone"
                },
                {
                    value: "Asia/Kuwait",
                    display_name: "Asia/Kuwait",
                    description: "Asia/Kuwait time zone"
                },
                {
                    value: "Asia/Macao",
                    display_name: "Asia/Macao",
                    description: "Asia/Macao time zone"
                },
                {
                    value: "Asia/Macau",
                    display_name: "Asia/Macau",
                    description: "Asia/Macau time zone"
                },
                {
                    value: "Asia/Magadan",
                    display_name: "Asia/Magadan",
                    description: "Asia/Magadan time zone"
                },
                {
                    value: "Asia/Makassar",
                    display_name: "Asia/Makassar",
                    description: "Asia/Makassar time zone"
                },
                {
                    value: "Asia/Manila",
                    display_name: "Asia/Manila",
                    description: "Asia/Manila time zone"
                },
                {
                    value: "Asia/Muscat",
                    display_name: "Asia/Muscat",
                    description: "Asia/Muscat time zone"
                },
                {
                    value: "Asia/Nicosia",
                    display_name: "Asia/Nicosia",
                    description: "Asia/Nicosia time zone"
                },
                {
                    value: "Asia/Novosibirsk",
                    display_name: "Asia/Novosibirsk",
                    description: "Asia/Novosibirsk time zone"
                },
                {
                    value: "Asia/Omsk",
                    display_name: "Asia/Omsk",
                    description: "Asia/Omsk time zone"
                },
                {
                    value: "Asia/Oral",
                    display_name: "Asia/Oral",
                    description: "Asia/Oral time zone"
                },
                {
                    value: "Asia/Phnom_Penh",
                    display_name: "Asia/Phnom Penh",
                    description: "Asia/Phnom_Penh time zone"
                },
                {
                    value: "Asia/Pontianak",
                    display_name: "Asia/Pontianak",
                    description: "Asia/Pontianak time zone"
                },
                {
                    value: "Asia/Pyongyang",
                    display_name: "Asia/Pyongyang",
                    description: "Asia/Pyongyang time zone"
                },
                {
                    value: "Asia/Qatar",
                    display_name: "Asia/Qatar",
                    description: "Asia/Qatar time zone"
                },
                {
                    value: "Asia/Qyzylorda",
                    display_name: "Asia/Qyzylorda",
                    description: "Asia/Qyzylorda time zone"
                },
                {
                    value: "Asia/Rangoon",
                    display_name: "Asia/Rangoon",
                    description: "Asia/Rangoon time zone"
                },
                {
                    value: "Asia/Riyadh",
                    display_name: "Asia/Riyadh",
                    description: "Asia/Riyadh time zone"
                },
                {
                    value: "Asia/Riyadh87",
                    display_name: "Asia/Riyadh87",
                    description: "Asia/Riyadh87 time zone"
                },
                {
                    value: "Asia/Riyadh88",
                    display_name: "Asia/Riyadh88",
                    description: "Asia/Riyadh88 time zone"
                },
                {
                    value: "Asia/Riyadh89",
                    display_name: "Asia/Riyadh89",
                    description: "Asia/Riyadh89 time zone"
                },
                {
                    value: "Asia/Saigon",
                    display_name: "Asia/Saigon",
                    description: "Asia/Saigon time zone"
                },
                {
                    value: "Asia/Sakhalin",
                    display_name: "Asia/Sakhalin",
                    description: "Asia/Sakhalin time zone"
                },
                {
                    value: "Asia/Samarkand",
                    display_name: "Asia/Samarkand",
                    description: "Asia/Samarkand time zone"
                },
                {
                    value: "Asia/Seoul",
                    display_name: "Asia/Seoul",
                    description: "Asia/Seoul time zone"
                },
                {
                    value: "Asia/Shanghai",
                    display_name: "Asia/Shanghai",
                    description: "Asia/Shanghai time zone"
                },
                {
                    value: "Asia/Singapore",
                    display_name: "Asia/Singapore",
                    description: "Asia/Singapore time zone"
                },
                {
                    value: "Asia/Taipei",
                    display_name: "Asia/Taipei",
                    description: "Asia/Taipei time zone"
                },
                {
                    value: "Asia/Tashkent",
                    display_name: "Asia/Tashkent",
                    description: "Asia/Tashkent time zone"
                },
                {
                    value: "Asia/Tbilisi",
                    display_name: "Asia/Tbilisi",
                    description: "Asia/Tbilisi time zone"
                },
                {
                    value: "Asia/Tehran",
                    display_name: "Asia/Tehran",
                    description: "Asia/Tehran time zone"
                },
                {
                    value: "Asia/Tel_Aviv",
                    display_name: "Asia/Tel Aviv",
                    description: "Asia/Tel_Aviv time zone"
                },
                {
                    value: "Asia/Thimbu",
                    display_name: "Asia/Thimbu",
                    description: "Asia/Thimbu time zone"
                },
                {
                    value: "Asia/Thimphu",
                    display_name: "Asia/Thimphu",
                    description: "Asia/Thimphu time zone"
                },
                {
                    value: "Asia/Tokyo",
                    display_name: "Asia/Tokyo",
                    description: "Asia/Tokyo time zone"
                },
                {
                    value: "Asia/Ujung_Pandang",
                    display_name: "Asia/Ujung Pandang",
                    description: "Asia/Ujung_Pandang time zone"
                },
                {
                    value: "Asia/Ulaanbaatar",
                    display_name: "Asia/Ulaanbaatar",
                    description: "Asia/Ulaanbaatar time zone"
                },
                {
                    value: "Asia/Ulan_Bator",
                    display_name: "Asia/Ulan Bator",
                    description: "Asia/Ulan_Bator time zone"
                },
                {
                    value: "Asia/Urumqi",
                    display_name: "Asia/Urumqi",
                    description: "Asia/Urumqi time zone"
                },
                {
                    value: "Asia/Vientiane",
                    display_name: "Asia/Vientiane",
                    description: "Asia/Vientiane time zone"
                },
                {
                    value: "Asia/Vladivostok",
                    display_name: "Asia/Vladivostok",
                    description: "Asia/Vladivostok time zone"
                },
                {
                    value: "Asia/Yakutsk",
                    display_name: "Asia/Yakutsk",
                    description: "Asia/Yakutsk time zone"
                },
                {
                    value: "Asia/Yekaterinburg",
                    display_name: "Asia/Yekaterinburg",
                    description: "Asia/Yekaterinburg time zone"
                },
                {
                    value: "Asia/Yerevan",
                    display_name: "Asia/Yerevan",
                    description: "Asia/Yerevan time zone"
                },
                {
                    value: "Atlantic/Azores",
                    display_name: "Atlantic/Azores",
                    description: "Atlantic/Azores time zone"
                },
                {
                    value: "Atlantic/Bermuda",
                    display_name: "Atlantic/Bermuda",
                    description: "Atlantic/Bermuda time zone"
                },
                {
                    value: "Atlantic/Canary",
                    display_name: "Atlantic/Canary",
                    description: "Atlantic/Canary time zone"
                },
                {
                    value: "Atlantic/Cape_Verde",
                    display_name: "Atlantic/Cape Verde",
                    description: "Atlantic/Cape_Verde time zone"
                },
                {
                    value: "Atlantic/Faeroe",
                    display_name: "Atlantic/Faeroe",
                    description: "Atlantic/Faeroe time zone"
                },
                {
                    value: "Atlantic/Faroe",
                    display_name: "Atlantic/Faroe",
                    description: "Atlantic/Faroe time zone"
                },
                {
                    value: "Atlantic/Jan_Mayen",
                    display_name: "Atlantic/Jan Mayen",
                    description: "Atlantic/Jan_Mayen time zone"
                },
                {
                    value: "Atlantic/Madeira",
                    display_name: "Atlantic/Madeira",
                    description: "Atlantic/Madeira time zone"
                },
                {
                    value: "Atlantic/Reykjavik",
                    display_name: "Atlantic/Reykjavik",
                    description: "Atlantic/Reykjavik time zone"
                },
                {
                    value: "Atlantic/South_Georgia",
                    display_name: "Atlantic/South Georgia",
                    description: "Atlantic/South_Georgia time zone"
                },
                {
                    value: "Atlantic/St_Helena",
                    display_name: "Atlantic/St Helena",
                    description: "Atlantic/St_Helena time zone"
                },
                {
                    value: "Atlantic/Stanley",
                    display_name: "Atlantic/Stanley",
                    description: "Atlantic/Stanley time zone"
                },
                {
                    value: "Australia/ACT",
                    display_name: "Australia/ACT",
                    description: "Australia/ACT time zone"
                },
                {
                    value: "Australia/Adelaide",
                    display_name: "Australia/Adelaide",
                    description: "Australia/Adelaide time zone"
                },
                {
                    value: "Australia/Brisbane",
                    display_name: "Australia/Brisbane",
                    description: "Australia/Brisbane time zone"
                },
                {
                    value: "Australia/Broken_Hill",
                    display_name: "Australia/Broken Hill",
                    description: "Australia/Broken_Hill time zone"
                },
                {
                    value: "Australia/Canberra",
                    display_name: "Australia/Canberra",
                    description: "Australia/Canberra time zone"
                },
                {
                    value: "Australia/Currie",
                    display_name: "Australia/Currie",
                    description: "Australia/Currie time zone"
                },
                {
                    value: "Australia/Darwin",
                    display_name: "Australia/Darwin",
                    description: "Australia/Darwin time zone"
                },
                {
                    value: "Australia/Eucla",
                    display_name: "Australia/Eucla",
                    description: "Australia/Eucla time zone"
                },
                {
                    value: "Australia/Hobart",
                    display_name: "Australia/Hobart",
                    description: "Australia/Hobart time zone"
                },
                {
                    value: "Australia/LHI",
                    display_name: "Australia/LHI",
                    description: "Australia/LHI time zone"
                },
                {
                    value: "Australia/Lindeman",
                    display_name: "Australia/Lindeman",
                    description: "Australia/Lindeman time zone"
                },
                {
                    value: "Australia/Lord_Howe",
                    display_name: "Australia/Lord Howe",
                    description: "Australia/Lord_Howe time zone"
                },
                {
                    value: "Australia/Melbourne",
                    display_name: "Australia/Melbourne",
                    description: "Australia/Melbourne time zone"
                },
                {
                    value: "Australia/NSW",
                    display_name: "Australia/NSW",
                    description: "Australia/NSW time zone"
                },
                {
                    value: "Australia/North",
                    display_name: "Australia/North",
                    description: "Australia/North time zone"
                },
                {
                    value: "Australia/Perth",
                    display_name: "Australia/Perth",
                    description: "Australia/Perth time zone"
                },
                {
                    value: "Australia/Queensland",
                    display_name: "Australia/Queensland",
                    description: "Australia/Queensland time zone"
                },
                {
                    value: "Australia/South",
                    display_name: "Australia/South",
                    description: "Australia/South time zone"
                },
                {
                    value: "Australia/Sydney",
                    display_name: "Australia/Sydney",
                    description: "Australia/Sydney time zone"
                },
                {
                    value: "Australia/Tasmania",
                    display_name: "Australia/Tasmania",
                    description: "Australia/Tasmania time zone"
                },
                {
                    value: "Australia/Victoria",
                    display_name: "Australia/Victoria",
                    description: "Australia/Victoria time zone"
                },
                {
                    value: "Australia/West",
                    display_name: "Australia/West",
                    description: "Australia/West time zone"
                },
                {
                    value: "Australia/Yancowinna",
                    display_name: "Australia/Yancowinna",
                    description: "Australia/Yancowinna time zone"
                },
                {
                    value: "BET",
                    display_name: "BET",
                    description: "BET time zone"
                },
                {
                    value: "BST",
                    display_name: "BST",
                    description: "BST time zone"
                },
                {
                    value: "Brazil/Acre",
                    display_name: "Brazil/Acre",
                    description: "Brazil/Acre time zone"
                },
                {
                    value: "Brazil/DeNoronha",
                    display_name: "Brazil/DeNoronha",
                    description: "Brazil/DeNoronha time zone"
                },
                {
                    value: "Brazil/East",
                    display_name: "Brazil/East",
                    description: "Brazil/East time zone"
                },
                {
                    value: "Brazil/West",
                    display_name: "Brazil/West",
                    description: "Brazil/West time zone"
                },
                {
                    value: "CAT",
                    display_name: "CAT",
                    description: "CAT time zone"
                },
                {
                    value: "CET",
                    display_name: "CET",
                    description: "CET time zone"
                },
                {
                    value: "CNT",
                    display_name: "CNT",
                    description: "CNT time zone"
                },
                {
                    value: "CST",
                    display_name: "CST",
                    description: "CST time zone"
                },
                {
                    value: "CST6CDT",
                    display_name: "CST6CDT",
                    description: "CST6CDT time zone"
                },
                {
                    value: "CTT",
                    display_name: "CTT",
                    description: "CTT time zone"
                },
                {
                    value: "Canada/Atlantic",
                    display_name: "Canada/Atlantic",
                    description: "Canada/Atlantic time zone"
                },
                {
                    value: "Canada/Central",
                    display_name: "Canada/Central",
                    description: "Canada/Central time zone"
                },
                {
                    value: "Canada/East-Saskatchewan",
                    display_name: "Canada/East-Saskatchewan",
                    description: "Canada/East-Saskatchewan time zone"
                },
                {
                    value: "Canada/Eastern",
                    display_name: "Canada/Eastern",
                    description: "Canada/Eastern time zone"
                },
                {
                    value: "Canada/Mountain",
                    display_name: "Canada/Mountain",
                    description: "Canada/Mountain time zone"
                },
                {
                    value: "Canada/Newfoundland",
                    display_name: "Canada/Newfoundland",
                    description: "Canada/Newfoundland time zone"
                },
                {
                    value: "Canada/Pacific",
                    display_name: "Canada/Pacific",
                    description: "Canada/Pacific time zone"
                },
                {
                    value: "Canada/Saskatchewan",
                    display_name: "Canada/Saskatchewan",
                    description: "Canada/Saskatchewan time zone"
                },
                {
                    value: "Canada/Yukon",
                    display_name: "Canada/Yukon",
                    description: "Canada/Yukon time zone"
                },
                {
                    value: "Chile/Continental",
                    display_name: "Chile/Continental",
                    description: "Chile/Continental time zone"
                },
                {
                    value: "Chile/EasterIsland",
                    display_name: "Chile/EasterIsland",
                    description: "Chile/EasterIsland time zone"
                },
                {
                    value: "Cuba",
                    display_name: "Cuba",
                    description: "Cuba time zone"
                },
                {
                    value: "EAT",
                    display_name: "EAT",
                    description: "EAT time zone"
                },
                {
                    value: "ECT",
                    display_name: "ECT",
                    description: "ECT time zone"
                },
                {
                    value: "EET",
                    display_name: "EET",
                    description: "EET time zone"
                },
                {
                    value: "EST",
                    display_name: "EST",
                    description: "EST time zone"
                },
                {
                    value: "EST5EDT",
                    display_name: "EST5EDT",
                    description: "EST5EDT time zone"
                },
                {
                    value: "Egypt",
                    display_name: "Egypt",
                    description: "Egypt time zone"
                },
                {
                    value: "Eire",
                    display_name: "Eire",
                    description: "Eire time zone"
                },
                {
                    value: "Etc/GMT",
                    display_name: "Etc/GMT",
                    description: "Etc/GMT time zone"
                },
                {
                    value: "Etc/GMT+0",
                    display_name: "Etc/GMT+0",
                    description: "Etc/GMT+0 time zone"
                },
                {
                    value: "Etc/GMT+1",
                    display_name: "Etc/GMT+1",
                    description: "Etc/GMT+1 time zone"
                },
                {
                    value: "Etc/GMT+10",
                    display_name: "Etc/GMT+10",
                    description: "Etc/GMT+10 time zone"
                },
                {
                    value: "Etc/GMT+11",
                    display_name: "Etc/GMT+11",
                    description: "Etc/GMT+11 time zone"
                },
                {
                    value: "Etc/GMT+12",
                    display_name: "Etc/GMT+12",
                    description: "Etc/GMT+12 time zone"
                },
                {
                    value: "Etc/GMT+2",
                    display_name: "Etc/GMT+2",
                    description: "Etc/GMT+2 time zone"
                },
                {
                    value: "Etc/GMT+3",
                    display_name: "Etc/GMT+3",
                    description: "Etc/GMT+3 time zone"
                },
                {
                    value: "Etc/GMT+4",
                    display_name: "Etc/GMT+4",
                    description: "Etc/GMT+4 time zone"
                },
                {
                    value: "Etc/GMT+5",
                    display_name: "Etc/GMT+5",
                    description: "Etc/GMT+5 time zone"
                },
                {
                    value: "Etc/GMT+6",
                    display_name: "Etc/GMT+6",
                    description: "Etc/GMT+6 time zone"
                },
                {
                    value: "Etc/GMT+7",
                    display_name: "Etc/GMT+7",
                    description: "Etc/GMT+7 time zone"
                },
                {
                    value: "Etc/GMT+8",
                    display_name: "Etc/GMT+8",
                    description: "Etc/GMT+8 time zone"
                },
                {
                    value: "Etc/GMT+9",
                    display_name: "Etc/GMT+9",
                    description: "Etc/GMT+9 time zone"
                },
                {
                    value: "Etc/GMT-0",
                    display_name: "Etc/GMT-0",
                    description: "Etc/GMT-0 time zone"
                },
                {
                    value: "Etc/GMT-1",
                    display_name: "Etc/GMT-1",
                    description: "Etc/GMT-1 time zone"
                },
                {
                    value: "Etc/GMT-10",
                    display_name: "Etc/GMT-10",
                    description: "Etc/GMT-10 time zone"
                },
                {
                    value: "Etc/GMT-11",
                    display_name: "Etc/GMT-11",
                    description: "Etc/GMT-11 time zone"
                },
                {
                    value: "Etc/GMT-12",
                    display_name: "Etc/GMT-12",
                    description: "Etc/GMT-12 time zone"
                },
                {
                    value: "Etc/GMT-13",
                    display_name: "Etc/GMT-13",
                    description: "Etc/GMT-13 time zone"
                },
                {
                    value: "Etc/GMT-14",
                    display_name: "Etc/GMT-14",
                    description: "Etc/GMT-14 time zone"
                },
                {
                    value: "Etc/GMT-2",
                    display_name: "Etc/GMT-2",
                    description: "Etc/GMT-2 time zone"
                },
                {
                    value: "Etc/GMT-3",
                    display_name: "Etc/GMT-3",
                    description: "Etc/GMT-3 time zone"
                },
                {
                    value: "Etc/GMT-4",
                    display_name: "Etc/GMT-4",
                    description: "Etc/GMT-4 time zone"
                },
                {
                    value: "Etc/GMT-5",
                    display_name: "Etc/GMT-5",
                    description: "Etc/GMT-5 time zone"
                },
                {
                    value: "Etc/GMT-6",
                    display_name: "Etc/GMT-6",
                    description: "Etc/GMT-6 time zone"
                },
                {
                    value: "Etc/GMT-7",
                    display_name: "Etc/GMT-7",
                    description: "Etc/GMT-7 time zone"
                },
                {
                    value: "Etc/GMT-8",
                    display_name: "Etc/GMT-8",
                    description: "Etc/GMT-8 time zone"
                },
                {
                    value: "Etc/GMT-9",
                    display_name: "Etc/GMT-9",
                    description: "Etc/GMT-9 time zone"
                },
                {
                    value: "Etc/GMT0",
                    display_name: "Etc/GMT0",
                    description: "Etc/GMT0 time zone"
                },
                {
                    value: "Etc/Greenwich",
                    display_name: "Etc/Greenwich",
                    description: "Etc/Greenwich time zone"
                },
                {
                    value: "Etc/UCT",
                    display_name: "Etc/UCT",
                    description: "Etc/UCT time zone"
                },
                {
                    value: "Etc/UTC",
                    display_name: "Etc/UTC",
                    description: "Etc/UTC time zone"
                },
                {
                    value: "Etc/Universal",
                    display_name: "Etc/Universal",
                    description: "Etc/Universal time zone"
                },
                {
                    value: "Etc/Zulu",
                    display_name: "Etc/Zulu",
                    description: "Etc/Zulu time zone"
                },
                {
                    value: "Europe/Amsterdam",
                    display_name: "Europe/Amsterdam",
                    description: "Europe/Amsterdam time zone"
                },
                {
                    value: "Europe/Andorra",
                    display_name: "Europe/Andorra",
                    description: "Europe/Andorra time zone"
                },
                {
                    value: "Europe/Athens",
                    display_name: "Europe/Athens",
                    description: "Europe/Athens time zone"
                },
                {
                    value: "Europe/Belfast",
                    display_name: "Europe/Belfast",
                    description: "Europe/Belfast time zone"
                },
                {
                    value: "Europe/Belgrade",
                    display_name: "Europe/Belgrade",
                    description: "Europe/Belgrade time zone"
                },
                {
                    value: "Europe/Berlin",
                    display_name: "Europe/Berlin",
                    description: "Europe/Berlin time zone"
                },
                {
                    value: "Europe/Bratislava",
                    display_name: "Europe/Bratislava",
                    description: "Europe/Bratislava time zone"
                },
                {
                    value: "Europe/Brussels",
                    display_name: "Europe/Brussels",
                    description: "Europe/Brussels time zone"
                },
                {
                    value: "Europe/Bucharest",
                    display_name: "Europe/Bucharest",
                    description: "Europe/Bucharest time zone"
                },
                {
                    value: "Europe/Budapest",
                    display_name: "Europe/Budapest",
                    description: "Europe/Budapest time zone"
                },
                {
                    value: "Europe/Chisinau",
                    display_name: "Europe/Chisinau",
                    description: "Europe/Chisinau time zone"
                },
                {
                    value: "Europe/Copenhagen",
                    display_name: "Europe/Copenhagen",
                    description: "Europe/Copenhagen time zone"
                },
                {
                    value: "Europe/Dublin",
                    display_name: "Europe/Dublin",
                    description: "Europe/Dublin time zone"
                },
                {
                    value: "Europe/Gibraltar",
                    display_name: "Europe/Gibraltar",
                    description: "Europe/Gibraltar time zone"
                },
                {
                    value: "Europe/Guernsey",
                    display_name: "Europe/Guernsey",
                    description: "Europe/Guernsey time zone"
                },
                {
                    value: "Europe/Helsinki",
                    display_name: "Europe/Helsinki",
                    description: "Europe/Helsinki time zone"
                },
                {
                    value: "Europe/Isle_of_Man",
                    display_name: "Europe/Isle of Man",
                    description: "Europe/Isle_of_Man time zone"
                },
                {
                    value: "Europe/Istanbul",
                    display_name: "Europe/Istanbul",
                    description: "Europe/Istanbul time zone"
                },
                {
                    value: "Europe/Jersey",
                    display_name: "Europe/Jersey",
                    description: "Europe/Jersey time zone"
                },
                {
                    value: "Europe/Kaliningrad",
                    display_name: "Europe/Kaliningrad",
                    description: "Europe/Kaliningrad time zone"
                },
                {
                    value: "Europe/Kiev",
                    display_name: "Europe/Kiev",
                    description: "Europe/Kiev time zone"
                },
                {
                    value: "Europe/Lisbon",
                    display_name: "Europe/Lisbon",
                    description: "Europe/Lisbon time zone"
                },
                {
                    value: "Europe/Ljubljana",
                    display_name: "Europe/Ljubljana",
                    description: "Europe/Ljubljana time zone"
                },
                {
                    value: "Europe/London",
                    display_name: "Europe/London",
                    description: "Europe/London time zone"
                },
                {
                    value: "Europe/Luxembourg",
                    display_name: "Europe/Luxembourg",
                    description: "Europe/Luxembourg time zone"
                },
                {
                    value: "Europe/Madrid",
                    display_name: "Europe/Madrid",
                    description: "Europe/Madrid time zone"
                },
                {
                    value: "Europe/Malta",
                    display_name: "Europe/Malta",
                    description: "Europe/Malta time zone"
                },
                {
                    value: "Europe/Mariehamn",
                    display_name: "Europe/Mariehamn",
                    description: "Europe/Mariehamn time zone"
                },
                {
                    value: "Europe/Minsk",
                    display_name: "Europe/Minsk",
                    description: "Europe/Minsk time zone"
                },
                {
                    value: "Europe/Monaco",
                    display_name: "Europe/Monaco",
                    description: "Europe/Monaco time zone"
                },
                {
                    value: "Europe/Moscow",
                    display_name: "Europe/Moscow",
                    description: "Europe/Moscow time zone"
                },
                {
                    value: "Europe/Nicosia",
                    display_name: "Europe/Nicosia",
                    description: "Europe/Nicosia time zone"
                },
                {
                    value: "Europe/Oslo",
                    display_name: "Europe/Oslo",
                    description: "Europe/Oslo time zone"
                },
                {
                    value: "Europe/Paris",
                    display_name: "Europe/Paris",
                    description: "Europe/Paris time zone"
                },
                {
                    value: "Europe/Podgorica",
                    display_name: "Europe/Podgorica",
                    description: "Europe/Podgorica time zone"
                },
                {
                    value: "Europe/Prague",
                    display_name: "Europe/Prague",
                    description: "Europe/Prague time zone"
                },
                {
                    value: "Europe/Riga",
                    display_name: "Europe/Riga",
                    description: "Europe/Riga time zone"
                },
                {
                    value: "Europe/Rome",
                    display_name: "Europe/Rome",
                    description: "Europe/Rome time zone"
                },
                {
                    value: "Europe/Samara",
                    display_name: "Europe/Samara",
                    description: "Europe/Samara time zone"
                },
                {
                    value: "Europe/San_Marino",
                    display_name: "Europe/San Marino",
                    description: "Europe/San_Marino time zone"
                },
                {
                    value: "Europe/Sarajevo",
                    display_name: "Europe/Sarajevo",
                    description: "Europe/Sarajevo time zone"
                },
                {
                    value: "Europe/Simferopol",
                    display_name: "Europe/Simferopol",
                    description: "Europe/Simferopol time zone"
                },
                {
                    value: "Europe/Skopje",
                    display_name: "Europe/Skopje",
                    description: "Europe/Skopje time zone"
                },
                {
                    value: "Europe/Sofia",
                    display_name: "Europe/Sofia",
                    description: "Europe/Sofia time zone"
                },
                {
                    value: "Europe/Stockholm",
                    display_name: "Europe/Stockholm",
                    description: "Europe/Stockholm time zone"
                },
                {
                    value: "Europe/Tallinn",
                    display_name: "Europe/Tallinn",
                    description: "Europe/Tallinn time zone"
                },
                {
                    value: "Europe/Tirane",
                    display_name: "Europe/Tirane",
                    description: "Europe/Tirane time zone"
                },
                {
                    value: "Europe/Tiraspol",
                    display_name: "Europe/Tiraspol",
                    description: "Europe/Tiraspol time zone"
                },
                {
                    value: "Europe/Uzhgorod",
                    display_name: "Europe/Uzhgorod",
                    description: "Europe/Uzhgorod time zone"
                },
                {
                    value: "Europe/Vaduz",
                    display_name: "Europe/Vaduz",
                    description: "Europe/Vaduz time zone"
                },
                {
                    value: "Europe/Vatican",
                    display_name: "Europe/Vatican",
                    description: "Europe/Vatican time zone"
                },
                {
                    value: "Europe/Vienna",
                    display_name: "Europe/Vienna",
                    description: "Europe/Vienna time zone"
                },
                {
                    value: "Europe/Vilnius",
                    display_name: "Europe/Vilnius",
                    description: "Europe/Vilnius time zone"
                },
                {
                    value: "Europe/Volgograd",
                    display_name: "Europe/Volgograd",
                    description: "Europe/Volgograd time zone"
                },
                {
                    value: "Europe/Warsaw",
                    display_name: "Europe/Warsaw",
                    description: "Europe/Warsaw time zone"
                },
                {
                    value: "Europe/Zagreb",
                    display_name: "Europe/Zagreb",
                    description: "Europe/Zagreb time zone"
                },
                {
                    value: "Europe/Zaporozhye",
                    display_name: "Europe/Zaporozhye",
                    description: "Europe/Zaporozhye time zone"
                },
                {
                    value: "Europe/Zurich",
                    display_name: "Europe/Zurich",
                    description: "Europe/Zurich time zone"
                },
                {
                    value: "GB",
                    display_name: "GB",
                    description: "GB time zone"
                },
                {
                    value: "GB-Eire",
                    display_name: "GB-Eire",
                    description: "GB-Eire time zone"
                },
                {
                    value: "GMT",
                    display_name: "GMT",
                    description: "GMT time zone"
                },
                {
                    value: "GMT0",
                    display_name: "GMT0",
                    description: "GMT0 time zone"
                },
                {
                    value: "Greenwich",
                    display_name: "Greenwich",
                    description: "Greenwich time zone"
                },
                {
                    value: "HST",
                    display_name: "HST",
                    description: "HST time zone"
                },
                {
                    value: "Hongkong",
                    display_name: "Hongkong",
                    description: "Hongkong time zone"
                },
                {
                    value: "IET",
                    display_name: "IET",
                    description: "IET time zone"
                },
                {
                    value: "IST",
                    display_name: "IST",
                    description: "IST time zone"
                },
                {
                    value: "Iceland",
                    display_name: "Iceland",
                    description: "Iceland time zone"
                },
                {
                    value: "Indian/Antananarivo",
                    display_name: "Indian/Antananarivo",
                    description: "Indian/Antananarivo time zone"
                },
                {
                    value: "Indian/Chagos",
                    display_name: "Indian/Chagos",
                    description: "Indian/Chagos time zone"
                },
                {
                    value: "Indian/Christmas",
                    display_name: "Indian/Christmas",
                    description: "Indian/Christmas time zone"
                },
                {
                    value: "Indian/Cocos",
                    display_name: "Indian/Cocos",
                    description: "Indian/Cocos time zone"
                },
                {
                    value: "Indian/Comoro",
                    display_name: "Indian/Comoro",
                    description: "Indian/Comoro time zone"
                },
                {
                    value: "Indian/Kerguelen",
                    display_name: "Indian/Kerguelen",
                    description: "Indian/Kerguelen time zone"
                },
                {
                    value: "Indian/Mahe",
                    display_name: "Indian/Mahe",
                    description: "Indian/Mahe time zone"
                },
                {
                    value: "Indian/Maldives",
                    display_name: "Indian/Maldives",
                    description: "Indian/Maldives time zone"
                },
                {
                    value: "Indian/Mauritius",
                    display_name: "Indian/Mauritius",
                    description: "Indian/Mauritius time zone"
                },
                {
                    value: "Indian/Mayotte",
                    display_name: "Indian/Mayotte",
                    description: "Indian/Mayotte time zone"
                },
                {
                    value: "Indian/Reunion",
                    display_name: "Indian/Reunion",
                    description: "Indian/Reunion time zone"
                },
                {
                    value: "Iran",
                    display_name: "Iran",
                    description: "Iran time zone"
                },
                {
                    value: "Israel",
                    display_name: "Israel",
                    description: "Israel time zone"
                },
                {
                    value: "JST",
                    display_name: "JST",
                    description: "JST time zone"
                },
                {
                    value: "Jamaica",
                    display_name: "Jamaica",
                    description: "Jamaica time zone"
                },
                {
                    value: "Japan",
                    display_name: "Japan",
                    description: "Japan time zone"
                },
                {
                    value: "Kwajalein",
                    display_name: "Kwajalein",
                    description: "Kwajalein time zone"
                },
                {
                    value: "Libya",
                    display_name: "Libya",
                    description: "Libya time zone"
                },
                {
                    value: "MET",
                    display_name: "MET",
                    description: "MET time zone"
                },
                {
                    value: "MIT",
                    display_name: "MIT",
                    description: "MIT time zone"
                },
                {
                    value: "MST",
                    display_name: "MST",
                    description: "MST time zone"
                },
                {
                    value: "MST7MDT",
                    display_name: "MST7MDT",
                    description: "MST7MDT time zone"
                },
                {
                    value: "Mexico/BajaNorte",
                    display_name: "Mexico/BajaNorte",
                    description: "Mexico/BajaNorte time zone"
                },
                {
                    value: "Mexico/BajaSur",
                    display_name: "Mexico/BajaSur",
                    description: "Mexico/BajaSur time zone"
                },
                {
                    value: "Mexico/General",
                    display_name: "Mexico/General",
                    description: "Mexico/General time zone"
                },
                {
                    value: "Mideast/Riyadh87",
                    display_name: "Mideast/Riyadh87",
                    description: "Mideast/Riyadh87 time zone"
                },
                {
                    value: "Mideast/Riyadh88",
                    display_name: "Mideast/Riyadh88",
                    description: "Mideast/Riyadh88 time zone"
                },
                {
                    value: "Mideast/Riyadh89",
                    display_name: "Mideast/Riyadh89",
                    description: "Mideast/Riyadh89 time zone"
                },
                {
                    value: "NET",
                    display_name: "NET",
                    description: "NET time zone"
                },
                {
                    value: "NST",
                    display_name: "NST",
                    description: "NST time zone"
                },
                {
                    value: "NZ",
                    display_name: "NZ",
                    description: "NZ time zone"
                },
                {
                    value: "NZ-CHAT",
                    display_name: "NZ-CHAT",
                    description: "NZ-CHAT time zone"
                },
                {
                    value: "Navajo",
                    display_name: "Navajo",
                    description: "Navajo time zone"
                },
                {
                    value: "PLT",
                    display_name: "PLT",
                    description: "PLT time zone"
                },
                {
                    value: "PNT",
                    display_name: "PNT",
                    description: "PNT time zone"
                },
                {
                    value: "PRC",
                    display_name: "PRC",
                    description: "PRC time zone"
                },
                {
                    value: "PRT",
                    display_name: "PRT",
                    description: "PRT time zone"
                },
                {
                    value: "PST",
                    display_name: "PST",
                    description: "PST time zone"
                },
                {
                    value: "PST8PDT",
                    display_name: "PST8PDT",
                    description: "PST8PDT time zone"
                },
                {
                    value: "Pacific/Apia",
                    display_name: "Pacific/Apia",
                    description: "Pacific/Apia time zone"
                },
                {
                    value: "Pacific/Auckland",
                    display_name: "Pacific/Auckland",
                    description: "Pacific/Auckland time zone"
                },
                {
                    value: "Pacific/Chatham",
                    display_name: "Pacific/Chatham",
                    description: "Pacific/Chatham time zone"
                },
                {
                    value: "Pacific/Easter",
                    display_name: "Pacific/Easter",
                    description: "Pacific/Easter time zone"
                },
                {
                    value: "Pacific/Efate",
                    display_name: "Pacific/Efate",
                    description: "Pacific/Efate time zone"
                },
                {
                    value: "Pacific/Enderbury",
                    display_name: "Pacific/Enderbury",
                    description: "Pacific/Enderbury time zone"
                },
                {
                    value: "Pacific/Fakaofo",
                    display_name: "Pacific/Fakaofo",
                    description: "Pacific/Fakaofo time zone"
                },
                {
                    value: "Pacific/Fiji",
                    display_name: "Pacific/Fiji",
                    description: "Pacific/Fiji time zone"
                },
                {
                    value: "Pacific/Funafuti",
                    display_name: "Pacific/Funafuti",
                    description: "Pacific/Funafuti time zone"
                },
                {
                    value: "Pacific/Galapagos",
                    display_name: "Pacific/Galapagos",
                    description: "Pacific/Galapagos time zone"
                },
                {
                    value: "Pacific/Gambier",
                    display_name: "Pacific/Gambier",
                    description: "Pacific/Gambier time zone"
                },
                {
                    value: "Pacific/Guadalcanal",
                    display_name: "Pacific/Guadalcanal",
                    description: "Pacific/Guadalcanal time zone"
                },
                {
                    value: "Pacific/Guam",
                    display_name: "Pacific/Guam",
                    description: "Pacific/Guam time zone"
                },
                {
                    value: "Pacific/Honolulu",
                    display_name: "Pacific/Honolulu",
                    description: "Pacific/Honolulu time zone"
                },
                {
                    value: "Pacific/Johnston",
                    display_name: "Pacific/Johnston",
                    description: "Pacific/Johnston time zone"
                },
                {
                    value: "Pacific/Kiritimati",
                    display_name: "Pacific/Kiritimati",
                    description: "Pacific/Kiritimati time zone"
                },
                {
                    value: "Pacific/Kosrae",
                    display_name: "Pacific/Kosrae",
                    description: "Pacific/Kosrae time zone"
                },
                {
                    value: "Pacific/Kwajalein",
                    display_name: "Pacific/Kwajalein",
                    description: "Pacific/Kwajalein time zone"
                },
                {
                    value: "Pacific/Majuro",
                    display_name: "Pacific/Majuro",
                    description: "Pacific/Majuro time zone"
                },
                {
                    value: "Pacific/Marquesas",
                    display_name: "Pacific/Marquesas",
                    description: "Pacific/Marquesas time zone"
                },
                {
                    value: "Pacific/Midway",
                    display_name: "Pacific/Midway",
                    description: "Pacific/Midway time zone"
                },
                {
                    value: "Pacific/Nauru",
                    display_name: "Pacific/Nauru",
                    description: "Pacific/Nauru time zone"
                },
                {
                    value: "Pacific/Niue",
                    display_name: "Pacific/Niue",
                    description: "Pacific/Niue time zone"
                },
                {
                    value: "Pacific/Norfolk",
                    display_name: "Pacific/Norfolk",
                    description: "Pacific/Norfolk time zone"
                },
                {
                    value: "Pacific/Noumea",
                    display_name: "Pacific/Noumea",
                    description: "Pacific/Noumea time zone"
                },
                {
                    value: "Pacific/Pago_Pago",
                    display_name: "Pacific/Pago Pago",
                    description: "Pacific/Pago_Pago time zone"
                },
                {
                    value: "Pacific/Palau",
                    display_name: "Pacific/Palau",
                    description: "Pacific/Palau time zone"
                },
                {
                    value: "Pacific/Pitcairn",
                    display_name: "Pacific/Pitcairn",
                    description: "Pacific/Pitcairn time zone"
                },
                {
                    value: "Pacific/Ponape",
                    display_name: "Pacific/Ponape",
                    description: "Pacific/Ponape time zone"
                },
                {
                    value: "Pacific/Port_Moresby",
                    display_name: "Pacific/Port Moresby",
                    description: "Pacific/Port_Moresby time zone"
                },
                {
                    value: "Pacific/Rarotonga",
                    display_name: "Pacific/Rarotonga",
                    description: "Pacific/Rarotonga time zone"
                },
                {
                    value: "Pacific/Saipan",
                    display_name: "Pacific/Saipan",
                    description: "Pacific/Saipan time zone"
                },
                {
                    value: "Pacific/Samoa",
                    display_name: "Pacific/Samoa",
                    description: "Pacific/Samoa time zone"
                },
                {
                    value: "Pacific/Tahiti",
                    display_name: "Pacific/Tahiti",
                    description: "Pacific/Tahiti time zone"
                },
                {
                    value: "Pacific/Tarawa",
                    display_name: "Pacific/Tarawa",
                    description: "Pacific/Tarawa time zone"
                },
                {
                    value: "Pacific/Tongatapu",
                    display_name: "Pacific/Tongatapu",
                    description: "Pacific/Tongatapu time zone"
                },
                {
                    value: "Pacific/Truk",
                    display_name: "Pacific/Truk",
                    description: "Pacific/Truk time zone"
                },
                {
                    value: "Pacific/Wake",
                    display_name: "Pacific/Wake",
                    description: "Pacific/Wake time zone"
                },
                {
                    value: "Pacific/Wallis",
                    display_name: "Pacific/Wallis",
                    description: "Pacific/Wallis time zone"
                },
                {
                    value: "Pacific/Yap",
                    display_name: "Pacific/Yap",
                    description: "Pacific/Yap time zone"
                },
                {
                    value: "Poland",
                    display_name: "Poland",
                    description: "Poland time zone"
                },
                {
                    value: "Portugal",
                    display_name: "Portugal",
                    description: "Portugal time zone"
                },
                {
                    value: "ROK",
                    display_name: "ROK",
                    description: "ROK time zone"
                },
                {
                    value: "SST",
                    display_name: "SST",
                    description: "SST time zone"
                },
                {
                    value: "Singapore",
                    display_name: "Singapore",
                    description: "Singapore time zone"
                },
                {
                    value: "SystemV/AST4",
                    display_name: "SystemV/AST4",
                    description: "SystemV/AST4 time zone"
                },
                {
                    value: "SystemV/AST4ADT",
                    display_name: "SystemV/AST4ADT",
                    description: "SystemV/AST4ADT time zone"
                },
                {
                    value: "SystemV/CST6",
                    display_name: "SystemV/CST6",
                    description: "SystemV/CST6 time zone"
                },
                {
                    value: "SystemV/CST6CDT",
                    display_name: "SystemV/CST6CDT",
                    description: "SystemV/CST6CDT time zone"
                },
                {
                    value: "SystemV/EST5",
                    display_name: "SystemV/EST5",
                    description: "SystemV/EST5 time zone"
                },
                {
                    value: "SystemV/EST5EDT",
                    display_name: "SystemV/EST5EDT",
                    description: "SystemV/EST5EDT time zone"
                },
                {
                    value: "SystemV/HST10",
                    display_name: "SystemV/HST10",
                    description: "SystemV/HST10 time zone"
                },
                {
                    value: "SystemV/MST7",
                    display_name: "SystemV/MST7",
                    description: "SystemV/MST7 time zone"
                },
                {
                    value: "SystemV/MST7MDT",
                    display_name: "SystemV/MST7MDT",
                    description: "SystemV/MST7MDT time zone"
                },
                {
                    value: "SystemV/PST8",
                    display_name: "SystemV/PST8",
                    description: "SystemV/PST8 time zone"
                },
                {
                    value: "SystemV/PST8PDT",
                    display_name: "SystemV/PST8PDT",
                    description: "SystemV/PST8PDT time zone"
                },
                {
                    value: "SystemV/YST9",
                    display_name: "SystemV/YST9",
                    description: "SystemV/YST9 time zone"
                },
                {
                    value: "SystemV/YST9YDT",
                    display_name: "SystemV/YST9YDT",
                    description: "SystemV/YST9YDT time zone"
                },
                {
                    value: "Turkey",
                    display_name: "Turkey",
                    description: "Turkey time zone"
                },
                {
                    value: "UCT",
                    display_name: "UCT",
                    description: "UCT time zone"
                },
                {
                    value: "US/Alaska",
                    display_name: "US/Alaska",
                    description: "US/Alaska time zone"
                },
                {
                    value: "US/Aleutian",
                    display_name: "US/Aleutian",
                    description: "US/Aleutian time zone"
                },
                {
                    value: "US/Arizona",
                    display_name: "US/Arizona",
                    description: "US/Arizona time zone"
                },
                {
                    value: "US/Central",
                    display_name: "US/Central",
                    description: "US/Central time zone"
                },
                {
                    value: "US/East-Indiana",
                    display_name: "US/East-Indiana",
                    description: "US/East-Indiana time zone"
                },
                {
                    value: "US/Eastern",
                    display_name: "US/Eastern",
                    description: "US/Eastern time zone"
                },
                {
                    value: "US/Hawaii",
                    display_name: "US/Hawaii",
                    description: "US/Hawaii time zone"
                },
                {
                    value: "US/Indiana-Starke",
                    display_name: "US/Indiana-Starke",
                    description: "US/Indiana-Starke time zone"
                },
                {
                    value: "US/Michigan",
                    display_name: "US/Michigan",
                    description: "US/Michigan time zone"
                },
                {
                    value: "US/Mountain",
                    display_name: "US/Mountain",
                    description: "US/Mountain time zone"
                },
                {
                    value: "US/Pacific",
                    display_name: "US/Pacific",
                    description: "US/Pacific time zone"
                },
                {
                    value: "US/Pacific-New",
                    display_name: "US/Pacific-New",
                    description: "US/Pacific-New time zone"
                },
                {
                    value: "US/Samoa",
                    display_name: "US/Samoa",
                    description: "US/Samoa time zone"
                },
                {
                    value: "UTC",
                    display_name: "UTC",
                    description: "UTC time zone"
                },
                {
                    value: "Universal",
                    display_name: "Universal",
                    description: "Universal time zone"
                },
                {
                    value: "VST",
                    display_name: "VST",
                    description: "VST time zone"
                },
                {
                    value: "W-SU",
                    display_name: "W-SU",
                    description: "W-SU time zone"
                },
                {
                    value: "WET",
                    display_name: "WET",
                    description: "WET time zone"
                },
                {
                    value: "Zulu",
                    display_name: "Zulu",
                    description: "Zulu time zone"
                }
            ]
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_interval",
            display_name: "Interval",
            description: "Time interval in milli-seconds between consecutive firing of triggers (Only applicable when Type is Simple or Hybrid.)",
            value: this.properties.scheduler_interval,
            type: Ws.PropertiesSheet.INTEGER,
            units: "ms"
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_repeat",
            display_name: "Repeat",
            description: "Number of times this trigger should repeat or keyword: indefinite (Only applicable when Type is Simple or Hybrid.)",
            value: this.properties.scheduler_repeat,
            type: Ws.PropertiesSheet.STRING
        });

        container.sections[0].properties.push({
            name: "properties_scheduler_cron_expr",
            display_name: "Cron Expression",
            description: "Quartz Cron Expression of when the trigger should fire (Only applicable when Type is Cron or Hybrid.)",
            value: this.properties.scheduler_cron_expr,
            type: Ws.PropertiesSheet.STRING
        });


        container.sections[0].properties.push({
            name: "properties_scheduler_duration",
            display_name: "Duration of Simple Trigger",
            description: "Duration in milli-seconds from the beginning time determined by Cron Expression until when the Simple Trigger will end (Only applicable when Type is Hybrid.)",
            value: this.properties.scheduler_duration,
            type: Ws.PropertiesSheet.INTEGER,
            units: "ms"
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var SchedulerAdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(SCHEDULER_ADAPTER, SchedulerAdapterNode);
Ws.Graph.NodeUiFactory.register_type(SCHEDULER_ADAPTER, SchedulerAdapterNodeUi);
