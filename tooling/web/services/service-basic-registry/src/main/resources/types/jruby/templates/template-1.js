{
    id: "jruby-template",

    display_name: "JRuby Service",
    description: "Basic template for JRuby services.",
    icon: null,
    tags: ["template", "service", "jruby"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10500,

    section: "templates",

    item_data: {
        node_type: "jruby",
        node_data: {
            properties: {
                code_name: "jruby-",
                is_shared: false,

                jruby_code: null,

                noop: null
            },

            ui_properties: {
                display_name: "JRuby ",
                description: "Executes arbitrary Ruby code.",
                icon: null,

                noop: null
            }
        }
    }
}