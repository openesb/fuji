/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.tooling.web.services.registry;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyInfo.DataType;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 *
 * @author chikkala
 */
public class ServiceTypeDescriptor implements TypeDescriptor {

    private static final Logger LOG = Logger.getLogger(ServiceTypeDescriptor.class.getName());
    /**
     * delegator
     */
    private ServiceDescriptor mSD;
    private URL mArchetypeURL = null;
    private URL mPluginURL = null;
    private URL mDragShadowURL = null;
    private URL mScriptURL = null;
    private List<URL> mTemplateURL = null;

    public ServiceTypeDescriptor(ServiceDescriptor sd) {
        this.mSD = sd;
        initURLs();
    }

    private void initURLs() {
        Map<String, URL> webRes = this.mSD.getWebResources();
        for (String key : webRes.keySet()) {
            if (key.endsWith("script.js")) {
                this.mScriptURL = webRes.get(key);
            } else if (key.endsWith("template.js")) {
                if (this.mTemplateURL == null) {
                    this.mTemplateURL = new ArrayList<URL>();
                }
                this.mTemplateURL.add(webRes.get(key));
            } else if (key.endsWith("drag-shadow.png")) {
                this.mDragShadowURL = webRes.get(key);
            } else if (key.endsWith("archetype.jar")) {
                this.mArchetypeURL = webRes.get(key);
            } else if ( key.endsWith("plugin.jar")) {
                this.mPluginURL = webRes.get(key);
            }
        }
    }

    public String getType() {
        return this.mSD.getServiceType();
    }

    public URL getArchetype() {
        return this.mArchetypeURL;
    }

    public List<URL> getInstallers() {
        List<URL> list = new ArrayList<URL>();
        return list;
    }

    public URL getPackager() {
        return this.mPluginURL;
    }

    public List<URL> getIcons() {
        return this.mSD.getIcons();
    }

    public URL getDragShadow() {
        return this.mDragShadowURL;
    }

    public String getScript() {
        // If the user provided a custom javascript type descriptor, use it. Otherwise,
        // we can generate a default one from the service descriptor.
        if (mScriptURL != null) {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                Utils.copy(mScriptURL, out);
                out.close();

                return out.toString();
            } catch (Exception ex) {
                LOG.log(Level.INFO, ex.getMessage(), ex);
            }
        } else {
            return generateDefaultScript();
        }

        return null;
    }

    public List<String> getTemplates() {
        List<String> templates = new ArrayList<String>();
        if (this.mTemplateURL == null) {
            return templates;
        }
        for (URL url : this.mTemplateURL) {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                Utils.copy(url, out);
                out.close();
                templates.add(out.toString());
            } catch (Exception ex) {
                LOG.log(Level.INFO, ex.getMessage(), ex);
            }
        }
        return templates;
    }

    private String generateDefaultScript() {
        final String type = mSD.getServiceType();
        final String allCapsType = type.toUpperCase();
        final String firstCapType = allCapsType.substring(0, 1) + type.substring(1);

        String script = "";

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Utils.copy(getClass().getResource("default-script-stub.txt"), out);
            out.close();
            script = out.toString();
        } catch (IOException e) {
            LOG.log(Level.INFO, e.getMessage(), e);
        }

        script = script.replace("${type}", type);
        script = script.replace("${all-caps-type}", allCapsType);
        script = script.replace("${first-cap-type}", firstCapType);

        final StringBuilder propertiesDefaults = new StringBuilder();
        final StringBuilder propertiesDefinitions = new StringBuilder();

        final List<Configuration> configurations = mSD.getConfigurations();

        if (!configurations.contains(mSD.getDefaultConfiguration())) {
            configurations.add(0, mSD.getDefaultConfiguration());
        }

        // Build a full list of properties.
        List<Property> properties = new LinkedList<Property>();
        for (Configuration configuration: configurations) {
            for (Property property: configuration.getPropertyList()) {
                // properties.contains() does not work here, we'll iterate and check
                // properties names...
                boolean propertyExists = false;
                for (Property existing: properties) {
                    if (existing.getName().equals(property.getName())) {
                        propertyExists = true;
                        break;
                    }
                }
                
                if (!propertyExists) {
                    properties.add(property);
                }
            }
        }

        // First handle the 'service_mode' property -- it is needed is there is more than
        // one configuration.
        if (configurations.size() > 1) {
            propertiesDefaults.append(
                    "            service_mode: " +
                    "\"" + mSD.getDefaultConfiguration().getName() + "\",\n");
            propertiesDefinitions.append(
                    "        container.sections[0].properties.push({\n" +
                    "            name: \"properties_service_mode\",\n" +
                    "            type: Ws.PropertiesSheet.ENUM,\n" +
                    "            value: this.properties.service_mode,\n" +
                    "            \n" +
                    "            display_name: \"Mode\",\n" +
                    "            description: \"Mode of operation of this service.\",\n" +
                    "            \n" +
                    "            values: [\n");
            for (int i = 0; i < configurations.size(); i++) {
                final Configuration configuration = configurations.get(i);

                final String confName =
                        configuration.getName();
                String confDisplayName = 
                        configuration.getInfo().getDisplayName();
                String confDescription = 
                        configuration.getInfo().getShortDescription();

                if (confDisplayName == null) {
                    confDisplayName = confName;
                } else {
                    confDisplayName =
                            confDisplayName.replace("\"", "\\\"").replace("\n", "\\n");
                }

                if (confDescription == null) {
                    confDescription = "";
                } else {
                    confDescription =
                            confDescription.replace("\"", "\\\"").replace("\n", "\\n");
                }

                propertiesDefinitions.append(
                        "                {\n" +
                        "                    value: \"" + confName + "\",\n" +
                        "                    display_name: \"" + confDisplayName + "\",\n" +
                        "                    description: \"" + confDescription + "\"\n" +
                        "                }"
                        );

                propertiesDefinitions.append(i < configurations.size() - 1 ? ",\n" : "\n");
            }
            propertiesDefinitions.append(
                    "            ]\n" +
                    "        });\n" +
                    "        \n"
                    );
        }

        for (int i = 0; i < properties.size(); i++) {
            Property property = properties.get(i);

            final String name = property.getName().
                    replace(".", "_");
            final String displayName = property.getInfo().getDisplayName().
                    replace("<html>", "").
                    replace("</html>", "").
                    replace("<br>", "\n").
                    replace("\"", "\\\"").
                    replace("\n", "\\n");
            final String description = property.getInfo().getShortDescription().
                    replace("<html>", "").
                    replace("</html>", "").
                    replace("<br>", "\n").
                    replace("\"", "\\\"").
                    replace("\n", "\\n");
            final String defaultValue = property.getInfo().getDefaultValue();
            final DataType dataType = property.getInfo().getDataType();
            
            String propertyType;
            String propertyDefaultValue;
            switch (dataType) {
                case BOOLEAN:
                    propertyType = "BOOLEAN";
                    propertyDefaultValue = defaultValue;
                    break;
                case INT:
                    propertyType = "INTEGER";
                    propertyDefaultValue = defaultValue;
                    break;
                default:
                    propertyType = "STRING";
                    propertyDefaultValue =
                            "\"" +
                            defaultValue.replace("\"", "\\\"").replace("\n", "\\n") +
                            "\"";
            }

            propertiesDefaults.append(
                    "            " + name + ": " + propertyDefaultValue + "");
            propertiesDefaults.append(i < properties.size() ? ",\n" : "\n");

            propertiesDefinitions.append(MessageFormat.format(
                    "        container.sections[0].properties.push('{'\n" +
                    "            name: \"properties_{0}\",\n" +
                    "            type: Ws.PropertiesSheet.{1},\n" +
                    "            value: this.properties.{0},\n" +
                    "            \n" +
                    "            display_name: \"{2}\",\n" +
                    "            description: \"{3}\"",
                    name, propertyType, displayName, description));

            // Now we need to check in which configurations this property is present.
            // Based on this information we'll be able to properly construct the
            // 'depends' statement. Of course this is only applicable if the number of
            // configurations is greater than one.
            if (configurations.size() > 1) {
                propertiesDefinitions.append(
                        ",\n" +
                        "            \n" +
                        "            depends: {\n" +
                        "                or: [\n");

                boolean dependsStarted = false;
                for (int j = 0; j < configurations.size(); j++) {
                    final Configuration configuration = configurations.get(j);

                    boolean propertyIsPresent = false;
                    for (Property confProperty: configuration.getPropertyList()) {
                        if (confProperty.getName().equals(property.getName())) {
                            propertyIsPresent = true;
                            break;
                        }
                    }

                    if (propertyIsPresent) {
                        if (dependsStarted) {
                            propertiesDefinitions.append(",\n");
                        }

                        dependsStarted = true;

                        propertiesDefinitions.append(
                                "                    {\n" +
                                "                        name: \"properties_service_mode\",\n" +
                                "                        value: \"" + configuration.getName() + "\"\n" +
                                "                    }");
                    }
                }

                propertiesDefinitions.append(
                        "\n" +
                        "                ]\n" +
                        "            }\n");
            }

            propertiesDefinitions.append(
                        "\n" +
                        "        });\n" +
                        "        \n");
        }

        script = script.replace("${properties-defaults}", 
                propertiesDefaults.substring(0, propertiesDefaults.length() - 2));
        script = script.replace("${properties-definitions}",
                propertiesDefinitions);

        return script;
    }
}
