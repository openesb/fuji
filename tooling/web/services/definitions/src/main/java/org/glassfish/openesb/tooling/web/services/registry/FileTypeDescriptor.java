/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.registry;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksorokin
 */
public class FileTypeDescriptor implements TypeDescriptor {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private String type;
    private File file;
    private ClassLoader loader;

    public FileTypeDescriptor(
            final File file) throws Exception {

        this(file, null);
    }

    public FileTypeDescriptor(
            final File file,
            final String type) throws Exception {

        this.file = file;
        this.type = type;

        loader = new URLClassLoader(
                new URL[]{file.toURL()},
                getClass().getClassLoader());

        if (this.type == null) {
            JarFile jar = null;

            try {
                jar = new JarFile(file);
                this.type =
                        jar.getManifest().getMainAttributes().getValue("Service-Type");
            } finally {
                try {
                    if (jar != null) {
                        jar.close();
                    }
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to close the JAR file input stream.", e);
                }
            }
        }
    }

    public File getFile() {
        return file;
    }

    public String getType() {
        return type;
    }

    public List<URL> getIcons() {
        // TODO: copy some logic from the classpath type descriptor, we need to support
        // several icons.
        return Arrays.asList(new URL[] {loader.getResource("web/icon.png")});
    }

    public URL getDragShadow() {
        return loader.getResource("web/drag-shadow.png");
    }

    public String getScript() {
        final URL url = loader.getResource("web/script.js");

        InputStream stream = null;
        try {
            return readStream(stream = url.openStream());
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to open (or read from) an input stream from " +
                    "an URL on the classpath.", e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to close the input stream of an " +
                            "URL on the classpath.", e);
                }
            }
        }

        return null;
    }

    public List<String> getTemplates() {
        int counter = 1;

        List<String> list = new LinkedList<String>();

        URL url = null;
        while ((url = loader.getResource("web/templates/template-" + counter + ".js")) != null) {

            InputStream stream = null;
            try {
                list.add(readStream(stream = url.openStream()));
            } catch (IOException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to open (or read from) an input stream from " +
                        "an URL on the classpath.", e);
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        logger.log(
                                Level.WARNING,
                                "Failed to close the input stream of an " +
                                "URL on the classpath.", e);
                    }
                }
            }

            counter++;
        }

        return list;
    }

    public URL getArchetype() {
        return loader.getResource("archetypes/archetype-1.jar");
    }

    public List<URL> getInstallers() {
        int counter = 1;

        List<URL> list = new LinkedList<URL>();

        URL url = null;
        while ((url = loader.getResource("bundles/bundle-" + counter + ".jar")) != null) {
            list.add(url);
            counter++;
        }

        return list;
    }

    public URL getPackager() {
        return loader.getResource("plugins/plugin-1.jar");
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private String readStream(
            final InputStream stream) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        StringBuilder builder = new StringBuilder();

        String line = null;
        while ((line = reader.readLine()) != null) {
            builder.append(line).append("\n");
        }

        return builder.toString();
    }
}
