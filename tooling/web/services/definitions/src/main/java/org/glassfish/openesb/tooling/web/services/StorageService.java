/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.GraphDescriptor;
import org.glassfish.openesb.tooling.web.model.user.UserSkinProperties;
import org.glassfish.openesb.tooling.web.model.user.User;
import java.util.List;

/**
 *
 * @author ksorokin
 */
public interface StorageService {

    // Graphs ----------------------------------------------------------------------------
    public void storeGraph(
            final Graph graph) throws StorageException;

    public Graph retrieveGraph(
            final String codeName) throws StorageException;

    public void deleteGraph(
            final Graph graph) throws StorageException;

    public List<GraphDescriptor> retrieveGraphDescriptors() throws StorageException;

    public boolean graphExists(
            final String codeName) throws StorageException;

    public void storeUserGraphSkin(int userId, int graphId,
            final UserSkinProperties properties)  throws StorageException ;

    // Users -----------------------------------------------------------------------------
    public void storeUser(
            final User user) throws StorageException;

    public User retrieveUser(
            final String username,
            final String passwordHash) throws StorageException;

    public User retrieveUser(
            final String username) throws StorageException;

    public User retrieveUser(
            final Integer entityId) throws StorageException;

    public void deleteUser(
            final User user) throws StorageException;

    public User retrieveNobody() throws StorageException;

    public boolean userExists(String username) throws StorageException;

    // Settings --------------------------------------------------------------------------
    public String getSetting(
            final String name,
            final User user) throws StorageException;

    public void setSetting(
            final String name,
            final String value,
            final User user) throws StorageException;

    // Logging ---------------------------------------------------------------------------
    public void log(
            final String sessionId,
            final User user,
            final String action,
            final String param1,
            final String param2,
            final String param3);
}
