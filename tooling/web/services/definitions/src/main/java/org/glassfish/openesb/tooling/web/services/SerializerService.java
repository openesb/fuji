/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import java.io.File;
import java.util.List;
import org.glassfish.openesb.tooling.web.model.graph.Node;

/**
 *
 * @author ksorokin
 */
public interface SerializerService {

    public void serialize(
            final Graph graph,
            final File directory) throws SerializerException;

    public Graph deserialize(
            final String codeName,
            final File directory) throws SerializerException;

    public Graph deserialize(
            final String codeName,
            final String source) throws SerializerException;

    public String getArtifact(
            final Graph graph,
            final File directory,
            final List<String> artifactPaths) throws SerializerException;

    /**
     * Returns the possible paths for a node's or graphs artifact, given its code name.
     * The return value is a list, as there can be several locations for a given
     * artifact: one primaty and a number (usually one) fallbacks. E.g. (for maven
     * projects) a service's   message.xsd file can reside in
     * src/main/resources/{type}/{name}/message.xsd, or in
     * src/main/resources/wsdl/message.xsd if the service type does not provide a
     * customized one.
     *
     * @param node Node, whose artifact's path we're looking for.
     * @param codeName Code name of the artifact.
     * @return List of potential paths for the artifact within the project directory.
     */
    public List<String> getArtifactPaths(
            final Node node,
            final String codeName);
}
