/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.services.registry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksorokin
 */
public class ClasspathTypeDescriptor implements TypeDescriptor {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static final Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private String type;
    private String prefix;
    private ClassLoader loader;

    public ClasspathTypeDescriptor(
            final String prefix) {

        this(prefix, null, null);
    }

    public ClasspathTypeDescriptor(
            final String prefix,
            final String type) {

        this(prefix, type, null);
    }

    public ClasspathTypeDescriptor(
            final String prefix,
            final String type,
            final ClassLoader loader) {

        this.prefix = prefix;
        this.type = type;

        if (prefix.endsWith("/")) {
            this.prefix = prefix.substring(0, prefix.length() - 1);
        }

        if (this.type == null) {
            final int index = prefix.lastIndexOf("/");

            if (index == -1) {
                this.type = prefix;
            } else {
                this.type = prefix.substring(index + 1, prefix.length());
            }
        }

        this.loader = loader == null ? getClass().getClassLoader() : loader;
    }

    public String getType() {
        return type;
    }

    public List<URL> getIcons() {
        if (loader.getResource(prefix + "/icons/icon-1.png") != null) {
            int counter = 1;

            final List<URL> list = new LinkedList<URL>();

            URL url = null;
            while ((url = loader.getResource(
                    prefix + "/icons/icon-" + counter + ".png")) != null) {

                list.add(url);
                counter++;
            }

            return list;
        } else {
            return Arrays.asList(new URL[] {loader.getResource(prefix + "/icon.png")});
        }
    }

    public URL getDragShadow() {
        return loader.getResource(prefix + "/drag-shadow.png");
    }

    public String getScript() {
        final URL url = loader.getResource(prefix + "/script.js");

        InputStream stream = null;
        try {
            return readStream(stream = url.openStream());
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to open (or read from) an input stream from " +
                    "an URL on the classpath.", e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to close the input stream of an " +
                            "URL on the classpath.", e);
                }
            }
        }

        return null;
    }

    public List<String> getTemplates() {
        int counter = 1;

        final List<String> list = new LinkedList<String>();

        URL url = null;
        while ((url = loader.getResource(
                prefix + "/templates/template-" + counter + ".js")) != null) {

            InputStream stream = null;
            try {
                list.add(readStream(stream = url.openStream()));
            } catch (IOException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to read from a classpath URL stream.", e);
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        logger.log(
                                Level.WARNING,
                                "Failed to close a classpath URL stream.", e);
                    }
                }
            }

            counter++;
        }

        return list;
    }

    public URL getArchetype() {
        return loader.getResource(prefix + "/archetypes/archetype-1.jar");
    }

    public List<URL> getInstallers() {
        int counter = 1;

        final List<URL> list = new LinkedList<URL>();

        URL url = null;
        while ((url = loader.getResource(
                prefix + "/bundles/bundle-" + counter + ".jar")) != null) {

            list.add(url);
            counter++;
        }

        return list;
    }

    public URL getPackager() {
        return loader.getResource(prefix + "/plugins/plugin-1.jar");
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private String readStream(
            final InputStream stream) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        StringBuilder builder = new StringBuilder();

        String line = null;
        while ((line = reader.readLine()) != null) {
            builder.append(line).append("\n");
        }

        return builder.toString();
    }
}
