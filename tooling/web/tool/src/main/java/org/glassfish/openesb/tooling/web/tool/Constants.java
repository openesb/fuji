/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool;

/**
 *
 * @author ksorokin
 */
public interface Constants {

    // Session attributes ----------------------------------------------------------------
    public static final String CURRENT_WORKDIR =
            "fuji.tooling.web.current-workdir"; // NOI18N
    public static final String CURRENT_TEMPDIR =
            "fuji.tooling.web.current-tempdir"; // NOI18N
    public static final String CURRENT_GRAPH =
            "fuji.tooling.web.current-graph"; // NOI18N
    public static final String CURRENT_DRAFT_GRAPH =
            "fuji.tooling.web.current-draft-graph"; // NOI18N
    public static final String CURRENT_USER =
            "fuji.tooling.web.current-user"; // NOI18N

    // HTTP request parameters -----------------------------------------------------------
    public static final String PARAM_USERNAME =
            "username"; // NOI18N
    public static final String PARAM_PASSWORD =
            "password"; // NOI18N
    public static final String PARAM_GRAPH =
            "graph"; // NOI18N
    public static final String PARAM_CODE_NAME =
            "code_name";
    public static final String PARAM_DISPLAY_NAME =
            "display_name";
    public static final String PARAM_DESCRIPTION =
            "description";
    public static final String PARAM_EXPLICIT =
            "explicit";
    public static final String PARAM_FIRST_NAME =
            "first_name";
    public static final String PARAM_LAST_NAME =
            "last_name";
    public static final String PARAM_COMPANY =
            "company";
    public static final String PARAM_EMAIL =
            "email";
    public static final String PARAM_MESSAGE =
            "message";

    // Cookies ---------------------------------------------------------------------------
    public static final String COOKIE_USERNAME =
            "fuji.tooling.web.cookie-username";

    // Untitled graph properties ---------------------------------------------------------
    public static final String UNTITLED_CODENAME_PREFIX =
            "untitled_"; // NOI18N
    public static final String UNTITLED_DISPLAYNAME =
            "Untitled"; // NOI18N
    public static final String UNTITLED_DESCRIPTION =
            ""; // NOI18N
    public static final String UNTITLED_REVISION =
            "0"; // NOI18N

    // Access control options ------------------------------------------------------------
    public static final String ACCESS_CONTROL_NONE =
            "none";
    public static final String ACCESS_CONTROL_OWNER =
            "owner";
    public static final String ACCESS_CONTROL_ACL =
            "acl";
    public static final String ACCESS_CONTROL_ALL =
            "all";

    // Configuration properties ----------------------------------------------------------
    public static final String CONFIG_WORKDIR =
            "fuji.tooling.web.sessions-workdir"; // NOI18N
    public static final String CONFIG_TEMPDIR =
            "fuji.tooling.web.sessions-tempdir"; // NOI18N
    public static final String CONFIG_DEPLOY_ENABLED =
            "fuji.tooling.web.deployment-enabled"; // NOI18N
    public static final String CONFIG_USERS_ENABLED =
            "fuji.tooling.web.users-enabled"; // NOI18N
    public static final String CONFIG_REGISTRATION_ENABLED =
            "fuji.tooling.web.registration-enabled"; // NOI18N
    public static final String CONFIG_FEEDBACK_ENABLED =
            "fuji.tooling.web.feedback-enabled"; // NOI18N
    public static final String CONFIG_LOGGING_ENABLED =
            "fuji.tooling.web.logging-enabled"; // NOI18N
    public static final String CONFIG_ACCESS_CONTROL =
            "fuji.tooling.web.access-control"; // NOI18N

    // SMTP properties (for feedback.)
    public static final String CONFIG_SMTP_HOST =
            "fuji.tooling.web.smtp.host";
    public static final String CONFIG_SMTP_PORT =
            "fuji.tooling.web.smtp.port";
    public static final String CONFIG_SMTP_USERNAME =
            "fuji.tooling.web.smtp.username";
    public static final String CONFIG_SMTP_PASSWORD =
            "fuji.tooling.web.smtp.password";
    public static final String CONFIG_SMTP_USE_SSL =
            "fuji.tooling.web.smtp.use-ssl";
    public static final String CONFIG_SMTP_USE_STARTTLS =
            "fuji.tooling.web.smtp.use-starttls";

    // Feedback properties.
    public static final String CONFIG_FEEDBACK_SMTP_FROM =
            "fuji.tooling.web.feedback.smtp.from";
    public static final String CONFIG_FEEDBACK_SMTP_TO =
            "fuji.tooling.web.feedback.smtp.to";

    // Miscellanea.
    public static final String CONFIG_HTML_APPENDIX =
            "fuji.tooling.web.html-appendix"; // NOI18N

    // Configuration default values ------------------------------------------------------
    public static final String DEFAULT_WORKDIR_NAME =
            "sessions"; // NOI18N
    public static final String DEFAULT_WORKDIR =
            System.getProperty("user.home") + "/.fuji/" + DEFAULT_WORKDIR_NAME; // NOI18N
    public static final String DEFAULT_TEMPDIR_NAME =
            "temp"; // NOI18N
    public static final String DEFAULT_TEMPDIR =
            System.getProperty("java.io.tmpdir"); // NOI18N
    public static final boolean DEFAULT_DEPLOY_ENABLED =
            true;
    public static final boolean DEFAULT_USERS_ENABLED =
            false;
    public static final boolean DEFAULT_REGISTRATION_ENABLED =
            false;
    public static final boolean DEFAULT_FEEDBACK_ENABLED =
            false;
    public static final boolean DEFAULT_LOGGING_ENABLED =
            false;
    public static final String DEFAULT_ACCESS_CONTROL =
            ACCESS_CONTROL_ALL;
    public static final String DEFAULT_HTML_APPENDIX =
            "";

    // HTTP Service bindings -------------------------------------------------------------
    public static final String HTTP_RESOURCE_URL =
            "/fuji/resource"; // NOI18N
    public static final String HTTP_EDITOR_URL =
            "/fuji/editor"; // NOI18N
    public static final String HTTP_GET_COMPONENT_TYPE_URL =
            "/fuji/callbacks/get-component-type"; // NOI18N
    public static final String HTTP_GET_PALETTE_ITEMS_URL =
            "/fuji/callbacks/get-palette-items"; // NOI18N
    public static final String HTTP_GET_GRAPH_DESCRIPTORS_URL =
            "/fuji/callbacks/get-graph-descriptors"; // NOI18N
    public static final String HTTP_EXPLICIT_DEPLOY_URL =
            "/fuji/callbacks/deploy"; // NOI18N
    public static final String HTTP_REESTABLISH_SESSION_URL =
            "/fuji/callbacks/reestablish-session"; // NOI18N
    public static final String HTTP_GET_EXTERNAL_ARTIFACT_URL =
            "/fuji/callbacks/get-external-artifact"; // NOI18N
    public static final String HTTP_SET_EXTERNAL_ARTIFACT_URL =
            "/fuji/callbacks/set-external-artifact"; // NOI18N
    public static final String HTTP_GET_TESTS_URL =
            "/fuji/callbacks/get-tests"; // NOI18N
    public static final String HTTP_UPDATE_TEST_URL =
            "/fuji/callbacks/update-test"; // NOI18N
    public static final String HTTP_DELETE_TEST_URL =
            "/fuji/callbacks/delete-test"; // NOI18N
    public static final String HTTP_GET_TRACING_DATA_URL =
            "/fuji/callbacks/get-tracing-data"; // NOI18N
    public static final String HTTP_RUN_TEST_DATA_URL =
            "/fuji/callbacks/run-test"; // NOI18N
    public static final String HTTP_NEW_URL =
            "/fuji/callbacks/new"; // NOI18N
    public static final String HTTP_OPEN_URL =
            "/fuji/callbacks/open"; // NOI18N
    public static final String HTTP_SAVE_URL =
            "/fuji/callbacks/save"; // NOI18N
    public static final String HTTP_LOGIN_URL =
            "/fuji/callbacks/login"; // NOI18N
    public static final String HTTP_REGISTER_URL =
            "/fuji/callbacks/register"; // NOI18N
    public static final String HTTP_DOWNLOAD_ZIP_URL =
            "/fuji/callbacks/download-zip";
    public static final String HTTP_FEEDBACK_URL =
            "/fuji/callbacks/feedback";
    public static final String HTTP_GRAPH_EXISTS =
            "/fuji/callbacks/graph-exists";
    public static final String HTTP_USER_EXISTS =
            "/fuji/callbacks/user-exists";
    public static final String HTTP_GET_IFL_SOURCE =
            "/fuji/callbacks/get-ifl-source";
    public static final String HTTP_SET_IFL_SOURCE =
            "/fuji/callbacks/set-ifl-source";

    // Settings --------------------------------------------------------------------------
    public static final String LAST_OPENED_GRAPH_NAME =
            "last-opened-graph-name"; // NOI18N

    // Log entries -----------------------------------------------------------------------
    public static final String LOG_SRVT_EDITOR =
            "EDITOR";
    public static final String LOG_SRVT_RESOURCE =
            "RESOURCE";
    public static final String LOG_SRVT_DEPLOY =
            "DEPLOY";
    public static final String LOG_SRVT_DOWNLOAD_ZIP =
            "DOWNLOAD_ZIP";
    public static final String LOG_SRVT_FEEDBACK =
            "FEEDBACK";
    public static final String LOG_SRVT_GET_COMPONENT_TYPE =
            "GET_COMPONENT_TYPE";
    public static final String LOG_SRVT_GET_EXTERNAL_ARTIFACT =
            "GET_EXTERNAL_ARTIFACT";
    public static final String LOG_SRVT_GET_GRAPH_DESCRIPTORS =
            "GET_GRAPH_DESCRIPTORS";
    public static final String LOG_SRVT_GET_TRACING_DATA_CALLBACK =
            "GET_TRACING_DATA_CALLBACK";
    public static final String LOG_SRVT_GET_PALETTE_ITEMS =
            "GET_PALETTE_ITEMS";
    public static final String LOG_SRVT_GRAPH_EXISTS =
            "GRAPH_EXISTS";
    public static final String LOG_SRVT_LOGIN =
            "LOGIN";
    public static final String LOG_SRVT_NEW =
            "NEW";
    public static final String LOG_SRVT_OPEN =
            "OPEN";
    public static final String LOG_SRVT_REESTABLISH_SESSION =
            "REESTABLISH_SESSION";
    public static final String LOG_SRVT_REGISTER =
            "REGISTER";
    public static final String LOG_SRVT_SAVE =
            "SAVE";
    public static final String LOG_SRVT_SET_EXTERNAL_ARTIFACT =
            "SET_EXTERNAL_ARTIFACT";
    public static final String LOG_SRVT_USER_EXISTS =
            "USER_EXISTS";
    public static final String LOG_SRVT_GET_IFL_SOURCE =
            "GET_IFL_SOURCE";
    public static final String LOG_SRVT_SET_IFL_SOURCE =
            "SET_IFL_SOURCE";
}
