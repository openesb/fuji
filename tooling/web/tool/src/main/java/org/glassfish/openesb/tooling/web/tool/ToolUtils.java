/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool;

import java.io.IOException;
import org.glassfish.openesb.tooling.web.services.StorageService;
import org.glassfish.openesb.tooling.web.services.DeployerException;
import org.glassfish.openesb.tooling.web.services.DeployerService;
import org.glassfish.openesb.tooling.web.services.SerializerException;
import org.glassfish.openesb.tooling.web.services.SerializerService;
import org.glassfish.openesb.tooling.web.services.VersioningException;
import org.glassfish.openesb.tooling.web.services.VersioningService;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.GraphDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.services.BuilderException;
import org.glassfish.openesb.tooling.web.services.BuilderService;
import org.glassfish.openesb.tooling.web.services.StorageException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphDeleteException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphUndeployException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCreateException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphOpenException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphDeployException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSerializeException;
import java.io.File;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.user.UserSkinProperties;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Property;
import org.glassfish.openesb.tooling.web.model.graph.comparison.FujiGraphComparator;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCheckException;
import org.glassfish.openesb.tooling.web.tool.exceptions.InitializationException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserAccessException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserCheckException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserLoginException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserRegistrationException;
import org.glassfish.openesb.tooling.web.tool.exceptions.ZipGenerationException;
import org.glassfish.openesb.tooling.web.utils.Utils;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public class ToolUtils implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static

    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ServiceTracker versioningTracker;
    private ServiceTracker serializerTracker;
    private ServiceTracker builderTracker;
    private ServiceTracker deployerTracker;
    private ServiceTracker storageTracker;
    private Properties config;

    public ToolUtils(
            final ServiceTracker versioningTracker,
            final ServiceTracker serializerTracker,
            final ServiceTracker builderTracker,
            final ServiceTracker deployerTracker,
            final ServiceTracker storageTracker,
            final Properties config) {

        this.versioningTracker = versioningTracker;
        this.serializerTracker = serializerTracker;
        this.builderTracker = builderTracker;
        this.deployerTracker = deployerTracker;
        this.storageTracker = storageTracker;
        this.config = config;
    }

    // Web stuff -------------------------------------------------------------------------
    public void configureSession(
            final HttpServletRequest request,
            final HttpServletResponse response) throws InitializationException {

        try {
            final HttpSession session = request.getSession(true);

            String username = null;

            final Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (COOKIE_USERNAME.equals(cookie.getName())) {
                        username = cookie.getValue();
                    }
                }
            }

            final File currentWorkdir = new File(
                    (File) config.get(CONFIG_WORKDIR),
                    session.getId());
            currentWorkdir.mkdirs();

            final File currentTempdir = new File(
                    (File) config.get(CONFIG_TEMPDIR),
                    session.getId());
            currentTempdir.mkdirs();

            final User user;
            if (username == null) {
                user = getStorage().retrieveNobody();
            } else {
                user = getStorage().retrieveUser(username);
            }

            session.setMaxInactiveInterval(60 * 60); // 60 minutes
            session.setAttribute(CURRENT_WORKDIR, currentWorkdir);
            session.setAttribute(CURRENT_TEMPDIR, currentTempdir);
            session.setAttribute(CURRENT_USER, user);
        } catch (StorageException e) {
            throw new InitializationException(e);
        }
    }

    public void handleLogin(
            final HttpServletRequest request) throws UserLoginException {

        final String username = request.getParameter(PARAM_USERNAME);
        final String password = request.getParameter(PARAM_PASSWORD);

        if ((username == null) || (password == null)) {
            throw new UserLoginException(
                    "Either the username or password was not supplied.");
        }

        final HttpSession session = request.getSession();

        try {
            final User user = getStorage().retrieveUser(username, Utils.md5(password));

            if (user == null) {
                throw new UserLoginException(
                        "Either the username or password were incorrect.");
            }

            session.setAttribute(CURRENT_USER, user);
        } catch (StorageException e) {
            throw new UserLoginException(e);
        }
    }

    public void sendInvalidSessionError(
            final HttpServletResponse response) throws IOException {

        response.sendError(
                HttpServletResponse.SC_FORBIDDEN,
                "This operation must not be called without a valid session.");
    }

    public void sendInvalidUserError(
            final HttpServletResponse response) throws IOException {

        response.sendError(
                HttpServletResponse.SC_FORBIDDEN,
                "You must be logged in to invoke this operation.");
    }

    public void sendForbidden(
            final HttpServletResponse response,
            final String message) throws IOException {

        response.sendError(HttpServletResponse.SC_FORBIDDEN, message);
    }

    public void sendBadRequest(
            final HttpServletResponse response,
            final String message) throws IOException {

        response.sendError(HttpServletResponse.SC_BAD_REQUEST, message);
    }

    public void sendInternalError(
            final HttpServletResponse response,
            final String message) throws IOException {

        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
    }

    public void outputGZip(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final CharSequence string) throws IOException {

        outputGZip(request, response, string, "UTF-8");
    }

    public void outputGZip(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final CharSequence string,
            final String charset) throws IOException {

        OutputStream plainOutput = response.getOutputStream();
        GZIPOutputStream gzipOutput = null;

        final String acceptEncoding = request.getHeader("Accept-Encoding");
        if (acceptEncoding != null && acceptEncoding.contains("gzip")) {
            gzipOutput = new GZIPOutputStream(plainOutput);
            response.setHeader("Content-Encoding", "gzip");
        }

        final byte[] bytes = string.toString().getBytes(charset);

        if (gzipOutput != null) {
            gzipOutput.write(bytes);
            gzipOutput.finish();
        } else {
            response.setHeader("Content-Length", "" + bytes.length);
            plainOutput.write(bytes);
        }
        
        plainOutput.flush();
    }

    // Config ----------------------------------------------------------------------------
    public boolean getConfigBoolean(
            final String name) {

        if (config.get(name) != null) {
            return ((Boolean) config.get(name)).booleanValue();
        } else {
            return false;
        }
    }

    public String getConfigString(
            final String name) {

        return (String) config.get(name);
    }

    // Graph, nodes and links handling ---------------------------------------------------
    public Graph open(
            final String requestedCodeName,
            final HttpSession session) throws GraphOpenException {

        final User user = (User) session.getAttribute(CURRENT_USER);
        final String untitledCodeName = getUntitledName(user);

        String codeName = requestedCodeName;
        Graph graph = null;

        try {
            if (codeName == null) {
                final String lastOpenedGraphName = getStorage().getSetting(
                        LAST_OPENED_GRAPH_NAME,
                        user);

                if (lastOpenedGraphName != null) {
                    codeName = lastOpenedGraphName;
                }
            }

            if ((codeName == null) || untitledCodeName.equals(codeName)) {
                // We should not use the last opened graph thing if the user is nobody
                // and users are enabled.
                if (!getConfigBoolean(CONFIG_USERS_ENABLED) || !isNobody(user)) {
                    graph = getStorage().retrieveGraph(untitledCodeName);
                }

                if (graph == null) {
                    graph = new Graph(
                            untitledCodeName,
                            UNTITLED_DISPLAYNAME,
                            UNTITLED_DESCRIPTION,
                            UNTITLED_REVISION,
                            new Date(),
                            user);

                    try {
                        save(graph, session, false);
                    } catch (GraphSaveException e) {
                        throw new GraphOpenException(e);
                    }
                }
            } else {
                graph = getStorage().retrieveGraph(codeName);

                if (graph == null) {
                    throw new GraphOpenException("The requested graph with the code " +
                            "name '" + codeName + "' does not exist.");
                } else {
                    try {
                        getVersioning().update(
                                graph.getCodeName(),
                                (File) session.getAttribute(CURRENT_WORKDIR));

                        Graph deserialized = getSerializer().deserialize(
                                graph.getCodeName(),
                                (File) session.getAttribute(CURRENT_WORKDIR));

                        return graph;
                    } catch (VersioningException e) {
                        // If we failed to update the message flow from the VCS, this
                        // might mean that the VCS settings were changed and the given
                        // graph simply does not exist there. But it of course might
                        // indicate some real error.
                        logger.log(
                                Level.WARNING,
                                "Failed to update the versioned serialized graph.", e);
                    }
                }
            }

            setLastOpenedGraph(user, graph);

            return graph;
        } catch (StorageException e) {
            throw new GraphOpenException(e);
        } catch (SerializerException e) {
            throw new GraphOpenException(e);
        } catch (UserCheckException e) {
            throw new GraphOpenException(e);
        }
    }

    public void saveUserGraphSkin(
            final Graph graph,
            final HttpSession session,
            Map<String, String> properties) {
        try {
            final User user = (User) session.getAttribute(CURRENT_USER);
            
            getStorage().storeUserGraphSkin(user.getEntityId(), graph.getEntityId(),
                    new UserSkinProperties(0, user.getEntityId(), properties));
        } catch (StorageException ex) {
            Logger.getLogger(ToolUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save(
            final Graph graph,
            final HttpSession session) throws GraphSaveException {
        save(graph, session, true);
    }

    public void saveUser(User user) {
        try {
            getStorage().storeUser(user);
        } catch (StorageException ex) {
            Logger.getLogger(ToolUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save(
            final Graph graph,
            final HttpSession session,
            final boolean generateSources) throws GraphSaveException {

        final User user = (User) session.getAttribute(CURRENT_USER);
        final String untitledCodeName = getUntitledName(user);

        // If users are enabled, then any changes made by nobody will not go anywhere.
        try {
            if (getConfigBoolean(CONFIG_USERS_ENABLED) && isNobody(user)) {
                return;
            }
        } catch (UserCheckException e) {
            throw new GraphSaveException(e);
        }

        // For untitled graphs we also set the created by field. Just in case.
        if (graph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
            graph.setCreatedBy(user);
        }

        graph.setUpdatedOn(new Date());
        graph.setUpdatedBy(user);

        try {
            if (!untitledCodeName.equals(graph.getCodeName()) && generateSources) {
                // Generate the serialized version of the graph, run the cleaning 
                // procedure and store it in the VCS.
                getSerializer().serialize(
                        graph,
                        (File) session.getAttribute(CURRENT_WORKDIR));

                getBuilder().clean(
                        graph.getCodeName(),
                        (File) session.getAttribute(CURRENT_WORKDIR));

                final String revision = getVersioning().commit(
                        graph.getCodeName(),
                        (File) session.getAttribute(CURRENT_WORKDIR));

                // Save the resulting revision as graph's "last modified" revision.
                graph.setRevision(revision);
            }

            // Then store the graph in the database and record the last opened setting.
            getStorage().storeGraph(graph);
            setLastOpenedGraph(user, graph);
        } catch (StorageException e) {
            throw new GraphSaveException(e);
        } catch (SerializerException e) {
            throw new GraphSaveException(e);
        } catch (VersioningException e) {
            throw new GraphSaveException(e);
        } catch (BuilderException e) {
            throw new GraphSaveException(e);
        } catch (UserCheckException e) {
            throw new GraphSaveException(e);
        }
    }

    public void deploy(
            final Graph graph,
            final HttpSession session) throws GraphDeployException {

        // Check whether the deployment is enabled at all. If it is not -- throw an
        // exception.
        if (!((Boolean) config.get(CONFIG_DEPLOY_ENABLED))) {
            throw new GraphDeployException(
                    "Deployment is disabled in system settings.");
        }

        final User user = (User) session.getAttribute(CURRENT_USER);
        final String untitledCodeName = getUntitledName(user);

        try {
            save(graph, session, true);

            if (!untitledCodeName.equals(graph.getCodeName())) {
                File application = getBuilder().build(
                        graph.getCodeName(),
                        (File) session.getAttribute(CURRENT_WORKDIR));

                getDeployer().deploy(graph.getCodeName(), application);
            }
        } catch (GraphSaveException e) {
            throw new GraphDeployException(e);
        } catch (BuilderException e) {
            throw new GraphDeployException(e);
        } catch (DeployerException e) {
            throw new GraphDeployException(e);
        }
    }

    public void undeploy(
            final Graph graph,
            final HttpSession session) throws GraphUndeployException {

        // Check whether the deployment is enabled at all. If it is not -- throw an
        // exception.
        if (!((Boolean) config.get(CONFIG_DEPLOY_ENABLED))) {
            throw new GraphUndeployException(
                    "Deployment is disabled in system settings.");
        }
    }

    public Graph create(
            final String codeName,
            final String displayName,
            final String description,
            final HttpSession session) throws GraphCreateException {

        final User user = (User) session.getAttribute(CURRENT_USER);

        try {
            // Check whether the user is requesting to create a new named graph
            if (codeName == null) {
                throw new GraphCreateException("It is impossible to create a " +
                        "graph with a null code name.");
            }

            // Check whether there is already a graph with the given codename.

            if (getStorage().retrieveGraph(codeName) != null) {
                throw new GraphCreateException("Sorry, but a graph with the code " +
                        "name '" + codeName + "' already exists.");
            }

            final String effDisplayName, effDescription;
            if ((displayName == null) || "".equals(displayName)) {
                effDisplayName = UNTITLED_DISPLAYNAME;
            } else {
                effDisplayName = displayName;
            }
            if (description == null) {
                effDescription = UNTITLED_DESCRIPTION;
            } else {
                effDescription = description;
            }

            final Graph graph = new Graph(
                    codeName,
                    effDisplayName,
                    effDescription,
                    UNTITLED_REVISION,
                    new Date(),
                    user);

            save(graph, session);
            setLastOpenedGraph(user, graph);

            return graph;
        } catch (StorageException e) {
            throw new GraphCreateException(e);
        } catch (GraphSaveException e) {
            throw new GraphCreateException(e);
        } catch (UserCheckException e) {
            throw new GraphCreateException(e);
        }
    }

    public void delete(
            final Graph graph,
            final HttpSession session) throws GraphDeleteException {

        final User user = (User) session.getAttribute(CURRENT_USER);
        final String untitledCodeName = getUntitledName(user);

        try {
            // If the graph is not an untitled one, try to undeploy it and remove it from
            // VCS.
            if (!untitledCodeName.equals(graph.getCodeName())) {
                getDeployer().undeploy(graph.getCodeName());

                // Then remove it from VCS
                getVersioning().delete(
                        graph.getCodeName(),
                        (File) session.getAttribute(CURRENT_WORKDIR));
            }

            // Then remove the graph from the database.
            getStorage().deleteGraph(graph);
        } catch (DeployerException e) {
            throw new GraphDeleteException(e);
        } catch (VersioningException e) {
            throw new GraphDeleteException(e);
        } catch (StorageException e) {
            throw new GraphDeleteException(e);
        }
    }

    public void delete(
            final String codeName,
            final HttpSession session) throws GraphDeleteException {

        try {
            delete(getStorage().retrieveGraph(codeName), session);
        } catch (StorageException e) {
            throw new GraphDeleteException(e);
        }
    }

    public List<GraphDescriptor> getGraphDescriptors() throws StorageException {
        return getStorage().retrieveGraphDescriptors();
    }

    public boolean graphExists(
            final String codeName) throws GraphCheckException {

        try {
            return getStorage().graphExists(codeName);
        } catch (StorageException e) {
            throw new GraphCheckException(e);
        }
    }

    public String nodesToJson(
            final List<Node> nodes) {

        final StringBuilder b = new StringBuilder();

        b.append("[");
        for (int i = 0; i < nodes.size(); i++) {
            final Node node = nodes.get(i);

            final String shortcutToId = node.getShortcutTo() != null
                    ? "" + node.getShortcutTo().getId()
                    : "null";
            final String parentId = node.getParent() != null
                    ? "" + node.getParent().getId()
                    : "null";

            b.append("{" + "\n");
            b.append("  \"type\": \"" + node.getType() + "\"," + "\n");
            b.append("  \"data\": {" + "\n");
            b.append("    \"id\": " + node.getId() + "," + "\n");
            b.append("    \"shortcut_to_id\": " + shortcutToId + "," + "\n");
            b.append("    \"parent_id\": " + parentId + "," + "\n");
            b.append("    \"properties\": {" + "\n");
            for (Property p : node.getProperties()) {
                b.append("      \"" + p.getName() + "\": " + Utils.toJson(p) + "," + "\n");
            }
            b.append("      \"noop\": null" + "\n");
            b.append("    }," + "\n");
            b.append("    \"ui_properties\": {" + "\n");
            for (Property p : node.getUiProperties()) {
                b.append("      \"" + p.getName() + "\": " + Utils.toJson(p) + "," + "\n");
            }
            b.append("      \"noop\": null" + "\n");
            b.append("    }" + "\n");
            b.append("  }" + "\n");
            b.append("}");

            if (i < nodes.size() - 1) {
                b.append(",");
            }

            b.append("\n");
        }
        b.append("]");

        return b.toString();
    }

    public String linksToJson(
            final List<Link> links) {

        final StringBuilder b = new StringBuilder();

        b.append("[");
        for (int i = 0; i < links.size(); i++) {
            final Link link = links.get(i);

            final String startNodeId = link.getStartNode() != null
                    ? "" + link.getStartNode().getId()
                    : "null";
            final String endNodeId = link.getEndNode() != null
                    ? "" + link.getEndNode().getId()
                    : "null";

            b.append("{" + "\n");
            b.append("  \"type\": \"" + link.getType() + "\"," + "\n");
            b.append("  \"data\": {" + "\n");
            b.append("    \"id\": " + link.getId() + "," + "\n");
            b.append("    \"start_node\": " + startNodeId + "," + "\n");
            b.append("    \"start_node_connector\": " + link.getStartNodeConnector() + "," + "\n");
            b.append("    \"end_node\": " + endNodeId + "," + "\n");
            b.append("    \"end_node_connector\": " + link.getEndNodeConnector() + "," + "\n");
            b.append("    \"properties\": {" + "\n");
            for (Property p : link.getProperties()) {
                b.append("      \"" + p.getName() + "\": " + Utils.toJson(p) + "," + "\n");
            }
            b.append("      \"noop\": null" + "\n");
            b.append("    }," + "\n");
            b.append("    \"ui_properties\": {" + "\n");
            for (Property p : link.getUiProperties()) {
                b.append("      \"" + p.getName() + "\": " + Utils.toJson(p) + "," + "\n");
            }
            b.append("      \"noop\": null" + "\n");
            b.append("    }" + "\n");
            b.append("  }" + "\n");
            b.append("}");

            if (i < links.size() - 1) {
                b.append(",");
            }

            b.append("\n");
        }
        b.append("]");

        return b.toString();
    }

    public File generateZip(
            final Graph graph,
            final HttpSession session) throws ZipGenerationException {

        final File currentTempDir =
                (File) session.getAttribute(CURRENT_TEMPDIR);
        final File projectDirectory =
                new File(currentTempDir, graph.getCodeName());
        final File resultingFile =
                new File(currentTempDir, "" + ((new Date()).getTime()) + ".zip");

        try {
            // Consider it to be safe to delete.
            if (projectDirectory.exists()) {
                Utils.deleteDirectory(projectDirectory);
            }

            getSerializer().serialize(graph, currentTempDir);

            Utils.archive(projectDirectory, resultingFile);

            return resultingFile;
        } catch (IOException e) {
            throw new ZipGenerationException(e);
        } catch (SerializerException e) {
            throw new ZipGenerationException(e);
        }
    }

    // External artifacts and IFL source -------------------------------------------------
    public String getArtifact(
            final HttpSession session,
            final Node node,
            final String artifact,
            final boolean generateIfMissing) throws GraphSerializeException {

        final File currentTempdir = (File) session.getAttribute(CURRENT_TEMPDIR);

        // First we need to check whether the node already has this artifact. If it
        // does -- return. Otherwise, we need to serialize the graph, get the
        // artifact by path, put it into the node and return it.
        if (node.getArtifact(artifact) != null) {
            return node.getArtifact(artifact);
        } else if (generateIfMissing) {
            try {
                return getSerializer().getArtifact(
                        node.getGraph(),
                        currentTempdir,
                        getSerializer().getArtifactPaths(node, artifact));
            } catch (SerializerException e) {
                throw new GraphSerializeException(e);
            }
        }

        return null;
    }

    public String getArtifact(
            final HttpSession session,
            final Graph graph,
            final String artifact,
            final boolean generateIfMissing) throws GraphSerializeException {

        final File currentTempdir = (File) session.getAttribute(CURRENT_TEMPDIR);

        // First we need to check whether the graph already has this artifact. If it
        // does -- return. Otherwise, we need to serialize the graph, get the
        // artifact by path, put it into the graph and return it.
        if (graph.getArtifact(artifact) != null) {
            return graph.getArtifact(artifact);
        } else if (generateIfMissing) {
            try {
                return getSerializer().getArtifact(
                        graph,
                        currentTempdir,
                        getSerializer().getArtifactPaths(null, artifact));
            } catch (SerializerException e) {
                throw new GraphSerializeException(e);
            }
        }

        return null;
    }

    public String getIflSource(
            final HttpSession session,
            final Graph graph) throws GraphSerializeException {

        return getArtifact(session, graph, "ifl-source", true);
    }

    /**
     * This method merges the changes introduced by manually changing the IFl source of
     * a graph into the provided graph object model.
     */
    public void setIflSource(
            final HttpSession session,
            final Graph graph,
            final String iflSource) throws GraphSerializeException {

        try {
            final Graph iflGraph =
                    getSerializer().deserialize(graph.getCodeName(), iflSource);
            Utils.normalize(iflGraph);

            graph.update(graph.diffTo(iflGraph, new FujiGraphComparator()));
            Utils.normalize(graph);
            Utils.layoutGraph(graph);
        } catch (SerializerException e) {
            throw new GraphSerializeException(e);
        }
    }

    // User handling ---------------------------------------------------------------------
    public boolean isNobody(
            final User user) throws UserCheckException {

        try {
            return user.equals(getStorage().retrieveNobody());
        } catch (StorageException e) {
            throw new UserCheckException(e);
        }
    }

    public User getNobody() throws UserAccessException {
        try {
            return getStorage().retrieveNobody();
        } catch (StorageException e) {
            throw new UserAccessException(e);
        }
    }

    public boolean userExists(
            final String username) throws UserCheckException {

        try {
            return getStorage().userExists(username);
        } catch (StorageException e) {
            throw new UserCheckException(e);
        }
    }

    public User registerUser(
            final String username,
            final String password,
            final String firstName,
            final String lastname,
            final String company,
            final String email) throws UserRegistrationException {

        try {
            User user = new User(
                    username,
                    Utils.md5(password),
                    firstName,
                    lastname,
                    company,
                    email, null);

            getStorage().storeUser(user);

            return user;
        } catch (StorageException e) {
            throw new UserRegistrationException(e);
        }
    }

    // Logging ---------------------------------------------------------------------------
    public void log(
            final HttpSession session, 
            final String action) {

        log(session, action, null, null, null);
    }

    public void log(
            final HttpSession session, 
            final String action,
            final Object param1) {

        log(session, action, param1, null, null);
    }

    public void log(
            final HttpSession session,
            final String action,
            final Object param1,
            final Object param2) {

        log(session, action, param1, param2, null);
    }

    public void log(
            final HttpSession session,
            final String action,
            final Object param1,
            final Object param2,
            final Object param3) {

        if (!getConfigBoolean(CONFIG_LOGGING_ENABLED)) {
            return;
        }

        final User user = (User) session.getAttribute(CURRENT_USER);

        getStorage().log(
                session.getId(),
                user,
                action,
                (param1 != null) ? param1.toString() : null,
                (param2 != null) ? param2.toString() : null,
                (param3 != null) ? param3.toString() : null);
    }

    // Miscellanea -----------------------------------------------------------------------
    public boolean wasUpdated(
            final Graph graph,
            final HttpSession session) {

        final User user = (User) session.getAttribute(CURRENT_USER);
        final String untitledCodeName = getUntitledName(user);

        if (untitledCodeName.equals(graph.getCodeName())) {
            return false;
        }

        final String currentRevision = graph.getRevision();

        try {
            final String newRevision = getVersioning().getLastModifiedRevision(
                    graph.getCodeName());

            if (currentRevision.equals(newRevision)) {
                return false;
            } else {
                return true;
            }
        } catch (VersioningException e) {
            logger.log(
                    Level.WARNING,
                    "Failed to check for the last modified revision of the graph.", e);

            return false;
        }
    }

    public String getUntitledName(
            final User user) {

        return UNTITLED_CODENAME_PREFIX + user.getUsername();
    }

    public boolean checkWriteAccess(
            final Graph graph,
            final User user) {

        final String accessControl = getConfigString(CONFIG_ACCESS_CONTROL);

        if (ACCESS_CONTROL_NONE.equals(accessControl)) {
            return false;
        } else if (ACCESS_CONTROL_OWNER.equals(accessControl)) {
            if (graph.getCreatedBy().equals(user)) {
                return true;
            } else {
                return false;
            }
        } else if (ACCESS_CONTROL_ACL.equals(accessControl)) {
            // We do not yet support it.
            return true;
        }

        // Same as ACCESS_CONTROL_ALL. The default.
        return true;
    }

    public boolean checkReadAccess(
            final Graph graph,
            final User user) {

        if (graph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX) &&
                !graph.getCreatedBy().equals(user)) {

            return false;
        }

        return true;
    }

    // Private ---------------------------------------------------------------------------
    private void setLastOpenedGraph(
            final User user,
            final Graph graph) throws UserCheckException, StorageException {

        if (!getConfigBoolean(CONFIG_USERS_ENABLED) || !isNobody(user)) {
            getStorage().setSetting(
                    LAST_OPENED_GRAPH_NAME,
                    graph.getCodeName(),
                    user);
        }
    }

    private StorageService getStorage() {
        return (StorageService) storageTracker.getService();
    }

    private VersioningService getVersioning() {
        return (VersioningService) versioningTracker.getService();
    }

    private SerializerService getSerializer() {
        return (SerializerService) serializerTracker.getService();
    }

    private BuilderService getBuilder() {
        return (BuilderService) builderTracker.getService();
    }

    private DeployerService getDeployer() {
        return (DeployerService) deployerTracker.getService();
    }
}
