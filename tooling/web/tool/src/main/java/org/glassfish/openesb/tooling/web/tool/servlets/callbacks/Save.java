/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.update.GraphUpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkRemovalDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.LinkUpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeRemovalDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.NodeUpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.graph.update.UpdateDescriptor;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.model.user.UserSkinProperties;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserCheckException;
import org.glassfish.openesb.tooling.web.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author ksorokin
 */
public class Save extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static

    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");
    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public Save(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doPost(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final Graph currentGraph =
                (Graph) session.getAttribute(CURRENT_GRAPH);
        final User currentUser =
                (User) session.getAttribute(CURRENT_USER);

        if (currentGraph == null) {
            try {
                toolUtils.sendInternalError(response,
                        "Current message flow is not registered for this session.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final StringBuilder builder = new StringBuilder();
        final BufferedReader requestReader;

        try {
            requestReader = request.getReader();
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to open an HttpServletRequest reader.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        try {
            int length;
            final char[] chars = new char[1024];
            while ((length = requestReader.read(chars)) != -1) {
                builder.append(chars, 0, length);
            }
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to read from the HttpServletRequest reader.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Process the updates to the model ----------------------------------------------
        final List<UpdateDescriptor> updates = new LinkedList();
        boolean explicit = false;
        UserSkinProperties properties =
                new UserSkinProperties(null, currentUser.getEntityId());

        try {
            final JSONObject json = new JSONObject(new JSONTokener(builder.toString()));

            explicit = json.getBoolean(PARAM_EXPLICIT);

            // If users are enabled and the current user is not logged in, we do not allow
            // to explicit saves, as they make no sense. We also forbid saves of anything
            // except 'untitled_nobody'.
            try {
                if (toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED)
                        && toolUtils.isNobody(currentUser)) {

                    if (explicit || !currentGraph.getCodeName().equals(
                            toolUtils.getUntitledName(currentUser))) {

                        try {
                            toolUtils.sendInvalidUserError(response);
                        } catch (IOException e) {
                            logger.log(
                                    Level.WARNING,
                                    "Failed to send an error message.", e);
                        }

                        return;
                    }
                }
            } catch (UserCheckException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to check whether the current user is a 'nobody'.", e);

                try {
                    toolUtils.sendInternalError(response, e.getMessage());
                } catch (IOException ex) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", ex);
                }

                return;
            }

            if (explicit) {
                String newCodeName = currentGraph.getCodeName();
                String newDisplayName = currentGraph.getDisplayName();
                String newDescription = currentGraph.getDescription();

                if (json.has(PARAM_CODE_NAME) && currentGraph.getCodeName().
                        startsWith(UNTITLED_CODENAME_PREFIX)) {

                    String codeName = json.getString(PARAM_CODE_NAME);

                    codeName = codeName.trim();

                    if ("".equals(codeName)) {
                        try {
                            toolUtils.sendBadRequest(response,
                                    "Code name cannot be empty.");
                        } catch (IOException e) {
                            logger.log(
                                    Level.WARNING,
                                    "Failed to send an error message.", e);
                        }

                        return;
                    }

                    if (!codeName.matches("^[a-zA-Z0-9-_]+$")) {
                        try {
                            toolUtils.sendBadRequest(response,
                                    "The parameter 'code_name' can contain only latin "
                                    + "characters, digits, dashes and underscores.");
                        } catch (IOException e) {
                            logger.log(
                                    Level.WARNING,
                                    "Failed to send an error message.", e);
                        }

                        return;
                    }

                    newCodeName = codeName;
                }

                if (json.has(PARAM_DISPLAY_NAME)) {
                    final String displayName = json.getString(PARAM_DISPLAY_NAME);

                    if ("".equals(displayName)) {
                        try {
                            toolUtils.sendBadRequest(response,
                                    "Display name cannot be empty.");
                        } catch (IOException e) {
                            logger.log(
                                    Level.WARNING,
                                    "Failed to send an error message.", e);
                        }

                        return;
                    }

                    newDisplayName = displayName;
                }

                if (json.has(PARAM_DESCRIPTION)) {
                    newDescription = json.getString(PARAM_DESCRIPTION);
                }

                updates.add(new GraphUpdateDescriptor(
                        newCodeName, newDisplayName, newDescription));
            }

            if (json.has("updated_nodes")) {
                final JSONArray jsonArray = json.getJSONArray("updated_nodes");

                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject node = jsonArray.getJSONObject(i);

                    final int id = node.getInt("id");
                    final String type = node.getString("type");

                    final Object shortcutToObject = node.get("shortcut_to_id");
                    final Integer shortcutToId = shortcutToObject.equals(JSONObject.NULL) ? null : node.getInt("shortcut_to_id");

                    final Object parentObject = node.get("parent_id");
                    final Integer parentId = parentObject.equals(JSONObject.NULL) ? null : node.getInt("parent_id");

                    final Properties updatedProperties;
                    if (node.has("updated_properties")) {
                        updatedProperties = Utils.toProperties(
                                node.getJSONObject("updated_properties"));
                    } else {
                        updatedProperties = new Properties();
                    }

                    final List<String> removedProperties = new LinkedList<String>();
                    if (node.has("removed_properties")) {
                        final JSONArray jsonPropertiesArray =
                                node.getJSONArray("removed_properties");

                        for (int j = 0; j < jsonPropertiesArray.length(); j++) {
                            removedProperties.add(jsonPropertiesArray.getString(j));
                        }
                    }

                    final Properties updatedUiProperties;
                    if (node.has("updated_ui_properties")) {
                        updatedUiProperties = Utils.toProperties(
                                node.getJSONObject("updated_ui_properties"));
                    } else {
                        updatedUiProperties = new Properties();
                    }

                    final List<String> removedUiProperties = new LinkedList<String>();
                    if (node.has("removed_ui_properties")) {
                        final JSONArray jsonPropertiesArray =
                                node.getJSONArray("removed_ui_properties");

                        for (int j = 0; j < jsonPropertiesArray.length(); j++) {
                            removedUiProperties.add(jsonPropertiesArray.getString(j));
                        }
                    }

                    updates.add(new NodeUpdateDescriptor(
                            id,
                            type,
                            shortcutToId != null ? currentGraph.getNodeById(shortcutToId) : null,
                            parentId != null ? currentGraph.getNodeById(parentId) : null,
                            Utils.toPropertySet(updatedProperties),
                            removedProperties,
                            Utils.toPropertySet(updatedUiProperties),
                            removedUiProperties));
                }
            }

            if (json.has("removed_nodes")) {
                final JSONArray jsonArray = json.getJSONArray("removed_nodes");

                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject node = jsonArray.getJSONObject(i);

                    final int id = node.getInt("id");

                    updates.add(new NodeRemovalDescriptor(currentGraph.getNodeById(id)));
                }
            }

            if (json.has("updated_links")) {
                final JSONArray jsonArray = json.getJSONArray("updated_links");

                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject link = jsonArray.getJSONObject(i);

                    final int id = link.getInt("id");
                    final String type = link.getString("type");

                    final Node startNode;
                    if ((link.get("start_node") != null)
                            && !link.get("start_node").equals(JSONObject.NULL)) {

                        startNode = currentGraph.getNodeById(link.getInt("start_node"));
                    } else {
                        startNode = null;
                    }

                    final int startNodeConnector = link.getInt("start_node_connector");

                    final Node endNode;
                    if ((link.get("end_node") != null)
                            && !link.get("end_node").equals(JSONObject.NULL)) {

                        endNode = currentGraph.getNodeById(link.getInt("end_node"));
                    } else {
                        endNode = null;
                    }

                    final int endNodeConnector = link.getInt("end_node_connector");

                    final Properties updatedProperties;
                    if (link.has("updated_properties")) {
                        updatedProperties = Utils.toProperties(
                                link.getJSONObject("updated_properties"));
                    } else {
                        updatedProperties = new Properties();
                    }

                    final List<String> removedProperties = new LinkedList<String>();
                    if (link.has("removed_properties")) {
                        final JSONArray jsonPropertiesArray =
                                link.getJSONArray("removed_properties");

                        for (int j = 0; j < jsonPropertiesArray.length(); j++) {
                            removedProperties.add(jsonPropertiesArray.getString(j));
                        }
                    }

                    final Properties updatedUiProperties;
                    if (link.has("updated_ui_properties")) {
                        updatedUiProperties = Utils.toProperties(
                                link.getJSONObject("updated_ui_properties"));
                    } else {
                        updatedUiProperties = new Properties();
                    }

                    final List<String> removedUiProperties = new LinkedList<String>();
                    if (link.has("removed_ui_properties")) {
                        final JSONArray jsonPropertiesArray =
                                link.getJSONArray("removed_ui_properties");

                        for (int j = 0; j < jsonPropertiesArray.length(); j++) {
                            removedUiProperties.add(jsonPropertiesArray.getString(j));
                        }
                    }

                    updates.add(new LinkUpdateDescriptor(
                            id,
                            type,
                            startNode,
                            startNodeConnector,
                            endNode,
                            endNodeConnector,
                            Utils.toPropertySet(updatedProperties),
                            removedProperties,
                            Utils.toPropertySet(updatedUiProperties),
                            removedUiProperties));
                }
            }

            if (json.has("removed_links")) {
                final JSONArray jsonArray = json.getJSONArray("removed_links");

                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject link = jsonArray.getJSONObject(i);

                    final int id = link.getInt("id");

                    updates.add(new LinkRemovalDescriptor(currentGraph.getLinkById(id)));
                }
            }

            // user properties
            if (toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED)) {
                for (String propertyName : UserSkinProperties.USER_SKIN_PROPERTIES) {
                    if (json.has(propertyName)) {
                        String propertyValue = json.getString(propertyName);
                        if (propertyValue.length() > 0) 
                            properties.putProperty(propertyName, propertyValue);
                        
                    }
                }
            }
        } catch (JSONException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to parse the JSON input to save procedure.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Check whether the current user does have any rights to save the graph.
        if (!toolUtils.checkWriteAccess(currentGraph, currentUser)) {
            try {
                toolUtils.sendBadRequest(
                        response,
                        "You do not have enough permissions to save this message flow.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        currentGraph.update(updates);

        try {
            toolUtils.save(currentGraph, session, explicit);
            if (!properties.isEmpty()) {
                currentUser.setProperties(properties);
                toolUtils.saveUser(currentUser);
            }
        } catch (GraphSaveException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to save the graph.", e);

            try {
                toolUtils.sendInternalError(response,
                        "The graph save procedure failed. <br/>"
                        + "The engine reported: \"" + e.getMessage() + "\".");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        toolUtils.log(session, LOG_SRVT_SAVE,
                currentGraph.getEntityId(), explicit);

        // Send the status report --------------------------------------------------------
        try {
            response.setContentType("application/json; charset=utf-8");

            final PrintWriter out = response.getWriter();
            try {
                out.println("{");
                out.println("  \"revision\": " + currentGraph.getRevision() + " , ");
                out.println("  \"noop\": null");
                out.println("}");
            } finally {
                out.close();
            }
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
