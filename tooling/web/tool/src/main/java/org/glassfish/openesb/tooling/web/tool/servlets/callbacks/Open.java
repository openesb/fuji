/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCheckException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphOpenException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class Open extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public Open(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        // Check whether the http session is valid.
        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        // Read and validate the request parameters.
        final String codeName = request.getParameter(PARAM_CODE_NAME);

        if (codeName == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + PARAM_CODE_NAME + "' was " +
                        "not found.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        if ("".equals(codeName)) {
            try {
                toolUtils.sendBadRequest(response,
                        "The parameter '" + PARAM_CODE_NAME + "' cannot be empty.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final User currentUser = (User) session.getAttribute(CURRENT_USER);

        // If there is already a graph in the session, save it, if we have enough
        // privileges.
        final Graph currentGraph = (Graph) session.getAttribute(CURRENT_GRAPH);
        if (currentGraph != null) {
            if (toolUtils.checkWriteAccess(currentGraph, currentUser)) {
                try {
                    toolUtils.save(currentGraph, session, true);
                } catch (GraphSaveException e) {
                    logger.log(
                            Level.SEVERE,
                            "Failed to save the current graph.", e);

                    try {
                        toolUtils.sendInternalError(response, e.getMessage());
                    } catch (IOException ex) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", ex);
                    }

                    return;
                }
            }

            if (currentGraph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
                session.setAttribute(CURRENT_DRAFT_GRAPH, currentGraph);
            }
        }

        // Open the requested graph and put it into the session. If the user want to
        // open the draft (of which he pnly has access to his own), we first check
        // whether the draft is already in the session.
        final Graph openedGraph;
        if (codeName.startsWith(UNTITLED_CODENAME_PREFIX) &&
                (session.getAttribute(CURRENT_DRAFT_GRAPH) != null)) {

            openedGraph = (Graph) session.getAttribute(CURRENT_DRAFT_GRAPH);
        } else {
            try {
                if (!toolUtils.graphExists(codeName)) {
                    try {
                        toolUtils.sendBadRequest(response,
                                "A message flow with the given code " +
                                "name ('" + codeName + "') does not exist.");
                    } catch (IOException e) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", e);
                    }
                    return;
                }
            } catch (GraphCheckException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to save the current graph.", e);

                try {
                    toolUtils.sendInternalError(response, e.getMessage());
                } catch (IOException ex) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", ex);
                }

                return;
            }

            try {
                openedGraph = toolUtils.open(codeName, session);
                
                if (!toolUtils.checkReadAccess(openedGraph, currentUser)) {
                    try {
                        toolUtils.sendBadRequest(
                                response,
                                "You do not have enough permissions to " +
                                "open this graph.");
                    } catch (IOException ex) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", ex);
                    }

                    return;
                }
            } catch (GraphOpenException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to open a graph.", e);

                try {
                    toolUtils.sendInternalError(response, e.getMessage());
                } catch (IOException ex) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", ex);
                }

                return;
            }
        }

        session.setAttribute(CURRENT_GRAPH, openedGraph);

        toolUtils.log(session, LOG_SRVT_OPEN,
                openedGraph.getEntityId());

        // Output the effective code name, display name and description.
        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("{" + "\n");
        b.append("  \"code_name\": " +
                Utils.toJson(codeName) + "," + "\n");
        b.append("  \"display_name\": " +
                Utils.toJson(openedGraph.getDisplayName()) + "," + "\n");
        b.append("  \"description\": " +
                Utils.toJson(openedGraph.getDescription()) + "," + "\n");
        b.append("  \"created_by\": " +
                Utils.toJson(openedGraph.getCreatedBy().getUsername()) + ",\n");
        b.append("  \"created_on\": " +
                Utils.toJson(openedGraph.getCreatedOn().getTime()) + ",\n");
        b.append("  \"updated_by\": " +
                Utils.toJson(openedGraph.getUpdatedBy().getUsername()) + ",\n");
        b.append("  \"updated_on\": " +
                Utils.toJson(openedGraph.getUpdatedOn().getTime()) + ",\n");
        b.append("  \"nodes\": " +
                toolUtils.nodesToJson(openedGraph.getNodes()) + "," + "\n");
        b.append("  \"links\": " +
                toolUtils.linksToJson(openedGraph.getLinks()) + "" + "\n");
        b.append("}" + "\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
