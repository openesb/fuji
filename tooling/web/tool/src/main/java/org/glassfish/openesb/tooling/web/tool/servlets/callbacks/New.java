/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCheckException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCreateException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserCheckException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class New extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public New(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        // Check whether the http session is valid.
        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        // Check whether the user is logged in, or users are disabled. If users are
        // enabled, but the current user is not logged in, we will not allow creating
        // any new graphs.
        final User currentUser = (User) session.getAttribute(CURRENT_USER);
        try {
            if (toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED) &&
                    toolUtils.isNobody(currentUser)) {

                try {
                    toolUtils.sendInvalidUserError(response);
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", e);
                }

                return;
            }
        } catch (UserCheckException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to check whether the user is a 'nobody'.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Read and validate the request parameters.
        String codeName = request.getParameter("code_name");
        String displayName = request.getParameter("display_name");
        String description = request.getParameter("description");

        if (codeName == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter 'code_name' was not found.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        codeName = codeName.trim();

        if ("".equals(codeName)) {
            try {
                toolUtils.sendBadRequest(response,
                        "The parameter 'code_name' cannot be empty.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        if (!codeName.matches("^[a-zA-Z0-9-_]+$")) {
            try {
                toolUtils.sendBadRequest(response,
                        "The parameter 'code_name' can contain only latin " +
                        "characters, digits, dashes and underscores.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        try {
            if (toolUtils.graphExists(codeName)) {
                try {
                    toolUtils.sendBadRequest(response,
                            "A message flow with the given code name ('" + codeName + "') " +
                            "already exists.");
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", e);
                }

                return;
            }
        } catch (GraphCheckException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to check whether the graph exists.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        if ((displayName == null) || ("".equals(displayName))) {
            displayName = codeName;
        }

        if (description == null) {
            description = "";
        }

        // If there is already a graph in the session, save it, if we have enough
        // privileges.
        final Graph currentGraph = (Graph) session.getAttribute(CURRENT_GRAPH);
        if (currentGraph != null) {
            if (toolUtils.checkWriteAccess(currentGraph, currentUser)) {
                try {
                    toolUtils.save(currentGraph, session, true);
                } catch (GraphSaveException e) {
                    logger.log(
                            Level.SEVERE,
                            "Failed to save the current graph.", e);

                    try {
                        toolUtils.sendInternalError(response, e.getMessage());
                    } catch (IOException ex) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", ex);
                    }

                    return;
                }
            }

            if (currentGraph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
                session.setAttribute(CURRENT_DRAFT_GRAPH, currentGraph);
            }
        }

        // Create the graph and store it in the session.
        final Graph newGraph;
        
        try {
            newGraph = toolUtils.create(codeName, displayName, description, session);
        } catch (GraphCreateException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to create a new graph.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        session.setAttribute(CURRENT_GRAPH, newGraph);

        toolUtils.log(session, LOG_SRVT_NEW,
                newGraph.getEntityId());

        // Output the effective code name, display name and description.
        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("{\n");
        b.append("  \"code_name\": " + Utils.toJson(newGraph.getCodeName()) + ",\n");
        b.append("  \"display_name\": " + Utils.toJson(newGraph.getDisplayName()) + ",\n");
        b.append("  \"description\": " + Utils.toJson(newGraph.getDescription()) + ",\n");
        b.append("  \"created_by\": " + Utils.toJson(newGraph.getCreatedBy().getUsername()) + ",\n");
        b.append("  \"created_on\": " + Utils.toJson(newGraph.getCreatedOn().getTime()) + ",\n");
        b.append("  \"updated_by\": " + Utils.toJson(newGraph.getUpdatedBy().getUsername()) + ",\n");
        b.append("  \"updated_on\": " + Utils.toJson(newGraph.getUpdatedOn().getTime()) + "\n");
        b.append("}");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
