/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphOpenException;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.exceptions.InitializationException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserLoginException;

/**
 *
 * @author ksorokin
 */
public class ReestablishSession extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public ReestablishSession(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        if (request.getSession(false) != null) {
            try {
                toolUtils.sendBadRequest(response,
                        "This URL must NOT be called within a valid session.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final HttpSession session = request.getSession(true);

        // Set the required session attributes. See also Editor.process() method.
        try {
            toolUtils.configureSession(request, response);
        } catch (InitializationException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to initialize the session.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // If users are enabled and the client supplied a username (which means he was
        // logged in), try to re-login.
        if (toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED) &&
                (request.getParameter(PARAM_USERNAME) != null)) {

            try {
                toolUtils.handleLogin(request);
            } catch (UserLoginException e) {
                logger.log(
                        Level.WARNING,
                        "Unsuccessful login attempt.", e);

                try {
                    toolUtils.sendBadRequest(response, e.getMessage());
                } catch (IOException ex) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", ex);
                }

                return;
            }
        }

        final User currentUser = (User) session.getAttribute(CURRENT_USER);
        final Graph currentGraph;

        // Now load the requested graph (or the default one). If it was not supplied or
        // is null, load the "untitled" graph.
        final String codeName = request.getParameter(PARAM_GRAPH);

        try {
            currentGraph = toolUtils.open(codeName, session);

            if (!toolUtils.checkReadAccess(currentGraph, currentUser)) {
                try {
                    toolUtils.sendBadRequest(
                            response,
                            "You do not have enough permissions to open this graph.");
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", e);
                }

                return;
            }

            session.setAttribute(CURRENT_GRAPH, currentGraph);
        } catch (GraphOpenException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to open a graph.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        toolUtils.log(session, LOG_SRVT_REESTABLISH_SESSION,
                currentUser.getEntityId(), currentGraph.getEntityId());
    }
}
