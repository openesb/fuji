/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.utils.Utils;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public class Resource extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;
    private ServiceTracker registryTracker;

    public Resource(
            final ToolUtils toolUtils,
            final ServiceTracker registryTracker) {

        this.toolUtils = toolUtils;
        this.registryTracker = registryTracker;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final List<String> namesList =
                new ArrayList(Arrays.asList(request.getQueryString().split(",")));

        for (int i = 0; i < namesList.size(); i++) {
            final String name = namesList.get(i).trim();

            if ("".equals(name)) {
                namesList.remove(i);
                i--;
            } else {
                namesList.set(i, name);
            }
        }

        final String[] names = namesList.toArray(new String[namesList.size()]);
        final InputStream[] inputs = new InputStream[names.length];

        int length = 0;
        String mimeType = "";

        for (int i = 0; i < names.length; i++) {
            // First we check whether we can handle the request ourselves, without
            // having to ask any other providers.
            URL url = getClass().getClassLoader().getResource("web/" + names[i]);
            if (url == null) {
                final RegistryService registry =
                        (RegistryService) registryTracker.getService();

                if (registry != null) {
                    url = registry.resolveToUrl(names[i]);

                    // In some cases this could be not a URL, but an input stream. We
                    // should try that as well.
                    if (url == null) {
                        final InputStream input =
                                registry.resolveToInputStream(names[i]);

                        if (input != null) {
                            final ByteArrayInputStream bais;

                            if (input instanceof ByteArrayInputStream) {
                                bais = (ByteArrayInputStream) input;
                            } else {
                                try {
                                    // To correctly report the length we need to transfer
                                    // the stream to a BAIS.
                                    final ByteArrayOutputStream baos =
                                            new ByteArrayOutputStream();
                                    Utils.transferStream(input, baos);
                                    bais = new ByteArrayInputStream(baos.toByteArray());
                                } catch (IOException e) {
                                    logger.log(
                                            Level.SEVERE,
                                            "Failed to prepare the output stream.", e);
                                    return;
                                }
                            }

                            inputs[i] = bais;
                            mimeType = getMimeType(names[i]);
                            length += bais.available();
                        }
                    }
                }
            }

            if (url != null) {
                URLConnection connection = null;

                try {
                    connection = url.openConnection();
                    length += connection.getContentLength();
                    mimeType = getMimeType(names[i]);

                    inputs[i] = connection.getInputStream();
                } catch (IOException e) {
                    logger.log(
                            Level.SEVERE,
                            "Failed to open an input stream to an " +
                                    "URL on the classpath.", e);

                    try {
                        toolUtils.sendInternalError(response, e.getMessage());
                    } catch (IOException ex) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", ex);
                    }

                    return;
                }
            }
        }

        // It is unclear whether we really want to log this.
        //toolUtils.log(request.getSession(), LOG_SRVT_RESOURCE,
        //        names.length);

        response.setContentType(mimeType);

        OutputStream output = null;
        try {
            output = response.getOutputStream();
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to get the HttpServletResponse output stream.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Check whether the client supports gzip. If it does, we will compress.
        final String acceptEncoding = request.getHeader("Accept-Encoding");
        if ((acceptEncoding != null) && acceptEncoding.contains("gzip") &&
                mimeType.startsWith("text") && (length > 512)) {
            try {
                output = new GZIPOutputStream(output);
                response.setHeader("Content-Encoding", "gzip");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to initialize a GZIPOutputStream.", e);
            }
        }

        if (!(output instanceof GZIPOutputStream)) {
            response.setContentLength(length);
        }

        for (int i = 0; i < inputs.length; i++) {
            if (inputs[i] != null) {
                InputStream input = inputs[i];
                
                try {
                    byte[] buffer = new byte[4096];
                    int read = 0;
                    while ((read = input.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }
                } catch (IOException e) {
                    logger.log(
                            Level.SEVERE,
                            "Failed to send the output.", e);
                    return;
                } finally {
                    try {
                        input.close();
                    } catch (IOException e) {
                        logger.log(
                                Level.WARNING,
                                "Failed to close the input stream.", e);
                    }
                }
            }
        }

        try {
            if (output instanceof GZIPOutputStream) {
                ((GZIPOutputStream) output).finish();
            } else {
                output.flush();
            }
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to flush (send everything) the response output stream.", e);
            
            return;
        }
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private String getMimeType(
            final String name) {

        if (name.endsWith(".png")) {
            return "image/png";
        }

        if (name.endsWith(".js")) {
            return "text/javascript";
        }

        if (name.endsWith(".css")) {
            return "text/css";
        }

        return "text/html";
    }
}
