/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCheckException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphDeleteException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserLoginException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class Login extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public Login(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        try {
            toolUtils.handleLogin(request);
        } catch (UserLoginException e) {
            logger.log(
                    Level.WARNING,
                    "Unsuccessful login attempt.", e);

            try {
                toolUtils.sendBadRequest(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Now we need to handle the current graph. Since the user was not previously
        // logged in, it is likely that he was editing an 'untitled' graph. This said,
        // we need to transform the 'untitled' by 'nobody' into 'untitled' by current
        // user. If there was already such a graph, we need to delete it.
        final Graph currentGraph = (Graph) session.getAttribute(CURRENT_GRAPH);
        final User currentUser = (User) session.getAttribute(CURRENT_USER);

        try {
            if (currentGraph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
                final String correctCodeName = toolUtils.getUntitledName(currentUser);

                currentGraph.setCodeName(correctCodeName);

                if (toolUtils.graphExists(correctCodeName)) {
                    toolUtils.delete(correctCodeName, session);
                }

                toolUtils.save(currentGraph, session);
            }
        } catch (GraphCheckException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to check whether a graph exists.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        } catch (GraphDeleteException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to delete the existing draft graph.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        } catch (GraphSaveException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to save the current draft graph.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Also correct the current draft, if there is one.
        if (session.getAttribute(CURRENT_DRAFT_GRAPH) != null) {
            final Graph draft = (Graph) session.getAttribute(CURRENT_DRAFT_GRAPH);
            draft.setCodeName(toolUtils.getUntitledName(currentUser));
            draft.setCreatedBy(currentUser);
            draft.setUpdatedBy(currentUser);
        }

        toolUtils.log(session, LOG_SRVT_LOGIN,
                currentUser.getEntityId(), currentGraph.getEntityId());

        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("{\n");
        b.append("  \"current_user\": {\n");
        b.append("    \"username\": " +
                Utils.toJson(currentUser.getUsername()) + ",\n");
        b.append("    \"first_name\": " +
                Utils.toJson(currentUser.getFirstName()) + ",\n");
        b.append("    \"last_name\": " +
                Utils.toJson(currentUser.getLastName()) + ",\n");
        b.append("    \"company\": " +
                Utils.toJson(currentUser.getCompany()) + ",\n");
        b.append("    \"email\": " +
                Utils.toJson(currentUser.getEmail()) + "\n");
        b.append("  },\n");
        b.append("  \"current_graph\": {\n");
        b.append("    \"code_name\": " +
                Utils.toJson(currentGraph.getCodeName()) + ",\n");
        b.append("    \"display_name\": " +
                Utils.toJson(currentGraph.getDisplayName()) + ",\n");
        b.append("    \"description\": " +
                Utils.toJson(currentGraph.getDescription()) + ",\n");
        b.append("    \"created_by\": " +
                Utils.toJson(currentGraph.getCreatedBy().getUsername()) + ",\n");
        b.append("    \"created_on\": " +
                Utils.toJson(currentGraph.getCreatedOn().getTime()) + ",\n");
        b.append("    \"updated_by\": " +
                Utils.toJson(currentGraph.getUpdatedBy().getUsername()) + ",\n");
        b.append("    \"updated_on\": " +
                Utils.toJson(currentGraph.getUpdatedOn().getTime()) + "\n");
        b.append("  }\n");
        b.append("}\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
