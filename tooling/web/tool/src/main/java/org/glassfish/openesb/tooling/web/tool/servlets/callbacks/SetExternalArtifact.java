/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.user.User;

/**
 *
 * @author ksorokin
 */
public class SetExternalArtifact extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public SetExternalArtifact(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doPost(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final String nodeIdString = request.getParameter(NODE_ID_PARAMETER);
        final Integer nodeId;

        if (nodeIdString == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + NODE_ID_PARAMETER +
                        "' was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        try {
            nodeId = new Integer(nodeIdString);
        } catch (NumberFormatException e) {
            logger.log(
                    Level.WARNING, 
                    "Failed to parse node id: '" + nodeIdString + "'.", e);

            try {
                toolUtils.sendBadRequest(response,
                        "Could not parse the supplied node identifier '" +
                                nodeIdString + "'.");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        final String artifact = request.getParameter(ARTIFACT_PARAMETER);

        if (artifact == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + ARTIFACT_PARAMETER +
                        "' was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }


        final String artifactCode = request.getParameter(ARTIFACT_CODE_PARAMETER);

        if (artifactCode == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + ARTIFACT_CODE_PARAMETER +
                        "' was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final Graph currentGraph =
                (Graph) session.getAttribute(CURRENT_GRAPH);
        final User currentUser =
                (User) session.getAttribute(CURRENT_USER);

        if (currentGraph == null) {
            try {
                toolUtils.sendInternalError(response,
                        "Current message flow is not registered for " +
                        "this session.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        toolUtils.log(session, LOG_SRVT_SET_EXTERNAL_ARTIFACT,
                currentGraph.getEntityId(), nodeId, artifact);

        if (nodeId == -1) {
            currentGraph.setArtifact(artifact, artifactCode);
        } else {
            final Node node = currentGraph.getNodeById(nodeId);

            if (node == null) {
                try {
                    toolUtils.sendInternalError(response,
                            "Current message flow does not contain a node with " +
                            "the requested id: '" + nodeId + "'.");
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", e);
                }

                return;
            }

            node.setArtifact(artifact, artifactCode);
            node.updateArtifacts();
        }

        try {
            // Check whether the current user does have any rights to save the graph.
            if (!toolUtils.checkWriteAccess(currentGraph, currentUser)) {
                try {
                    toolUtils.sendBadRequest(
                            response,
                            "You do not have enough permissions to save " +
                                    "this message flow.");
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", e);
                    return;
                }
            }

            toolUtils.save(currentGraph, session, false);
        } catch (GraphSaveException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to save the graph.", e);

            try {
                toolUtils.sendInternalError(
                        response,
                        "An error occured while saving the updated message flow. " +
                        "Please try again. The error message was: " +
                        "\"" + e.getMessage() + "\".");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        try {
            response.setContentType("application/json; charset=utf-8");
            
            final PrintWriter out = response.getWriter();
            try {
                out.println("{");
                out.println("  \"noop\": null");
                out.println("}");
            } finally {
                out.close();
            }
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String NODE_ID_PARAMETER = "node-id"; // NOI18N
    private static final String ARTIFACT_PARAMETER = "artifact"; // NOI18N
    private static final String ARTIFACT_CODE_PARAMETER = "artifact-code"; // NOI18N
}
