/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.osgi;

import com.sun.jbi.messaging.MessageService;
import org.glassfish.openesb.tooling.web.services.BuilderService;
import org.glassfish.openesb.tooling.web.services.StorageService;
import org.glassfish.openesb.tooling.web.services.DeployerService;
import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.services.SerializerService;
import org.glassfish.openesb.tooling.web.services.VersioningService;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.tool.servlets.Editor;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.Deploy;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.Save;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GetComponentType;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GetExternalArtifact;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GetGraphDescriptors;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GetPaletteItems;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.ReestablishSession;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.SetExternalArtifact;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.glassfish.openesb.tooling.web.tool.servlets.Resource;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.DownloadZip;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.Feedback;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GetIflSource;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GetTracingDataCallback;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.GraphExists;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.Login;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.New;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.Open;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.Register;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.SetIflSource;
import org.glassfish.openesb.tooling.web.tool.servlets.callbacks.UserExists;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public class Activator implements BundleActivator, Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ServiceTracker versioningTracker;
    private ServiceTracker serializerTracker;
    private ServiceTracker builderTracker;
    private ServiceTracker deployerTracker;
    private ServiceTracker storageTracker;
    private ServiceTracker registryTracker;
    private ServiceTracker messageTracker;
    private ServiceTracker httpTracker;
    private File workdir;
    private File tempdir;

    public void start(
            final BundleContext context) throws Exception {

        // Get hold of the required services ---------------------------------------------
        versioningTracker = new ServiceTracker(
                context,
                VersioningService.class.getName(),
                null);

        serializerTracker = new ServiceTracker(
                context,
                SerializerService.class.getName(),
                null);

        builderTracker = new ServiceTracker(
                context,
                BuilderService.class.getName(),
                null);

        deployerTracker = new ServiceTracker(
                context,
                DeployerService.class.getName(),
                null);

        storageTracker = new ServiceTracker(
                context,
                StorageService.class.getName(),
                null);

        registryTracker = new ServiceTracker(
                context,
                RegistryService.class.getName(),
                null);

        messageTracker = new ServiceTracker(
                context,
                MessageService.class.getName(),
                null);

        httpTracker = new ServiceTracker(
                context,
                HttpService.class.getName(),
                null);

        versioningTracker.open();
        serializerTracker.open();
        builderTracker.open();
        deployerTracker.open();
        storageTracker.open();
        registryTracker.open();
        messageTracker.open();
        httpTracker.open();

        // Initialize the working directory ----------------------------------------------
        String workdirPath = System.getProperty(CONFIG_WORKDIR);
        if (workdirPath == null) {
            final File workdirInData = context.getDataFile(DEFAULT_WORKDIR_NAME);

            if (workdirInData == null) {
                workdirPath = DEFAULT_WORKDIR;
            } else {
                workdirPath = workdirInData.getCanonicalPath();
            }
        }

        workdir = new File(workdirPath);
        workdir.mkdirs();

        // Initialize the temp directory -------------------------------------------------
        String tempdirPath = System.getProperty(CONFIG_TEMPDIR);
        if (tempdirPath == null) {
            final File tempdirInData = context.getDataFile(DEFAULT_TEMPDIR_NAME);

            if (tempdirInData == null) {
                tempdirPath = DEFAULT_TEMPDIR;
            } else {
                tempdirPath = tempdirInData.getCanonicalPath();
            }
        }

        tempdir = new File(tempdirPath);
        tempdir.mkdirs();

        // Initialize boolean configuration stuff ----------------------------------------
        boolean usersEnabled;
        if (System.getProperty(CONFIG_USERS_ENABLED) != null) {
            usersEnabled = Boolean.getBoolean(CONFIG_USERS_ENABLED);
        } else {
            usersEnabled = DEFAULT_USERS_ENABLED;
        }

        boolean registrationEnabled;
        if (System.getProperty(CONFIG_REGISTRATION_ENABLED) != null) {
            registrationEnabled = Boolean.getBoolean(CONFIG_REGISTRATION_ENABLED);
        } else {
            registrationEnabled = DEFAULT_REGISTRATION_ENABLED;
        }

        boolean deployEnabled;
        if (System.getProperty(CONFIG_DEPLOY_ENABLED) != null) {
            deployEnabled = Boolean.getBoolean(CONFIG_DEPLOY_ENABLED);
        } else {
            deployEnabled = DEFAULT_DEPLOY_ENABLED;
        }

        boolean feedbackEnabled;
        if (System.getProperty(CONFIG_FEEDBACK_ENABLED) != null) {
            feedbackEnabled = Boolean.getBoolean(CONFIG_FEEDBACK_ENABLED);
        } else {
            feedbackEnabled = DEFAULT_FEEDBACK_ENABLED;
        }

        String accessControl;
        if (System.getProperty(CONFIG_ACCESS_CONTROL) != null) {
            accessControl = System.getProperty(CONFIG_ACCESS_CONTROL);
        } else {
            accessControl = DEFAULT_ACCESS_CONTROL;
        }

        String htmlAppendix;
        if (System.getProperty(CONFIG_HTML_APPENDIX) != null) {
            htmlAppendix = System.getProperty(CONFIG_HTML_APPENDIX);
        } else {
            htmlAppendix = DEFAULT_HTML_APPENDIX;
        }

        boolean loggingEnabled;
        if (System.getProperty(CONFIG_LOGGING_ENABLED) != null) {
            loggingEnabled = Boolean.getBoolean(CONFIG_LOGGING_ENABLED);
        } else {
            loggingEnabled = DEFAULT_LOGGING_ENABLED;
        }

        // Funny hack to work around felix behavior, which tries to sustitute properties
        // placeholders, fails and throws a runtime exception... Oh.
        htmlAppendix = htmlAppendix.replace("&#123;", "{").replace("&#125;", "}");

        // Initialize the properties to be passed around ---------------------------------
        final Properties config = new Properties();

        config.put(CONFIG_WORKDIR, workdir);
        config.put(CONFIG_TEMPDIR, tempdir);
        config.put(CONFIG_USERS_ENABLED, usersEnabled);
        config.put(CONFIG_REGISTRATION_ENABLED, registrationEnabled);
        config.put(CONFIG_DEPLOY_ENABLED, deployEnabled);
        config.put(CONFIG_FEEDBACK_ENABLED, feedbackEnabled);
        config.put(CONFIG_LOGGING_ENABLED, loggingEnabled);
        config.put(CONFIG_ACCESS_CONTROL, accessControl);
        config.put(CONFIG_HTML_APPENDIX, htmlAppendix);

        // Obtain the services and init the Utils class ----------------------------------
        final ToolUtils toolUtils = new ToolUtils(
                versioningTracker,
                serializerTracker,
                builderTracker,
                deployerTracker,
                storageTracker,
                config);

        // Init the HTTP Service ---------------------------------------------------------
        final HttpService httpService = (HttpService) httpTracker.waitForService(60000);
        final HttpContext httpContext = new MyHttpContext();

        httpService.registerServlet(
                HTTP_RESOURCE_URL,
                new Resource(toolUtils, registryTracker),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_EDITOR_URL,
                new Editor(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GET_COMPONENT_TYPE_URL,
                new GetComponentType(toolUtils, registryTracker),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GET_PALETTE_ITEMS_URL,
                new GetPaletteItems(toolUtils, registryTracker),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GET_GRAPH_DESCRIPTORS_URL,
                new GetGraphDescriptors(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_EXPLICIT_DEPLOY_URL,
                new Deploy(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_REESTABLISH_SESSION_URL,
                new ReestablishSession(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GET_EXTERNAL_ARTIFACT_URL,
                new GetExternalArtifact(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_SET_EXTERNAL_ARTIFACT_URL,
                new SetExternalArtifact(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GET_TRACING_DATA_URL,
                new GetTracingDataCallback(toolUtils,
                (MessageService) messageTracker.getService()),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_NEW_URL,
                new New(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_OPEN_URL,
                new Open(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_SAVE_URL,
                new Save(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_LOGIN_URL,
                new Login(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_REGISTER_URL,
                new Register(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_FEEDBACK_URL,
                new Feedback(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_DOWNLOAD_ZIP_URL,
                new DownloadZip(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GRAPH_EXISTS,
                new GraphExists(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_USER_EXISTS,
                new UserExists(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_GET_IFL_SOURCE,
                new GetIflSource(toolUtils),
                new Properties(),
                httpContext);
        httpService.registerServlet(
                HTTP_SET_IFL_SOURCE,
                new SetIflSource(toolUtils),
                new Properties(),
                httpContext);
    }

    public void stop(
            final BundleContext context) throws Exception {

        final HttpService httpService = (HttpService) httpTracker.getService();

        httpTracker.close();
        storageTracker.close();
        deployerTracker.close();
        builderTracker.close();
        serializerTracker.close();
        versioningTracker.close();
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Inner classes
    public class MyHttpContext implements HttpContext {

        //////////////////////////////////////////////////////////////////////////////////////
        // Instance
        public boolean handleSecurity(
                HttpServletRequest request,
                HttpServletResponse response) throws IOException {

            return true;
        }

        public URL getResource(
                final String name) {

            return null;
        }

        public String getMimeType(
                final String name) {

            return null;
        }
    }
}
