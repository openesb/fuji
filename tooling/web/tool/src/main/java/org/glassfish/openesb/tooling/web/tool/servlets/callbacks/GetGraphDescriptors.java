/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.GraphDescriptor;
import org.glassfish.openesb.tooling.web.services.StorageException;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserCheckException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author vbychkov
 * @author ksorokin
 */
public class GetGraphDescriptors extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public GetGraphDescriptors(final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        List<GraphDescriptor> descriptors = null;
        try {
            descriptors = toolUtils.getGraphDescriptors();
        } catch (StorageException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to load the list of graph descriptors.", e);

            try {
                toolUtils.sendInternalError(response,
                        "Cannot retrieve code names - storage exception.");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        final Graph currentGraph = (Graph) session.getAttribute(CURRENT_GRAPH);
        final User currentUser = (User) session.getAttribute(CURRENT_USER);

        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        
        b.append("{" + "\n");
        b.append("  descriptors: [" + "\n");

        if (descriptors == null) {
            descriptors = new ArrayList<GraphDescriptor>();
        }

        // It is not clear whether we really want to log this.
        //toolUtils.log(session, LOG_SRVT_GET_GRAPH_DESCRIPTORS,
        //        descriptors.size());

        // If there is a current draft in the session -- append it. Otherwise -- check
        // whether there is an untitled graph for the current user (unless users are
        // enabled and the current one is 'nobody')
        if (!currentGraph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
            if (session.getAttribute(CURRENT_DRAFT_GRAPH) != null) {
                appendDescriptor(
                        (GraphDescriptor) session.getAttribute(CURRENT_DRAFT_GRAPH),
                        b);
            } else {
                try {
                    if (!toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED) ||
                            !toolUtils.isNobody(currentUser)) {

                        for (GraphDescriptor descriptor: descriptors) {
                            if (descriptor.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX) &&
                                    descriptor.getCreatedBy().equals(currentUser)) {

                                appendDescriptor(descriptor, b);
                                break;
                            }
                        }
                    }
                } catch (UserCheckException e) {
                    logger.log(
                            Level.SEVERE,
                            "Failed to check whether the user is a 'nobody'.", e);

                    try {
                        toolUtils.sendInternalError(response, e.getMessage());
                    } catch (IOException ex) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", ex);
                    }

                    return;
                }
            }
        }

        Collections.sort(descriptors, new Comparator<GraphDescriptor>() {
            public int compare(GraphDescriptor d1, GraphDescriptor d2) {
                return d1.getDisplayName().compareTo(d2.getDisplayName());
            }
        });
        
        for (int i = 0; i < descriptors.size(); i++) {
            GraphDescriptor descriptor = descriptors.get(i);
            if (descriptor == null) {
                continue;
            }

            // We also need to hide the 'draft' (or 'untitled') graphs.
            if (descriptor.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
                continue;
            }

            // Skip the currently open graph as well. Since the classes of objects
            // differ we compare by entity ids.
            if (descriptor.getEntityId().equals(currentGraph.getEntityId())) {
                continue;
            }

            appendDescriptor(descriptor, b);
        }
        b.append("    null" + "\n");
        b.append("  ]" + "\n");
        b.append("}" + "\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private void appendDescriptor(
            final GraphDescriptor descriptor,
            final StringBuilder b) {

        b.append("    {" + "\n");
        b.append("      code_name: " + Utils.toJson(descriptor.getCodeName()) + "," + "\n");
        if (descriptor.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX)) {
            b.append("      display_name: " + Utils.toJson("<i>Current draft</i>") + "," + "\n");
        } else {
            b.append("      display_name: " + Utils.toJson(descriptor.getDisplayName()) + "," + "\n");
        }
        b.append("      description: " + Utils.toJson(descriptor.getDescription()) + "," + "\n");
        b.append("      created_on: " + Utils.toJson(descriptor.getCreatedOn().getTime()) + "," + "\n");
        b.append("      created_by: " + Utils.toJson(descriptor.getCreatedBy().getUsername()) + "," + "\n");
        b.append("      updated_on: " + Utils.toJson(descriptor.getUpdatedOn().getTime()) + "," + "\n");
        b.append("      updated_by: " + Utils.toJson(descriptor.getUpdatedBy().getUsername()) + "" + "\n");
        b.append("    }," + "\n");
    }
}
