/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphDeployException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ksorokin
 */
public class Deploy extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public Deploy(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {
        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        // Process the updates to the model ----------------------------------------------
        final Graph currentGraph =
                (Graph) session.getAttribute(CURRENT_GRAPH);

        if (currentGraph == null) {
            try {
                toolUtils.sendInternalError(response,
                        "Current message flow is not registered for " +
                        "this session.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        toolUtils.log(session, LOG_SRVT_DEPLOY,
                currentGraph.getEntityId());

        // Deploy the graph --------------------------------------------------------------
        try {
            toolUtils.deploy(currentGraph, session);
        } catch (GraphDeployException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to deploy the graph.", e);

            try {
                toolUtils.sendInternalError(response,
                        "The graph save procedure failed. <br/>" +
                        "The engine reported: \"" + e.getMessage() + "\".");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Send the status report --------------------------------------------------------
        try {
            response.setContentType("application/json; charset=utf-8");

            final PrintWriter out = response.getWriter();
            try {
                out.println("{");
                out.println("  \"revision\": " + currentGraph.getRevision() + " , ");
                out.println("  \"noop\": null");
                out.println("}");
            } finally {
                out.close();
            }
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
