/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.validation;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.validation.Validator;
import org.glassfish.openesb.tooling.web.model.graph.validation.ValidationResult;


/**
 *
 * @author ksorokin
 */
public class FujiValidator implements Validator {
    
    public ValidationResult validate(Graph graph) {
        return null;
    }
    
    public ValidationResult validate(Node node) {
        return null;
    }
    
    public ValidationResult validate(Link link) {
//        if (link.getStartNode() == null) {
//            return new Validator.Result(
//                    Validator.ResultType.ERROR, 
//                    "A link must have a start node.");
//        }
//        
//        if (link.getEndNode() == null) {
//            return new Validator.Result(
//                    Validator.ResultType.ERROR, 
//                    "A link must have an end node.");
//        }
        
        return null;
    }
    
}
