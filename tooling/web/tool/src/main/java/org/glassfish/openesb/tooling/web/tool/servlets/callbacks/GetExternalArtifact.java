/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSerializeException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class GetExternalArtifact extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public GetExternalArtifact(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final String nodeIdString = request.getParameter(PARAM_NODE_ID);
        final Integer nodeId;

        if (nodeIdString == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + PARAM_NODE_ID + "' " +
                        "was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        try {
            nodeId = new Integer(nodeIdString);
        } catch (NumberFormatException e) {
            logger.log(
                    Level.WARNING,
                    "Failed to parse the supplied number: '" + nodeIdString + "'.", e);

            try {
                toolUtils.sendBadRequest(response,
                        "Could not parse the supplied node " +
                        "identifier '" + nodeIdString + "'.");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        final String artifact = request.getParameter(PARAM_ARTIFACT);

        if (artifact == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + PARAM_ARTIFACT +
                        "' was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final boolean generateIfMissing =
                Boolean.parseBoolean(request.getParameter("generate"));

        final Graph currentGraph =
                (Graph) session.getAttribute(CURRENT_GRAPH);

        if (currentGraph == null) {
            try {
                toolUtils.sendInternalError(response,
                        "Current message flow is not registered for " +
                        "this session.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        toolUtils.log(session, LOG_SRVT_GET_EXTERNAL_ARTIFACT,
                currentGraph.getEntityId(), nodeId, artifact);

        // When nodeId is -1, this a special case and we're looking for a graph-wide
        // artifact, such as the IFL source code.
        String artifactCode = null;
        try {
            if (nodeId != -1) {
                final Node node = currentGraph.getNodeById(nodeId);

                if (node == null) {
                    try {
                        toolUtils.sendInternalError(response,
                                "Current message flow does not contain a node with " +
                                "the requested id: '" + nodeId + "'.");
                    } catch (IOException e) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", e);
                    }

                    return;
                }

                artifactCode = toolUtils.getArtifact(
                        session, node, artifact, generateIfMissing);
            } else {
                artifactCode = toolUtils.getArtifact(
                        session, currentGraph, artifact, generateIfMissing);
            }
        } catch (GraphSerializeException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to serialize the graph.", e);

            try {
                toolUtils.sendInternalError(response,
                        "An error occured while generating the default value for the " +
                        "artifact. Please try again. The error message " +
                        "was: \"" + e.getMessage() + "\".");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        if (generateIfMissing && (artifactCode == null)) {
            artifactCode = "";
        }

        response.setContentType("application/json; charset=utf-8");

        try {
            toolUtils.outputGZip(
                    request,
                    response,
                    "{ code: " + Utils.toJson(artifactCode) + " }");
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String PARAM_NODE_ID = "node-id"; // NOI18N
    private static final String PARAM_ARTIFACT = "artifact"; // NOI18N
}
