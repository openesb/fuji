/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets;

import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphOpenException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.model.user.UserSkinProperties;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCheckException;
import org.glassfish.openesb.tooling.web.tool.exceptions.InitializationException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class Editor extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public Editor(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession();

        if (session.isNew()) {
            try {
                toolUtils.configureSession(request, response);
            } catch (InitializationException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to initialize the session.", e);

                try {
                    toolUtils.sendInternalError(response, e.getMessage());
                } catch (IOException ex) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", ex);
                }

                return;
            }
        }

        User currentUser = (User) session.getAttribute(CURRENT_USER);
        Graph currentGraph = (Graph) session.getAttribute(CURRENT_GRAPH);

        try {
            if (currentGraph == null) {
                currentGraph = toolUtils.open(null, session);
            } else {
                if (toolUtils.wasUpdated(currentGraph, session)) {
                    currentGraph = toolUtils.open(
                            currentGraph.getCodeName(), session);
                }
            }
        } catch (GraphOpenException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to open the default graph, or re-open the current one.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Check whether a user has explicitly requested a certain graph. If he did,
        // check whether the graph actually exists.
        try {
            final String queryString = request.getQueryString();
            if ((queryString != null) && !"".equals(queryString)) {
                try {
                    if (!toolUtils.graphExists(queryString)) {
                        try {
                            toolUtils.sendBadRequest(response,
                                    "The requested message flow '" + queryString +
                                    "' does not exist.");
                        } catch (IOException ex) {
                            logger.log(
                                    Level.WARNING,
                                    "Failed to send an error message.", ex);
                        }

                        return;
                    }
                } catch (GraphCheckException e) {
                    logger.log(
                            Level.SEVERE,
                            "Failed to check whether the request graph exists.", e);

                    try {
                        toolUtils.sendInternalError(response, e.getMessage());
                    } catch (IOException ex) {
                        logger.log(
                                Level.WARNING,
                                "Failed to send an error message.", ex);
                    }

                    return;
                }

                final Graph graph = toolUtils.open(queryString, session);

                if ((graph != null) && toolUtils.checkReadAccess(graph, currentUser)) {
                    currentGraph = graph;
                }
            }
        } catch (GraphOpenException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to open the request graph.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }


        UserSkinProperties properties = currentUser.getProperties();

        toolUtils.log(session, LOG_SRVT_EDITOR,
                currentGraph.getEntityId());

        // Put currentGraph to the session -----------------------------------------------
        session.setAttribute(CURRENT_GRAPH, currentGraph);

        // Output HTML -------------------------------------------------------------------
        response.setContentType("text/html; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("\n");
        b.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \n");
        b.append("        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
        b.append("\n");
        b.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
        b.append("  \n");
        b.append("  <head>\n");
        b.append("    <title>Fuji editor</title>\n");
        b.append("    \n");
        b.append("    <meta http-equiv=\"Content-Type\" ");
        b.append("          content=\"text/html; charset=utf-8\"/>\n");
        b.append("    \n");
        b.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"resource?" +
                "css/ui/editor.css," +
                "css/ui/palette/palette.css," +
                "css/ui/diagram/diagram.css," +
                "css/ui/toolbar/toolbar.css" + "\"/>\n");
        b.append("    \n");
        b.append("    <script type=\"text/javascript\" src=\"resource?" +
                "js/lib/prototype/1.6.0.3/prototype.js," +
                "js/lib/scriptaculous/1.8.2/scriptaculous.js," +
                "js/lib/scriptaculous/1.8.2/effects.js," +
                "js/lib/excanvas/r50/excanvas.js," +
                "js/lib/canvastext/canvastext.js," +
                "js/lib/web-ui/1.0/ws-utils.js," +
                "js/lib/web-ui/1.0/ws-drag-manager.js," +
                "js/lib/web-ui/1.0/ws-selection-manager.js," +
                "js/lib/web-ui/1.0/ws-load-manager.js," +
                "js/lib/web-ui/1.0/ws-mouse-manager.js," +
                "js/lib/web-ui/1.0/ws-rounded-rectangle.js," +
                "js/lib/web-ui/1.0/ws-floating-window.js," +
                "js/lib/web-ui/1.0/ws-progress-bar.js," +
                "js/lib/web-ui/1.0/ws-properties-sheet.js," +
                "js/lib/web-ui/1.0/ws-toolbar.js," +
                "js/lib/web-ui/1.0/ws-growl.js," +
                "js/lib/web-ui/1.0/ws-graph.js," +
                "js/lib/web-ui/1.0/ws-graph-default-node.js," +
                "js/lib/web-ui/1.0/ws-graph-default-link.js," +
                "js/lib/web-ui/1.0/ws-bayan.js," +
                "js/lib/web-ui/1.0/ws-palette.js," +
                "js/lib/web-ui/1.0/ws-modal-window.js," +
                "js/lib/web-ui/1.0/ws-windowed-properties-sheet.js," +
                "js/lib/web-ui/1.0/ws-modal-prompt.js," +
                "js/lib/web-ui/1.0/ws-tabbed-pane.js," +
                "js/lib/web-ui/1.0/ws-messages-viewer.js," +
                "js/lib/web-ui/1.0/ws-tree-table.js," +
                "js/lib/web-ui/1.0/ws-tooltip.js," +
                "js/ui/editor.js," +
                "js/ui/palette/palette.js," +
                "js/ui/diagram/diagram.js," +
                "js/ui/toolbar/toolbar.js," +
                "js/ui/tracing.js," +
                "js/ui/debugger.js," +
                "js/ui/tester.js," +
                "js/ui/users.js," +
                "js/lib/edit_area/edit_area_loader.js," +
                "js/lib/edit_area/reg_syntax/ifl.js," +
                "js/lib/edit_area/reg_syntax/xml.js," +
                "js/lib/edit_area/langs/en.js," + "\"></script>\n");

// Do not remove -- this get uncommented when there is need to debug javascript.
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/prototype/1.6.0.3/prototype.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/scriptaculous/1.8.2/scriptaculous.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/scriptaculous/1.8.2/effects.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/excanvas/r50/excanvas.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/canvastext/canvastext.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-utils.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-drag-manager.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-selection-manager.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-load-manager.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-mouse-manager.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-rounded-rectangle.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-floating-window.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-progress-bar.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-properties-sheet.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-toolbar.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-growl.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-graph.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-graph-default-node.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-graph-default-link.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-bayan.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-palette.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-modal-window.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-windowed-properties-sheet.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-modal-prompt.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-tabbed-pane.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-messages-viewer.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-tree-table.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/web-ui/1.0/ws-tooltip.js" + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/editor.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/palette/palette.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/diagram/diagram.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/toolbar/toolbar.js" + "\"></script>\n");
//
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/tracing.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/debugger.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/tester.js," + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/ui/users.js" + "\"></script>\n");
//
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/edit_area/edit_area_loader.js" + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/edit_area/reg_syntax/ifl.js" + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/edit_area/reg_syntax/xml.js" + "\"></script>\n");
//        b.append("    <script type=\"text/javascript\" src=\"resource?" + "js/lib/edit_area/langs/en.js" + "\"></script>\n");

        b.append("  </head>\n");
        b.append("  \n");
        b.append("  <body>\n");
        b.append("  \n");
        b.append("  <div id=\"palette\" class=\"palette\">\n");
        b.append("    <div class=\"palette-header -wsfw-header\">\n");
        b.append("      <div class=\"contents\">Palette</div>\n");
        b.append("    </div>\n");
        b.append("    <div class=\"palette-footer -wsfw-footer\">\n");
        b.append("      <div class=\"contents\"><img src=\"resource?img/ui/palette/search-icon.png\" class=\"search-icon\"/><input type=\"text\" class=\"search-field\"/></div>\n");
        b.append("    </div>\n");
        b.append("      <div class=\"-wsfw-contents\">\n");
        b.append("      </div>\n");
        b.append("  </div>\n");
        b.append("    \n");
        b.append("  <div id=\"diagram\"></div>\n");
        b.append("    \n");
        b.append("  <div id=\"toolbar\">\n");
        b.append("    <div id=\"buttons\">\n");

        b.append("      <a id=\"toolbarNew\" class=\"button\" href=\"javascript:toolbar_handle_new();\">" +
                "<img src=\"resource?img/ui/toolbar/new.png\" alt=\"New Message Flow\" title=\"New message flow\"/></a>\n");
        b.append("      <a id=\"toolbarOpen\" class=\"button\" href=\"javascript:toolbar_handle_open();\">" +
                "<img src=\"resource?img/ui/toolbar/open.png\" alt=\"Open Message Flow\" title=\"Open message flow\"/></a>\n");
        b.append("      <a id=\"toolbarSave\" class=\"button\" href=\"javascript:toolbar_handle_save();\">" +
                "<img src=\"resource?img/ui/toolbar/save.png\" alt=\"Save Message Flow\" title=\"Save message flow\"/></a>\n");
        if (toolUtils.getConfigBoolean(CONFIG_DEPLOY_ENABLED)) {
            b.append("      <a id=\"toolbarDeploy\" class=\"button\" href=\"javascript:toolbar_handle_deploy();\">" +
                    "<img src=\"resource?img/ui/toolbar/deploy.png\" alt=\"Deploy Message Flow\" title=\"Deploy message flow\"/></a>\n");
        }
        b.append("      <a id=\"toolbarProperties\" class=\"button\" href=\"javascript:toolbar_handle_properties();\">" +
                "<img src=\"resource?img/ui/toolbar/properties.png\" alt=\"Message Flow Properties\" title=\"Edit message flow properties\"/></a>\n");

        if (toolUtils.getConfigBoolean(CONFIG_DEPLOY_ENABLED)) {
            b.append("      \n");
            b.append("      <img src=\"resource?img/ui/toolbar/separator.png\" alt=\"Separator\" title=\"Separator\"/>\n");
            b.append("      \n");

            if (currentGraph.isTracingEnabled()) {
                b.append("      <a id=\"toolbarTracing\" class=\"button\" href=\"javascript:toolbar_handle_tracing();\">" +
                    "<img id=\"tracingToolbar\" src=\"resource?img/ui/tracing/stop_tests_24.png\" alt=\"Start Tracing\" title=\"Start message tracing\"/></a>\n");
            } else {
                b.append("      <a id=\"toolbarTracing\" class=\"button\" href=\"javascript:toolbar_handle_tracing();\">" +
                    "<img id=\"tracingToolbar\" src=\"resource?img/ui/tracing/run_tests_24.png\" alt=\"Start Tracing\" title=\"Start message tracing\"/></a>\n");
            }
        }

        if (toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED)) {
            b.append("      \n");
            b.append("      <img src=\"resource?img/ui/toolbar/separator.png\" alt=\"Separator\" title=\"Separator\"/>\n");
            b.append("      \n");

            b.append("      <a id=\"toolbarLogin\" class=\"button\" href=\"javascript:toolbar_handle_login();\">" +
                    "<img src=\"resource?img/ui/toolbar/login.png\" alt=\"Login\" title=\"Login\"/></a>\n");

            if (toolUtils.getConfigBoolean(CONFIG_REGISTRATION_ENABLED)) {
                b.append("      <a id=\"toolbarRegister\" class=\"button\" href=\"javascript:toolbar_handle_register();\">" +
                        "<img src=\"resource?img/ui/toolbar/register.png\" alt=\"Register\" title=\"Register\"/></a>\n");
            }
        }

        b.append("      \n");
        b.append("      <img src=\"resource?img/ui/toolbar/separator.png\" alt=\"Separator\" title=\"Separator\"/>\n");
        b.append("      \n");

        b.append("      <a id=\"toolbarEditSource\" class=\"button\" href=\"javascript:toolbar_handle_edit_source();\">" +
                "<img src=\"resource?img/ui/toolbar/edit_source.png\" alt=\"Edit IFL Source\" title=\"Edit IFL source of the current message flow\"/></a>\n");
        b.append("      <a id=\"toolbarDownloadZip\" class=\"button\" href=\"javascript:toolbar_handle_download_zip();\">" +
                "<img src=\"resource?img/ui/toolbar/download_zip.png\" alt=\"Download ZIP\" title=\"Download project sources as a ZIP archive\"/></a>\n");
        b.append("      \n");

        if (toolUtils.getConfigBoolean(CONFIG_FEEDBACK_ENABLED)) {
            b.append("      \n");
            b.append("      <img src=\"resource?img/ui/toolbar/separator.png\" alt=\"Separator\" title=\"Separator\"/>\n");
            b.append("      \n");

            b.append("      <a id=\"toolbarFeedback\" class=\"button\" href=\"javascript:toolbar_handle_feedback();\">" +
                    "<img src=\"resource?img/ui/toolbar/feedback.png\" alt=\"Send Feedback\" title=\"Send feedback\"/></a>\n");
        }
        b.append("    </div>\n");

        b.append("    <div id=\"status\">\n");
        b.append("      <div id=\"statusString\">Hello!</div>\n");
        b.append("      <div id=\"statusProgress\"></div>\n");
        b.append("    </div>\n");
        b.append("  </div>\n");
        b.append("  \n");
        b.append("  <div id=\"logger\"></div>\n");
        b.append("  \n");
        b.append("  <script type=\"text/javascript\">\n");
        b.append("      // Preload images.\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/new.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/new_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/open.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/open_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/save.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/save_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/deploy.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/deploy_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/properties.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/properties_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/run_tests.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/run_tests_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/login.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/login_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/register.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/register_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/edit_source.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/edit_source_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/download_zip.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/download_zip_d.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/feedback.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/toolbar/feedback_d.png\")\n");
        b.append("      \n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/diagram/node-badges/badge-shortcut.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/diagram/node-toolbar/tbar-delete.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/diagram/node-toolbar/tbar-properties.png\")\n");
        b.append("      Ws.ImagesLoadManager.load(\"resource?img/ui/diagram/node-toolbar/tbar-shortcut.png\")\n");
        b.append("      \n");
        b.append("      // Output current user data.\n");
        b.append("      var current_user = {\n");
        b.append("        \"username\": " + Utils.toJson(currentUser.getUsername()) + ",\n");
        b.append("        \"first_name\": " + Utils.toJson(currentUser.getFirstName()) + ",\n");
        b.append("        \"last_name\": " + Utils.toJson(currentUser.getLastName()) + ",\n");
        b.append("        \"company\": " + Utils.toJson(currentUser.getCompany()) + ",\n");
        b.append("        \"email\": " + Utils.toJson(currentUser.getEmail()) + "\n");
        b.append("      };\n");
        b.append("      \n");
        b.append("      // Output current graph data.\n");
        b.append("      var current_graph = {\n");
        b.append("        \"code_name\": " + Utils.toJson(currentGraph.getCodeName()) + ",\n");
        b.append("        \"display_name\": " + Utils.toJson(currentGraph.getDisplayName()) + ",\n");
        b.append("        \"description\": " + Utils.toJson(currentGraph.getDescription()) + ",\n");
        b.append("        \"created_by\": " + Utils.toJson(currentGraph.getCreatedBy().getUsername()) + ",\n");
        b.append("        \"created_on\": " + Utils.toJson(currentGraph.getCreatedOn().getTime()) + ",\n");
        b.append("        \"updated_by\": " + Utils.toJson(currentGraph.getUpdatedBy().getUsername()) + ",\n");
        b.append("        \"updated_on\": " + Utils.toJson(currentGraph.getUpdatedOn().getTime()) + ",\n");
        b.append("        \"revision\": " + Utils.toJson(currentGraph.getRevision()) + ",\n");
        b.append("        \"loaded_with_errors\": false" + "\n");
        b.append("      };\n");
        b.append("      var current_graph_nodes = " + toolUtils.nodesToJson(currentGraph.getNodes()) + ";");
        b.append("      var current_graph_links = " + toolUtils.linksToJson(currentGraph.getLinks()) + ";");

        if (properties != null) {
            b.append("      var current_user_skin = {\n");
            for (String name : properties.getPropertiesName()) {
                b.append(Utils.toJson(name) + " : " + Utils.toJson(properties.getPropertyValue(name)));
            }
            b.append("      };\n");
        }
        b.append("      \n");
        b.append("      // Output current config.\n");
        b.append("      var config = {\n");
        b.append("        \"users_enabled\": " + Utils.toJson(toolUtils.getConfigBoolean(CONFIG_USERS_ENABLED)) + ",\n");
        b.append("        \"registration_enabled\": " + Utils.toJson(toolUtils.getConfigBoolean(CONFIG_REGISTRATION_ENABLED)) + ",\n");
        b.append("        \"deployment_enabled\": " + Utils.toJson(toolUtils.getConfigBoolean(CONFIG_DEPLOY_ENABLED)) + ",\n");
        b.append("        \"feedback_enabled\": " + Utils.toJson(toolUtils.getConfigBoolean(CONFIG_FEEDBACK_ENABLED)) + ",\n");
        b.append("      };\n");
        b.append("  </script>\n");

        b.append(toolUtils.getConfigString(CONFIG_HTML_APPENDIX));

        b.append("  </body>\n");
        b.append("  \n");
        b.append("</html>\n");


        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
