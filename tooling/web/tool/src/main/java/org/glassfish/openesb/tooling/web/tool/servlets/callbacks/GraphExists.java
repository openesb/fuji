/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphCheckException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class GraphExists extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public GraphExists(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final String codename = request.getParameter(PARAM_CODE_NAME);

        if (codename == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + PARAM_CODE_NAME + "' " +
                        "was not supplied.");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        if ("".equals(codename)) {
            try {
                toolUtils.sendBadRequest(response,
                        "The '" + PARAM_CODE_NAME + "' parameter cannot be empty.");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }


        boolean exists = false;

        try {
            exists = toolUtils.graphExists(codename);
        } catch (GraphCheckException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to check whether a graph with the given code name " +
                    "'" + codename + "' exists.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // It is unclear whether we really want to log this.
        //toolUtils.log(session, LOG_SRVT_GRAPH_EXISTS,
        //        codename, exists);

        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("{\n");
        b.append("  \"exists\": " + Utils.toJson(exists) + "\n");
        b.append("}\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}
