/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSaveException;
import org.glassfish.openesb.tooling.web.tool.exceptions.GraphSerializeException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class SetIflSource extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public SetIflSource(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doPost(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final String iflSource = request.getParameter("code");

        if (iflSource == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + "code" +
                        "' was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final Graph currentGraph =
                (Graph) session.getAttribute(CURRENT_GRAPH);
        if (currentGraph == null) {
            try {
                toolUtils.sendInternalError(response,
                        "Current message flow is not registered for " +
                        "this session.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        // Check whether the current user does have any rights to save the graph.
        final User currentUser =
                (User) session.getAttribute(CURRENT_USER);
        if (!toolUtils.checkWriteAccess(currentGraph, currentUser)) {
            try {
                toolUtils.sendBadRequest(
                        response,
                        "You do not have enough permissions to save " +
                                "this message flow.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
                return;
            }
        }

        try {
            toolUtils.setIflSource(session, currentGraph, iflSource);
            toolUtils.save(currentGraph, session, true);
        } catch (GraphSerializeException e) {
            try {
                toolUtils.sendInternalError(
                        response,
                        "An error occurred while parsing and applying the IFL " +
                        "source changes. Please edit the code and try again. The " +
                        "error message was: \"" + e.getMessage() + "\".");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        } catch (GraphSaveException e) {
            try {
                toolUtils.sendInternalError(
                        response,
                        "An error occurred while trying to save the updated graph. " +
                        "Please try again. The error message " +
                        "was: \"" + e.getMessage() + "\".");
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }

        // Output the effective code name, display name and description.
        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("{" + "\n");
        b.append("  \"code_name\": " +
                Utils.toJson(currentGraph.getCodeName()) + "," + "\n");
        b.append("  \"display_name\": " +
                Utils.toJson(currentGraph.getDisplayName()) + "," + "\n");
        b.append("  \"description\": " +
                Utils.toJson(currentGraph.getDescription()) + "," + "\n");
        b.append("  \"created_by\": " +
                Utils.toJson(currentGraph.getCreatedBy().getUsername()) + ",\n");
        b.append("  \"created_on\": " +
                Utils.toJson(currentGraph.getCreatedOn().getTime()) + ",\n");
        b.append("  \"updated_by\": " +
                Utils.toJson(currentGraph.getUpdatedBy().getUsername()) + ",\n");
        b.append("  \"updated_on\": " +
                Utils.toJson(currentGraph.getUpdatedOn().getTime()) + ",\n");
        b.append("  \"nodes\": " +
                toolUtils.nodesToJson(currentGraph.getNodes()) + "," + "\n");
        b.append("  \"links\": " +
                toolUtils.linksToJson(currentGraph.getLinks()) + "" + "\n");
        b.append("}" + "\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }
}