/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.utils.Utils;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public class GetComponentType extends HttpServlet implements Constants {

    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    private static List<String> specialTypes = new LinkedList<String>();
    
    private static Map<String, String> specialTypesIcons =
            new HashMap<String, String>();
    private static Map<String, String> specialTypesShadows =
            new HashMap<String, String>();
    private static Map<String, String> specialTypesScripts =
            new HashMap<String, String>();
    
    static {
        specialTypes.add("aggregate");
        specialTypesIcons.put(
                "aggregate", "img/component-types/aggregate/aggregate-icon.png");
        specialTypesShadows.put(
                "aggregate", "img/component-types/aggregate/aggregate-shadow.png");
        specialTypesScripts.put(
                "aggregate", "js/component-types/aggregate/aggregate.js");

        specialTypes.add("broadcast");
        specialTypesIcons.put(
                "broadcast", "img/component-types/broadcast/broadcast-icon.png");
        specialTypesShadows.put(
                "broadcast", "img/component-types/broadcast/broadcast-shadow.png");
        specialTypesScripts.put(
                "broadcast", "js/component-types/broadcast/broadcast.js");
        
        specialTypes.add("filter");
        specialTypesIcons.put(
                "filter", "img/component-types/filter/filter-icon.png");
        specialTypesShadows.put(
                "filter", "img/component-types/filter/filter-shadow.png");
        specialTypesScripts.put(
                "filter", "js/component-types/filter/filter.js");

        specialTypes.add("route");
        specialTypesIcons.put(
                "route", "img/component-types/route/route-icon.png");
        specialTypesShadows.put(
                "route", "img/component-types/route/route-shadow.png");
        specialTypesScripts.put(
                "route", "js/component-types/route/route.js");

        specialTypes.add("select");
        specialTypesIcons.put(
                "select", "img/component-types/select/select-icon.png");
        specialTypesShadows.put(
                "select", "img/component-types/select/select-shadow.png");
        specialTypesScripts.put(
                "select", "js/component-types/select/select.js");

        specialTypes.add("split");
        specialTypesIcons.put(
                "split", "img/component-types/split/split-icon.png");
        specialTypesShadows.put(
                "split", "img/component-types/split/split-shadow.png");
        specialTypesScripts.put(
                "split", "js/component-types/split/split.js");
        
        specialTypes.add("tee");
        specialTypesIcons.put(
                "tee", "img/component-types/tee/tee-icon.png");
        specialTypesShadows.put(
                "tee", "img/component-types/tee/tee-shadow.png");
        specialTypesScripts.put(
                "tee", "js/component-types/tee/tee.js");
    }
    
    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;
    private ServiceTracker registryTracker;

    public GetComponentType(
            final ToolUtils toolUtils,
            final ServiceTracker registryTracker) {

        this.toolUtils = toolUtils;
        this.registryTracker = registryTracker;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final String type = request.getParameter(CODE_NAME_PARAMETER);
        if (type == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + CODE_NAME_PARAMETER + "' " +
                        "was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        
        if (!specialTypes.contains(type) &&
                !getRegistry().getAvailableTypes().contains(type)) {

            try {
                toolUtils.sendBadRequest(response,
                        "Component data for the supplied type -- '" + type + "' " +
                        "-- was not found.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        // It is not clear whether we really want to log this.
        //toolUtils.log(session, LOG_SRVT_GET_COMPONENT_TYPE,
        //        type);

        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        final List<String> icons = specialTypes.contains(type) ?
            Arrays.asList(new String[] {specialTypesIcons.get(type)}) :
            getRegistry().getIconUrls(type);
        final String shadow = specialTypes.contains(type) ?
            specialTypesShadows.get(type) : getRegistry().getDragShadowUrl(type);
        final String script = specialTypes.contains(type) ?
            specialTypesScripts.get(type) : getRegistry().getScriptUrl(type);


        b.append("{" + "\n");

        b.append("  code_name: " + Utils.toJson(type) + "," + "\n");
        b.append("  script: " + Utils.toJson(script) + "," + "\n");
        b.append("  icons: [" + "\n");
        for (String icon: icons) {
            b.append("    " + Utils.toJson(icon) + "," + "\n");
        }
        b.append("    null" + "\n");
        b.append("  ]," + "\n");
        b.append("  drag_shadow: " + Utils.toJson(shadow) + "" + "\n");

        b.append("}" + "\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private RegistryService getRegistry() {
        return (RegistryService) registryTracker.getService();
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Inner classes
    /**
     *
     * @author ksorokin
     */
    public static class ComponentTypeDescriptor {

        private String displayName;
        private String description;
        private String script;
        private String icon;
        private String dragShadow;

        public ComponentTypeDescriptor(
                final String displayName,
                final String description,
                final String script,
                final String icon,
                final String dragShadow) {

            this.displayName = displayName;
            this.description = description;
            this.script = script;
            this.icon = icon;
            this.dragShadow = dragShadow;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getDescription() {
            return description;
        }

        public String getScript() {
            return script;
        }

        public String getIcon() {
            return icon;
        }

        public String getDragShadow() {
            return dragShadow;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String CODE_NAME_PARAMETER = "code_name"; // NOI18N
}
