/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.services.RegistryService;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author ksorokin
 */
public class GetPaletteItems extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;
    private ServiceTracker registryTracker;

    public GetPaletteItems(
            final ToolUtils toolUtils,
            final ServiceTracker registryTracker) {

        this.toolUtils = toolUtils;
        this.registryTracker = registryTracker;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }
            
            return;
        }

        // It is unclear whether we really want to log this.
        //toolUtils.log(session, LOG_SRVT_GET_PALETTE_ITEMS);

        response.setContentType("application/json; charset=utf-8");
        
        final StringBuilder b = new StringBuilder();
        b.append("{" + "\n");

        b.append("  updated: [" + "\n");

        for (String type : getRegistry().getAvailableTypes()) {
            for (String template : getRegistry().getTemplates(type)) {
                b.append(template + "," + "\n");
            }
        }

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"broadcast-template\"," + "\n");

        b.append("      display_name: \"Broadcast\"," + "\n");
        b.append("      description: \"Basic template for broadcast controls.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"broadcast\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1000," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"broadcast\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"broadcast-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"Broadcast \"," + "\n");
        b.append("            description: \"Allows to send the incoming " +
                "message to an arbitrary number of recipients.\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"split-template\"," + "\n");

        b.append("      display_name: \"Split\"," + "\n");
        b.append("      description: \"Basic template for split controls.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"split\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1100," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"split\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"split-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            expression_type: null," + "\n");
        b.append("            expression: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"Split \"," + "\n");
        b.append("            description: \"Allows to split the incoming " +
                "message part, which will then be forwarded.\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"aggregate-template\"," + "\n");

        b.append("      display_name: \"Aggregate\"," + "\n");
        b.append("      description: \"Basic template for aggregate controls.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"aggregate\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1200," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"aggregate\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"aggregate-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            expression_type: null," + "\n");
        b.append("            expression: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"Aggregate \"," + "\n");
        b.append("            description: \"Allows to aggregate several " +
                "incoming messages into one sinle message.\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"filter-template\"," + "\n");

        b.append("      display_name: \"Filter\"," + "\n");
        b.append("      description: \"Basic template for filter controls.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"filter\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1300," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"filter\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"filter-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            expression_type: null," + "\n");
        b.append("            expression: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"Filter \"," + "\n");
        b.append("            description: \"Allows to filter incoming " +
                "messages.\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"tee-template\"," + "\n");

        b.append("      display_name: \"Wire Tap\"," + "\n");
        b.append("      description: \"Basic template for wire tap controls.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"tee\", \"wire\", \"tap\", \"wire tap\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1400," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"tee\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"tee-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"Tee \"," + "\n");
        b.append("            description: \"\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"select-template\"," + "\n");

        b.append("      display_name: \"Content Based Router\"," + "\n");
        b.append("      description: \"Basic template for content based router controls.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"cbr\", \"content\", \"router\", \"based\", \"content based router\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1500," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"select\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"select-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"CBR \"," + "\n");
        b.append("            description: \"\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        b.append("    {" + "\n");
        b.append("      id: \"route-template\"," + "\n");

        b.append("      display_name: \"Named Route\"," + "\n");
        b.append("      description: \"Basic template for named routes.\"," + "\n");
        b.append("      icon: null," + "\n");
        b.append("      tags: [\"eip\", \"pattern\", \"control\", \"named\", \"route\", \"named route\"]," + "\n");

        b.append("      is_drag_source: true," + "\n");
        b.append("      drag_shadow: null," + "\n");

        b.append("      offset: 1600," + "\n");

        b.append("      section: \"eip\"," + "\n");

        b.append("      item_data: {" + "\n");
        b.append("        node_type: \"route\"," + "\n");
        b.append("        node_data: {" + "\n");
        b.append("          properties: {" + "\n");
        b.append("            code_name: \"route-\"," + "\n");
        b.append("            is_shared: false," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }," + "\n");
        b.append("          ui_properties: {" + "\n");
        b.append("            display_name: \"Route \"," + "\n");
        b.append("            description: \"\"," + "\n");
        b.append("            icon: null," + "\n");
        b.append("            noop: null" + "\n");
        b.append("          }" + "\n");
        b.append("        }" + "\n");
        b.append("      }" + "\n");
        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        b.append("    null" + "\n");
        b.append("  ]," + "\n");

        b.append("  removed: [" + "\n");

        // ---------------------------------------------------------------------------
//        b.append("    {" + "\n");
//        b.append("      id: \"\"," + "\n");
//        b.append("    }," + "\n");
        // ---------------------------------------------------------------------------

        b.append("    null" + "\n");
        b.append("  ]" + "\n");

        b.append("}" + "\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private RegistryService getRegistry() {
        return (RegistryService) registryTracker.getService();
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final String LAST_ACCESS =
            "callbacks_get_palette_items_last_access"; // NOI18N
}
