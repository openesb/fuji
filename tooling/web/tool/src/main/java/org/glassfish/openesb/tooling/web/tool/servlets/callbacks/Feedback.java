/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.user.User;
import org.glassfish.openesb.tooling.web.tool.exceptions.UserCheckException;

/**
 *
 * @author ksorokin
 */
public class Feedback extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;

    public Feedback(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        // We need this to correctly read parameters and stuff.
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "UTF-8 encoding is not supported.", e);
            
            return;
        }

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final User currentUser = (User) session.getAttribute(CURRENT_USER);

        final String message = request.getParameter(PARAM_MESSAGE);
        if (message == null) {
            try {
                toolUtils.sendBadRequest(response,
                        "The required parameter '" + PARAM_MESSAGE +
                        "' was not supplied.");
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        toolUtils.log(session, LOG_SRVT_FEEDBACK);

        final Properties properties = System.getProperties();
        properties.setProperty(
                "mail.smtp.host", properties.getProperty(CONFIG_SMTP_HOST));
        properties.setProperty(
                "mail.smtp.port", properties.getProperty(CONFIG_SMTP_PORT));
        properties.setProperty(
                "mail.smtp.timeout", "15000"); // 15 seconds
        properties.setProperty(
                "mail.smtps.host", properties.getProperty(CONFIG_SMTP_HOST));
        properties.setProperty(
                "mail.smtps.port", properties.getProperty(CONFIG_SMTP_PORT));
        properties.setProperty(
                "mail.smtps.timeout", "15000"); // 15 seconds

        properties.setProperty(
                "mail.smtp.starttls.enable",
                properties.getProperty(CONFIG_SMTP_USE_STARTTLS));
        properties.setProperty(
                "mail.smtps.starttls.enable",
                properties.getProperty(CONFIG_SMTP_USE_STARTTLS));

        try {
            final Session mailSession = Session.getDefaultInstance(properties);
            final MimeMessage mailMessage = new MimeMessage(mailSession);

            mailMessage.setFrom(
                    new InternetAddress(properties.getProperty(CONFIG_FEEDBACK_SMTP_FROM)));
            mailMessage.setRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(properties.getProperty(CONFIG_FEEDBACK_SMTP_TO)));

            if (toolUtils.isNobody(currentUser)) {
                mailMessage.setSubject("Anonymous feedback.");
                mailMessage.setText(message);
            } else {
                mailMessage.setSubject("Feedback from " + currentUser.getUsername() + ".");
                mailMessage.setText(
                        "User data:\n" +
                        "  First Name: " + currentUser.getFirstName() + "\n" +
                        "  Last Name:  " + currentUser.getLastName() + "\n" +
                        "  Company:    " + currentUser.getCompany() + "\n" +
                        "  Email:      " + currentUser.getEmail() + "\n" +
                        "------------------------------------------------------------\n\n" +
                        message);
            }


            if ((currentUser.getEmail() != null) && !"".equals(currentUser.getEmail())) {
                mailMessage.setHeader("Reply-To", currentUser.getEmail());
            }

            mailMessage.setSentDate(new Date());
            mailMessage.saveChanges();
            
            final Transport mailTransport;

            if (Boolean.parseBoolean(properties.getProperty(CONFIG_SMTP_USE_SSL))) {
                mailTransport = mailSession.getTransport("smtps");
            } else {
                mailTransport = mailSession.getTransport("smtp");
            }

            if (properties.getProperty(CONFIG_SMTP_USERNAME) != null) {
                mailTransport.connect(
                        properties.getProperty(CONFIG_SMTP_USERNAME),
                        properties.getProperty(CONFIG_SMTP_PASSWORD));
            } else {
                mailTransport.connect();
            }

            mailTransport.sendMessage(mailMessage, mailMessage.getAllRecipients());
        } catch (MessagingException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send feedback message. The message was: " + message, e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        } catch (UserCheckException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to check whether the current user is a 'nobody'.", e);

            try {
                toolUtils.sendInternalError(response, e.getMessage());
            } catch (IOException ex) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", ex);
            }

            return;
        }
    }
}
