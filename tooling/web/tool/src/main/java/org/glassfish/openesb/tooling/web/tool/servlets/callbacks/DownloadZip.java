/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import java.io.File;
import java.io.FileInputStream;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.exceptions.ZipGenerationException;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author ksorokin
 */
public class DownloadZip extends HttpServlet implements Constants {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;
    private File cache;

    public DownloadZip(
            final ToolUtils toolUtils) {

        this.toolUtils = toolUtils;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final Graph currentGraph = (Graph) session.getAttribute(CURRENT_GRAPH);

        if ("do".equals(request.getQueryString())) {
            toolUtils.log(session, LOG_SRVT_DOWNLOAD_ZIP,
                    currentGraph.getEntityId());

            final String filename =
                    currentGraph.getCodeName().startsWith(UNTITLED_CODENAME_PREFIX) ? 
                        "untitled" :
                        currentGraph.getCodeName();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + filename + ".zip");
            response.setHeader("Content-Length", "" + cache.length());

            InputStream input = null;
            try {
                input = new FileInputStream(cache);
                Utils.transferStream(input, response.getOutputStream());
            } catch (IOException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to send the output. " +
                        "Or failed to get the HttpServletResponse output stream.", e);

                return;
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        logger.log(
                                Level.WARNING,
                                "Failed to close an input stream from a file.", e);
                    }
                }

                if (!cache.delete()) {
                    logger.log(Level.WARNING, "Failed to delete the cache file.");
                }
            }
        } else {
            try {
                cache = toolUtils.generateZip(currentGraph, session);
            } catch (ZipGenerationException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to generate a ZIP archive.", e);

                try {
                    toolUtils.sendInternalError(response, e.getMessage());
                } catch (IOException ex) {
                    logger.log(
                            Level.WARNING,
                            "Failed to send an error message.", ex);
                }

                return;
            }

            try {
                response.setContentType("text/plain; charset=utf-8");
                final PrintWriter out = response.getWriter();
                out.print("<iframe src=\"" + request.getContextPath() +
                        HTTP_DOWNLOAD_ZIP_URL + "?do\"></iframe>");
            } catch (IOException e) {
                logger.log(
                        Level.SEVERE,
                        "Failed to send the output.", e);
            }
        }
    }
}
