/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.tool.servlets.callbacks;

import com.sun.jbi.ext.ExchangeDescriptor;
import com.sun.jbi.ext.ExchangeListener;
import com.sun.jbi.ext.MessageDescriptor;
import com.sun.jbi.messaging.MessageService;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.tool.Constants;
import org.glassfish.openesb.tooling.web.tool.ToolUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.ExchangeStatus;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import javax.jbi.messaging.MessageExchange.Role;
import org.glassfish.openesb.tooling.web.utils.Utils;

/**
 *
 * @author apermyakov
 * @author ksorokin
 */
public class GetTracingDataCallback extends HttpServlet
        implements Constants, ExchangeListener {

    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger =
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ToolUtils toolUtils;
    private MessageService service;

    public GetTracingDataCallback(
            final ToolUtils toolUtils,
            final MessageService service) {

        this.toolUtils = toolUtils;
        this.service = service;
    }

    @Override
    protected void doGet(
            final HttpServletRequest request,
            final HttpServletResponse response) {

        final HttpSession session = request.getSession(false);

        if (session == null) {
            try {
                toolUtils.sendInvalidSessionError(response);
            } catch (IOException e) {
                logger.log(
                        Level.WARNING,
                        "Failed to send an error message.", e);
            }

            return;
        }

        final Graph currentGraph =
                (Graph) session.getAttribute(CURRENT_GRAPH);
        if (currentGraph == null) {
            // TODO send Error
            return;
        }

        final Boolean resetTracing =
                Boolean.parseBoolean(request.getParameter("reset"));
        final Boolean startTracing =
                Utils.parseBoolean(request.getParameter("start"));
                request.getAttributeNames();
        final Boolean stopTracing =
                Utils.parseBoolean(request.getParameter("stop"));

//        final Integer dataLength =
//                Integer.parseInt(request.getParameter("data_length"));

        if (startTracing && !currentGraph.isTracingEnabled()) {
            
            service.addListener(this, null);
            currentGraph.setTracingEnabled(true);
        }
        if (stopTracing && currentGraph.isTracingEnabled()) {
            service.removeListener(this);
            currentGraph.setTracingEnabled(false);
            return;
        }

        if (resetTracing) {
            tracingDataActive.clear();
            length = 0;
            return;
        }

        toolUtils.log(session, LOG_SRVT_GET_TRACING_DATA_CALLBACK,
                currentGraph.getEntityId(), startTracing, stopTracing);

        response.setContentType("application/json; charset=utf-8");

        final StringBuilder b = new StringBuilder();
        b.append("{" + "\n");
        b.append("  messages: [" + "\n");

        MessageDescriptor md;
        boolean inbound = true;
        boolean has_fault = false;
        String code_name = "";
        String content = "";

        for (; length < tracingDataActive.size(); length++) {
            ExchangeDescriptor edActive = tracingDataActive.get(length);

            if (edActive == null) {
                continue;
            }
            if ((md = edActive.getIn()) != null) {
                content = md.getMessage();
                inbound = false;
            }
            if ((md = edActive.getOut()) != null) {
                content = md.getMessage();
                inbound = true;
            }
            if ((md = edActive.getFault()) != null) {
                content = md.getMessage();
                has_fault = true;
            }

            if (OW_OPERATION_NAME.equals(edActive.getOperation().getLocalPart())) {
                if (edActive.getConsumer() != null && edActive.getConsumer().lastIndexOf("}") > 0) {
                    code_name = edActive.getConsumer().substring(edActive.getConsumer().lastIndexOf("}") + 1);
                }
                if (edActive.getProvider() != null && edActive.getProvider().lastIndexOf("}") > 0) {
                    code_name = edActive.getProvider().substring(edActive.getProvider().lastIndexOf("}") + 1);
                }
                if (edActive.getService() != null) {
                    code_name = edActive.getService().getLocalPart();
                    inbound = true;
                }
            }
            if (RR_OPERATION_NAME.equals(edActive.getOperation().getLocalPart())) {
                if (edActive.getService() != null) {
                    if (Role.PROVIDER.toString().equalsIgnoreCase(edActive.getRole())) {
                        inbound = true;
                    }
                    if (Role.CONSUMER.toString().equalsIgnoreCase(edActive.getRole())) {
                        inbound = false;
                    }
                    code_name = edActive.getService().getLocalPart();
                }
            }

            Node node = currentGraph.getNodeByPropertyValue("code_name", code_name);

            if (node != null) {
                b.append("        {" + "\n");
                b.append("          content: " + Utils.toJson(content) + "," + "\n");
                b.append("          service: " + Utils.toJson(node.getId()) + "," + "\n");
                b.append("          inbound: " + Utils.toJson(inbound) + "," + "\n");
                b.append("          time: " + Utils.toJson(edActive.getProperties().
                        get(CURRENT_TIME_PROPERTY)) + "," + "\n");
                b.append("          type: " + Utils.toJson("xml") + "," + "\n");
                b.append("          has_fault: " + has_fault + "" + "\n");
                b.append("        }," + "\n");
            }
        }

        b.append("        null" + "\n");
        b.append("      ]" + "\n");
        b.append("}" + "\n");

        try {
            toolUtils.outputGZip(request, response, b);
        } catch (IOException e) {
            logger.log(
                    Level.SEVERE,
                    "Failed to send the output.", e);
        }
    }

    public void onReceive(ExchangeDescriptor ed) {
        //ed.get
        if (ExchangeStatus.ACTIVE.equals(ExchangeStatus.valueOf(ed.getStatus()))) {
            if (OW_OPERATION_NAME.equals(ed.getOperation().getLocalPart())) {
                if (tracingDataId.add(ed.getId())) {
                    tracingDataActive.add(ed);
                    ed.getProperties().put(CURRENT_TIME_PROPERTY,
                            System.currentTimeMillis());
                }
            }
            if (RR_OPERATION_NAME.equals(ed.getOperation().getLocalPart())) {
                if("accept".equals(ed.getAction()))
                {
                    tracingDataActive.add(ed);
                    ed.getProperties().put(CURRENT_TIME_PROPERTY,
                            System.currentTimeMillis());
                }
            }
        }
    }

    private static final String CURRENT_TIME_PROPERTY = "current_time";

    private static int length = 0;
    private static Set<String>  tracingDataId = new HashSet<String>();
    private static List<ExchangeDescriptor> tracingDataActive =
            new ArrayList<ExchangeDescriptor>();

    /** Default operation used for service filters */
    private static final String RR_OPERATION_NAME = "requestReply";
    /** Default operation used for service provider */
    private static final String OW_OPERATION_NAME = "oneWay";

}
