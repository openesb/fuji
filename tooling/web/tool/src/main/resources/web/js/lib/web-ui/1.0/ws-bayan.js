/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Bayan = Class.create({
    sections: null,
    maximized_index: -1,
    element: null,

    initialize: function(element, options, fw_options, rr_options) {
        if (options) {
            Object.extend(this, options);
        }


        if (!rr_options) {
            rr_options = {};
        }

        Element.extend(element);

        this.element = element;
        this.element.ws_bayan = this;

        var self = this;

        if (!fw_options) {
            fw_options = {
                resize_callback : function() {
                    self.__update();
                }
            }
        } else {
            if (fw_options.resize_callbacks) {
                fw_options.resize_callbacks.push(function() {
                    self.__update();
                });
            } else {
                fw_options.resize_callbacks = [ function() {
                    self.__update();
                }
                ]
            }
        }

        new Ws.FloatingWindow(element, fw_options, rr_options);

        this.sections = [];
//        if (this.sections && (this.sections.length > 0)) {
//            var sections_to_add = this.sections; this.sections = [];
//            sections_to_add.each(function(section) {
//                self.add_section(section, true);
//            });
//        }
    },
    __update: function() {
        var i;

        // We do not need to layout anything is there are no sections
        if (this.sections && (this.sections.length > 0)) {
            // If the index of the maximized section is greater or equal to the size
            // of the sections array, we'll set it to _always_ be the last section.


            var palette_height = this.__get_free_height();

            // First, we set the heights of '.section' elements according to the
            // given proportions and their actual required heights. I.e. if a
            // section's contents occupy less height than is allowed by the given
            // proportions, it will be left untouched. If the contents' size exceeds
            // the allowed limit, its height will be forcibly set to the maximum
            // value, and the 'overflow' property will be set to 'auto', resulting in
            // scrollbars.
            var sum_proportion = this.__get_sum_proproportion();
            var sections_to_resize = {};
            var sections_to_reresize = [];

            for (i = 0; i < this.sections.length; i++) {
                var section = this.sections[i];

                if (!section.__isExpanded) {
                    continue;
                }
                // Here we assume that there will be only one '.header' and only one
                // '.contents' element in a palette section.

                //
                //                var max_preferred_contents_height = section.contents_element.scrollHeight +
                //                Ws.Utils.get_bmp(section.contents_element).get_border_vertical();
                var max_preferred_contents_height = section.preferred_content_height;

                var new_contents_height = Math.round(
                    (palette_height * section.proportion / sum_proportion)
                    - section.border_height);

                if (new_contents_height >= max_preferred_contents_height) {
                    sections_to_resize[section.id] = max_preferred_contents_height;
                    sum_proportion = sum_proportion - section.proportion;

                    palette_height = palette_height - section.preferred_height;
                    continue;
                }
                if (new_contents_height <= section.min_content_height) {
                    sections_to_resize[section.id] = section.min_content_height;

                    sum_proportion = sum_proportion - section.proportion;
                    palette_height = palette_height - section.min_height
                    continue;
                }

                sections_to_reresize.push(section);
            }

            for (i = 0; i < sections_to_reresize.length; i++) {
                var section = sections_to_reresize[i];

                sections_to_resize[section.id] = Math.round(
                    (palette_height * section.proportion / sum_proportion)
                    - section.border_height);
            }

            for (var id in sections_to_resize) {
                var section = this.get_section(id);
                if (!section.__isExpanded) {
                    continue;
                }

                section.contents_element.setStyle({
                    overflow: "auto",
                    height: sections_to_resize[section.id] + "px"
                });

                if (section.resize_callback) {
                    section.resize_callback(sections_to_resize[section.id]);
                }
            }
        }
    },

    __get_free_height: function() {
        var wsfw_contents = this.element.select(
            "." + this.element.ws_fwindow.contents_classname).reduce();

        var palette_height = Element.getHeight(wsfw_contents);
        var preferred_height = 0;

        for (var i = 0; i < this.sections.length; i++) {
            var section = this.sections[i];
            if (!section.__isExpanded) {
                palette_height = palette_height - Element.getHeight(section.element) -
                Ws.Utils.get_bmp(section.element).get_margin_vertical();
            } else {
                preferred_height += section.preferred_height;
            }
        }

        return palette_height < preferred_height ? palette_height : preferred_height;
    },

    __get_sum_proproportion: function() {
        var sum_proproportion = 0;
        for (var i = 0; i < this.sections.length; i++) {
            if (this.sections[i].__isExpanded) {
                sum_proproportion = sum_proproportion + this.sections[i].proportion;
            }
        }

        return sum_proproportion;
    },


    get_section: function(id) {
        for (var i = 0; i < this.sections.length; i++) {
            if (this.sections[i].id == id) {
                return this.sections[i];
            }
        }

        return null;
    },

    get_next_section: function(section) {
        for (var i = 0; i < this.sections.length - 1; i++) {
            if (this.sections[i] == section) {
                return this.sections[i + 1];
            }
        }

        return null;
    },

    get_next_visible_section: function(section) {
        for (var i = 0; i < this.sections.length - 1; i++) {
            if (this.sections[i] == section) {
                for (var j = i + 1; j < this.sections.length; j++) {
                    if (this.sections[j].__isExpanded) {
                        return this.sections[j];
                    }
                }

                return null;
            }
        }

        return null;
    },

    add_section: function(section, skip__update, options) {
        this.sections.push(section);
        var palette = this;

        var section_element = new Element("div");
        var section_header_element = new Element("div");
        var section_contents_element = new Element("div");
        var section_footer_resize = new Element("div");
        var section__update_marker_element = new Element("div");
        var section__collapser = new Element("div");
        var section_empty_element = new Element("div");
        var section_no_match_element = new Element("div");

        section_element.addClassName("section");
        section_element.appendChild(section_header_element);
        section_element.appendChild(section_contents_element);
        section_element.appendChild(section_footer_resize);

        section_header_element.addClassName("header");
        section_header_element.innerHTML = section.display_name;
        section_header_element.setAttribute("title", section.description);
        section_header_element.appendChild(section__update_marker_element);
        section_header_element.appendChild(section__collapser);
        if (options && options.header_buttons) {
            for (var i = 0; i < options.header_buttons.length; i++) {
                section_header_element.appendChild(options.header_buttons[i]);
            }
        }

        section_footer_resize.addClassName("footer_resize");
        section_footer_resize.innerHTML = "&nbsp;";
        this.__addResizeListEvent(section_footer_resize);
        section_footer_resize.__section = section;

        section_contents_element.addClassName("contents");

        section__update_marker_element.addClassName("update-marker");

        section__collapser.addClassName("collapser-expander");
        section__collapser.addClassName("expanded");
        section.__isExpanded = true;
        section__collapser.onclick = function() {
            if (this.hasClassName("collapsed")) {
                this.removeClassName("collapsed").addClassName("expanded");
                //    this;
                section.__isExpanded = true;
                section_footer_resize.setStyle({
                    display : "block"
                });
                section_contents_element.setStyle({
                    display : "block"
                });
            } else {
                this.removeClassName("expanded").addClassName("collapsed");
                //  this.addClassName("collapsed");
                section.__isExpanded = false;
                section_contents_element.setStyle({
                    display : "none"
                })
                section_footer_resize.setStyle({
                    display : "none"
                })
            }
            palette.__update();
        }

        section_empty_element.addClassName("text");
        section_empty_element.innerHTML = section.empty_text;

        section_no_match_element.addClassName("text");
        section_no_match_element.innerHTML = section.no_match_text;

        section.element = section_element;
        section.header_element = section_header_element;
        section.footer_element = section_footer_resize;
        section.update_marker_element = section__update_marker_element;
        section.contents_element = section_contents_element;
        section.empty_element = section_empty_element;
        section.no_match_element = section_no_match_element;

        var palette_window_contents = this.element.select(
            "." + this.element.ws_fwindow.contents_classname).reduce();

        palette_window_contents.appendChild(section_element);
        section_contents_element.appendChild(section_empty_element);
        section_contents_element.appendChild(section_no_match_element);

        if (section.items && (section.items.length > 0)) {
            section.empty_element.setStyle({
                display: "none"
            });

            var items_to_add = section.items; section.items = [];
            items_to_add.each(function(item) {
                palette.add_item(item, section, true);
            });
        } else {
            section.empty_element.setStyle({
                display: "block"
            });

            section.items = [];
        }

        section.no_match_element.setStyle({
            display: "none"
        });

        section.min_content_height = parseInt(Element.getStyle(section_contents_element,
            "minHeight")) + Ws.Utils.get_bmp(section_contents_element).get_bmp_vertical();

        section.border_height = Element.getHeight(section_footer_resize) +
        Ws.Utils.get_bmp(section_footer_resize).get_margin_vertical() +
        Element.getHeight(section_header_element) +
        Ws.Utils.get_bmp(section_header_element).get_margin_vertical();

        section.min_height = section.min_content_height + section.border_height;

        section.preferred_height = section.min_height;
        section.preferred_content_height = section.min_content_height;

        if (!skip__update) this.__update();
    },

    remove_section: function(section, skip__update) {
        var index = this.sections.indexOf(section);

        if (index != -1) {
            this.sections = this.sections.splice(index, 1);

            if (section.element) {
                section.element.parentNode.removeChild(section.element);
            }
        }

        if (!skip__update) this.__update();
    },

    __addResizeListEvent: function(resizer) {
        var palette = this;

        resizer.ds_accept_drag = function(x, y) {

            resizer.__next_section = palette.get_next_visible_section(resizer.__section);
            if (resizer.__next_section =="undefined" || resizer.__next_section == null) {
                return false;
            }

            var offset = Element.cumulativeOffset(this);
            var s_offset = Element.cumulativeScrollOffset(this);

            var offset_x = x - (offset.left - s_offset.left);
            var offset_y = y - (offset.top - s_offset.top);

            if ((offset_x > 0) && (offset_x < Element.getWidth(this)) &&
                (offset_y >= 0) && (offset_y < Element.getHeight(this))) {
                return true;
            }

            return false;
        }

        resizer.ds_drag_started = function(x, y) {
            palette.element.setStyle({
                cursor: "n-resize"
            });

            resizer.__initial_section_height = Element.getHeight(
                resizer.__section.contents_element)
            resizer.__initial_next_section_height = Element.getHeight(
                resizer.__next_section.contents_element)
            resizer.__initial_y = y;
        }

        resizer.ds_dragged_to = function(x, y) {
            var dy = y - resizer.__initial_y;
            var section_new_height = resizer.__initial_section_height + dy;
            var next_section_new_height = resizer.__initial_next_section_height - dy;

            if (section_new_height >= resizer.__section.min_content_height
                && next_section_new_height >= resizer.__next_section.min_content_height &&
                section_new_height <= resizer.__section.preferred_content_height &&
                next_section_new_height <= resizer.__next_section.preferred_content_height) {
                resizer.__section.contents_element.setStyle({
                    height: section_new_height + "px"
                });
                if (resizer.__section.resize_callback) {
                    resizer.__section.resize_callback(section_new_height);
                }
                resizer.__next_section.contents_element.setStyle({
                    height: next_section_new_height + "px"
                });
                if (resizer.__next_section.resize_callback) {
                    resizer.__next_section.resize_callback(next_section_new_height);
                }
            }
        }

        resizer.ds_drag_finished = function(x, y) {
            palette.element.setStyle({
                cursor: "default"
            });

            var section_next_height = Element.getHeight(resizer.__next_section.element);
            var section_height = Element.getHeight(resizer.__section.element);

            var palette_height = palette.__get_free_height();
            var sum = palette.__get_sum_proproportion();

            resizer.__next_section.proportion = sum * section_next_height / palette_height;
            resizer.__section.proportion = sum * section_height / palette_height;
        }

        Ws.DragManager.add_drag_source(resizer);
    },

    __update_proportion: function() {

    }
});
