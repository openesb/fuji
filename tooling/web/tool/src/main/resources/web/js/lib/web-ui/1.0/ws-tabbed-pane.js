/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.TabbedPane = Class.create({
    classname_tpane: "-ws-tpane",
    classname_tab: "-ws-tpane-tab",
    classname_contents: "-ws-tpane-contents",
    classname_tab_selected: "-ws-tpane-selected",

    rr_options: null,
    rr_options_selected: null,

    descriptors: null,
    selected_index: 0,

    listeners: null,

    __cached_wsrr: null,

    initialize: function(element, descriptors, options) {
        var i;

        if (options) Object.extend(this, options);
        
        Element.extend(this.element);
        
        this.element = element;
        this.element.ws_tpane = this;

        this.element.addClassName(this.classname_tpane);
        this.element.makePositioned();

        if (!this.element.id) {
            this.element.id = "ws-tpane-" + Ws.TabbedPane.counter++;
        }

        this.descriptors = [];
        this.listeners = [];

        // Iterate over the existing children of the tabbed pane and register them in the
        // descriptors. Then iterate over descriptors and create a tab for each of them.
        // Then hide all tabs instead of the selected one.
        var children = this.element.childElements();
        for (i = 0; i < children.length; i++) {
            descriptors[i].contents = children[i];
        }

        for (i = 0; i < descriptors.length; i++) {
            this.add_tab(descriptors[i], i, true, true);
        }

        this.update_selection(this.selected_index);
    },

    add_tab: function(descriptor, index, skip_contents, skip_layout) {
        var self = this;

        // Initialize the tab element
        descriptor.tab = new Element("div");
        descriptor.tab.addClassName(this.classname_tab);
        descriptor.tab.innerHTML = descriptor.display_name;
        if (descriptor.description) {
            descriptor.tab.setAttribute("title", descriptor.description);
        }

        Event.observe(descriptor.tab, "click", function() {
            self.update_selection(index);
        });

        this.element.appendChild(descriptor.tab);

        // Initialize the contents
        descriptor.contents.addClassName(this.classname_contents);
        if (!skip_contents) {
            if (descriptor.contents.parentNode) {
                descriptor.contents.parentNode.removeChild(descriptor.contents);
            }

            this.element.appendChild(descriptor.contents);
        }

        // Add the tab descriptor to the list of descriptors stored in this tabbed pane
        this.descriptors.splice(index, 0, descriptor);

        // Relayout the tabs
        if (!skip_layout) {
            this.layout();
        }
    },

    update_selection: function(index, skip_layout) {
        for (var i = 0; i < this.descriptors.length; i++) {
            if (i == index) {
                this.descriptors[i].tab.addClassName(this.classname_tab_selected);

                this.descriptors[i].contents.setStyle({
                    display: "block"
                });

                this.selected_index = index;
            } else {
                this.descriptors[i].tab.removeClassName(this.classname_tab_selected);

                this.descriptors[i].contents.setStyle({
                    display: "none"
                });
            }
        }

        if (!skip_layout) {
            this.relayout();
        }
        
        if (this.descriptors[index].select_callback) {
            this.descriptors[index].select_callback();
        }

        this.__notify_listeners(Ws.TabbedPane.EVENT_SELECTION_CHANGED, {});
    },

    relayout: function(ignore_container_height) {
        // In order to properly relayout the tabs, we need to do the following:
        // - remove 'width' and 'height' attributes from the tab's style, to make sure it
        //   has the "natural" dimensions
        // - set the position, width and height of the element according to it's natural
        //   dimensions
        // - update the rounded rectangle, if needed.

        // Relayouting the contents is much easier. We get the tab's lower border
        // coordinates and layout the contents accordingly. If the contents are not
        // visible at the moment, we make no changes.

        var element = this.element;
        var element_bmp = Ws.Utils.get_bmp(element);

        // Starting coordinates for the tabs: since 'absolute' position is calculated
        // relative to the border, we simply set that to the padding.
        var x = element_bmp.get_padding_left(); 
        var y = element_bmp.get_padding_top();

        for (var i = 0; i < this.descriptors.length; i++) {
            var tab = this.descriptors[i].tab;
            var contents = this.descriptors[i].contents;

            var tab_bmp = Ws.Utils.get_bmp(tab);
            var contents_bmp = Ws.Utils.get_bmp(contents);

            // Save the original width and height of the tab to use when reverting to
            // the "natural" dimensions.
            if (typeof tab._wstp_original_width == "undefined") {
                tab._wstp_original_width = tab.style.width;
                tab._wstp_original_height = tab.style.height;
            }

            // Position and size the tab. First make sure it takes the "natural"
            // dimensions. Then apply the 'width' and 'height' styles to make sure
            // the rounded rectangle is drawn correctly.
            tab.setStyle({
                position: "absolute",
                top: y + "px",
                left: x + "px",
                width: tab._wstp_original_width,
                height: tab._wstp_original_height
            });

            var tab_width = Element.getWidth(tab);
            var tab_height = Element.getHeight(tab);
            tab.setStyle({
                width: (tab_width - tab_bmp.get_bp_horizontal() + 1) + "px",
                height: (tab_height - tab_bmp.get_bp_vertical()) + "px"
            });

            // Then position and size the contents. --------------------------------------
            var tab_margin_vertical = tab_bmp.get_margin_vertical();

            var contents_height = Element.getHeight(element) -
                    element_bmp.get_bp_vertical() - tab_height - tab_margin_vertical -
                    contents_bmp.get_bmp_vertical();
            var contents_width = Element.getWidth(element) -
                    element_bmp.get_bp_horizontal() - contents_bmp.get_bp_horizontal();

            // If the height of the contents element is less or equal than zero, we
            // might want to calculate it according to the required size of the
            // contents element content (we have a tab content div element, which in
            // turn has some content, which does require some height.)
            // Note: the same does not apply to the width, as we do not yet have a
            // reliable way to calculate the desired width of the child elements.
            // Note: this uses the value returned by {@link Ws.Utils.get_children_height}
            // which has some usage limitations.
            // 'cc' stands for contents_content.
            if ((contents_height <= 0) || ignore_container_height) {
                var cc_height = Ws.Utils.get_children_height(contents);

                if (cc_height == 0) {
                    contents_height = 10;
                } else {
                    contents_height = cc_height;
                }
            }
            if (contents_width <= 0) {
                contents_width = 10;
            }

            contents.setStyle({
                position: "absolute",
                top: (y + tab_margin_vertical + tab_height) + "px",
                left: element_bmp.get_padding_left() + "px",
                width: contents_width + "px",
                height: contents_height + "px"
            });

            // Adjust the starting X position for the next tab. --------------------------
            x = x + tab_bmp.get_margin_horizontal() + tab_width;
        }

        // If the user wants to have a rounded rectangle on tabs, we should --------------
        // create it on the tab element. It will be half-round in the bottom, as the tab
        // should be an integral part of the contents.
        this.__update_wsrrs();
    },

    add_listener: function(listener) {
        this.listeners.push(listener);
    },

    remove_listener: function(listener) {
        for (var i = 0; i < this.listeners.length; i++) {
            if (this.listeners[i] == listener) {
                this.listeners.splice(i, 1);
                break;
            }
        }
    },

    __notify_listeners: function(event_type, event_data) {
        if (!event_data) event_data = {};

        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i](event_type, event_data);
        }
    },

    __update_wsrrs: function() {
        if (this.rr_options) {
            for (var i = 0; i < this.descriptors.length; i++) {
                var tab = this.descriptors[i].tab;

                if (!tab.ws_rrectangle) {
                    if (i == this.selected_index) {
                        this.rr_options_selected.half_round = "bottom";

                        this.__cached_wsrr = new Ws.RoundedRectangle(
                            tab,
                            this.rr_options_selected
                        );
                    } else {
                        this.rr_options.half_round = "bottom";

                        new Ws.RoundedRectangle(
                            tab,
                            this.rr_options
                        );
                    }
                } else {
                    if ((i == this.selected_index) &&
                            (tab.ws_rrectangle != this.__cached_wsrr)) {

                        var wsrr = tab.ws_rrectangle;
                        var element = this.__cached_wsrr.element;

                        tab.ws_rrectangle.detach();
                        this.__cached_wsrr.detach();

                        wsrr.attach(element, true);
                        this.__cached_wsrr.attach(tab, true);

                        wsrr.relayout();

                        // For MSIE, we also need to repaint, as all drawing is lost
                        // after the DOM changes.
                        if (typeof G_vmlCanvasManager != "undefined") {
                            wsrr.repaint();
                        }
                    }

                    tab.ws_rrectangle.relayout();

                    // For MSIE, we also need to repaint, as all drawing is lost
                    // after DOM changes.
                    if (typeof G_vmlCanvasManager != "undefined") {
                        tab.ws_rrectangle.repaint();
                    }
                }
            }
        }
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});

//----------------------------------------------------------------------------------------
Ws.TabbedPane.EVENT_SELECTION_CHANGED = "selection-changed";

//----------------------------------------------------------------------------------------
Ws.TabbedPane.counter = 0;
