var file_container = {
    pc_get_descriptors: function() {
        var container = {
            sections: [
                {
                    display_name: "Basic",
                    description: "Basic settings.",
                    properties: [
                        {
                            name: "properties_code_name",
                            display_name: "Code Name",
                            description: "Code name of the component.",
                            value: "",
                            type: "string"
                        }
                        ,
                        {
                            name: "properties_file_use",
                            type: Ws.PropertiesSheet.ENUM,

                            display_name: "Use",
                            description: "Use",
                            value: "literal",
                            values:
                                [
                                    {value: "literal", display_name: "Literal"}
                                ]
                        }
                        ,
                        {
                            name: "properties_file_file_directory",
                            type: Ws.PropertiesSheet.STRING,

                            display_name: "File Directory",
                            description: "Path to the directory where the target file resides",
                            value: ""
                        }
                        ,
                        {
                            name: "properties_file_file_name",
                            type: Ws.PropertiesSheet.STRING,

                            display_name: "File Name",
                            description: "Name of the target file.",
                            value: ""
                        }
                        ,
                        {
                            name: "properties_file_file_name_is_pattern",
                            type: Ws.PropertiesSheet.BOOLEAN,

                            display_name: "Name Is Pattern",
                            description: "Whether the file name supplied is really a pattern.",
                            value: false
                        }
                        ,
                        {
                            name: "properties_file_polling_interval",
                            type: Ws.PropertiesSheet.INTEGER,

                            display_name: "Polling interval",
                            description: "Time to wait between subsequent file polls.",
                            value: "",
                            units: "ms"
                        }
                        ,
                        {
                            name: "properties_file_multiple_records_per_file",
                            type: Ws.PropertiesSheet.BOOLEAN,

                            display_name: "Multiple Records Per File",
                            description: "Whether the target file contains multiple records to be read, or to append multiple records to the target file",
                            value: false
                        }
                        ,
                        {
                            name: "properties_file_record_delimiter",
                            type: Ws.PropertiesSheet.STRING,

                            display_name: "Multiple Record Delimiter",
                            description: "Delimiter to use to read or write multiple records in the target file",
                            value: "",

                            depends: {
                                name: "properties_file_multiple_records_per_file",
                                value: true
                            }
                        }
                    ]
                }
                ,
                {
                    display_name: "Display",
                    description: "Display settings.",
                    properties: [
                        {
                            name: "ui_properties_display_name",
                            type: Ws.PropertiesSheet.STRING,

                            display_name: "Display Name",
                            description: "Display Name description",
                            value: ""
                        },
                        {
                            name: "ui_properties_description",
                            type: Ws.PropertiesSheet.TEXT,

                            display_name: "Description",
                            description: "Description description",
                            value: ""
                        }
                    ]
                }
                ,
                {
                    display_name: "Advanced",
                    description: "Advanced settings.",
                    properties: [
                        {
                            name: "raw_warning",
                            type: Ws.PropertiesSheet.LABEL,

                            display_name: "The below properties allow editing the raw " +
                                    "configuration files for this service. Please do " +
                                    "this if and only if you really know what you're " +
                                    "doing.",
                            description: ""
                        }
                        ,
                        {
                            name: "raw_warning_2",
                            type: Ws.PropertiesSheet.LABEL,

                            display_name: "Note that once you click any of the Edit " +
                                    "buttons, the other properties that you might " +
                                    "have changed will be automatically saved.",
                            description: ""
                        }
                        ,
                        {
                            name: "raw_edit_message_xsd",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit message.xsd",
                            description: "The message.xsd file is used to define the data types of the message going in and out of a service.",
                            action: function(psheet) {
                                psheet.save();
                            }
                        }
                        ,
                        {
                            name: "raw_edit_interface_wsdl",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit interface.wsdl",
                            description: "The interface.wsdl file is used to define the service interface.",
                            action: function(psheet) {
                                psheet.save();
                            }
                        }
                        ,
                        {
                            name: "raw_edit_binding_wsdl",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit binding.wsdl",
                            description: "The binding.wsdl file is used to define the actual service-type specific binding of the service interface.",
                            action: function(psheet) {
                                psheet.save();
                            }
                        }
                    ]
                }
                ,
                {
                    display_name: "Tracing",
                    description: "Tracing settings.",
                    properties: [
                        {
                            name: "messages",
                            type: Ws.PropertiesSheet.TABLE,

                            display_name: "Messages",
                            description: "Messages used to test the message flow.",
                            value: {
                                body_caption: [
                                    {
                                        value: "&nbsp;Input"
                                    },
                                    {
                                        value: "&nbsp;Output"
                                    }
                                ],

                                body_value: [
                                    [
                                        {
                                            value: "<img src=\"img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                                            functions_change: false
                                        },
                                        {
                                            value: "<img src=\"img/ui/tester/empty_16.png\"/> <i>No output messages.</i>",
                                            functions_change: false
                                        }
                                    ]
                                ]
                            }
                        }
                    ]
                }
            ]
        };

        // Correct the description height. It would be the second property in the second
        // section.
        container.sections[1].properties[1].initial_height = 30;

        return container;
    },

    pc_set_properties: function(properties) {
        var html = "";

        for (var i in properties) {
            html += "" + i + " => " + properties[i] + "<br/>";
        }

        $("logger").innerHTML = html;
    }
};
