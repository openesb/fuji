/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
/////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

/////////////////////////////////////////////////////////////////////////////////////
Ws.Growl = {
    LEVEL_INFO: "info",
    LEVEL_WARNING: "warning",
    LEVEL_ERROR: "error",

    TTL: 4500,

    classname: "-ws-growl",
    classname_close: "-ws-growl-close",

    offset: 20,
    width: 200,
    radius: 8,
    
    info_fill: "rgba(0, 116, 171, 0.8)", // #006699
    info_stroke: "rgb(0, 81, 120)", // #336699
    info_stroke_width: 2,
    
    warning_fill: "rgba(246, 188, 39, 0.8)", // #ffcc33
    warning_stroke: "rgb(179, 132, 0)", // #cc9933
    warning_stroke_width: 2,
    
    error_fill: "rgba(204, 0, 0, 0.8)", // #cc0000
    error_stroke: "rgb(153, 51, 0)", // #993300
    error_stroke_width: 2,
    
    max_notifications: 5,
    notifications: [],
    
    locked: false,
    
    notify: function(level, text, ttl, show_close_handle) {
        if (Ws.Growl.notifications.length > Ws.Growl.max_notifications - 1) {
            setTimeout(function() {
                Ws.Growl.notify(level, text, ttl, show_close_handle);
            }, 500);
            
            return;
        }
        
        var top_offset = 0;
        Ws.Growl.notifications.each(function(item) {
            top_offset += item.height + Ws.Growl.offset;
        });
        
        var element = new Element("div");
        element.addClassName(Ws.Growl.classname);
        element.setStyle({
            top: top_offset + "px",
            right: "0px",
            
            width: Ws.Growl.width + "px"
        });
        
        element.innerHTML = text;
        
        var fill, stroke, stroke_width;
        switch (level) {
            case Ws.Growl.LEVEL_WARNING:
                fill = Ws.Growl.warning_fill;
                stroke = Ws.Growl.warning_stroke;
                stroke_width = Ws.Growl.warning_stroke_width;
                break;
            
            case Ws.Growl.LEVEL_ERROR:
                fill = Ws.Growl.error_fill;
                stroke = Ws.Growl.error_stroke;
                stroke_width = Ws.Growl.error_stroke_width;
                break;
            
            default:
                fill = Ws.Growl.info_fill;
                stroke = Ws.Growl.info_stroke;
                stroke_width = Ws.Growl.info_stroke_width;
                break;
        }
        
        document.body.appendChild(element);

        var element_bmp = Ws.Utils.get_bmp(element);

        element.setStyle({
            height: (Element.getHeight(element) - element_bmp.get_bp_vertical()) + "px"
        });

        new Ws.RoundedRectangle(element, {
            fill: fill,
            stroke: stroke,
            stroke_width: stroke_width
        });

        var notification = {
            element: element,
            height: Element.getHeight(element)
        }
        Ws.Growl.notifications.push(notification);


        if ((ttl == -1) || show_close_handle) {
            var close_handle_element = new Element("div");
            var close_handle_canvas = Ws.Utils.create_canvas();

            close_handle_element.addClassName(this.classname_close);

            close_handle_element.appendChild(close_handle_canvas);
            element.appendChild(close_handle_element);

            Ws.Utils.draw_close_handle(
                    close_handle_canvas,
                    15,
                    "rgba(0, 0, 0, 0.75)",
                    "rgb(255, 255, 255)",
                    2);

            close_handle_element.onclick = function(event) {
                Ws.Growl.remove_notification(notification);

                if (notification.timeout) {
                    clearTimeout(notification.timeout);
                }
            }
        }

        if (ttl != -1) {
            if (!ttl) {
                ttl = Ws.Growl.TTL;
            }

            notification.timeout = setTimeout(function() {
                Ws.Growl.remove_notification(notification);
            }, ttl);
        }
    },
    
    notify_info: function(text, ttl, show_close_handle) {
        Ws.Growl.notify(Ws.Growl.LEVEL_INFO, text, ttl, show_close_handle);
    },
    
    notify_warning: function(text, ttl, show_close_handle) {
        Ws.Growl.notify(Ws.Growl.LEVEL_WARNING, text, ttl, show_close_handle);
    },
    
    notify_error: function(text, ttl, show_close_handle) {
        Ws.Growl.notify(Ws.Growl.LEVEL_ERROR, text, ttl, show_close_handle);
    },
    
    remove_notification: function(notification) {
        clearTimeout(notification.timeout);
        document.body.removeChild(notification.element);
        
        var do_move = false;
        var index = -1;
        for (var i = 0; i < Ws.Growl.notifications.length; i++) {
            if (Ws.Growl.notifications[i] == notification) {
                index = i;
                do_move = true;
            }
            
            if (do_move) {
                var currentTop = parseInt(
                        Ws.Growl.notifications[i].element.getStyle("top"));
                        
                Ws.Growl.notifications[i].element.setStyle({
                    top: (currentTop - notification.height - 20) + "px"
                });
            }
        }
        
        if (index != -1) {
            Ws.Growl.notifications.splice(index, 1);
        }
    },
    
    // No-op ------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
}
