/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////

var debugging_enabled = false;
var debugging_data = {};
var debugging_timeout = null;
var debugging_messages_list_dialogs = {};
var debugging_message_dialogs = {};
var debugging_breakpoints = {};
var debugging_tracing_autostarted = false;
var debugging_current_message = {};
var debugging_step_by_message = false;

function toggle_debugging() {
    if (!debugging_enabled) {
        debugging_enabled = true;

        if (!tracing_enabled) {
            toggle_tracing();
            debugging_tracing_autostarted = true;
        }

        // Show the debugging dialog
        if (debug_dialog) {
            debug_dialog.ws_psheet.reset();
            debug_dialog.show();
        } else {
            debug_dialog = new Element("div");
            debug_dialog.addClassName("-ws-psheet-window");

            debug_dialog.innerHTML = "" +
                    "<div class=\"-wsfw-header\">Debugging &mdash; Current Message</div>" +
                    "<div class=\"-wsfw-contents\"></div>" +
                    "<div class=\"-wsfw-footer\"></div>";

            document.body.appendChild(debug_dialog);

            new Ws.WindowedPropertiesSheet(
                debug_dialog,
                debugging_message_container,
                {
                }
            );
        }
    } else {
        debugging_enabled = false;
        debug_dialog.hide();

        if (tracing_enabled && debugging_tracing_autostarted) {
            toggle_tracing();
        }
    }
}

function debugging_continue() {
    toggle_tracing();
}

function debugging_step() {
    debugging_step_by_message = true;
    toggle_tracing();
}

var debug_dialog = null;

var debugging_message_container = {
    pc_get_descriptors: function() {
        var content = debugging_current_message.content ?
                debugging_current_message.content : "<No current message>";

        var descriptor = {
            properties: [
                {
                    name: "content",
                    type: Ws.PropertiesSheet.CODE,

                    display_name: "Content",
                    description: "Content of the message.",
                    value: content,

                    maximizable: true,
                    read_only: true
                }
            ]
        }

        return descriptor;
    },

    pc_save_properties: function(properties) {
        // Does nothing
    }
}
