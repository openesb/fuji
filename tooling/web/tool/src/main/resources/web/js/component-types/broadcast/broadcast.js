/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var CONTROL_BROADCAST = "broadcast";

//-----------------------------------------------------------------------------------
var BroadcastControlNode = Class.create(EipNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    CONTROL_BROADCAST, this, ui_options);
        }
    },

    get_outbound_connectors: function() {
        var outbound_links = this.get_outbound_links();
        var max = 2;
//        outbound_links.each(function(item) {
//            if (item.ui.start_node_connector_index > max) {
//                max = item.ui.start_node_connector_index;
//            }
//        });
        for (var i = 0; i < outbound_links.length; i++) {
            var item = outbound_links[i];
            if (item.ui.start_node_connector_index > max) {
                max = item.ui.start_node_connector_index;
            }
        }

        max = max > outbound_links.length ? max : outbound_links.length;

        var connectors = [];

        var length = max + 2;
        for (var i = 1; i < length; i++) {
            connectors.push(
                {
                    max_links: 1
                }
            );
        }

        return connectors;
    }
});

//-----------------------------------------------------------------------------------
var BroadcastControlNodeUi = Class.create(EipNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);

        this.radius = 22;
        this.width = (this.radius * 2) + (this.margin * 2);
        this.height = (this.radius * 2) + (this.margin * 2);

        this.toolbar_x = this.width - this.margin;
    },

    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();
        var angle_step = Math.PI / (connectors.length + 1);

        for (var i = 0; i < connectors.length; i++) {
            var angle = (angle_step * (i + 1)) - (Math.PI / 2);

            connectors[i].x = this.margin +
                    this.radius + (this.radius * Math.cos(angle));
            connectors[i].y = this.margin +
                    this.radius + (this.radius * Math.sin(angle));
            connectors[i].type = Ws.Graph.NODE_CONNECTOR_ORTHOGONAL;
        }

        return connectors;
    },

    ds_drag_started: function($super, x, y, event) {
        // We need to repaint all links in case a new one started to be
        // dragged. The number of connectors might have changed and we need
        // to repaint to make sure the links connect to proper places.
        var do_repaint = this.__drag_new_link;

        $super(x, y, event);

        if (do_repaint) {
            var links = this.node.get_outbound_links();
            for (var i = 0; i < links.length; i++) {
                links[i].ui.repaint();
            }
        }
    },

    cl_accept_doubleclick: function(x, y, event) {
        // Do not accept double clicks, as there are no properties for
        // broadcast.
        return false;
    },

    prepare_path: function() {
        // A--B*
        // |    |
        // D--C*
        var A_x = this.margin, A_y = this.margin;
        var B_x = this.margin + this.radius, B_y = this.margin;
        var C_y = this.height - this.margin;
        var D_x = this.margin, D_y = this.height - this.margin;
        var pi_2 = Math.PI / 2;

        this.context.beginPath();
        this.context.moveTo(A_x, A_y);
        this.context.lineTo(B_x, B_y);
        this.context.arc(B_x, (B_y + C_y) / 2, this.radius, -pi_2, pi_2, false);
        this.context.lineTo(D_x, D_y);
        this.context.lineTo(A_x, A_y);
    },

    __get_orth_point: function(point_on_shape, orth_length) {
        var x_center = this.margin + this.radius;
        var y_center = this.height / 2;

        var sin = (point_on_shape.y - y_center) / this.radius;
        var cos = (point_on_shape.x - x_center) / this.radius;

        return {
            x: point_on_shape.x + cos * orth_length,
            y: point_on_shape.y + sin * orth_length
        }
    },

    draw_node: function($super) {
        $super();

        var span = 17;
        var main_span = 20;
        var diag_span = span / Math.sqrt(2);

        var start_x = this.margin + 5;
        var start_y = this.height / 2;

        var base_x = this.margin + 14;
        var base_y = this.height / 2;

        var diag_offset_1 = (5 - 2) / Math.sqrt(2);
        var diag_offset_2 = (5 + 2) / Math.sqrt(2);

        this.context.lineWidth = 1;
        this.context.fillStyle = this.context.strokeStyle;

        this.context.beginPath();
        this.context.moveTo(start_x, start_y);
        this.context.lineTo(base_x, base_y);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(base_x, base_y);
        this.context.lineTo(base_x, base_y - span);
        this.context.lineTo(base_x - 2, base_y - span + 5);
        this.context.lineTo(base_x + 2, base_y - span + 5);
        this.context.lineTo(base_x, base_y - span);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(base_x, base_y);
        this.context.lineTo(base_x + diag_span, base_y - diag_span);
        this.context.lineTo(base_x + diag_span - diag_offset_2, base_y - diag_span + diag_offset_1);
        this.context.lineTo(base_x + diag_span - diag_offset_1, base_y - diag_span + diag_offset_2);
        this.context.lineTo(base_x + diag_span, base_y - diag_span);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(base_x, base_y);
        this.context.lineTo(base_x + main_span, base_y);
        this.context.lineTo(base_x + main_span - 5, base_y - 2);
        this.context.lineTo(base_x + main_span - 5, base_y + 2);
        this.context.lineTo(base_x + main_span, base_y);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(base_x, base_y);
        this.context.lineTo(base_x + diag_span, base_y + diag_span);
        this.context.lineTo(base_x + diag_span - diag_offset_1, base_y + diag_span - diag_offset_2);
        this.context.lineTo(base_x + diag_span - diag_offset_2, base_y + diag_span - diag_offset_1);
        this.context.lineTo(base_x + diag_span, base_y + diag_span);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(base_x, base_y);
        this.context.lineTo(base_x, base_y + span);
        this.context.lineTo(base_x - 2, base_y + span - 5);
        this.context.lineTo(base_x + 2, base_y + span - 5);
        this.context.lineTo(base_x, base_y + span);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(base_x, base_y, 2, 0, 2 * Math.PI, true);
        this.context.stroke();
        this.context.fill();
    },

    draw_text: function() {
        // Does nothing (no text on broadcast)
    },

    draw_icon: function() {
        // Does nothing (no icon on broadcast)
    },

    __get_toolbar_items: function($super) {
        var self = this;
        var items = [];

        if (!this.read_only) {
            items.push({
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        return items;
    },

    noop: function() {
        // Does nothing.
    }
});

//-----------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(CONTROL_BROADCAST, BroadcastControlNode);
Ws.Graph.NodeUiFactory.register_type(CONTROL_BROADCAST, BroadcastControlNodeUi);
