/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.TreeTable = Class.create({
    element: null,

    initialize: function(tree, element, options, fw_options, rr_options) {
        if (tree) {
            this.tree = tree;
        } else {
            this.tree = [];
        }

        if (options) {
            Object.extend(this, options);
        }

        if (!fw_options) {
            fw_options = {};
        }

        if (!rr_options) {
            rr_options = {};
        }

        Element.extend(element);

        this.element = element;
        this.element.ws_treeTable = this;

    //    new Ws.FloatingWindow(element, fw_options, rr_options);
        if (!this.element.id) {
            this.element.id = "tree_table_id" + Ws.TreeTable.counter++;
        }

        this.render();
    },

    __expandNode: function(node) {
        this.element.select("#" + node.id)[0].setStyle({
            display : "table-row"
        })
        if (node.isExpanded && node.children) {
            for (var i = 0; i < node.children.length; i++) {
                this.__expandNode(node.children[i]);
            }
        }
    },

    __collapseNode: function(node) {
        this.element.select("#" + node.id)[0].setStyle({
            display : "none"
        })
        if (node.isExpanded && node.children) {
            for (var i = 0; i < node.children.length; i++) {
                this.__collapseNode(node.children[i]);
            }
        }
    },

    expandCollapseNode : function(nodeId) {
        var i, node = this.nodes[nodeId];

        if (node.isExpanded) {
            this.element.select("#" + nodeId + " span.expander")[0].
                addClassName("collapse").removeClassName("expand");

        } else {
            this.element.select("#" + nodeId + " span.expander")[0].
                addClassName("expand").removeClassName("collapse");
        }

        var children = this.nodes[nodeId].children;
        if (children) {
            if (node.isExpanded) {
                for (i = 0; i < children.length; i++) {
                    this.__collapseNode(children[i]);
                }
            } else {
                for (i = 0; i < children.length; i++) {
                    this.__expandNode(children[i]);
                }               
            }
        }
        
        node.isExpanded = !node.isExpanded;
    },

    render: function() {
        this.__nodeCounter = 0;
        this.nodes = {};
        var node = null;
        var html = "<table class=\"treeTable\"><tbody>";
        for (var i = 0; i < this.tree.length; i++) {
            node = this.tree[i];
            node.isRoot = true;
            node.isLast = i == this.tree.length - 1;
            html += this.getHtmlForNode(this.tree[i], 0, "table-row", null, "");
        }
        html += "</tbody></table>"
        if (this.element.ws_fwindow) {
            this.element.ws_fwindow.contents.innerHTML = html;
        } else {
            this.element.innerHTML = html;
        }
    },

    getHtmlForNode: function(node, padding, display, parentNode, htmlSpan) {
        var i, result = "";
        node.id = this.__nodeCounter++;
        this.nodes[node.id] = node;

        result += "<tr id=\"" + node.id + "\" style=\"display:" + display + "\"";
        if (node.isRoot) {
            result += " class=\"isRoot\"";
        }
        result +=  ">";
        
        result += "<td class=\"tree\" style=\"padding-left: " + padding + "px\">";
        if (!node.isRoot) {
            if ((parentNode != null) && !parentNode.isRoot) {
                result += htmlSpan;
                if (parentNode.isLast) {
                    result += "<span class=\"secondIcon\"></span>";
                    htmlSpan += "<span class=\"secondIcon\"></span>";
                } else {
                    result += "<span class=\"secondIcon Line\"></span>";
                    htmlSpan += "<span class=\"secondIcon Line\"></span>";
                }
            }
            if (node.isLast) {
                result += "<span class=\"firstIcon Last\"></span>";
            } else {
                result += "<span class=\"firstIcon T\"></span>";
            }
        }

        if (node.children && (node.children.length > 0)) {
            if (node.isExpanded) {
                result += "<span " +
                        "class=\"expander expand\" " +
                        "onclick=\"$('" + this.element.id +
                        "').ws_treeTable.expandCollapseNode(" + node.id + ")\">" +
                    "</span>";
            } else {
                result += "<span " +
                        "class=\"expander collapse\" " +
                        "onclick=\"$('" + this.element.id +
                        "').ws_treeTable.expandCollapseNode(" + node.id + ")\">" +
                    "</span>";
            }
        } else {
            result += "<span class=\"leaf\"></span>";
        }

        if (node.type) {
            result += "<span class=\"node-type-" + node.type + "\"></span>"
        }
        
        result += node.name;
        result += "</td>";

        // table data
        for (i = 0; i < node.data.length; i++) {
            result += "<td>";
            result += node.data[i].name;
            result += "</td>";
        }

        result += "</tr>";
        padding += padding + 0;

        for (i = 0; i < node.children.length; i++) {
            node.children[i].isLast = (i == (node.children.length - 1));
            
            if (node.isExpanded) {
                result += this.getHtmlForNode(node.children[i], padding, 
                display, node, htmlSpan);
            } else {
                result += this.getHtmlForNode(node.children[i], padding, 
                "none", node, htmlSpan);
            }
        }

        return result;
    },

    __update: function() {
  
    },

    noop: function() {

    }

});

Ws.TreeTable.counter = 0;