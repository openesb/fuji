/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

Ws.Graph.NODE_TBAR_DELETE = "resource?img/ui/diagram/node-toolbar/tbar-delete.png";
Ws.Graph.NODE_TBAR_EDIT_PROPERTIES = "resource?img/ui/diagram/node-toolbar/tbar-properties.png";
Ws.Graph.NODE_TBAR_CREATE_SHORTCUT = "resource?img/ui/diagram/node-toolbar/tbar-shortcut.png";
Ws.Graph.NODE_TBAR_DELETE_DESATURATE = "resource?img/ui/diagram/node-toolbar/tbar-delete_d.png";
Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE = "resource?img/ui/diagram/node-toolbar/tbar-properties_d.png";
Ws.Graph.NODE_TBAR_CREATE_SHORTCUT_DESATURATE = "resource?img/ui/diagram/node-toolbar/tbar-shortcut_d.png";

Ws.Graph.NODE_BADGE_SHORTCUT = "resource?img/ui/diagram/node-badges/badge-shortcut.png";

Ws.FloatingWindow.WINDOW_CLOSE = "resource?img/ui/window/window_close.png";
Ws.FloatingWindow.WINDOW_NORMAL = "resource?img/ui/window/window_normal.png";
Ws.FloatingWindow.WINDOW_MAX = "resource?img/ui/window/window_max.png";
Ws.FloatingWindow.WINDOW_MIN = "resource?img/ui/window/window_min.png";

Event.observe(window, "load", function() {
    new Ws.Graph($("diagram"));
    
    // Add the already existing nodes to the diagram. The nodes are added anynchronously,
    // as their types might require additional loading. Once all the nodes (and their 
    // types are loaded, the links loading process is triggered).
    load_graph(current_graph_nodes, current_graph_links);
    
    // Attach the graph modification listener, which will update the server-side model
    $("diagram").ws_graph.add_listener(save_graph);
});

function update_graph_properties() {
    var i, graph = $("diagram").ws_graph;

    var read_only = is_current_graph_read_only();

    for (i = 0; i < graph.nodes.length; i++) {
        graph.nodes[i].ui.read_only = read_only;
        graph.nodes[i].read_only = read_only;

        if (graph.nodes[i].__ws_psheet_window) {
            document.body.removeChild(graph.nodes[i].__ws_psheet_window);

            graph.nodes[i].__ws_psheet = null;
            graph.nodes[i].__ws_psheet_window = null;
        }
    }

    for (i = 0; i < graph.links.length; i++) {
        graph.links[i].ui.read_only = read_only;
        graph.links[i].read_only = read_only;
    }

    // Lastly, if we're readonly we need to hide the palette and update the status
    // message.
    if (read_only) {
        $("palette").setStyle({
            display: "none"
        });
    } else {
        $("palette").setStyle({
            display: "block"
        });
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Graph loading
var __graph_nodes_loaded = 0;

function load_graph(nodes, links) {
    __graph_nodes_loaded = 0;
    
    for (var i = 0; i < nodes.length; i++) {
        __load_graph_node(nodes[i], nodes, links);
    }
}

function __load_graph_node(node, nodes, links) {
    ComponentTypesLoadManager.load(node.type, function() {
        __do_load_graph_node(node, nodes, links);
    }, function() {
        Ws.Growl.notify_error(
                "Failed to add an existing node to the graph.", -1, true);
    });
}

function __do_load_graph_node(node, nodes, links) {
    var type = ComponentTypesLoadManager.get_type(node.type);

    node.data.ui_addenda = {};
    node.data.ui_addenda.icons = type.icons;

    if (node.data.shortcut_to_id != null) {
        var shortcut_to = $("diagram").ws_graph.get_node_by_id(node.data.shortcut_to_id);

        if (shortcut_to != null) {
            node.data.shortcut_to_id = null;
            node.data.shortcut_to = shortcut_to;
        } else {
            setTimeout(function() {
                __do_load_graph_node(node, nodes, links);
            }, 100);
            return;
        }
    }

    if (node.data.parent_id != null) {
        var parent = $("diagram").ws_graph.get_node_by_id(node.data.parent_id);

        if (parent != null) {
            node.data.parent_id = null;
            node.data.parent = parent;
        } else {
            setTimeout(function() {
                __do_load_graph_node(node, nodes, links);
            }, 100);
            return;
        }
    }

    $("diagram").ws_graph.create_and_add_node(node.type, node.data, true);

    __graph_nodes_loaded++;

    // If all existing nodes were already loaded, load the links
    if (__graph_nodes_loaded == nodes.length) {
        for (var j = 0; j < links.length; j++) {
            __load_graph_link(links[j]);
        }

        $("diagram").ws_graph.ui.repaint();
        update_graph_properties();
    }
}

function __load_graph_link(link) {
    if (link.data.start_node) {
        link.data.start_node = 
                $("diagram").ws_graph.get_node_by_id(link.data.start_node);
    }
    
    if (link.data.end_node) {
        link.data.end_node = 
                $("diagram").ws_graph.get_node_by_id(link.data.end_node);
    }
    
    $("diagram").ws_graph.create_and_add_link(link.type, link.data, true);
}

//////////////////////////////////////////////////////////////////////////////////////////
// Graph saving
var save_graph_queue = [];

function save_graph(event) {
    save_graph_queue.push(event);
    __save_graph();
}

function __save_graph() {
    if (lock) return;

    if (is_current_graph_read_only()) {
        return; // We decide to silently not do anything is we're asked to save and the
                // graph is read-only.
    }


    if (save_graph_queue.length == 0) {
        hide_status_progress();
        return;
    }
    
    lock = true;
    show_status_progress();

    var event = save_graph_queue.shift();
    
    var request = "";
    var data = event.data;
    var i;

    request += "explicit: false, ";

    if ((event.type == Ws.Graph.EVENT_NODE_ADDED) || 
            (event.type == Ws.Graph.EVENT_NODE_UPDATED)) {

        request += "updated_nodes: [";
        request += "{";
        
        request += "id: " + Object.toJSON(data.node.id) + ", ";
        request += "type: " + Object.toJSON(data.node.type) + ", ";
        
        if (data.node.shortcut_to) {
            request += "shortcut_to_id: " + Object.toJSON(data.node.shortcut_to.id) + ", ";
        } else {
            request += "shortcut_to_id: null, ";
        }

        if (data.node.parent) {
            request += "parent_id: " + Object.toJSON(data.node.parent.id) + ", ";
        } else {
            request += "parent_id: null, ";
        }
        
        if (data.updated_properties) {
            request += "updated_properties: {";
            for (i = 0; i < data.updated_properties.length; i++) {
                if (i != "noop") {
                    request += "" + data.updated_properties[i].name + ": " + 
                        Object.toJSON(data.updated_properties[i].new_value) + ", ";
                }
            }
            request += "noop: null";
            request += "}, ";
        }
        
        if (data.updated_ui_properties) {
            request += "updated_ui_properties: {";
            for (i = 0; i < data.updated_ui_properties.length; i++) {
                if (i != "noop") {
                    request += "" + data.updated_ui_properties[i].name + ": " + 
                        Object.toJSON(data.updated_ui_properties[i].new_value) + ", ";
                }
            }
            request += "noop: null";
            request += "}, ";
        }
        
        request += "noop: null";
        
        request += "}";
        request += "]";
    }
    
    if (event.type == Ws.Graph.EVENT_NODE_REMOVED) {
        request += "removed_nodes: ["
        request += "{";
        
        request += "id: " + Object.toJSON(data.node.id) + ", ";
        
        request += "noop: null";
        
        request += "}";
        request += "]";
    }
    
    if (event.type == Ws.Graph.EVENT_LINK_ADDED) {
        request += "updated_links: [";
        request += "{";
        
        request += "id: " + Object.toJSON(data.link.id) + ", ";
        request += "type: " + Object.toJSON(data.link.type) + ", ";
        
        if (data.link.start_node) {
            request += "start_node: " + Object.toJSON(data.link.start_node.id) + ", ";
            request += "start_node_connector: " + Object.toJSON(data.link.start_node_connector) + ", ";
        } else {
            request += "start_node: null, ";
            request += "start_node_connector: 0, ";
        }
        
        if (data.link.end_node) {
            request += "end_node: " + Object.toJSON(data.link.end_node.id) + ", ";
            request += "end_node_connector: " + Object.toJSON(data.link.end_node_connector) + ", ";
        } else {
            request += "end_node: null, ";
            request += "end_node_connector: 0, ";
        }
        
        request += "updated_properties: {";
        for (i in data.link.properties) {
            if (i != "noop") {
                request += "" + i + ": " + Object.toJSON(data.link.properties[i]) + ", ";
            }
        }
        request += "noop: null";
        request += "}, ";
        
        request += "updated_ui_properties: {";
        for (i in data.link.ui_properties) {
            if (i != "noop") {
                request += "" + i + ": " + Object.toJSON(data.link.ui_properties[i]) + ", ";
            }
        }
        request += "noop: null";
        request += "}, ";
        
        request += "noop: null";
        
        request += "}";
        request += "]";
    }
    
    if (event.type == Ws.Graph.EVENT_LINK_UPDATED) {
        request += "updated_links: [";
        request += "{";
        
        request += "id: " + Object.toJSON(data.link.id) + ", ";
        request += "type: " + Object.toJSON(data.link.type) + ", ";
        
        if (data.link.start_node) {
            request += "start_node: " + Object.toJSON(data.link.start_node.id) + ", ";
            request += "start_node_connector: " + Object.toJSON(data.link.start_node_connector) + ", ";
        } else {
            request += "start_node: null, ";
            request += "start_node_connector: 0, ";
        }
        
        if (data.link.end_node) {
            request += "end_node: " + Object.toJSON(data.link.end_node.id) + ", ";
            request += "end_node_connector: " + Object.toJSON(data.link.end_node_connector) + ", ";
        } else {
            request += "end_node: null, ";
            request += "end_node_connector: 0, ";
        }
        
        if (data.updated_properties) {
            request += "updated_properties: {";
            for (i = 0; i < data.updated_properties.length; i++) {
                if (i != "noop") {
                    request += "" + data.updated_properties[i].name + ": " + 
                        Object.toJSON(data.updated_properties[i].new_value) + ", ";
                }
            }
            request += "noop: null";
            request += "}, ";
        }
        
        if (data.updated_ui_properties) {
            request += "updated_ui_properties: {";
            for (i = 0; i < data.updated_ui_properties.length; i++) {
                if (i != "noop") {
                    request += "" + data.updated_ui_properties[i].name + ": " + 
                        Object.toJSON(data.updated_ui_properties[i].new_value) + ", ";
                }
            }
            request += "noop: null";
            request += "}, ";
        }
        
        request += "noop: null";
        
        request += "}";
        request += "]";
    }
    
    if (event.type == Ws.Graph.EVENT_LINK_REMOVED) {
        request += "removed_links: ["
        request += "{";
        
        request += "id: " + Object.toJSON(data.link.id) + ", ";        
        request += "noop: null";
        
        request += "}";
        request += "]";
    }
    
    request = "{" + request + "}";
    new Ajax.Request("callbacks/save", {
        method: "post",
        
        postBody: request,
        
        onSuccess: function(transport) {
            // Mystically the onSuccess is called if the server cannot be contacted. So
            // we look at the status and if it 0, treat it as failure.
            if (transport.status == 0) {
                Ws.Growl.notify_error(
                        "Failed to save the message flow. " +
                        "The server could not be contacted.", -1, true);
                lock = false;
                save_graph_queue = [];
            } else {
                lock = false;
                __save_graph();
            }
        },
        
        onFailure: function(transport) {
            // If we failed due to a session timeout, we just need to resend the data,
            // as the session was automatically reestablished on the server side. We'll
            // display a warning message to the user, telling him of what's happened.
            // TODO: We'd like to add the re-login dialog here, if we move back to
            //       multi-user mode.
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                save_graph_queue.unshift(event);

                lock = false;
                reestablish_session(__save_graph);
            } else {
                Ws.Growl.notify_error(
                        "Failed to save the message flow. " +
                        "The server responded: <br/><i>" +
                        transport.statusText + "</i>", -1, true);
                lock = false;
                save_graph_queue = [];
            }
        }
    });
}

//////////////////////////////////////////////////////////////////////////////////////////
// External artifacts and IFL sources
var external_artifacts_editor = null;
var ifl_source_editor = null;

function edit_external_artifact(node_id, name, read_only, type) {
    if (lock) {
        setTimeout(function() {
            edit_external_artifact(node_id, name, read_only, type);
        }, 200);
    }

    new Ajax.Request("callbacks/get-external-artifact", {
        method: "get",

        parameters: {
            "node-id": node_id,
            "artifact": name
        },

        onSuccess: function(transport) {
            var result = transport.responseJSON;

            if (!result.code) {
                show_global_progress("Loading value...");
                setTimeout(function() {
                    new Ajax.Request("callbacks/get-external-artifact", {
                        method: "get",

                        parameters: {
                            "node-id": node_id,
                            "artifact": name,
                            "generate": true
                        },

                        onSuccess: function(transport) {
                            hide_global_progress();
                            show_external_artifact_editor(node_id, 
                                    name, transport.responseJSON.code, read_only, type);
                        },

                        onFailure: function(transport) {
                            // TODO
                        }
                    });
                }, 700);
            } else {
                show_external_artifact_editor(node_id,
                        name, transport.responseJSON.code, read_only, type);
            }
        },

        onFailure: function(transport) {
            // TODO
        }
    });
}

function show_external_artifact_editor(node_id, name, code, read_only, type) {
    var textarea;

    if (external_artifacts_editor) {
        // Load the title.
        // TODO

        // Load the code.
        textarea = external_artifacts_editor.select("textarea").reduce();
        textarea.value = code;

        // Load the input actions. The first input is Cancel, the last one is Save.
        var input_cancel = external_artifacts_editor.select("input.cancel")[0];
        input_cancel.onclick = function() {
            external_artifacts_editor.ws_mwindow.close();
            editAreaLoader.delete_instance(textarea.id);
        }

        var input_save = external_artifacts_editor.select("input.save")[0];
        input_save.onclick = function() {
            new Ajax.Request("callbacks/set-external-artifact", {
                method: "post",

                parameters: {
                    "node-id": node_id,
                    "artifact": name,
                    "artifact-code": editAreaLoader.getValue(textarea.id)
                },

                onSuccess: function(transport) {
                    external_artifacts_editor.ws_mwindow.close();
                    editAreaLoader.delete_instance(textarea.id);
                },

                onFailure: function(transport) {
                    // TODO
                }
            });
        }

        if (read_only) {
            input_cancel.value = "Close";
            input_save.style.display = "none";
        } else {
            input_cancel.value = "Cancel";
            input_save.style.display = "";
        }
        
        if (external_artifacts_editor.ws_mwindow) {
            external_artifacts_editor.ws_mwindow.show();
            editAreaLoader.init({
                id : textarea.id,
                min_height : 5,
                min_width: 5,
                syntax: type,
                start_highlight: true,
                allow_toggle: false,
                allow_resize: "no"
            });
        } else {
            if (textarea.id == "") {
                textarea.id = "idTextArea";
            }

            new Ws.ModalWindow(external_artifacts_editor);
            editAreaLoader.init({
                id : textarea.id,
                min_height : 5,
                min_width: 5,
                syntax: type,
                start_highlight: true,
                allow_toggle: false,
                allow_resize: "no"
            });
        }
    } else {
        var window_wh = Ws.Utils.get_window_dimensions();

        external_artifacts_editor = new Element("div");
        external_artifacts_editor.setStyle({
            position: "relative",
            width: (window_wh.width - 50) + "px",
            height: (window_wh.height - 50) + "px",
            padding: "2px 2px 2px 2px"
        });

        textarea = new Element("textarea");
        textarea.addClassName("-ws-psheet-type-maximized-code");
        textarea.setStyle({
            display: "block",
            width: (window_wh.width - 73) + "px",
            height: (window_wh.height - 98) + "px",
            margin: "3px 3px 0px 3px"
        });

        var buttonsDiv = new Element("div");
        buttonsDiv.setStyle({
            height: "20px",
            margin: "5px 3px 0px 3px",
            textAlign: "right"
        });

        var cancelButton = new Element("input");
        cancelButton.setAttribute("type", "button");
        cancelButton.value = "Cancel";
        cancelButton.addClassName("cancel");
        cancelButton.setStyle({
            marginRight: "10px"
        });

        var saveButton = new Element("input");
        saveButton.setAttribute("type", "button");
        saveButton.value = "Save";
        saveButton.addClassName("save");

        external_artifacts_editor.appendChild(textarea);
        external_artifacts_editor.appendChild(buttonsDiv);
        buttonsDiv.appendChild(cancelButton);
        buttonsDiv.appendChild(saveButton);
        document.body.appendChild(external_artifacts_editor);

        show_external_artifact_editor(node_id, name, code, read_only, type);
    }
}

function edit_ifl_source() {
    if (lock) {
        setTimeout(function() {
            edit_ifl_source();
        }, 200);
    }

    new Ajax.Request("callbacks/get-ifl-source", {
        method: "get",

        parameters: {
        },

        onSuccess: function(transport) {
            show_ifl_source_editor(transport.responseJSON.code);
        },

        onFailure: function(transport) {
            // TODO
        }
    });
}

function show_ifl_source_editor(code) {
    var textarea;

    if (ifl_source_editor) {
        // Load the title.
        // TODO

        // Load the code.
        textarea = ifl_source_editor.select("textarea").reduce();
        textarea.value = code;

        // Load the input actions. The first input is Cancel, the last one is Save.
        var input_cancel = ifl_source_editor.select("input.cancel")[0];
        input_cancel.onclick = function() {
            ifl_source_editor.ws_mwindow.close();
            editAreaLoader.delete_instance(textarea.id);
        }

        var input_save = ifl_source_editor.select("input.save")[0];
        input_save.onclick = function() {
            new Ajax.Request("callbacks/set-ifl-source", {
                method: "post",

                parameters: {
                    "code": editAreaLoader.getValue(textarea.id)
                },

                onSuccess: function(transport) {
                    ifl_source_editor.ws_mwindow.close();
                    editAreaLoader.delete_instance(textarea.id);

                    $("diagram").ws_graph.clear(true);

                    var graph_data = transport.responseJSON;
                    load_graph(graph_data.nodes, graph_data.links);
                },

                onFailure: function(transport) {
                    // TODO
                }
            });
        }

        if (ifl_source_editor.ws_mwindow) {
            ifl_source_editor.ws_mwindow.show();
            editAreaLoader.init({
                id : textarea.id,
                min_height : 5,
                min_width: 5,
                syntax: "ifl",
                start_highlight: true,
                allow_toggle: false,
                allow_resize: "no"
            });
        } else {
            textarea.id = "iflSourceTextAreaId";

            new Ws.ModalWindow(ifl_source_editor);
            editAreaLoader.init({
                id : textarea.id,
                min_height : 5,
                min_width: 5,
                syntax: "ifl",
                start_highlight: true,
                allow_toggle: false,
                allow_resize: "no"
            });
        }
    } else {
        var window_wh = Ws.Utils.get_window_dimensions();

        ifl_source_editor = new Element("div");
        ifl_source_editor.setStyle({
            position: "relative",
            width: (window_wh.width - 50) + "px",
            height: (window_wh.height - 50) + "px",
            padding: "2px 2px 2px 2px"
        });

        textarea = new Element("textarea");
        textarea.addClassName("-ws-psheet-type-maximized-code");
        textarea.setStyle({
            display: "block",
            width: (window_wh.width - 73) + "px",
            height: (window_wh.height - 98) + "px",
            margin: "3px 3px 0px 3px"
        });

        var buttonsDiv = new Element("div");
        buttonsDiv.setStyle({
            height: "20px",
            margin: "5px 3px 0px 3px",
            textAlign: "right"
        });

        var cancelButton = new Element("input");
        cancelButton.setAttribute("type", "button");
        cancelButton.value = "Cancel";
        cancelButton.addClassName("cancel");
        cancelButton.setStyle({
            marginRight: "10px"
        });

        var saveButton = new Element("input");
        saveButton.setAttribute("type", "button");
        saveButton.value = "Save";
        saveButton.addClassName("save");

        ifl_source_editor.appendChild(textarea);
        ifl_source_editor.appendChild(buttonsDiv);
        buttonsDiv.appendChild(cancelButton);
        buttonsDiv.appendChild(saveButton);
        document.body.appendChild(ifl_source_editor);

        show_ifl_source_editor(code);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Base classes for component types nodes
var BaseNode = Class.create(Ws.Graph.DefaultNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);
    },

    get_inbound_connectors: function() {
        return [
            {
                max_links: 1
            }
        ];
    },

    get_outbound_connectors: function() {
        // If we're connected to a CBR or a Wire Tap (1st connector), we cannot connect to
        // anything on the outside.

        var can_have_outbound_connectors = true;

        var inbound_links = this.get_inbound_links();
        for (var i = 0; i < inbound_links.length; i++) {
            var link = inbound_links[i];

            if (link.start_node && (link.start_node.type == "select" ||
                    (link.start_node.type == "tee" && link.start_node_connector == 1))) {

                can_have_outbound_connectors = false;
                break;
            }
        }

        if (!can_have_outbound_connectors) {
            return [];
        }

        return [
            {
                max_links: 1
            }
        ];
    },

    pc_get_descriptors: function($super) {
        var node = this;
        var container = {
            sections: [
                {
                    display_name: "Basic",
                    description: "Basic settings.",
                    properties: []
                },
                {
                    display_name: "Display",
                    description: "Display settings.",
                    properties: []
                },
                {
                    display_name: "Advanced",
                    description: "Advanced settings.",
                    properties: [
                        {
                            name: "raw_warning",
                            type: Ws.PropertiesSheet.LABEL,

                            display_name: "The below properties allow editing the raw " +
                                    "configuration files for this service. Please do " +
                                    "this if and only if you really know what you're " +
                                    "doing.",
                            description: ""
                        }
                        ,
                        {
                            name: "raw_warning_2",
                            type: Ws.PropertiesSheet.LABEL,

                            display_name: "Note that once you click any of the Edit " +
                                    "buttons, the other properties that you might " +
                                    "have changed will be automatically saved.",
                            description: ""
                        }
                        ,
                        {
                            name: "raw_edit_message_xsd",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit message.xsd",
                            description: "The message.xsd file is used to define the data types of the message going in and out of a service.",
                            action: function(psheet) {
                                psheet.save();
                                edit_external_artifact(node.id, "message-xsd", false, "xml");
                            }
                        }
                        ,
                        {
                            name: "raw_edit_interface_wsdl",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit interface.wsdl",
                            description: "The interface.wsdl file is used to define the service interface.",
                            action: function(psheet) {
                                psheet.save();
                                edit_external_artifact(node.id, "interface-wsdl", false, "xml");
                            }
                        }
                        ,
                        {
                            name: "raw_edit_binding_wsdl",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit binding.wsdl",
                            description: "The binding.wsdl file is used to define the actual service-type specific binding of the service interface.",
                            action: function(psheet) {
                                psheet.save();
                                edit_external_artifact(node.id, "binding-wsdl", false, "xml");
                            }
                        }
                    ]
                }
            ]
        };
        var super_container = $super();

        container.sections[0].properties.push({
            name: "properties_code_name",
            display_name: "Code Name",
            description: "Code name of the component.",
            value: this.properties.code_name,
            type: "string"
        });

        super_container.properties.each(function(property) {
            container.sections[1].properties.push(property);
        });

        // Correct the description height. It would be the second property in the second
        // section.
        container.sections[1].properties[1].initial_height = 30;

        if (config.deployment_enabled) {
            container.sections.push({
                    display_name: "Tracing",
                    description: "Tracing settings.",
                    properties: []
                });

            var tracing_property = {
                name: "messages",
                type: Ws.PropertiesSheet.TABLE,

                display_name: "Messages",
                description: "Messages used to test the message flow.",
                value: {
                    body_caption: [
                    {
                        value: "&nbsp;Input"
                    },
                    {
                        value: "&nbsp;Output"
                    }
                    ],

                    body_value: [[
                    {
                        value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                        functions_change: false
                    },
                    {
                        value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No output messages.</i>",
                        functions_change: false
                    }
                    ]]
                }
            }

            if (tracing_data && tracing_data[this.id]) {

                var messagesIn = tracing_data[this.id].inbound;
                var messagesOut = tracing_data[this.id].outbound;

                var lenght = Math.max(messagesIn.length, messagesOut.length);

                var value = tracing_property.value;

                value.body_value = [];
                for (var i = lenght - 1; i >= 0; i--) {
                    var row = []

                    if (i < messagesIn.length) {
                        row.push({
                            value: "<img src=\"resource?img/ui/tracing/message_16.png\"/> " +
                            (new Date(messagesIn[i].time).toLocaleString()),
                            functions_change: true,
                            functions_change_params: "" + this.id + ", " + true + ", " + i
                        });
                    } else if (i == 0 && messagesIn.length == 0) {
                        row.push({
                            value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                            functions_change: false

                        });
                    } else {
                        row.push({
                            value: "",
                            functions_change: false
                        });
                    }

                    if (i < messagesOut.length) {
                        row.push({
                            value: "<img src=\"resource?img/ui/tracing/message_16.png\"/> " +
                            (new Date(messagesOut[i].time).toLocaleString()),
                            functions_change: true,
                            functions_change_params: "" + this.id + ", " + false + ", " + i
                        });
                    } else if (i == 0 && messagesOut.length == 0) {
                        row.push({
                            value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                            functions_change: false

                        });
                    } else {
                        row.push({
                            value: "",
                            functions_change: false
                        });
                    }

                    value.body_value.push(row);
                }
            }

            container.sections[3].properties.push(tracing_property);
        }

        return container;
    }
});

//----------------------------------------------------------------------------------------
var BaseNodeUi = Class.create(Ws.Graph.DefaultNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    },

    repaint: function($super) {
        $super();

        this.__paint_tracing_data();
    //    this.__paint_debugging_data();
    //    this.__paint_test_ranner();
    },

    get_inbound_connectors: function() {
        var connectors = this.node.get_inbound_connectors();

        if (connectors.length > 0) {
            connectors[0].x = this.margin;
            connectors[0].y = this.height / 2;
            connectors[0].type = Ws.Graph.NODE_CONNECTOR_WEST;
        }

        return connectors;
    },

    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();

        if (connectors.length > 0) {
            connectors[0].x = this.width - this.margin;
            connectors[0].y = this.height / 2;
            connectors[0].type = Ws.Graph.NODE_CONNECTOR_EAST;
        }

        return connectors;
    },

    check_point_node_path: function(x, y) {
        var A_x = this.margin, A_y = this.margin;
        var C_x = this.width - this.margin, C_y = this.height - this.margin;

        return (x >= A_x) && (x <= C_x) && (y >= A_y) && (y <= C_y);
    },

    sl_accept_selection: function($super, x, y, event) {
        if ($super(x, y, event)) {
            return true;
        }

        // monitoring messages
        if (tracing_enabled) {
            var point = this.__get_point_relative_to_node(x, y);

            if (this.__is_selected) {
                var x1 = this.margin - 5;
                var y1 = this.margin - 5;

                var x2 = this.width - this.margin - 5;
                var y2 = this.margin - 5;

                if ((point.x > x1) && (point.x < x1 + 10) &&
                    (point.y > y1) && (point.y < y1 + 10)) {

                    return true;
                }

                if ((point.x > x2) && (point.x < x2 + 10) &&
                    (point.y > y2) && (point.y < y2 + 10)) {

                    return true;
                }
            }
        }
        return false;
    },

    cl_accept_click: function($super, x, y, event) {
        if ($super(x, y, event)) {
            return true;
        }

        var point = this.__get_point_relative_to_node(x, y);

        var x1, y1, x2, y2;

        // run test
        x1 = this.margin + 17;
        y1 = this.margin - 16;
        if ((point.x > x1) && (point.x < x1 + 11) &&
            (point.y > y1) && (point.y < y1 + 16)) {
            return true;
        }

        if (tracing_enabled) {
            x1 = this.margin - 21;
            y1 = this.margin + 3;

            x2 = this.width - this.margin + 5;
            y2 = this.margin + 3;

            if ((point.x > x1) && (point.x < x1 + 16) &&
                (point.y > y1) && (point.y < y1 + 16)) {

                return true;
            }

            if ((point.x > x2) && (point.x < x2 + 16) &&
                (point.y > y2) && (point.y < y2 + 16)) {

                return true;
            }
        }

        if (this.__is_selected && debugging_enabled) {
            x1 = this.margin - 5;
            y1 = this.margin - 5;

            x2 = this.width - this.margin - 5;
            y2 = this.margin - 5;

            if ((point.x > x1) && (point.x < x1 + 10) &&
                (point.y > y1) && (point.y < y1 + 10)) {

                return true;
            }

            if ((point.x > x2) && (point.x < x2 + 10) &&
                (point.y > y2) && (point.y < y2 + 10)) {

                return true;
            }
        }

        return false;
    },

    cl_handle_click: function($super, x, y, event) {
        var options = {};
        if (tracing_enabled) {
            options.functions_change = [{funct: create_tracing_message_dialog, type: "properties"}]
        }
        $super(x, y, event, options);

        var point = this.__get_point_relative_to_node(x, y);

        var x1, y1, x2, y2;

        // run test
        x1 = this.margin + 17;
        y1 = this.margin - 16;
        if ((point.x > x1) && (point.x < x1 + 11) &&
            (point.y > y1) && (point.y < y1 + 16))
        {
        //    alert("run test");
            show_message_dialog_for_run_simple_test(this.node.id);
            return;
        }

        if (tracing_enabled) {
            x1 = this.margin - 21;
            y1 = this.margin + 3;

            x2 = this.width - this.margin + 5;
            y2 = this.margin + 3;

            if ((point.x > x1) && (point.x < x1 + 16) &&
                (point.y > y1) && (point.y < y1 + 16)) {

                create_tracing_messages_list_dialog(this.node.id, true);
            }

            if ((point.x > x2) && (point.x < x2 + 16) &&
                (point.y > y2) && (point.y < y2 + 16)) {

                create_tracing_messages_list_dialog(this.node.id, false);
            }
        }

        if (this.__is_selected && debugging_enabled) {
            x1 = this.margin - 5;
            y1 = this.margin - 5;

            x2 = this.width - this.margin - 5;
            y2 = this.margin - 5;

            if (!debugging_breakpoints[this.node.id]) {
                debugging_breakpoints[this.node.id] = {
                    inbound: false,
                    outbound: false
                }
            }

            if ((point.x > x1) && (point.x < x1 + 10) &&
                (point.y > y1) && (point.y < y1 + 10)) {

                if (debugging_breakpoints[this.node.id].inbound) {
                    debugging_breakpoints[this.node.id].inbound = false;
                } else {
                    debugging_breakpoints[this.node.id].inbound = true;
                }
            }

            if ((point.x > x2) && (point.x < x2 + 10) &&
                (point.y > y2) && (point.y < y2 + 10)) {

                if (debugging_breakpoints[this.node.id].outbound) {
                    debugging_breakpoints[this.node.id].outbound = false;
                } else {
                    debugging_breakpoints[this.node.id].outbound = true;
                }
            }

            this.repaint();
        }
    },

    __paint_tracing_data: function() {
        if (!tracing_enabled && !debugging_enabled) {
            return;
        }

        if (!tracing_data[this.node.id]) {
            tracing_data[this.node.id] = {
                inbound: [],
                outbound: []
            }
        }

        var url, image, x, y, self = this, need_repaint = false;

        if (tracing_data[this.node.id].inbound.length > 0) {
            if (tracing_data[this.node.id].inbound.length > 1) {
                if (tracing_data[this.node.id].inbound.has_fault) {
                    url = "resource?img/ui/tracing/message_fault_queue_16.png";
                } else {
                url = "resource?img/ui/tracing/message_queue_16.png";
                }

            } else {
                if (tracing_data[this.node.id].inbound.has_fault) {
                    url = "resource?img/ui/tracing/message_fault_16.png";
                } else {
                url = "resource?img/ui/tracing/message_16.png";
            }
            }

            image = Ws.ImagesLoadManager.get(url);
            if (image != null) {
                x = this.margin - 21;
                y = this.margin + 3;

                if (debugging_enabled && (debugging_current_message.service == this.node.id) && debugging_current_message.inbound) {
                    this.context.beginPath();
                    this.context.arc(x + 8, y + 8, 15, 0, 2 * Math.PI, false);

                    this.context.fillStyle = "rgba(255, 255, 0, 1.0)";
                    this.context.fill();

                    // Reset path.
                    this.context.beginPath();
                    this.context.closePath();
                } else {
                    if (tracing_data[this.node.id].inbound.has_new) {
                        this.__inbound_queue_highlight_opacity = 1.0;
                        this.__inbound_queue_highlight_radius = 15;
                    }

                    if (this.__inbound_queue_highlight_opacity > 0.1) {
                        this.context.beginPath();
                        this.context.arc(x + 8, y + 8, this.__inbound_queue_highlight_radius, 0, 2 * Math.PI, false);

                        this.context.fillStyle = "rgba(255, 255, 0, " + this.__inbound_queue_highlight_opacity + ")";
                        this.context.fill();

                        // Reset path.
                        this.context.beginPath();
                        this.context.closePath();

                        this.__inbound_queue_highlight_opacity -= 0.1;
                        this.__inbound_queue_highlight_radius -= 0.5;

                        need_repaint = true;
                    }
                }

                this.context.drawImage(image, x, y, 16, 16);

                tracing_data[this.node.id].inbound.has_new = false;
            } else {
                Ws.ImagesLoadManager.load(url, function() {
                    self.repaint();
                });
            }
        }

        if (tracing_data[this.node.id].outbound.length > 0) {
            if (tracing_data[this.node.id].outbound.length > 1) {
                if (tracing_data[this.node.id].outbound.has_fault) {
                    url = "resource?img/ui/tracing/message_fault_queue_16.png";
                } else {
                url = "resource?img/ui/tracing/message_queue_16.png";
                }
            } else {
                if (tracing_data[this.node.id].outbound.has_fault) {
                    url = "resource?img/ui/tracing/message_fault_queue_16.png";
                } else {
                url = "resource?img/ui/tracing/message_16.png";
            }
            }

            image = Ws.ImagesLoadManager.get(url);
            if (image != null) {
                x = this.width - this.margin + 5;
                y = this.margin + 3;

                if (debugging_enabled &&
                    (debugging_current_message.service == this.node.id) &&
                    !debugging_current_message.inbound) {

                    this.context.beginPath();
                    this.context.arc(x + 8, y + 8, 15, 0, 2 * Math.PI, false);

                    this.context.fillStyle = "rgba(255, 255, 0, 1.0)";
                    this.context.fill();

                    // Reset path.
                    this.context.beginPath();
                    this.context.closePath();
                } else {
                    if (tracing_data[this.node.id].outbound.has_new) {
                        this.__outbound_queue_highlight_opacity = 1.0;
                        this.__outbound_queue_highlight_radius = 15;
                    }

                    if (this.__outbound_queue_highlight_opacity > 0.1) {
                        this.context.beginPath();
                        this.context.arc(x + 8, y + 8, this.__outbound_queue_highlight_radius, 0, 2 * Math.PI, false);

                        this.context.fillStyle = "rgba(255, 255, 0, " + this.__outbound_queue_highlight_opacity + ")";
                        this.context.fill();

                        // Reset path.
                        this.context.beginPath();
                        this.context.closePath();

                        this.__outbound_queue_highlight_opacity -= 0.1;
                        this.__outbound_queue_highlight_radius -= 0.5;

                        need_repaint = true;
                    }
                }

                this.context.drawImage(image, x, y, 16, 16);

                tracing_data[this.node.id].outbound.has_new = false;
            } else {
                Ws.ImagesLoadManager.load(url, function() {
                    self.repaint();
                });
            }
        }

        if (need_repaint) {
            setTimeout(function() {
                self.repaint();
            }, 50);
        }
    },

    __check_link_vs_connector: function($super, link, connector, connector_index, is_inbound) {
        if (!$super(link, connector, connector_index, is_inbound)) {
            return false;
        }

        // Now we need to check a bunch of things.
        // First, if we are an EIP and the link is inbound and the node on the other side of
        // the link is a CBR or Wire Tap (its 1st connector), we do not want to connect.
        // Second, if we're a regular node and we do have outbound links, and the link is inbound,
        // and there is a CBR or Wire Tap o the other end, we do not want to connect either.
        if (is_inbound && link.start_node && (link.start_node.type == "select" ||
                (link.start_node.type == "tee" && link.start_node_connector == 1))) {

            if ((this.node.type == "broadcast") || (this.node.type == "split") ||
                    (this.node.type == "aggregate") || (this.node.type == "select") ||
                    (this.node.type == "tee") || (this.node.type == "filter")) {

                return false;
            }

            if (this.node.get_outbound_links().length > 0) {
                return false;
            }
        }

        return true;
    },

    noop: function() {
        // Does nothing, a placeholder.
    }
});

//----------------------------------------------------------------------------------------
var ServiceNode = Class.create(BaseNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);
    }
});

//----------------------------------------------------------------------------------------
var ServiceNodeUi = Class.create(BaseNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
var AdapterNode = Class.create(BaseNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);
    },

    get_inbound_connectors: function() {
        if (this.properties.direction != "out-only") {
            return [
                {
                    max_links: 1
                }
            ];
        } else {
            return [];
        }
    },

    get_outbound_connectors: function() {
        if (this.properties.direction != "in-only") {
            return [
                {
                    max_links: 1
                }
            ];
        } else {
            return [];
        }
    }
});

//----------------------------------------------------------------------------------------
var AdapterNodeUi = Class.create(BaseNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
        
        this.skew_span = 3;
        this.skew_depth = 10;
        this.ear_distance = 7;
        
        this.width = this.width + (2 * this.skew_depth) + (2 * this.ear_distance);
        this.canvastext_text_offset_x = this.width / 2;
        this.browser_text_offset_x = this.width / 2;
        this.icon_offset_x = this.icon_offset_x + this.skew_depth + this.ear_distance;
        this.toolbar_x = this.toolbar_x + this.skew_depth + this.ear_distance;
        this.canvastext_text_margin = 20;
        this.html_div_text_margin = 20;
        this.browser_text_margin = 20;
    },

    prepare_path: function() {
        //             A1
        //  /| A----B |  \ B1
        // | |-|    |-|    |
        //  \| D----C |  / C1
        //             D1

        var A_x = this.margin + this.skew_depth + this.ear_distance, A_y = this.margin;
        var B_x = this.width - this.margin - this.skew_depth - this.ear_distance, B_y = this.margin;
        var C_x = this.width - this.margin - this.skew_depth - this.ear_distance, C_y = this.height - this.margin;
        var D_x = this.margin + this.skew_depth + this.ear_distance, D_y = this.height - this.margin;

        this.context.beginPath();

        var direction = this.node.properties.direction;

        // left trapezoid
        if (!direction || (direction == "in-only") || (direction == "in-out")) {
            this.context.moveTo(A_x, (A_y + D_y) / 2);
            this.context.lineTo(A_x - this.ear_distance, (A_y + D_y) / 2);
            this.context.lineTo(A_x - this.ear_distance, A_y);
            this.context.lineTo(A_x - this.ear_distance - this.skew_depth, A_y + this.skew_span);
            this.context.lineTo(D_x - this.ear_distance - this.skew_depth, D_y - this.skew_span);
            this.context.lineTo(D_x - this.ear_distance, D_y);
            this.context.lineTo(A_x - this.ear_distance, (A_y + D_y) / 2);
        }

        // rectangle
        this.context.moveTo(A_x, A_y);
        this.context.lineTo(B_x, B_y);
        this.context.lineTo(C_x, C_y);
        this.context.lineTo(D_x, D_y);
        this.context.lineTo(A_x, A_y);

        // right trapezoid
        if (!direction || (direction == "out-only") || (direction == "in-out")) {

            this.context.moveTo(B_x, (B_y + C_y) / 2);
            this.context.lineTo(B_x + this.ear_distance, (B_y + C_y) / 2);
            this.context.lineTo(B_x + this.ear_distance, B_y);
            this.context.lineTo(B_x + this.ear_distance + this.skew_depth, B_y + this.skew_span);
            this.context.lineTo(C_x + this.ear_distance + this.skew_depth, C_y - this.skew_span);
            this.context.lineTo(C_x + this.ear_distance, C_y);
            this.context.lineTo(C_x + this.ear_distance, (B_y + C_y) / 2);
        }
    },

    check_point_node_path: function($super, x, y) {
        if (!$super(x, y)) {
            return false;
        }

        var A_x = this.margin + this.skew_depth + this.ear_distance, A_y = this.margin;
        var C_x = this.width - this.margin - this.skew_depth - this.ear_distance, C_y = this.height - this.margin;
        // check rectangle
        if ((x >= A_x) && (x <= C_x) && (y >= A_y) && (y <= C_y)) {
            return true;
        }

        var B_x = this.width - this.margin - this.skew_depth - this.ear_distance, B_y = this.margin;
        var D_x = this.margin + this.skew_depth + this.ear_distance, D_y = this.height - this.margin;
        var direction = this.node.properties.direction;

        // right trapezoid
        if (!direction || (direction == "out-only") || (direction == "in-out")) {
            var A1_x = B_x + this.ear_distance;
        
            if (y == (B_y + C_y) / 2 && x > Bx && x < A1_x) {
                return true;
            }

            var B1_x = B_x + this.ear_distance + this.skew_depth;
            var B1_y = B_y + this.skew_span;
            var C1_x = B1_x;
            var C1_y = C_y - this.skew_span;
            var D1_x = C_x + this.ear_distance;
            var D1_y = C_y;
            var A1_y = B_y;

            if ((x > A1_x) && (x < B1_x)) {
                // rectangle
                if (y > B1_y && y < C1_y) {
                    return true;
                }
                // triangle
                if (y >= ((B1_y - A1_y) * x + (A1_y * B1_x - A1_x * B1_y)) / (B1_x - A1_x) &&
                    y <= ((C1_y - D1_y) * x + (D1_y * C1_x - D1_x * C1_y)) / (C1_x - D1_x) ) {
                    return true;
                }
            }
        }

        // left trapezoid
        if (!direction || (direction == "in-only") || (direction == "in-out")) {
            var A1_x = A_x - this.ear_distance;

            if (y == (A_y + D_y) / 2 && x > Bx && x < A1_x) {
                return true;
            }

            var B1_x = A_x - this.ear_distance - this.skew_depth;
            var B1_y = A_y + this.skew_span;
            var C1_x = B1_x;
            var C1_y = D_y - this.skew_span;
            var D1_x = D_x - this.ear_distance;
            var A1_y = B_y;
            var D1_y = D_y;

            if ((x > B1_x) && (x < A1_x)) {
                // rectangle
                if (y > B1_y && y < C1_y) {
                    return true;
                }
                // triangle
                if (y >= ((B1_y - A1_y) * x + (A1_y * B1_x - A1_x * B1_y)) / (B1_x - A1_x) &&
                    y <= ((C1_y - D1_y) * x + (D1_y * C1_x - D1_x * C1_y)) / (C1_x - D1_x) ) {
                    return true;
                }
            }
        }

        return false;
    }
});

//----------------------------------------------------------------------------------------
var EipNode = Class.create(BaseNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);
    },

    pc_get_descriptors: function($super) {
        var super_container = $super();
        super_container.properties = [];
        super_container.sections = null;

        return super_container;
    },

    noop: function() {
        // Does nothing.
    }
});

//----------------------------------------------------------------------------------------
var EipNodeUi = Class.create(BaseNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    },

    check_point_node_path: function($super, x, y) {
        if (this.context.isPointInPath) {
            this.prepare_path();
            return this.context.isPointInPath(x, y);
        }
        
        return $super(x, y);
    },

    noop: function() {
        // Does nothing.
    }
});
