/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Tooltip = Class.create({
    element: null,
    timer : null,
    rr_options : null,

    initialize: function(option, element, rr_options) {
        if (!option.html_tooltip) {
            alert("bed option");
            return;
        }

        this.element = element;

        var element_tooltip = new Element("div");
        element_tooltip.addClassName("tooltip");
        document.body.appendChild(element_tooltip);

        var content = new Element("div");
     //   content.addClassName("tooltip");
        content.addClassName("content");
        element_tooltip.appendChild(content);
        this.content = content;

        this.canvas = Ws.Utils.create_canvas();
//        this.canvas.setStyle({
//            height: "0px",
//            width: "0px"
//        });
        element_tooltip.appendChild(this.canvas);

        this.element_tooltip = element_tooltip;
        this.element_tooltip.ws_tooltip = this;
        this.element_tooltip.id = "id_tooltip_" + Ws.Tooltip.counter++;
         
        if (!rr_options || !rr_options.fill) {
            rr_options = {
                fill : Ws.Tooltip.DEFAULT_COLOR
            };
        }

        this.set_html_tooltip(option.html_tooltip);
        Object.extend(this, option);
        this.element_tooltip.hide();

        element_tooltip.onmouseover = function() {
            if (element_tooltip.ws_tooltip.skip_over) {
                element_tooltip.ws_tooltip.skip_over = false;
                return;
            }
            element_tooltip.hide();
            if (element_tooltip.ws_tooltip.timer) {
                clearTimeout(element_tooltip.ws_tooltip.timer);
            }
        }

        this.element.onmouseover = function(event) {
            if (element_tooltip.ws_tooltip.skip_over) {
                element_tooltip.ws_tooltip.skip_over = false;
                return;
            }
            element_tooltip.hide();
            if (element_tooltip.ws_tooltip.timer) {
                clearTimeout(element_tooltip.ws_tooltip.timer);
            }
            if (element_tooltip.ws_tooltip.accept_point &&
                !element_tooltip.ws_tooltip.accept_point(event.clientX, event.clientY)) {
                return;
            }
            element_tooltip.ws_tooltip.timer = setTimeout(function() {
                element_tooltip.ws_tooltip.show(event);
            }, Ws.Tooltip.SLEEP_TIMEOUT);
        }

        this.element.onmousemove = function(event) {
            element_tooltip.hide();
            if (element_tooltip.ws_tooltip.timer) {
                clearTimeout(element_tooltip.ws_tooltip.timer);
            }
            if (element_tooltip.ws_tooltip.accept_point &&
                !element_tooltip.ws_tooltip.accept_point(event.clientX, event.clientY)) {
                return;
            }
            element_tooltip.ws_tooltip.timer = setTimeout(function() {
                element_tooltip.ws_tooltip.show(event);
            }, Ws.Tooltip.SLEEP_TIMEOUT);
        }

        this.element.onmouseout = function() {
            element_tooltip.hide();
            if (element_tooltip.ws_tooltip.timer) {
                clearTimeout(element_tooltip.ws_tooltip.timer);
            }
        }
    },

    show : function(event) {
        //    var element_x = parseInt(this.element.getStyle("left"));
        //    var element_y = parseInt(this.element.getStyle("top"));
        var x = 0;
        var y = 0;
        if (this.position) {
            x = this.position.x;
            y = this.position.y;
        } else {
            x = event.clientX;
            y = event.clientY;
        //         x = event.clientX - element_x;
        //         y = event.clientY - element_y;
        }

        if (this.orientation) {
            if (this.orientation.vertical == Ws.Tooltip.ORIENTATION_TOP) {
                y = y - this.height;
            }
            if (this.orientation.vertical == Ws.Tooltip.ORIENTATION_BOTTOM) {
                y += 10;
            }
            if (this.orientation.horizontal == Ws.Tooltip.ORIENTATION_LEFT) {
                x = x - this.width;
            }
            if (this.orientation.horizontal == Ws.Tooltip.ORIENTATION_RIGHT) {
                x += 10;
            }
        } else {
            x += 10;
            y += 10;
        }

        var dimension = Ws.Utils.get_window_dimensions();
        if (x <= 0) {
            //x = -element_x;
            x = x + this.width + 10;
        }
        if (y <= 0) {
            //y = -element_y;
            y = y + this.height + 10;
        }
        if (x + this.width > dimension.width) {
            x = x - this.width - 10;
        //x = dimension.width - element_x - this.width;
        }
        if (y + this.height > dimension.height) {
            y = y - this.height - 10;
        //y = dimension.height - element_y - this.height;
        }
        var z_ = parseInt(this.element.getStyle("zIndex"));
        if (!z_) {
            z_ = 0;
        }
        this.element_tooltip.setStyle({
            top : y + "px",
            left : x + "px",
            zIndex : z_ + 1000000
        });
        this.element_tooltip.show();
        this.skip_over = true;
    },

    hide : function() {
        this.element_tooltip.hide();
        this.skip_over = false;
        if (this.timer) {
            clearTimeout(this.timer);
        }
    },

    set_html_tooltip : function(html_tooltip) {
        if (html_tooltip == this.html_tooltip) {
            return;
        }

        this.html_tooltip = html_tooltip;
        this.content.innerHTML = html_tooltip;

        var temp_div = new Element("div");
        temp_div.innerHTML = this.html_tooltip;
        temp_div.addClassName("tooltip");
        temp_div.setStyle({
            position : "absolute",
            top : "-300px",
            left : "-300px"
        });
        document.body.appendChild(temp_div);
        
        this.height = temp_div.offsetHeight;
        this.width = temp_div.offsetWidth;
        temp_div.parentNode.removeChild(temp_div);

        var bmp_tooltip = Ws.Utils.get_bmp(this.content);

        this.canvas.width = this.width + bmp_tooltip.get_bp_horizontal();
        this.canvas.height = this.height + bmp_tooltip.get_bp_vertical();
        this.canvas.setStyle({
            height: this.canvas.height + "px",
            width: this.canvas.width + "px"
        });
        this.content.setStyle({
            height: this.height + "px",
            width: this.width + "px"
        });
        this.render();
    },

    render: function() {
        //  (x0,y0) ------- (x1,y0)
        //     |               |
        //     |               |
        //  (x0,y1) ------- (x1,y1)
        var context = this.canvas.getContext("2d");
        var r = Ws.Tooltip.DEFAULT_RADIUS;
        var x0 = 1;
        var y0 = 1;
        var x1 = x0 + this.canvas.width - 2;
        var y1 = y0 + this.canvas.height - 2;
       
        context.fillStyle = Ws.Tooltip.DEFAULT_COLOR;
        context.strokeStyle = Ws.Tooltip.DEFAULT_COLOR_BORDER;
        context.lineWidth = 2;
//        draw for bezierCurveTo
//        var d = 0.4477 * r;
//        context.moveTo(x0 + r, y0);
//        context.lineTo(x1 - r, y0);
//        context.bezierCurveTo(x1 - d, y0, x1, y0 + d, x1, y0 + r);
//        context.lineTo(x1, y1 - r);
//        context.bezierCurveTo(x1, y1 - d, x1 - d, y1, x1 - r, y1);
//        context.lineTo(x0 + r, y1);
//        context.bezierCurveTo(x0 + d, y1, x0, y1 - d, x0, y1 - r);
//        context.lineTo(x0, y0 + r);
//        context.bezierCurveTo(x0, y0 + d, x0 + d, y0, x0 + r, y0);
//        context.closePath();

//        draw for quadraticCurveTo
        context.beginPath();
        context.moveTo(x0 + r, y0);
        context.lineTo(x1 - r, y0);
        context.quadraticCurveTo(x1, y0, x1, y0 + r);
        context.lineTo(x1, y1 - r);
        context.quadraticCurveTo(x1, y1, x1 - r, y1);
        context.lineTo(x0 + r, y1);
        context.quadraticCurveTo(x0, y1, x0, y1 - r);
        context.lineTo(x0, y0 + r);
        context.quadraticCurveTo(x0, y0, x0 + r, y0);
        context.closePath();
        context.stroke();
        context.fill();
    },
    
    noop : function() {

    }
});

Ws.Tooltip.counter = 0;

Ws.Tooltip.ORIENTATION_TOP = "top";
Ws.Tooltip.ORIENTATION_BOTTOM = "bottom";
Ws.Tooltip.ORIENTATION_LEFT = "left";
Ws.Tooltip.ORIENTATION_RIGHT = "right";

Ws.Tooltip.SLEEP_TIMEOUT = 400;

Ws.Tooltip.DEFAULT_COLOR = "rgba(255, 255, 225, 0.90)";
Ws.Tooltip.DEFAULT_COLOR_BORDER = "rgba(70, 70, 160, 1)";

Ws.Tooltip.DEFAULT_RADIUS = 6.0;
