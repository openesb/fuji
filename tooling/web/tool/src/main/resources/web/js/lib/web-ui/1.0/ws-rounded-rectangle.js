/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.RoundedRectangle = Class.create({
    fill: "rgba(0, 0, 0, 0.85)",
    stroke: null,
    stroke_width: 0,
    
    tl_fill: null,
    tl_stroke: null,
    tl_stroke_width: null,
    
    t_fill: null,
    t_stroke: null,
    t_stroke_width: null,
    
    tr_fill: null,
    tr_stroke: null,
    tr_stroke_width: null,
    
    l_fill: null,
    l_stroke: null,
    l_stroke_width: null,
    
    c_fill: null,
    
    r_fill: null,
    r_stroke: null,
    r_stroke_width: null,
    
    bl_fill: null,
    bl_stroke: null,
    bl_stroke_width: null,
    
    b_fill: null,
    b_stroke: null,
    b_stroke_width: null,
    
    br_fill: null,
    br_stroke: null,
    br_stroke_width: null,
    
    half_round: null,
    
    radius: 8,
    
    t_size: 0,
    l_size: 0,
    r_size: 0,
    b_size: 0,
    
    background_z_index: 10,
    contents_z_index: 100,
    
    element: null,
    
    contents_classname: "-wsrr-contents",

    initialize: function(element, options) {
        // If some custom options were passed through the constructor arguments, apply ---
        // them.
        if (options) {
            Object.extend(this, options);
        }
        
        // Initialize the contents element -- when the rectangle is attached to an -------
        // element (see below), the contents will serve as the container for the
        // original element's children.
        this.__ce = new Element("div");
        this.__ce.addClassName(this.contents_classname);
        this.__ce.makePositioned();
        this.__ce.setStyle({
            width: "100%",
            height: "100%",

            margin: "0px 0px 0px 0px",
            padding: "0px 0px 0px 0px"
        });

        // Initialize the background mosaic elements, in tota there will be 10 of --------
        // them the wrapper, and the nine actually forming the mosaic: 4 corners,
        // 4 "borders" and one center.
        this.__bge = new Element("div");
        this.__bge_tl = new Element("div");
        this.__bge_t = new Element("div");
        this.__bge_tr = new Element("div");
        this.__bge_l = new Element("div");
        this.__bge_c = new Element("div");
        this.__bge_r = new Element("div")
        this.__bge_bl = new Element("div");
        this.__bge_b = new Element("div")
        this.__bge_br = new Element("div");
        
        this.__bge_tl.appendChild(Ws.Utils.create_canvas());
        this.__bge_t.appendChild(Ws.Utils.create_canvas());
        this.__bge_tr.appendChild(Ws.Utils.create_canvas());
        this.__bge_l.appendChild(Ws.Utils.create_canvas());
        this.__bge_c.appendChild(Ws.Utils.create_canvas());
        this.__bge_r.appendChild(Ws.Utils.create_canvas());
        this.__bge_bl.appendChild(Ws.Utils.create_canvas());
        this.__bge_b.appendChild(Ws.Utils.create_canvas());
        this.__bge_br.appendChild(Ws.Utils.create_canvas());

        this.__bge.appendChild(this.__bge_tl);
        this.__bge.appendChild(this.__bge_t);
        this.__bge.appendChild(this.__bge_tr);
        this.__bge.appendChild(this.__bge_l);
        this.__bge.appendChild(this.__bge_c);
        this.__bge.appendChild(this.__bge_r);
        this.__bge.appendChild(this.__bge_bl);
        this.__bge.appendChild(this.__bge_b);
        this.__bge.appendChild(this.__bge_br);
        
        // Attach the rectangle to the element, layout the background and the ------------
        // element itself and then repaint the background (both done automatically when
        // attaching.)
        this.attach(element);
    },

    attach: function(element, skip_repaint) {
        if (this.element) {
            this.detach();
        }

        if (element.ws_rrectangle) {
            element.ws_rrectangle.detach();
        }

        Element.extend(element);

        this.element = element;
        this.element.ws_rrectangle = this;

        while (this.element.hasChildNodes()) {
            this.__ce.appendChild(
                    this.element.removeChild(this.element.firstChild));
        }

        this.element.appendChild(this.__ce);
        this.element.appendChild(this.__bge);

        this.relayout();

        if (!skip_repaint) {
            this.repaint();
        }
    },

    detach: function() {
        if (!this.element) {
            return;
        }

        while (this.__ce.hasChildNodes()) {
            this.element.appendChild(
                    this.__ce.removeChild(this.__ce.firstChild));
        }

        this.element.removeChild(this.__ce);
        this.element.removeChild(this.__bge);

        this.element.ws_rrectangle = null;
        this.element = null;
    },

    relayout: function() {
        // Init the stroke size (half-stroke size, actually) and the sizes of the --------
        // background "borders".
        var hs = this.stroke_width ? parseInt(this.stroke_width / 2) : 0;

        this.__bg_ls =
                this.l_size > this.radius + hs ? this.l_size : this.radius + hs;
        this.__bg_ts =
                this.t_size > this.radius + hs ? this.t_size : this.radius + hs;
        this.__bg_rs =
                this.r_size > this.radius + hs ? this.r_size : this.radius + hs;
        this.__bg_bs =
                this.b_size > this.radius + hs ? this.b_size : this.radius + hs;

        // Position the contents element and the mosaic on the z-axis. -------------------
        this.__ce.setStyle({
            zIndex: this.contents_z_index
        });
        this.__bge.setStyle({
            zIndex: this.background_z_index
        });

        // Correct the sizes and visibility of background elements if the ----------------
        // rectangle is set to be half round.
        if (this.half_round != null) {
            switch (this.half_round) {
                case "top":
                    // Hide the top three background tiles
                    this.__bge_tl.setStyle({
                        display: "none"
                    });
                    this.__bge_t.setStyle({
                        display: "none"
                    });
                    this.__bge_tr.setStyle({
                        display: "none"
                    });

                    // Set the top-border size to 0
                    this.__bg_ts = 0;

                    break;
                case "right":
                    // Hide the right three background tiles
                    this.__bge_tr.setStyle({
                        display: "none"
                    });
                    this.__bge_r.setStyle({
                        display: "none"
                    });
                    this.__bge_br.setStyle({
                        display: "none"
                    });

                    // Set the top-border size to 0
                    this.__bg_rs = 0;

                    break;
                case "bottom":
                    // Hide the bottom three background tiles
                    this.__bge_bl.setStyle({
                        display: "none"
                    });
                    this.__bge_b.setStyle({
                        display: "none"
                    });
                    this.__bge_br.setStyle({
                        display: "none"
                    });

                    // Set the top-border size to 0
                    this.__bg_bs = 0;

                    break;
                case "left":
                    // Hide the left three background tiles
                    this.__bge_tl.setStyle({
                        display: "none"
                    });
                    this.__bge_l.setStyle({
                        display: "none"
                    });
                    this.__bge_bl.setStyle({
                        display: "none"
                    });

                    // Set the top-border size to 0
                    this.__bg_ls = 0;

                    break;
                default:
                    // Do nothing
            }
        }

        // Define the size of the target element. ----------------------------------------
        this.element.makePositioned();

        var element_bmp = Ws.Utils.get_bmp(this.element);

        var width = Element.getWidth(this.element);
        var height = Element.getHeight(this.element)

        if (width < this.__bg_ls + this.__bg_rs) {
            width = this.__bg_ls + this.__bg_rs;
        }

        if (height < this.__bg_ts + this.__bg_bs) {
            height = this.__bg_ts + this.__bg_bs;
        }

        this.element.setStyle({
            width: (width - element_bmp.get_bp_horizontal()) + "px",
            height: (height - element_bmp.get_bp_vertical()) + "px"
        });

        // Get the new offset width and height, then layout the background mosaic --------
        width = Element.getWidth(this.element);
        height = Element.getHeight(this.element);

        // Top-left corner ---------------------------------------------------------------
        this.__bge_tl.setStyle({
            position: "absolute",
            top: "0px",
            left: "0px",
            width: this.__bg_ls + "px",
            height: this.__bg_ts + "px",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Top "border" ------------------------------------------------------------------
        this.__bge_t.setStyle({
            position: "absolute",
            top: "0px",
            left: this.__bg_ls + "px",
            width: (width - this.__bg_ls - this.__bg_rs) + "px",
            height: this.__bg_ts + "px",
            overflow: "hidden",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Top-right corner --------------------------------------------------------------
        this.__bge_tr.setStyle({
            position: "absolute",
            top: "0px",
            right: "0px",
            width: this.__bg_rs + "px",
            height: this.__bg_ts + "px",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Left "border" -----------------------------------------------------------------
        this.__bge_l.setStyle({
            position: "absolute",
            top: this.__bg_ts + "px",
            left: "0px",
            width: this.__bg_ls + "px",
            height: (height - this.__bg_ts - this.__bg_bs) + "px",
            overflow: "hidden",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Center fill -------------------------------------------------------------------
        this.__bge_c.setStyle({
            position: "absolute",
            top: this.__bg_ts + "px",
            left: this.__bg_ls + "px",
            width: (width - this.__bg_ls - this.__bg_rs) + "px",
            height: (height - this.__bg_ts - this.__bg_bs) + "px",
            overflow: "hidden",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Right "border" ----------------------------------------------------------------
        this.__bge_r.setStyle({
            position: "absolute",
            top: this.__bg_ts + "px",
            right: "0px",
            width: this.__bg_rs + "px",
            height: (height - this.__bg_ts - this.__bg_bs) + "px",
            overflow: "hidden",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Bottom-left corner ------------------------------------------------------------
        this.__bge_bl.setStyle({
            position: "absolute",
            bottom: "0px",
            left: "0px",
            width: this.__bg_ls + "px",
            height: this.__bg_bs + "px",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Bottom "border" ---------------------------------------------------------------
        this.__bge_b.setStyle({
            position: "absolute",
            bottom: "0px",
            left: this.__bg_ls + "px",
            width: (width - this.__bg_ls - this.__bg_rs) + "px",
            height: this.__bg_bs + "px",
            overflow: "hidden",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Bottom-right corner -----------------------------------------------------------
        this.__bge_br.setStyle({
            position: "absolute",
            bottom: "0px",
            right: "0px",
            width: this.__bg_rs + "px",
            height: this.__bg_bs + "px",
            fontSize: "0px",
            lineHeight: "0px"
        });

        // Background container ----------------------------------------------------------
        this.__bge.setStyle({
            position: "absolute",
            top: "0px",
            left: "0px",
            width: width + "px",
            height: height + "px",
            fontSize: "0px",
            lineHeight: "0px"
        });
    },

    repaint: function() {
        // Init the graphics: top-left corner --------------------------------------------
        Ws.Utils.draw_rr_top_left(
            this.__bge_tl.firstChild,
            this.__bg_ls,
            this.__bg_ts,
            this.radius,
            this.tl_fill ? this.tl_fill : this.fill,
            this.tl_stroke ? this.tl_stroke : this.stroke,
            this.tl_stroke_width ? this.tl_stroke_width : this.stroke_width);

        // Init the graphics: top "border" -----------------------------------------------
        Ws.Utils.draw_rr_top(
            this.__bge_t.firstChild,
            this.__bg_ts,
            this.t_fill ? this.t_fill : this.fill,
            this.t_stroke ? this.t_stroke : this.stroke,
            this.t_stroke_width ? this.t_stroke_width : this.stroke_width);

        // Init the graphics: top-right corner -------------------------------------------
        Ws.Utils.draw_rr_top_right(
            this.__bge_tr.firstChild,
            this.__bg_rs,
            this.__bg_ts,
            this.radius,
            this.tr_fill ? this.tr_fill : this.fill,
            this.tr_stroke ? this.tr_stroke : this.stroke,
            this.tr_stroke_width ? this.tr_stroke_width : this.stroke_width);

        // Init the graphics: left "border" ----------------------------------------------
        Ws.Utils.draw_rr_left(
            this.__bge_l.firstChild,
            this.__bg_ls,
            this.l_fill ? this.l_fill : this.fill,
            this.l_stroke ? this.l_stroke : this.stroke,
            this.l_stroke_width ? this.l_stroke_width : this.stroke_width);

        // Init the graphics: center fill ------------------------------------------------
        Ws.Utils.draw_rr_center(
            this.__bge_c.firstChild,
            this.c_fill ? this.c_fill : this.fill);

        // Init the graphics: right "border" ---------------------------------------------
        Ws.Utils.draw_rr_right(
            this.__bge_r.firstChild,
            this.__bg_rs,
            this.r_fill ? this.r_fill : this.fill,
            this.r_stroke ? this.r_stroke : this.stroke,
            this.r_stroke_width ? this.r_stroke_width : this.stroke_width);

        // Init the graphics: bottom-left corner -----------------------------------------
        Ws.Utils.draw_rr_bottom_left(
            this.__bge_bl.firstChild,
            this.__bg_ls,
            this.__bg_bs,
            this.radius,
            this.bl_fill ? this.bl_fill : this.fill,
            this.bl_stroke ? this.bl_stroke : this.stroke,
            this.bl_stroke_width ? this.bl_stroke_width : this.stroke_width);

        // Init the graphics: bottom "border" --------------------------------------------
        Ws.Utils.draw_rr_bottom(
            this.__bge_b.firstChild,
            this.__bg_bs,
            this.b_fill ? this.b_fill : this.fill,
            this.b_stroke ? this.b_stroke : this.stroke,
            this.b_stroke_width ? this.b_stroke_width : this.stroke_width);

        // Init the graphics: bottom-right corner ----------------------------------------
        Ws.Utils.draw_rr_bottom_right(
            this.__bge_br.firstChild,
            this.__bg_rs,
            this.__bg_bs,
            this.radius,
            this.br_fill ? this.br_fill : this.fill,
            this.br_stroke ? this.br_stroke : this.stroke,
            this.br_stroke_width ? this.br_stroke_width : this.stroke_width);
    },

    // "Private" fields ------------------------------------------------------------------
    __ce: null,     // Contents element

    __bge: null,    // Background mosaic: wrapper element

    __bge_tl: null, // Background mosaic: top-left element
    __bge_t: null,  // Background mosaic: top element
    __bge_tr: null, // Background mosaic: top-right element
    __bge_l: null,  // Background mosaic: left element
    __bge_c: null,  // Background mosaic: center element
    __bge_r: null,  // Background mosaic: right element
    __bge_bl: null, // Background mosaic: bottom-left element
    __bge_b: null,  // Background mosaic: bottom element
    __bge_br: null, // Background mosaic: bottom-right element

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});
