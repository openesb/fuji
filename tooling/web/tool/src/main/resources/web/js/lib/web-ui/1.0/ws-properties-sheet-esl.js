Ws.PropertiesSheet.EDITABLE_STRING_LIST = "editable-string-list";

if (!Ws.PropertiesSheet._Types) {
    Ws.PropertiesSheet._Types = {};
}

Ws.PropertiesSheet._Types.EditableStringList = {
    render_type: Ws.PropertiesSheet.RENDER_TWO_COLUMNS,

    delete_icon_url: "img/delete-item.png",
    delete_d_icon_url: "img/delete-item_d.png",
    move_up_icon_url: "img/move-up-item.png",
    move_up_d_icon_url: "img/move-up-item_d.png",
    move_down_icon_url: "img/move-down-item.png",
    move_down_d_icon_url: "img/move-down-item_d.png",

    no_items_text: "No items.",
    add_item_text: "Add item",

    render: function(psheet, descriptor) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        var html = "";

        var list = this.__get_value_as_array(descriptor);

        html += "<div class=\"" + classname + "-wrapper\" id=\"" + id + "-wrapper" + "\">";

        if (list.length == 0) {
            html += this.__render_no_items_text(psheet, descriptor);
        } else {
            for (var i = 0; i < list.length; i++) {
                html += this.__render_item(psheet, descriptor, i, list.length);
            }
        }

        if (!descriptor.disable_inline_description_render && descriptor.inline_description) {
            html +=
                "<div id=\"" + id + "-inline-description\" " +
                        "class=\"" + classname + "-inline-description\">" +
                    descriptor.inline_description +
                "</div>";
        }

        if (!descriptor.disable_buttons_menu_render) {
            html +=
                "<div id=\"" + id + "-buttons-wrapper\" " +
                        "class=\"" + classname + "-buttons-wrapper" + "\">";

            if (!descriptor.disable_add_render && !descriptor.read_only) {
                var add_onclick =
                    "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_LIST]." +
                    "handle_add('" + psheet.element.id + "', '" + descriptor.name + "')";

                var add_item_text = descriptor.add_item_text
                        ? descriptor.add_item_text
                        : this.add_item_text;

                if (!descriptor.disable_add) {
                    html +=
                        "<input id=\"" + id + "-add-button\" " +
                                "class=\"" + classname + "-add-button\" " +
                                "type=\"button\" " +
                                "value=\"" + add_item_text + "\" " +
                                "onclick=\"" + add_onclick + "\"/>";
                } else {
                    html +=
                        "<input id=\"" + id + "-add-button\" " +
                                "class=\"" + classname + "-add-button\" " +
                                "type=\"button\" " +
                                "value=\"" + add_item_text + "\" " +
                                "disabled=\"disabled\"/>";
                }
            }

            if (!descriptor.disable_validation_note_render) {
                html +=
                    "<div id=\"" + id + "-validation-note\" " +
                            "class=\"" + classname + "-validation-note\"></div>";
            }

            html += "</div>";
        }

        html += "</div>";
        return html;
    },

    set_value: function(psheet, descriptor) {
        if (!descriptor.read_only) {
            var id = psheet.__get_input_id(descriptor);
            var list = this.__get_value_as_array(descriptor);

            for (var i = 0; i < list.length; i++) {
                var element = $(id + "-" + i);

                if (element != null) {
                    element.value = list[i];
                }
            }
        }
    },

    get_value: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return this.__get_value_as_array(descriptor);
        } else {
            var id = psheet.__get_input_id(descriptor);
            var list = [];

            var i = 0;
            var element = null;
            while ((element = $(id + "-" + i)) != null) {
                list.push(element.value);

                i++;
            }

            return list;
        }
    },

    validate: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return []; // TODO: There is nothing to validate if we're read-only, or not?
        } else {
            var results = [];

            if (descriptor.validation) {
                var values = this.get_value(psheet, descriptor);

                if (descriptor.validation.list_not_empty) {
                    if (values.length == 0) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be an empty list.",
                            index: -1
                        });
                    }
                }

                if (descriptor.validation.list_min_length &&
                        descriptor.validation.list_min_length > 0) {

                    if (values.length < descriptor.validation.list_min_length) {
                        if (descriptor.validation.list_min_length == 1) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " cannot be an empty list.",
                                index: -1
                            });
                        } else {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " size cannot be less than " +
                                    descriptor.validation.list_min_length + " items.",
                                index: -1
                            });
                        }
                    }
                }

                if (descriptor.validation.list_max_length &&
                        descriptor.validation.list_max_length > 0) {

                    if (values.length > descriptor.validation.list_max_length) {
                        if (descriptor.validation.list_max_length == 1) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " cannot contain more " +
                                    "than 1 item.",
                                index: -1
                            });
                        } else {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " size cannot be larger than " +
                                    descriptor.validation.list_max_length + " items.",
                                index: -1
                            });
                        }
                    }
                }

                for (var i = 0; i < values.length; i++) {
                    var value = values[i];

                    if (descriptor.validation.item_regexp) {
                        if (!descriptor.validation.item_regexp.test(value)) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " (item #" + (i + 1) + ") " +
                                    "does not match the regular expression: " +
                                    descriptor.validation.item_regexp,
                                index: i
                            });
                        }
                    }

                    if (descriptor.validation.item_not_empty) {
                        if (value.length == 0) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " (item #" + (i + 1) + ") " +
                                    "cannot be empty.",
                                index: i
                            });
                        }
                    }

                    if (descriptor.validation.item_min_length &&
                            descriptor.validation.item_min_length > 0) {
                        if (value.length < descriptor.validation.item_min_length) {
                            if (descriptor.validation.item_min_length == 1) {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + " (item #" + (i + 1) + ") " +
                                        "cannot be less than 1 character.",
                                    index: i
                                });
                            } else {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + " (item #" + (i + 1) + ") " +
                                        "cannot be less than " +
                                        descriptor.validation.item_min_length + " characters.",
                                    index: i
                                });
                            }
                        }
                    }

                    if (descriptor.validation.item_max_length &&
                            descriptor.validation.item_max_length > 0) {
                        if (value.length > descriptor.validation.item_max_length) {
                            if (descriptor.validation.item_max_length == 1) {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + " (item #" + (i + 1) + ") " +
                                        "cannot be longer than 1 character.",
                                    index: i
                                });
                            } else {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + " (item #" + (i + 1) + ") " +
                                        "cannot exceed " +
                                        descriptor.validation.item_max_length + " characters.",
                                    index: i
                                });
                            }
                        }
                    }
                }

                this.__render_validation_results(psheet, descriptor, results);
            }

            this.__update_ui(psheet, descriptor, values);

            return results;
        }
    },

    // Private -------------------------------------------------------------------------------------
    __get_value_as_array: function(descriptor) {
        var list = descriptor.value;
        if ((list == null) || (list == undefined)) {
            list = [];
        } else {
            list = $A(list); // Convert to array using Prototype's special function.
        }

        return list;
    },

    __render_no_items_text: function(psheet, descriptor) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        var html = "";

        if (!descriptor.disable_no_items_text) {
            var no_items_text = descriptor.no_items_text
                    ? descriptor.no_items_text
                    : this.no_items_text;

            html +=
                "<div id=\"" + id + "-no-items-text\" " +
                        "class=\"" + classname + "-no-items-text\">" +
                    no_items_text +
                "</div>";
        }

        return html;
    },
    
    __render_item: function(psheet, descriptor, i, length) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        var html = "";
        
        html +=
            "<div class=\"" + classname + "-item-wrapper" + "\" " +
                    "id=\"" + id + "-" + i + "-item-wrapper" + "\">";

        html +=
            "<div class=\"" + classname + "-item-menu-wrapper" + "\" " +
                    "id=\"" + id + "-" + i + "-item-menu-wrapper" + "\">";
        html += this.__render_menu(psheet, descriptor, i, length);
        html +=
            "</div>";

        if (!descriptor.read_only) {
            html +=
                "<div class=\"" + classname + "-input-wrapper" + "\" " +
                        "id=\"" + id + "-" + i + "-input-wrapper" + "\">";
            html +=
                "<input type=\"text\" " +
                       "class=\"" + classname + "-input" + "\" " +
                       "id=\"" + id + "-" + i + "\" " +
                       "value=\"" + "" + "\" " +
                       this.__render_change_trigger(psheet, descriptor) + " " +
                       this.__render_save_trigger(psheet, descriptor) + "/>";
            html +=
                "</div>";
        } else {
            var list = this.__get_value_as_array(descriptor);

            html +=
                "<div class=\"" + classname + "-input-read-only" + "\" " +
                        "id=\"" + id + "-" + i + "-input-read-only" + "\">" +
                list[i] +
                "</div>";
        }

        html +=
            "</div>";

        return html;
    },

    __render_menu: function(psheet, descriptor, i, length) {
        var psheet_id = psheet.element.id;
        var classname = psheet.__get_input_class(descriptor);

        var html = "";

        if (!descriptor.disable_menu_render && !descriptor.read_only) {
            var delete_onclick =
                "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_LIST]." +
                "handle_delete('" + psheet_id + "', '" + descriptor.name + "', " + i + ")";
            var move_down_onclick =
                "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_LIST]." +
                "handle_move_down('" + psheet_id + "', '" + descriptor.name + "', " + i + ")";
            var move_up_onclick =
                "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_LIST]." +
                "handle_move_up('" + psheet_id + "', '" + descriptor.name + "', " + i + ")";

            if (!descriptor.disable_delete_render) {
                var delete_disabled_due_to_validation = false;
                if (descriptor.validation) {
                    if (descriptor.validation.list_not_empty && (length == 1)) {
                        delete_disabled_due_to_validation = true;
                    }

                    if (descriptor.validation.list_min_length) {
                        if (length <= descriptor.validation.list_min_length) {
                            delete_disabled_due_to_validation = true;
                        }
                    }
                }

                if (!descriptor.disable_delete && !delete_disabled_due_to_validation &&
                        !descriptor.read_only) {

                    html +=
                        "<img src=\"" + this.delete_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon" + "\" " +
                            "onclick=\"" + delete_onclick + "\"/>"
                } else {
                    html +=
                        "<img src=\"" + this.delete_d_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon-disabled" + "\"/>"
                }
            }

            if (!descriptor.disable_reorder_render) {
                if ((i < length - 1) && !descriptor.disable_reorder &&
                        !descriptor.read_only) {

                    html +=
                        "<img src=\"" + this.move_down_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon" + "\" " +
                            "onclick=\"" + move_down_onclick + "\"/>"
                } else {
                    html +=
                        "<img src=\"" + this.move_down_d_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon-disabled" + "\"/>"
                }

                if ((i > 0) && !descriptor.disable_reorder &&
                        !descriptor.read_only) {

                    html +=
                        "<img src=\"" + this.move_up_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon" + "\" " +
                            "onclick=\"" + move_up_onclick + "\"/>"
                } else {
                    html +=
                        "<img src=\"" + this.move_up_d_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon-disabled" + "\"/>"
                }
            }
        }

        return html;
    },

    __render_change_trigger: function(psheet, descriptor) {
        var onkeyup = "onkeyup=\"";
        var onblur = "onblur=\"";

        if (descriptor.validation) {
            var validation_code = "$('" + psheet.element.id + "').ws_psheet.validate();";
            if (descriptor.validation.trigger == "key") {
                onkeyup += validation_code;
            } else if (descriptor.validation.trigger == "focus") {
                onblur += validation_code;
            }
        }

        onkeyup += "$('" + psheet.element.id + "').ws_psheet.update_depends();";
        onkeyup += "$('" + psheet.element.id + "').ws_psheet.update_dynamic();";

        return onkeyup + "\" " + onblur + "\"";
    },

    __render_save_trigger: function(psheet, descriptor) {
        if (!descriptor.skip_save_on_enter) {
            return "onkeypress=\"if (event.keyCode == 13) { " +
                    "$('" + psheet.element.id + "').ws_psheet.save(); " +
                "}\"";
        } else {
            return "";
        }
    },

    __render_validation_results: function(psheet, descriptor, results) {
        var i, j, element;

        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        // First reset the error badges if any. This means run through the elements and remove the
        // classnames.
        i = 0;
        while ((element = $(id + "-" + i)) != null) {
            element.removeClassName(classname + "-input-error");
            element.removeClassName(classname + "-input-warning");

            i++;
        }

        // Then run through the results assigning classnames to whichever elements require it.
        for (i = 0; i < results.length; i++) {
            if (results[i].severity == Ws.PropertiesSheet.ERROR) {
                if (results[i].index == -1) {
                    j = 0;
                    while ((element = $(id + "-" + j)) != null) {
                        element.addClassName(classname + "-input-error");
                        element.removeClassName(classname + "-input-warning");

                        j++;
                    }

                    break;
                }

                element = $(id + "-" + results[i].index);
                
                if (element != null) {
                    element.removeClassName(classname + "-input-warning"); // It might have been
                                                                           // assigned already.
                    element.addClassName(classname + "-input-error");
                }
            } else if (results[i].severity == Ws.PropertiesSheet.WARNING) {
                if (results[i].index == -1) {
                    j = 0;
                    while ((element = $(id + "-" + j)) != null) {
                        if (!element.hasClassName(classname + "-input-error")) {
                            element.addClassName(classname + "-input-warning");
                        }

                        j++;
                    }
                } else {
                    element = $(id + "-" + results[i].index);

                    if ((element != null) && !element.hasClassName(classname + "-input-error")) {
                        element.addClassName(classname + "-input-warning");
                    }
                }
            }
        }
    },

    __update_ui: function(psheet, descriptor, values) {
        var i, element;

        var id = psheet.__get_input_id(descriptor);

        // Update the menus of the items.
        for (i = 0; i < values.length; i++) {
            $(id + "-" + i + "-item-menu-wrapper").innerHTML =
                    this.__render_menu(psheet, descriptor, i, values.length);
        }

        if (!descriptor.disable_add_render) {
            if (descriptor.validation && descriptor.validation.list_max_length) {
                if (values.length < descriptor.validation.list_max_length) {
                    $(id + "-add-button").disabled = false;
                } else {
                    $(id + "-add-button").disabled = true;
                }
            }
        }

        // Tackle the no items text.
        if (!descriptor.disable_no_items_text) {
            element = $(id + "-no-items-text");
            
            if ((values.length > 0) && (element != null)) {
                element.parentNode.removeChild(element);
            } else if ((values.length == 0) && (element == null)) {
                $(id + "-wrapper").innerHTML =
                        this.__render_no_items_text(psheet, descriptor) +
                        $(id + "-wrapper").innerHTML;
            }
        }

        // Update the validation note, explaining the reason behind disabling certain actions.
        if (!descriptor.disable_validation_note_render) {
            element = $(id + "-validation-note");

            if ((element != null) && descriptor.validation) {
                element.innerHTML = "";

                if (!descriptor.disable_delete && !descriptor.disable_delete_render &&
                        descriptor.validation.list_not_empty &&
                        (values.length == 1)) {

                    element.innerHTML = "Delete action was disabled as the list cannot be empty.";
                }

                if (!descriptor.disable_delete && !descriptor.disable_delete_render &&
                        descriptor.validation.list_min_length &&
                        (descriptor.validation.list_min_length >= values.length)) {

                    element.innerHTML = "Delete action was disabled as the number " +
                        "of elements in the list cannot be less than " +
                        descriptor.validation.list_min_length + ".";
                }

                if (!descriptor.disable_add && !descriptor.disable_add_render &&
                        descriptor.validation.list_max_length &&
                        (descriptor.validation.list_max_length <= values.length)) {

                    element.innerHTML = "Add action was disabled as the number " +
                        "of elements in the list cannot be more than " +
                        descriptor.validation.list_max_length + ".";
                }
            }
        }
    },

    // Callbacks -----------------------------------------------------------------------------------
    handle_delete: function(psheet_id, descriptor_id, index) {
        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        var element = $(id + "-" + index + "-item-wrapper");
        element.parentNode.removeChild(element);

        // Now run from index to whatever exists, updating indices.
        var max = index + 1;
        while ($(id + "-" + max + "-item-wrapper") != null) {
            $(id + "-" + max + "-item-wrapper").id =
                    id + "-" + (max - 1) + "-item-wrapper";
            $(id + "-" + max + "-input-wrapper").id =
                    id + "-" + (max - 1) + "-input-wrapper";
            $(id + "-" + max + "-item-menu-wrapper").id =
                    id + "-" + (max - 1) + "-item-menu-wrapper";
            $(id + "-" + max).id =
                    id + "-" + (max - 1);

            max++;
        }

        psheet.validate();
    },

    handle_move_up: function(psheet_id, descriptor_id, index) {
        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        // We do not need to change anything except the values.
        var current_element = $(id + "-" + index);
        var previous_element = $(id + "-" + (index - 1));

        if ((current_element != null) && (previous_element != null)) {
            var temp = current_element.value;
            current_element.value = previous_element.value;
            previous_element.value = temp;
        }

        psheet.validate();
    },

    handle_move_down: function(psheet_id, descriptor_id, index) {
        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        // We do not need to change anything except the values.
        var current_element = $(id + "-" + index);
        var next_element = $(id + "-" + (index + 1));

        if ((current_element != null) && (next_element != null)) {
            var temp = current_element.value;
            current_element.value = next_element.value;
            next_element.value = temp;
        }

        psheet.validate();
    },

    handle_add: function(psheet_id, descriptor_id) {
        var element;

        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        var max = 0;
        while ($(id + "-" + max + "-item-wrapper") != null) {
            max++;
        }

        var new_element = document.createElement("div");
        new_element.innerHTML = this.__render_item(psheet, descriptor, max, max + 1);
        new_element = new_element.firstChild;

        if (!descriptor.disable_inline_description_render && descriptor.inline_description) {
            element = $(id + "-inline-description");
            element.parentNode.insertBefore(new_element, element);
        } else if (!descriptor.disable_add_render) {
            element = $(id + "-buttons-wrapper");
            element.parentNode.insertBefore(new_element, element);
        } else {
            $(id + "-wrapper").appendChild(new_element);
        }

        psheet.validate();
    },

    // No-op ---------------------------------------------------------------------------------------
    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_LIST] =
        Ws.PropertiesSheet._Types.EditableStringList;
