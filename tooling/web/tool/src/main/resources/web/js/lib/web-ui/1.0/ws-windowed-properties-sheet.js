/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////

Ws.WindowedPropertiesSheet = Class.create({
    auto_resize_to_fit_psheet: true,
    auto_resize_max_height: 400,
    auto_resize_min_height: 100,
    auto_resize_adjust_if_bigger: false,

    fwindow: null,
    psheet: null,

    element: null,

    __has_been_resized_manually: false,

    initialize: function(w_element, psh_container, psh_options, w_options, w_rr_options) {
        this.element = w_element;

        var window_contents = w_element.select(".-wsfw-contents")[0];

        var psheet = new Ws.PropertiesSheet(psh_container, window_contents, psh_options);
        var fwindow = new Ws.FloatingWindow(w_element, w_options, w_rr_options);

        this.element.ws_psheet = psheet;

        this.fwindow = fwindow;
        this.psheet = psheet;

        var self = this;

        // Add a 'callback' to the window, so that we know if the user resized the
        // properties sheet windpow manually and do not adjust its size.
        fwindow.resize_callbacks.push(function() {
            self.__has_been_resized_manually = true;
            self.layout();
        });

        // If we want to, add a listener to the properties sheet, which would adjust the
        // size of the window whenever the properties sheet is relaid out. Also attach a
        // listener to the tabbed pane (if there is any), so that the size of the window
        // is asjusted whenever the tab selection changes.
        this.__attach_psheet_listener();
        this.__attach_tpane_listener();

        this.layout();
    },

    __attach_tpane_listener: function() {
        if (this.auto_resize_to_fit_psheet && this.fwindow.contents.ws_tpane) {
            var self = this;
            
            var listener = function(event_type, event_data) {
                if (event_type == Ws.TabbedPane.EVENT_SELECTION_CHANGED) {
                    self.layout();
                }
            };

            listener.__ws_wpsheet = true;

            this.fwindow.contents.ws_tpane.add_listener(listener);
        }
    },

    __attach_psheet_listener: function() {
        if (this.auto_resize_to_fit_psheet) {
            var self = this;

            this.psheet.add_listener(function(event_type, event_data) {
                if (event_type == Ws.PropertiesSheet.EVENT_LAYOUT_CHANGED) {
                    // Check whether the listener we attached to the tabbed pane is still
                    // there, as the propertie sheet will recreate it from scratch on
                    // reset.
                    var tpane = self.fwindow.contents.ws_tpane;
                    var tpane_listener_exists = false;
                    if (tpane) {
                        for (var i = 0; i < tpane.listeners.length; i++) {
                            if (tpane.listeners[i].__ws_wpsheet) {
                                tpane_listener_exists = true;
                            }
                        }
                    }

                    if (!tpane_listener_exists) {
                        self.__attach_tpane_listener();
                    }

                    self.layout();
                }
            });
        }
    },

    layout: function() {
        var self = this;

        // If there are no sections in the properties sheet, we expect that the scrolling
        // will be provided by the properties sheet container element, that is the
        // contents element of the floating window. In this case we perform no additional 
        // actions, except calling {@link Ws.FloatingWindow.resize_to_fit_height} if 
        // necessary.
        //
        // Otherwise, scrolling should be provided by the contents elements of the tabbed
        // pane. To do this, we first layout the tabbed pane to make sure the contents
        // element size is set according to its preferences. Then, if required we adjust
        // the size of the floating window (it will max out if the tabbed pane contents
        // is large enough). Then we layout the tabbed pane again, so that it fits
        // exactly within the size of the window.

        var tpane = this.fwindow.contents.ws_tpane;
        var psheet = this.psheet;
        var fwindow = this.fwindow;
        if (tpane) {
            if (this.auto_resize_to_fit_psheet && !this.__has_been_resized_manually) {
                tpane.relayout(true);

                // Here we mimic the behavior of 'resize_height_to_fit', but do not use it
                // directly (as all of the tpane elements are positioned absolutely. Plus
                // we take into account the inner logic of tpane, which would otherwise be
                // inaccessible by the floating window.
                var header_height = 0;
                if (fwindow.header) {
                    header_height = Element.getHeight(fwindow.header) +
                            Ws.Utils.get_bmp(fwindow.header).get_margin_vertical();
                }

                var footer_height = 0;
                if (fwindow.footer) {
                    footer_height = Element.getHeight(fwindow.footer) +
                            Ws.Utils.get_bmp(fwindow.footer).get_margin_vertical();
                }

                var tab_height = 0, content_height = 0;
                for (var i = 0; i < tpane.descriptors.length; i++) {
                    var descriptor = tpane.descriptors[i];

                    var current_tab_height = Element.getHeight(descriptor.tab) +
                            Ws.Utils.get_bmp(descriptor.tab).get_margin_vertical();
                    var current_content_height = Element.getHeight(descriptor.contents) +
                            Ws.Utils.get_bmp(descriptor.contents).get_margin_vertical();

                    if (current_tab_height > tab_height) {
                        tab_height = current_tab_height;
                    }
                    if (current_content_height > content_height) {
                        content_height = current_content_height;
                    }
                }

                var height = tab_height + content_height;
                var bmp_vertical = Ws.Utils.get_bmp(fwindow.contents).get_bmp_vertical();
                if (this.auto_resize_max_height && (this.auto_resize_max_height < height)) {
                    height = this.auto_resize_max_height;
                }
                if (this.auto_resize_min_height && (this.auto_resize_min_height > height)) {
                    height = this.auto_resize_min_height;
                }

                // Note: we explicitly set 'overflow' to 'hidden' here to avoid a weird
                // Safari issue, which tends to render scrollbars which scroll exactly
                // to the amount required to draw the scrollbars. Other browsers behave
                // properly.
                fwindow.contents.setStyle({
                    height: height + "px",
                    overflow: "hidden"
                });

                fwindow.element.setStyle({
                    height: (header_height + footer_height + height + bmp_vertical) + "px"
                });

                fwindow.__update();
            }
            
            tpane.relayout();
        } else {
            if (this.auto_resize_to_fit_psheet && !this.__has_been_resized_manually) {
                self.element.ws_fwindow.resize_height_to_fit({
                    max_height: self.auto_resize_max_height,
                    min_height: self.auto_resize_min_height,
                    adjust_if_bigger: self.auto_resize_adjust_if_bigger
                });
            }
        }

        psheet.layout(true);
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder.
    }

})
