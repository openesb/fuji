var email_container = {
    pc_get_descriptors: function() {
        var container = {
            sections: [
                {
                    display_name: "Basic",
                    description: "Basic settings.",
                    properties: []
                },
                {
                    display_name: "Advanced",
                    description: "Advanced settings.",
                    properties: [
                        {
                            name: "raw_warning",
                            type: Ws.PropertiesSheet.LABEL,

                            display_name: "The below properties allow editing the raw " +
                                    "configuration files for this service. Please do " +
                                    "this if and only if you really know what you're " +
                                    "doing.",
                            description: ""
                        }
                        ,
                        {
                            name: "raw_edit_message_xsd",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit message.xsd",
                            description: "The message.xsd file is used to define the data types of the message going in and out of a service.",
                            action: function(event) {
                            }
                        }
                        ,
                        {
                            name: "raw_edit_interface_wsdl",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit interface.wsdl",
                            description: "The interface.wsdl file is used to define the service interface.",
                            action: function(event) {
                            }
                        }
                        ,
                        {
                            name: "raw_edit_binding_wsdl",
                            type: Ws.PropertiesSheet.BUTTON,

                            display_name: "Edit binding.wsdl",
                            description: "The binding.wsdl file is used to define the actual service-type specific binding of the service interface.",
                            action: function(event) {
                            }
                        }
                    ]
                }
            ]
        };

        container.sections[0].properties.push(
            {
                name: "properties_code_name",
                display_name: "Code Name",
                description: "Code name of the component.",
                value: "",
                type: "string"
            }
            ,
            {
                name: "properties_email_type",
                display_name: "Protocol",
                description: "Protocol Type: SMTP, POP3, or IMAP",
                value: "SMTP",
                type: Ws.PropertiesSheet.ENUM,
                values: [
                    {
                        value: "SMTP",
                        display_name: "SMTP",
                        description: ""
                    },
                    {
                        value: "POP3",
                        display_name: "POP3",
                        description: ""
                    },
                    {
                        value: "IMAP",
                        display_name: "IMAP",
                        description: ""
                    }
                ]
            }
            ,
            
            //////////////////////////////////////////////////////////////////////////////
            // Common
            {
	            name: "properties_email_emailServer",
	            display_name: "EMail Server",
	            description: "EMail Server Name",
	            value: "",
	            type: Ws.PropertiesSheet.STRING
	        }
            ,
            {
                name: "properties_email_useSSL",
                display_name: "Use SSL",
                description: "Use SSL? true or false",
                value: true,
                type: Ws.PropertiesSheet.BOOLEAN
            }
            ,
            {
                name: "properties_email_port",
                display_name: "Port",
                type: Ws.PropertiesSheet.INTEGER
                ,
                dynamic: [
                    {
                        and: [
                            {
                                name: "properties_email_type",
                                value: "SMTP"
                            },
                            {
                                name: "properties_email_useSSL",
                                value: true
                            }
                        ],
                        
                        changes: {
                            description: "Port value of the SSL connector of the target SMTP Server. Typically 465.",
                            value: 465
                        }
                    },
                    {
                        and: [
                            {
                                name: "properties_email_type",
                                value: "SMTP"
                            },
                            {
                                name: "properties_email_useSSL",
                                value: false
                            }
                        ],

                        changes: {
                            description: "Port value of the non-SSL connector of the target SMTP Server. Typically 25.",
                            value: 25
                        }
                    },
                    {
                        and: [
                            {
                                name: "properties_email_type",
                                value: "IMAP"
                            },
                            {
                                name: "properties_email_useSSL",
                                value: true
                            }
                        ],

                        changes: {
                            description: "Port value of the SSL connector of the target IMAP Server. Typically 993.",
                            value: 993
                        }
                    },
                    {
                        and: [
                            {
                                name: "properties_email_type",
                                value: "IMAP"
                            },
                            {
                                name: "properties_email_useSSL",
                                value: false
                            }
                        ],

                        changes: {
                            description: "Port value of the non-SSL connector of the target IMAP Server. Typically 143.",
                            value: 143
                        }
                    },
                    {
                        and: [
                            {
                                name: "properties_email_type",
                                value: "POP3"
                            },
                            {
                                name: "properties_email_useSSL",
                                value: true
                            }
                        ],

                        changes: {
                            description: "Port value of the SSL connector of the target POP3 Server. Typically 995.",
                            value: 995
                        }
                    },
                    {
                        and: [
                            {
                                name: "properties_email_type",
                                value: "POP3"
                            },
                            {
                                name: "properties_email_useSSL",
                                value: false
                            }
                        ],

                        changes: {
                            description: "Port value of the non-SSL connector of the target POP3 Server. Typically 110.",
                            value: 110
                        }
                    }
                ],
                dynamic_defaults: {
                    description: "Server Port",
                    value: 465
                }
            }
            ,
            {
	            name: "properties_email_userName",
	            display_name: "User Name",
	            description: "User Name",
	            value: "",
	            type: Ws.PropertiesSheet.STRING
	        }
            ,
            {
	            name: "properties_email_password",
	            display_name: "Password",
	            description: "Password",
	            value: "",
	            type: Ws.PropertiesSheet.PASSWORD
	        }
            ,

            //////////////////////////////////////////////////////////////////////////////
            // SMTP
            {
                name: "properties_email_location",
                display_name: "Location",
                description: "mailto URL (RFC 2368). For example, mailto:someUser@someCompany.com.",
                value: "mailto:someUser@someCompany.com",
                type: Ws.PropertiesSheet.STRING
                ,
                depends: {
                    name: "properties_email_type",
                    value: "SMTP"
                }
            }
            ,
            {
                name: "properties_email_charset",
                display_name: "Charset",
                description: "Charset",
                value: "",
                type: Ws.PropertiesSheet.STRING
                ,
                depends: {
                    name: "properties_email_type",
                    value: "SMTP"
                }
            }
            ,
            {
                name: "properties_email_use",
                display_name: "Use",
                description: "Use literal or encoded",
                value: "literal",
                type: Ws.PropertiesSheet.ENUM,
                values: [
                    {
                        value: "literal",
                        display_name: "literal",
                        description: ""
                    },
                    {
                        value: "encoded",
                        display_name: "encoded",
                        description: ""
                    }
                ]
                ,
                depends: {
                    name: "properties_email_type",
                    value: "SMTP"
                }
            }
            ,
            {
                name: "properties_email_encodingStyle",
                display_name: "Encoding Style",
                description: "Encoding Style",
                value: "",
                type: Ws.PropertiesSheet.STRING
                ,
                depends: {
                    name: "properties_email_type",
                    value: "SMTP"
                }
            }
            ,

            //////////////////////////////////////////////////////////////////////////////
            // IMAP
            {
                name: "properties_email_mailFolder",
                display_name: "Mail Folder",
                description: "Mail Folder",
                value: "Inbox",
                type: Ws.PropertiesSheet.STRING
                ,
                depends: {
                    name: "properties_email_type",
                    value: "IMAP"
                }
            }
            ,

            //////////////////////////////////////////////////////////////////////////////
            // POP3 and IMAP
            {
                name: "properties_email_maxMessageCount",
                display_name: "Max Msg Count",
                description: "Max Message Count, 0 means unlimited",
                value: 10,
                type: Ws.PropertiesSheet.INTEGER
                ,
                depends: {
                    or: [
                        {
                            name: "properties_email_type",
                            value: "IMAP"
                        },
                        {
                            name: "properties_email_type",
                            value: "POP3"
                        }
                    ]
                }
            }
            ,
            {
                name: "properties_email_messageAckMode",
                display_name: "Ack Mode",
                description: "Message Ack Mode: automatic or manual",
                value: "automatic",
                type: Ws.PropertiesSheet.ENUM,
                values: [
                    {
                        value: "automatic",
                        display_name: "automatic",
                        description: ""
                    },
                    {
                        value: "manual",
                        display_name: "manual",
                        description: ""
                    }
                ]
                ,
                depends: {
                    or: [
                        {
                            name: "properties_email_type",
                            value: "IMAP"
                        },
                        {
                            name: "properties_email_type",
                            value: "POP3"
                        }
                    ]
                }
            }
            ,
            {
                name: "properties_email_messageAckOperation",
                display_name: "Ack Operation",
                description: "Message Ack Operation: delete or markAsRead",
                value: "markAsRead",
                type: Ws.PropertiesSheet.ENUM,
                values: [
                    {
                        value: "delete",
                        display_name: "delete",
                        description: ""
                    },
                    {
                        value: "markAsRead",
                        display_name: "markAsRead",
                        description: ""
                    }
                ]
                ,
                depends: {
                    or: [
                        {
                            name: "properties_email_type",
                            value: "IMAP"
                        },
                        {
                            name: "properties_email_type",
                            value: "POP3"
                        }
                    ]
                }
            }
            ,
            {
                name: "properties_email_pollingInterval",
                display_name: "Polling Interval",
                description: "Polling Interval, in minute",
                value: 1,
                type: Ws.PropertiesSheet.INTEGER
                ,
                depends: {
                    or: [
                        {
                            name: "properties_email_type",
                            value: "IMAP"
                        },
                        {
                            name: "properties_email_type",
                            value: "POP3"
                        }
                    ]
                }
            }
        );

        return container;
    },

    pc_set_properties: function(properties) {
        var html = "";

        for (var i in properties) {
            html += "" + i + " => " + properties[i] + "<br/>";
        }

        $("logger").innerHTML = html;
    }
};
