/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var CONTROL_SELECT = "select";

//----------------------------------------------------------------------------------------
var SelectControlNode = Class.create(EipNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    CONTROL_SELECT, this, ui_options);
        }

        Ws.Utils.apply_defaults(this.properties, {
            expression_type: "xpath",
            use_external_config: false,
            expression: "",
            cases_number: 2,
            case_1: "abc",
            case_2: "def",
            external_config_name: "cbr-config"
        });
        
        this.properties.cases = [];
        for (var i = 1; typeof this.properties["case_" + i] != "undefined"; i++) {
            this.properties.cases.push(this.properties["case_" + i]);
        }
    },

    get_outbound_connectors: function() {
        var connectors = [];

        connectors.push([
            {
                max_links: 1
            }
        ]);

        for (var i = 0; i < this.properties.cases_number; i++) {
            connectors.push({
                max_links: 1
            });
        }

        return connectors;
    },

    pc_get_descriptors: function($super) {
        var container = $super();
        var node = this;

        var cases = this.properties.cases;
        var calculate_values = function(number) {
            var value = [];

            for (var i = 0; i < number; i++) {
                if (i < cases.length) {
                    value[i] = cases[i];
                } else {
                    value[i] = "";
                }
            }

            return value;
        }

        container.properties.push(
            {
                name: "properties_expression_type",
                type: Ws.PropertiesSheet.ENUM,
                display_name: "Type",
                description: "Type of the expression used to route incoming messages.",
                value: this.properties.expression_type,

                values: [
                    {
                        display_name: "XPath",
                        description: "XPath expression. Works best when used with XML data.",
                        value: "xpath"
                    },
                    {
                        display_name: "Regular Expression",
                        description: "Regular Expression. Most useful for non-XML textual data.",
                        value: "regex"
                    }
                ]
            }
            ,
            {
                name: "properties_use_external_config",
                type: Ws.PropertiesSheet.BOOLEAN,

                display_name: "Use External",
                description: "Whether the configuration data for this EIP is inlined or loaded from an external file.",
                value: this.properties.use_external_config
            }
            ,
            {
                name: "properties_expression",
                type: Ws.PropertiesSheet.CODE,
                code_type : "xml",
                display_name: "Expression",
                description: "Expression to evaluate on the incoming messages.",
                value: this.properties.expression,

                auto_adjust_height: true,
                maximizable: true,

                depends: {
                    name: "properties_use_external_config",
                    value: false
                },

                dynamic: [
                    {
                        name: "properties_expression_type",
                        value: "xpath",

                        changes: {
                            inline_description: "Enter the XPath expression which will be used to fetch the value which will be compared to the cases below. There is no need to quote or escape it."
                        }
                    },
                    {
                        name: "properties_expression_type",
                        value: "regex",

                        changes: {
                            inline_description: "Enter the regular expression which will be used to fetch the value which will be compared to the cases below. There is no need to quote or escape it."
                        }
                    }
                ]
            }
            ,
            {
                name: "properties_cases_number",
                type: Ws.PropertiesSheet.ENUM,
                display_name: "# of Cases",
                description: "Number of outbound routes for this router.",
                value: this.properties.cases_number,

                values: [
                    {
                        display_name: "1",
                        description: "",
                        value: "1"
                    },
                    {
                        display_name: "2",
                        description: "",
                        value: "2"
                    },
                    {
                        display_name: "3",
                        description: "",
                        value: "3"
                    },
                    {
                        display_name: "4",
                        description: "",
                        value: "4"
                    },
                    {
                        display_name: "5",
                        description: "",
                        value: "5"
                    }
                ]
            }
            ,
            {
                name: "properties_cases",
                type: Ws.PropertiesSheet.ARRAY,

                display_name: "Case",
                description: "Condition which must be satisfied for this route to become active.",
                value: this.properties.cases,

                display_name_suffix: " %i",
                description_suffix: "",
                items_type: Ws.PropertiesSheet.STRING,

                dynamic: [
                    {
                        name: "properties_cases_number",
                        value: "1",

                        changes: {
                            value: calculate_values(1)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "2",

                        changes: {
                            value: calculate_values(2)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "3",

                        changes: {
                            value: calculate_values(3)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "4",

                        changes: {
                            value: calculate_values(4)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "5",

                        changes: {
                            value: calculate_values(5)
                        }
                    },
                ]
            }
            ,
            {
                name: "__else_case_message",
                type: Ws.PropertiesSheet.LABEL,

                display_name: "\"Else\" case is not listed here, to use it conect something to the bottommost connector of the CBR node on the canvas.",
                description: ""
            }
            ,
            {
                name: "properties_external_config_name",
                type: Ws.PropertiesSheet.STRING,

                display_name: "Config name",
                description: "Name of the extenrnal configuration.",
                value: this.properties.external_config_name,

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config_warning",
                type: Ws.PropertiesSheet.LABEL,

                display_name: "Note: once you click the Edit Configuration button, changes you made to the other properties will be automatically saved.",
                description: "",

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config",
                type: Ws.PropertiesSheet.BUTTON,

                display_name: "Edit Configuration",
                description: "Opens the external configuragion file in the editor.",
                action: function(psheet) {
                    psheet.save();
                    edit_external_artifact(node.id, "external-config", false);
                },

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
        );

        return container;
    },

    pc_set_properties: function($super, properties) {
        var i, cases = properties["properties_cases"];

        for (i = 0; i < cases.length; i++) {
            properties["properties_case_" + (i + 1)] = cases[i];
        }

        $super(properties);

        var links = this.get_outbound_links();
        for (i = 0; i < links.length; i++) {
            links[i].ui.repaint();
        }
    }
});

//----------------------------------------------------------------------------------------
var SelectControlNodeUi = Class.create(EipNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);

        this.shape_size = 44;
        this.skew_size = 10;
        this.width = this.shape_size + (this.margin * 2);
        this.height = this.shape_size - 5 + (this.margin * 2);

        this.toolbar_x = this.width - this.margin;
    },

    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();

        connectors[0].x = this.width / 2;
        connectors[0].y = this.height - this.margin;
        connectors[0].type = Ws.Graph.NODE_CONNECTOR_SOUTH;

        for (var i = 1; i < connectors.length; i++) {
            var x = this.width - this.margin;
            var y = this.margin + i * ((this.height - (2 * this.margin)) / (connectors.length));

            if (y < this.margin + this.skew_size) {
                x -= (this.margin + this.skew_size) - y;
            }

            if (y > this.height - this.margin - this.skew_size) {
                x -= y - (this.height - this.margin - this.skew_size);
            }

            connectors[i].x = x;
            connectors[i].y = y;
            connectors[i].type = Ws.Graph.NODE_CONNECTOR_EAST;
        }

        return connectors;
    },

    prepare_path: function() {
        //   B--C
        //  /    \
        // A      D
        // |      |
        // |      |
        // H      E
        //  \    /
        //   G--F
        var A_x = this.margin, A_y = this.margin + this.skew_size;
        var B_x = this.margin + this.skew_size, B_y = this.margin;
        var C_x = this.width - this.margin - this.skew_size, C_y = this.margin;
        var D_x = this.width - this.margin, D_y = this.margin + this.skew_size;
        var E_x = this.width - this.margin, E_y = this.height - this.margin - this.skew_size;
        var F_x = this.width - this.margin - this.skew_size, F_y = this.height - this.margin;
        var G_x = this.margin + this.skew_size, G_y = this.height - this.margin;
        var H_x = this.margin, H_y = this.height - this.margin - this.skew_size;

        this.context.beginPath();
        this.context.moveTo(A_x, A_y);
        this.context.lineTo(B_x, B_y);
        this.context.lineTo(C_x, C_y);
        this.context.lineTo(D_x, D_y);
        this.context.lineTo(E_x, E_y);
        this.context.lineTo(F_x, F_y);
        this.context.lineTo(G_x, G_y);
        this.context.lineTo(H_x, H_y);
        this.context.lineTo(A_x, A_y);
    },

    draw_node: function($super) {
        $super();

        var base_x = this.width / 2;
        var base_y = this.height / 2;

        this.context.lineWidth = 1;
        this.context.fillStyle = this.context.strokeStyle;

        this.context.beginPath();
        this.context.arc(base_x - 11, base_y, 2, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(base_x + 11, base_y - 8, 2, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(base_x + 11, base_y, 2, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(base_x + 11, base_y + 8, 2, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(base_x - 16, base_y);
        this.context.lineTo(base_x - 11, base_y);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(base_x + 16, base_y - 8);
        this.context.lineTo(base_x + 11, base_y - 8);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(base_x + 16, base_y);
        this.context.lineTo(base_x + 11, base_y);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(base_x + 16, base_y + 8);
        this.context.lineTo(base_x + 11, base_y + 8);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(base_x - 11, base_y);
        this.context.lineTo(base_x + 11, base_y - 8);
        this.context.stroke();
    },

    draw_text: function() {
        // Does nothing (no text on tee)
    },

    draw_icon: function() {
        // Does nothing (no icon on tee)
    },

    __get_toolbar_items: function($super) {
        var self = this;
        var items = [];

        if (!this.read_only) {
            items.push({
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        items.push({
            icon: Ws.Graph.NODE_TBAR_EDIT_PROPERTIES,
            icon_d : Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE,
            icon_width: 16,
            icon_height: 16,
            tooltip: "Properties",
            callback: function(event, options) {
                self.__tbar_edit_properties(options);
            }
        });

        return items;
    },

    noop: function() {
        // Does nothing.
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(CONTROL_SELECT, SelectControlNode);
Ws.Graph.NodeUiFactory.register_type(CONTROL_SELECT, SelectControlNodeUi);
