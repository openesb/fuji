/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

if (typeof Ws.Graph == "undefined") {
    Ws.Graph = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Graph.DefaultLink = Class.create({
    id: 0,

    graph: null,

    start_node: null,
    start_node_connector: 0,

    end_node: null,
    end_node_connector: 0,

    properties: null,
    ui_properties: null,

    pre_add_hook: null,
    post_add_hook: null,
    pre_remove_hook: null,
    post_remove_hook: null,
    
    initialize: function(data, ui_options, skip_ui_init) {
        // See the comment in DefaultNode constructor, for the explanation of why we're
        // not automatically copying all fields of 'data' to the new instance.

        if (data.id) {
            this.id = data.id;
        }

        if (data.graph) {
            this.graph = data.graph;
        }

        if (data.start_node) {
            this.start_node = data.start_node;
        }
        if (data.end_node) {
            this.end_node = data.end_node;
        }

        if (data.start_node_connector) {
            this.start_node_connector = data.start_node_connector;
        }
        if (data.end_node_connector) {
            this.end_node_connector = data.end_node_connector;
        }

        if (data.properties) {
            this.properties = Ws.Utils.clone_object(data.properties);
        } else {
            this.properties = {};
        }
        if (data.ui_properties) {
            this.ui_properties = Ws.Utils.clone_object(data.ui_properties);
        } else {
            this.ui_properties = {};
        }

        if (data.pre_add_hook) {
            this.pre_add_hook = data.pre_add_hook;
        }
        if (data.post_add_hook) {
            this.post_add_hook = data.post_add_hook;
        }
        if (data.pre_remove_hook) {
            this.pre_remove_hook = data.pre_remove_hook;
        }
        if (data.post_remove_hook) {
            this.post_remove_hook = data.post_remove_hook;
        }

        if (!skip_ui_init) {
            this.ui = Ws.Graph.LinkUiFactory.new_link_ui(
                Ws.Graph.DEFAULT_LINK_TYPE,
                this,
                ui_options
                );
        }
    },

    get_property: function(name) {
        return this.properties[name];
    },

    set_property: function(name, new_value, silent) {
        if (this.properties[name] != new_value) {
            var old_value = this.properties[name];

            this.properties[name] = new_value;

            if (!silent) {
                this.graph.notify_listeners({
                    type: Ws.Graph.EVENT_LINK_UPDATED,
                    data: {
                        link: this,
                        updated_properties: [
                        {
                            name: name,
                            old_value: old_value,
                            new_value: new_value
                        }
                        ]
                    }
                });
            }
        }
    },

    set_properties: function(properties, silent) {
        var updated_properties = [];

        for (var i in properties) {
            if (this.properties[i] != properties[i]) {
                var old_value = this.properties[i];

                this.properties[i] = properties[i];

                updated_properties.push({
                    name: i,
                    old_value: old_value,
                    new_value: properties[i]
                });
            }
        }

        if ((updated_properties.length > 0) && !silent) {
            this.graph.notify_listeners({
                type: Ws.Graph.EVENT_LINK_UPDATED,
                data: {
                    link: this,
                    updated_properties: updated_properties
                }
            });
        }
    },

    get_ui_property: function(name) {
        return this.ui_properties[name];
    },

    set_ui_property: function(name, new_value, silent) {
        if (this.ui_properties[name] != new_value) {
            var old_value = this.ui_properties[name];

            this.ui_properties[name] = new_value;
            this.ui[name] = new_value;

            if (!silent) {
                this.graph.notify_listeners({
                    type: Ws.Graph.EVENT_LINK_UPDATED,
                    data: {
                        link: this,
                        updated_ui_properties: [
                        {
                            name: name,
                            old_value: old_value,
                            new_value: new_value
                        }
                        ]
                    }
                });
            }
        }
    },

    set_ui_properties: function(ui_properties, silent) {
        var updated_ui_properties = [];

        for (var i in ui_properties) {
            if (this.ui_properties[i] != ui_properties[i]) {
                var old_value = this.ui_properties[i];

                this.ui_properties[i] = ui_properties[i];
                this.ui[i] = ui_properties[i];

                updated_ui_properties.push({
                    name: i,
                    old_value: old_value,
                    new_value: ui_properties[i]
                });
            }
        }

        if ((updated_ui_properties.length > 0) && !silent) {
            this.graph.notify_listeners({
                type: Ws.Graph.EVENT_LINK_UPDATED,
                data: {
                    link: this,
                    updated_ui_properties: updated_ui_properties
                }
            });
        }
    },

    set_all_properties: function(properties, ui_properties, silent) {
        var updated_properties = [];
        var updated_ui_properties = [];

        var i, old_value;

        for (i in properties) {
            if (this.properties[i] != properties[i]) {
                old_value = this.properties[i];

                this.properties[i] = properties[i];

                updated_properties.push({
                    name: i,
                    old_value: old_value,
                    new_value: properties[i]
                });
            }
        }

        for (i in ui_properties) {
            if (this.ui_properties[i] != ui_properties[i]) {
                old_value = this.ui_properties[i];

                this.ui_properties[i] = ui_properties[i];
                this.ui[i] = ui_properties[i];

                updated_ui_properties.push({
                    name: i,
                    old_value: old_value,
                    new_value: ui_properties[i]
                });
            }
        }

        if (((updated_properties.length > 0) || (updated_ui_properties.length > 0)) &&
            !silent) {

            this.graph.notify_listeners({
                type: Ws.Graph.EVENT_LINK_UPDATED,
                data: {
                    link: this,
                    updated_properties: updated_properties,
                    updated_ui_properties: updated_ui_properties
                }
            });
        }
    },

    // Properties container --------------------------------------------------------------
    pc_get_descriptors: function() {
        return [];
    },

    pc_set_properties: function(properties) {
        var updated_properties = {};
        var updated_ui_properties = {};

        for (var i in properties) {
            if (i.indexOf("properties_") == 0) {
                updated_properties[i.substr(11)] = properties[i];
            }

            if (i.indexOf("ui_properties_") == 0) {
                updated_ui_properties[i.substr(14)] = properties[i];
            }
        }

        this.set_all_properties(updated_properties, updated_ui_properties);
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.DefaultLinkUi = Class.create({
    // Basic properties ------------------------------------------------------------------
    x: 10,
    y: 10,
    width: 180,
    height: 180,

    // Properties used by GraphUi for registering in various managers --------------------
    is_selectable: true,
    is_drag_source: true,
    is_clickable : true,

    // Drawing (Basic) properties --------------------------------------------------------
    margin: 100,
    cp_offset: 100,

    start_x: this.x + this.margin,
    start_y: this.y + this.margin,
    end_x: this.x + this.width - this.margin,
    end_y: this.y + this.height - this.margin,

    fill_style: "rgb(0, 0, 255)",
    stroke_style: "rgb(0, 0, 0)",
    line_width: 2,

    fill_style_selected: "rgb(0, 0, 255)",
    stroke_style_selected: "rgb(255, 0, 0)",
    line_width_selected: 4,

    // Drawing (Advanced) properties -----------------------------------------------------
    
    // Whether the link line (bezier curve by default) will turn into a straight
    // line when the start point and end point are almost on the same line vertically
    // or horizontally (i.e. a straight line between them will be almost vertical or
    // horizontal.) The parameter controlling the minimal distance before a link
    // turns into a straight line is 'switch_to_straight_line_threshold'. Note that
    // this switch will only happen if the link-node connectors are of appropriate
    // types (i.e. there will be switch on an almost vertical link between an EAST
    // and WEST connectors).
    switch_to_straight_line: true,
    switch_to_straight_line_threshold: 5,

    // Whether the 'cp_offset' (distance to control points) will be reduced, if the
    // distance between link's start and end points multiplied by
    // 'reduce_cp_offset_ratio' is smaller than 'cp_offset'.
    reduce_cp_offset: true,
    reduce_cp_offset_ratio: 0.3,
    reduce_cp_offset_minimum: 25,

    // HTML ------------------------------------------------------------------------------
    element: null,
    canvas: null,

    // Canvas context --------------------------------------------------------------------
    context: null,

    // Toolbar
    toolbar_spacing : 2,

    initialize: function(link, options) {
        // First apply the customizing options, that may have been passed via the
        // constructor arguments.
        if (options) {
            Object.extend(this, options);
        }
        
        var self = this;

        // Save the link reference -------------------------------------------------------
        this.link = link;

        // Initialize some elements, which cannot be initialized statically --------------
        this.canvas = Ws.Utils.create_canvas();

        this.element = new Element("div");
        this.element.style.position = "absolute";
        this.element.appendChild(this.canvas);
        this.element.setStyle({
            margin: "0px",
            padding: "0px",
            borderWidth: "0px",
            outlineStyle: "none" // This one is to disable native focus indicators,
        // we have our own
        });
        this.element.tabIndex = 0;

        // Save the back-link in the element ---------------------------------------------
        this.element.link = this.link;

        // Save the canvas context -------------------------------------------------------
        this.context = this.canvas.getContext("2d");
        
        // Apply the UI properties of the link -------------------------------------------
        Object.extend(this, Ws.Utils.clone_object(this.link.ui_properties));

        // Animation
        if (this.animation) {
            this.animation_interval = setInterval(function() {
                self.__draw_animation();
            }, 40);
        }
    },

    __get_toolbar_items : function() {
        var self = this;
        var items = [];

        items.push({
            name: "properties",
            icon: Ws.Graph.NODE_TBAR_EDIT_PROPERTIES,
            icon_d : Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE,
            icon_width: 16,
            icon_height: 16,
            tooltip: "Properties",
            callback: function(event, options) {
         //       self.__tbar_edit_properties(options);
            }
        });
        
        if (!this.read_only) {
            items.push({
                name: "delete",
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        return items;
    },

    repaint: function() {
        var x1, y1, type1, x2, y2, type2, p1, p2, start_node_xy, end_node_xy;

        if (this.link.start_node && this.link.end_node) {
            p1 = this.get_start_connector();
            p2 = this.get_end_connector();

            start_node_xy =
            this.graph_ui.get_node_graph_coordinates(this.link.start_node.ui);
            end_node_xy =
            this.graph_ui.get_node_graph_coordinates(this.link.end_node.ui);

            x1 = start_node_xy.x + p1.x;
            y1 = start_node_xy.y + p1.y;
            type1 = p1.type;
            x2 = end_node_xy.x + p2.x;
            y2 = end_node_xy.y + p2.y;
            type2 = p2.type;
        } else

        if (this.link.start_node && !this.link.end_node) {
            p1 = this.get_start_connector();

            start_node_xy =
            this.graph_ui.get_node_graph_coordinates(this.link.start_node.ui);

            x1 = start_node_xy.x + p1.x;
            y1 = start_node_xy.y + p1.y;
            type1 = p1.type;
            x2 = this.end_x + this.x;
            y2 = this.end_y + this.y;
            type2 = Ws.Graph.NODE_CONNECTOR_FREEFORM;
        } else

        if (!this.link.start_node && this.link.end_node) {
            p2 = this.get_end_connector();

            end_node_xy =
            this.graph_ui.get_node_graph_coordinates(this.link.end_node.ui);

            x1 = this.start_x + this.x;
            y1 = this.start_y + this.y;
            type1 = Ws.Graph.NODE_CONNECTOR_FREEFORM;
            x2 = end_node_xy.x + p2.x;
            y2 = end_node_xy.y + p2.y;
            type2 = p2.type;
        } else

/* if (!this.link.start_node && !this.link.end_node) */ {
            x1 = this.start_x + this.x;
            y1 = this.start_y + this.y;
            type1 = Ws.Graph.NODE_CONNECTOR_FREEFORM;
            x2 = this.end_x + this.x;
            y2 = this.end_y + this.y;
            type2 = Ws.Graph.NODE_CONNECTOR_FREEFORM;
        }

        // We should update the ui properties silently if the link is being
        // dragged, or if the start or end node is being dragged
        var be_silent = this.dragged_start || this.dragged_end;
        if (!be_silent && this.link.graph.ui.__dragged) {
            var dragged = this.link.graph.ui.__dragged;

            if (this.link.start_node && (dragged == this.link.start_node.ui)) {
                be_silent = true;
            } else

            if (this.link.end_node && (dragged == this.link.end_node.ui)) {
                be_silent = true;
            }
        }

        var new_x = (x1 < x2 ? x1 : x2) - this.margin;
        var new_y = (y1 < y2 ? y1 : y2) - this.margin;

        if (!be_silent) {
            this.link.set_ui_properties({
                x: new_x,
                y: new_y,
                width: Math.abs(x1 - x2) + (this.margin * 2),
                height: Math.abs(y1 - y2) + (this.margin * 2),
                start_x: x1 - new_x,
                start_y: y1 - new_y,
                end_x: x2 - new_x,
                end_y: y2 - new_y
            });
        } else {
            this.x = new_x;
            this.y = new_y;
            this.width = Math.abs(x1 - x2) + (this.margin * 2);
            this.height = Math.abs(y1 - y2) + (this.margin * 2);
            this.start_x = x1 - new_x;
            this.start_y = y1 - new_y;
            this.end_x = x2 - new_x;
            this.end_y = y2 - new_y;
        }

        this.canvas.width = this.width;
        this.canvas.height = this.height;
        
        if (this.canvas.firstChild) {
            Element.extend(this.canvas.firstChild);

            this.canvas.firstChild.setStyle({
                width: this.width + "px",
                height: this.height + "px"
            });
        }

        this.element.setStyle({
            left: this.x + "px",
            top: this.y + "px",
            width: this.width + "px",
            height: this.height + "px"
        });

        this.start_type = type1;
        this.end_type = type2;

        if (this.is_selected) {
            this.context.strokeStyle = this.stroke_style_selected;
            this.context.lineWidth = this.line_width_selected;
        } else {
            this.context.strokeStyle = this.stroke_style;
            this.context.lineWidth = this.line_width;
        }

        //        if (!this.is_selected) {
        //            this.__draw_toolbar();
        //        }
        this.prepare_path();
        this.context.stroke();

        if (this.is_selected || this.show_connectors) {
            this.context.strokeStyle = "rgb(255, 0, 255)";
            this.context.lineWidth = 1;

            this.context.beginPath();
            this.context.arc(this.start_x, this.start_y, 6, 0, Math.PI * 2, true);
            this.context.stroke();

            this.context.beginPath();
            this.context.arc(this.end_x, this.end_y, 6, 0, Math.PI * 2, true);
            this.context.stroke();
        }

        if (this.is_selected) {
            this.__draw_toolbar(this.selected_point);
        }
    //this.draw_icon();
    },

    __move_toolbar : function(x, y) {

    },

    /**
     * Handles the Delete action of the standard link toolbar. Effectively this means
     * that the node this UI belongs to is removed from the graph.
     */
    __tbar_delete: function() {
        if (this.link.graph.ui.tooltip) {
            this.link.graph.ui.tooltip.hide();
        }
        this.link.graph.remove_link(this.link);
    },

    __draw_toolbar : function(point) {
        var items = this.__get_toolbar_items();
        if (items.length < 1) {
            return;
        }
        var self = this;
        var x = 0;
        var y = 0;

        if (point) {
            x = point.x;
            y = point.y;
        } else {
            var points = this.__get_cp_points();
            var start_cp_x = points.start_cp_x;
            var start_cp_y = points.start_cp_y;
            var end_cp_x = points.end_cp_x;
            var end_cp_y = points.end_cp_y;
            var start_x = this.start_x;
            var start_y = this.start_y;
            var end_x = this.end_x;
            var end_y = this.end_y;
            x = 0.125 * (start_x + 3 * start_cp_x + 3 * end_cp_x + end_x);
            y = 0.125 * (start_y + 3 * start_cp_y + 3 * end_cp_y + end_y);
        }

        var r = 8 + this.toolbar_spacing;

        this.context.save();
        this.context.lineWidth = 1;
        this.context.fillStyle = "rgba(200, 200, 200, 0.5)";

        this.context.beginPath();
        
        this.context.arc(x + r, y + r, r, 3.14 / 2, -3.14 / 2, false);
        this.context.moveTo(x + r, y);
        this.context.lineTo(x + r + (2 * r - this.toolbar_spacing) * (items.length - 1) + 1, y);
        this.context.arc(x + r + (2 * r - this.toolbar_spacing) * (items.length - 1) + 1,
            y + r, r, -3.14 / 2, 3.14 / 2, false);
        this.context.lineTo(x + r, y + 2 * r);
        this.context.fill();

        //this.context.beginPath();
        this.context.strokeStyle = "rgba(210, 210, 210, 1)";
        this.context.arc(x + r, y + r, r, 3.14 / 2, -3.14 / 2, false);
        this.context.moveTo(x + r, y);
        this.context.lineTo(x + r + (2 * r - this.toolbar_spacing) * (items.length - 1) + 1, y);
        this.context.arc(x + r + (2 * r - this.toolbar_spacing) * (items.length - 1) + 1, y + r, r, -3.14 / 2, 3.14 / 2, false);
        this.context.lineTo(x + r, y + 2 * r);
        this.context.stroke();
        this.context.restore();

        x = x + this.toolbar_spacing;
        y = y + this.toolbar_spacing;
        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            var url = this.is_selected ? item.icon : item.icon_d;
            var image = Ws.ImagesLoadManager.get(url);

            if (image) {
                this.context.drawImage(
                    image, x, y, item.icon_width, item.icon_height);
                x = x + item.icon_width + this.toolbar_spacing;
            } else {
                Ws.ImagesLoadManager.load(
                    url,
                    function() {
                        self.__draw_toolbar(point);
                    });
                return;
            }
        }
    },

    draw_icon : function() {
        //  if (!this.icon) return;

        var x, y, self = this;

        var image_icon = Ws.ImagesLoadManager.get(Ws.Graph.DEFAULT_LINK_ICON);

        if (image_icon != null) {
            var points = this.__get_cp_points();
            var start_cp_x = points.start_cp_x;
            var start_cp_y = points.start_cp_y;
            var end_cp_x = points.end_cp_x;
            var end_cp_y = points.end_cp_y;
            var start_x = this.start_x;
            var start_y = this.start_y;
            var end_x = this.end_x;
            var end_y = this.end_y;

            x = 0.125 * (start_x + 3 * start_cp_x + 3 * end_cp_x + end_x);
            y = 0.125 * (start_y + 3 * start_cp_y + 3 * end_cp_y + end_y);

            // If we're on MSIE (excanvas is initialized), correct the initial position.
            if (typeof G_vmlCanvasManager != "undefined") {
                x -= 7;
                y -= 8;
            }

            this.context.drawImage(image_icon, x, y);
        } else {
            Ws.ImagesLoadManager.load(Ws.Graph.DEFAULT_LINK_ICON, function() {
                self.repaint();
            });
        }
    },

    __draw_animation: function() {
        var x, y, self = this;

        var image_icon = Ws.ImagesLoadManager.get(Ws.Graph.DEFAULT_LINK_MESSAGE_ICON);

        if (image_icon != null) {
            var points = this.__get_cp_points();
            var start_cp_x = points.start_cp_x;
            var start_cp_y = points.start_cp_y;
            var end_cp_x = points.end_cp_x;
            var end_cp_y = points.end_cp_y;
            var start_x = this.start_x;
            var start_y = this.start_y;
            var end_x = this.end_x;
            var end_y = this.end_y;
            var t = 0;
            if (this.__animation_position) {
                t = this.__animation_position;
            } else {
                this.__animation_position = 0;
            }

            x = (1 - t) * (1 - t) * (1 - t) * start_x +
            3 * t * (1 - t) *(1 - t) * start_cp_x +
            3 * t * t * (1 - t) * end_cp_x + t * t * t * end_x;

            y = (1 - t) * (1 - t) * (1 - t) * start_y +
            3 * t * (1 - t) *(1 - t) * start_cp_y +
            3 * t * t * (1 - t) * end_cp_y + t * t * t * end_y;

            // If we're on MSIE (excanvas is initialized), correct the initial position.
            if (typeof G_vmlCanvasManager != "undefined") {
                x -= 7;
                y -= 8;
            }

            if (this.__last_image) {
                this.context.putImageData(this.__last_image,
                    this.__last_position_x, this.__last_position_y);
            }
            this.__last_image = this.context.getImageData(x - 8, y - 8, 17, 17);
            this.__last_position_x = x - 8;
            this.__last_position_y = y - 8;

            this.context.drawImage(image_icon, x - 8, y - 8);
            this.__animation_position = this.__animation_position + 0.01;
            if (this.__animation_position > 1) {
                this.__animation_position = 0;
            }
        } else {
            Ws.ImagesLoadManager.load(Ws.Graph.DEFAULT_LINK_MESSAGE_ICON, function() {
                self.__draw_animation();
            });
        }
    },

    get_start_connector: function() {
        return this.link.start_node.ui.get_outbound_connector(
            this.link.start_node_connector);
    },

    get_end_connector: function() {
        return this.link.end_node.ui.get_inbound_connector(
            this.link.end_node_connector);
    },

    // Drag source methods ---------------------------------------------------------------
    ds_accept_drag: function(x, y, event) {
        var point = this.__get_real_point(x, y);

        if ((point.x > this.start_x - 6) &&
            (point.x < this.start_x + 6) &&
            (point.y > this.start_y - 6) &&
            (point.y < this.start_y + 6)) {

            this.dragged_start = true;
            this.dragged_end = false;

            return true;
        }

        if ((point.x > this.end_x - 6) &&
            (point.x < this.end_x + 6) &&
            (point.y > this.end_y - 6) &&
            (point.y < this.end_y + 6)) {

            this.dragged_start = false;
            this.dragged_end = true;
            
            return true;
        }

        return false;
    },

    ds_drag_started: function(x, y, event) {
        this.__dad_start = true;
        if (this.animation_interval) {
            clearInterval(this.animation_interval);
            this.animation_interval = null;
            this.__last_image = null;
        }
    },

    ds_dragged_to: function(x, y, event) {
        var point = this.__get_real_point_canvas(x, y);

        if (this.dragged_start) {
            this.link.start_node = null;

            this.start_x = point.x - this.x;
            this.start_y = point.y - this.y;

            this.repaint();
        }

        if (this.dragged_end) {
            this.link.end_node = null;

            this.end_x = point.x - this.x;
            this.end_y = point.y - this.y;

            this.repaint();
        }
    },

    ds_drag_finished: function(x, y, event) {
        this.dragged_start = false;
        this.dragged_end = false;
        var self = this;
        if (this.animation) {
            this.animation_interval = setInterval(function() {
                self.__draw_animation();
            }, 40);
        }

        this.repaint();
    },

    // Animation
    animated_start: function() {
        if (this.animation_interval) {
            return;
        }
        var self = this;
        this.animation_interval = setInterval(function() {
            self.__draw_animation();
        }, 40);
        this.animation = true;
    },

    animated_stop: function() {
        if (this.animation_interval) {
            clearInterval(this.animation_interval);
        }
        this.animation_interval = null;
        this.animation = false;
    },

    // Selectable methods ----------------------------------------------------------------
    sl_accept_selection: function(x, y, event) {
        var point = this.__get_real_point(x, y);

        if (this.cl_accept_click(x, y, event)) {
            return true;
        }

        if (Ws.Utils.BrowserDetect.browser != "Explorer") {

            //            for (var i = -2; i <= 2; i++) {
            //                for (var j = -2; j <= 2; j++) {
            //                    if (this.context.isPointInPath(point.x + i, point.y + j)) {
            //                        return true;
            //                    }
            //                }
            //            }
            return this.__check_point_link_path(point.x, point.y);
        }
        return false;
    },

    sl_select: function(x, y, event) {
        this.is_selected = true;
        //      this.selected_point = this.__get_real_point(x, y);

        this.repaint();

        if (this.animation) {
            var self = this;
            setInterval(function() {
                self.__moveToolbar(x,y);
            }, 40);
        }
    },

    sl_deselect: function(x, y, event, new_selection) {
        this.is_selected = false;
        this.selected_point = null;

        this.repaint();
    },

    // Clickable -------------------------------------------------------------------------
    cl_accept_click: function(x, y, event) {
        if (this.is_selected) {
            var point = this.__get_real_point(x, y);

            var points = this.__get_cp_points();
            var start_cp_x = points.start_cp_x;
            var start_cp_y = points.start_cp_y;
            var end_cp_x = points.end_cp_x;
            var end_cp_y = points.end_cp_y;
            var start_x = this.start_x;
            var start_y = this.start_y;
            var end_x = this.end_x;
            var end_y = this.end_y;

            var x1 = 0.125 * (start_x + 3 * start_cp_x + 3 * end_cp_x + end_x);
            var y1 = 0.125 * (start_y + 3 * start_cp_y + 3 * end_cp_y + end_y);

            var items = this.__get_toolbar_items();
            var r = items[0].icon_width / 2 + this.toolbar_spacing;
            var x2 = x1 + 2 * r +
                (2 * items[0].icon_width + this.toolbar_spacing) * (items.length - 1);
            var y2 = y1 + 2 * r;

            if ((point.x > x1) && (point.x < x2) &&
                (point.y > y1) && (point.y < y2)) {

                return true;
            }
            
        }
        return false;
    },

    cl_handle_click: function(x, y, event, options) {
        var point = this.__get_real_point(x, y);

        var points = this.__get_cp_points();
        var start_cp_x = points.start_cp_x;
        var start_cp_y = points.start_cp_y;
        var end_cp_x = points.end_cp_x;
        var end_cp_y = points.end_cp_y;
        var start_x = this.start_x;
        var start_y = this.start_y;
        var end_x = this.end_x;
        var end_y = this.end_y;

        var x1 = 0.125 * (start_x + 3 * start_cp_x + 3 * end_cp_x + end_x);
        var y1 = 0.125 * (start_y + 3 * start_cp_y + 3 * end_cp_y + end_y);

        var x2 = x1;
        var items = this.__get_toolbar_items();
        var y2 = y1 + items[0].icon_height + this.toolbar_spacing * 2;
        
        for (var i = 0; i < items.length; i++) {
            x1 = x2;

            x2 = x2 + items[i].icon_width + this.toolbar_spacing;
            if ((point.x > x1) && (point.x < x2) &&
                (point.y > y1) && (point.y < y2)) {

                items[i].callback(event, options);
                break;
            }
        }
    },

    cl_accept_doubleclick: function(x, y, event) {
        // do nothing;
        return false;
    },

    cl_handle_doubleclick: function(x, y, event, options) {
        // do nothing;
        return;
    },

    // Protected methods -----------------------------------------------------------------
    prepare_path: function() {
        var points = this.__get_cp_points();
        var arrow_A = points.arrow_A;
        var arrow_B = points.arrow_B;

        var leg_h = this.end_x - this.start_x;
        var leg_v = this.end_y - this.start_y;

        var start_cp_x = points.start_cp_x;
        var start_cp_y = points.start_cp_y;

        var end_cp_x = points.end_cp_x;
        var end_cp_y = points.end_cp_y;

        // Check whether we need to switch to a straight line: if both end and control
        // points are almost on the same 'y', we'll switch to straight horizontal, if
        // they're almost on the same 'x', we'll switch to straight vertical.
        var thold = this.switch_to_straight_line_threshold;
        if (((Math.abs(leg_h) < thold) && (Math.abs(end_cp_x - start_cp_x) < thold)) ||
            ((Math.abs(leg_v) < thold) && (Math.abs(end_cp_y - start_cp_y) < thold))) {

            start_cp_x = this.start_x;
            start_cp_y = this.start_y;
            end_cp_x = this.end_x;
            end_cp_y = this.end_y;
        }

        this.context.beginPath();
        this.context.moveTo(this.start_x, this.start_y);
        this.context.bezierCurveTo(
            start_cp_x,
            start_cp_y,
            end_cp_x,
            end_cp_y,
            this.end_x,
            this.end_y);

        this.context.moveTo(this.end_x, this.end_y);
        this.context.lineTo(arrow_A.x, arrow_A.y);
        this.context.lineTo(arrow_B.x, arrow_B.y);
        this.context.lineTo(this.end_x, this.end_y);
    },

    __get_cp_points: function() {
        var start_cp_offset_x, start_cp_offset_y, end_cp_offset_x, end_cp_offset_y;
        var arrow_A, arrow_B;
        var connector, cp;

        var leg_h = this.end_x - this.start_x;
        var leg_v = this.end_y - this.start_y;
        var hypotenuse = Math.sqrt((leg_h * leg_h) + (leg_v * leg_v));

        var cp_offset = this.cp_offset;

        // Check whether we need to reduce the size of cp_offset: if the 'offset_ratio'
        // part of the distance between endpoints is smaller than the current offset,
        // we will reduce it. But not to a value smaller than the minimum.
        if (this.reduce_cp_offset &&
            (cp_offset > hypotenuse * this.reduce_cp_offset_ratio)) {

            cp_offset = Math.floor(hypotenuse * this.reduce_cp_offset_ratio);

            if (cp_offset < this.reduce_cp_offset_minimum) {
                cp_offset = this.reduce_cp_offset_minimum;
            }
        }

        switch (this.start_type) {
            case Ws.Graph.NODE_CONNECTOR_WEST:
                start_cp_offset_x = -cp_offset;
                start_cp_offset_y = 0;
                break;
            case Ws.Graph.NODE_CONNECTOR_EAST:
                start_cp_offset_x = cp_offset;
                start_cp_offset_y = 0;
                break;
            case Ws.Graph.NODE_CONNECTOR_SOUTH:
                start_cp_offset_x = 0;
                start_cp_offset_y = cp_offset;
                break;
            case Ws.Graph.NODE_CONNECTOR_NORTH:
                start_cp_offset_x = 0;
                start_cp_offset_y = -cp_offset;
                break;
            case Ws.Graph.NODE_CONNECTOR_ORTHOGONAL:
                if (this.link.start_node) {
                    connector = this.get_start_connector();
                    cp = this.link.start_node.ui.__get_orth_point(
                        connector, cp_offset);

                    start_cp_offset_x = cp.x - connector.x;
                    start_cp_offset_y = cp.y - connector.y;

                    break;
                }
            // If we don't have a start node, we fall back to freeform behavior.
            case Ws.Graph.NODE_CONNECTOR_FREEFORM:
            default:
                start_cp_offset_x = cp_offset * (leg_h / hypotenuse);
                start_cp_offset_y = cp_offset * (leg_v / hypotenuse);
        }

        switch (this.end_type) {
            case Ws.Graph.NODE_CONNECTOR_WEST:
                end_cp_offset_x = -cp_offset;
                end_cp_offset_y = 0;
                arrow_A = {
                    x: this.end_x - 5,
                    y: this.end_y - 2
                }; // TODO: parametrize
                arrow_B = {
                    x: this.end_x - 5,
                    y: this.end_y + 2
                }; // TODO: parametrize
                break;
            case Ws.Graph.NODE_CONNECTOR_EAST:
                end_cp_offset_x = cp_offset;
                end_cp_offset_y = 0;
                arrow_A = {
                    x: this.end_x + 5,
                    y: this.end_y - 2
                }; // TODO: parametrize
                arrow_B = {
                    x: this.end_x + 5,
                    y: this.end_y + 2
                }; // TODO: parametrize
                break;
            case Ws.Graph.NODE_CONNECTOR_SOUTH:
                end_cp_offset_x = 0;
                end_cp_offset_y = cp_offset;
                arrow_A = {
                    x: this.end_x - 2,
                    y: this.end_y + 5
                }; // TODO: parametrize
                arrow_B = {
                    x: this.end_x + 2,
                    y: this.end_y + 5
                }; // TODO: parametrize
                break;
            case Ws.Graph.NODE_CONNECTOR_NORTH:
                end_cp_offset_x = 0;
                end_cp_offset_y = -cp_offset;
                arrow_A = {
                    x: this.end_x - 2,
                    y: this.end_y - 5
                }; // TODO: parametrize
                arrow_B = {
                    x: this.end_x + 2,
                    y: this.end_y - 5
                }; // TODO: parametrize
                break;
            case Ws.Graph.NODE_CONNECTOR_ORTHOGONAL:
                if (this.link.end_node) {
                    connector = this.get_end_connector();
                    cp = this.link.end_node.ui.__get_orth_point(
                        connector, cp_offset);

                    end_cp_offset_x = cp.x - connector.x;
                    end_cp_offset_y = cp.y - connector.y;

                    var arrow_head = Ws.Utils.get_arrow_head_coordinates(
                        cp, connector, 5, 2);

                    arrow_A = {
                        x: this.end_x - (connector.x - arrow_head.C.x),
                        y: this.end_y - (connector.y - arrow_head.C.y)
                    };
                    arrow_B = {
                        x: this.end_x - (connector.x - arrow_head.D.x),
                        y: this.end_y - (connector.y - arrow_head.D.y)
                    };

                    break;
                }
            // Important. If we don't have an end node, we fall back to freeform
            // behavior
            case Ws.Graph.NODE_CONNECTOR_FREEFORM:
            default:
                var sine = leg_v / hypotenuse;
                var cosine = leg_h / hypotenuse;
                end_cp_offset_x = -(cp_offset * cosine);
                end_cp_offset_y = -(cp_offset * sine);
                arrow_A = {
                    x: this.end_x - (5 * cosine) - (2 * sine),
                    y: this.end_y - (5 * sine) + (2 * cosine)
                }; // TODO: parametrize
                arrow_B = {
                    x: this.end_x - (5 * cosine) + (2 * sine),
                    y: this.end_y - (5 * sine) - (2 * cosine)
                }; // TODO: parametrize
        }

        return {
            start_cp_x : this.start_x + start_cp_offset_x,
            start_cp_y : this.start_y + start_cp_offset_y,
            end_cp_x : this.end_x + end_cp_offset_x,
            end_cp_y : this.end_y + end_cp_offset_y,
            arrow_A : arrow_A,
            arrow_B : arrow_B
        }

    },

    __check_point_link_path: function(x, y) {
        var points = this.__get_cp_points();
        var start_cp_x = points.start_cp_x;
        var start_cp_y = points.start_cp_y;
        var end_cp_x = points.end_cp_x;
        var end_cp_y = points.end_cp_y;
        var start_x = this.start_x;
        var start_y = this.start_y;
        var end_x = this.end_x;
        var end_y = this.end_y;

        var x_t = 0;
        var y_t = 0;
        var t = 0;
        var N = 150; //namber of iteration
        var dt = 1 / (N - 1);
        var r = 5;
        for (var i = 0; i < N; i++) {
            x_t = (1 - t) * (1 - t) * (1 - t) * start_x +
            3 * t * (1 - t) *(1 - t) * start_cp_x +
            3 * t * t * (1 - t) * end_cp_x + t * t * t * end_x;

            y_t = (1 - t) * (1 - t) * (1 - t) * start_y +
            3 * t * (1 - t) *(1 - t) * start_cp_y +
            3 * t * t * (1 - t) * end_cp_y + t * t * t * end_y;

            r = Math.sqrt((x_t - x) * (x_t - x) + (y_t - y) * (y_t - y));
            if (r < this.line_width / 2 + 3.5) {
                return true;
            }
            t += dt;
        }

        return false;
    },

    __get_real_point: function(x, y) {
        var offset = Element.cumulativeOffset(this.element);
        var scroll_offset = Element.cumulativeScrollOffset(this.element);

        return {
            x: x - offset.left + scroll_offset.left,
            y: y - offset.top + scroll_offset.top
        };
    },

    __get_real_point_canvas: function(x, y) {
        var offset = Element.cumulativeOffset(this.graph_ui.element);
        var scroll_offset = Element.cumulativeScrollOffset(this.graph_ui.element);

        return {
            x: x - offset.left + scroll_offset.left,
            y: y - offset.top + scroll_offset.top
        };
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});

//////////////////////////////////////////////////////////////////////////////////////////
// Constants
Ws.Graph.DEFAULT_LINK_TYPE = "default";

// ICON
Ws.Graph.DEFAULT_LINK_ICON = "img/link.png";
Ws.Graph.DEFAULT_LINK_MESSAGE_ICON = "img/message_16.png";


// Whether tooltips are enabled or not (it is usefult o disable them for, say, tests. ----
Ws.Graph.LINK_TOOLTIPS_ENABLED = true;

//////////////////////////////////////////////////////////////////////////////////////////
// Static initialization
Ws.Graph.LinkFactory.register_type(
    Ws.Graph.DEFAULT_LINK_TYPE, Ws.Graph.DefaultLink);
Ws.Graph.LinkUiFactory.register_type(
    Ws.Graph.DEFAULT_LINK_TYPE, Ws.Graph.DefaultLinkUi);
