var cbr_container = {
    pc_get_descriptors: function() {
        var container = {
            properties: []
        };

        var cases = ["asd", "fgh"];
        var calculate_values = function(number) {
            var value = [];

            for (var i = 0; i < number; i++) {
                if (i < cases.length) {
                    value[i] = cases[i];
                } else {
                    value[i] = "";
                }
            }

            return value;
        }

        container.properties.push(
            {
                name: "properties_expression_type",
                type: Ws.PropertiesSheet.ENUM,
                display_name: "Type",
                description: "Type of the expression used to route incoming messages.",
                value: "xpath",

                values: [
                    {
                        display_name: "XPath",
                        description: "XPath expression. Works best when used with XML data.",
                        value: "xpath"
                    },
                    {
                        display_name: "Regular Expression",
                        description: "Regular Expression. Most useful for non-XML textual data.",
                        value: "regex"
                    }
                ]
            }
            ,
            {
                name: "properties_use_external_config",
                type: Ws.PropertiesSheet.BOOLEAN,

                display_name: "Use External",
                description: "Whether the configuration data for this EIP is inlined or loaded from an external file.",
                value: false
            }
            ,
            {
                name: "properties_expression",
                type: Ws.PropertiesSheet.CODE,
                code_type: "xml",
                display_name: "Expression",
                description: "Expression to evaluate on the incoming messages.",
                value: "",

                auto_adjust_height: true,
                maximizable: true,

                depends: {
                    name: "properties_use_external_config",
                    value: false
                }
            }
            ,
            {
                name: "properties_cases_number",
                type: Ws.PropertiesSheet.ENUM,
                display_name: "# of Routes",
                description: "Number of outbound routes for this router.",
                value: "2",

                values: [
                    {
                        display_name: "1",
                        description: "",
                        value: "1"
                    },
                    {
                        display_name: "2",
                        description: "",
                        value: "2"
                    },
                    {
                        display_name: "3",
                        description: "",
                        value: "3"
                    },
                    {
                        display_name: "4",
                        description: "",
                        value: "4"
                    },
                    {
                        display_name: "5",
                        description: "",
                        value: "5"
                    }
                ]
            }
            ,
            {
                name: "properties_cases",
                type: Ws.PropertiesSheet.ARRAY,
                
                display_name: "Route",
                description: "Condition which must be satisfied for this route to become active.",

                display_name_suffix: " %i",
                description_suffix: "",
                items_type: Ws.PropertiesSheet.STRING,

                dynamic: [
                    {
                        name: "properties_cases_number",
                        value: "1",
                        
                        changes: {
                            value: calculate_values(1)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "2",

                        changes: {
                            value: calculate_values(2)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "3",

                        changes: {
                            value: calculate_values(3)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "4",

                        changes: {
                            value: calculate_values(4)
                        }
                    },
                    {
                        name: "properties_cases_number",
                        value: "5",

                        changes: {
                            value: calculate_values(5)
                        }
                    },
                ],

                dynamic_defaults: {
                    value: ["asd", "fgh"]
                }
            }
            ,
            {
                name: "properties_external_config_name",
                type: Ws.PropertiesSheet.STRING,

                display_name: "Config name",
                description: "Name of the extenrnal configuration.",
                value: "",

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config",
                type: Ws.PropertiesSheet.BUTTON,

                display_name: "Edit Configuration",
                description: "Opens the external configuragion file in the editor.",
                action: function(psheet) {
                },

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
        );
        
        return container;
    },

    pc_set_properties: function(properties) {
        var html = "";

        for (var i in properties) {
            html += "" + i + " => " + properties[i] + "<br/>";
        }

        $("logger").innerHTML = html;
    }
};
