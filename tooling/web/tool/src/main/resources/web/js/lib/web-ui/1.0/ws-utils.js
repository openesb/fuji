/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
/////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Utils = {
    // Dynamic graphics: rounded rectangle -----------------------------------------------
    draw_rr_top_left: function(
            canvas, width, height, radius, fill, stroke, stroke_width) {
                
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        if (width < radius + hs) {
            real_width = radius + hs;
        } else {
            real_width = width;
        }
        if (height < radius + hs) {
            real_height = radius + hs;
        } else {
            real_height = height;
        }
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }

        if (fill) {
            context.beginPath();
            context.moveTo(hs, real_height);
            context.lineTo(hs, radius + hs)
            context.quadraticCurveTo(hs, hs, radius + hs, hs);
            context.lineTo(real_width, hs);
            context.lineTo(real_width, real_height);
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(hs, real_height);
            context.lineTo(hs, radius + hs)
            context.quadraticCurveTo(hs, hs, radius + hs, hs);
            context.lineTo(real_width, hs);
    
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_top: function(canvas, height, fill, stroke, stroke_width) {
        var WIDTH = 2000;
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        real_width = WIDTH;
        if (height < stroke_width) {
            real_height = stroke_width;
        } else {
            real_height = height;
        }
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(0, hs);
            context.lineTo(real_width, hs);
            context.lineTo(real_width, real_height);
            context.lineTo(0, real_height);
            context.closePath();
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(0, hs);
            context.lineTo(real_width, hs);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_top_right: function(
            canvas, width, height, radius, fill, stroke, stroke_width) {

        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        if (width < radius + hs) {
            real_width = radius + hs;
        } else {
            real_width = width;
        }
        if (height < radius + hs) {
            real_height = radius + hs;
        } else {
            real_height = height;
        }
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(0, hs);
            context.lineTo(real_width - radius - hs, hs)
            context.quadraticCurveTo(real_width - hs, hs, real_width - hs, radius + hs);
            context.lineTo(real_width - hs, real_height);
            context.lineTo(0, real_height);
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(0, hs);
            context.lineTo(real_width - radius - hs, hs)
            context.quadraticCurveTo(real_width - hs, hs, real_width - hs, radius + hs);
            context.lineTo(real_width - hs, real_height);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_left: function(canvas, width, fill, stroke, stroke_width) {
        var HEIGHT = 2000;
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        if (width < stroke_width) {
            real_width = stroke_width;
        } else {
            real_width = width;
        }
        real_height = HEIGHT;
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(hs, 0);
            context.lineTo(hs, real_height);
            context.lineTo(real_width, real_height);
            context.lineTo(real_width, 0);
            context.closePath();
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(hs, 0);
            context.lineTo(hs, real_height);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_center: function(canvas, fill) {
        var WIDTH = 2000;
        var HEIGHT = 2000;

        var context = canvas.getContext("2d");
        
        canvas.width = WIDTH;
        canvas.height = HEIGHT;
        
        canvas.setStyle({
            position: "absolute",
            width: WIDTH + "px",
            height: HEIGHT + "px"
        });

        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: WIDTH + "px",
                height: HEIGHT + "px"
            });
        }
        
        if (fill) {
            context.fillStyle = fill;
            context.fillRect(0, 0, WIDTH, HEIGHT);
        }
    },
    
    draw_rr_right: function(canvas, width, fill, stroke, stroke_width) {
        var HEIGHT = 2000;
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        if (width < stroke_width) {
            real_width = stroke_width;
        } else {
            real_width = width;
        }
        real_height = HEIGHT;
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(real_width - hs, 0);
            context.lineTo(real_width - hs, real_height);
            context.lineTo(0, real_height);
            context.lineTo(0, 0);
            context.closePath();
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(real_width - hs, 0);
            context.lineTo(real_width - hs, real_height);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_bottom_left: function(
            canvas, width, height, radius, fill, stroke, stroke_width) {
                
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        if (width < radius + hs) {
            real_width = radius + hs;
        } else {
            real_width = width;
        }
        if (height < radius + hs) {
            real_height = radius + hs;
        } else {
            real_height = height;
        }
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(hs, 0);
            context.lineTo(hs, real_height - radius - hs)
            context.quadraticCurveTo(
                    hs, real_height - hs, radius + hs, real_height - hs);
            context.lineTo(real_width, real_height - hs);
            context.lineTo(real_width, 0);
            context.closePath();
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(hs, 0);
            context.lineTo(hs, real_height - radius - hs)
            context.quadraticCurveTo(
                    hs, real_height - hs, radius + hs, real_height - hs);
            context.lineTo(real_width, real_height - hs);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_bottom: function(canvas, height, fill, stroke, stroke_width) {
        var WIDTH = 2000;
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        real_width = WIDTH;
        if (height < stroke_width) {
            real_height = stroke_width;
        } else {
            real_height = height;
        }
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(0, real_height - hs);
            context.lineTo(real_width, real_height - hs);
            context.lineTo(real_width, 0);
            context.lineTo(0, 0);
            context.closePath();
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(0, real_height - hs);
            context.lineTo(real_width, real_height - hs);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    draw_rr_bottom_right: function(
            canvas, width, height, radius, fill, stroke, stroke_width) {
                
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var hs = stroke_width / 2;
        
        var real_width, real_height;
        if (width < radius + hs) {
            real_width = radius + hs;
        } else {
            real_width = width;
        }
        if (height < radius + hs) {
            real_height = radius + hs;
        } else {
            real_height = height;
        }
        
        canvas.width = real_width;
        canvas.height = real_height;
        
        canvas.setStyle({
            position: "absolute",
            width: real_width + "px",
            height: real_height + "px"
        });
        
        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: real_width + "px",
                height: real_height + "px"
            });
        }
        
        if (fill) {
            context.beginPath();
            context.moveTo(0, real_height - hs);
            context.lineTo(real_width - radius - hs, real_height - hs)
            context.quadraticCurveTo(
                    real_width - hs, real_height - hs,
                    real_width - hs, real_height - radius - hs);
            context.lineTo(real_width - hs, 0);
            context.lineTo(0, 0);
            context.closePath();
            
            context.fillStyle = fill;
            context.fill();
        }
        
        if (stroke) {
            context.beginPath();
            context.moveTo(0, real_height - hs);
            context.lineTo(real_width - radius - hs, real_height - hs)
            context.quadraticCurveTo(
                    real_width - hs, real_height - hs,
                    real_width - hs, real_height - radius - hs);
            context.lineTo(real_width - hs, 0);
            
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.stroke();
        }
    },
    
    // Dynamic graphics: miscellanea -----------------------------------------------------
    draw_close_handle: function(canvas, size, fill, stroke, stroke_width) {
        var context = canvas.getContext("2d");

        canvas.width = size;
        canvas.height = size;

        canvas.setStyle({
            width: size + "px",
            height: size + "px"
        });

        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);

            canvas.firstChild.setStyle({
                width: size + "px",
                height: size + "px"
            });
        }

        var radius = size / 2;

        context.fillStyle = fill;

        context.beginPath();
        context.moveTo(radius, radius);
        context.arc(radius, radius, radius, 0, Math.PI * 2, false);
        context.fill();

        context.strokeStyle = stroke;
        context.lineWidth = stroke_width;
        context.lineCap = "round";

        context.beginPath();
        context.moveTo(radius - 3, radius - 3);
        context.lineTo(radius + 3, radius + 3);
        context.moveTo(radius - 3, radius + 3);
        context.lineTo(radius + 3, radius - 3);
        context.stroke();
    },

    draw_resize_handle: function(canvas, size, stroke, stroke_width) {
        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        canvas.width = size;
        canvas.height = size;
        
        canvas.setStyle({
            width: size + "px",
            height: size + "px"
        });

        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: size + "px",
                height: size + "px"
            });
        }
        
        var hs = stroke_width / 2;
        
        context.beginPath();
        
        context.moveTo(size - hs, hs);
        context.lineTo(hs, size - hs);
        
        context.moveTo(size - hs, (size / 3) + hs);
        context.lineTo((size / 3) + hs, size - hs);
        
        context.moveTo(size - hs, (size / 1.5) + hs);
        context.lineTo((size / 1.5) + hs, size - hs);
        
        if (stroke) {
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            context.lineCap = "round";
            
            context.stroke();
        }
    },
    
    draw_progress_bar_line: function(
            canvas, size, line_length, line_width, line_span,
            angle, fill, stroke, stroke_width) {

        var context = canvas.getContext("2d");
        
        if (stroke) {
            if (typeof stroke_width == "undefined") stroke_width = 1;
        } else {
            stroke_width = 0;
        }
        
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);
        
        var hw = line_width / 2;
        
        context.save();
        context.translate(size / 2, size / 2);
        
        context.beginPath();
        context.moveTo((line_span * cos) + (hw * sin), (line_span * sin) - (hw * cos));
        context.arc(
                line_span * cos, line_span * sin, hw,
                -(Math.PI / 2) + angle, (Math.PI / 2) + angle, true);
        context.lineTo(
                ((line_span + line_length) * cos) - (hw * sin),
                ((line_span + line_length) * sin) + (hw * cos));
        context.arc(
                (line_span + line_length) * cos, (line_span + line_length) * sin, hw,
                (Math.PI / 2) + angle, -(Math.PI / 2) + angle, true);
        context.closePath();
        
        if (stroke) {
            context.strokeStyle = stroke;
            context.lineWidth = stroke_width;
            
            context.stroke();
        }
        
        if (fill) {
            context.fillStyle = fill;
            
            context.fill();
        }
        
        context.restore();
    },
    
    draw_progress_bar: function(
            canvas, size, num_lines, base_angle, fill, stroke, stroke_width) {

        var context = canvas.getContext("2d");
        
        canvas.width = size;
        canvas.height = size;
        
        canvas.setStyle({
            width: size + "px",
            height: size + "px"
        });

        // Handle the case of excanvas -- it does not properly resize the inner div
        if (canvas.firstChild) {
            Element.extend(canvas.firstChild);
            
            canvas.firstChild.setStyle({
                width: size + "px",
                height: size + "px"
            });
        }
        
        var line_length = size / 4;
        var line_width = size / 12;
        var line_span = size / 5;
        
        var angle_step = (Math.PI * 2) / num_lines;
        
        for (var i = 0; i < num_lines; i++) {
            context.globalAlpha = (i / num_lines) + 0.1;
            
            Ws.Utils.draw_progress_bar_line(
                    canvas, 
                    size, 
                    line_length, 
                    line_width, 
                    line_span, 
                    base_angle + (i * angle_step),
                    fill,
                    stroke,
                    stroke_width);
        }
    },
    
    create_linear_gradient: function(x1, y1, x2, y2) {
        var canvas = this.create_canvas();
        var context = canvas.getContext("2d");
        
        return context.createLinearGradient(x1, y1, x2, y2);
    },
    
    create_radial_gradient: function(x1, y1, r1, x2, y2, r2) {
        var canvas = this.create_canvas();
        var context = canvas.getContext("2d");
        
        return context.createRadialGradient(x1, y1, r1, x2, y2, r2);
    },
    
    create_canvas: function() {
        var canvas = new Element("canvas");

        // Handle the excanvas case, where we need to manually create a context
        if (typeof G_vmlCanvasManager != "undefined") {
            var excanvas = G_vmlCanvasManager;
            
            excanvas.initElement(canvas);
        }

        return canvas;
    },

    create_gradients_template: function(radius, rgba_in, rgba_out) {
        var t_gradient = Ws.Utils.create_linear_gradient(0, 0, 0, radius);
        t_gradient.addColorStop(0, rgba_out);
        t_gradient.addColorStop(1, rgba_in);

        var tl_gradient = Ws.Utils.create_radial_gradient(radius, radius, 0,
            radius, radius, radius);
        tl_gradient.addColorStop(1, rgba_out);
        tl_gradient.addColorStop(0, rgba_in);

        var l_gradient = Ws.Utils.create_linear_gradient(radius, 0, 0, 0);
        l_gradient.addColorStop(1, rgba_out);
        l_gradient.addColorStop(0, rgba_in);

        var bl_gradient = Ws.Utils.create_radial_gradient(
                radius, 0, 0, radius, 0, radius);
        bl_gradient.addColorStop(1, rgba_out);
        bl_gradient.addColorStop(0, rgba_in);

        var b_gradient = Ws.Utils.create_linear_gradient(0, 0, 0, radius);
        b_gradient.addColorStop(1, rgba_out);
        b_gradient.addColorStop(0, rgba_in);

        var br_gradient = Ws.Utils.create_radial_gradient(0, 0, 0, 0, 0, radius);
        br_gradient.addColorStop(1, rgba_out);
        br_gradient.addColorStop(0, rgba_in);

        var r_gradient = Ws.Utils.create_linear_gradient(0, 0, radius, 0);
        r_gradient.addColorStop(1, rgba_out);
        r_gradient.addColorStop(0, rgba_in);

        var tr_gradient = Ws.Utils.create_radial_gradient(
                0, radius, 0, 0, radius, radius);
        tr_gradient.addColorStop(1, rgba_out);
        tr_gradient.addColorStop(0, rgba_in);

        return  {
                    radius: radius,

                    t_fill: t_gradient,
                    tl_fill: tl_gradient,
                    l_fill: l_gradient,
                    bl_fill: bl_gradient,
                    b_fill: b_gradient,
                    br_fill: br_gradient,
                    r_fill: r_gradient,
                    tr_fill: tr_gradient,

                    fill: rgba_in
        }
    },
    
    // Element handling: resize ----------------------------------------------------------
    resize_to_fill_viewport: function(element) {
        Ws.Utils.resize_to_fill_height(element);
        Ws.Utils.resize_to_fill_width(element);
    },
    
    resize_to_fill_height: function(element) {
        var border_top = parseInt(element.getStyle("border-top-width"));
        var border_bottom = parseInt(element.getStyle("border-bottom-width"));
        var margin_top = parseInt(element.getStyle("margin-top"));
        var margin_bottom = parseInt(element.getStyle("margin-bottom"));
        var padding_top = parseInt(element.getStyle("padding-top"));
        var padding_bottom = parseInt(element.getStyle("padding-bottom"));
        
        border_top = isNaN(border_top) ? 0 : border_top;
        border_bottom = isNaN(border_bottom) ? 0 : border_bottom;
        margin_top = isNaN(margin_top) ? 0 : margin_top;
        margin_bottom = isNaN(margin_bottom) ? 0 : margin_bottom;
        padding_top = isNaN(padding_top) ? 0 : padding_top;
        padding_bottom = isNaN(padding_bottom) ? 0 : padding_bottom;
        
        var window_dimensions = Ws.Utils.get_window_dimensions();
        
        var new_height = window_dimensions.height -
                border_top - border_bottom - 
                margin_top - margin_bottom - 
                padding_top - padding_bottom;
        
        element.setStyle({
            height: new_height + "px"
        });
    },
    
    resize_to_fill_parent_width: function(element) {
        var element_bmp = Ws.Utils.get_bmp(element);
        var parent_dimensions = Ws.Utils.get_dimensions(element.parentNode);
        
        var new_width = parent_dimensions.width - element_bmp.get_bmp_horizontal();

        element.setStyle({
            width: new_width + "px"
        });
    },

    // Composite (or just complex) components generation ---------------------------------
    create_gradiented_rounded_rectangle: function(
            element, radius, rgba_in, rgba_out) {
                
        var t_gradient = Ws.Utils.create_linear_gradient(0, 0, 0, radius);
        t_gradient.addColorStop(0, rgba_out);
        t_gradient.addColorStop(1, rgba_in);

        var tl_gradient = Ws.Utils.create_radial_gradient(
                radius, radius, 0, radius, radius, radius);
        tl_gradient.addColorStop(1, rgba_out);
        tl_gradient.addColorStop(0, rgba_in);

        var l_gradient = Ws.Utils.create_linear_gradient(radius, 0, 0, 0);
        l_gradient.addColorStop(1, rgba_out);
        l_gradient.addColorStop(0, rgba_in);

        var bl_gradient = Ws.Utils.create_radial_gradient(
                radius, 0, 0, radius, 0, radius);
        bl_gradient.addColorStop(1, rgba_out);
        bl_gradient.addColorStop(0, rgba_in);

        var b_gradient = Ws.Utils.create_linear_gradient(0, 0, 0, radius);
        b_gradient.addColorStop(1, rgba_out);
        b_gradient.addColorStop(0, rgba_in);

        var br_gradient = Ws.Utils.create_radial_gradient(0, 0, 0, 0, 0, radius);
        br_gradient.addColorStop(1, rgba_out);
        br_gradient.addColorStop(0, rgba_in);

        var r_gradient = Ws.Utils.create_linear_gradient(0, 0, radius, 0);
        r_gradient.addColorStop(1, rgba_out);
        r_gradient.addColorStop(0, rgba_in);

        var tr_gradient = Ws.Utils.create_radial_gradient(
                0, radius, 0, 0, radius, radius);
        tr_gradient.addColorStop(1, rgba_out);
        tr_gradient.addColorStop(0, rgba_in);

        new Ws.RoundedRectangle(element, {
            radius: radius,

            t_fill: t_gradient,
            tl_fill: tl_gradient,
            l_fill: l_gradient,
            bl_fill: bl_gradient,
            b_fill: b_gradient,
            br_fill: br_gradient,
            r_fill: r_gradient,
            tr_fill: tr_gradient,

            fill: rgba_in
        });
    },

    // Miscellanea -----------------------------------------------------------------------
    get_scrollbar_size: function() {
        var outer = new Element("div");
        outer.setStyle({
            position: "absolute",
            left: "-200px",
            top: "-200px",
            overflow: "hidden",
            width: "100px",
            height: "100px"
        });
        var inner = new Element("div");
        inner.setStyle({
            height: "500px"
        });
        
        outer.appendChild(inner);
        document.body.appendChild(outer);
        
        var width_hidden = inner.getWidth();
        
        outer.setStyle({
            overflow: "auto"
        });
        
        var width_scroll = inner.getWidth();
        
        document.body.removeChild(outer);
        
        return width_hidden - width_scroll;
    },
    
    get_window_dimensions: function() {
        if (document.documentElement.clientWidth) {
            if ((Ws.Utils.BrowserDetect.browser == "Opera") && 
                    (Ws.Utils.BrowserDetect.version < 9.5)) {
                
                var wi_height = window.innerHeight;
                var dc_height = document.documentElement.clientHeight;
                
                var real_height = wi_height;
                if (dc_height > wi_height) {
                    real_height -= scrollbar_size;
                }
                
                return {
                    width: document.documentElement.clientWidth,
                    height: real_height
                }
            } else {
                return {
                    width: document.documentElement.clientWidth,
                    height: document.documentElement.clientHeight
                }
            }
        } else {
            return {
                width: -1,
                height: -1
            }
        }
    },
    
    apply_default: function(target, source, property_name, is_localized) {
        if (!is_localized) {
            if (!target[property_name]) target[property_name] = source[property_name];
        } else {
            for (var i in source) {
                if (i.indexOf(property_name) == 0) {
                    if (!target[i]) target[i] = source[i];
                }
            }
        }
    },
    
    apply_defaults: function(target, source) {
        for (i in source) {
            if (!target[i]) {
                target[i] = source[i];
            }
        }
    },
    
    clone_object: function(object) {
        // Check whether the object is a primitive thingie. If it is, there is no need
        // to actually clone it.
        switch (typeof object) {
            case "string":
            case "number":
            case "boolean":
                return object;
        }

        var i, clone;

        // If the object is an array, create a new array and "for (i in XXX)" clone its
        // contents.
        // http://ajaxian.com/archives/isarray-why-is-it-so-bloody-hard-to-get-right
        if (Object.prototype.toString.call(object) === '[object Array]') {
            clone = [];

            for (i = 0; i < object.length; i++) {
                clone[i] = object[i];
            }

            return clone;
        }

        clone = {};

        // If the object is a generic thing, iterate over its members, cloning each and
        // every one.
        for (i in object) {
            var value = object[i];
            
            if (value == null) continue; // We do not copy nulls
            
            switch (typeof value) {
                case "string":
                case "number":
                case "boolean":
                    clone[i] = value;
                    break;
                    
                case "undefined": // We do not copy undefined's
                case "function": // We do not copy functions
                    break;
                    
                default:
                    clone[i] = Ws.Utils.clone_object(value);
                    break;
            }
        }
        
        return clone;
    },

    get_arrow_head_coordinates: function(A, B, h_length, h_height) {
        
        // Arrow:                                  C                 |
        //                                           *--             |
        //                                              *-- (theta)  |
        //                                                 *--       |
        //                                         (omega)    *--    |
        //                                                       *-- |
        // A ======================================================== B
        //                                       -               *-- |
        //                                       |            *--    |
        //                                    h_height     *--       |
        //                                       |      *--   (psi)  |
        //                                       -   *--             |
        //                                         D                 |
        //                                           |<-  h_length ->|
        //   |<---------------------- length ----------------------->|
        //
        //             phi = theta + omega = Pi - (omega + psi)
        //

        var length = Math.sqrt(Math.pow(A.x - B.x, 2) + Math.pow(A.y - B.y, 2));
        var hr = Math.sqrt((h_length * h_length) + (h_height * h_height));

        var sin_phi = (A.y - B.y) / length;   // Angle of the arrow shaft itself
        var cos_phi = (A.x - B.x) / length;   //

        var sin_omega = h_height / hr;   // Angle between arrow head sides and its shaft
        var cos_omega = h_length / hr;   //

        var sin_theta = 
                (sin_phi * cos_omega) - (cos_phi * sin_omega);
        var cos_theta =
                (cos_phi * cos_omega) + (sin_phi * sin_omega);
        
        var sin_psi = 
                (cos_phi * cos_omega) - (sin_phi * sin_omega);
        var cos_psi = 
                (sin_phi * cos_omega) + (cos_phi * sin_omega);

        return {
            C: {
                x: B.x + (hr * cos_theta),
                y: B.y + (hr * sin_theta)
            },

            D: {
                x: B.x + (hr * sin_psi),
                y: B.y + (hr * cos_psi)
            }
        }
    },
    
    get_bmp: function(element) {
        return new Ws.__bmp_element(element);
    },

    get_dimensions: function(element) {
        if (element == document.body) {
            return Ws.Utils.get_window_dimensions();
        } else {
            return {
                width: Element.getWidth(element),
                height: Element.getHeight(element)
            };
        }
    },

    get_z_index: function(thing) {
        var z_index = 0;

        if (thing.z_index) { // Web-graph nodes and links
            z_index = thing.z_index;
        } else if (thing.element && thing.element.style) {
            z_index = parseInt(thing.element.getStyle("z-index"));

            if (isNaN(z_index)) z_index = 0;
        } else if (thing.style) { // HTML elements
            z_index = parseInt(thing.getStyle("z-index"));

            if (isNaN(z_index)) z_index = 0;
        }

        return z_index;
    },

    escape_entities: function(string) {
        var escaped = string;

        escaped = escaped.replace("&", "&amp;");
        escaped = escaped.replace("\"", "&quot;");
        escaped = escaped.replace("<", "&lt;");
        escaped = escaped.replace(">", "&gt;");

        return escaped;
    },

    unescape_entities: function(escaped) {
        var string = escaped;

        string = string.replace("&lt;", "<");
        string = string.replace("&gt;", ">");
        string = string.replace("&quot;", "\"");
        string = string.replace("&amp;", "&");

        return string;
    },

    /**
     * This method calculates the cumulative height of the children elements. However,
     * there are serious limitations for the use of it: it takes into account only 
     * 'block' and 'block'-like children, namely: <p>, <div>, <table>, <ol>, <ul> and 
     * those with 'display' set to 'block'.
     */
    get_children_height: function(element) {
        var total_height = 0;

        for (var i = 0; i < element.childNodes.length; i++) {
            var child = element.childNodes[i];
            var tagName = child.tagName;

            if (tagName) {
                tagName = tagName.toLowerCase();

                var qualifies = false;

                if ((tagName == "p") || (tagName == "div") || (tagName == "table") ||
                        (tagName == "ol") || (tagName == "ul")) {

                    qualifies = true;
                }
                
                if (child.style && (child.style.display == "block")) {
                    qualifies = true;
                }

                if (qualifies) {
                    total_height += child.offsetHeight;
                    total_height += Ws.Utils.get_bmp(child).get_margin_vertical();
                }
            }
        }

        return total_height;
    },

    /**
     * This methods examines two objects field by field, excluding functions (we consider
     * them inessential for this matter), going recursively deep for complex fields,
     * trying to find out whether they are identical or not. If we're asked to compare
     * two objects of which one is a function, the result is not defined.
     */
    equals: function(obj1, obj2) {
        var i;

        // Simplest case: if obj1 evalutes to 'false' (is null, undefined or boolean
        // false) we just need to check whether obj2 is the same.
        if (!obj1) {
            return !obj2;
        }
        if (!obj2) {
            return !obj1;
        }

        // For primitives we simply compare them.
        switch (typeof obj1) {
            case "string":
            case "number":
            case "boolean":
                return obj1 == obj2;
        }

        // For arrays we iterate over the array and recusively compare each item.
        if (Object.prototype.toString.call(obj1) === '[object Array]') {
            if (obj1.length != obj2.length) {
                return false;
            }

            for (i = 0; i < obj1.length; i++) {
                if (!Ws.Utils.equals(obj1[i], obj2[i])) {
                    return false;
                }
            }
        }

        // For objects iterate over the fields, recursively comparing them.
        for (i in obj1) {
            if ((typeof obj1[i] != "function") && (!Ws.Utils.equals(obj1[i], obj2[i]))) {
                return false;
            }
        }
        for (i in obj2) {
            // If we manage to find a field in obj2, which is missing from in obj1, they
            // are not equal.
            if ((typeof obj2[i] != "function") && (typeof obj1[i] == "undefined")) {
                return false;
            }
        }

        return true;
    },

    // Browser detection -----------------------------------------------------------------
    // This is a copy of the script provided by Peter-Paul Koch and available at
    // http://www.quirksmode.org/js/detect.html 
    // The licensing terms are unclear, assuming it is safe to be used.
    BrowserDetect: {
        init: function() {
            this.browser = this.search_string(this.browser_data) || 
                    "An unknown browser";
            this.version = this.search_version(navigator.userAgent) || 
                    this.search_version(navigator.appVersion) || 
                    "an unknown version";
            this.os = this.search_string(this.os_data) || "an unknown OS";
        },
        
        search_string: function(data) {
            for (var i = 0; i < data.length; i++) {
                var data_string = data[i].string;
                var data_prop = data[i].prop;
                
                this.version_search_string = 
                        data[i].versionSearch || data[i].identity;
                
                if (data_string) {
                    if (data_string.indexOf(data[i].subString) != -1) {
                        return data[i].identity;
                    }
                } else if (data_prop) {
                    return data[i].identity;
                }
            }
        },
        
        search_version: function(data_string) {
            var index = data_string.indexOf(this.version_search_string);
            if (index == -1) return;
            
            return parseFloat(data_string.substring(
                    index + this.version_search_string.length + 1));
        },
        
        browser_data: [
            {   string: navigator.userAgent,
                subString: "OmniWeb",
                versionSearch: "OmniWeb/",
                identity: "OmniWeb"
            },
            {
                string: navigator.vendor,
                subString: "Apple",
                identity: "Safari"
            },
            {
                prop: window.opera,
                identity: "Opera"
            },
            {
                string: navigator.vendor,
                subString: "iCab",
                identity: "iCab"
            },
            {
                string: navigator.vendor,
                subString: "KDE",
                identity: "Konqueror"
            },
            {
                string: navigator.userAgent,
                subString: "Firefox",
                identity: "Firefox"
            },
            {
                string: navigator.vendor,
                subString: "Camino",
                identity: "Camino"
            },
            {   // for newer Netscapes (6+)
                string: navigator.userAgent,
                subString: "Netscape",
                identity: "Netscape"
            },
            {
                string: navigator.userAgent,
                subString: "MSIE",
                identity: "Explorer",
                versionSearch: "MSIE"
            },
            {
                string: navigator.userAgent,
                subString: "Gecko",
                identity: "Mozilla",
                versionSearch: "rv"
            },
            {   // for older Netscapes (4-)
                string: navigator.userAgent,
                subString: "Mozilla",
                identity: "Netscape",
                versionSearch: "Mozilla"
            }
        ],
        
        os_data: [
            {
                string: navigator.platform,
                subString: "Win",
                identity: "Windows"
            },
            {
                string: navigator.platform,
                subString: "Mac",
                identity: "Mac"
            },
            {
                string: navigator.platform,
                subString: "Linux",
                identity: "Linux"
            }
        ]
    },
        
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
};

Ws.__bmp_element = Class.create({
    __element : null,

    initialize: function(element) {
        this.__element = element;
        Element.extend(this.__element);
    },

    get_border_top: function() {
        var border_top = parseInt(this.__element.getStyle("border-top-width"));
        return isNaN(border_top) ? 0 : border_top;
    },

    get_border_left: function() {
        var border_left = parseInt(this.__element.getStyle("border-left-width"));
        return isNaN(border_left) ? 0 : border_left;
    },

    get_border_bottom: function() {
        var border_bottom = parseInt(this.__element.getStyle("border-bottom-width"));
        return isNaN(border_bottom) ? 0 : border_bottom;
    },

    get_border_right: function() {
        var border_right = parseInt(this.__element.getStyle("border-right-width"));
        return isNaN(border_right) ? 0 : border_right;
    },

    get_margin_top: function() {
        var margin_top = parseInt(this.__element.getStyle("margin-top"));
        return isNaN(margin_top) ? 0 : margin_top;
    },

    get_margin_left: function() {
        var margin_left = parseInt(this.__element.getStyle("margin-left"));
        return isNaN(margin_left) ? 0 : margin_left;
    },

    get_margin_bottom: function() {
        var margin_bottom = parseInt(this.__element.getStyle("margin-bottom"));
        return isNaN(margin_bottom) ? 0 : margin_bottom;
    },

    get_margin_right: function() {
        var margin_right = parseInt(this.__element.getStyle("margin-right"));
        return isNaN(margin_right) ? 0 : margin_right;
    },

    get_padding_top: function() {
        var padding_top = parseInt(this.__element.getStyle("padding-top"));
        return isNaN(padding_top) ? 0 : padding_top;
    },

    get_padding_left: function() {
        var padding_left = parseInt(this.__element.getStyle("padding-left"));
        return isNaN(padding_left) ? 0 : padding_left;
    },

    get_padding_bottom: function() {
        var padding_bottom = parseInt(this.__element.getStyle("padding-bottom"));
        return isNaN(padding_bottom) ? 0 : padding_bottom;
    },

    get_padding_right: function() {
        var padding_right = parseInt(this.__element.getStyle("padding-right"));
        return isNaN(padding_right) ? 0 : padding_right;
    },

    get_bp_top: function() {
        return this.get_border_top() + this.get_padding_top();
    },

    get_bp_left: function() {
        return this.get_border_left() + this.get_padding_left();
    },

    get_bp_bottom: function() {
        return this.get_border_bottom() + this.get_padding_bottom();
    },

    get_bp_right: function() {
        return this.get_border_right() + this.get_padding_right();
    },

    get_bmp_top: function() {
        return this.get_border_top() + this.get_padding_top() + this.get_margin_top();
    },

    get_bmp_left: function() {
        return this.get_border_left() + this.get_padding_left() + this.get_margin_left();
    },

    get_bmp_bottom: function() {
        return this.get_border_bottom() + this.get_padding_bottom() + this.get_margin_bottom();
    },

    get_bmp_right: function() {
        return this.get_border_right() + this.get_padding_right() + this.get_margin_right();
    },

    get_border_horizontal: function() {
        return this.get_border_left() + this.get_border_right();
    },

    get_border_vertical: function() {
        return this.get_border_top() + this.get_border_bottom();
    },

    get_margin_horizontal: function() {
        return this.get_margin_left() + this.get_margin_right();
    },

    get_margin_vertical: function() {
        return this.get_margin_top() + this.get_margin_bottom();
    },

    get_padding_horizontal: function() {
        return this.get_padding_left() + this.get_padding_right();
    },

    get_padding_vertical: function() {
        return this.get_padding_top() + this.get_padding_bottom();
    },

    get_bp_horizontal: function() {
        return this.get_border_left() + this.get_padding_left() +
            this.get_border_right() + this.get_padding_right();
    },

    get_bp_vertical: function() {
        return this.get_border_top() + this.get_padding_top() +
            this.get_border_bottom() + this.get_padding_bottom();
    },

    get_bmp_horizontal: function() {
        return this.get_border_left() + this.get_padding_left() + this.get_margin_left() +
            this.get_border_right() + this.get_padding_right() + this.get_margin_right();
    },

    get_bmp_vertical: function() {
        return this.get_border_top() + this.get_padding_top() + this.get_margin_top() +
            this.get_border_bottom() + this.get_padding_bottom() + this.get_margin_bottom();
    }
});

//////////////////////////////////////////////////////////////////////////////////////////
// Global functions
function __debug_log(message) {
    var logger = $("logger");

    if (logger) {
        if (typeof message == "undefined") {
            message = "";
        }

        logger.innerHTML += message + "<br/>";
    }
}

function __debug_log_object(object, depth, indent, return_value) {
    var message = "";

    if (!indent) {
        indent = "";
    }

    if (!depth) {
        depth = 0;
    }

    message += indent + "{<br/>";
    for (var i in object) {
        if ((typeof object[i] == "object") && (depth < 2)) {
            message += indent + "  " + i + " => <br/>";
            message += __debug_log_object(object[i], depth + 1, indent + "    ", true);
        } else {
            var value = object[i];
            if (typeof object[i] == "function") {
                value = "function";
            }

            message += indent + "  " + i + " => " + value + "<br/>";
        }
    }
    message += indent + "}<br/>";

    if (return_value) {
        return message;
    } else {
        __debug_log(message);
        return "";
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Static initialization
Ws.Utils.BrowserDetect.init();

