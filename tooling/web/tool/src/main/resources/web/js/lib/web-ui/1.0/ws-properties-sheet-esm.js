Ws.PropertiesSheet.EDITABLE_STRING_MAP = "editable-string-map";

if (!Ws.PropertiesSheet._Types) {
    Ws.PropertiesSheet._Types = {};
}

Ws.PropertiesSheet._Types.EditableStringMap = {
    render_type: Ws.PropertiesSheet.RENDER_ONE_COLUMN,

    delete_icon_url: "img/delete-item.png",
    delete_d_icon_url: "img/delete-item_d.png",
    move_up_icon_url: "img/move-up-item.png",
    move_up_d_icon_url: "img/move-up-item_d.png",
    move_down_icon_url: "img/move-down-item.png",
    move_down_d_icon_url: "img/move-down-item_d.png",

    no_items_text: "No items.",
    add_item_text: "Add item",

    render: function(psheet, descriptor) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        var html = "";

        var map = this.__get_value_as_array(descriptor);

        html += "<div class=\"" + classname + "-wrapper\" id=\"" + id + "-wrapper" + "\">";

        if (map.length == 0) {
            html += this.__render_no_items_text(psheet, descriptor);
        } else {
            for (var i = 0; i < map.length; i++) {
                html += this.__render_item(psheet, descriptor, i, map.length);
            }
        }

        if (!descriptor.disable_inline_description_render && descriptor.inline_description) {
            html +=
                "<div id=\"" + id + "-inline-description\" " +
                        "class=\"" + classname + "-inline-description\">" +
                    descriptor.inline_description +
                "</div>";
        }

        if (!descriptor.disable_buttons_menu_render) {
            html +=
                "<div id=\"" + id + "-buttons-wrapper\" " +
                        "class=\"" + classname + "-buttons-wrapper" + "\">";

            if (!descriptor.disable_add_render && !descriptor.read_only) {
                var add_onclick =
                    "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_MAP]." +
                    "handle_add('" + psheet.element.id + "', '" + descriptor.name + "')";

                var add_item_text = descriptor.add_item_text
                        ? descriptor.add_item_text
                        : this.add_item_text;

                if (!descriptor.disable_add) {
                    html +=
                        "<input id=\"" + id + "-add-button\" " +
                                "class=\"" + classname + "-add-button\" " +
                                "type=\"button\" " +
                                "value=\"" + add_item_text + "\" " +
                                "onclick=\"" + add_onclick + "\"/>";
                } else {
                    html +=
                        "<input id=\"" + id + "-add-button\" " +
                                "class=\"" + classname + "-add-button\" " +
                                "type=\"button\" " +
                                "value=\"" + add_item_text + "\" " +
                                "disabled=\"disabled\"/>";
                }
            }

            if (!descriptor.disable_validation_note_render) {
                html +=
                    "<div id=\"" + id + "-validation-note\" " +
                            "class=\"" + classname + "-validation-note\"></div>";
            }

            html += "</div>";
        }

        html += "</div>";
        return html;
    },

    set_value: function(psheet, descriptor) {
        var element;

        if (!descriptor.read_only) {
            var id = psheet.__get_input_id(descriptor);
            var map = this.__get_value_as_array(descriptor);

            for (var i = 0; i < map.length; i++) {
                element = $(id + "-key-" + i);
                if (element != null) {
                    element.value = map[i][0];
                }

                element = $(id + "-value-" + i);
                if (element != null) {
                    element.value = map[i][1];
                }
            }
        }
    },

    get_value: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return this.__get_value_as_array(descriptor);
        } else {
            var id = psheet.__get_input_id(descriptor);
            var map = [];

            var i = 0;
            var element = null;
            while ((element = $(id + "-key-" + i)) != null) {
                map.push([element.value, $(id + "-value-" + i).value]);

                i++;
            }

            return map;
        }
    },

    validate: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return []; // TODO: There is nothing to validate if we're read-only, or not?
        } else {
            var results = [];

            if (descriptor.validation) {
                var map = this.get_value(psheet, descriptor);

                if (descriptor.validation.map_not_empty) {
                    if (map.length == 0) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be an empty map.",
                            index: -1
                        });
                    }
                }

                if (descriptor.validation.map_min_size &&
                        descriptor.validation.map_min_size > 0) {

                    if (map.length < descriptor.validation.map_min_size) {
                        if (descriptor.validation.map_min_size == 1) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " cannot be an empty map.",
                                index: -1
                            });
                        } else {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " size cannot be less than " +
                                    descriptor.validation.map_min_size + " items.",
                                index: -1
                            });
                        }
                    }
                }

                if (descriptor.validation.map_max_size &&
                        descriptor.validation.map_max_size > 0) {

                    if (map.length > descriptor.validation.map_max_size) {
                        if (descriptor.validation.map_max_size == 1) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " cannot contain more " +
                                    "than 1 item.",
                                index: -1
                            });
                        } else {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name +
                                    " size cannot be larger than " +
                                    descriptor.validation.map_max_size + " items.",
                                index: -1
                            });
                        }
                    }
                }

                for (var i = 0; i < map.length; i++) {
                    var item = map[i];

                    if (descriptor.validation.key_regexp) {
                        if (!descriptor.validation.key_regexp.test(item[0])) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name +
                                    " (key #" + (i + 1) + ") " +
                                    "does not match the regular expression: " +
                                    descriptor.validation.item_regexp,
                                index: i,
                                is_key: true
                            });
                        }
                    }

                    if (descriptor.validation.key_not_empty) {
                        if (item[0].length == 0) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name +
                                    " (key #" + (i + 1) + ") " +
                                    "cannot be empty.",
                                index: i,
                                is_key: true
                            });
                        }
                    }

                    if (descriptor.validation.key_min_length &&
                            descriptor.validation.key_min_length > 0) {
                        if (item[0].length < descriptor.validation.key_min_length) {
                            if (descriptor.validation.key_min_length == 1) {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name +
                                        " (key #" + (i + 1) + ") " +
                                        "cannot be less than 1 character.",
                                    index: i,
                                    is_key: true
                                });
                            } else {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + 
                                        " (key #" + (i + 1) + ") " +
                                        "cannot be less than " +
                                        descriptor.validation.key_min_length + " characters.",
                                    index: i,
                                    is_key: true
                                });
                            }
                        }
                    }

                    if (descriptor.validation.key_max_length &&
                            descriptor.validation.key_max_length > 0) {
                        if (item[0].length > descriptor.validation.key_max_length) {
                            if (descriptor.validation.key_max_length == 1) {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + 
                                        " (key #" + (i + 1) + ") " +
                                        "cannot be longer than 1 character.",
                                    index: i,
                                    is_key: true
                                });
                            } else {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name + 
                                        " (key #" + (i + 1) + ") " +
                                        "cannot exceed " +
                                        descriptor.validation.key_max_length + " characters.",
                                    index: i,
                                    is_key: true
                                });
                            }
                        }
                    }

                    if (descriptor.validation.value_regexp) {
                        if (!descriptor.validation.value_regexp.test(item[1])) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name +
                                    " (value #" + (i + 1) + ") " +
                                    "does not match the regular expression: " +
                                    descriptor.validation.item_regexp,
                                index: i,
                                is_key: false
                            });
                        }
                    }

                    if (descriptor.validation.value_not_empty) {
                        if (item[1].length == 0) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name +
                                    " (value #" + (i + 1) + ") " +
                                    "cannot be empty.",
                                index: i,
                                is_key: false
                            });
                        }
                    }

                    if (descriptor.validation.value_min_length &&
                            descriptor.validation.value_min_length > 0) {
                        if (item[1].length < descriptor.validation.value_min_length) {
                            if (descriptor.validation.value_min_length == 1) {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name +
                                        " (value #" + (i + 1) + ") " +
                                        "cannot be less than 1 character.",
                                    index: i,
                                    is_key: false
                                });
                            } else {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name +
                                        " (value #" + (i + 1) + ") " +
                                        "cannot be less than " +
                                        descriptor.validation.value_min_length + " characters.",
                                    index: i,
                                    is_key: false
                                });
                            }
                        }
                    }

                    if (descriptor.validation.value_max_length &&
                            descriptor.validation.value_max_length > 0) {
                        if (item[1].length > descriptor.validation.value_max_length) {
                            if (descriptor.validation.value_max_length == 1) {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name +
                                        " (value #" + (i + 1) + ") " +
                                        "cannot be longer than 1 character.",
                                    index: i,
                                    is_key: false
                                });
                            } else {
                                results.push({
                                    severity: Ws.PropertiesSheet.ERROR,
                                    message: descriptor.display_name +
                                        " (value #" + (i + 1) + ") " +
                                        "cannot exceed " +
                                        descriptor.validation.value_max_length + " characters.",
                                    index: i,
                                    is_key: false
                                });
                            }
                        }
                    }
                }

                this.__render_validation_results(psheet, descriptor, results);
            }

            this.__update_ui(psheet, descriptor, map);

            return results;
        }
    },

    // Private -------------------------------------------------------------------------------------
    __get_value_as_array: function(descriptor) {
        var map = descriptor.value;
        if ((map == null) || (map == undefined)) {
            map = [];
        } else {
            map = $A(map); // Convert to array using Prototype's special function.
            for (var i = 0; i < map.length; i++) {
                if ((map[i] == null) || (map[i] == undefined)) {
                    map[i] = ["", ""];
                } else {
                    map[i] = $A(map[i]);
                }

                if (map[i].length == 0) {
                    map[i] = ["", ""];
                } else if (map[i].length == 1) {
                    map[i].push("");
                }
            }
        }

        return map;
    },

    __render_no_items_text: function(psheet, descriptor) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        var html = "";

        if (!descriptor.disable_no_items_text) {
            var no_items_text = descriptor.no_items_text
                    ? descriptor.no_items_text
                    : this.no_items_text;

            html +=
                "<div id=\"" + id + "-no-items-text\" " +
                        "class=\"" + classname + "-no-items-text\">" +
                    no_items_text +
                "</div>";
        }

        return html;
    },
    
    __render_item: function(psheet, descriptor, i, size) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        var html = "";
        
        html +=
            "<div class=\"" + classname + "-item-wrapper" + "\" " +
                    "id=\"" + id + "-" + i + "-item-wrapper" + "\">";

        html +=
            "<div class=\"" + classname + "-item-menu-wrapper" + "\" " +
                    "id=\"" + id + "-" + i + "-item-menu-wrapper" + "\">";
        html += this.__render_menu(psheet, descriptor, i, size);
        html +=
            "</div>";

        if (!descriptor.read_only) {
            html +=
                "<div class=\"" + classname + "-key-input-wrapper" + "\" " +
                        "id=\"" + id + "-" + i + "-key-input-wrapper" + "\">";
            html +=
                "<input type=\"text\" " +
                       "class=\"" + classname + "-key-input" + "\" " +
                       "id=\"" + id + "-key-" + i + "\" " +
                       "value=\"" + "" + "\" " +
                       this.__render_change_trigger(psheet, descriptor) + " " +
                       this.__render_save_trigger(psheet, descriptor) + "/>";
            html +=
                "</div>";

            html +=
                "<div class=\"" + classname + "-value-input-wrapper" + "\" " +
                        "id=\"" + id + "-" + i + "-value-input-wrapper" + "\">";
            html +=
                "<input type=\"text\" " +
                       "class=\"" + classname + "-value-input" + "\" " +
                       "id=\"" + id + "-value-" + i + "\" " +
                       "value=\"" + "" + "\" " +
                       this.__render_change_trigger(psheet, descriptor) + " " +
                       this.__render_save_trigger(psheet, descriptor) + "/>";
            html +=
                "</div>";
        } else {
            var map = this.__get_value_as_array(descriptor);

            html +=
                "<div class=\"" + classname + "-key-input-read-only" + "\" " +
                        "id=\"" + id + "-" + i + "-key-input-read-only" + "\">" +
                map[i][0] +
                "</div>";
            html +=
                "<div class=\"" + classname + "-value-input-read-only" + "\" " +
                        "id=\"" + id + "-" + i + "-value-input-read-only" + "\">" +
                map[i][1] +
                "</div>";
        }

        html +=
            "</div>";

        return html;
    },

    __render_menu: function(psheet, descriptor, i, size) {
        var psheet_id = psheet.element.id;
        var classname = psheet.__get_input_class(descriptor);

        var html = "";

        if (!descriptor.disable_menu_render && !descriptor.read_only) {
            var delete_onclick =
                "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_MAP]." +
                "handle_delete('" + psheet_id + "', '" + descriptor.name + "', " + i + ")";
            var move_down_onclick =
                "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_MAP]." +
                "handle_move_down('" + psheet_id + "', '" + descriptor.name + "', " + i + ")";
            var move_up_onclick =
                "Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_MAP]." +
                "handle_move_up('" + psheet_id + "', '" + descriptor.name + "', " + i + ")";

            if (!descriptor.disable_delete_render) {
                var delete_disabled_due_to_validation = false;
                if (descriptor.validation) {
                    if (descriptor.validation.map_not_empty && (size == 1)) {
                        delete_disabled_due_to_validation = true;
                    }

                    if (descriptor.validation.map_min_size) {
                        if (size <= descriptor.validation.map_min_size) {
                            delete_disabled_due_to_validation = true;
                        }
                    }
                }

                if (!descriptor.disable_delete && !delete_disabled_due_to_validation) {
                    html +=
                        "<img src=\"" + this.delete_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon" + "\" " +
                            "onclick=\"" + delete_onclick + "\"/>"
                } else {
                    html +=
                        "<img src=\"" + this.delete_d_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon-disabled" + "\"/>"
                }
            }

            if (!descriptor.disable_reorder_render) {
                if ((i < size - 1) && !descriptor.disable_reorder) {
                    html +=
                        "<img src=\"" + this.move_down_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon" + "\" " +
                            "onclick=\"" + move_down_onclick + "\"/>"
                } else {
                    html +=
                        "<img src=\"" + this.move_down_d_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon-disabled" + "\"/>"
                }

                if ((i > 0) && !descriptor.disable_reorder) {
                    html +=
                        "<img src=\"" + this.move_up_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon" + "\" " +
                            "onclick=\"" + move_up_onclick + "\"/>"
                } else {
                    html +=
                        "<img src=\"" + this.move_up_d_icon_url + "\" " +
                            "class=\"" + classname + "-input-menu-icon-disabled" + "\"/>"
                }
            }
        }

        return html;
    },

    __render_change_trigger: function(psheet, descriptor) {
        var onkeyup = "onkeyup=\"";
        var onblur = "onblur=\"";

        if (descriptor.validation) {
            var validation_code = "$('" + psheet.element.id + "').ws_psheet.validate();";
            if (descriptor.validation.trigger == "key") {
                onkeyup += validation_code;
            } else if (descriptor.validation.trigger == "focus") {
                onblur += validation_code;
            }
        }

        onkeyup += "$('" + psheet.element.id + "').ws_psheet.update_depends();";
        onkeyup += "$('" + psheet.element.id + "').ws_psheet.update_dynamic();";

        return onkeyup + "\" " + onblur + "\"";
    },

    __render_save_trigger: function(psheet, descriptor) {
        if (!descriptor.skip_save_on_enter) {
            return "onkeypress=\"if (event.keyCode == 13) { " +
                    "$('" + psheet.element.id + "').ws_psheet.save(); " +
                "}\"";
        } else {
            return "";
        }
    },

    __render_validation_results: function(psheet, descriptor, results) {
        var i, j, element;

        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);

        // First reset the error badges if any. This means run through the elements and remove the
        // classnames.
        i = 0;
        while ((element = $(id + "-key-" + i)) != null) {
            element.removeClassName(classname + "-input-error");
            element.removeClassName(classname + "-input-warning");

            element = $(id + "-value-" + i);
            element.removeClassName(classname + "-input-error");
            element.removeClassName(classname + "-input-warning");

            i++;
        }

        // Then run through the results assigning classnames to whichever elements require it.
        for (i = 0; i < results.length; i++) {
            if (results[i].severity == Ws.PropertiesSheet.ERROR) {
                if (results[i].index == -1) {
                    j = 0;
                    while ((element = $(id + "-key-" + j)) != null) {
                        element.addClassName(classname + "-input-error");
                        element.removeClassName(classname + "-input-warning");

                        element = $(id + "-value-" + j);
                        element.removeClassName(classname + "-input-error");
                        element.removeClassName(classname + "-input-warning");

                        j++;
                    }

                    break;
                }

                element = results[i].is_key 
                        ? $(id + "-key-" + results[i].index)
                        : $(id + "-value-" + results[i].index);
                
                if (element != null) {
                    element.removeClassName(classname + "-input-warning"); // It might have been
                                                                           // assigned already.
                    element.addClassName(classname + "-input-error");
                }
            } else if (results[i].severity == Ws.PropertiesSheet.WARNING) {
                if (results[i].index == -1) {
                    j = 0;
                    while ((element = $(id + "-" + j)) != null) {
                        if (!element.hasClassName(classname + "-input-error")) {
                            element.addClassName(classname + "-input-warning");
                        }

                        element = $(id + "-value-" + j);
                        if (!element.hasClassName(classname + "-input-error")) {
                            element.addClassName(classname + "-input-warning");
                        }

                        j++;
                    }
                } else {
                    element = results[i].is_key
                            ? $(id + "-key-" + results[i].index)
                            : $(id + "-value-" + results[i].index);

                    if ((element != null) && !element.hasClassName(classname + "-input-error")) {
                        element.addClassName(classname + "-input-warning");
                    }
                }
            }
        }
    },

    __update_ui: function(psheet, descriptor, map) {
        var i, element;

        var id = psheet.__get_input_id(descriptor);

        // Update the menus of the items.
        for (i = 0; i < map.length; i++) {
            $(id + "-" + i + "-item-menu-wrapper").innerHTML =
                    this.__render_menu(psheet, descriptor, i, map.length);
        }

        if (!descriptor.disable_add_render) {
            if (descriptor.validation && descriptor.validation.map_max_size) {
                if (map.length < descriptor.validation.map_max_size) {
                    $(id + "-add-button").disabled = false;
                } else {
                    $(id + "-add-button").disabled = true;
                }
            }
        }

        // Tackle the no items text.
        if (!descriptor.disable_no_items_text) {
            element = $(id + "-no-items-text");
            
            if ((map.length > 0) && (element != null)) {
                element.parentNode.removeChild(element);
            } else if ((map.length == 0) && (element == null)) {
                $(id + "-wrapper").innerHTML =
                        this.__render_no_items_text(psheet, descriptor) +
                        $(id + "-wrapper").innerHTML;
            }
        }

        // Update the validation note, explaining the reason behind disabling certain actions.
        if (!descriptor.disable_validation_note_render) {
            element = $(id + "-validation-note");

            if ((element != null) && descriptor.validation) {
                element.innerHTML = "";

                if (!descriptor.disable_delete && !descriptor.disable_delete_render &&
                        descriptor.validation.map_not_empty &&
                        (map.length == 1)) {

                    element.innerHTML = "Delete action was disabled as the map cannot be empty.";
                }

                if (!descriptor.disable_delete && !descriptor.disable_delete_render &&
                        descriptor.validation.map_min_size &&
                        (descriptor.validation.map_min_size >= map.length)) {

                    element.innerHTML = "Delete action was disabled as the number " +
                        "of elements in the map cannot be less than " +
                        descriptor.validation.map_min_size + ".";
                }

                if (!descriptor.disable_add && !descriptor.disable_add_render &&
                        descriptor.validation.map_max_size &&
                        (descriptor.validation.map_max_size <= map.length)) {

                    element.innerHTML = "Add action was disabled as the number " +
                        "of elements in the map cannot be more than " +
                        descriptor.validation.map_max_size + ".";
                }
            }
        }
    },

    // Callbacks -----------------------------------------------------------------------------------
    handle_delete: function(psheet_id, descriptor_id, index) {
        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        var element = $(id + "-" + index + "-item-wrapper");
        element.parentNode.removeChild(element);

        // Now run from index to whatever exists, updating indices.
        var max = index + 1;
        while ($(id + "-" + max + "-item-wrapper") != null) {
            $(id + "-" + max + "-item-wrapper").id =
                    id + "-" + (max - 1) + "-item-wrapper";
            $(id + "-" + max + "-key-input-wrapper").id =
                    id + "-" + (max - 1) + "-key-input-wrapper";
            $(id + "-" + max + "-value-input-wrapper").id =
                    id + "-" + (max - 1) + "-value-input-wrapper";
            $(id + "-" + max + "-item-menu-wrapper").id =
                    id + "-" + (max - 1) + "-item-menu-wrapper";
            $(id + "-key-" + max).id =
                    id + "-key-" + (max - 1);
            $(id + "-value-" + max).id =
                    id + "-value-" + (max - 1);

            max++;
        }

        psheet.validate();
    },

    handle_move_up: function(psheet_id, descriptor_id, index) {
        var temp;

        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        // We do not need to change anything except the values.
        var current_key = $(id + "-key-" + index);
        var current_value = $(id + "-value-" + index);
        var previous_key = $(id + "-key-" + (index - 1));
        var previous_value = $(id + "-value-" + (index - 1));

        if ((current_key != null) && (previous_key != null)) {
            temp = current_key.value;
            current_key.value = previous_key.value;
            previous_key.value = temp;
        }

        if ((current_value != null) && (previous_value != null)) {
            temp = current_value.value;
            current_value.value = previous_value.value;
            previous_value.value = temp;
        }

        psheet.validate();
    },

    handle_move_down: function(psheet_id, descriptor_id, index) {
        var temp;

        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        // We do not need to change anything except the values.
        var current_key = $(id + "-key-" + index);
        var current_value = $(id + "-value-" + index);
        var next_key = $(id + "-key-" + (index + 1));
        var next_value = $(id + "-value-" + (index + 1));

        if ((current_key != null) && (next_key != null)) {
            temp = current_key.value;
            current_key.value = next_key.value;
            next_key.value = temp;
        }

        if ((current_value != null) && (next_value != null)) {
            temp = current_value.value;
            current_value.value = next_value.value;
            next_value.value = temp;
        }

        psheet.validate();
    },

    handle_add: function(psheet_id, descriptor_id) {
        var element;

        var psheet = $(psheet_id).ws_psheet;
        var descriptor = psheet.__get_descriptor(descriptor_id);

        var id = psheet.__get_input_id(descriptor);

        var max = 0;
        while ($(id + "-" + max + "-item-wrapper") != null) {
            max++;
        }

        var new_element = document.createElement("div");
        new_element.innerHTML = this.__render_item(psheet, descriptor, max, max + 1);
        new_element = new_element.firstChild;

        if (!descriptor.disable_inline_description_render && descriptor.inline_description) {
            element = $(id + "-inline-description");
            element.parentNode.insertBefore(new_element, element);
        } else if (!descriptor.disable_add_render) {
            element = $(id + "-buttons-wrapper");
            element.parentNode.insertBefore(new_element, element);
        } else {
            $(id + "-wrapper").appendChild(new_element);
        }

        psheet.validate();
    },

    // No-op ---------------------------------------------------------------------------------------
    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.EDITABLE_STRING_MAP] =
        Ws.PropertiesSheet._Types.EditableStringMap;
