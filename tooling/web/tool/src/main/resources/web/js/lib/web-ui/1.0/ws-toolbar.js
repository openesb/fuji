/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Toolbar = Class.create({
    width: -1,
    height: -1,
    top: 0,
    left: 0,
    
    radius: 8,
    fill: "rgba(0, 0, 0, 0.8)",
    stroke: null,
    stroke_width: 0,

    auto_resize_if_in_window: true,
    
    element: null,
    
    initialize: function(element, options, rr_options) {
        if (options) Object.extend(this, options);

        this.element = element;
        this.element.ws_toolbar = this;
        this.element.show();
        
        if (!rr_options) rr_options = {};

        Ws.Utils.apply_defaults(rr_options, {
            radius: this.radius,
            fill: this.fill,
            stroke: this.stroke,
            stroke_width: this.stroke_width,
            half_round: "top"
        });
        
        new Ws.RoundedRectangle(this.element, rr_options);
        
        // If the parent element is the document body, and we're told to resize
        // automatically, add a resize listener to the window object which woudl update
        // the toolbar.
        if ((this.element.parentNode == document.body) && this.auto_resize_if_in_window) {
            var self = this;

            Event.observe(window, "resize", function() {
                self.relayout();
                self.element.ws_rrectangle.relayout();
            });
        }
    },

    relayout: function () {
        var position;
        if (this.element.parentNode == document.body) {
            position = "fixed";
        } else {
            position = "absolute";
        }

        this.element.setStyle({
            position: position,
            top: this.top + "px",
            left: this.left + "px"
        });

        if (this.width == -1) {
            Ws.Utils.resize_to_fill_parent_width(this.element);
        } else {
            var element_bmp = Ws.Utils.get_bmp(this.element);

            this.element.setStyle({
                width: (this.width - element_bmp.get_bmp_horizontal()) + "px"
            });
        }
    },
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});
