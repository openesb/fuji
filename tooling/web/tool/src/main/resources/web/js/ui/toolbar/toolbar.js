/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

var toolbar = null;

Event.observe(window, "load", function() {
    toolbar = $("toolbar");
    new Ws.Toolbar(toolbar);

    // Initialize the status, non-intrusive progress bar, which should be shown during
    // background tasks.
    var status_progress_bar = $("statusProgress");
    __status_progress = new Ws.ProgressBar(status_progress_bar, {
        fill: "rgb(0, 0, 0)"
    });

    __status_progress.hide();

    // Update the toolbar buttons.
    update_toolbar_buttons();

    // Update the status message.
    set_status_message("Hello!", 5, true, true);

});

function update_toolbar_buttons(force_disabled) {
    var button;

    // New.
    button = $("toolbarNew");
    if ((current_user.username == NOBODY) && config.users_enabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/new_d.png";
        button.firstChild.title="Creating a new message flow is not possible now.\n" +
                "You must login first.";
    } else if (force_disabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/new_d.png";
        button.firstChild.title="Please wait...";
    } else {
        button.href="javascript: toolbar_handle_new()";
        button.setStyle({
            cursor: "pointer"
        });
        button.firstChild.src="resource?img/ui/toolbar/new.png";
        button.firstChild.title="Create a new message flow.";
    }

    // Open.
    button = $("toolbarOpen");
    if (force_disabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/open_d.png";
        button.firstChild.title="Please wait...";
    } else {
        button.href="javascript: toolbar_handle_open()";
        button.setStyle({
            cursor: "pointer"
        });
        button.firstChild.src="resource?img/ui/toolbar/open.png";
        button.firstChild.title="Open an existing message flow.";
    }

    // Save.
    button = $("toolbarSave");
    if ((current_user.username == NOBODY) && config.users_enabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/save_d.png";
        button.firstChild.title="Saving the message flow is not possible now.\n" +
                "You must login first.";
    } else if (force_disabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/save_d.png";
        button.firstChild.title="Please wait...";
    } else {
        button.href="javascript: toolbar_handle_save()";
        button.setStyle({
            cursor: "pointer"
        });
        button.firstChild.src="resource?img/ui/toolbar/save.png";
        button.firstChild.title="Save the current message flow.";
    }

    // Deploy.
    if (config.deployment_enabled) {
        button = $("toolbarDeploy");
        if ((current_user.username == NOBODY) && config.users_enabled) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/deploy_d.png";
            button.firstChild.title="Deploying the message flow is not possible now.\n" +
                    "You must login first.";
        } else if (is_current_graph_untitled()) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/deploy_d.png";
            button.firstChild.title="Deploying the message flow is not possible now.\n" +
                    "You must save the message flow first.";
        } else if (force_disabled) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/deploy_d.png";
            button.firstChild.title="Please wait...";
        } else {
            button.href="javascript: toolbar_handle_deploy()";
            button.setStyle({
                cursor: "pointer"
            });
            button.firstChild.src="resource?img/ui/toolbar/deploy.png";
            button.firstChild.title="Deploy the current message flow.";
        }
    }

    // Properties.
    button = $("toolbarProperties");
    if (is_current_graph_untitled()) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/properties_d.png";
        button.firstChild.title="Editing the message flow properties is not possible now.\n" +
                    "You must save the message flow first.";
    } else if (force_disabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/properties_d.png";
        button.firstChild.title="Please wait...";
    } else {
        button.href="javascript: toolbar_handle_properties()";
        button.setStyle({
            cursor: "pointer"
        });
        button.firstChild.src="resource?img/ui/toolbar/properties.png";
        button.firstChild.title="Properties of the current message flow.";
    }

    // Tracing.
    if (config.deployment_enabled) {
        button = $("toolbarTracing");
        if ((current_user.username == NOBODY) && config.users_enabled) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/run_tests_d.png";
            button.firstChild.title="Tracing the messages is not possible now.\n" +
                    "You must login first.";
        } else if (is_current_graph_untitled()) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/run_tests_d.png";
            button.firstChild.title="Tracing the messages is not possible now.\n" +
                    "You must save and deploy the message flow first.";
        } else if (force_disabled) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/run_tests_d.png";
            button.firstChild.title="Please wait...";
        } else {
            button.href="javascript: toolbar_handle_tracing()";
            button.setStyle({
                cursor: "pointer"
            });
            button.firstChild.src="resource?img/ui/toolbar/run_tests.png";
            button.firstChild.title="Enable message tracing for the current message flow.";
        }
    }

    // Login.
    if (config.users_enabled) {
        button = $("toolbarLogin");
        if ((current_user.username == NOBODY) && config.users_enabled) {
            button.href="javascript: toolbar_handle_login()";
            button.setStyle({
                cursor: "pointer"
            });
            button.firstChild.src="resource?img/ui/toolbar/login.png";
            button.firstChild.title="Log in to your user account.";
        } else if (force_disabled) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/login_d.png";
            button.firstChild.title="Please wait...";
        } else {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/login_d.png";
            button.firstChild.title="Logging in is not possible now.\n" +
                    "You are already logged in.";
        }
    }

    // Register.
    if (config.users_enabled && config.registration_enabled) {
        button = $("toolbarRegister");
        if ((current_user.username == NOBODY) && config.users_enabled) {
            button.href="javascript: toolbar_handle_register()";
            button.setStyle({
                cursor: "pointer"
            });
            button.firstChild.src="resource?img/ui/toolbar/register.png";
            button.firstChild.title="Register a new user account.";
        } else if (force_disabled) {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/register_d.png";
            button.firstChild.title="Please wait...";
        } else {
            button.href="javascript: //";
            button.setStyle({
                cursor: "default"
            });
            button.firstChild.src="resource?img/ui/toolbar/register_d.png";
            button.firstChild.title="Registering is not possible now.\n" +
                    "You are already registered and logged in.";
        }
    }

    // Edit source.
    button = $("toolbarEditSource");
    if (force_disabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/edit_source_d.png";
        button.firstChild.title="Please wait...";
    } else {
        button.href="javascript: toolbar_handle_edit_source()";
        button.setStyle({
            cursor: "pointer"
        });
        button.firstChild.src="resource?img/ui/toolbar/edit_source.png";
        button.firstChild.title="Edit IFL source of the current message flow.";
    }

    // Download ZIP.
    button = $("toolbarDownloadZip");
    if (force_disabled) {
        button.href="javascript: //";
        button.setStyle({
            cursor: "default"
        });
        button.firstChild.src="resource?img/ui/toolbar/download_zip_d.png";
        button.firstChild.title="Please wait...";
    } else {
        button.href="javascript: toolbar_handle_download_zip()";
        button.setStyle({
            cursor: "pointer"
        });
        button.firstChild.src="resource?img/ui/toolbar/download_zip.png";
        button.firstChild.title="Download project sources as a ZIP archive.";
    }
}

// Toolbar actions -----------------------------------------------------------------------
function toolbar_handle_new() {
    if (lock) {
        setTimeout(toolbar_handle_new, 100);
        return;
    }

    lock = true; // To be released in __new_graph_dialog's action.

    if (!__new_graph_dialog) {
        __new_graph_dialog = create_modal_dialog(
                "new-graph-dialog",
                "New Message Flow",
                __new_graph_pc,
                [
                    {
                        display_name: "Create",
                        action: __new_graph_dialog_action
                    },
                    {
                        display_name: "Cancel",
                        action: function() {
                            lock = false;
                            $("new-graph-dialog-validation").innerHTML = "";
                            __new_graph_dialog.ws_mwindow.close();
                        }
                    }
                ]);
    }

    __new_graph_dialog.ws_psheet.reset();
    __new_graph_dialog.ws_mwindow.show();

    $("new-graph-dialog-button-0").disabled = false;
    $("new-graph-dialog-button-1").disabled = false;
}

function toolbar_handle_open() {
    if (lock) {
        setTimeout(toolbar_handle_open, 100);
        return;
    }

    lock = true;

    new Ajax.Request("callbacks/get-graph-descriptors", {
        method: "get",

        onSuccess: function(transport) {
            var descriptors = transport.responseJSON.descriptors.compact();

            __open_graph_pc.set_graph_descriptors(descriptors);
            __open_graph_dialog_show();
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    toolbar_handle_open();
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to retrieve available message flows data. " +
                    "The server responded: <br/><i>" + transport.statusText + "</i>",
                    -1, true);
        },

        onComplete: function(transport) {
            lock = false;
        }
    });
}

function toolbar_handle_save() {
    if (lock) {
        setTimeout(toolbar_handle_save, 100);
        return;
    }

    lock = true;

    if (is_current_graph_untitled()) {        
        if (!__save_graph_dialog) {
            __save_graph_dialog = create_modal_dialog(
                    "save-graph-dialog",
                    "Save Message Flow",
                    __save_graph_pc,
                    [
                        {
                            display_name: "Save",
                            action: __save_graph_dialog_action
                        },
                        {
                            display_name: "Cancel",
                            action: function() {
                                lock = false;
                                $("save-graph-dialog-validation").innerHTML = "";
                                __save_graph_dialog.ws_mwindow.close();
                            }
                        }
                    ]);
        }

        __save_graph_dialog.ws_psheet.reset();
        __save_graph_dialog.ws_mwindow.show();

        $("save-graph-dialog-button-0").disabled = false;
        $("save-graph-dialog-button-1").disabled = false;
    } else {
        show_status_progress();
        set_status_message("Saving...");

        new Ajax.Request("callbacks/save", {
            method: "post",

            postBody: "{ explicit: true }",

            onSuccess: function(transport) {
                if (transport.responseJSON) {
                    current_graph.revision = transport.responseJSON.revision;
                }
                set_status_message("Message flow was saved successfully.", 2000, true);
            },

            onFailure: function(transport) {
                if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                    reestablish_session(toolbar_handle_save);
                } else {
                    Ws.Growl.notify_error(
                            "Failed to save the message flow. " +
                            "The server responded: <br/><i>" + transport.statusText +
                            "</i>", -1, true);
                }

                set_status_message("Saving...", 50, true);
            },

            onComplete: function(transport) {
                lock = false;
                hide_status_progress();
            }
        });
    }
}

function toolbar_handle_deploy() {
    if (lock) {
        setTimeout(toolbar_handle_deploy, 100);
        return;
    }

    if (is_current_graph_untitled()) {
        Ws.Growl.notify_error(
                "You cannot deploy an untitled message " +
                "flow. Please first save it and give it a name.", -1, true);

        return;
    }

    lock = true;

    show_status_progress();
    set_status_message("Deploying...");

    new Ajax.Request("callbacks/deploy", {
        method: "get",

        onSuccess: function(transport) {
            if (transport.responseJSON) {
                current_graph.revision = transport.responseJSON.revision;
            }
            set_status_message("Message flow was deployed successfully.", 2000, true);
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(toolbar_handle_deploy);
            } else {
                Ws.Growl.notify_error(
                        "Failed to deploy the message flow. " +
                        "The server responded: <br/><i>" + transport.statusText +
                        "</i>", -1, true);
            }

            set_status_message("Deploying...", 50, true);
        },

        onComplete: function(transport) {
            lock = false;
            hide_status_progress();
        }
    });
}


function toolbar_handle_properties() {
    if (lock) {
        setTimeout(toolbar_handle_properties, 100);
        return;
    }

    lock = true;

    if (is_current_graph_read_only()) {
        if (!__graph_properties_ro_dialog) {
            __graph_properties_ro_dialog = create_modal_dialog(
                    "graph-properties-ro-dialog",
                    "Properties",
                    __graph_properties_ro_pc,
                    [
                        {
                            display_name: "Close",
                            action: function() {
                                lock = false;
                                __graph_properties_ro_dialog.ws_mwindow.close();
                            }
                        }
                    ]);
        }

        __graph_properties_ro_dialog.ws_psheet.reset();
        __graph_properties_ro_dialog.ws_mwindow.show();
    } else {
        if (!__graph_properties_dialog) {
            __graph_properties_dialog = create_modal_dialog(
                    "graph-properties-dialog",
                    "Properties",
                    __graph_properties_pc,
                    [
                        {
                            display_name: "Save",
                            action: __graph_properties_dialog_action
                        },
                        {
                            display_name: "Cancel",
                            action: function() {
                                lock = false;
                                $("graph-properties-dialog-validation").innerHTML = "";
                                __graph_properties_dialog.ws_mwindow.close();
                            }
                        }
                    ]);
        }

        __graph_properties_dialog.ws_psheet.reset();
        __graph_properties_dialog.ws_mwindow.show();

        $("graph-properties-dialog-button-0").disabled = false;
        $("graph-properties-dialog-button-1").disabled = false;
    }
}


function toolbar_handle_tracing() {
    if (!tracing_enabled) {
        tracing_enabled = true;
        get_tracing_data(true);
        $('tracingToolbar').setAttribute("src", "resource?img/ui/tracing/stop_tests_24.png");
    } else {
        tracing_enabled = false;
        clearTimeout(tracing_timeout);
        get_tracing_data(false, true);
        $('tracingToolbar').setAttribute("src", "resource?img/ui/tracing/run_tests_24.png");
    }

    $('diagram').ws_graph.ui.repaint();
}


function toolbar_handle_login() {
    if (lock) {
        setTimeout(toolbar_handle_login, 100);
        return;
    }

    lock = true;

    if (!__login_dialog) {
        __login_dialog = create_modal_dialog(
                "login-dialog",
                "Login",
                __login_pc,
                [
                    {
                        display_name: "Login",
                        action: __login_dialog_action
                    },
                    {
                        display_name: "Cancel",
                        action: function() {
                            lock = false;
                            __login_dialog.ws_mwindow.close();
                        }
                    }
                ]);
    }

    __login_dialog.ws_psheet.reset();
    __login_dialog.ws_mwindow.show();

    $("login-dialog-button-0").disabled = false;
    $("login-dialog-button-1").disabled = false;
}

function toolbar_handle_register() {
    if (lock) {
        setTimeout(toolbar_handle_register, 100);
        return;
    }

    lock = true;

    if (!__register_dialog) {
        __register_dialog = create_modal_dialog(
                "register-dialog",
                "Registration",
                __register_pc,
                [
                    {
                        display_name: "Register",
                        action: __register_dialog_action
                    },
                    {
                        display_name: "Cancel",
                        action: function() {
                            lock = false;
                            __register_dialog.ws_mwindow.close();
                        }
                    }
                ]);
    }

    __register_dialog.ws_psheet.reset();
    __register_dialog.ws_mwindow.show();

    $("register-dialog-button-0").disabled = false;
    $("register-dialog-button-1").disabled = false;
}


function toolbar_handle_edit_source() {
    edit_ifl_source();
}

function toolbar_handle_download_zip() {
    if (lock) {
        setTimeout(toolbar_handle_save, 100);
        return;
    }

    lock = true;

    show_status_progress();
    set_status_message("Generating archive...");

    new Ajax.Request("callbacks/download-zip", {
        method: "get",

        parameters: {},

        onSuccess: function(transport) {
            set_status_message("Archive generated successfully.", 500, true);
            hide_status_progress();

            var iframe_container = new Element("div");
            iframe_container.setStyle({
                position: "absolute",
                top: "-2500px",
                left: "-2500px"
            });

            document.body.appendChild(iframe_container);
            iframe_container.innerHTML = transport.responseText;
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(toolbar_handle_download_zip);
            } else {
                Ws.Growl.notify_error(
                        "Failed to generate the sources archive.<br/>" +
                        "The server responded:<br/>" + 
                        "<i>" + transport.statusText + "</i>", -1, true);
            }

            set_status_message("Generating archive...", 50, true);
        },

        onComplete: function(transport) {
            lock = false;
        }
    });
}


function toolbar_handle_feedback() {
    if (lock) {
        setTimeout(toolbar_handle_feedback, 100);
        return;
    }

    lock = true;

    if (!__feedback_dialog) {
        __feedback_dialog = create_modal_dialog(
                "feedback-dialog",
                "Feedback",
                __feedback_pc,
                [
                    {
                        display_name: "Send",
                        action: __feedback_dialog_action
                    },
                    {
                        display_name: "Cancel",
                        action: function() {
                            lock = false;
                            __feedback_dialog.ws_mwindow.close();
                        }
                    }
                ]);
    }

    __feedback_dialog.ws_psheet.reset();
    __feedback_dialog.ws_mwindow.show();

    $("feedback-dialog-button-0").disabled = false;
    $("feedback-dialog-button-1").disabled = false;
}

// Status string -------------------------------------------------------------------------
function set_status_message(message, timeout, force_restore_to_default, skip_italic) {
    if (__status_timeout) {
        clearTimeout(__status_timeout);
    }

    var old_status = $("statusString").innerHTML;
    if (!skip_italic) {
        message = "<i>" + message + "</i>";
    }
    $("statusString").innerHTML = message;


    var graph_description = "<table><tbody>"
    graph_description += "<tr><td class=\"name\"> name: </td><td>" + current_graph.display_name + "</td></tr>";
    graph_description += "<tr><td class=\"name\">description: </td><td>" + current_graph.description + "</td></tr>";
    graph_description += "<tr><td class=\"name\">created_by: </td><td>" + current_graph.created_by + "</td></tr>";
    graph_description += "<tr><td class=\"name\">created_on: </td><td>" + new Date(current_graph.created_on) + "</td></tr>";
    graph_description += "<tr><td class=\"name\">updated_by: </td><td>" + current_graph.updated_by + "</td></tr>";
    graph_description += "<tr><td class=\"name\">updated_on: </td><td>" + new Date(current_graph.updated_on) + "</td></tr>";
    graph_description += "<tr><td class=\"name\">revision: </td><td>" + current_graph.revision + "</td></tr>";
    graph_description += "</tbody></table>"

    new Ws.Tooltip({html_tooltip: graph_description} ,$("statusString"));
    
    if (!timeout || (timeout == 0)) {
        return;
    }

    if (force_restore_to_default) {
        __status_timeout = setTimeout(function() {
            var message = "";

            if (current_user.username == NOBODY) {
                message += __status_default_hello_nobody;
            } else {
                message += __status_default_hello;
            }

            if (current_graph.loaded_with_errors) {
                message += __status_default_graph_error;
            } else if (current_graph.created_by != current_user.username) {
                message += __status_default_graph_ro;
            } else {
                message += __status_default_graph;
            }

            message = message.
                        replace("{0}", Ws.Utils.escape_entities(current_user.username)).
                        replace("{1}", Ws.Utils.escape_entities(current_graph.display_name)).
                        replace("{2}", Ws.Utils.escape_entities(current_graph.description));

            set_status_message(message, 0, false, true);
        }, timeout);
    } else {
        __status_timeout = setTimeout(function() {
            set_status_message(old_status, 0, false, true);
        }, timeout);
    }
}

function show_status_progress() {
    __status_progress.show();

    update_toolbar_buttons(true);
}

function hide_status_progress() {
    __status_progress.hide();

    update_toolbar_buttons();
}

// Private -------------------------------------------------------------------------------
var __new_graph_dialog = null;
var __open_graph_dialog = null;
var __save_graph_dialog = null;
var __graph_properties_dialog = null;
var __graph_properties_ro_dialog = null;
var __login_dialog = null;
var __register_dialog = null;
var __feedback_dialog = null;
var __status_progress = null;

var __status_default_hello = "Hello, <b>{0}</b>!";
var __status_default_hello_nobody = "Hello!";
var __status_default_graph = " You're editing <i style=\"color: white\" title=\"{2}\">{1}</i>."
var __status_default_graph_ro = " You're viewing <i style=\"color: orange\" title=\"{2}\">{1}</i>.";
var __status_default_graph_error = " You're viewing <i style=\"color: white\" title=\"{2}\">{1} <span style=\"color: red\">(loaded with errors)</span></i>.";
var __status_timeout = null;

function __new_graph_dialog_action() {
    $("new-graph-dialog-button-0").disabled = true;
    $("new-graph-dialog-button-1").disabled = true;

    var properties = __new_graph_dialog.ws_psheet.get_properties();
    var validation = $("new-graph-dialog-validation");

    var code_name = properties.code_name;
    var display_name = properties.display_name;
    var description = properties.description;

    var trimmed_cn = code_name.replace(/^\s+/, "").replace(/\s+$/, "");
    if ((trimmed_cn == "") || /^untitled.*/.test(trimmed_cn)) {
        validation.innerHTML = "Code Name cannot be empty or \"untitled*\". The " +
                "latter is a reserved prefix.";

        $("new-graph-dialog-button-0").disabled = false;
        $("new-graph-dialog-button-1").disabled = false;
        return;
    }

    if (!/^[a-zA-Z0-9-_]+$/.test(trimmed_cn)) {
        validation.innerHTML = "Code Name can contain only latin " +
                "characters, digits, dashes and underscores.";

        $("new-graph-dialog-button-0").disabled = false;
        $("new-graph-dialog-button-1").disabled = false;
        return;
    }

    var trimmed_dn = display_name.replace(/^\s+/, "").replace(/\s+$/, "");
    if (trimmed_dn == "") {
        validation.innerHTML = "Display Name cannot be empty. Sorry.";

        $("new-graph-dialog-button-0").disabled = false;
        $("new-graph-dialog-button-1").disabled = false;
        return;
    }

    __new_graph_dialog_action_do(code_name, display_name, description);
}

function __new_graph_dialog_action_do(code_name, display_name, description) {
    var validation = $("new-graph-dialog-validation");

    lock = true;
    new Ajax.Request("callbacks/graph-exists", {
        method: "get",

        parameters: {
            "code_name": code_name
        },

        onSuccess: function(transport) {
            var exists = transport.responseJSON.exists;

            if (!exists) {
                __new_graph_dialog.ws_mwindow.close();
                validation.innerHTML = "";

                __new_graph_dialog_action_do_do(code_name, display_name, description);
            } else {
                validation.innerHTML = "A message flow with this code name " +
                        "already exists. You'll have to choose another one. Sorry.";
                    
                $("new-graph-dialog-button-0").disabled = false;
                $("new-graph-dialog-button-1").disabled = false;
            }
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                __new_graph_dialog.ws_mwindow.close(true);
                reestablish_session(function() {
                    __new_graph_dialog.ws_mwindow.show();
                    __new_graph_dialog_action_do(code_name, display_name, description);
                });

                return;
            }

            Ws.Growl.notify_warning(
                    "Failed to check whether the message flow exists. " +
                    "The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);

            __new_graph_dialog.ws_mwindow.close();
            validation.innerHTML = "";

            __new_graph_dialog_action_do_do(code_name, display_name, description);
        },

        onComplete: function(transport) {
            lock = false;
        }
    });
}

function __new_graph_dialog_action_do_do(code_name, display_name, description) {
    show_global_progress("Creating...");

    lock = false;

    new Ajax.Request("callbacks/new", {
        method: "get",

        parameters: {
            "code_name": code_name,
            "display_name": display_name,
            "description": description
        },

        onSuccess: function(transport) {
            $("diagram").ws_graph.clear(true);
            current_graph = transport.responseJSON;

            set_status_message("New message flow created successfully.", 2000, true);
            update_toolbar_buttons();
            update_graph_properties();
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __new_graph_dialog_action_do_do(code_name, display_name, description);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to create the message flow. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            hide_global_progress();
        }
    });
}


function __open_graph_dialog_show() {
    lock = true; // To be released in __open_graph_dialog's action.

    if (!__open_graph_dialog) {
        __open_graph_dialog = create_modal_dialog(
                "open-graph-dialog",
                "Open",
                __open_graph_pc,
                [
                    {
                        display_name: "Open",
                        action: __open_graph_dialog_action
                    },
                    {
                        display_name: "Cancel",
                        action: function() {
                            lock = false;
                            $("open-graph-dialog-validation").innerHTML = "";
                            __open_graph_dialog.ws_mwindow.close();
                        }
                    }
                ]);
    }

    if (__open_graph_pc.graph_descriptors.length == 0) {
        $("open-graph-dialog-button-0").disabled = true;
    } else {
        $("open-graph-dialog-button-0").disabled = false;
    }
    $("open-graph-dialog-button-1").disabled = false;

    __open_graph_dialog.ws_psheet.reset();
    __open_graph_dialog.ws_mwindow.show();
}

function __open_graph_dialog_action() {
    $("open-graph-dialog-button-0").disabled = true;
    $("open-graph-dialog-button-1").disabled = true;

    var properties = __open_graph_dialog.ws_psheet.get_properties();
    var validation = $("open-graph-dialog-validation");

    var code_name = properties.code_name;

    // No validation is really needed here, so skipping it altogether.

    validation.innerHTML = "";
    __open_graph_dialog.ws_mwindow.close();

    __open_graph_dialog_action_do(code_name);
}

function __open_graph_dialog_action_do(code_name) {
    show_global_progress("Opening...");

    lock = false;

    new Ajax.Request("callbacks/open", {
        method: "get",

        parameters: {
            "code_name": code_name
        },

        onSuccess: function(transport) {
            $("diagram").ws_graph.clear(true);

            var graph_data = transport.responseJSON;

            current_graph.code_name = graph_data.code_name;
            current_graph.display_name = graph_data.display_name;
            current_graph.description = graph_data.description;
            current_graph.created_by = graph_data.created_by;
            current_graph.created_on = graph_data.created_on;
            current_graph.updated_by = graph_data.updated_by;
            current_graph.updated_on = graph_data.updated_on;

            load_graph(graph_data.nodes, graph_data.links);

            set_status_message("New message flow opened successfully.", 2000, true);
            update_toolbar_buttons();
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __open_graph_dialog_action_do(code_name);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to open the message flow. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            hide_global_progress();
        }
    });
}


function __save_graph_dialog_action() {
    $("save-graph-dialog-button-0").disabled = true;
    $("save-graph-dialog-button-1").disabled = true;

    var properties = __save_graph_dialog.ws_psheet.get_properties();
    var validation = $("save-graph-dialog-validation");
    validation.innerHTML = "";

    var code_name = properties.code_name;
    var display_name = properties.display_name;
    var description = properties.description;

    var trimmed_cn = code_name.replace(/^\s+/, "").replace(/\s+$/, "");
    if ((trimmed_cn == "") || /^untitled.*/.test(trimmed_cn)) {
        validation.innerHTML = "Code Name cannot be empty or start with \"untitled\".";

        $("save-graph-dialog-button-0").disabled = false;
        $("save-graph-dialog-button-1").disabled = false;
        return;
    }

    if (!/^[a-zA-Z0-9-_]+$/.test(trimmed_cn)) {
        validation.innerHTML = "Code Name can contain only latin " +
                "characters, digits, dashes and underscores.";

        $("save-graph-dialog-button-0").disabled = false;
        $("save-graph-dialog-button-1").disabled = false;
        return;
    }

    var trimmed_dn = display_name.replace(/^\s+/, "").replace(/\s+$/, "");
    if (trimmed_dn == "") {
        validation.innerHTML = "Display Name cannot be empty. Sorry.";

        $("save-graph-dialog-button-0").disabled = false;
        $("save-graph-dialog-button-1").disabled = false;
        return;
    }

    __save_graph_dialog_action_do(trimmed_cn, trimmed_dn, description);
}

function __save_graph_dialog_action_do(code_name, display_name, description) {
    var validation = $("save-graph-dialog-validation");

    lock = true;
    new Ajax.Request("callbacks/graph-exists", {
        method: "get",

        parameters: {
            "code_name": code_name
        },

        onSuccess: function(transport) {
            var exists = transport.responseJSON.exists;

            if (!exists) {
                __save_graph_dialog.ws_mwindow.close();
                validation.innerHTML = "";

                __save_graph_dialog_action_do_do(code_name, display_name, description);
            } else {
                validation.innerHTML = "A message flow with this code name " +
                        "already exists. You'll have to choose another one. Sorry.";

                $("save-graph-dialog-button-0").disabled = false;
                $("save-graph-dialog-button-1").disabled = false;
            }
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                __save_graph_dialog.ws_mwindow.close(true);
                reestablish_session(function() {
                    __save_graph_dialog.ws_mwindow.show();
                    __save_graph_dialog_action_do(code_name, display_name, description);
                });

                return;
            }

            Ws.Growl.notify_warning(
                    "Failed to check whether the message flow exists. " +
                    "The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);

            __save_graph_dialog.ws_mwindow.close();
            validation.innerHTML = "";

            __save_graph_dialog_action_do_do(code_name, display_name, description);
        },

        onComplete: function(transport) {
            lock = false;
        }
    });
}

function __save_graph_dialog_action_do_do(code_name, display_name, description) {
    show_global_progress("Saving...");
    new Ajax.Request("callbacks/save", {
        method: "post",

        postBody: "{" +
            "explicit: true," +
            "code_name: " + Object.toJSON(code_name) + "," +
            "display_name: " + Object.toJSON(display_name) + "," +
            "description: " + Object.toJSON(description) +
            "}",

        onSuccess: function(transport) {
            current_graph.code_name = code_name;
            current_graph.display_name = display_name;
            current_graph.description = description;
            if (transport.responseJSON) {
                current_graph.revision = transport.responseJSON.revision;
            }

            set_status_message("Message flow saved successfully.", 2000, true);
            update_toolbar_buttons();
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __save_graph_dialog_action_do_do(code_name, display_name, description);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to save the message flow. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            lock = false;
            hide_global_progress();
        }
    });
}


function __graph_properties_dialog_action() {
    $("graph-properties-dialog-button-0").disabled = false;
    $("graph-properties-dialog-button-1").disabled = false;

    var properties = __graph_properties_dialog.ws_psheet.get_properties();
    var validation = $("graph-properties-dialog-validation");

    properties.display_name = properties.display_name.replace(/^\s+/, "").replace(/\s+$/, "");
    if (properties.display_name == "") {
        validation.innerHTML = "Display Name cannot be empty. Sorry.";

        $("graph-properties-dialog-button-0").disabled = false;
        $("graph-properties-dialog-button-1").disabled = false;
        return;
    }

    validation.innerHTML = "";
    __graph_properties_dialog.ws_mwindow.close();

    __graph_properties_dialog_action_do(properties);
}

function __graph_properties_dialog_action_do(properties) {
    show_global_progress("Saving...");
      var data = properties;
      data.code_name = current_graph.code_name;
      data.explicit = true;

      new Ajax.Request("callbacks/save", {
        method: "post",

        postBody: Object.toJSON(data),

        onSuccess: function(transport) {
            current_graph.display_name = properties.display_name;
            current_graph.description = properties.description;
            if (transport.responseJSON) {
                current_graph.revision = transport.responseJSON.revision;
            }
            set_status_message("Message flow saved successfully.", 2000, true);

        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __graph_properties_dialog_action_do(properties);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to save the message flow. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            lock = false;
            hide_global_progress();
        }
    });
}


function __login_dialog_action() {
    $("login-dialog-button-0").disabled = true;
    $("login-dialog-button-1").disabled = true;

    var properties = __login_dialog.ws_psheet.get_properties();
    var validation = $("login-dialog-validation");

    var username = properties.username;
    var password = properties.password;

    var trimmed_un = username.replace(/^\s+/, "").replace(/\s+$/, "");
    if (trimmed_un == "") {
        validation.innerHTML = "Username cannot be empty. An empty string is not a " +
                "good indentifier, is it?";

        $("login-dialog-button-0").disabled = false;
        $("login-dialog-button-1").disabled = false;
        return;
    }

    if (password.length < 5) {
        validation.innerHTML = "Password cannot be shorter than 5 characters. We " +
                "did not allow this when you registered and still don't.";

        $("login-dialog-button-0").disabled = false;
        $("login-dialog-button-1").disabled = false;
        return;
    }

    validation.innerHTML = "";
    __login_dialog.ws_mwindow.close();

    __login_dialog_action_do(trimmed_un, password);
}

function __login_dialog_action_do(username, password) {
    show_global_progress("Logging in...");

    new Ajax.Request("callbacks/login", {
        method: "get",

        parameters: {
            "username": username,
            "password": password
        },

        onSuccess: function(transport) {
            current_user = transport.responseJSON.current_user;
            
            // We need this, as the graph code name might change at login. E.g. when the 
            // user was editing an untitled graph prior to logging in. In this case the
            // code name will change from 'untitled_nobody' to 'untitled_<username>'.
            current_graph = transport.responseJSON.current_graph;

            set_status_message("Login successful.", 1000, true);
            update_toolbar_buttons();
            update_graph_properties();
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __login_dialog_action_do(username, password);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to login. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            lock = false;
            hide_global_progress();
        }
    });
}


function __register_dialog_action() {
    $("register-dialog-button-0").disabled = true;
    $("register-dialog-button-1").disabled = true;

    var properties = __register_dialog.ws_psheet.get_properties();
    var validation = $("register-dialog-validation");

    var username = properties.username;
    var password = properties.password;
    var password2 = properties.password2;
    var first_name = properties.first_name;
    var last_name = properties.last_name;
    var company = properties.company;
    var email = properties.email;

    var trimmed_un = username.replace(/^\s+/, "").replace(/\s+$/, "");
    if (trimmed_un == "") {
        validation.innerHTML = "Username cannot be empty. An empty string is not a " +
                "good indentifier, is it?";

        $("register-dialog-button-0").disabled = false;
        $("register-dialog-button-1").disabled = false;
        return;
    }

    if (password.length < 5) {
        validation.innerHTML = "Password cannot be shorter than 5 characters. We " +
            "think it would too easy to guess if it is.";

        $("register-dialog-button-0").disabled = false;
        $("register-dialog-button-1").disabled = false;
        return;
    }

    if (password != password2) {
        validation.innerHTML = "The passwords you entered do not match. Are you " +
            "sure that you typed them in correctly?";

        $("register-dialog-button-0").disabled = false;
        $("register-dialog-button-1").disabled = false;
        return;
    }

    __register_dialog_action_do(trimmed_un, password, first_name, last_name, company, email);
}

function __register_dialog_action_do(username, password, first_name, last_name, company, email) {
    var validation = $("register-dialog-validation");

    lock = true;
    new Ajax.Request("callbacks/user-exists", {
        method: "get",

        parameters: {
            "username": username
        },

        onSuccess: function(transport) {
            var exists = transport.responseJSON.exists;

            if (!exists) {
                __register_dialog.ws_mwindow.close();
                validation.innerHTML = "";

                __register_dialog_action_do_do(username, password, first_name, last_name, company, email);
            } else {
                validation.innerHTML = "A user with this username " +
                        "already exists. You'll have to choose another one. Sorry.";
                    
                $("register-dialog-button-0").disabled = false;
                $("register-dialog-button-1").disabled = false;
            }
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                __register_dialog.ws_mwindow.close(true);
                reestablish_session(function() {
                    __register_dialog.ws_mwindow.show();
                    __register_dialog_action_do(username, password, first_name, last_name, company, email);
                });

                return;
            }

            Ws.Growl.notify_warning(
                    "Failed to check whether the user exists. " +
                    "The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);

            __register_dialog.ws_mwindow.close();
            validation.innerHTML = "";

            __register_dialog_action_do_do(username, password, first_name, last_name, company, email);
        },

        onComplete: function(transport) {
            lock = false;
        }
    });
}

function __register_dialog_action_do_do(username, password, first_name, last_name, company, email) {
    show_global_progress("Registering...");

    new Ajax.Request("callbacks/register", {
        method: "get",

        parameters: {
            "username": username,
            "password": password,
            "first_name": first_name,
            "last_name": last_name,
            "company": company,
            "email": email
        },

        onSuccess: function(transport) {
            current_user = transport.responseJSON.current_user;

            // We need this, as the graph code name might change upon registration, which
            // assumes automatic login. E.g. when the user was editing an untitled graph
            // prior to logging in. In this case the code name will change from
            // 'untitled_nobody' to 'untitled_<username>'.
            current_graph = transport.responseJSON.current_graph;

            set_status_message("Registration successful.", 1000, true);
            update_toolbar_buttons();
            update_graph_properties();
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __register_dialog_action_do_do(username, password, first_name, last_name, company, email);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to register. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            lock = false;
            hide_global_progress();
        }
    });
}


function __feedback_dialog_action() {
    $("feedback-dialog-button-0").disabled = true;
    $("feedback-dialog-button-1").disabled = true;

    var properties = __feedback_dialog.ws_psheet.get_properties();
    var validation = $("feedback-dialog-validation");

    var message = properties.message;

    var trimmed_m = message.replace(/^\s+/, "").replace(/\s+$/, "");
    if (trimmed_m == "") {
        validation.innerHTML = "You do not want to send an empty message, do you?";
        
        $("feedback-dialog-button-0").disabled = false;
        $("feedback-dialog-button-1").disabled = false;
        return;
    }

    validation.innerHTML = "";
    __feedback_dialog.ws_mwindow.close();

    __feedback_dialog_action_do(trimmed_m);
}

function __feedback_dialog_action_do(message) {
    show_global_progress("Sending feedback...");

    new Ajax.Request("callbacks/feedback", {
        method: "get",

        parameters: {
            "message": message
        },

        onSuccess: function(transport) {
            set_status_message("Feedback sent. Thanks!", 2000, true);
        },

        onFailure: function(transport) {
            if (transport.statusText == SESSION_TIMED_OUT_STATUS) {
                reestablish_session(function() {
                    __feedback_dialog_action_do(message);
                });
                return;
            }

            Ws.Growl.notify_error(
                    "Failed to send feeedback.<br/>" +
                    "The server responded:<br/>" +
                    "<i>" + transport.statusText + "</i>", -1, true);
        },

        onComplete: function(transport) {
            lock = false;
            hide_global_progress();
        }
    });
}

var __new_graph_pc = {
    pc_get_descriptors: function() {
        var descriptors = __graph_properties_pc.pc_get_descriptors().sections[0];

        descriptors.properties[0].read_only = false;
        descriptors.properties[0].value = "";
        descriptors.properties[1].value = "";
        descriptors.properties[2].value = "";

        return descriptors;
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __open_graph_pc = {
    graph_descriptors: [],

    set_graph_descriptors: function(descriptors) {
        this.graph_descriptors = descriptors;
    },

    pc_get_descriptors: function() {
        var i;

        if (this.graph_descriptors.length == 0) {
            return {
                properties: [
                    {
                        name: "sorry_message",
                        type: Ws.PropertiesSheet.LABEL,

                        display_name: "Sorry, but currently there are no existing " +
                            "message flows. Why don't you create one?",
                        description: ""
                    }
                ]
            }
        }

        var code_name_values = [];
        for (i = 0; i < this.graph_descriptors.length; i++) {
            code_name_values.push({
                value: this.graph_descriptors[i].code_name,
                display_name: this.graph_descriptors[i].display_name,
                description: this.graph_descriptors[i].description
            });
        }

        var properties = [
            {
                name: "code_name",
                type: Ws.PropertiesSheet.ENUM,

                display_name: "Message Flow",
                description: "Message flow you're going to open.",
                values: code_name_values,
                value: code_name_values[0].value
            },
            {
                name: "description",
                type: "text",

                display_name: "Description",
                description: "The textual description of the message flow.",
                read_only: true,

                dynamic: [],
                dynamic_defaults: {
                    value: this.graph_descriptors[0].description
                }
            }
        ];

        for (i = 0; i < this.graph_descriptors.length; i++) {
            properties[1].dynamic.push({
                name: "code_name",
                value: this.graph_descriptors[i].code_name,

                changes: {
                    value: this.graph_descriptors[i].description
                }
            });
        }

        return {
            properties : properties
        }
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __save_graph_pc = {
    pc_get_descriptors: function() {
        var descriptors = __graph_properties_pc.pc_get_descriptors().sections[0];

        descriptors.properties[0].read_only = false;
        descriptors.properties[0].value = "";
        descriptors.properties[1].value = "";
        descriptors.properties[2].value = "";

        return descriptors;
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __graph_properties_pc = {
    pc_get_descriptors: function() {
        return {
            sections: [
            {
                display_name: "general",
                description: "general properties",
                properties: [
                {
                    name: "code_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Code Name",
                    description: "The code name of the message flow. It will be " +
                    "used as the project name and also will form the message " +
                    "flow's namespace.",
                    read_only: true,
                    value: current_graph.code_name
                },
                {
                    name: "display_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Display Name",
                    description: "The human readable name of the message flow. " +
                    "This name will be used in all GUI forms.",
                    value: current_graph.display_name
                },
                {
                    name: "description",
                    type: Ws.PropertiesSheet.TEXT,

                    display_name: "Description",
                    description: "The textual description of the message flow.",
                    value: current_graph.description
                }
                ]
            }, {
                display_name: "Appearance",
                description: "Appearance properties",
                properties: [
                { 
                    name: "window_buttons",
                    type: Ws.PropertiesSheet.ENUM,

                    display_name: "windows and buttons",
                    description: "windows and buttons",
                    value: "standard",

                    values: [
                    {
                        display_name: "standard",
                        description: "Standard",
                        value: "standard"
                    },
                    {
                        display_name: "standard_1",
                        description: "standard_1",
                        value: "standard_1"
                    }
                    ]
                },
                {
                    name: "color_scheme",
                    type: Ws.PropertiesSheet.ENUM,

                    display_name: "color scheme",
                    description: "color scheme",
                    value: "black",

                    values: [
                    {
                        display_name: "black",
                        description: "",
                        value: "black"
                    },
                    {
                        display_name: "red",
                        description: "",
                        value: "red"
                    },
                    {
                        display_name: "green",
                        description: "",
                        value: "green"
                    },
                    {
                        display_name: "blue",
                        description: "",
                        value: "blue"
                    }
                    ]
                },
                {
                    name: "font_size",
                    type: Ws.PropertiesSheet.ENUM,

                    display_name: "font size",
                    description: "font_size",
                    value: "black",

                    values: [
                    {
                        display_name: "normal",
                        description: "",
                        value: "normal"
                    },
                    {
                        display_name: "large",
                        description: "",
                        value: "large"
                    }
                    ]
                    }
                ]
            }
            ]
        }
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __graph_properties_ro_pc = {
    pc_get_descriptors: function() {
        var container = __graph_properties_pc.pc_get_descriptors();

        for (var i = 0; i < container.properties.length; i++) {
            container.properties[i].read_only = true;
        }

        return container;
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __register_pc = {
    pc_get_descriptors: function() {
        return {
            properties: [
                {
                    name: "username",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Username",
                    description: "Username to use when logging into the system.",
                    value: ""
                },
                {
                    name: "password",
                    type: Ws.PropertiesSheet.PASSWORD,

                    display_name: "Password",
                    description: "Password to use when logging into the system.",
                    value: ""
                },
                {
                    name: "password2",
                    type: Ws.PropertiesSheet.PASSWORD,

                    display_name: "Repeat Password",
                    description: "Retype the password you entered in the previous field.",
                    value: ""
                },
                {
                    name: "first_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "First Name",
                    description: "Your first name.",
                    value: ""
                },
                {
                    name: "last_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Last Name",
                    description: "Your last name.",
                    value: ""
                },
                {
                    name: "company",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Company",
                    description: "Where you work.",
                    value: ""
                },
                {
                    name: "email",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Email",
                    description: "Your email address.",
                    value: ""
                }
            ]
        }
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __feedback_pc = {
    pc_get_descriptors: function() {
        return {
            properties: [
                {
                    name: "message",
                    type: Ws.PropertiesSheet.TEXT,

                    display_name: "Message",
                    description: "Text of the message you want to send to us.",
                    value: ""
                }
            ]
        }
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}
