/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
var tests_list_dialog = null;
var test_dialogs = {};
var message_dialogs = {};
var result_dialogs = {};
var test_runing_window = null;
var is_runed_test = false;
var runing_tests = [];

var count_edit_tests = 0;
var current_id_edit_test;

function toolbar_handle_open_list_tests() {
    if (!tests_by_id) {
        new Ajax.Request("callbacks/get-tests", {
            method: "get",

            onSuccess: function(response) {
                var i, j, k, message, result;

                var tests = response.responseJSON.tests;
                tests.pop();

                tests_by_id = {};

                for (i = 0; i < tests.length; i++) {
                    var id = tests[i].id;
                    var input_messages = tests[i].input_messages;
                    var output_messages = tests[i].output_messages;
                    var results = tests[i].results;

                    input_messages.pop();
                    output_messages.pop();
                    results.pop();

                    tests_by_id[id] = tests[i];
                    tests_by_id[id].input_messages = {};
                    tests_by_id[id].output_messages = {};
                    tests_by_id[id].results = {};

                    for (j = 0; j < input_messages.length; j++) {
                        message = input_messages[j];
                        tests_by_id[id].input_messages[message.id] = message;
                    }

                    for (j = 0; j < output_messages.length; j++) {
                        message = output_messages[j];
                        tests_by_id[id].output_messages[message.id] = message;
                    }

                    for (j = 0; j < results.length; j++) {
                        result = results[j];
                        var messages = result.messages;

                        tests_by_id[id].results[result.id] = result;
                        tests_by_id[id].results[result.id].messages = {};

                        messages.pop();
                        for (k = 0; k < messages.length; k++) {
                            tests_by_id[id].results[result.id].messages[messages[k].id] = messages[k];
                        }
                    }
                }

                toolbar_handle_open_list_tests();
            }
        });
        
        return;
    }

    if (!tests_list_dialog) {
        tests_list_dialog = new Element("div");
        tests_list_dialog.addClassName("-ws-psheet-window");

        var display_name = graph_properties_container.display_name;
        if (display_name == "") {
            display_name = "<i>untitled</i>";
        }

        tests_list_dialog.innerHTML = "" +
        "<div class=\"-wsfw-header\">Tests for " + display_name + "</div>" +
        "<div class=\"-wsfw-contents\"></div>" +
        "<div class=\"-wsfw-footer\">" +
        "<input class=\"run\" type=\"button\" value=\"Run\"/>" +
        "<input class=\"create\" type=\"button\" value=\"Create\"/>" +
        "<input class=\"close\" type=\"button\" value=\"Close\"/>" +
        "</div>";

        document.body.appendChild(tests_list_dialog);

        new Ws.WindowedPropertiesSheet(
            tests_list_dialog,
            tests_list_container,
            {
                functions_change: [
                {
                    funct: show_test_dialog,
                    type: "properties"
                },

                {
                    funct: delete_test,
                    type: "delete"
                },

                {
                    funct: move_up_function_tests_list_dialog,
                    type: "up"
                },

                {
                    funct: move_down_function_tests_list_dialog,
                    type: "down"
                },
                {
                    funct: run_test,
                    type: "run_test"
                }
                ]
            }
            );

        tests_list_dialog.select("input.run")[0].onclick = function() {
            run_tests();
        }

        tests_list_dialog.select("input.create")[0].onclick = function() {
            var tests = get_array_by_field(tests_by_id, "offset");
            var offset = 1000;

            if (tests.length > 0) {
                offset = tests[tests.length - 1].offset + 1000;
            }

            var new_test = {
                display_name: "New Test",
                description: "",
                input_messages: {},
                output_messages: {},
                results: {},
                timeout: 1000,
                output_ordered: false,
                offset: offset
            };

            var request = "";

            request += "{";
            request += "  display_name: " + Object.toJSON(new_test.display_name) + ",";
            request += "  description: " + Object.toJSON(new_test.description) + ",";
            request += "  updated_input_messages: [],";
            request += "  updated_output_messages: [],";
            request += "  removed_input_messages: [],";
            request += "  removed_output_messages: [],";
            request += "  removed_results: [],";
            request += "  timeout: " + Object.toJSON(new_test.timeout) + ",";
            request += "  output_ordered: " + Object.toJSON(new_test.output_ordered) + ",";
            request += "  offset: " + Object.toJSON(new_test.offset) + "";
            request += "}";

            new Ajax.Request("callbacks/update-test", {
                method: "post",
                postBody: request,

                onSuccess: function(response) {
                    var id = response.responseJSON.id;

                    new_test.id = id;
                    tests_by_id[id] = new_test;
                    new_test.display_name += id;

                    // change size on properties-sheet
                    tests_list_dialog.ws_psheet.reloadPropertiesContainer();
                    tests_list_dialog.windowed_ps_sheet.relayout();
                },

                onFailure: function(response) {
                // Does nothing
                },

                onComplete: function(response) {
                // Does nothing
                }
            });
        }

        tests_list_dialog.select("input.close")[0].onclick = function() {
            tests_list_dialog.setStyle({
                display: "none"
            });
        }
    } else {
        tests_list_dialog.show();
    }
}

function move_up_function_tests_list_dialog(id) {
    var tests = get_array_by_field(tests_by_id, "offset");

    var i;
    for (i = 0; i < tests.length; i++) {
        if (tests[i].id == id) {
            break;
        }
    }

    if (i == 0) {
    // Take no action, we're already the first test. The button itself
    // should've been disabled though.
    } else {
        var smaller_offset = tests[i - 1].offset;
        var current_offset = tests[i].offset;

        var request;

        var first_request_completed = false;

        function handle_success(response) {
            if (!first_request_completed) {
                first_request_completed = true;
            } else {
                tests[i - 1].offset = current_offset;
                tests[i].offset = smaller_offset;

                tests_list_dialog.ws_psheet.reset();
            }
        }

        request = "";

        request += "{";
        request += "  id: " + id + ",";
        request += "  offset: " + smaller_offset + "";
        request += "}";

        new Ajax.Request("callbacks/update-test", {
            method: "post",
            postBody: request,

            onSuccess: handle_success,

            onFailure: function(response) {
            // Does nothing
            },

            onComplete: function(response) {
            // Does nothing
            }
        });

        request = "";

        request += "{";
        request += "  id: " + tests[i - 1].id + ",";
        request += "  offset: " + current_offset + "";
        request += "}";

        new Ajax.Request("callbacks/update-test", {
            method: "post",
            postBody: request,

            onSuccess: handle_success,

            onFailure: function(response) {
            // Does nothing
            },

            onComplete: function(response) {
            // Does nothing
            }
        });
    }
}

function move_down_function_tests_list_dialog(id) {
    var tests = get_array_by_field(tests_by_id, "offset");

    var i;
    for (i = 0; i < tests.length; i++) {
        if (tests[i].id == id) {
            break;
        }
    }

    if (i == tests.length - 1) {
    // Take no action, we're already the last test. The button itself
    // should've been disabled though.
    } else {
        var bigger_offset = tests[i + 1].offset;
        var current_offset = tests[i].offset;

        var request;

        var first_request_completed = false;

        function handle_success(response) {
            if (!first_request_completed) {
                first_request_completed = true;
            } else {
                tests[i + 1].offset = current_offset;
                tests[i].offset = bigger_offset;

                tests_list_dialog.ws_psheet.reset();
            }
        }

        request = "";

        request += "{";
        request += "  id: " + id + ",";
        request += "  offset: " + bigger_offset + "";
        request += "}";

        new Ajax.Request("callbacks/update-test", {
            method: "post",
            postBody: request,

            onSuccess: handle_success,

            onFailure: function(response) {
            // Does nothing
            },

            onComplete: function(response) {
            // Does nothing
            }
        });

        request = "";

        request += "{";
        request += "  id: " + tests[i + 1].id + ",";
        request += "  offset: " + current_offset + "";
        request += "}";

        new Ajax.Request("callbacks/update-test", {
            method: "post",
            postBody: request,

            onSuccess: handle_success,

            onFailure: function(response) {
            // Does nothing
            },

            onComplete: function(response) {
            // Does nothing
            }
        });
    }
}

function delete_test(id) {
    var request = "";

    request += "{";
    request += "  id: " + id + "";
    request += "}";

    new Ajax.Request("callbacks/delete-test", {
        method: "post",
        postBody: request,

        onSuccess: function(response) {
            if (test_dialogs[id]) {
                test_dialogs[id].hide();
            }

            test_dialogs[id] = null;
            tests_by_id[id] = null;

            tests_list_dialog.ws_psheet.reloadPropertiesContainer();
            tests_list_dialog.windowed_ps_sheet.relayout();

            test_runing_window.ws_psheet.reloadPropertiesContainer();
            test_runing_window.windowed_ps_sheet.relayout();
        },

        onFailure: function(response) {
        // Does nothing
        },

        onComplete: function(response) {
        // Does nothing
        }
    });

}

function show_test_dialog(id) {
    if (!test_dialogs[id]) {
        test_dialogs[id] = new Element("div");
        test_dialogs[id].addClassName("-ws-psheet-window");

        test_dialogs[id].innerHTML = "" +
        "<div class=\"-wsfw-header\">" + tests_by_id[id].display_name + "</div>" +
        "<div class=\"-wsfw-contents\"></div>" +
        "<div class=\"-wsfw-footer\">" +
        "<input class=\"new_input\" type=\"button\" value=\"New In\"/>" +
        "<input class=\"new_output\" type=\"button\" value=\"New Out\"/>" +
        "<input class=\"save\" type=\"button\" value=\"Save\"/>" +
        "<input class=\"cancel\" type=\"button\" value=\"Cancel\"/>" +
        "</div>";

        test_dialogs[id].setStyle({
            minWidth: 425 + "px"
        });
        document.body.appendChild(test_dialogs[id]);
        count_edit_tests++;
        current_id_edit_test = id;
        //      $("diagram").ws_graph.repaint();

        new Ws.WindowedPropertiesSheet(
            test_dialogs[id],
            create_test_container(id),
            {
                functions_change: [
                {
                    funct: show_messages_dialog,
                    type: "properties"
                },
                {
                    funct: delete_message,
                    type: "delete"
                },
                {
                    funct: move_up_messages_in_test,
                    type: "up"
                },
                {
                    funct: move_down_messages_in_test,
                    type: "down"                
                },
                ]
            }
            );

        test_dialogs[id].select("input.new_input")[0].onclick = function() {
            var offset = 1000;

            var messages = get_array_by_field(tests_by_id[id].input_messages, "offset");
            if (messages.length > 0) {
                offset = messages[messages.length - 1].offset + 1000;
            }

            var new_message = {
                content: "",
                service: 0,
                inbound: true,
                time: 0,
                offset: offset
            }

            var request = "";

            request += "{";
            request += "  id: " + id + ",";
            request += "  updated_input_messages: [";
            request += "    {";
            request += "      content: " + Object.toJSON(new_message.content) + ",";
            request += "      service: " + new_message.service + ",";
            request += "      inbound: " + new_message.inbound + ",";
            request += "      time: " + new_message.time + ",";
            request += "      offset: " + new_message.offset + "";
            request += "    }";
            request += "  ]";
            request += "}";

            new Ajax.Request("callbacks/update-test", {
                method: "post",
                postBody: request,

                onSuccess: function(response) {
                    var message_id = response.responseJSON.input_messages[0];

                    new_message.id = message_id;
                    tests_by_id[id].input_messages[message_id] = new_message;

                    test_dialogs[id].ws_psheet.reloadPropertiesContainer();
                    test_dialogs[id].windowed_ps_sheet.relayout();
                },

                onFailure: function(response) {
                // Does nothing
                },

                onComplete: function(response) {
                // Does nothing
                }
            });
        };

        test_dialogs[id].select("input.new_output")[0].onclick = function() {
            var offset = 1000;

            var messages = get_array_by_field(tests_by_id[id].output_messages, "offset");
            if (messages.length > 0) {
                offset = messages[messages.length - 1].offset + 1000;
            }

            var new_message = {
                content: "",
                service: 0,
                inbound: true,
                time: 0,
                offset: offset
            }

            var request = "";

            request += "{";
            request += "  id: " + id + ",";
            request += "  updated_output_messages: [";
            request += "    {";
            request += "      content: " + Object.toJSON(new_message.content) + ",";
            request += "      service: " + new_message.service + ",";
            request += "      inbound: " + new_message.inbound + ",";
            request += "      time: " + new_message.time + ",";
            request += "      offset: " + new_message.offset + "";
            request += "    }";
            request += "  ]";
            request += "}";

            new Ajax.Request("callbacks/update-test", {
                method: "post",
                postBody: request,

                onSuccess: function(response) {
                    var message_id = response.responseJSON.output_messages[0];

                    new_message.id = message_id;
                    tests_by_id[id].output_messages[message_id] = new_message;

                    test_dialogs[id].ws_psheet.reloadPropertiesContainer();
                    test_dialogs[id].windowed_ps_sheet.relayout();
                },

                onFailure: function(response) {
                // Does nothing
                },

                onComplete: function(response) {
                // Does nothing
                }
            });
        };

        test_dialogs[id].select("input.save")[0].onclick = function() {
            var properties = test_dialogs[id].ws_psheet.get_properties();
            var request = "";

            tests_by_id[id].display_name = properties.display_name;
            tests_by_id[id].description = properties.description;
            tests_by_id[id].timeout = properties.timeout;
            tests_by_id[id].output_ordered = properties.output_ordered;

            request += "{";
            request += "  id: " + id + ",";
            request += "  display_name: " + Object.toJSON(tests_by_id[id].display_name) + ",";
            request += "  description: " + Object.toJSON(tests_by_id[id].description) + ",";
            request += "  updated_input_messages: [],";
            request += "  updated_output_messages: [],";
            request += "  removed_input_messages: [],";
            request += "  removed_output_messages: [],";
            request += "  removed_results: [],";
            request += "  timeout: " + Object.toJSON(tests_by_id[id].timeout) + ",";
            request += "  output_ordered: " + Object.toJSON(tests_by_id[id].output_ordered) + ",";
            request += "  offset: " + Object.toJSON(tests_by_id[id].offset) + "";
            request += "}";

            new Ajax.Request("callbacks/update-test", {
                method: "post",
                postBody: request,

                onSuccess: function(response) {
                    test_dialogs[id].select("div.-wsfw-header")[0].innerHTML =
                    tests_by_id[id].display_name;

                    tests_list_dialog.ws_psheet.reloadPropertiesContainer();
                    tests_list_dialog.windowed_ps_sheet.relayout();
                },

                onFailure: function(response) {
                // Does nothing
                },

                onComplete: function(response) {
                // Does nothing
                }
            });

            test_dialogs[id].hide();
            count_edit_tests--;
            $("diagram").ws_graph.ui.repaint();
        };

        test_dialogs[id].select("input.cancel")[0].onclick = function() {
            test_dialogs[id].hide();
            count_edit_tests--;
            $("diagram").ws_graph.ui.repaint();
        };
    } else {
        test_dialogs[id].show();
        count_edit_tests++;
        current_id_edit_test = id;
    }
    $("diagram").ws_graph.ui.repaint();
}

function move_up_messages_in_test(test_id, message_id) {
    var input = false;
    if (tests_by_id[test_id].input_messages[message_id]) {
        input = true;
    }

    var messages;
    if (input) {
        messages = get_array_by_field(tests_by_id[test_id].input_messages, "offset");
    } else {
        messages = get_array_by_field(tests_by_id[test_id].output_messages, "offset");
    }

    var i;
    for (i = 0; i < messages.length; i++) {
        if (messages[i].id == message_id) {
            break;
        }
    }

    if (i == 0) {
    // Take no action, we're already the first test. The button itself
    // should've been disabled though.
    } else {
        var smaller_offset = messages[i - 1].offset;
        var current_offset = messages[i].offset;

        var request;

        request = "";

        request += "{";
        request += "  id: " + test_id + ",";
        request += "  updated_input_messages: [";
        if (input) {
            request += "    {";
            request += "      id: " + message_id + ",";
            request += "      offset: " + smaller_offset + ",";
            request += "    },";
            request += "    {";
            request += "      id: " + messages[i - 1].id + ",";
            request += "      offset: " + current_offset + ",";
            request += "    }";
        }
        request += "  ],";
        request += "  updated_output_messages: [";
        if (!input) {
            request += "    {";
            request += "      id: " + message_id + ",";
            request += "      offset: " + smaller_offset + ",";
            request += "    },";
            request += "    {";
            request += "      id: " + messages[i - 1].id + ",";
            request += "      offset: " + current_offset + ",";
            request += "    }";
        }
        request += "  ]";
        request += "}";

        new Ajax.Request("callbacks/update-test", {
            method: "post",
            postBody: request,

            onSuccess: function(response) {
                messages[i - 1].offset = current_offset;
                messages[i].offset = smaller_offset;

                test_dialogs[test_id].ws_psheet.reset();
            },

            onFailure: function(response) {
            // Does nothing
            },

            onComplete: function(response) {
            // Does nothing
            }
        });
    }
}

function move_down_messages_in_test(test_id, message_id) {
    var input = false;
    if (tests_by_id[test_id].input_messages[message_id]) {
        input = true;
    }

    var messages;
    if (input) {
        messages = get_array_by_field(tests_by_id[test_id].input_messages, "offset");
    } else {
        messages = get_array_by_field(tests_by_id[test_id].output_messages, "offset");
    }

    var i;
    for (i = 0; i < messages.length; i++) {
        if (messages[i].id == message_id) {
            break;
        }
    }

    if (i == messages.length - 1) {
    // Take no action, we're already the first test. The button itself
    // should've been disabled though.
    } else {
        var bigger_offset = messages[i + 1].offset;
        var current_offset = messages[i].offset;

        var request;

        request = "";

        request += "{";
        request += "  id: " + test_id + ",";
        request += "  updated_input_messages: [";
        if (input) {
            request += "    {";
            request += "      id: " + message_id + ",";
            request += "      offset: " + bigger_offset + ",";
            request += "    },";
            request += "    {";
            request += "      id: " + messages[i + 1].id + ",";
            request += "      offset: " + current_offset + ",";
            request += "    }";
        }
        request += "  ],";
        request += "  updated_output_messages: [";
        if (!input) {
            request += "    {";
            request += "      id: " + message_id + ",";
            request += "      offset: " + bigger_offset + ",";
            request += "    },";
            request += "    {";
            request += "      id: " + messages[i + 1].id + ",";
            request += "      offset: " + current_offset + ",";
            request += "    }";
        }
        request += "  ]";
        request += "}";

        new Ajax.Request("callbacks/update-test", {
            method: "post",
            postBody: request,

            onSuccess: function(response) {
                messages[i + 1].offset = current_offset;
                messages[i].offset = bigger_offset;

                test_dialogs[test_id].ws_psheet.reset();
            },

            onFailure: function(response) {
            // Does nothing
            },

            onComplete: function(response) {
            // Does nothing
            }
        });
    }
}

function run_test(test_id) {
    run_tests([tests_by_id[test_id]]);
}

function run_tests(tests) {
    if (test_runing_window == null) {
        test_runing_window = new Element("div");

        test_runing_window.addClassName("-ws-psheet-window");

        test_runing_window.innerHTML =
        "<div class=\"-wsfw-header\">" +
        "Properties &mdash; " + "Tests" +
        "</div>" +
        "<div class=\"-wsfw-contents\">" +
    
        "</div>" +
    
        "<div class=\"-wsfw-footer\">" +

        "<div class=\"progress_bar\"><i>Runing...</i> <span class=\"progress_bar\" style=\"width: 14px; height: 14px; display: inline-block;\"></span></div>" +
        "<input class=\"ok\" type=\"button\" value=\"Ok\"/>" +
        "<input class=\"cansel\" type=\"button\" value=\"Cancel\"/>" +
        "</div>";

        document.body.appendChild(test_runing_window);

        test_runing_container.test_result = {};

        var window_dimension = Ws.Utils.get_window_dimensions();
        new Ws.WindowedPropertiesSheet(test_runing_window, test_runing_container, {} ,
        {
            isCenter      : true,
            centerX       : 2 * window_dimension.width / 3 - 5,
            centerY       : 7 * window_dimension.height / 8 - 5,
            window_width  : 2 * window_dimension.width / 3,
            window_height : window_dimension.height / 4 + 10
        });

        var progress_bar = new Ws.ProgressBar(test_runing_window.select("span.progress_bar")[0], {
            fill: "rgb(255, 255, 255)"
        });
        progress_bar.hide();

        test_runing_window.select("input.ok")[0].onclick = function() {
            // progress_bar.remove();
            test_runing_window.hide();
            runing_tests = [];

        };

        test_runing_window.select("input.cansel")[0].onclick = function() {
            // progress_bar.remove();
            test_runing_window.hide();
            runing_tests = [];
        };
    } else {
        test_runing_window.show();
    }

    if (tests != null || tests == "undefined") {
        runing_tests = runing_tests.concat(tests);
    } else {
        runing_tests = runing_tests.concat(get_array_by_field(tests_by_id, "offset"));
    }

    if (runing_tests.length > 0 && !is_runed_test) {
        is_runed_test = true;
        test_runing_container.test_result = {};
        test_runing_window.ws_psheet.reset();
        test_runing_window.select("div.progress_bar")[0].show();
        test_runing_window.select("span.progress_bar")[0].ws_progress_bar.create();


        run_test_recursive();
    }
}

function run_test_recursive() {
    if (runing_tests.length <= 0) {
        is_runed_test = false;
        test_runing_window.select("div.progress_bar")[0].hide();
        //test_runing_window.select("span.progress_bar")[0].ws_progress_bar.remove();
        return;
    }

    var test =  runing_tests.shift();

    receive_test_messages.args = [];
    send_test_messages.args = [];

    for (var i in test.input_messages) {
        if (test.input_messages[i].inbound) {
            receive_test_messages.args.push(i);
            window.setTimeout(
                function() {
                    var message_id = receive_test_messages.args.pop()
                    receive_test_messages(test.id, message_id, test.input_messages[message_id].service);
                }, 1000 * test.input_messages[i]);
        } else {
            send_test_messages.args.push(i);
            window.setTimeout(
                function() {
                    var message_id = send_test_messages.args.pop()
                    send_test_messages(test.id, message_id, test.input_messages[message_id].service);
                }, 1000 * test.input_messages[i]);
        }
    }

    var request = "{id :" + test.id + "}";

    new Ajax.Request("callbacks/run-test", {
        method: "post",
        postBody: request,

        onSuccess: function(response) {
            test_runing_container.addTestPassed(test.id);
            test_runing_window.ws_psheet.reloadPropertiesContainer();
        
            run_test_recursive();
        },

        onFailure: function(response) {
        // Does nothing
        },

        onComplete: function(response) {
        // Does nothing
        }
    });
}

function receive_test_messages(test_id, message_id, node_id) {
    var node = $('diagram').ws_graph.get_node_by_id(node_id);

    tracing_enabled = true;
    if (!tracing_data[node_id]) {
        tracing_data[node_id] = {
            inbound: [],
            outbound: []
        }
    }

    tracing_data[node_id].inbound.push(tests_by_id[test_id].input_messages[message_id]);
    tracing_data[node_id].inbound.has_new = true;

    if (tracing_messages_list_dialogs[node_id]) {
        if (tracing_messages_list_dialogs[node_id].inbound) {
            tracing_messages_list_dialogs[node_id].inbound.ws_psheet.reloadPropertiesContainer();
            tracing_messages_list_dialogs[node_id].inbound.windowed_ps_sheet.relayout();
        }
    }

    node.ui.repaint();

    if (node.__ws_psheet) {
        node.__ws_psheet.reloadPropertiesContainer();
        node.__ws_psheet_window.windowed_ps_sheet.relayout();
    }
    setTimeout( function() {
        send_test_messages(test_id, message_id, node_id);
    }, 150);
}

function show_message_dialog_for_run_simple_test(node_id) {
    tests_by_id = {};
    var tests = get_array_by_field(tests_by_id, "offset");
    var offset = 1000;

    if (tests.length > 0) {
        offset = tests[tests.length - 1].offset + 1000;
    }

    var new_test = {
        display_name: "New Test",
        description: "",
        input_messages: {},
        output_messages: {},
        results: {},
        timeout: 1000,
        output_ordered: false,
        offset: offset
    };

    var request = "";

    request += "{";
    request += "  display_name: " + Object.toJSON(new_test.display_name) + ",";
    request += "  description: " + Object.toJSON(new_test.description) + ",";
    request += "  updated_input_messages: [],";
    request += "  updated_output_messages: [],";
    request += "  removed_input_messages: [],";
    request += "  removed_output_messages: [],";
    request += "  removed_results: [],";
    request += "  timeout: " + Object.toJSON(new_test.timeout) + ",";
    request += "  output_ordered: " + Object.toJSON(new_test.output_ordered) + ",";
    request += "  offset: " + Object.toJSON(new_test.offset) + "";
    request += "}";

    new Ajax.Request("callbacks/update-test", {
        method: "post",
        postBody: request,

        onSuccess: function(response) {
            var _current_id_edit_test = response.responseJSON.id;

            new_test.id = _current_id_edit_test;
            tests_by_id[_current_id_edit_test] = new_test;
            new_test.display_name += _current_id_edit_test;

            // change size on properties-sheet
            if (tests_list_dialog && tests_list_dialog.ws_psheet) {
                tests_list_dialog.ws_psheet.reloadPropertiesContainer();
                tests_list_dialog.windowed_ps_sheet.relayout();
            }

            //new message
            var offset = 1000;

            var messages = get_array_by_field(
                tests_by_id[_current_id_edit_test].input_messages, "offset");
            if (messages.length > 0) {
                offset = messages[messages.length - 1].offset + 1000;
            }

            var new_message = {
                content: "The test message",
                service: node_id,
                inbound: true,
                time: 0,
                offset: offset
            }

            var request = "";

            request += "{";
            request += "  id: " + _current_id_edit_test + ",";
            request += "  updated_input_messages: [";
            request += "    {";
            request += "      content: " + Object.toJSON(new_message.content) + ",";
            request += "      service: " + new_message.service + ",";
            request += "      inbound: " + new_message.inbound + ",";
            request += "      time: " + new_message.time + ",";
            request += "      offset: " + new_message.offset + "";
            request += "    }";
            request += "  ]";
            request += "}";

            new Ajax.Request("callbacks/update-test", {
                method: "post",
                postBody: request,

                onSuccess: function(response) {
                    var message_id = response.responseJSON.input_messages[0];

                    new_message.id = message_id;
                    tests_by_id[_current_id_edit_test].input_messages[message_id] = new_message;

                    if (test_dialogs && test_dialogs[_current_id_edit_test] && test_dialogs[_current_id_edit_test].ws_psheet) {
                        test_dialogs[_current_id_edit_test].ws_psheet.reloadPropertiesContainer();
                        test_dialogs[_current_id_edit_test].windowed_ps_sheet.relayout();
                    }

                    // show_messages_dialog with posibility run test
                    if (!message_dialogs[message_id]) {
                        message_dialogs[message_id] = new Element("div");
                        message_dialogs[message_id].addClassName("-ws-psheet-window");

                        message_dialogs[message_id].innerHTML = "" +
                        "<div class=\"-wsfw-header\">Message</div>" +
                        "<div class=\"-wsfw-contents\"></div>" +
                        "<div class=\"-wsfw-footer\">" +
                        "<input class=\"delete\" type=\"button\" value=\"Delete\"/>" +
                        "<input class=\"run\" type=\"button\" value=\"Run test\"/>" +
                        "<input class=\"cancel\" type=\"button\" value=\"Cancel\"/>" +
                        "</div>";

                        document.body.appendChild(message_dialogs[message_id]);

                        new Ws.WindowedPropertiesSheet(
                            message_dialogs[message_id],
                            create_test_message_container(_current_id_edit_test, message_id));

                        message_dialogs[message_id].select("input.run")[0].onclick = function() {
                            var properties = message_dialogs[message_id].ws_psheet.get_properties();
                            var request = "";

                            var input = false;
                            if (tests_by_id[_current_id_edit_test].input_messages[message_id]) {
                                input = true;
                            }

                            var message;
                            if (input) {
                                message = tests_by_id[_current_id_edit_test].input_messages[message_id];
                            } else {
                                message = tests_by_id[_current_id_edit_test].output_messages[message_id];
                            }

                            message.content = properties.content;
                            message.service = (properties.service == null ? 0 : properties.service);
                            message.inbound = properties.inbound;
                            message.time = properties.time;

                            var message_request = "";

                            message_request += "{";
                            message_request += "  id: " + message.id + ",";
                            message_request += "  content: " + Object.toJSON(message.content) + ",";
                            message_request += "  service: " + message.service + ",";
                            message_request += "  inbound: " + message.inbound + ",";
                            message_request += "  time: " + message.time + "";
                            message_request += "}";

                            request += "{";
                            request += "  id: " + _current_id_edit_test + ",";
                            request += "  updated_input_messages: [" + (input ? message_request : "") + "],";
                            request += "  updated_output_messages: [" + (input ? "" : message_request) + "],";
                            request += "}";

                            new Ajax.Request("callbacks/update-test", {
                                method: "post",
                                postBody: request,

                                onSuccess: function(response) {
                                    //test_dialogs[_current_id_edit_test].ws_psheet.reset();
                                    run_test(_current_id_edit_test);
                                },

                                onFailure: function(response) {
                                // Does nothing
                                },

                                onComplete: function(response) {
                                // Does nothing
                                }
                            });

                            message_dialogs[message_id].hide();
                        };

                        message_dialogs[message_id].select("input.cancel")[0].onclick = function() {
                            message_dialogs[message_id].hide();
                        };
                    } else {
                        message_dialogs[message_id].show();
                    }
                },

                onFailure: function(response) {
                // Does nothing
                },

                onComplete: function(response) {
                // Does nothing
                }
            });
        },

        onFailure: function(response) {
        // Does nothing
        },

        onComplete: function(response) {
        // Does nothing
        }
    });





}


function send_test_messages(test_id, message_id, node_id) {
    var node = $('diagram').ws_graph.get_node_by_id(node_id);

    tracing_enabled = true;
    if (!tracing_data[node_id]) {
        tracing_data[node_id] = {
            inbound: [],
            outbound: []
        }
    }

    tracing_data[node_id].outbound.push(tests_by_id[test_id].input_messages[message_id]);
    tracing_data[node_id].outbound.has_new = true;

    if (tracing_messages_list_dialogs[node_id]) {
        if (tracing_messages_list_dialogs[node_id].outbound) {
            tracing_messages_list_dialogs[node_id].outbound.ws_psheet.reloadPropertiesContainer();
            tracing_messages_list_dialogs[node_id].outbound.windowed_ps_sheet.relayout();
        }
    }

    node.ui.repaint();

    if (node.__ws_psheet) {
        node.__ws_psheet.reloadPropertiesContainer();
        node.__ws_psheet_window.windowed_ps_sheet.relayout();
    }   

    var links = $('diagram').ws_graph.get_outbound_links_by_node(node);

    var link_function = function(){
        for (var i = 0; i < links.length; i++) {
            receive_test_messages(test_id, message_id, links[i].end_node.id);
        }
    }
    setTimeout(link_function , 150);
}

function show_messages_dialog(test_id, message_id, result_id) {
    if (!result_id) {
        if (!message_dialogs[message_id]) {
            message_dialogs[message_id] = new Element("div");
            message_dialogs[message_id].addClassName("-ws-psheet-window");

            message_dialogs[message_id].innerHTML = "" +
            "<div class=\"-wsfw-header\">Message</div>" +
            "<div class=\"-wsfw-contents\"></div>" +
            "<div class=\"-wsfw-footer\">" +
            "<input class=\"delete\" type=\"button\" value=\"Delete\"/>" +
            "<input class=\"save\" type=\"button\" value=\"Save\"/>" +
            "<input class=\"cancel\" type=\"button\" value=\"Cancel\"/>" +
            "</div>";

            document.body.appendChild(message_dialogs[message_id]);

            new Ws.WindowedPropertiesSheet(
                message_dialogs[message_id],
                create_test_message_container(test_id, message_id));

            message_dialogs[message_id].select("input.save")[0].onclick = function() {
                var properties = message_dialogs[message_id].ws_psheet.get_properties();
                var request = "";

                var input = false;
                if (tests_by_id[test_id].input_messages[message_id]) {
                    input = true;
                }

                var message;
                if (input) {
                    message = tests_by_id[test_id].input_messages[message_id];
                } else {
                    message = tests_by_id[test_id].output_messages[message_id];
                }

                message.content = properties.content;
                message.service = (properties.service == null ? 0 : properties.service);
                message.inbound = properties.inbound;
                message.time = properties.time;

                var message_request = "";

                message_request += "{";
                message_request += "  id: " + message.id + ",";
                message_request += "  content: " + Object.toJSON(message.content) + ",";
                message_request += "  service: " + message.service + ",";
                message_request += "  inbound: " + message.inbound + ",";
                message_request += "  time: " + message.time + "";
                message_request += "}";

                request += "{";
                request += "  id: " + test_id + ",";
                request += "  updated_input_messages: [" + (input ? message_request : "") + "],";
                request += "  updated_output_messages: [" + (input ? "" : message_request) + "],";
                request += "}";

                new Ajax.Request("callbacks/update-test", {
                    method: "post",
                    postBody: request,

                    onSuccess: function(response) {
                        test_dialogs[test_id].ws_psheet.reset();
                    },

                    onFailure: function(response) {
                    // Does nothing
                    },

                    onComplete: function(response) {
                    // Does nothing
                    }
                });

                message_dialogs[message_id].hide();
            };

            message_dialogs[message_id].select("input.cancel")[0].onclick = function() {
                message_dialogs[message_id].hide();
            };
        } else {
            message_dialogs[message_id].show();
        }
    } else {
        if (!result_dialogs[result_id]) {
            result_dialogs[result_id] = new Element("div");
            result_dialogs[result_id].addClassName("-ws-psheet-window");

            result_dialogs[result_id].innerHTML = "" +
            "<div class=\"-wsfw-header\">Result of " + (new Date(tests_by_id[test_id].results[result_id].start_time).toLocaleString()) + "</div>" +
            "<div class=\"-wsfw-contents\"></div>" +
            "<div class=\"-wsfw-footer\">" +
            "<input class=\"delete\" type=\"button\" value=\"Delete\"/>" +
            "<input class=\"close\" type=\"button\" value=\"Close\"/>" +
            "</div>";

            document.body.appendChild(result_dialogs[result_id]);

            new Ws.WindowedPropertiesSheet(
                result_dialogs[result_id],
                create_result_container(test_id, result_id),
                {
                    functions_change: [{
                        funct: change_function_test_result_dialog,
                        type:"properties"
                    }]
                });

            result_dialogs[result_id].select("input.delete")[0].onclick = function() {
                var request = "";

                request += "{";
                request += "  id: " + test_id + ",";
                request += "  removed_results: [" + result_id + "]";
                request += "}";

                new Ajax.Request("callbacks/update-test", {
                    method: "post",
                    postBody: request,

                    onSuccess: function(response) {
                        if (result_dialogs[result_id]) {
                            result_dialogs[result_id].hide();
                        }

                        tests_by_id[test_id].results[result_id] = null;

                        result_dialogs[result_id] = null;

                        test_dialogs[test_id].ws_psheet.reset();
                    },

                    onFailure: function(response) {
                    // Does nothing
                    },

                    onComplete: function(response) {
                    // Does nothing
                    }
                });
            }

            result_dialogs[result_id].select("input.close")[0].onclick = function() {
                result_dialogs[result_id].hide();
            }
        }
    }
}

function delete_message(test_id, message_id) {
    var input = false;
    if (tests_by_id[test_id].input_messages[message_id]) {
        input = true;
    }

    var request = "";

    request += "{";
    request += "  id: " + test_id + ",";
    request += "  removed_input_messages: [" + (input ? message_id : "") + "],";
    request += "  removed_output_messages: [" + (input ? "" : message_id) + "]";
    request += "}";

    new Ajax.Request("callbacks/update-test", {
        method: "post",
        postBody: request,

        onSuccess: function(response) {
            if (message_dialogs[message_id]) {
                message_dialogs[message_id].hide();
            }

            if (input) {
                tests_by_id[test_id].input_messages[message_id] = null;
            } else {
                tests_by_id[test_id].output_messages[message_id] = null;
            }

            message_dialogs[message_id] = null;

            test_dialogs[test_id].ws_psheet.reset();
        },

        onFailure: function(response) {
        // Does nothing
        },

        onComplete: function(response) {
        // Does nothing
        }
    });
}

function change_function_test_result_dialog(test_id, result_id, message_id) {
    if (!message_dialogs[message_id]) {
        message_dialogs[message_id] = new Element("div");
        message_dialogs[message_id].addClassName("-ws-psheet-window");

        message_dialogs[message_id].innerHTML = "" +
        "<div class=\"-wsfw-header\">Message</div>" +
        "<div class=\"-wsfw-contents\"></div>" +
        "<div class=\"-wsfw-footer\">" +
        "<input class=\"close\" type=\"button\" value=\"Close\"/>" +
        "</div>";

        document.body.appendChild(message_dialogs[message_id]);

        new Ws.WindowedPropertiesSheet(
            message_dialogs[message_id],
            create_result_message_container(test_id, result_id, message_id));

        message_dialogs[message_id].select("input.close")[0].onclick = function() {
            message_dialogs[message_id].hide();
        };
    } else {
        message_dialogs[message_id].show();
    }
}

var tests_by_id = null;

var tests_list_container = {
    descriptor: {
        properties: [
        {
            name: "tests_list",
            type: Ws.PropertiesSheet.TABLE,

            display_name: "Tests",
            description: "Tests for this message flow.",
            value: {
                body_value: null
            }
        }
        ]
    },

    pc_get_descriptors: function() {
        this.descriptor.properties[0].value.body_value = [];

        var tests = get_array_by_field(tests_by_id, "offset");

        if (tests.length == 0) {
            this.descriptor.properties[0].value.body_value.push([
            {
                value: "<i>No tests are currently available.</i>",
                functions_change: false
            }
            ]);
        }

        for (var i = 0; i < tests.length; i++) {
            this.descriptor.properties[0].value.body_value.push([
            {
                value: "<img src=\"resource?img/ui/tester/test_16.png\"/> " + tests[i].display_name,
                functions_change: true,
                functions_change_params: "" + tests[i].id
            }
            ]);
        }

        return this.descriptor;
    },

    pc_set_properties: function(properties) {
    // Does nothing
    }
}

function create_test_container(id) {
    return {
        pc_get_descriptors: function() {
            var descriptor = {
                properties: [
                {
                    name: "display_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Name",
                    description: "Human-readable name of this test.",
                    value: tests_by_id[id].display_name
                },

                {
                    name: "description",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Description",
                    description: "Description of this test.",
                    value: tests_by_id[id].description
                },

                {
                    name: "timeout",
                    type: Ws.PropertiesSheet.INTEGER,

                    display_name: "Timeout",
                    description: "Global time limit within which all output messages should be received (in milliseconds.)",
                    value: tests_by_id[id].timeout
                },

                {
                    name: "output_ordered",
                    type: Ws.PropertiesSheet.BOOLEAN,

                    display_name: "Output Ordered",
                    description: "Whether the order of output messages matters.",
                    value: tests_by_id[id].output_ordered
                },

                {
                    name: "messages",
                    type: Ws.PropertiesSheet.TABLE,

                    display_name: "Messages",
                    description: "Messages used to test the message flow.",
                    value: {
                        body_caption: [
                        {
                            value: "&nbsp;Input"
                        },
                        {
                            value: "&nbsp;Output"
                        }
                        ],

                        body_value: null
                    }
                },
                {
                    name: "results",
                    type: Ws.PropertiesSheet.TABLE,

                    display_name: "Results",
                    description: "Results of previous runs of this test.",
                    value: {
                        body_value: null
                    }
                }
                ]
            };

            var input_messages = get_array_by_field(tests_by_id[id].input_messages, "offset");
            var output_messages = get_array_by_field(tests_by_id[id].output_messages, "offset");

            var length = input_messages.length > output_messages.length ?
            input_messages.length : output_messages.length;

            var messages_value =
            descriptor.properties[descriptor.properties.length - 2].value;

            messages_value.body_value = [];

            if (length == 0) {
                messages_value.body_value.push([
                {
                    value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                    functions_change: false
                },
                {
                    value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No output messages.</i>",
                    functions_change: false
                }
                ]);
            }

            for (var i = 0; i < length; i++) {
                var row = [];

                if (i < input_messages.length) {
                    row.push({
                        value: "<img src=\"resource?img/ui/tester/message_16.png\"/> " + get_test_message_display_name(input_messages[i]),
                        functions_change: true,
                        functions_change_params: "" + id + "," + input_messages[i].id
                    });
                } else if ((i == 0) && (input_messages.length == 0)) {
                    row.push({
                        value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                        functions_change: false
                    });
                } else {
                    row.push({
                        value: "",
                        functions_change: false
                    });
                }

                if (i < output_messages.length) {
                    row.push({
                        value: "<img src=\"resource?img/ui/tester/message_16.png\"/> " + get_test_message_display_name(output_messages[i]),
                        functions_change: true,
                        functions_change_params: "" + id + "," + output_messages[i].id
                    });
                } else if ((i == 0) && (output_messages.length == 0)) {
                    row.push({
                        value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No output messages.</i>",
                        functions_change: false
                    });
                } else {
                    row.push({
                        value: "",
                        functions_change: false
                    });
                }

                messages_value.body_value.push(row);
            }

            var results = get_array_by_field(tests_by_id[id].results, "start_time");

            var results_value =
            descriptor.properties[descriptor.properties.length - 1].value;
            
            results_value.body_value = [];
            for (i = 0; i < results.length; i++) {
                row = [];

                row.push({
                    value: new Date(results[i].start_time).toLocaleString(),
                    functions_change: true,
                    functions_change_params: "" + id + ", null, " + results[i].id
                });

                results_value.body_value.push(row);
            }

            return descriptor;
        },

        pc_set_properties: function(properties) {
        // Does nothing
        }
    }
}

function create_test_message_container(test_id, message_id) {
    var i;
    var test = tests_by_id[test_id];
    var message = null;
    var inbound = false;

    for (i in test.input_messages) {
        if (test.input_messages[i].id == message_id) {
            message = test.input_messages[i];
            inbound = true;
            break;
        }
    }

    if (message == null) {
        for (i in test.output_messages) {
            if (test.output_messages[i].id == message_id) {
                message = test.output_messages[i];
                break;
            }
        }
    }

    return {
        pc_get_descriptors: function() {
            var nodes = [];
            var pairs = $('diagram').ws_graph.get_nodes_id_dn_pairs();

            for (var i = 0; i < pairs.length; i++) {
                nodes.push({
                    value: pairs[i].id,
                    display_name: pairs[i].display_name
                });
            }

            nodes.sort(function(a, b) {
                return a.display_name > b.display_name;
            });

            return {
                properties: [
                {
                    name: "service",
                    type: "enum",

                    display_name: "Service",
                    description: "Service that this message is coming from or going to.",
                    value: message.service,

                    values: nodes
                },

                {
                    name: "inbound",
                    type: "boolean",

                    display_name: "Inbound",
                    description: "Whether the message is going into a service or out of it.",
                    value: message.inbound
                },

                {
                    name: "time",
                    type: "integer",

                    display_name: (inbound ? "Delay" : "Timeout"),
                    description: (inbound ? "Amount of time to wait before sending the message the service. '0' means 'send immediately.'" : "Time limit within which this particular message should arrive. '0' means no limit."),
                    value: message.time
                },

                {
                    name: "content",
                    type: "code",

                    display_name: "Content",
                    description: "Message's content.",
                    value: message.content,
                        
                    maximizable: true
                }
                ]
            }
        },

        pc_set_properties: function(properties) {
        // Does nothing
        }
    }
}

function create_result_container(test_id, result_id) {
    return {
        pc_get_descriptors: function() {
            var descriptor = {
                properties: [
                {
                    name: "start_time",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Started On",
                    description: "Date and time of this test run.",
                    value: tests_by_id[test_id].results[result_id].start_time,

                    read_only: true
                },

                {
                    name: "passed",
                    type: Ws.PropertiesSheet.BOOLEAN,

                    display_name: "Passed",
                    description: "Whether this test run was successful or not.",
                    value: tests_by_id[test_id].results[result_id].passed,

                    read_only: true
                },

                {
                    name: "duration",
                    type: Ws.PropertiesSheet.INTEGER,

                    display_name: "Duration",
                    description: "How long this test tun took (in milliseconds.)",
                    value: tests_by_id[test_id].results[result_id].duration,

                    read_only: true
                },

                {
                    name: "messages",
                    type: Ws.PropertiesSheet.TABLE,

                    display_name: "Messages",
                    description: "Messages which were recorded during this test run.",
                    value: {
                        body_value: null
                    }
                }
                ]
            }

            var messages = get_array_by_field(
                tests_by_id[test_id].results[result_id].messages, "offset");

            var messages_value =
            descriptor.properties[descriptor.properties.length - 1].value;

            messages_value.body_value = [];

            if (messages.length == 0) {
                messages_value.body_value.push({
                    value: "<img src=\"resource?img/ui/tester/empty_16.png\"/> <i>No messages were recorded.</i>",
                    functions_change: false
                });
            }

            for (var i = 0; i < messages.length; i++) {
                var row = [];

                var service = $("diagram").ws_graph.get_node_by_id(messages[i].service);
                if (service == null) {
                    service = "<i>Undefined</i>";
                } else {
                    service = service.ui_properties.display_name;
                }

                row.push({
                    value: "<img src=\"resource?img/ui/tester/message_16.png\"/> " + service,
                    functions_change: true,
                    functions_change_params: "" + test_id + ", " + result_id + ", " + messages[i].id
                });

                messages_value.body_value.push(row);
            }


            return descriptor;
        },

        pc_set_properties: function(properties) {
        // Does nothing
        }
    }
}

function create_result_message_container(test_id, result_id, message_id) {
    return {
        pc_get_descriptors: function() {
            var message = tests_by_id[test_id].results[result_id].messages[message_id];

            var nodes = [];
            var pairs = $('diagram').ws_graph.get_nodes_id_dn_pairs();

            for (var i = 0; i < pairs.length; i++) {
                nodes.push({
                    value: pairs[i].id,
                    display_name: pairs[i].display_name
                });
            }

            nodes.sort(function(a, b) {
                return a.display_name > b.display_name;
            });

            return {
                properties: [
                {
                    name: "service",
                    type: "enum",

                    display_name: "Service",
                    description: "Service that this message is coming from or going to.",
                    value: message.service,

                    values: nodes,

                    read_only: true
                },

                {
                    name: "inbound",
                    type: "boolean",

                    display_name: "Inbound",
                    description: "Whether the message is going into a service or out of it.",
                    value: message.inbound,

                    read_only: true
                },

                {
                    name: "time",
                    type: "integer",

                    display_name: "Timestamp",
                    description: "When the message arrived to the current point.",
                    value: message.time,

                    read_only: true
                },

                {
                    name: "content",
                    type: "code",

                    display_name: "Content",
                    description: "Message's content.",
                    value: message.content,

                    read_only: true,
                    maximizable: true
                }
                ]
            }
        },

        pc_set_properties: function(properties) {
        // Does nothing
        }
    }
}

var test_runing_container = {
    test_result : {},

    pc_get_descriptors: function() {
        var properties = [
        {
            name: "testing",
            type: Ws.PropertiesSheet.TEXT,

            display_name: "tests result ",
            description: "runing tests",
            value: "",
            auto_adjust_height: true
        },
        {
            name: "test_out",
            type: Ws.PropertiesSheet.TEXT,

            display_name: "test out",
            description: "test out",
            value: "text_text_test"
        },
        ]

        for (var id in this.test_result) {
            if (this.test_result[id]) {
                properties[0].value += tests_by_id[id].display_name + " - passed\n";
            } else {
                properties[0].value += tests_by_id[id].display_name + " - failed\n";
            }
        }

        return {
            properties : properties
        }
    },

    pc_set_properties: function(properties) {
    // Does nothing
    },

    addTestPassed: function(id) {
        this.test_result[id] = true;
    },

    addTestError: function(id) {
        this.test_result[id] = false;
    }
}

function get_array_by_field(things, field_name) {
    var things_array = [];

    for (var i in things) {
        if (things[i]) {
            things_array.push(things[i]);
        }
    }

    things_array.sort(function(a, b) {
        return (a[field_name] > b[field_name]);
    });

    return things_array;
}

function get_test_message_display_name(message) {
    var display_name = "";

    var input_service = $("diagram").ws_graph.get_node_by_id(message.service);
    if (input_service == null) {
        display_name += "<i>Undefined</i>";
    } else {
        display_name += input_service.ui_properties.display_name;
    }

    if (message.inbound) {
        display_name += ", In";
    } else {
        display_name += ", Out";
    }

    var time = message.time;

    var seconds = Math.floor(time / 1000);
    var tenths = (time % 1000) / 100;

    display_name += ", " + seconds;
    if (tenths != 0) {
        display_name += "." + tenths;
    }
    display_name += "s";

    return display_name;
}