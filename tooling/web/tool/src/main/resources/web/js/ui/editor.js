/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
var SESSION_TIMED_OUT_STATUS =
        "This operation must not be called without a valid session.";
var NOT_LOGGED_IN_STATUS =
        "You must be logged in to invoke this operation.";

var NOBODY = "nobody";

var lock = false;

Event.observe(window, "load", function() {
    // Initialize the global progress bar, which will be used to disable the UI elements
    // when a lengthy non-interruptable action is happening (first-time save, open,
    // create new graph, etc.)
    var global_overlay = new Element("div");
    global_overlay.id = "global-progress-bar";

    var global_overlay_progress_bar = new Element("div");
    global_overlay_progress_bar.addClassName("progress-bar");

    __global_progress_message = new Element("div");
    __global_progress_message.addClassName("message");

    global_overlay.appendChild(global_overlay_progress_bar);
    global_overlay.appendChild(__global_progress_message);

    document.body.appendChild(global_overlay);

    new Ws.ProgressBar(global_overlay_progress_bar, {
        fill: "rgb(255, 255, 255)"
    });
    global_overlay_progress_bar.ws_progress_bar.hide();

    __global_progress = new Ws.ModalWindow(global_overlay, {
        show_callback: function() {
            global_overlay_progress_bar.ws_progress_bar.show();
        },
        close_callback : function() {
            global_overlay_progress_bar.ws_progress_bar.hide();
        }
    });
    global_overlay.ws_mwindow.close();
});

Event.observe(window, "beforeunload", function(event) {
    var message = "An important background operation is currently in progress. " +
            "Are you sure you want to close the window?\n It is better to try again " +
            "in a couple of minutes.";

    if (lock) {
        event.returnValue = message;
        return message;
    }

    return null;
});

// Current graph -------------------------------------------------------------------------
function is_current_graph_untitled() {
    return /^untitled.*/.test(current_graph.code_name);
}

function is_current_graph_read_only() {
    return (current_user.username != current_graph.created_by) ||
            current_graph.loaded_with_errors;
}

// Progress bars -------------------------------------------------------------------------
function show_global_progress(message) {
    if (!message) {
        message = "";
    }

    __global_progress_message.innerHTML = message;
    __global_progress.show();
}

function hide_global_progress(immediately) {
    __global_progress.close(immediately);
}

// Reestablish session -------------------------------------------------------------------
function reestablish_session(callback) {
    __reestablish_session_callback = callback;

    if (current_user.username == NOBODY) {
        if (!__reestablish_session_dialog) {
            __reestablish_session_dialog = create_modal_dialog(
                    "reestablish-session-dialog",
                    "Session Expired",
                    __reestablish_session_pc,
                    [
                        {
                            "display_name": "OK",
                            "action": __reestablish_dialog_action
                        }
                    ]);
        }

        __reestablish_session_dialog.ws_psheet.reset();
        __reestablish_session_dialog.ws_mwindow.show();
    } else {
        if (!__relogin_dialog) {
            __relogin_dialog = create_modal_dialog(
                    "relogin-dialog",
                    "Session Expired",
                    __relogin_pc,
                    [
                        {
                            "display_name": "Login",
                            "action": __relogin_dialog_action
                        },
                        {
                            "display_name": "Cancel",
                            "action": function() {
                                $("relogin-dialog-validation").innerHTML = "";
                                __relogin_dialog.ws_mwindow.close();
                            }
                        }
                    ]);
        }

        __relogin_dialog.ws_psheet.reset();
        __relogin_dialog.ws_mwindow.show();
    }
}

// Modal dialogs -------------------------------------------------------------------------
function create_modal_dialog(id, title, pc, buttons) {
    var i;

    var new_dialog = new Element("div");
    new_dialog.addClassName("modal-dialog");
    new_dialog.id = id;

    var html = "" +
        "<div id=\"" + id + "-header\" class=\"header\">" + title + "</div>" +
        "<div id=\"" + id + "-contents\" class=\"contents\"></div>" +
        "<div id=\"" + id + "-footer\" class=\"footer\">" +
            "<div id=\"" + id + "-validation\" class=\"validation\"></div>";

    for (i = 0; i < buttons.length; i++) {
        html +=
            "<input id=\"" + id + "-button-" + i + "\" " +
                   "type=\"button\" " +
                   "value=\"" + buttons[i].display_name + "\"/>"
    }

    html += "</div>";

    new_dialog.innerHTML = html;

    document.body.appendChild(new_dialog);

    new Ws.ModalWindow(new_dialog);
    new Ws.RoundedRectangle(new_dialog, {
        fill: "rgba(128, 128, 128, 0.7)"
    });
    new Ws.PropertiesSheet(pc, $(id + "-contents"), {});

    new_dialog.ws_psheet = $(id + "-contents").ws_psheet;

    for (i = 0; i < buttons.length; i++) {
        $(id + "-button-" + i).onclick = buttons[i].action;
    }

    new_dialog.ws_mwindow.close(true);

    return new_dialog;
}

// Private -------------------------------------------------------------------------------
var __global_progress = null;
var __global_progress_message = null;
var __reestablish_session_dialog = null;
var __relogin_dialog = null;
var __reestablish_session_callback = null;

function __reestablish_dialog_action() {
    __reestablish_session_dialog.ws_mwindow.close();

    new Ajax.Request("callbacks/reestablish-session", {
        method: "get",

        parameters: {
        },

        onSuccess: function(transport) {
            if (__reestablish_session_callback) {
                __reestablish_session_callback();
                __reestablish_session_callback = null;
            }
        },

        onFailure: function(transport) {
            Ws.Growl.notify_error(
                    "Failed to reestablish the session. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            // Does nothing.
        }
    });
}

function __relogin_dialog_action() {
    var properties = __relogin_dialog.ws_psheet.get_properties();
    var validation = $("relogin-dialog-validation");

    var username = properties.username;
    var password = properties.password;

    var trimmed_un = username.replace(/^\s+/, "").replace(/\s+$/, "");
    if (trimmed_un == "") {
        validation.innerHTML = "Username cannot be empty. An empty string is not a " +
                "good indentifier, is it?";
        return;
    }

    if (password.length < 5) {
        validation.innerHTML = "Password cannot be shorter than 5 characters. We " +
                "did not allow this when you registered and still don't.";
        return;
    }

    validation.innerHTML = "";
    show_global_progress("Creating...");
    __relogin_dialog.ws_mwindow.close();

    new Ajax.Request("callbacks/reestablish-session", {
        method: "get",

        parameters: {
            "username": trimmed_un,
            "password": password
        },

        onSuccess: function(transport) {
            if (__reestablish_session_callback) {
                __reestablish_session_callback();
                __reestablish_session_callback = null;
            }
        },

        onFailure: function(transport) {
            Ws.Growl.notify_error(
                    "Failed to relogin. The server responded: <br/>" +
                    "<i>" + transport.statusText + "</i>",
                    -1,
                    true);
        },

        onComplete: function(transport) {
            hide_global_progress();
        }
    });
}


var __reestablish_session_pc = {
    pc_get_descriptors: function() {
        return {
            properties: [
                {
                    name: "label",
                    type: Ws.PropertiesSheet.LABEL,

                    display_name:
                            "The current operation failed because the HTTP " +
                            "session had timed out. Click OK to reestablish " +
                            "the session.<br/>Sorry to say that, but as you were" +
                            "not logged in, the graph you were editing was stored " +
                            "solely within the session, so it was lost.",
                    description: ""
                }
            ]
        }
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __relogin_pc = {
    pc_get_descriptors: function() {
        var descriptors = __login_pc.pc_get_descriptors();

        descriptors.properties.unshift({
            name: "label",
            type: Ws.PropertiesSheet.LABEL,

            display_name:
                    "The current operation failed because the HTTP " +
                    "session had timed out. Please enter your login " +
                    "credentials to reestablish the session.",
            description: ""
        });

        return descriptors;
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

var __login_pc =  {
    pc_get_descriptors: function() {
        var username = current_user.username == NOBODY ? "" : current_user.username;

        return {
            properties: [
                {
                    name: "username",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Username",
                    description: "Username to use when logging into the system.",
                    value: username
                },
                {
                    name: "password",
                    type: Ws.PropertiesSheet.PASSWORD,

                    display_name: "Password",
                    description: "Password to use when logging into the system.",
                    value: ""
                }
            ]
        }
    },

    pc_set_properties: function(properties) {
        // Does nothing.
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
ComponentTypesLoadManager = {
    STATE_LOADING: "loading",
    STATE_LOADED: "loaded",
    STATE_FAILED: "failed",
    
    LOAD_ICON: 1,
    LOAD_SCRIPT: 2,
    LOAD_DRAG_SHADOW: 3,
    
    _types: [],
    
    load: function(code_name, on_success, on_failure) {
        var ctl = ComponentTypesLoadManager;
        
        for (var i = 0; i < ctl._types.length; i++) {
            var item = ctl._types[i];
            
            if (item.code_name == code_name) {
                if ((item.state == ctl.STATE_LOADED) && on_success) {
                    on_success();
                } else 
                
                if ((item.state == ctl.STATE_FAILED) && on_failure) {
                    on_failure();
                } else 
                
                if (item.state == ctl.STATE_LOADING) {
                    if (on_success) {
                        if (item.on_success) {
                            var old_on_success = item.on_success;
                            item.on_success = function() {
                                old_on_success();
                                on_success();
                            }
                        } else {
                            item.on_success = on_success;
                        }
                    }
                    
                    if (on_failure) {
                        if (item.on_failure) {
                            var old_on_failure = item.on_failure;
                            item.on_failure = function() {
                                old_on_failure();
                                on_failure();
                            }
                        } else {
                            item.on_failure = on_failure;
                        }
                    }
                }
                
                return;
            }
        }
        
        var type = {
            code_name: code_name,
            on_success: on_success,
            on_failure: on_failure,
            state: ctl.STATE_LOADING
        };
        
        ctl._types.push(type);
        
        new Ajax.Request("callbacks/get-component-type", {
            method: "get",
            
            parameters: {
                code_name: code_name
            },
            
            onSuccess: function(transport) {
                ctl._load(transport.responseJSON);
            },
            
            onFailure: function(transport) {
                Ws.Growl.notify_error(
                        "Failed to load data for component type " + 
                        "codenamed '" + code_name + "'. <br/> The server " + 
                        "responded: <br/><i>" + transport.statusText + "</i>", -1, true);
                
                type.state = ctl.STATE_FAILED;
                type.on_failure();
            },
            
            onComplete: function(transport) {
                // Does nothing
            }
        });
    },
    
    get_state: function(code_name) {
        var ctl = ComponentTypesLoadManager;
        
        for (var i = 0; i < ctl._types.length; i++) {
            var type = ctl._types[i];
            
            if (type.code_name == code_name) {
                return type.state;
            }
        }
        
        return null;
    },
    
    get_type: function(code_name) {
        var ctl = ComponentTypesLoadManager;
        
        for (var i = 0; i < ctl._types.length; i++) {
            var type = ctl._types[i];
            
            if (type.code_name == code_name) {
                return type;
            }
        }
        
        return null;
    },
    
    _load: function(response_json) {
        var i;
        var ctl = ComponentTypesLoadManager;
        
        // Update the existing type entry with the received data
        var type = null;
        for (i = 0; i < ctl._types.length; i++) {
            if (ctl._types[i].code_name == response_json.code_name) {
                type = ctl._types[i];
                
                Object.extend(type, response_json);
                break;
            }
        }
        
        if (type != null) {
            var prefix = "resource?";

            type.icons = type.icons.compact();
            type.icons_to_load = type.icons.length;

            // Schedule loading of the type icons
            for (i = 0; i < type.icons.length; i++) {
                type.icons[i] = prefix + type.icons[i];

                Ws.ImagesLoadManager.load(type.icons[i], function() {
                    ctl._something_loaded(type, ctl.LOAD_ICON, true);
                }, function() {
                    ctl._something_loaded(type, ctl.LOAD_ICON, false);
                });
            }
            
            // Schedule loading of the type javascript file
            type.script = prefix + type.script;
            Ws.ScriptsLoadManager.load(type.script, function() {
                ctl._something_loaded(type, ctl.LOAD_SCRIPT, true);
            }, function() {
                ctl._something_loaded(type, ctl.LOAD_SCRIPT, false);
            });
            
            // Schedule loading of the type drag shadow
            type.drag_shadow = prefix + type.drag_shadow;
            Ws.ImagesLoadManager.load(type.drag_shadow, function() {
                ctl._something_loaded(type, ctl.LOAD_DRAG_SHADOW, true);
            }, function() {
                ctl._something_loaded(type, ctl.LOAD_DRAG_SHADOW, false);
            });
        }
    },
    
    _something_loaded: function(type, what_loaded, loaded_successfully) {
        var ctl = ComponentTypesLoadManager;
                
        if (type != null) {
            switch (what_loaded) {
                case ctl.LOAD_ICON:
                    type.icons_to_load--;
                    
                    if (!loaded_successfully) {
                        Ws.Growl.notify_warning(
                                "Failed to load icon image for " + 
                                type.code_name + " component type", 5000, true);
                    }
                    break;
                case ctl.LOAD_SCRIPT:
                    type.script_loaded = true;
                    
                    if (!loaded_successfully) {
                        type.script_failed = true;
                        
                        Ws.Growl.notify_error(
                                "Failed to load javascript for " + 
                                type.code_name + " component type", -1, true);
                    }
                    break;
                case ctl.LOAD_DRAG_SHADOW:
                    type.drag_shadow_loaded = true;
                    
                    if (!loaded_successfully) {
                        Ws.Growl.notify_warning(
                                "Failed to load drag shadow for " + 
                                type.code_name + " component type", 5000, true);
                    }
                    break;
            }
            
            if ((type.icons_to_load == 0) &&
                    type.script_loaded && 
                    type.drag_shadow_loaded) {
                
                if (!type.script_failed) {
                    type.state = ctl.STATE_LOADED;
                    type.on_success();
                } else {
                    type.state = ctl.STATE_FAILED;
                    type.on_failure();
                }
            }
        }
    },
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing
    }
}