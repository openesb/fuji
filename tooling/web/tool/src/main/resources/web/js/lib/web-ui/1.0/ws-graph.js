/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Graph = Class.create({
    nodes: null,
    links: null,
    
    last_id: 10,
    
    listeners: null,
    
    code_name: "",
    display_name: "",
    description: "",

    misconnected_links_policy: null,

    ui: null,

    initialize: function(element) {
        // It is important to initialize the Object-typed properties in the
        // constructor, as otherwise we'll end up with the same objects for all
        // instances of Ws.Graph.
        this.nodes = [];
        this.links = [];
        this.listeners = [];

        this.misconnected_links_policy = Ws.Graph.MISCONNECTED_LINKS_DELETE;

        this.ui = new Ws.Graph.GraphUi(this, element);
        
        element.ws_graph = this;
    },

    // Add/remove nodes and links --------------------------------------------------------
    add_node: function(node, silent) {
        node.graph = this;
        
        if (node.id == 0) {
            node.id = this.last_id;
            this.last_id += 10;
        } else if (!this.__is_id_used(node.id)) {
            if (node.id >= this.last_id) {
                this.last_id = node.id + 10;
            }
        } else {
            node.graph = null;
            
            throw "ID " + node.id + " is already used";
        }
        
        if (node.pre_add_hook) node.pre_add_hook();
        
        this.nodes.push(node);
        this.ui.add_node_ui(node.ui);
        
        if (node.post_add_hook) node.post_add_hook();

        if (!silent) {
            // Build the list of updated properties and ui_properties.
            var updated_properties = [], updated_ui_properties = [], i;

            for (i in node.properties) {
                if (!node.shortcut_to || (node.propagation_exceptions.indexOf(i) != -1)) {
                    updated_properties.push({
                        name: i,
                        old_value: undefined,
                        new_value: node.properties[i]
                    });
                }
            }

            for (i in node.ui_properties) {
                if (!node.shortcut_to || (node.propagation_ui_exceptions.indexOf(i) != -1)) {
                    updated_ui_properties.push({
                        name: i,
                        old_value: undefined,
                        new_value: node.ui_properties[i]
                    });
                }
            }

            // Notify the listeners.
            this.notify_listeners({
                type: Ws.Graph.EVENT_NODE_ADDED,
                data: {
                    node: node,
                    updated_properties: updated_properties,
                    updated_ui_properties: updated_ui_properties
                }
            });
        }
    },
    
    create_and_add_node: function(type, data, silent) {
        var node = Ws.Graph.NodeFactory.new_node(type, data);
        
        this.add_node(node, silent);
        return node;
    },
    
    remove_node: function(node, silent) {
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i] == node) {
                // First delete all shortcuts to this node.
                if (!node.shortcut_to) {
                    var shortcuts = this.get_shortcuts_to(node);
                    for (var k = 0; k < shortcuts.length; k++) {
                        this.remove_node(shortcuts[k], silent);
                    }
                }

                // The remove the node itself.
                if (node.pre_remove_hook) node.pre_remove_hook();
                
                for (var j = 0; j < this.links.length; j++) {
                    if ((this.links[j].start_node == node) || 
                        (this.links[j].end_node == node)) {
                                
                        this.remove_link(this.links[j], silent);
                        j--;
                    }
                }
                
                this.nodes.splice(i, 1);
                this.ui.remove_node_ui(node.ui);
                
                if (node.post_remove_hook) node.post_remove_hook();
                
                break;
            }
        }
        
        if (!silent) {
            this.notify_listeners({
                type: Ws.Graph.EVENT_NODE_REMOVED,
                data: {
                    node: node
                }
            });
        }
    },
    
    add_link: function(link, silent) {
        // Generate the link id if it was not already set (if it was set, use the
        // provided one.)
        if (!link.id) {
            link.id = this.last_id;
            this.last_id += 10;
        }

        // Save the graph reference in the link (so that it can be used when verifying
        // that the link is OK to be added.
        link.graph = this;

        // Check whether the link is OK to be added.
        this.__check_link_before_adding(link);

        // Notify the link that it is _about_ to be added.
        if (link.pre_add_hook) {
            link.pre_add_hook();
        }

        // Add the link to the graph.
        this.links.push(link);
        this.ui.add_link_ui(link.ui);
        
        // Notify the link that it is has been added.
        if (link.post_add_hook) {
            link.post_add_hook();
        }

        // Unless we've been told to be silent, notify the graph listeners of the fact
        // that a link has been added.
        if (!silent) {
            this.notify_listeners({
                type: Ws.Graph.EVENT_LINK_ADDED,
                data: {
                    link: link
                }
            });
        }
    },
    
    create_and_add_link: function(type, data, silent) {
        var link = Ws.Graph.LinkFactory.new_link(type, data);
        
        this.add_link(link, silent);
        return link;
    },
    
    remove_link: function(link, silent) {
        for (var i = 0; i < this.links.length; i++) {
            if (this.links[i] == link) {
                if (link.pre_remove_hook) link.pre_remove_hook();
                
                this.links.splice(i, 1);
                this.ui.remove_link_ui(link.ui);
                
                if (link.post_remove_hook) link.post_remove_hook();
                break;
            }
        }
        
        if (!silent) {
            this.notify_listeners({
                type: Ws.Graph.EVENT_LINK_REMOVED,
                data: {
                    link: link
                }
            });
        }
    },

    /**
     * Removes all nodes and links from the graph.
     *
     * @param silent defines whether to generate or skip generating events for removal of
     *      the graph elements.
     */
    clear: function(silent) {
        while (this.nodes.length > 0) {
            this.remove_node(this.nodes[0], silent);
        }

        // Just in case, remove all remaining links as well. If all were connected, they
        // would already be removed by now.
        while (this.links.length > 0) {
            this.remove_link(this.links[0], silent);
        }

        // The last thing to do is to reset the internal id counter.
        this.last_id = 10;
    },

    // Node queries ----------------------------------------------------------------------
    get_node_by_id: function(id) {
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].id == id) {
                return this.nodes[i];
            }
        }
        
        return null;
    },
    
    get_nodes_by_type: function(type) {
        var filtered = [];
        
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].type == type) {
                filtered.push(this.nodes[i]);
            }
        }
        
        return filtered;
    },

    get_shortcuts_to: function(node) {
        var shortcuts = [];

        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].shortcut_to == node) {
                shortcuts.push(this.nodes[i]);
            }
        }

        return shortcuts;
    },

    is_shortcut_to: function(node, potential_target) {
        if (node.shortcut_to) {
            if (node.shortcut_to == potential_target) {
                return true;
            } else {
                return this.is_shortcut_to(node.shortcut_to, potential_target);
            }
        }

        return false;
    },

    get_children: function(node) {
        var children = [];

        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].parent == node) {
                children.push(this.nodes[i]);
            }
        }

        return children;
    },

    get_nodes_by_property_value: function(name, value) {
        var nodes = [];

        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].properties[name] == value) {
                nodes.push(this.nodes[i]);
            }
        }

        return nodes;
    },

    get_node_by_property_value: function(name, value) {
        var nodes = this.get_nodes_by_property_value(name, value);

        if (nodes.length > 0) {
            return nodes[0];
        }

        return null;
    },

    get_node_by_code_name: function(code_name) {
        return this.get_node_by_property_value("code_name", code_name);
    },

    get_nodes_by_ui_property_value: function(name, value) {
        var nodes = [];

        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].ui_properties[name] == value) {
                nodes.push(this.nodes[i]);
            }
        }

        return nodes;
    },

    get_node_by_ui_property_value: function(name, value) {
        var nodes = this.get_nodes_by_ui_property_value(name, value);

        if (nodes.length > 0) {
            return nodes[0];
        }

        return null;
    },

    get_node_by_display_name: function(display_name) {
        return this.get_node_by_ui_property_value("display_name", display_name);
    },

    // Link queries ----------------------------------------------------------------------
    get_link_by_id: function(id) {
        for (var i = 0; i < this.links.length; i++) {
            if (this.links[i].id == id) {
                return this.links[i];
            }
        }

        return null;
    },

    get_links_by_node: function(node) {
        return this.get_inbound_links_by_node(node).concat(
            this.get_outbound_links_by_node(node));
    },
    
    get_outbound_links_by_node: function(node) {
        var filtered = [];

        for (var i = 0; i < this.links.length; i++) {
            var link = this.links[i];

            if (link.start_node == node) {
                filtered.push(link);
            }
        }

        return filtered;
    },
    
    get_inbound_links_by_node: function(node) {
        var filtered = [];

        for (var i = 0; i < this.links.length; i++) {
            var link = this.links[i];

            if (link.end_node == node) {
                filtered.push(link);
            }
        }

        return filtered;
    },
    
    // Listeners -------------------------------------------------------------------------
    add_listener: function(listener) {
        this.listeners.push({
            listener: listener
        });
    },
    
    remove_listener: function(listener) {
        for (var i = 0; i < this.listeners.length; i++) {
            if (listener == this.listeners[i].listener) {
                this.listeners.splice(i, 1);
                return;
            }
        }
    },
    
    notify_listeners: function(event) {
        // Check if we need to correct something according to policies. We do not do this
        // for node-added or node-removed tpe of events, as we expect that to be handled
        // in the corresponding methods.
        if (event.type == Ws.Graph.EVENT_NODE_UPDATED) {
            this.check_misconnected_links();
        }

        // Actually run through the list of listeners, notifying them of what has
        // happened.
        this.listeners.each(function(item) {
            item.listener(event);
        });
    },

    // Policies --------------------------------------------------------------------------
    check_misconnected_links: function() {
        // First we find misconnected links, then act according to the setting.
        for (var i = 0; i < this.links.length; i++) {
            var link = this.links[i];

            var start_misconnected = false;
            var end_misconnected = false;

            // For each link we check that the node it is connected to really exists and 
            // has the connector the link declares.

            if (link.start_node) {
                if (this.nodes.indexOf(link.start_node) == -1) {
                    start_misconnected = true;
                } else {
                    if (link.start_node.get_outbound_connectors().length <=
                            link.start_node_connector) {

                        start_misconnected = true;
                    }
                }
            }

            if (link.end_node) {
                if (this.nodes.indexOf(link.end_node) == -1) {
                    end_misconnected = true;
                } else {
                    if (link.end_node.get_inbound_connectors().length <=
                            link.end_node_connector) {

                        end_misconnected = true;
                    }
                }
            }

            if (this.misconnected_links_policy == Ws.Graph.MISCONNECTED_LINKS_DELETE) {
                if (start_misconnected || end_misconnected) {
                    this.remove_link(link);
                    i--;
                }
            } else {
                if (start_misconnected) {
                    link.start_node = null;
                    link.start_node_connector = 0;
                }

                if (end_misconnected) {
                    link.end_node = null;
                    link.end_node_connector = 0;
                }

                this.notify_listeners({
                    type: Ws.Graph.EVENT_LINK_UPDATED,
                    data: {
                        link: link
                    }
                });
            }
        }
    },

    // Private ---------------------------------------------------------------------------
    __is_id_used: function(id) {
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].id == id) {
                return true;
            }
        }
        
        for (var j = 0; j < this.links.length; j++) {
            if (this.links[j].id == id) {
                return true;
            }
        }
        
        return false;
    },

    __check_link_before_adding: function(link) {
        var connector, links_count;

        // Check the generated/provided link id for validity.
        if (this.__is_id_used(link.id)) {
            throw "Link identifier '" + link.id + "' is already used.";
        } else {
            // Make sure that the generated ids will not overlap the provided one.
            if (link.id >= this.last_id) {
                this.last_id = link.id + 10;
            }
        }

        // Check whether the start node that we're attaching to is ok with this.
        if (link.start_node) {
            if (link.start_node.graph != this) {
                throw "The link's start node does not belong to the graph.";
            }

            connector = link.start_node.get_outbound_connector(
                    link.start_node_connector);

            if (!connector) {
                throw "The start node connector specified in the link does not exist.";
            }

            if (connector.supported_link_types) {
                if (connector.supported_link_types.indexOf(link.type) == -1) {
                    throw "The start node connector does not support this link type.";
                }
            }

            if (connector.max_links && (connector.max_links > 0)) {
                links_count = link.start_node.get_outbound_links(
                        link.start_node_connector).length;

                if (links_count >= connector.max_links) {
                    throw "The maximum number of links for the start node " +
                            "connector is exceeded."
                }
            }
        }

        // Check whether the end node that we're attaching to is ok with this.
        if (link.end_node) {
            if (link.end_node.graph != this) {
                throw "The link's end node does not belong to the graph.";
            }

            connector = link.end_node.get_inbound_connector(link.end_node_connector);

            if (!connector) {
                throw "The end node connector specified in the link does not exist.";
            }

            if (connector.supported_link_types) {
                if (connector.supported_link_types.indexOf(link.type) == -1) {
                    throw "The end node connector does not support this link type.";
                }
            }

            if (connector.max_links && (connector.max_links > 0)) {
                links_count = link.end_node.get_inbound_links(
                        link.end_node_connector).length;

                if (links_count >= connector.max_links) {
                    throw "The maximum number of links for the end node " +
                            "connector is exceeded."
                }
            }
        }
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});

// ---------------------------------------------------------------------------------------
Ws.Graph.GraphUi = Class.create({

    graph: null,
    element: null,

    node_uis: null,
    link_uis: null,

    last_node_z_index: 1000, // one thousand
    last_link_z_index: 10000,  // ten thousands

    __dragged: null,

    __to_handle_drop: null,

    __to_select: null,
    __selected: null,

    __to_click: null,
    __to_doubleclick: null,

    initialize: function(graph, element) {
        var self = this;

        this.graph = graph;
        this.element = element;
        
        this.node_uis = [];
        this.link_uis = [];
        
        // Initialize the required methods on the element, so that it can be registered
        // in the various mouse managers.
        this.element.ds_accept_drag = function(x, y, event) {
            return self.ds_accept_drag(x, y, event);
        }
        this.element.ds_drag_started = function(x, y, event) {
            self.ds_drag_started(x, y, event);
        }
        this.element.ds_dragged_to = function(x, y, event) {
            self.ds_dragged_to(x, y, event);
        }
        this.element.ds_drag_finished = function(x, y, event) {
            self.ds_drag_finished(x, y, event);
        }

        this.element.dt_accept_drop = function(drag_source, x, y, event) {
            return self.dt_accept_drop(drag_source, x, y, event);
        }
        this.element.dt_handle_drop = function(drag_source, x, y, event) {
            self.dt_handle_drop(drag_source, x, y, event);
        }
        this.element.dt_drag_started = function(drag_source, x, y, event) {
            self.dt_drag_started(drag_source, x, y, event);
        }
        this.element.dt_dragged_to = function(drag_source, x, y, event) {
            self.dt_dragged_to(drag_source, x, y, event);
        }
        this.element.dt_drag_finished = function(drag_source, x, y, event) {
            self.dt_drag_finished(drag_source, x, y, event);
        }

        this.element.sl_accept_selection = function(x, y, event) {
            return self.sl_accept_selection(x, y, event);
        }
        this.element.sl_select = function(x, y, event) {
            self.sl_select(x, y, event);
        }
        this.element.sl_deselect = function(x, y, event) {
            self.sl_deselect(x, y, event);
        }

        this.element.cl_accept_click = function(x, y, event) {
            return self.cl_accept_click(x, y, event);
        };
        this.element.cl_handle_click = function(x, y, event) {
            self.cl_handle_click(x, y, event);
        };
        this.element.cl_accept_doubleclick = function(x, y, event) {
            return self.cl_accept_doubleclick(x, y, event);
        };
        this.element.cl_handle_doubleclick = function(x, y, event) {
            self.cl_handle_doubleclick(x, y, event);
        };

        // Define a back-reference from the element to the graph ui.
        this.element.__graph_ui = this;

        Ws.DragManager.add_drag_source(this.element);
        Ws.DragManager.add_drop_target(this.element);
        Ws.SelectionManager.add_selectable(this.element);
        Ws.MouseManager.add_clickable(this.element);

        // Register a key listener, which will handle node and link removal and maybe
        // something else going forward.
        Event.observe(this.element, "keypress", function(event) {
            self.pr_handle_keypress(event);
        })

        //Toolip for nodes ----------------------------------------
        this.tooltip = new Ws.Tooltip({
            html_tooltip: " ",
            accept_point: function(x, y) {
                if (Ws.Graph.NODE_TOOLTIPS_ENABLED) {
                    var node;
                    for (var i = 0; i < self.node_uis.length; i++) {
                        node = self.node_uis[i];
                        var point = node.__get_point_relative_to_node(x, y);
                        if (node.check_point_node_path(point.x, point.y)) {
                            self.tooltip.set_html_tooltip(node.__get_tooltip_text());
                            return true;
                        }
                    }
                }

                return false;
            }

        },
        this.element);
    },

    // Add/remove node and link UIs ------------------------------------------------------
    add_node_ui: function(ui) {
        this.node_uis.push(ui);

        this.last_node_z_index += 10;

        this.__add_ui(ui, this.last_node_z_index);

        // We need to repaint the parent, if the node being added is a child of somebody.
        if (ui.node.parent) {
            ui.node.parent.ui.repaint();
        }
    },
    
    remove_node_ui: function(ui) {
        for (var i = 0; i < this.node_uis.length; i++) {
            if (this.node_uis[i] == ui) {
                this.node_uis.splice(i, 1);
                break;
            }
        }
        
        this.__remove_ui(ui);
    },
    
    add_link_ui: function(ui) {
        this.link_uis.push(ui);
        
        this.last_link_z_index += 10;

        this.__add_ui(ui, this.last_link_z_index);
    },
    
    remove_link_ui: function(ui) {
        for (var i = 0; i < this.link_uis.length; i++) {
            if (this.link_uis[i] == ui) {
                this.link_uis.splice(i, 1);
                break;
            }
        }
        
        this.__remove_ui(ui);
    },

    // Paint -----------------------------------------------------------------------------
    repaint: function() {
        for (var i = 0; i < this.node_uis.length; i++) {
            this.node_uis[i].repaint();
        }
    },

    // Drag source -----------------------------------------------------------------------
    ds_accept_drag: function(x, y, event) {
        var to_drag;

        // First check whether the original target of the event is a node or a link (it
        // is assumed that it is a node or a link of *this* graph, as we would not be
        // getting this event otherwise). If it is, ask it first and if it agrees, we
        // don't need to look any further. Otherwise we should scan the nodes to find out
        // whether there is another candidate.

        // Hack for MSIE. We're using excanvas, so the original target of the event would
        // be the 'shape' tag or something, which is *not* the direct child of <div>
        // wrapping the <canvas>, so we must go up the hierarchy and replace the target
        // with the corresponding <canvas> if any.
        if (event.is_ie) {
            var ieHackTarget = event.srcElement;
            while (ieHackTarget.parentNode && (ieHackTarget.tagName != 'canvas')) {
                ieHackTarget = ieHackTarget.parentNode;
            }

            if (ieHackTarget) {
                event.target = ieHackTarget;
            }
        }
        
        // We take the parent node here, as the original target would be a <canvas> in
        // case of graph elements.
        var target = event.target.parentNode;
        if (target && target.__wsg_ui && target.__wsg_ui.is_drag_source && 
                target.__wsg_ui.ds_accept_drag(x, y, event)) {

            this.__dragged = target.__wsg_ui;

            return true;
        } else {
            var i;

            // First check links (they have higher z indices).
            for (i = 0; i < this.link_uis.length; i++) {
                var link_ui = this.link_uis[i];

                // If link ui is selectable, its z index is bigger than that of the
                // selected ui (or the selected ui has not yet been set) and it is ok
                // to handle the selection.
                if (link_ui.is_drag_source &&
                        (!to_drag || to_drag.z_index < link_ui.z_index) &&
                        link_ui.ds_accept_drag(x, y, event)) {

                    to_drag = link_ui;
                }
            }

            // If we have found no link ready to handle selection, check the nodes.
            if (!to_drag) {
                for (i = 0; i < this.node_uis.length; i++) {
                    var node_ui = this.node_uis[i];

                    if (node_ui.is_drag_source &&
                            (!to_drag || to_drag.z_index < node_ui.z_index) &&
                            node_ui.ds_accept_drag(x, y, event)) {

                        to_drag = node_ui;
                    }
                }
            }
        }

        if (to_drag) {
            this.__dragged = to_drag;
            return true;
        } else {
            return false;
        }
    },

    ds_drag_started: function(x, y, event) {
        // If we have selected something that will be dragged on the previous step, we
        // should notify it.
        if (this.__dragged && this.__dragged.ds_drag_started) {
            this.__dragged.ds_drag_started(x, y, event);
        }
    },

    ds_dragged_to: function(x, y, event) {
        // If we have selected something that will be dragged on the previous step, we
        // should notify it.
        if (this.__dragged && this.__dragged.ds_dragged_to) {
            this.__dragged.ds_dragged_to(x, y, event);
        }
    },

    ds_drag_finished: function(x, y, event) {
        // If we have selected something that will be dragged on the previous step, we
        // should notify it.
        if (this.__dragged) {
            var dragged = this.__dragged;
            this.__dragged = null;

            if (dragged.ds_drag_finished) {
                dragged.ds_drag_finished(x, y, event);
            }
        }
    },

    // Drop target -----------------------------------------------------------------------
    dt_accept_drop: function(drag_source, x, y, event) {
        // First, ask our beloved nodes, whether any of them wants to handle the drop.
        var to_handle_drop;
        for (var i = 0; i < this.node_uis.length; i++) {
            var node_ui = this.node_uis[i];

            if (node_ui.is_drop_target &&
                    (!to_handle_drop || to_handle_drop.z_index < node_ui.z_index) &&
                    node_ui.dt_accept_drop(drag_source, x, y, event)) {

                to_handle_drop = node_ui;
            }
        }

        if (to_handle_drop) {
            this.__to_handle_drop = to_handle_drop;
            return true;
        } else {
            // If a node description was dropped (e.g. from a palette), we will
            // handle the drop ourselves, even if all nodes refused.
            if (drag_source.node_type && drag_source.node_data) {
                return true;
            }
            
            return false;
        }
    },

    dt_handle_drop: function(drag_source, x, y, event) {
        // Otherwise, if we have a node that is ready to handle the drop -- ask it to
        // do so.
        if (this.__to_handle_drop) {
            this.__to_handle_drop.dt_handle_drop(drag_source, x, y, event);
            this.__to_handle_drop = null;
        } else {
            // If we're asked to handle the drop of a node description, we can do it
            // ourselves.
            if (drag_source.node_type && drag_source.node_data) {
                var node = this.graph.create_and_add_node(
                        drag_source.node_type, drag_source.node_data);
                node.ui.move_to(
                        x - drag_source.drag_start_x - node.ui.margin,
                        y - drag_source.drag_start_y - node.ui.margin);

                return;
            }
        }
    },
    
    dt_drag_started: function(drag_source, x, y, event) {
        // Notify the nodes that the drag has started, we do not need to do anything
        // else here.
        for (var i = 0; i < this.node_uis.length; i++) {
            var node_ui = this.node_uis[i];

            if (node_ui.is_drop_target && node_ui.dt_drag_started) {
                node_ui.dt_drag_started(drag_source, x, y, event);
            }
        }
    },
    
    dt_dragged_to: function(drag_source, x, y, event) {
        // Notify the nodes that the drag is in progress, we do not need to do anything
        // else here.
        for (var i = 0; i < this.node_uis.length; i++) {
            var node_ui = this.node_uis[i];

            if (node_ui.is_drop_target && node_ui.dt_dragged_to) {
                node_ui.dt_dragged_to(drag_source, x, y, event);
            }
        }
    },
    
    dt_drag_finished: function(drag_source, x, y, event) {
        // Notify the nodes that the drag finished.
        for (var i = 0; i < this.node_uis.length; i++) {
            var node_ui = this.node_uis[i];

            if (node_ui.is_drop_target && node_ui.dt_drag_finished) {
                node_ui.dt_drag_finished(drag_source, x, y, event);
            }
        }
    },

    // Selectable ------------------------------------------------------------------------
    sl_accept_selection: function(x, y, event) {
        var to_select;

        // First check whether the original target of the event is a node or a link (it
        // is assumed that it is a node or a link of *this* graph, as we would not be
        // getting this event otherwise). If it is, ask it first and if it agrees, we
        // don't need to look any further. Otherwise we should scan the nodes to find out
        // whether there is another candidate.
        
        // Hack for MSIE. We're using excanvas, so the original target of the event would
        // be the 'shape' tag or something, which is *not* the direct child of <div>
        // wrapping the <canvas>, so we must go up the hierarchy and replace the target
        // with the corresponding <canvas> if any.
        if (event.is_ie) {
            var ieHackTarget = event.srcElement;
            while (ieHackTarget.parentNode && (ieHackTarget.tagName != 'canvas')) {
                ieHackTarget = ieHackTarget.parentNode;
            }

            if (ieHackTarget) {
                event.target = ieHackTarget;
            }
        }

        // We take the parent node here, as the original target would be a <canvas> in 
        // case of graph elements.
        var target = event.target.parentNode;
        if (target && target.__wsg_ui && target.__wsg_ui.is_selectable &&
                target.__wsg_ui.sl_accept_selection(x, y, event)) {
            
            this.__to_select = target.__wsg_ui;

            return true;
        } else {
            var i;

            // First check links (they have higher z indices).
            for (i = 0; i < this.link_uis.length; i++) {
                var link_ui = this.link_uis[i];

                // If link ui is selectable, its z index is bigger than that of the
                // selected ui (ot the selected ui has not yet been set) and it is ok
                // to handle the selection.
                if (link_ui.is_selectable &&
                        (!to_select || to_select.z_index < link_ui.z_index) &&
                        link_ui.sl_accept_selection(x, y, event)) {

                    to_select = link_ui;
                }
            }

            // If we have found no link ready to handle selection, check the nodes.
            if (!to_select) {
                for (i = 0; i < this.node_uis.length; i++) {
                    var node_ui = this.node_uis[i];

                    if (node_ui.is_selectable &&
                            (!to_select || to_select.z_index < node_ui.z_index) &&
                            node_ui.sl_accept_selection(x, y, event)) {

                        to_select = node_ui;
                    }
                }
            }
        }

        if (to_select) {
            this.__to_select = to_select;
            return true;
        } else {
            return false;
        }
    },

    sl_select: function(x, y, event) {
        if (this.__to_select && (this.__to_select != this.__selected)) {
            this.__to_select.sl_select(x, y, event);
            this.__to_select.element.focus();

            this.__selected = this.__to_select;
            this.__to_select = null;
        }
    },

    sl_deselect: function(x, y, event, new_selection) {
        if (this.__selected && (this.__selected != new_selection)) {
            this.__selected.sl_deselect(x, y, event, new_selection);
            this.__selected.element.blur();

            this.__selected = null;
        }
    },

    select_node: function(node_ui) {
        var event = document.createEvent("MouseEvent");

        var coordinates = this.get_node_graph_coordinates(node_ui);
        var offset = Element.cumulativeOffset(this.element);
        var scroll_offset = Element.cumulativeScrollOffset(this.element);

        // Position the event exactly at the center of the node.
        var x = coordinates.x + (node_ui.width / 2) + offset.left - scroll_offset.left;
        var y = coordinates.y + (node_ui.height / 2) + offset.top - scroll_offset.top;

        // Ugly trick -- we need Ws.SelectionManager to be ready to accept mouseup.
        event.initMouseEvent("mouseup", true, true, window, 0, 
                x, y, x, y, false, false, false, false, 0, null);
        event.ws_drag_manager_ignore = true; // We do not want this event to be
                                             // processed by the drag manager.
        Ws.SelectionManager.__ready_to_select = true;
        node_ui.element.firstChild.dispatchEvent(event);
    },

    // Clickable -------------------------------------------------------------------------
    cl_accept_click: function(x, y, event) {
        var to_click;

        // First check whether the original target of the event is a node or a link (it
        // is assumed that it is a node or a link of *this* graph, as we would not be
        // getting this event otherwise). If it is, ask it first and if it agrees, we
        // don't need to look any further. Otherwise we should scan the nodes to find out
        // whether there is another candidate.

        // Hack for MSIE. We're using excanvas, so the original target of the event would
        // be the 'shape' tag or something, which is *not* the direct child of <div>
        // wrapping the <canvas>, so we must go up the hierarchy and replace the target
        // with the corresponding <canvas> if any.
        if (event.is_ie) {
            var ieHackTarget = event.srcElement;
            while (ieHackTarget.parentNode && (ieHackTarget.tagName != 'canvas')) {
                ieHackTarget = ieHackTarget.parentNode;
            }

            if (ieHackTarget) {
                event.target = ieHackTarget;
            }
        }

        // We take the parent node here, as the original target would be a <canvas> in
        // case of graph elements.
        var target = event.target.parentNode;
        if (target && target.__wsg_ui && target.__wsg_ui.is_clickable &&
                target.__wsg_ui.cl_accept_click(x, y, event)) {

            this.__to_click = target.__wsg_ui;

            return true;
        } else {
            var i;

            // First check links (they have higher z indices).
            for (i = 0; i < this.link_uis.length; i++) {
                var link_ui = this.link_uis[i];

                // If link ui is selectable, its z index is bigger than that of the
                // selected ui (ot the selected ui has not yet been set) and it is ok
                // to handle the selection.
                if (link_ui.is_clickable &&
                        (!to_click || to_click.z_index < link_ui.z_index) &&
                        link_ui.cl_accept_click(x, y, event)) {

                    to_click = link_ui;
                }
            }

            // If we have found no link ready to handle selection, check the nodes.
            if (!to_click) {
                for (i = 0; i < this.node_uis.length; i++) {
                    var node_ui = this.node_uis[i];

                    if (node_ui.is_clickable &&
                            (!to_click || to_click.z_index < node_ui.z_index) &&
                            node_ui.cl_accept_click(x, y, event)) {

                        to_click = node_ui;
                    }
                }
            }
        }

        if (to_click) {
            this.__to_click = to_click;
            return true;
        } else {
            return false;
        }
    },

    cl_handle_click: function(x, y, event) {
        if (this.__to_click) {
            this.__to_click.cl_handle_click(x, y, event);
            this.__to_click = null;
        }
    },

    cl_accept_doubleclick: function(x, y, event) {
        var to_click;

        // First check whether the original target of the event is a node or a link (it
        // is assumed that it is a node or a link of *this* graph, as we would not be
        // getting this event otherwise). If it is, ask it first and if it agrees, we
        // don't need to look any further. Otherwise we should scan the nodes to find out
        // whether there is another candidate.

        // We take the parent node here, as the original target would be a <canvas> in
        // case of graph elements.
        var target = event.target.parentNode;
        if (target && target.__wsg_ui && target.__wsg_ui.is_clickable &&
                target.__wsg_ui.cl_accept_doubleclick(x, y, event)) {

            this.__to_doubleclick = target.__wsg_ui;

            return true;
        } else {
            var i;

            // First check links (they have higher z indices).
            for (i = 0; i < this.link_uis.length; i++) {
                var link_ui = this.link_uis[i];

                // If link ui is selectable, its z index is bigger than that of the
                // selected ui (ot the selected ui has not yet been set) and it is ok
                // to handle the selection.
                if (link_ui.is_clickable &&
                        (!to_click || to_click.z_index < link_ui.z_index) &&
                        link_ui.cl_accept_doubleclick(x, y, event)) {

                    to_click = link_ui;
                }
            }

            // If we have found no link ready to handle selection, check the nodes.
            if (!to_click) {
                for (i = 0; i < this.node_uis.length; i++) {
                    var node_ui = this.node_uis[i];

                    if (node_ui.is_clickable &&
                            (!to_click || to_click.z_index < node_ui.z_index) &&
                            node_ui.cl_accept_doubleclick(x, y, event)) {

                        to_click = node_ui;
                    }
                }
            }
        }

        if (to_click) {
            this.__to_doubleclick = to_click;
            return true;
        } else {
            return false;
        }
    },

    cl_handle_doubleclick: function(x, y, event) {
        if (this.__to_doubleclick) {
            this.__to_doubleclick.cl_handle_doubleclick(x, y, event);
            this.__to_doubleclick = null;
        }
    },

    // Pressable -------------------------------------------------------------------------
    pr_handle_keypress: function(event) {
        // For deletion check whether the key code is 46 (DELETE) or 8 (BACKSPACE), if
        // it's not -- take no action.
        if ((event.keyCode == 46) || (event.keyCode == 8)) {
            if (this.__selected) {
                this.__selected.sl_deselect(0, 0, event, null);
                Ws.SelectionManager.__selected = null;

                if (this.__selected.link) {
                    this.graph.remove_link(this.__selected.link);
                } else {
                    this.graph.remove_node(this.__selected.node);
                }

                event.stopPropagation();
                event.preventDefault();
            }
        }
    },

    // Z-ordering ------------------------------------------------------------------------
    bring_forward: function(ui) {
        var all_uis = this.node_uis.concat(this.link_uis);
        var selected_ui = null;
        
        // Find the ui with the next z-index and exchange indices
        for (var i = 0; i < all_uis.length; i++) {
            var current_ui = all_uis[i];
            
            if ((current_ui.z_index > ui.z_index) && 
                (!selected_ui || (current_ui.z_index < selected_ui.z_index))) {
                selected_ui = current_ui;
            }
        }
        
        if (selected_ui) {
            var _z_index = selected_ui.z_index;
            
            selected_ui.z_index = ui.z_index;
            selected_ui.element.style.zIndex = ui.z_index;
            
            ui.z_index = _z_index;
            ui.element.style.zIndex = _z_index;
        }
    },
    
    send_backward: function(ui) {
        var all_uis = this.node_uis.concat(this.link_uis);
        var selected_ui = null;
        
        // Find the ui with the previous z-index and exchange indices
        for (var i = 0; i < all_uis.length; i++) {
            var current_ui = all_uis[i];
            
            if ((current_ui.z_index < ui.z_index) && 
                (!selected_ui || (current_ui.z_index > selected_ui.z_index))) {
                selected_ui = current_ui;
            }
        }
        
        if (selected_ui) {
            var _z_index = selected_ui.z_index;
            
            selected_ui.z_index = ui.z_index;
            selected_ui.element.style.zIndex = ui.z_index;
            
            ui.z_index = _z_index;
            ui.element.style.zIndex = _z_index;
        }
    },
    
    bring_to_front: function(ui) {
        var all_uis = this.node_uis.concat(this.link_uis);
        var selected_ui = null;
        
        // Find the ui with the maximum z-index and set the z-index of the current 
        // one to maximum + 1000
        for (var i = 0; i < all_uis.length; i++) {
            var current_ui = all_uis[i];
            
            if ((current_ui.z_index > ui.z_index) && 
                (!selected_ui || (current_ui.z_index > selected_ui.z_index))) {
                selected_ui = current_ui;
            }
        }
        
        if (selected_ui) {
            var _z_index = selected_ui.z_index;
            
            ui.z_index = _z_index + 1000;
            ui.element.style.zIndex = _z_index + 1000;
        }
        
        this.__normalize_z_indices();
    },
    
    send_to_back: function(ui) {
        var all_uis = this.node_uis.concat(this.link_uis);
        var selected_ui = null;
        
        // Find the ui with the minimum z-index and set the z-index of the current 
        // one to minimum - 1000
        for (var i = 0; i < all_uis.length; i++) {
            var current_ui = all_uis[i];
            
            if ((current_ui.z_index < ui.z_index) && 
                (!selected_ui || (current_ui.z_index < selected_ui.z_index))) {
                selected_ui = current_ui;
            }
        }
        
        if (selected_ui) {
            var _z_index = selected_ui.z_index;
            
            ui.z_index = _z_index - 1000;
            ui.element.style.zIndex = _z_index - 1000;
        }
        
        this.__normalize_z_indices();
    },

    // Parents and children --------------------------------------------------------------
    repaint_node_parent: function(ui) {
        // The ui is assumed to be node ui.
        if (ui.node.parent) {
            ui.node.parent.ui.repaint();
            this.repaint_node_parent(ui.node.parent.ui);
        }
    },

    repaint_node_parent_with_links: function(ui) {
        // The ui is assumed to be node ui.
        if (ui.node.parent) {
            ui.node.parent.ui.repaint();
            this.repaint_links(ui.node.parent.ui);
            this.repaint_node_parent_with_links(ui.node.parent.ui);
        }
    },

    repaint_node_children: function(ui) {
        // The ui is assumed to be node ui.
        var children = ui.node.get_children();

        for (var i = 0; i < children.length; i++) {
            children[i].ui.repaint();
            this.repaint_node_children(children[i].ui);
        }
    },

    repaint_node_children_with_links: function(ui) {
        // The ui is assumed to be node ui.
        var children = ui.node.get_children();

        for (var i = 0; i < children.length; i++) {
            children[i].ui.repaint();
            this.repaint_links(children[i].ui);
            this.repaint_node_children_with_links(children[i].ui);
        }
    },

    // Generic helpers -------------------------------------------------------------------
    /**
     * Returns the coordinates of a node within graph. This method takes into
     * account that a node might have a parent node, which can have a parent of
     * its own, ad infinum.
     */
    get_node_graph_coordinates: function(ui) {
        // ui is assumed to be node ui
        if (ui.node.parent) {
            var parent_ui = ui.node.parent.ui;
            var parent_coords = this.get_node_graph_coordinates(parent_ui);

            return {
                x: ui.x + parent_ui.margin +
                    parent_ui.inner_margin + parent_coords.x,
                y: ui.y + parent_ui.margin +
                    parent_ui.inner_margin + parent_coords.y
            }
        } else {
            return {
                x: ui.x,
                y: ui.y
            }
        }
    },

    get_node_inbound_connector_screen_coordinates: function(node, connector_index) {
        var connector = node.ui.get_inbound_connector(connector_index);

        return this.get_node_connector_screen_coordinates(node, connector);
    },

    get_node_outbound_connector_screen_coordinates: function(node, connector_index) {
        var connector = node.ui.get_outbound_connector(connector_index);

        return this.get_node_connector_screen_coordinates(node, connector);
    },

    get_node_connector_screen_coordinates: function(node, connector) {
        var node_coordinates = this.get_node_graph_coordinates(node.ui);

        var offset = Element.cumulativeOffset(this.element);
        var scroll_offset = Element.cumulativeScrollOffset(this.element);

        return {
            x: Math.floor(offset.left - scroll_offset.left + node_coordinates.x + connector.x),
            y: Math.floor(offset.top - scroll_offset.top + node_coordinates.y + connector.y)
        }
    },

    repaint_links: function(ui) {
        // The ui is assumed to be node ui.
        var links = ui.node.get_links();
        for (var i = 0; i < links.length; i++) {
            links[i].ui.repaint();
        }
    },

    // Private ---------------------------------------------------------------------------
    __add_ui: function(ui, z_index) {
        this.element.appendChild(ui.element);

        ui.element.__wsg_ui = ui;
        ui.graph_ui = this;
        
        ui.z_index = z_index;
        ui.element.setStyle({
            zIndex: z_index
        });
        
        ui.repaint();
    },
    
    __remove_ui: function(ui) {
        this.element.removeChild(ui.element);

        if (ui.is_drag_source) {
            Ws.DragManager.remove_drag_source(ui);
        }

        if (ui.is_drop_target) {
            Ws.DragManager.remove_drop_target(ui);
        }

        if (ui.is_selectable) {
            Ws.SelectionManager.remove_selectable(ui);
        }

        if (ui.is_clickable) {
            Ws.MouseManager.remove_clickable(ui);
        }
    },
    
    __normalize_z_indices: function() {
        var all_uis = this.node_uis.concat(this.link_uis).sortBy(function(item) {
            return item.z_index
        });
        
        var z_index = 1000;
        all_uis.each(function(item) {
            item.z_index = z_index;
            item.element.style.zIndex = z_index;
            
            z_index += 10;
        });
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Graph.FactoryStub = {
    register_type: function(type, constructor) {
        this.types.push({
            type: type,
            constructor: constructor
        });
    },

    unregister_type: function(type) {
        for (var i = 0; i < this.types.length; i++) {
            if (this.types[i].type == type) {
                this.types.splice(i, 1);
            }
        }
    },
    
    create: function(type, a1, a2, a3, a4, a5) {
        var constructor = null;

        for (var i = 0; i < this.types.length; i++) {
            if (this.types[i].type == type) {
                constructor = this.types[i].constructor;
            }
        }

        if (constructor) {
            var thing = new constructor(a1, a2, a3, a4, a5);
            thing.type = type;

            return thing;
        }

        return null;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Graph.NodeFactory = {
    types: [],
    
    register_type: Ws.Graph.FactoryStub.register_type,
    
    unregister_type: Ws.Graph.FactoryStub.unregister_type,

    new_node: Ws.Graph.FactoryStub.create,
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
};

//----------------------------------------------------------------------------------------
Ws.Graph.NodeUiFactory = {
    types: [],
    
    register_type: Ws.Graph.FactoryStub.register_type,

    unregister_type: Ws.Graph.FactoryStub.unregister_type,

    new_node_ui: Ws.Graph.FactoryStub.create,
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
};

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Graph.LinkFactory = {
    types: [],
    
    register_type: Ws.Graph.FactoryStub.register_type,

    unregister_type: Ws.Graph.FactoryStub.unregister_type,

    new_link: Ws.Graph.FactoryStub.create,
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
};

//----------------------------------------------------------------------------------------
Ws.Graph.LinkUiFactory = {
    types: [],
    
    register_type: Ws.Graph.FactoryStub.register_type,

    unregister_type: Ws.Graph.FactoryStub.unregister_type,

    new_link_ui: Ws.Graph.FactoryStub.create,
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
};

//////////////////////////////////////////////////////////////////////////////////////////
// Constants

// Events --------------------------------------------------------------------------------
Ws.Graph.EVENT_GRAPH_UPDATED = "graph-updated";
Ws.Graph.EVENT_NODE_ADDED = "node-added";
Ws.Graph.EVENT_NODE_REMOVED = "node-removed";
Ws.Graph.EVENT_LINK_ADDED = "link-added";
Ws.Graph.EVENT_LINK_REMOVED = "link-removed";
Ws.Graph.EVENT_NODE_UPDATED = "node-updated";
Ws.Graph.EVENT_LINK_UPDATED = "link-updated";

// Policies ------------------------------------------------------------------------------
Ws.Graph.MISCONNECTED_LINKS_DELETE = "delete";
Ws.Graph.MISCONNECTED_LINKS_CLEAN = "clean";

