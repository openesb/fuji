/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var CONTROL_FILTER = "filter";

//----------------------------------------------------------------------------------------
var FilterControlNode = Class.create(EipNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    CONTROL_FILTER, this, ui_options);
        }

        Ws.Utils.apply_defaults(this.properties, {
            expression_type: "xpath",
            use_external_config: false,
            expression: "",
            external_config_name: "filter-config"
        });
    },

    pc_get_descriptors: function($super) {
        var container = $super();
        var node = this;

        container.properties.push(
            {
                name: "properties_expression_type",
                type: Ws.PropertiesSheet.ENUM,

                display_name: "Type",
                description: "Type of the expression used to filter incoming messages.",
                value: this.properties.expression_type,
                values: [
                    {
                        display_name: "XPath",
                        description: "XPath expression. Works best when used with XML data.",
                        value: "xpath"
                    },
                    {
                        display_name: "Regular Expression",
                        description: "Regular Expression. Most useful for non-XML textual data.",
                        value: "regex"
                    }
                ]
            }
            ,
            {
                name: "properties_use_external_config",
                type: Ws.PropertiesSheet.BOOLEAN,

                display_name: "Use External",
                description: "Whether the configuration data for this EIP is inlined or loaded from an external file.",
                value: this.properties.use_external_config
            }
            ,
            {
                name: "properties_expression",
                type: Ws.PropertiesSheet.CODE,
                code_type : "xml",
                display_name: "Expression",
                description: "The expression used to filter incoming messages.",
                value: this.properties.expression,

                auto_adjust_height: true,
                maximizable: true,

                depends: {
                    name: "properties_use_external_config",
                    value: false
                },

                dynamic: [
                    {
                        name: "properties_expression_type",
                        value: "xpath",

                        changes: {
                            inline_description: "Enter the XPath expression which should evaluate to <b>true</b> in order for the message to pass. There is no need to quote or escape it."
                        }
                    },
                    {
                        name: "properties_expression_type",
                        value: "regex",

                        changes: {
                            inline_description: "Enter the regular expression which should be found in the text in order for the message to pass. There is no need to quote or escape it."
                        }
                    }
                ]
            }
            ,
            {
                name: "properties_external_config_name",
                type: Ws.PropertiesSheet.STRING,

                display_name: "Config name",
                description: "Name of the extenrnal configuration.",
                value: this.properties.external_config_name,

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config_warning",
                type: Ws.PropertiesSheet.LABEL,

                display_name: "Note: once you click the Edit Configuration button, changes you made to the other properties will be automatically saved.",
                description: "",

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config",
                type: Ws.PropertiesSheet.BUTTON,

                display_name: "Edit Configuration",
                description: "Opens the external configuragion file in the editor.",
                action: function(psheet) {
                    psheet.save();
                    edit_external_artifact(node.id, "external-config", false);
                },

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
        );

        return container;
    }
});

//----------------------------------------------------------------------------------------
var FilterControlNodeUi = Class.create(EipNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);

        this.shape_size = 44;
        this.skew_size = 10;
        this.width = this.shape_size + (this.margin * 2);
        this.height = this.shape_size + (this.margin * 2);

        this.toolbar_x = this.width - this.margin;
    },

    get_inbound_connectors: function() {
        var connectors = this.node.get_inbound_connectors();

        if (connectors.length > 0) {
            connectors[0].x = this.margin + (this.skew_size / 2),
            connectors[0].y = this.height / 2,
            connectors[0].type = Ws.Graph.NODE_CONNECTOR_WEST
        }

        return connectors;
    },

    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();

        if (connectors.length > 0) {
            connectors[0].x = this.width - this.margin - (this.skew_size / 2),
            connectors[0].y = this.height / 2,
            connectors[0].type = Ws.Graph.NODE_CONNECTOR_EAST
        }

        return connectors;
    },

    prepare_path: function() {
        // A---------B
        //  \       /
        //   \     /
        //    \   /
        //     D-C
        var A_x = this.margin, A_y = this.margin;
        var B_x = this.width - this.margin, B_y = this.margin;
        var C_x = this.width - this.margin - this.skew_size, C_y = this.height - this.margin;
        var D_x = this.margin + this.skew_size, D_y = this.height - this.margin;

        this.context.beginPath();
        this.context.moveTo(A_x, A_y);
        this.context.lineTo(B_x, B_y);
        this.context.lineTo(C_x, C_y);
        this.context.lineTo(D_x, D_y);
        this.context.lineTo(A_x, A_y);
    },

    draw_node: function($super) {
        $super();

        var base_x = this.width / 2;
        var base_y = this.height / 2;

        this.context.lineWidth = 1;
        this.context.fillStyle = this.context.strokeStyle;

        this.context.beginPath();
        this.context.moveTo(base_x - 12, base_y - 13);
        this.context.lineTo(base_x + 12, base_y - 13);
        this.context.lineTo(base_x + 3, base_y);
        this.context.lineTo(base_x + 3, base_y + 10);
        this.context.lineTo(base_x - 3, base_y + 13);
        this.context.lineTo(base_x - 3, base_y);
        this.context.lineTo(base_x - 12, base_y - 13);
        this.context.stroke();
    },

    draw_text: function() {
        // Does nothing (no text on filter)
    },

    draw_icon: function() {
        // Does nothing (no icon on filter)
    },

    __get_toolbar_items: function($super) {
        var self = this;
        var items = [];

        if (!this.read_only) {
            items.push({
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        items.push({
            icon: Ws.Graph.NODE_TBAR_EDIT_PROPERTIES,
            icon_d : Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE,
            icon_width: 16,
            icon_height: 16,
            tooltip: "Properties",
            callback: function(event, options) {
                self.__tbar_edit_properties(options);
            }
        });

        return items;
    },

    noop: function() {
        // Does nothing.
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(CONTROL_FILTER, FilterControlNode);
Ws.Graph.NodeUiFactory.register_type(CONTROL_FILTER, FilterControlNodeUi);
