/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.LoadManager = {
    STATE_LOADING: "loading",
    STATE_LOADED: "loaded",
    STATE_FAILED: "failed",
    
    load_timeout: 20000, // in milliseconds
    
    _objects: [],
    
    load: function(url, on_success, on_failure) {
        for (var i = 0; i < this._objects.length; i++) {
            var item = this._objects[i];
            
            if (item.url == url) {
                if ((item.state == this.STATE_LOADED) && on_success) {
                    on_success();
                } else
                
                if ((item.state == this.STATE_FAILED) && on_failure) {
                    on_failure();
                } else 
                
                if (item.state == this.STATE_LOADING) {
                    if (on_success) {
                        var old_on_success = item.on_success;
                        item.on_success = function() {
                            on_success();
                            old_on_success();
                        };
                    }
                    
                    if (on_failure) {
                        var old_on_failure = item.on_failure;
                        item.on_failure= function() {
                            on_failure();
                            old_on_failure();
                        };
                    }
                }
                
                return;
            }
        }
        
        var descriptor = {
            url: url,
            on_success: on_success,
            on_failure: on_failure,
            state: this.STATE_LOADING
        };
        
        this._objects.push(descriptor);
        
        var _this = this;
        descriptor.timeout = setTimeout(function() {
            _this._load_state_changed(url, _this.STATE_FAILED);
        }, this.load_timeout);
        
        descriptor.object = this._construct_object(url);
        
    },
    
    get: function(url) {
        for (var i = 0; i < this._objects.length; i++) {
            var object = this._objects[i];
            
            if (object.url == url) {
                if (object.state == this.STATE_LOADED) {
                    return object.object;
                } else {
                    return null;
                }
            }
        }
        
        return null;
    },
    
    get_state: function(url) {
        for (var i = 0; i < this._objects.length; i++) {
            var object = this._objects[i];
            
            if (object.url == url) {
                return object.state;
            }
        }
        
        return null;
    },
    
    _construct_object: function(url) {
        return {};
    },
    
    _load_state_changed: function(url, state) {
        for (var i = 0; i < this._objects.length; i++) {
            var descriptor = this._objects[i];
            
            if (descriptor.url == url) {
                if (descriptor.state == this.STATE_LOADING) {
                    descriptor.state = state;
                    
                    if ((state == this.STATE_LOADED) && descriptor.on_success) {
                        if (descriptor.timeout) {
                            clearTimeout(descriptor.timeout);
                        }
                        
                        descriptor.on_success();
                    } else 
                    
                    if ((state == this.STATE_FAILED) && descriptor.on_failure) {
                        descriptor.on_failure();
                    }
                }
                
                return;
            }
        }
    },
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
};

//////////////////////////////////////////////////////////////////////////////////////////
Ws.ImagesLoadManager = {}; Object.extend(Ws.ImagesLoadManager, Ws.LoadManager);

Ws.ImagesLoadManager._construct_object = function(url) {
    var _this = this;
    
    var image = new Image();
    image.onload = function() {
        _this._load_state_changed(url, _this.STATE_LOADED);
    };
    image.src = url;
    
    return image;
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.ScriptsLoadManager = {}; Object.extend(Ws.ScriptsLoadManager, Ws.LoadManager);

Ws.ScriptsLoadManager._construct_object = function(url) {
    var _this = this;
    
    var script = new Element("script");
    script.type = "text/javascript";
    script.src = url;
    
    if (Ws.Utils.BrowserDetect.browser == "Explorer") {
        script.onreadystatechange = function() {
            if ((script.readyState == "complete") || (script.readyState == "loaded")) {
                _this._load_state_changed(url, _this.STATE_LOADED);
            }
        }
    } else {
        script.onload = function() {
            _this._load_state_changed(url, _this.STATE_LOADED);
        }
    }
    
    $A($$("head"))[0].appendChild(script);
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.MultiLoadManager = Class.create({
    _objects: [],
    
    _on_success: null,
    _on_failure: null,
    
    /**
     *
     * @param objects An array of descriptors which describes the entities needing to be 
     *      loaded. Each element of the array should contain the following properties:
     *      <ul>
     *      <li> <code>url</code> -- URL of the target entity
     *      <li> <code>load_manager</code> -- <code>Ws.LoadManager</code> object which 
     *              should be used for loading this particular entity
     *      <li> <code>critical</code> -- indicates whether this entity if critical to 
     *              whole set, i.e. whether failing to load it will fail the whole set. 
     *              By default the set is considered to be loaded successfully if at 
     *              least one of the requested entitites loaded successfully.
     *      </ul>
     * @param on_success Function which should be called upon successful loading of the 
     *      given set of objects.
     * @param on_failure Function which should be called upon failure to load the given
     *      set of objects.
     */
    initialize: function(objects, on_success, on_failure) {
        this._objects = objects;
        
        this._on_success = on_success;
        this._on_failure = on_failure;
    },
    
    load: function() {
        var _this = this;
        this._objects.each(function(item) {
            item.load_manager.load(item.url, function() {
                _this._loaded(item.url, true);
            }, function() {
                _this._loaded(item.url, false);
            });
        });
    },
    
    _loaded: function(url, success) {
        var wslm = Ws.LoadManager;
        
        var overall_state = null;
        
        for (var i = 0; i < this._objects.length; i++) {
            var object = this._objects[i];
            
            if (object.url == url) {
                if (success) {
                    object.state = wslm.STATE_LOADED;
                } else {
                    object.state = wslm.STATE_FAILED;
                }
            }
            
            if (!object.state) {
                overall_state = wslm.STATE_LOADING;
            }
            
            if ((overall_state == null) && (object.state == wslm.STATE_LOADED)) {
                overall_state = wslm.STATE_LOADED;
            }
            
            if (((overall_state == null) || (overall_state == wslm.STATE_LOADED)) && 
                    (object.state == wslm.STATE_FAILED) && object.critical) {
                overall_state = wslm.STATE_FAILED;
            }
        }
        
        if ((overall_state == wslm.STATE_LOADED) && this._on_success) {
            this._on_success();
        }
        
        if ((overall_state == wslm.STATE_FAILED) && this._on_failure) {
            this._on_failure();
        }
    },
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});