/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
/**
 * This class manages mouse clicks. It is currently capable of handling 'click' and 
 * 'doubleclick' events.
 *
 * It uses raw mouse events, which helps overcome the inability of MSIE. It does not fire
 * '*click' events if the DOM structure changes between 'mousedown' and 'mouseup', which
 * is always the case when, say, a <canvas> (using excanvas) is repainted as the result
 * of a 'mousedown'.
 */
Ws.MouseManager = {
    clickables: [],

    double_click_delay: 500,

    __ready_to_click: false,
    __last_one_up: false,
    __click_count: 0,

    __last_down_x: -100,
    __last_down_y: -100,

    initialize: function() {
        // Does nothing
    },
    
    add_clickable: function(clickable) {
        this.clickables.push(clickable);

        Event.observe(clickable, "mousedown", this.__mousedown);
        Event.observe(clickable, "mousemove", this.__mousemove);
        Event.observe(clickable, "mouseup", this.__mouseup);
    },
    
    remove_clickable: function(clickable) {
        for (var i = 0; i < Ws.MouseManager.clickables.length; i++) {
            if (this.clickables[i] == clickable) {
                this.clickables.splice(i, 1);
                break;
            }
        }

        Event.stopObserving(clickable, "mousedown", this.__mousedown);
        Event.stopObserving(clickable, "mousemove", this.__mousemove);
        Event.stopObserving(clickable, "mouseup", this.__mouseup);
    },

    __mousedown: function(event) {
        //__debug_log("-- down: " + event.clientX + ", " + event.clientY);

        Ws.MouseManager.__ready_to_click = true;
        Ws.MouseManager.__last_one_up = false;

        Ws.MouseManager.__last_down_x = event.clientX;
        Ws.MouseManager.__last_down_y = event.clientY;
    },
    
    __mousemove: function(event) {
        //__debug_log("-- move: " + event.clientX + ", " + event.clientY);

        if ((event.clientX != Ws.MouseManager.__last_down_x) ||
                (event.clientY != Ws.MouseManager.__last_down_y)) {
            
            Ws.MouseManager.__ready_to_click = false;
            Ws.MouseManager.__last_one_up = false;
        }
    },

    __mouseup: function(event) {
        //__debug_log("-- up: " + event.clientX + ", " + event.clientY);

        if (Ws.MouseManager.__ready_to_click || Ws.MouseManager.__last_one_up) {
            Ws.MouseManager.__ready_to_click = false;
            Ws.MouseManager.__last_one_up = true;
            Ws.MouseManager.__click_count += 1;

            //__debug_log("-- clicks:" + Ws.MouseManager.__click_count);

            // Hack for MSIE, if there is no event.currentTarget, replace it with
            // 'this'. Or, it might should have been 'this' from the very beginning.
            if (!event.currentTarget) {
                event.currentTarget = this;
                event.target = event.srcElement;
                event.is_ie = true;
            }

            if (Ws.MouseManager.__click_count == 1) {
                Ws.MouseManager.__click(event);
            }

            if (Ws.MouseManager.__click_count == 2) {
                Ws.MouseManager.__doubleclick(event);
                Ws.MouseManager.__click_count = 0;
            }

            setTimeout(function() {
                Ws.MouseManager.__click_count = 0;
            }, 500);
        }
    },

    __click: function(event) {
        //__debug_log(">>>> Click!");

        var x = event.clientX;
        var y = event.clientY;
        
        var selected_cl = null;
        var selected_z_index = -1;

        // First check the current target of the mouse event. If it accepts the
        // selection, we don't need to look any further. Otherwise we should iterate
        // over the selectables picking the one we want.
        if (event.currentTarget.cl_accept_click(x, y, event)) {
            selected_cl = event.currentTarget;
        } else {
            for (var i = 0; i < Ws.MouseManager.clickables.length; i++) {
                var clickable = Ws.MouseManager.clickables[i];

                if (clickable.cl_accept_click(x, y, event)) {
                    var z_index = Ws.Utils.get_z_index(clickable);

                    if ((selected_cl == null) || (z_index > selected_z_index)) {
                        selected_cl = clickable;
                        selected_z_index = z_index;
                    }
                }
            }
        }

        if (selected_cl) {
            selected_cl.cl_handle_click(x, y, event);
        }
    },

    __doubleclick: function(event) {
        //__debug_log(">>>> Double Click!");

        var x = event.clientX;
        var y = event.clientY;

        var selected_cl = null;
        var selected_z_index = -1;

        // First check the current target of the mouse event. If it accepts the
        // selection, we don't need to look any further. Otherwise we should iterate
        // over the selectables picking the one we want.
        if (event.currentTarget.cl_accept_doubleclick(x, y, event)) {
            selected_cl = event.currentTarget;
        } else {
            for (var i = 0; i < Ws.MouseManager.clickables.length; i++) {
                var clickable = Ws.MouseManager.clickables[i];

                if (clickable.cl_accept_doubleclick(x, y, event)) {
                    var z_index = Ws.Utils.get_z_index(clickable);

                    if ((selected_cl == null) || (z_index > selected_z_index)) {
                        selected_cl = clickable;
                        selected_z_index = z_index;
                    }
                }
            }
        }

        if (selected_cl) {
            selected_cl.cl_handle_doubleclick(x, y, event);
        }
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
};

//////////////////////////////////////////////////////////////////////////////////////////
// Static initialization
Event.observe(window, "load", function() {
    Ws.MouseManager.initialize();
});
