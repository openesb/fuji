/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.PropertiesSheet = Class.create({
    container: null,
    element: null,
    validation_results_element: null,

    id_prefix: "-ws-psheet-",

    classname_table: "-ws-psheet",
    classname_label: "-ws-psheet-label",
    classname_input: "-ws-psheet-input",
    classname_type_prefix: "-ws-psheet-type-",
    classname_onecolumn_suffix: "-one-column",
    classname_twocolumn_suffix: "-two-column",
    classname_error: "-ws-psheet-validation-error",
    classname_warning: "-ws-psheet-validation-warning",
    classname_info: "-ws-psheet-validation-info",

    descriptors: null,

    validate_on_save: true,
    validate_on_save_threshold: 400,

    __tab_descriptors: null,

    listeners: null,

    initialize: function(container, element, options) {
        if (options) Object.extend(this, options);

        Element.extend(element);

        this.container = container;
        this.element = element;

        this.element.ws_psheet = this;
        this.element.makePositioned();

        if (!this.element.id) {
            this.element.id = "ws-psheet-" + Ws.PropertiesSheet.counter++;
        }

        this.listeners = [];

        this.reset();
    },

    reset: function() {
        this.element.innerHTML = "";

        this.descriptors = this.container.pc_get_descriptors();

        // Apply the dynamic defaults, so we're sure that everything renders correctly.
        // E.g. an ENUM might not have 'values' set initially, so we need to move it from
        // 'dynamic_defaults' to the descriptor itself.
        var i, j, descriptor, dynamic_data;
        if (this.descriptors.sections) {
            for (i = 0; i < this.descriptors.sections.length; i++) {
                for (j = 0; j < this.descriptors.sections[i].properties.length; j++) {
                    descriptor = this.descriptors.sections[i].properties[j];

                    if (descriptor.dynamic && descriptor.dynamic_defaults) {
                        Ws.Utils.apply_defaults(descriptor, descriptor.dynamic_defaults);
                    }
                }
            }
        } else {
            for (i = 0; i < this.descriptors.properties.length; i++) {
                descriptor = this.descriptors.properties[i];

                if (descriptor.dynamic && descriptor.dynamic_defaults) {
                    Ws.Utils.apply_defaults(descriptor, descriptor.dynamic_defaults);
                }
            }
        }


        this.render();         // Render the HTML skeleton of the properties sheet
        this.reload();         // Load the actual values to the inputs
        this.update_depends(); // Hide or show dependent properties
        this.update_dynamic(); // According to other values, update the properties
        this.validate();       // Validate the values of the properties
        this.layout();         // Adjust the layout of the properties sheet
    },

    render: function() {
        var inner_html = "";

        if (this.descriptors.sections) {
            this.__tab_descriptors = [];

            for (var i = 0; i < this.descriptors.sections.length; i++) {
                var section = this.descriptors.sections[i];

                inner_html += "<div>";
                inner_html += "<table class=\"" + this.classname_table + "\">";
                inner_html += this.__render_section(section.properties);
                inner_html += "</table>";
                inner_html += "</div>";

                var self = this;
                this.__tab_descriptors.push({
                   display_name: section.display_name,
                   description: section.description,

                   select_callback: function() {
                       self.layout();
                   }
                });
            }

            this.element.innerHTML = inner_html;
            
            var selected_index = this.element.ws_tpane ? this.element.ws_tpane.selected_index : 0;
            new Ws.TabbedPane(this.element, this.__tab_descriptors, {selected_index: selected_index});
            
            for (var j = 0; j < this.descriptors.sections.length; j++) {
                var section = this.descriptors.sections[j];
                this.__do_code_editor(section.properties);
            }
        } else {
            inner_html += "<table class=\"" + this.classname_table + "\">";
            inner_html += this.__render_section(this.descriptors.properties);
            inner_html += "</table>";

            this.element.innerHTML = inner_html;
            this.__do_code_editor(this.descriptors.properties);
        }

        
    },

    __do_code_editor: function(properties) {
        for (var i = 0; i < properties.length; i++) {
            var descriptor = properties[i];
            if (descriptor.type == Ws.PropertiesSheet.CODE) {
                this.element.select("#" + this.__get_input_id(descriptor)).each(function(element) {
                    var id = element.id;
                    editAreaLoader.init({
                        id : id,
                        min_height : 5,
                        min_width: 5,
                        syntax: descriptor.code_type,
                        toolbar: "",
                        start_highlight: true,
                        allow_toggle: false,
                        allow_resize: "no"
                    });
                })
            }
        }
    },

    reload: function() {
        if (this.descriptors.sections) {
            for (var i = 0; i < this.descriptors.sections.length; i++) {
                this.__reload_section(this.descriptors.sections[i].properties);
            }
        } else {
            this.__reload_section(this.descriptors.properties);
        }
    },

    update_depends: function() {
        var i, result, updated = false;

        if (this.descriptors.sections) {
            for (i = 0; i < this.descriptors.sections.length; i++) {
                result = this.__update_depends_section(
                        this.descriptors.sections[i].properties);

                if (result.updated) {
                    updated = true;
                }
            }
        } else {
            result = this.__update_depends_section(this.descriptors.properties);

            updated = result.updated;
        }

        if (updated) {
            this.__notify_listeners(Ws.PropertiesSheet.EVENT_LAYOUT_CHANGED, {
                event_cause: "updated-depends"
            });
        }
    },

    update_dynamic: function() {
        var i, result, updated = false;

        if (this.descriptors.sections) {
            for (i = 0; i < this.descriptors.sections.length; i++) {
                result = this.__update_dynamic_section(
                        this.descriptors.sections[i].properties);

                if (result.updated) {
                    updated = true;
                }
            }
        } else {
            result = this.__update_dynamic_section(this.descriptors.properties);

            updated = result.updated;
        }

        if (updated) {
            this.__notify_listeners(Ws.PropertiesSheet.EVENT_LAYOUT_CHANGED, {
                event_cause: "updated-dynamic"
            });
        }
    },

    validate: function() {
        var results = [];

        if (this.descriptors.sections) {
            for (var i = 0; i < this.descriptors.sections.length; i++) {
                results = results.concat(this.__validate_section(
                        this.descriptors.sections[i].properties));
            }
        } else {
            results = results.concat(
                    this.__validate_section(this.descriptors.properties));
        }

        if (this.validation_results_element) {
            this.validation_results_element.innerHTML = "";

            for (var j = 0; j < results.length; j++) {
                var classname = "";
                if (results[j].severity == Ws.PropertiesSheet.ERROR) {
                    classname = this.classname_error;
                } else if (results[j].severity == Ws.PropertiesSheet.WARNING) {
                    classname = this.classname_warning;
                } else {
                    classname = this.classname_info;
                }

                this.validation_results_element.innerHTML +=
                    "<div class=\"" + classname + "\">" + results[j].message + "</div>";
            }
        }

        this.__notify_listeners(Ws.PropertiesSheet.EVENT_VALIDATION_FINISHED, {
            results: results
        });

        return results;
    },

    layout: function(silent) {
        if (this.descriptors.sections) {
            for (var i = 0; i < this.descriptors.sections.length; i++) {
                if (this.__tab_descriptors[i].contents.getStyle("display") == "block") {
                    this.__layout_section(this.descriptors.sections[i].properties);
                }
            }
        } else {
            this.__layout_section(this.descriptors.properties);
        }

        if (!silent) {
            this.__notify_listeners(Ws.PropertiesSheet.EVENT_LAYOUT_CHANGED, {
                event_cause: "explicit-call"
            });
        }
    },

    save: function() {
        var validation_passed = true;

        if (this.validate_on_save) {
            var validation_results = this.validate();

            for (var i = 0; i < validation_results.length; i++) {
                if (validation_results[i].severity > this.validate_on_save_threshold) {
                    validation_passed = false;
                }
            }
        }

        if (validation_passed) {
            var properties = this.get_properties();
            this.container.pc_set_properties(properties);
        }
    },

    get_properties: function() {
        var properties = {};

        if (this.descriptors.sections) {
            for (var i = 0; i < this.descriptors.sections.length; i++) {
                this.__get_section_properties(this.descriptors.sections[i].properties, properties);
            }
        } else {
            this.__get_section_properties(this.descriptors.properties, properties);
        }

        return properties;
    },

    add_listener: function(listener) {
        this.listeners.push(listener);
    },

    remove_listener: function(listener) {
        for (var i = 0; i < this.listeners.length; i++) {
            if (this.listeners[i] == listener) {
                this.listeners.splice(i, 1);
                break;
            }
        }
    },

    __notify_listeners: function(event_type, event_data) {
        if (!event_data) event_data = {};

        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i](event_type, event_data);
        }
    },

    __render_section: function(descriptors) {
        var inner_html = "";

        for (var i = 0; i < descriptors.length; i++) {
            var descriptor = descriptors[i];

            var label_id = this.__get_label_td_id(descriptor.name);
            var input_id = this.__get_input_td_id(descriptor.name);

            switch (this.__get_render_type(descriptor)) {
                case Ws.PropertiesSheet.RENDER_ONE_COLUMN:
                    inner_html += "<tr>";
                    inner_html +=
                            "<td class=\"" + this.classname_label + " " + this.classname_label + this.classname_onecolumn_suffix + "\" " +
                                    "id=\"" + label_id + "\">" +
                                this.__render_label(descriptor) +
                            "</td>" +
                            "<td></td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td class=\"" + this.classname_input + " " + this.classname_input + this.classname_onecolumn_suffix + "\" " +
                                    "colspan=\"2\" " +
                                    "id=\"" + input_id + "\">" +
                                this.__render_input(descriptor) +
                            "</td>";
                    inner_html += "</tr>";
                    break;
                case Ws.PropertiesSheet.RENDER_TWO_COLUMNS:
                    inner_html += "<tr>";
                    inner_html +=
                            "<td class=\"" + this.classname_label + " " + this.classname_label + this.classname_twocolumn_suffix + "\" " +
                                    "id=\"" + label_id + "\">" +
                                this.__render_label(descriptor) +
                            "</td>" +
                            "<td class=\"" + this.classname_input + " " + this.classname_label + this.classname_twocolumn_suffix + "\" " +
                                    "id=\"" + input_id + "\">" +
                                this.__render_input(descriptor) +
                            "</td>";
                    inner_html += "</tr>";
                    break;
                case Ws.PropertiesSheet.RENDER_CUSTOM:
                default:
                    inner_html += this.__render_input(descriptor);
                    break;
            }
        }

        return inner_html;
    },

    __render_label: function(descriptor) {
        return "<div title=\"" + descriptor.description + "\">" + descriptor.display_name + ":" + "</div>";
    },

    __render_input: function(descriptor) {
        var html = "";

        if (descriptor.render) {
            html += descriptor.render(this, descriptor);
        } else if (Ws.PropertiesSheet.Types[descriptor.type] &&
                Ws.PropertiesSheet.Types[descriptor.type].render) {

            html += Ws.PropertiesSheet.Types[descriptor.type].render(this, descriptor);
        }

        return html;
    },

    __layout_section: function(descriptors) {
        for (var i = 0; i < descriptors.length; i++) {
            var descriptor = descriptors[i];

            if (descriptor.layout) {
                descriptor.layout(this, descriptor);
            } else if (Ws.PropertiesSheet.Types[descriptor.type] &&
                    Ws.PropertiesSheet.Types[descriptor.type].layout) {

                Ws.PropertiesSheet.Types[descriptor.type].layout(this, descriptor);
            }
        }
    },

    __reload_section: function(descriptors) {
        for (var i = 0; i < descriptors.length; i++) {
            this.__reload_descriptor(descriptors[i]);
        }
    },

    __reload_descriptor: function(descriptor) {
        if (descriptor.set_value) {
            descriptor.set_value(this, descriptor);
        } else if (Ws.PropertiesSheet.Types[descriptor.type] &&
                Ws.PropertiesSheet.Types[descriptor.type].set_value) {

            Ws.PropertiesSheet.Types[descriptor.type].set_value(this, descriptor);
        }
    },

    __validate_section: function(descriptors) {
        var results = [];

        for (var i = 0; i < descriptors.length; i++) {
            var descriptor = descriptors[i];

            if (descriptor.validate) {
                results = results.concat(descriptor.validate(this, descriptor));
            } else if (Ws.PropertiesSheet.Types[descriptor.type] &&
                    Ws.PropertiesSheet.Types[descriptor.type].validate) {

                results = results.concat(Ws.PropertiesSheet.Types[descriptor.type].validate(
                        this, descriptor));
            }
        }

        return results;
    },

    __update_depends_section: function(descriptors) {
        var i, updated = false;

        for (i = 0; i < descriptors.length; i++) {
            var descriptor = descriptors[i];
            
            var dependency_satified = descriptor.depends ?
                    this.__calculate_expression(descriptor.depends) : true;

            var descriptor_hidden = descriptor.__hidden ? true : false;

            if ((dependency_satified && descriptor_hidden) ||
                    (!dependency_satified && !descriptor_hidden)) {
                
                updated = true;
                descriptor.__hidden = !dependency_satified;

                if (dependency_satified) {
                    this.__show(descriptor);
                } else {
                    this.__hide(descriptor);
                }
            }
        }

        var result = {
            updated: updated
        };

        return result;
    },

    __update_dynamic_section: function(descriptors) {
        var i, j, updated = false;
        
        for (i = 0; i < descriptors.length; i++) {
            var static_descriptor = descriptors[i];

            if (!static_descriptor.dynamic) {
                continue;
            }

            var dynamic_changes = null;

            for (j = 0; j < static_descriptor.dynamic.length; j++) {
                dynamic_changes = this.__calculate_expression(
                        static_descriptor.dynamic[j], "changes");

                if (dynamic_changes) {
                    break;
                }
            }

            if (!dynamic_changes) {
                dynamic_changes = static_descriptor.dynamic_defaults;
            }

            var dynamic_data =
                    this.__copy_dynamic_changes(static_descriptor, dynamic_changes);

            if (dynamic_data.differs) {
                var dynamic_descriptor = dynamic_data.descriptor;

                var value_policy;

                if (dynamic_descriptor.dynamic_value_policy) {
                    value_policy = dynamic_descriptor.dynamic_value_policy;
                } else {
                    value_policy = Ws.PropertiesSheet.DYNAMIC_VALUE_REPLACE_DEFAULT;
                }

                var current_value = this.__get_value_descriptor(static_descriptor);
                
                switch (this.__get_render_type(dynamic_descriptor)) {
                    case Ws.PropertiesSheet.RENDER_ONE_COLUMN:
                    case Ws.PropertiesSheet.RENDER_TWO_COLUMNS:

                        $(this.__get_label_td_id(static_descriptor.name)).innerHTML =
                                this.__render_label(dynamic_descriptor);
                        $(this.__get_input_td_id(static_descriptor.name)).innerHTML =
                                this.__render_input(dynamic_descriptor);

                        break;
                    case Ws.PropertiesSheet.RENDER_CUSTOM:
                    default:
                        Ws.PropertiesSheet.Types[static_descriptor.type].rerender(
                                this, static_descriptor, dynamic_descriptor);
                        break;
                }

                switch (value_policy) {
                    case Ws.PropertiesSheet.DYNAMIC_VALUE_ALWAYS_KEEP:
                        dynamic_descriptor.value = current_value;
                    case Ws.PropertiesSheet.DYNAMIC_VALUE_ALWAYS_REPLACE:
                        this.__reload_descriptor(dynamic_descriptor);
                        break;
                    case Ws.PropertiesSheet.DYNAMIC_VALUE_REPLACE_DEFAULT:
                        var default_value = dynamic_descriptor.value;

                        var __field = "" + dynamic_descriptor.__repeated_update;
                        
                        var repeated_update = (__field != "undefined");
                        var value_changed = !Ws.Utils.equals(static_descriptor.value, current_value);
                        var update_to_current_value = !repeated_update || value_changed;

                        if (update_to_current_value) {
                            dynamic_descriptor.value = current_value;
                        }

                        this.__reload_descriptor(dynamic_descriptor);
                        dynamic_descriptor.__repeated_update = true;
                        dynamic_descriptor.value = default_value;
                        
                        break;
                }

                descriptors[i] = dynamic_descriptor;
            }

            updated = true;
        }

        var result = {
            updated: updated
        }

        return result;
    },

    __get_section_properties: function(descriptors, properties) {
        for (var i = 0; i < descriptors.length; i++) {
            var descriptor = descriptors[i];

            properties[descriptor.name] = this.__get_value_descriptor(descriptor);
        }
    },

    __get_value_descriptor: function(descriptor) {
        if (descriptor.get_value) {
            return descriptor.get_value(this, descriptor);
        } else if (Ws.PropertiesSheet.Types[descriptor.type] &&
                Ws.PropertiesSheet.Types[descriptor.type].get_value) {

            return Ws.PropertiesSheet.Types[descriptor.type].get_value(this, descriptor);
        }

        return null;
    },

    __get_label_td_id: function(name) {
        return this.element.id + "-" + name + "-label";
    },

    __get_input_td_id: function(name) {
        return this.element.id + "-" + name + "-input";
    },

    __get_input_id: function(descriptor) {
        return this.element.id + "-" + descriptor.name + "-input-" + descriptor.type;
    },

    __get_input_class: function(descriptor) {
        return this.classname_type_prefix + descriptor.type;
    },

    __get_descriptor: function(name) {
        if (this.descriptors.sections) {
            for (var i = 0; i < this.descriptors.sections.length; i++) {
                var descriptor = this.__get_descriptor_in_section(
                        this.descriptors.sections[i].properties, name);

                if (descriptor != null) {
                    return descriptor;
                }
            }
        } else {
            return this.__get_descriptor_in_section(this.descriptors.properties, name);
        }

        return null;
    },

    __get_descriptor_in_section: function(descriptors, name) {
        for (var i = 0; i < descriptors.length; i++) {
            if (descriptors[i].name == name) {
                return descriptors[i];
            }
        }

        return null;
    },

    __calculate_expression: function(expression, return_name, properties) {
        if (!properties) {
            properties = this.get_properties();
        }

        var i, result;

        if (expression.and) {
            result = true;

            for (i = 0; i < expression.and.length; i++) {
                if (!this.__calculate_expression(expression.and[i], false, properties)) {
                    result = false;
                    break;
                }
            }
        } else if (expression.or) {
            result = false;

            for (i = 0; i < expression.or.length; i++) {
                if (this.__calculate_expression(expression.or[i], false, properties)) {
                    result = true;
                    break;
                }
            }
        } else {
            result = false;

            var value = properties[expression.name];

            // We cannot just check for true-ness of expression.value, as it can be
            // literal 'false'.
            if ((typeof expression.value != "undefined") && (expression.value == value)) {
                result = true;
            }

            if (expression.value_set) {
                for (i = 0; i < expression.value_set.length; i++) {
                    if (expression.value_set[i] == value) {
                        result = true;
                        break;
                    }
                }
            }

            if (expression.value_regexp && expression.value_regexp.test("" + value)) {
                result = true;
            }

            if (expression.value_range && (value >= expression.value_range.min) &&
                    (value <= expression.value_range.max)) {

                result = true;
            }
        }

        if (!return_name) {
            return result;
        } else {
            return result ? expression[return_name] : null;
        }
    },

    __get_render_type: function(descriptor) {
        if (descriptor.render_type) {
            return descriptor.render_type;
        } else if (Ws.PropertiesSheet.Types[descriptor.type] &&
                Ws.PropertiesSheet.Types[descriptor.type].render_type) {

            return Ws.PropertiesSheet.Types[descriptor.type].render_type;
        }

        return null;
    },

    __hide: function(descriptor) {
        this.__correct_display(descriptor, "none", "hide");
    },

    __show: function(descriptor) {
        this.__correct_display(descriptor, "", "show");
    },

    __correct_display: function(descriptor, display_value, method_name) {
        if (descriptor[method_name]) {
            descriptor[method_name](this, descriptor);
        } else {
            switch (this.__get_render_type(descriptor)) {
                case Ws.PropertiesSheet.RENDER_ONE_COLUMN:
                    // We need to separately show table rows for the label
                    // and the input field, as they are on different rows.
                    $(this.__get_label_td_id(
                            descriptor.name)).parentNode.style.display = display_value;
                    $(this.__get_input_td_id(
                            descriptor.name)).parentNode.style.display = display_value;
                    break;
                case Ws.PropertiesSheet.RENDER_TWO_COLUMNS:
                    // Both the label and the input field are on the same
                    // row.
                    $(this.__get_label_td_id(
                            descriptor.name)).parentNode.style.display = display_value;
                    break;
                case Ws.PropertiesSheet.RENDER_CUSTOM:
                default:
                    Ws.PropertiesSheet.Types[descriptor.type][method_name](
                            this, descriptor);
                    break;
            }
        }
    },

    __copy_dynamic_changes: function(static_descriptor, changes) {
        var dynamic_descriptor = Ws.Utils.clone_object(static_descriptor);
        var dynamic_differs = false;

        for (var i in changes) {
            // Changing type and name is explicitly forbidden.
            if ((i != "type") && (i != "name")) {
                if (!Ws.Utils.equals(static_descriptor[i], changes[i])) {
                    dynamic_differs = true;
                }

                dynamic_descriptor[i] = changes[i];
            }
        }

        return {
            differs: dynamic_differs,
            descriptor: dynamic_descriptor
        }
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});

//----------------------------------------------------------------------------------------
Ws.PropertiesSheet.Types = {};

Ws.PropertiesSheet.ERROR = 500;
Ws.PropertiesSheet.WARNING = 300;
Ws.PropertiesSheet.INFO = 100;

Ws.PropertiesSheet.RENDER_TWO_COLUMNS = "two-columns";
Ws.PropertiesSheet.RENDER_ONE_COLUMN = "one-column";
Ws.PropertiesSheet.RENDER_CUSTOM = "custom";

Ws.PropertiesSheet.STRING = "string";
Ws.PropertiesSheet.INTEGER = "integer";
Ws.PropertiesSheet.TEXT = "text";
Ws.PropertiesSheet.CODE = "code";
Ws.PropertiesSheet.ENUM = "enum";
Ws.PropertiesSheet.BOOLEAN = "boolean";
Ws.PropertiesSheet.PASSWORD = "password";
Ws.PropertiesSheet.ARRAY = "array";
Ws.PropertiesSheet.TABLE = "table-properties";
Ws.PropertiesSheet.LABEL = "label";
Ws.PropertiesSheet.BUTTON = "button";

Ws.PropertiesSheet.DEFAULT_SECTION_DISPLAY_NAME = "";
Ws.PropertiesSheet.DEFAULT_SECTION_DESCRIPTION = "";

Ws.PropertiesSheet.EVENT_LAYOUT_CHANGED = "layout-changed";
Ws.PropertiesSheet.EVENT_VALIDATION_FINISHED = "validation-finished";

Ws.PropertiesSheet.DYNAMIC_VALUE_ALWAYS_KEEP = "always-keep";
Ws.PropertiesSheet.DYNAMIC_VALUE_ALWAYS_REPLACE = "always-replace";
Ws.PropertiesSheet.DYNAMIC_VALUE_REPLACE_DEFAULT = "replace-default"

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING] = {
    render_type: Ws.PropertiesSheet.RENDER_TWO_COLUMNS,

    render: function(psheet, descriptor) {
        var id = psheet.__get_input_id(descriptor);
        var classname = psheet.__get_input_class(descriptor);
        var html = "";

        if (descriptor.read_only) {
            html += "<div class=\"" + classname + "-read-only\">" + descriptor.value + "</div>";
        } else {
            html += "<div class=\"" + classname + "\">";
            html +=
                "<input type=\"text\" " +
                       "class=\"" + classname + "\" " +
                       "id=\"" + id + "\" " +
                        this.__render_change_trigger(psheet, descriptor) + " " +
                        this.__render_save_trigger(psheet, descriptor) + "/>";
            html += "</div>";
        }

        if (descriptor.units) {
            html += "<div class=\"" + classname + "-units\">" + descriptor.units + "</div>";
        }

        if (descriptor.inline_description) {
            html +=
                "<div class=\"" + classname + "-inline-description\">" +
                    descriptor.inline_description +
                "</div>";
        }

        return html;
    },

    set_value: function(psheet, descriptor) {
        if (!descriptor.read_only) {
            $(psheet.__get_input_id(descriptor)).value = descriptor.value;
        }
    },

    get_value: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return descriptor.value;
        } else {
            return $(psheet.__get_input_id(descriptor)).value;
        }
    },

    validate: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return []; // TODO: There is nothing to validate if we're read-only, or not?
        } else {
            var results = [];

            if (descriptor.validation) {
                var value = this.get_value(psheet, descriptor);

                if (descriptor.validation.regexp) {
                    if (!descriptor.validation.regexp.test(value)) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " does not " +
                                    "match the regular expression: " +
                                    descriptor.validation.regexp
                        });
                    }
                }

                if (descriptor.validation.not_empty) {
                    if (value.length == 0) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be empty."
                        });
                    }
                }

                if (descriptor.validation.max_length) {
                    if (value.length > descriptor.validation.max_length) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot exceed " +
                                descriptor.validation.max_length + " characters."
                        });
                    }
                }

                if (descriptor.validation.min_length) {
                    if (value.length < descriptor.validation.min_length) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be less than " +
                                descriptor.validation.min_length + " characters."
                        });
                    }
                }

                this.__render_validation_results(psheet, descriptor, results);
            }

            return results;
        }
    },

    __render_change_trigger: function(psheet, descriptor) {
        var onkeyup = "onkeyup=\"";
        var onblur = "onblur=\"";
    
        if (descriptor.validation) {
            var validation_code = "$('" + psheet.element.id + "').ws_psheet.validate();";
            if (descriptor.validation.trigger == "key") {
                onkeyup += validation_code;
            } else if (descriptor.validation.trigger == "focus") {
                onblur += validation_code;
            }
        }
        
        onkeyup += "$('" + psheet.element.id + "').ws_psheet.update_depends();";
        onkeyup += "$('" + psheet.element.id + "').ws_psheet.update_dynamic();";
        
        return onkeyup + "\" " + onblur + "\"";
    },

    __render_validation_results: function(psheet, descriptor, results) {
        var validation_errors = false;
        var validation_warnings = false;

        for (var i = 0; i < results.length; i++) {
            if (results[i].severity == Ws.PropertiesSheet.ERROR) {
                validation_errors = true;
                break;
            } else if (results[i].severity == Ws.PropertiesSheet.WARNING) {
                validation_warnings = true;

                // We do not break here, as there can be errors in other results.
            }
        }

        if (validation_errors) {
            $(psheet.__get_input_id(descriptor)).setStyle({
                color: "red"
            });
        } else if (validation_warnings) {
            $(psheet.__get_input_id(descriptor)).setStyle({
                color: "yellow"
            });
        } else {
            $(psheet.__get_input_id(descriptor)).setStyle({
                color: ""
            });
        }
    },

    __render_save_trigger: function(psheet, descriptor) {
        if (!descriptor.skip_save_on_enter) {
            return "onkeypress=\"if (event.keyCode == 13) { $('" + psheet.element.id + "').ws_psheet.save(); }\"";
        } else {
            return "";
        }
    },

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.INTEGER] = {
    render_type: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].render_type,

    render: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].render,

    set_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].set_value,

    get_value: function(psheet, descriptor) {
        return parseInt(this._get_raw_value(psheet, descriptor));
    },

    validate: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return []; // TODO: There is nothing to validate if we're read-only, or not?
        } else {
            var results = [];

            if (descriptor.validation) {
                var value = this._get_raw_value(psheet, descriptor);

                // Built-in check for the value being an integer
                if (!(/^-?([0-9]|[1-9][0-9]+)$/.test(value))) {
                    results.push({
                        severity: Ws.PropertiesSheet.ERROR,
                        message: descriptor.display_name + " is not an integer."
                    });
                } else {
                    if (descriptor.validation.maximum) {
                        if (parseInt(value) > descriptor.validation.maximum) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " cannot be greater " +
                                    "than " + descriptor.validation.maximum + "."
                            });
                        }
                    }

                    if (descriptor.validation.minimum) {
                        if (parseInt(value) < descriptor.validation.minimum) {
                            results.push({
                                severity: Ws.PropertiesSheet.ERROR,
                                message: descriptor.display_name + " cannot be less " +
                                    "than " + descriptor.validation.maximum + "."
                            });
                        }
                    }
                }

                this.__render_validation_results(psheet, descriptor, results);
            }

            return results;
        }

    },

    _get_input_id: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_id,

    _get_input_class: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_class,

    __render_change_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_change_trigger,

    __render_validation_results: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_validation_results,

    __render_save_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_save_trigger,

    _get_raw_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].get_value,

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT] = {
    render_type: Ws.PropertiesSheet.RENDER_ONE_COLUMN,

    render: function(psheet, descriptor) {
        var classname = psheet.__get_input_class(descriptor);
        var id = psheet.__get_input_id(descriptor);
        var maximized_classname =
                psheet.classname_type_prefix + "maximized-" + descriptor.type;

        var html = "<div class=\"" + classname + "\">";

        var height = descriptor.initial_height ?
                " style=\"height: " + descriptor.initial_height + "px\"" : ""

        html += "<textarea class=\"" + classname + "\" " +
        "id=\"" + id + "\" ";
        var _read_only;
        if (descriptor.read_only) {
            html += "readonly=\"readonly\"" + height + ">" + descriptor.value + "</textarea>";
            _read_only = true;
        } else {
            html += this.__render_change_trigger(psheet, descriptor) + "" +
            height + "></textarea>";
            _read_only = false;
        }

        if (descriptor.maximizable) {
            html +=
            '<div class="' + classname + '-menu" >' +
            '<a href="javascript: Ws.PropertiesSheet.maximize(\'' + id + '\', \'' 
            + maximized_classname + '\',' + _read_only +
            ',';
            if (descriptor.code_type) {
              html += "'" + descriptor.code_type + "'" ;
            } else {
              html +=  false;
            }

             html +=   ')">Maximize</a>' +
            '</div>';
        }

        html += "</div>";

        if (descriptor.inline_description) {
            html +=
                "<div class=\"" + classname + "-inline-description\">" +
                    descriptor.inline_description +
                "</div>";
        }
        
        return html;
    },

    set_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].set_value,

    get_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].get_value,

    layout: function(psheet, descriptor) {
        var element = $(psheet.__get_input_id(descriptor));

        if (descriptor.auto_adjust_height) {
                var table_candidates =
                        psheet.element.select("." + psheet.classname_table);

                var table = null;
                for (var i = 0; i < table_candidates.length; i++) {
                    if (element.descendantOf(table_candidates[i])) {
                        table = table_candidates[i];
                        break;
                    }
                }
                
                var table_parent = Element.extend(table.parentNode);

                var table_bmp = Ws.Utils.get_bmp(table);
                var element_bmp = Ws.Utils.get_bmp(element);

                var parent_height = parseInt(table_parent.getStyle("height"));
                    //Ws.Utils.get_bmp(table_parent).get_padding_vertical();
                var table_height = Element.getHeight(table) + table_bmp.get_margin_vertical();
                var element_height = Element.getHeight(element) -
                        element_bmp.get_bp_vertical();

                var d_height = parent_height - table_height;

                var effective_height = element_height + d_height;

                if (!descriptor.auto_min_height || (descriptor.auto_min_height <= 0)) {
                    descriptor.auto_min_height = 50;
                }

                if (effective_height < descriptor.auto_min_height) {
                    effective_height = descriptor.auto_min_height;
                }

                element.setStyle({
                    height: (effective_height - 2) + "px"
                });
                return d_height;
            }
            return null;
    },

    validate: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].validate,

    _get_input_id: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_id,

    _get_input_class: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_class,

    __render_change_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_change_trigger,

    __render_validation_results: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_validation_results,

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.CODE] = {
    render_type: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].render_type,

    render: function(psheet, descriptor) {
        return Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].render(psheet, descriptor);
    },

    set_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].set_value,

    get_value: function(psheet, descriptor) {
        var frame = $("frame_" + psheet.__get_input_id(descriptor));
        if (frame) {
            var textarea = frame.contentDocument.getElementById("textarea");
            if (textarea) {
                return textarea.value;
            }
        }
        return Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].get_value(psheet, descriptor);
    },

    layout: function(psheet, descriptor) {
        var d_height = Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].
        layout(psheet, descriptor);

        if (descriptor.auto_adjust_height) {
            var element = $("frame_" + psheet.__get_input_id(descriptor));

            if (element && element != null) {
                var element_height = parseInt(element.style.height);
                var effective_height = element_height + d_height;

                if (!descriptor.auto_min_height || (descriptor.auto_min_height <= 0)) {
                    descriptor.auto_min_height = 50;
                }

                if (effective_height < descriptor.auto_min_height) {
                    effective_height = descriptor.auto_min_height;
                }
                element.style.height = effective_height + "px";
            }
        }
    },

    validate: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].validate,

    _get_input_id: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT]._get_input_id,

    _get_input_class: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT]._get_input_class,

    __render_change_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].__render_change_trigger,

    __render_validation_results: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].__render_validation_results,

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.ENUM] = {
    // Note: There is not much validation I can think of for an enum property
    // represented by a selectbox. Thus this property type does not support validation
    // altogether.

    render_type: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].render_type,

    render: function(psheet, descriptor) {
        var html = "<div>";

        if (descriptor.read_only) {
            html += "<div class=\"read-only\">" + descriptor.value + "</div>";
        } else {
            html +=
                "<select class=\"" + psheet.__get_input_class(descriptor) + "\" " +
                        "id=\"" + psheet.__get_input_id(descriptor) + "\" " +
                        this.__render_change_trigger(psheet, descriptor) + "></select>";
        }

        html += "</div>";

        return html;
    },

    set_value: function(psheet, descriptor) {
        if (!descriptor.read_only) {
            var element = $(psheet.__get_input_id(descriptor));

            while (element.hasChildNodes()) {
                element.removeChild(element.firstChild);
            }

            if (descriptor.values) {
                var inner_html = "";

                descriptor.values.each(function(value) {
                    inner_html += "<option value=\"" + value.value +
                        "\" title=\"" + value.description + "\"" +
                        (value.value == descriptor.value ? " selected=\"selected\"" : "") + ">" +
                        value.display_name + "</option>";
                });

                element.innerHTML = inner_html;
            }
        }
    },

    get_value: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return descriptor.value;
        } else {
            var element = $(psheet.__get_input_id(descriptor));

            if (element.options.length == 0) {
                return null;
            } else {
                return element.options[element.selectedIndex].value;
            }
        }
    },

    layout: function(psheet, descriptor) {
        var element = $(psheet.__get_input_id(descriptor));

        if (descriptor.style) {
            element.setStyle(descriptor.style);
        }
    },

    __render_change_trigger: function(psheet, descriptor) {
        var onchange = "onchange=\"";

        onchange += "$('" + psheet.element.id + "').ws_psheet.update_depends();";
        onchange += "$('" + psheet.element.id + "').ws_psheet.update_dynamic();";

        onchange += "\"";

        return onchange;
    },

    _get_input_id: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_id,

    _get_input_class: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_class,

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.BOOLEAN] = {
    // Note: There is not much validation I can think of for a boolean property
    // represented by a checkbox. Thus this property type does not support validation
    // altogether.

    render_type: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].render_type,

    render: function(psheet, descriptor) {
        var html = "<div>";

        var read_only_code = descriptor.read_only ? "readonly=\"readonly\"" : "";

        html +=
            "<input type=\"checkbox\" " +
                   "class=\"" + psheet.__get_input_class(descriptor) + "\" " +
                   "id=\"" + psheet.__get_input_id(descriptor) + "\" " +
                    this.__render_change_trigger(psheet, descriptor) + " " +
                    read_only_code + "/>";

        html += "</div>";

        return html;
    },

    set_value: function(psheet, descriptor) {
        if (!descriptor.read_only) {
            if (descriptor.value) {
                $(psheet.__get_input_id(descriptor)).checked = true;
            } else {
                $(psheet.__get_input_id(descriptor)).checked = false;
            }
        }
    },

    get_value: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return descriptor.value;
        } else {
            return $(psheet.__get_input_id(descriptor)).checked;
        }
    },

    __render_change_trigger: function(psheet, descriptor) {
        var onchange = "onchange=\"";

        onchange += "$('" + psheet.element.id + "').ws_psheet.update_depends();";
        onchange += "$('" + psheet.element.id + "').ws_psheet.update_dynamic();";

        onchange += "\"";

        return onchange;
    },

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.PASSWORD] = {
    render_type: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].render_type,

    render: function(psheet, descriptor) {
        var html = "<div class=\"" + psheet.__get_input_class(descriptor) + "\">";

        if (descriptor.read_only) {
            html += "<div class=\"read-only\">" + "XXX" + "</div>";
        } else {
            html +=
                "<input type=\"password\" " +
                       "class=\"" + psheet.__get_input_class(descriptor) + "\" " +
                       "id=\"" + psheet.__get_input_id(descriptor) + "\" " +
                        this.__render_change_trigger(psheet, descriptor) + " " +
                        this.__render_save_trigger(psheet, descriptor) + "/>";
        }

        html += "</div>";

        return html;
    },

    set_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].set_value,

    get_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].get_value,

    validate: function(psheet, descriptor) {
        if (descriptor.read_only) {
            return []; // TODO: There is nothing to validate if we're read-only, or not?
        } else {
            var results = [];

            if (descriptor.validation) {
                var value = this.get_value(psheet, descriptor);

                if (descriptor.validation.not_empty) {
                    if (value.length == 0) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be empty."
                        });
                    }
                }

                if (descriptor.validation.max_length) {
                    if (value.length > descriptor.validation.max_length) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be exceed " +
                                descriptor.validation.max_length + " characters."
                        });
                    }
                }

                if (descriptor.validation.min_length) {
                    if (value.length < descriptor.validation.min_length) {
                        results.push({
                            severity: Ws.PropertiesSheet.ERROR,
                            message: descriptor.display_name + " cannot be less than " +
                                descriptor.validation.min_length + " characters."
                        });
                    }
                }

                this.__render_validation_results(psheet, descriptor, results);
            }

            return results;
        }
    },

    __render_change_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_change_trigger,

    __render_validation_results: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_validation_results,

    __render_save_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].__render_save_trigger,

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.ARRAY] = {
    render_type: Ws.PropertiesSheet.RENDER_CUSTOM,
    
    render: function(psheet, descriptor) {
        var i, html = "";
        
        for (i = 0; i < descriptor.value.length; i++) {
            html += "<tr>" + this.__render_item(psheet, descriptor, i) + "</tr>";
        }
        
        return html;
    },

    __render_item: function(psheet, descriptor, i) {
        var html = "";
        var index = i + 1;
        var display_name = descriptor.display_name + descriptor.display_name_suffix.replace("%i", "" + index);
        var description = descriptor.description + descriptor.description_suffix.replace("%i", "" + index);
        var name = descriptor.name + "----" + index;
        var input_html;
        
        var validation = null;
        if (descriptor.validation) {
            if (descriptor.items_type) {
                validation = descriptor.validation;
            } else {
                validation = descriptor.validation[i];
            }
        }

        if (descriptor.items_type) {
            input_html = Ws.PropertiesSheet.Types[descriptor.items_type].render(
                psheet,
                {
                    name: name,
                    type: descriptor.items_type,
                    value: descriptor.value[i],

                    read_only: descriptor.read_only,

                    validation: validation
                }
            );
        } else {
            input_html = Ws.PropertiesSheet.Types[descriptor.types[i]].render(
                psheet,
                {
                    name: name,
                    type: descriptor.types[i],
                    value: descriptor.value[i],

                    read_only: descriptor.read_only,

                    validation: validation
                }
            );
        }

        html += "<td class=\"" + psheet.classname_label + "\" " +
                        "title=\"" + description + "\" " +
                        "id=\"" + psheet.__get_label_td_id(name) + "\">" + display_name + ":</td>" +
                "<td class=\"" + psheet.classname_input + "\"        " +
                        "id=\"" + psheet.__get_input_td_id(name) + "\">" + input_html + "</td>";

        return html;
    },

    rerender: function(psheet, old_descriptor, new_descriptor) {
        var i;

        var element;
        var parentTr, parentTable, nextSiblingTr;

        for (i = 0; ; i++) {
            var index = i + 1;
            var name = old_descriptor.name + "----" + index;
            var id = psheet.__get_label_td_id(name);

            element = $(id);

            if (element == null) {
                break;
            }

            parentTr = element.parentNode;
            parentTable = parentTr.parentNode;
            nextSiblingTr = parentTr.nextSibling;
            
            parentTable.removeChild(parentTr);
        }

        for (i = 0; i < new_descriptor.value.length; i++) {
            element = new Element("tr");
            var html = this.__render_item(psheet, new_descriptor, i);

            if (nextSiblingTr != null) {
                parentTable.insertBefore(element, nextSiblingTr);
            } else {
                parentTable.appendChild(element);
            }
            
            element.innerHTML = html;
        }
    },

    set_value: function(psheet, descriptor) {
        var i;
        
        if (!descriptor.read_only) {
            for (i = 0; i < descriptor.value.length; i++) {
                var index = i + 1;
                var name = descriptor.name + "----" + index;
                var type;
                
                if (descriptor.items_type) {
                    type = descriptor.items_type;
                } else {
                    type = descriptor.types[i];
                }

                var item_descriptor = {
                    name: name,
                    type: type,
                    value: descriptor.value[i]
                }

                // Check whether the input for this index exists. IF it does not, we
                // shan't set the value.
                if (!$(psheet.__get_input_id(item_descriptor))) {
                    break;
                }
                
                Ws.PropertiesSheet.Types[type].set_value(
                    psheet,
                    item_descriptor
                );
            }
        }
    },

    get_value: function(psheet, descriptor) {
        var i, value = [];
        
        if (descriptor.read_only) {
            return descriptor.value;
        } else {
            for (i = 0; i < descriptor.value.length; i++) {
                var index = i + 1;
                var name = descriptor.name + "----" + index;
                var type;
                var current_value;
                
                if (descriptor.items_type) {
                    type = descriptor.items_type;
                } else {
                    type = descriptor.types[i];
                }
                
                current_value = Ws.PropertiesSheet.Types[type].get_value(
                    psheet,
                    {
                        name: name,
                        type: type
                    }
                );
                
                value.push(current_value);
            }
        }

        return value;
    },

    validate: function(psheet, descriptor) {
        var i, results = [];

            for (i = 0; i < descriptor.value.length; i++) {
                var index = i + 1;
                var name = descriptor.name + "----" + index;
                var display_name = descriptor.display_name + descriptor.display_name_suffix.replace("%i", "" + index);
                var description = descriptor.description + descriptor.description_suffix.replace("%i", "" + index);

                var type, validation;

                if (descriptor.items_type) {
                    type = descriptor.items_type;
                    if (descriptor.validation) {
                        validation = descriptor.validation;
                    }
                } else {
                    type = descriptor.types[i];
                    if (descriptor.validation) {
                        validation = descriptor.validation[i];
                    }
                }

                var current_results = Ws.PropertiesSheet.Types[type].validate(
                    psheet,
                    {
                        name: name,
                        type: type,

                        display_name: display_name,
                        description: description,

                        validation: validation
                    }
                );

                results = results.concat(current_results);
            }

        return results;
    },

    hide: function(psheet, descriptor) {
        for (var i = 0; i < descriptor.value.length; i++) {
            var name = descriptor.name + "----" + (i + 1);
            
            $(psheet.__get_label_td_id(name)).parentNode.style.display = "none";
        }
    },
    
    show: function(psheet, descriptor) {
        for (var i = 0; i < descriptor.value.length; i++) {
            var name = descriptor.name + "----" + (i + 1);
            
            $(psheet.__get_label_td_id(name)).parentNode.style.display = "";
        }
    },
    
    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.LABEL] = {
    // Labels are read-only by definition and thus no validation is applicable.

    render_type: Ws.PropertiesSheet.RENDER_CUSTOM,

    render: function(psheet, descriptor) {
        var html = "";

        var id = psheet.__get_label_td_id(descriptor.name);
        var classname = psheet.classname_type_prefix + descriptor.type;

        html +=
                "<tr>" +
                    "<td id=\"" + id + "\" colspan=\"2\">" +
                        "<div class=\"" + classname + "\" title=\"" + descriptor.description + "\">" + descriptor.display_name + "</div>" +
                    "</td>" +
                "</tr>";

        return html;
    },

    rerender: function(psheet, old_descriptor, new_descriptor) {
        var element = psheet.__get_label_td_id(old_descriptor.name);

        element.id = psheet.__get_label_td_id(new_descriptor.name);
        element.firstChild.innerHTML = new_descriptor.display_name;
        element.firstChild.title = new_descriptor.description;
    },

    set_value: function(psheet, descriptor) {
        // Labels are read-only, we cannot set value for them.
    },

    get_value: function(psheet, descriptor) {
        return "";
    },

    hide: function(psheet, descriptor) {
        var element = $(psheet.__get_label_td_id(descriptor.name));
        var parent = Element.extend(element.parentNode);

        parent.style.display = "none";
    },

    show: function(psheet, descriptor) {
        var element = $(psheet.__get_label_td_id(descriptor.name));
        var parent = Element.extend(element.parentNode);

        parent.style.display = "";
    },

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.BUTTON] = {
    // Buttons are read-only by definition and thus no validation is applicable.

    render_type: Ws.PropertiesSheet.RENDER_CUSTOM,

    render: function(psheet, descriptor) {
        var html = "";

        var id = psheet.__get_label_td_id(descriptor.name);
        var classname = psheet.classname_type_prefix + descriptor.type;

        html +=
                "<tr>" +
                    "<td id=\"" + id + "\" colspan=\"2\">" +
                        "<div class=\"" + classname + "\">" +
                            "<input type=\"button\" class=\"" + classname + "\" value=\"" + descriptor.display_name + "\" title=\"" + descriptor.description + "\"/>" +
                         "</div>" +
                    "</td>" +
                "</tr>";

        return html;
    },

    layout: function(psheet, descriptor) {
        if (descriptor.action) {
            var td_element = $(psheet.__get_label_td_id(descriptor.name));
            var input_element = td_element.firstChild.firstChild;

            input_element.onclick = function() {
                descriptor.action(psheet, descriptor);
            }
        }
    },

    rerender: function(psheet, old_descriptor, new_descriptor) {
        var element = psheet.__get_label_td_id(old_descriptor.name);

        element.id = psheet.__get_label_td_id(new_descriptor.name);
        element.firstChild.firstChild.value = new_descriptor.display_name;
        element.firstChild.firstChild.title = new_descriptor.description;
    },

    set_value: function(psheet, descriptor) {
        // Buttons are read-only, we cannot set value for them.
    },

    get_value: function(psheet, descriptor) {
        return "";
    },

    hide: function(psheet, descriptor) {
        var element = $(psheet.__get_label_td_id(descriptor.name));
        var parent = Element.extend(element.parentNode);

        parent.style.display = "none";
    },

    show: function(psheet, descriptor) {
        var element = $(psheet.__get_label_td_id(descriptor.name));
        var parent = Element.extend(element.parentNode);

        parent.style.display = "";
    },

    noop: function() {
        // Does nothing
    }
}

Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TABLE] = {
    render_type: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.TEXT].render_type,

    render: function(psheet, descriptor) {
        var i, j;

        var html = "<table " +
            "class=\"" + psheet.__get_input_class(descriptor) + "\" " +
            "id=\"" + psheet.__get_input_id(descriptor) + "\">";

        if (descriptor.value.body_caption) {
            html += "<thead>";
            for (i = 0; i < descriptor.value.body_caption.length; i++) {
                html += "<td>" + descriptor.value.body_caption[i].value + "</td>";
            }

            html += "</thead>";
        }

        html += "<tbody>";
        for (i = 0; i < descriptor.value.body_value.length; i++) {
            html += "<tr>";

            for (j = 0; j < descriptor.value.body_value[i].length; j++) {
                var body_value = descriptor.value.body_value[i][j];

                html += "<td><div class=\"layout\">" + body_value.value;
                if (body_value.functions_change && psheet.functions_change && psheet.functions_change.length > 0) {
                    for (var k = 0; k < psheet.functions_change.length; k++) {
                        html += "<div type=\"button\" class=\"button-" + psheet.functions_change[k].type + "\"" +
                            "style=\"right: "+ 16 * k + "px\"" +
                            "onclick=\"$('" + psheet.element.id + "').ws_psheet.functions_change["+ k +"].funct(" +
                            (body_value.functions_change_params ? body_value.functions_change_params: "") + ")\"></div>";
                    }
                }

                html +="</div></td>"
            }

            html += "</tr>";
        }

        html += "</tbody>";

        html += "</table>";

        return html;
    },

    set_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].set_value,

    get_value: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING].get_value,

    validate: function(psheet, descriptor) {
        //TODO
        return [];
    },

//    _get_input_id: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_id,
//
//    _get_input_class: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._get_input_class,
//
//    _render_change_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._render_change_trigger,
//
//    _render_validation_results: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._render_validation_results,
//
//    _render_save_trigger: Ws.PropertiesSheet.Types[Ws.PropertiesSheet.STRING]._render_save_trigger,

    noop: function() {
        // Does nothing
    }
}

//----------------------------------------------------------------------------------------
Ws.PropertiesSheet.counter = 0;

Ws.PropertiesSheet.maximize = function(id, className, read_only, code_type) {
    var textArea;

    var input = (!code_type) ? $(id) :
        $("frame_" + id).contentDocument.getElementById("textarea");

    if (input.ws_mwindow) {
        textArea = input.ws_mwindow.window_element.select("." + className).reduce();
        textArea.value = input.value;
        input.ws_mwindow.show();
        if (code_type) {
            editAreaLoader.init({
                id : textArea.id,
                min_height : 5,
                min_width: 5,
                syntax: "" + code_type,
                toolbar: "",
                start_highlight: true,
                allow_toggle: false,
                allow_resize: "no"
            });
            editAreaLoader.setValue(textArea.id, input.value);
        }
    } else {
        var window_wh = Ws.Utils.get_window_dimensions();

        var element = new Element("div");
        element.setStyle({
            position: "relative",
            width: (window_wh.width - 50) + "px",
            height: (window_wh.height - 50) + "px",
            padding: "2px 2px 2px 2px"
        });

        textArea = new Element("textarea");
        textArea.addClassName(className);
        textArea.id = id + "m";
        if (read_only) {
            textArea.setAttribute("readonly", "readonly");
        }
        textArea.setStyle({
            display: "block",
            width: (window_wh.width - 73) + "px",
            height: (window_wh.height - 98) + "px",
            margin: "3px 3px 0px 3px"
        });
        textArea.value = input.value;

        var buttonsDiv = new Element("div");
        buttonsDiv.setStyle({
            height: "20px",
            margin: "5px 3px 0px 3px",
            textAlign: "right"
        });

        if (!read_only) {
            var cancelButton = new Element("input");
            cancelButton.setAttribute("type", "button");
            cancelButton.value = "Cancel";
            cancelButton.setStyle({
                marginRight: "10px"
            });
            cancelButton.onclick = function() {
                input.ws_mwindow.close();
            };

            var saveButton = new Element("input");
            saveButton.setAttribute("type", "button");
            saveButton.value = "Save";
            saveButton.onclick = function() {
                if (code_type) {
                    editAreaLoader.setValue(id, editAreaLoader.getValue(id + "m"));
                } else {
                    input.value = textArea.value;
                }
                input.ws_mwindow.close();
            };

            buttonsDiv.appendChild(cancelButton);
            buttonsDiv.appendChild(saveButton);
        } else {
            var closeButton = new Element("input");
            closeButton.setAttribute("type", "button");
            closeButton.value = "Close";
            closeButton.setStyle({
                marginRight: "10px"
            });
            closeButton.onclick = function() {
                input.ws_mwindow.close();
            };

            buttonsDiv.appendChild(closeButton);
        }
        
        element.appendChild(textArea);
        element.appendChild(buttonsDiv);
        document.body.appendChild(element);

        new Ws.ModalWindow(element).show();
        if (code_type) {
            editAreaLoader.init({
                id : textArea.id,
                min_height : 5,
                min_width: 5,
                syntax: "" + code_type,
                toolbar: "",
                start_highlight: true,
                allow_toggle: false,
                allow_resize: "no"
            });
        
        }

        input.ws_mwindow = element.ws_mwindow;
    }
}