/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.ModalPrompt = Class.create({
    type: null,
    options: {},

    mwindow: null,
    mwindow_options: null,

    initialize: function(type, options, mwindow_options) {
        if (options) {
            this.options = options;
        }

        this.type = type;
        this.mwindow_options = mwindow_options;
        this.reset();
    },

    show: function() {
        if (this.mwindow) {
            this.mwindow.show();
        } else {
            throw "Operation not permitted -- the prompt has not " +
                    "yet been initialized or has been removed."
        }
    },

    close: function() {
        if (this.mwindow) {
            this.mwindow.close();
        } else {
            throw "Operation not permitted -- the prompt has not " +
                    "yet been initialized or has been removed."
        }
    },

    remove: function() {
        if (this.mwindow) {
            this.mwindow.remove();
        } else {
            throw "Operation not permitted -- the prompt has not " +
                    "yet been initialized or has been removed."
        }
    },

    reset: function() {
        if (this.mwindow) {
            this.mwindow.remove();
        }

        var handler = Ws.ModalPrompt.handlers[this.type];

        if (!handler) {
            throw "Unsupported modal prompt type.";
        }

        var element = handler.build(this);
        element.ws_mprompt = this;

        this.mwindow = new Ws.ModalWindow(element, this.mwindow_options);
        handler.decorate(this, element);
        this.mwindow.close(true);
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});

//----------------------------------------------------------------------------------------
Ws.ModalPrompt.TYPE_OK = "ok";
Ws.ModalPrompt.TYPE_RR_OK = "rr-ok";

Ws.ModalPrompt.last_id = 0;
Ws.ModalPrompt.handlers = {};
Ws.ModalPrompt.handlers[Ws.ModalPrompt.TYPE_OK] = {
    build: function(prompt) {
        var id = "ws-modal-prompt-" + Ws.ModalPrompt.last_id++;

        if (!prompt.options.ok_handler) {
            prompt.options.ok_handler = function() {
                prompt.close();
            };
        }

        if (!prompt.options.ok_text) {
            prompt.options.ok_text = "OK";
        }

        var element = new Element("div");
        element.id = id;
        element.addClassName("ws-modal-prompt");
        element.addClassName("wsmp-" + Ws.ModalPrompt.TYPE_OK);
        element.innerHTML =
                "<div class=\"ok-text\">" +
                    prompt.options.text +
                "</div>" +
                "<input class=\"ok-button\" type=\"button\" " +
                    "value=\"" + prompt.options.ok_text + "\" " +
                    "onclick=\"$('" + id + "').ws_mprompt.options.ok_handler();\"/>";

        document.body.appendChild(element);
        
        return element;
    },

    decorate: function(prompt, element) {
        // Does nothing.
    }
}

Ws.ModalPrompt.handlers[Ws.ModalPrompt.TYPE_RR_OK] = {
    build: function(prompt) {
        var element =
                Ws.ModalPrompt.handlers[Ws.ModalPrompt.TYPE_OK].build(prompt);
        element.removeClassName("wsmp-" + Ws.ModalPrompt.TYPE_OK);
        element.addClassName("wsmp-" + Ws.ModalPrompt.TYPE_RR_OK);

        return element;
    },

    decorate: function(prompt, element) {
        if (!prompt.options.rr_options) {
            prompt.options.rr_options = {
                fill: "rgb(128, 128, 128)"
            };
        }

        new Ws.RoundedRectangle(element, prompt.options.rr_options);
    }
}
