/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

if (typeof Ws.Graph == "undefined") {
    Ws.Graph = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
/**
 * Base class for logical node representation. Essentially a logical node model,
 * encapsulates all node data not related (or not dependent on) node's visual
 * appearance.
 */
Ws.Graph.DefaultNode = Class.create({
    id: 0,

    shortcut_to: null,
    parent: null,
    propagation_exceptions: null,
    propagation_ui_exceptions: null,

    properties: null,
    ui_properties: null,

    pre_add_hook: null,
    post_add_hook: null,
    pre_remove_hook: null,
    post_remove_hook: null,

    /**
     * Consructs a new DefaultNode object. The constructor initializes the object fields
     * based on the data supplied by the caller. Additionally, unless the user specified
     * otherwise, the UI of the node is initialized.
     */
    initialize: function(data, ui_options, skip_ui_init) {
        // We cannot simply use 'Ws.Utils.apply_defaults(this, data)', or
        // 'Object.extend(this, data)' as some of the fields ('properties',
        // 'ui_properties') already have non-null default values, and since we need to
        // clone the 'properties' and 'ui_properties' fields of 'data' to have new
        // objects.
        if (data.id) {
            this.id = data.id;
        }

        // It is important that we initialize the properties and ui_properties with their
        // default values here, in constructor. This allows us to have a different object
        // for each DefaultNode instance.
        if (data.properties) {
            this.properties = Ws.Utils.clone_object(data.properties);
        } else {
            this.properties = {};
        }
        if (data.ui_properties) {
            this.ui_properties = Ws.Utils.clone_object(data.ui_properties);
        } else {
            this.ui_properties = {};
        }
        if (data.ui_addenda) {
            this.ui_addenda = Ws.Utils.clone_object(data.ui_addenda);
        } else {
            this.ui_addenda = {};
        }

        this.propagation_exceptions = [];
        if (data.propagation_exceptions) {
            for (var i = 0; i < data.propagation_exceptions.length; i++) {
                this.propagation_exceptions[i] = data.propagation_exceptions[i];
            }
        }

        if (data.propagation_ui_exceptions) {
            this.propagation_ui_exceptions = [];

            for (i = 0; i < data.propagation_ui_exceptions.length; i++) {
                this.propagation_ui_exceptions[i] = data.propagation_ui_exceptions[i];
            }
        } else {
            this.propagation_ui_exceptions = [
                "x",
                "y"
            ];
        }

        if (data.shortcut_to) {
            this.shortcut_to = data.shortcut_to;

            // If we're a shortcut, we need to copy all properties and ui properties
            // from the "target" node except those listed as exceptions.
            for (i in this.shortcut_to.properties) {
                if (this.propagation_exceptions.indexOf(i) == -1) {
                    this.properties[i] = this.shortcut_to.properties[i];
                }
            }

            for (i in this.shortcut_to.ui_properties) {
                if (this.propagation_ui_exceptions.indexOf(i) == -1) {
                    this.ui_properties[i] = this.shortcut_to.ui_properties[i];
                }
            }
        }

        if (data.parent) {
            this.parent = data.parent;
        }

        // We can simply copy the hooks from the data object even if it does not contain
        // them -- the default values are 'null' anyway.
        this.pre_add_hook = data.pre_add_hook;
        this.post_add_hook = data.post_add_hook;
        this.pre_remove_hook = data.pre_remove_hook;
        this.post_remove_hook = data.post_remove_hook;

        // Initialize the UI, unless we were told not to.
        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                Ws.Graph.DEFAULT_NODE_TYPE,
                this,
                ui_options
            );
        }
    },

    // Connectors ------------------------------------------------------------------------
    /**
     * This method return the logical metadata about the available inbound connectors to
     * this node. For a very simple node the return value would be just an array of empty
     * objects, i.e. the only metadata returned will be the actual number of connectors.
     * In more complex cases, the objects in the connectors array will not be empty, but
     * will instead contain some metadata specific to a particular connector: supported
     * link types, maximum number of links for the connector, etc.
     *
     * If you're looking for the UI data of the connector, you should be calling the
     * DefaultNodeUi#get_inbound_connectors method. It will return te same array, but
     * enriched with UI data, such as coordinates, connector type, etc.
     *
     * @return Node's inbound connectors metadata.
     */
    get_inbound_connectors: function() {
        return [
            {
                
            },
            {
                
            }
        ];
    },

    get_inbound_connector: function(index) {
        return this.get_inbound_connectors()[index];
    },

    /**
     * This method return the logical metadata about the available outbound connectors to
     * this node. See also #get_inbound_connectors for a more detailed information on the
     * data being returned.
     *
     * @return Node's outbound connectors metadata.
     */
    get_outbound_connectors: function() {
        return [
            {

            },
            {

            }
        ];
    },

    get_outbound_connector: function(index) {
        return this.get_outbound_connectors()[index];
    },

    // Queries ---------------------------------------------------------------------------
    get_links: function() {
        return this.graph.get_links_by_node(this);
    },

    get_inbound_links: function(connector_index) {
        var links = this.graph.get_inbound_links_by_node(this);

        if (typeof connector_index == "undefined") {
            return links;
        } else {
            var filtered = [];
            for (var i = 0; i < links.length; i++) {
                if (links[i].end_node_connector == connector_index) {
                    filtered.push(links[i]);
                }
            }

            return filtered;
        }
    },

    get_outbound_links: function(connector_index) {
        var links = this.graph.get_outbound_links_by_node(this);

        if (typeof connector_index == "undefined") {
            return links;
        } else {
            var filtered = [];
            for (var i = 0; i < links.length; i++) {
                if (links[i].start_node_connector == connector_index) {
                    filtered.push(links[i]);
                }
            }

            return filtered;
        }
    },

    // Properties and UI properties ------------------------------------------------------
    get_property: function(name) {
        return this.properties[name];
    },

    set_property: function(name, new_value, silent, part_of_batch) {
        var result = {
            update_descriptor: null,
            propagate: false
        }

        if (this.properties[name] != new_value) {
            var old_value = this.properties[name];

            // The new value does not match the old one, so we populate the
            // update_descriptor property of the result object.
            result.update_descriptor = {
                name: name,
                old_value: old_value,
                new_value: new_value
            }

            // Apply the new value.
            this.properties[name] = new_value;

            // If the property being set is not on the propagation exception list, we
            // need to perform a couple of checks for possible updates to either "parent"
            // or shortcuts.
            if (this.propagation_exceptions.indexOf(name) == -1) {
                // If this node is a shortcut, we'll call the "parent" node's API to set
                // the property there as well and force us to be silent (i.e. not
                // generate an event). However, if this call is part of a batch (see
                // set_properties), skip all of this, but always set the propagate flag
                // on the result object. Also, reset the properties sheet of the node
                // we're a shortcut to (only if we're not part of a batch).
                if (this.shortcut_to) {
                    result.propagate = true;

                    if (!part_of_batch) {
                        this.shortcut_to.set_property(name, new_value, silent);
                        silent = true;

                        if (this.shortcut_to.__ws_psheet) {
                            this.shortcut_to.__ws_psheet.reset();
                        }
                    }
                }

                // If this node is not a shortcut, check whether there are shortcuts
                // pointing to this node. If there are, update their corresponding
                // properties, but do it directly, without API invokations. Also, if
                // we're not part of a batch, reset the properties sheets of the
                // shortcuts.
                else {
                    if (this.graph) {
                        var shortcuts = this.graph.get_shortcuts_to(this);
                        
                        for (var i = 0; i < shortcuts.length; i++) {
                            shortcuts[i].properties[name] = new_value;

                            if (!part_of_batch && shortcuts[i].__ws_psheet) {
                                shortcuts[i].__ws_psheet.reset();
                            }
                        }
                    }
                }
            }

            // Generate a NODE_UPDATE event, unless we were told to be silent, or we're
            // part of a batch update.
            if (!silent && !part_of_batch) {
                this.graph.notify_listeners({
                    type: Ws.Graph.EVENT_NODE_UPDATED,
                    data: {
                        node: this,
                        updated_properties: [
                            {
                                name: name,
                                old_value: old_value,
                                new_value: new_value
                            }
                        ],
                        updated_ui_properties: []
                    }
                });
            }
        }

        return result;
    },

    set_properties: function(properties, silent) {
        var updates = [];
        var propagates = {};
        var propagates_length = 0;

        for (var name in properties) {
            var result = this.set_property(name, properties[name], true, true);

            if (result.update_descriptor != null) {
                if (result.propagate && this.shortcut_to) {
                    propagates[name] = properties[name];
                    propagates_length++;
                } else {
                    updates.push(result.update_descriptor);
                }
            }
        }

        if (propagates_length > 0) {
            this.shortcut_to.set_properties(propagates, silent);
            
            if (this.shortcut_to.__ws_psheet) {
                this.shortcut_to.__ws_psheet.reset();
            }

            if (this.graph) {
                var shortcuts = this.graph.get_shortcuts_to(this);

                for (var i = 0; i < shortcuts.length; i++) {
                    if (shortcuts[i].__ws_psheet) {
                        shortcuts[i].__ws_psheet.reset();
                    }
                }
            }
        }

        if (!silent && (updates.length > 0)) {
            this.graph.notify_listeners({
                type: Ws.Graph.EVENT_NODE_UPDATED,
                data: {
                    node: this,
                    updated_properties: updates,
                    updated_ui_properties: []
                }
            });
        }
    },

    get_ui_property: function(name) {
        return this.ui_properties[name];
    },

    set_ui_property: function(name, new_value, silent, part_of_batch) {
        var result = {
            update_descriptor: null,
            propagate: false
        }

        if (this.ui_properties[name] != new_value) {
            var old_value = this.ui_properties[name];

            // The new value does not match the old one, so we populate the
            // update_descriptor property of the result object.
            result.update_descriptor = {
                name: name,
                old_value: old_value,
                new_value: new_value
            }

            // Apply the new value.
            this.ui_properties[name] = new_value;
            this.ui[name] = new_value;

            // If the property being set is not on the propagation exception list, we
            // need to perform a couple of checks for possible updates to either "parent"
            // or shortcuts.
            if (this.propagation_ui_exceptions.indexOf(name) == -1) {
                // If this node is a shortcut, we'll call the "parent" node's API to set
                // the property there as well and force us to be silent (i.e. not
                // generate an event). However, if this call is part of a batch (see
                // set_properties), skip all of this, but always set the propagate flag
                // on the result object. Also, reset the properties sheet of the node
                // we're a shortcut to (only if we're not part of a batch).
                if (this.shortcut_to) {
                    result.propagate = true;

                    if (!part_of_batch) {
                        this.shortcut_to.set_ui_property(name, new_value, silent);
                        silent = true;
                        
                        if (this.shortcut_to.__ws_psheet) {
                            this.shortcut_to.__ws_psheet.reset();
                        }
                    }
                }

                // If this node is not a shortcut, check whether there are shortcuts
                // pointing to this node. If there are, update their corresponding
                // properties, but do it directly, without API invokations. Also, if
                // we're not part of a batch, reset the properties sheets of the
                // shortcuts.
                else {
                    if (this.graph) {
                        var shortcuts = this.graph.get_shortcuts_to(this);

                        for (var i = 0; i < shortcuts.length; i++) {
                            shortcuts[i].ui_properties[name] = new_value;
                            shortcuts[i].ui[name] = new_value;

                            if (!part_of_batch && shortcuts[i].__ws_psheet) {
                                shortcuts[i].__ws_psheet.reset();
                            }

                            if (!part_of_batch) {
                                shortcuts[i].ui.repaint();
                            }
                        }
                    }
                }
            }

            // Generate a NODE_UPDATE event, unless we were told to be silent, or we're
            // part of a batch update.
            if (!silent && !part_of_batch) {
                this.graph.notify_listeners({
                    type: Ws.Graph.EVENT_NODE_UPDATED,
                    data: {
                        node: this,
                        updated_properties: [],
                        updated_ui_properties: [
                            {
                                name: name,
                                old_value: old_value,
                                new_value: new_value
                            }
                        ]
                    }
                });
            }

            if (!part_of_batch) {
                this.ui.repaint();
            }
        }

        return result;
    },

    set_ui_properties: function(ui_properties, silent) {
        var updates = [];
        var propagates = {};
        var propagates_length = 0;

        for (var name in ui_properties) {
            var result = this.set_ui_property(name, ui_properties[name], true, true);

            if (result.update_descriptor != null) {
                if (result.propagate && this.shortcut_to) {
                    propagates[name] = ui_properties[name];
                    propagates_length++;
                } else {
                    updates.push(result.update_descriptor);
                }
            }
        }

        if (propagates_length > 0) {
            this.shortcut_to.set_ui_properties(propagates, silent);

            if (this.shortcut_to.__ws_psheet) {
                this.shortcut_to.__ws_psheet.reset();
            }
        }

        if (this.graph) {
            var shortcuts = this.graph.get_shortcuts_to(this);

            for (var i = 0; i < shortcuts.length; i++) {
                if (shortcuts[i].__ws_psheet) {
                    shortcuts[i].__ws_psheet.reset();
                }
                shortcuts[i].ui.repaint();
            }
        }

        if (!silent && (updates.length > 0)) {
            this.graph.notify_listeners({
                type: Ws.Graph.EVENT_NODE_UPDATED,
                data: {
                    node: this,
                    updated_properties: [],
                    updated_ui_properties: updates
                }
            });
        }

        this.ui.repaint();
    },

    set_all_properties: function(properties, ui_properties, silent) {
        this.set_properties(properties, silent);
        this.set_ui_properties(ui_properties, silent);
    },

    // Properties container --------------------------------------------------------------
    pc_get_descriptors: function() {
        return {
            properties: [
                {
                    name: "ui_properties_display_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Display Name",
                    description: "Display Name description",
                    value: this.ui_properties.display_name
                },
                {
                    name: "ui_properties_description",
                    type: Ws.PropertiesSheet.TEXT,

                    display_name: "Description",
                    description: "Description description",
                    value: this.ui_properties.description
                }
            ]
        }
    },

    pc_set_properties: function(properties) {
        var updated_properties = {};
        var updated_ui_properties = {};

        for (var i in properties) {
            if (i.indexOf("properties_") == 0) {
                updated_properties[i.substr(11)] = properties[i];
            } else

            if (i.indexOf("ui_properties_") == 0) {
                updated_ui_properties[i.substr(14)] = properties[i];
            }
        }

        this.set_all_properties(updated_properties, updated_ui_properties);
    },

    // Child-parent ----------------------------------------------------------------------
    get_children: function() {
        return this.graph.get_children(this);
    },

    is_parent_of: function(node) {
        var parent = node.parent;

        while (parent) {
            if (parent == this) {
                return true;
            }

            parent = parent.parent;
        }

        return false;
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});

// ---------------------------------------------------------------------------------------
/**
 * Base class for visual (UI) nodes representations. Encapsulates all node data
 * related to how a node is shown in the UI.
 */
Ws.Graph.DefaultNodeUi = Class.create({
    // Basic properties ------------------------------------------------------------------
    x: 10,
    y: 10,
    width: 180,
    height: 145,
    display_name: "Node",
    description: "Node description.",

    graph_ui: null,

    // Basic (Selection) properties ------------------------------------------------------
    __is_selected: false,

    // Drawing (Basic) properties --------------------------------------------------------
    margin: 50,
    radius: 5,

    fill_style: "rgba(255, 255, 255, 0.6)",
    stroke_style: "rgb(0, 81, 120)",
    line_width: 2,
    line_cap: "round",

    fill_style_selected: "rgba(191, 234, 255, 0.7)",
    stroke_style_selected: "rgb(0, 81, 120)",
    line_width_selected: 2,
    line_cap_selected: "round",

    // Drawing (Connectors) properties ---------------------------------------------------
    in_connector_radius: 2,
    in_connector_radius_highlighted: 8,
    in_connector_radius_highlighted_active: 5,

    in_connector_stroke_style: "rgb(0, 81, 120)",
    in_connector_fill_style: "rgb(0, 81, 120)",
    in_connector_line_width: 1,
    in_connector_line_cap: "round",

    in_connector_stroke_style_highlighted: "rgb(0, 143, 0)",
    in_connector_fill_style_highlighted: "rgb(0, 143, 0)",
    in_connector_line_width_highlighted: 1,
    in_connector_line_cap_highlighted: "round",

    in_connector_arrow_length: 14,
    in_connector_arrow_head_length: 5,
    in_connector_arrow_head_height: 2,

    out_connector_radius: 2,
    out_connector_radius_highlighted: 8,
    out_connector_radius_highlighted_active: 5,

    out_connector_stroke_style: "rgb(0, 81, 120)",
    out_connector_fill_style: "rgb(0, 81, 120)",
    out_connector_line_width: 1,
    out_connector_line_cap: "round",

    out_connector_stroke_style_highlighted: "rgb(143, 0, 0)",
    out_connector_fill_style_highlighted: "rgb(143, 0, 0)",
    out_connector_line_width_highlighted: 1,
    out_connector_line_cap_highlighted: "round",

    out_connector_arrow_length: 11,
    out_connector_arrow_head_length: 5,
    out_connector_arrow_head_height: 2,
    
    connector_animation_trigger_offset: 0,

    in_connector_highlight_animated: true,
    in_connector_highlight_animated_distance: 50,
    in_connector_highlight_animated_opacity: 0.8,

    out_connector_highlight_animated: true,
    out_connector_highlight_animated_distance: 50,
    out_connector_highlight_animated_opacity: 0.8,

    // Drawing (Text) --------------------------------------------------------------------
    truncate_long_text: true,

    canvastext_font: "sans",
    canvastext_font_size: 12,
    canvastext_font_color: "rgb(0, 0, 0)",
    canvastext_text_offset_x: 90,
    canvastext_text_offset_y: 78,
    canvastext_text_margin: 5,

    html_div_classname: "node-display-name",
    html_div_font: "normal normal normal 12pt " + // This will be set as the 'font' 
            "Geneva, Verdana, Arial, sans-serif", // style, should follow the CSS spec.
    html_div_color: "#000",
    html_div_text_decoration: "none",
    html_div_text_align: "center",
    html_div_text_margin: 5,

    browser_text_stroke: null,
    browser_text_fill: "rgba(0, 0, 0, 0.9)",
    browser_text_style: "normal normal normal 12pt " +
            "Geneva, Verdana, Arial, sans-serif",
    browser_text_offset_x: 90,
    browser_text_offset_y: 78,
    browser_text_margin: 5,

    text_draw_method: "browser-api",

    // Drawing (Icon) --------------------------------------------------------------------
    icon: null,
    icon_offset_x: 55,
    icon_offset_y: 88,

    // Properties used by GraphUi for registering in various managers --------------------
    is_drag_source: true,
    is_drop_target: true,
    is_selectable: true,
    is_clickable: true,

    // HTML ------------------------------------------------------------------------------
    element: null,
    canvas: null,

    // Canvas context --------------------------------------------------------------------
    context:  null,

    // Toolbar ---------------------------------------------------------------------------
    toolbar_x: 135, // the initial X  of the toolbar
    toolbar_y: 38, // the initial Y  of the toolbar

    toolbar_spacing_x: 2,
    toolbar_spacing_y: 2,
    toolbar_direction: "right-to-left",

    // Constructor -----------------------------------------------------------------------
    initialize: function(node, options) {
        // First apply the customizing options, that may have been passed via the
        // constructor arguments.
        if (options) {
            Object.extend(this, options);
        }

        // Save the node reference -------------------------------------------------------
        this.node = node;

        // Initialize the DOM elements which represents this UI on the browser level -----
        this.canvas = Ws.Utils.create_canvas();

        this.element = new Element("div");
        this.element.style.position = "absolute";
        this.element.setStyle({
            margin: "0px",
            padding: "0px",
            borderWidth: "0px",
            outlineStyle: "none" // This one is to disable native focus indicators,
                                 // we have our own
        });
        this.element.appendChild(this.canvas);
        this.element.tabIndex = 0;

        // Save the canvas context -------------------------------------------------------
        this.context = this.canvas.getContext("2d");

        // Initialize the toolbar --------------------------------------------------------
        //   Toolbar is initialized in the __get_toolbar_items() method, as it is
        //   dynamic.

        // Add CanvasText support --------------------------------------------------------
        if (CanvasTextFunctions) {
            CanvasTextFunctions.enable(this.context);
        }

        // Apply the UI properties of the node -------------------------------------------
        Object.extend(this, Ws.Utils.clone_object(this.node.ui_properties));

    },

    // Drawing ---------------------------------------------------------------------------
    repaint: function() {
        this.position();

        if (!this.__is_selected) {
            this.draw_toolbar();
        }
        // Draw the main element graphics
        this.draw_node();

        // Draw auxiliary graphics
        this.draw_connectors();
        this.draw_text();
        if (this.__is_selected) {
            this.draw_toolbar();
        }

        this.draw_icon();

      //  this.__reset_tooltip();
    },

    draw_node: function() {
        this.prepare_path();

        this.context.fillStyle =
        this.__is_selected ? this.fill_style_selected : this.fill_style;
        this.context.strokeStyle =
        this.__is_selected ? this.stroke_style_selected : this.stroke_style;
        this.context.lineWidth =
        this.__is_selected ? this.line_width_selected : this.line_width;
        this.context.lineCap =
        this.line_cap;

        this.context.stroke();
        this.context.fill();
    },

    draw_connectors: function() {
        var i, connectors, link_ui;

        connectors = this.get_inbound_connectors();
        if (this.__is_selected || this.__highlight_in_connectors) {
            for (i = 0; i < connectors.length; i++) {
                // If we need to highlight the connectors not because we're selected, 
                // but because a link is being dragged, we should first check whether 
                // this link can actually be connected to this connector. If it cannot
                // we simply draw a non-highlighted connector and continue the loop.
                if (!this.__is_selected && this.__link_is_dragged) {
                    link_ui = Ws.DragManager.__active_ds.__graph_ui.__dragged;

                    if (!this.__check_link_vs_connector(link_ui.link, connectors[i], i, true)) {
                        this.__draw_in_connector(connectors[i], i);
                        continue;
                    }
                }

                this.__draw_highlighted_in_connector(connectors[i], i);
            }
        } else {
            for (i = 0; i < connectors.length; i++) {
                this.__draw_in_connector(connectors[i], i);
            }
        }

        connectors = this.get_outbound_connectors();
        if (this.__is_selected || this.__highlight_out_connectors) {
            for (i = 0; i < connectors.length; i++) {
                // Same story as for the inbound connectors.
                if (!this.__is_selected && this.__link_is_dragged) {
                    link_ui = Ws.DragManager.__active_ds.__graph_ui.__dragged;

                    if (!this.__check_link_vs_connector(link_ui.link, connectors[i], i, false)) {

                        this.__draw_out_connector(connectors[i], i);
                        continue;
                    }
                }

                this.__draw_highlighted_out_connector(connectors[i], i);
            }
        } else {
            for (i = 0; i < connectors.length; i++) {
                this.__draw_out_connector(connectors[i], i);
            }
        }
    },

    draw_text: function() {
        if (!this.display_name) return;

        var self = this;

        var text_to_draw = this.display_name;
        switch (this.text_draw_method) {
            case Ws.Graph.OVERLAY_HTML_DIV:
                var text_element = this.element.select("." + this.html_div_classname);

                if (text_element.length == 0) {
                    text_element = new Element("div");
                    text_element.addClassName(this.html_div_classname);
                    this.element.appendChild(text_element);
                } else {
                    text_element = text_element.reduce();
                }

                var text_bmp = Ws.Utils.get_bmp(text_element);

                text_element.setStyle({
                    position: "absolute",
                    left: this.margin + "px",
                    top: "50%",
                    width: (this.width - (2 * this.margin) - text_bmp.get_bp_horizontal()) + "px",
                    font: this.html_div_font,
                    color: this.html_div_color,
                    textDecoration: this.html_div_text_decoration,
                    textAlign: this.html_div_text_align,
                    whiteSpace: "pre",
                    cursor: "default"
                });

                // If we're told to, check whether the text fits into the node. If
                // it does not -- truncate it.
                if (this.truncate_long_text) {
                    var text_offset;
                    switch (this.html_div_text_align) {
                        case "left":
                            text_offset = this.margin;
                            break;
                        case "right":
                            text_offset = this.width - this.margin;
                            break;
                        default:
                            text_offset = this.width / 2;
                            break;
                    }

                    text_to_draw = this.__truncate_text(
                            text_to_draw,
                            function(text) {
                                var div = new Element("div");
                                
                                div.setStyle({
                                    position: "absolute",
                                    top: "-1000px",
                                    left: "-1000px",
                                    margin: "0px",
                                    padding: "0px",
                                    font: self.html_div_font
                                });
                                div.innerHTML = text;

                                document.body.appendChild(div);
                                var width = div.offsetWidth;
                                document.body.removeChild(div);

                                return width;
                            },
                            text_offset,
                            this.html_div_text_margin,
                            this.html_div_text_align);
                }
                
                text_element.innerHTML = text_to_draw;

                // Now we get the offsetHeight of the element and position it vertically.
                text_element.setStyle({
                    marginTop: "-" + Math.floor(text_element.offsetHeight / 2) + "px"
                });

                break;
            case Ws.Graph.OVERLAY_BROWSER_API:
                if (this.context.mozDrawText) {
                    // First reset the current path, or mozilla does not correctly
                    // handle if a current path exists.
                    this.context.beginPath();
                    this.context.closePath();

                    this.context.mozTextStyle = this.browser_text_style;

                    // If we're told to, check whether the text fits into the node. If
                    // it does not -- truncate it.
                    if (this.truncate_long_text) {
                        text_to_draw = this.__truncate_text(
                                text_to_draw,
                                function(text) {
                                    return self.context.mozMeasureText(text);
                                },
                                this.browser_text_offset_x,
                                this.browser_text_margin,
                                "center");
                    }

                    var text_width = this.context.mozMeasureText(text_to_draw);

                    this.context.save();
                    this.context.translate(
                            this.browser_text_offset_x - (text_width / 2),
                            this.browser_text_offset_y);
                    this.context.mozPathText(text_to_draw);

                    if (this.browser_text_fill) {
                        this.context.fillStyle = this.browser_text_fill;
                        this.context.fill();
                    }
                    if (this.browser_text_stroke) {
                        this.context.strokeStyle = this.browser_text_stroke;
                        this.context.stroke();
                    }

                    this.context.restore();

                    break;
                }

                // If none of the if's executed we knowingly fall through to the default,
                // CanvasText behavior.
            case Ws.Graph.OVERLAY_CANVASTEXT:
            default:
                this.context.strokeStyle = this.canvastext_font_color;

                // If we're told to, check whether the text fits into the node. If it
                // does not -- truncate it.
                if (this.truncate_long_text) {
                    text_to_draw = this.__truncate_text(
                            text_to_draw,
                            function(text) {
                                return self.context.measureText(
                                        self.canvastext_font, 
                                        self.canvastext_font_size,
                                        text);
                            },
                            this.canvastext_text_offset_x,
                            this.canvastext_text_margin,
                            "center");
                }

                // Find out whether it is OK to draw the text. For this, find out the
                // length of the string, and check whether it fits in the defined
                // "window". If it does not -- reduce the string length.
                var text_does_not_fit = true;
                while (text_does_not_fit) {
                    var h_width = this.context.measureText(this.canvastext_font,
                            this.canvastext_font_size, text_to_draw) / 2;

                    var fits_left = this.canvastext_text_offset_x - h_width >=
                            this.margin + this.canvastext_text_margin;
                    var fits_right = this.canvastext_text_offset_x + h_width <=
                            this.width - this.margin - this.canvastext_text_margin;

                    if (fits_left && fits_right) {
                        text_does_not_fit = false;
                    } else {
                        var old_text_to_draw = text_to_draw;
                        text_to_draw = this.__truncate_text(text_to_draw);

                        // If the truncation procedure did not change the string, we
                        // should output it as is, we can't change anything anymore.
                        if (old_text_to_draw == text_to_draw) {
                            text_does_not_fit = false;
                        }
                    }
                }

                this.context.drawTextCenter(
                    this.canvastext_font,
                    this.canvastext_font_size,
                    this.canvastext_text_offset_x,
                    this.canvastext_text_offset_y,
                    text_to_draw);
        }
    },

    draw_toolbar: function() {
        var items = this.__get_toolbar_items();
        if (items.length < 1) {
            return;
        }
        var self = this;
        
        var current_x = this.toolbar_x;
        var current_y = this.toolbar_y;
        
        var r = 8 + this.toolbar_spacing_x;

        var x = current_x - (2 * r - this.toolbar_spacing_x) * (items.length - 1) - 2 * r;
        var y = current_y;

        this.context.lineWidth = 1;
        this.context.fillStyle = this.__is_selected ? "rgba(200, 200, 200, 0.3)" :
           "rgba(200, 200, 200, 0.5)";

        this.context.beginPath();
        
        this.context.arc(x + r, y + r, r, 3.14 / 2, -3.14 / 2, false);
        this.context.moveTo(x + r, y);
        this.context.lineTo(x + r + (2 * r - this.toolbar_spacing_x) * (items.length - 1) + 1, y);
        this.context.arc(x + r + (2 * r - this.toolbar_spacing_x) * (items.length - 1) + 1, y + r,
        r, -3.14 / 2, 3.14 / 2, false);
        this.context.lineTo(x + r, y + 2 * r);
        this.context.fill();

        this.context.strokeStyle = this.__is_selected ? "rgba(210, 210, 210, 0.7)" :
            "rgba(210, 210, 210, 1)";
        this.context.arc(x + r, y + r, r, 3.14 / 2, -3.14 / 2, false);
        this.context.moveTo(x + r, y);
        this.context.lineTo(x + r + (2 * r - this.toolbar_spacing_x) * (items.length - 1) + 1, y);
        this.context.arc(x + r + (2 * r - this.toolbar_spacing_x) * (items.length - 1) + 1, y + r,
        r, -3.14 / 2, 3.14 / 2, false);
        this.context.lineTo(x + r, y + 2 * r);
        this.context.stroke();
        this.context.restore();

        current_x = current_x - this.toolbar_spacing_x - 16;
        current_y = current_y + this.toolbar_spacing_y;
        for (var i = 0; i < items.length; i++) {
            var url = this.__is_selected ? items[i].icon : items[i].icon_d;
            var image = Ws.ImagesLoadManager.get(url);

            if (image) {
                this.context.drawImage(
                        image, current_x,
                        current_y,
                        items[i].icon_width, items[i].icon_height);
                current_x = current_x - items[i].icon_width - this.toolbar_spacing_x;
                 
            } else {
                // This is also a hack for MSIE, which ensures that there is at least
                // some time for the image to load correctly, otherwise it goes into an
                // infinite loop returning 'undefined' for the get() calls. We also take
                // caution to load images one by one.

              //  setTimeout(function() {
                    Ws.ImagesLoadManager.load(
                        url,
                        function() {
                            self.repaint();
                        });
              //  }, 50);
                
                return;
            }
        }
    },

    draw_icon: function() {
        if (!this.icon) return;

        var x, y, self = this;

        var image_icon = Ws.ImagesLoadManager.get(this.icon);
        var image_badge_shortcut = Ws.ImagesLoadManager.get(Ws.Graph.NODE_BADGE_SHORTCUT);

        if (image_icon != null) {
            x = this.icon_offset_x;
            y = this.icon_offset_y;

            // If we're on MSIE (excanvas is initialized), correct the initial position.
            if (typeof G_vmlCanvasManager != "undefined") {
                x -= 7;
                y -= 8;
            }
            
            this.context.drawImage(image_icon, x, y);
        } else {
            Ws.ImagesLoadManager.load(this.icon, function() {
                self.repaint();
            });
        }

        if (this.node.shortcut_to) {
            if (image_badge_shortcut != null) {
                x = this.icon_offset_x + 16;
                y = this.icon_offset_y + 10;

                // If we're on MSIE (excanvas is initialized), correct the initial position.
                if (typeof G_vmlCanvasManager != "undefined") {
                    x -= 7;
                    y -= 8;
                }

                this.context.drawImage(image_badge_shortcut, x, y);
            } else {
                Ws.ImagesLoadManager.load(Ws.Graph.NODE_BADGE_SHORTCUT, function() {
                    self.repaint();
                });
            }
        }
    },

    // Paths -----------------------------------------------------------------------------
    /**
     * Prepares path for the node itself. It will then be used to stroke and fill to
     * form the main node visual. It has a sister method {@link #check_point_node_path()},
     * which checks whether the given point is within the "main" path of the node.
     */
    prepare_path: function() {
        // A ---- B
        // |      |
        // D ---- C

        var A_x = this.margin, A_y = this.margin;
        var B_x = this.width - this.margin, B_y = this.margin;
        var C_x = this.width - this.margin, C_y = this.height - this.margin;
        var D_x = this.margin, D_y = this.height - this.margin;

        this.context.beginPath();
        this.context.moveTo(A_x, A_y + this.radius);
        this.context.quadraticCurveTo(A_x, A_y, A_x + this.radius, A_y);
        this.context.lineTo(B_x - this.radius, B_y);
        this.context.quadraticCurveTo(B_x, B_y, B_x, B_y + this.radius);
        this.context.lineTo(C_x, C_y - this.radius);
        this.context.quadraticCurveTo(C_x, C_y, C_x - this.radius, C_y);
        this.context.lineTo(D_x + this.radius, D_y);
        this.context.quadraticCurveTo(D_x, D_y, D_x, D_y - this.radius);
        this.context.lineTo(A_x, A_y + this.radius);
    },

    /**
     * Checks whether the given point is within the node's main visual element's path.
     * This has a sister method {@link prepare_path()}, which actually prepares the
     * path. This method is a necessary workaround for MSIE, which does not have a
     * <canvas> implemntation, while all simulations missing the isPointInPath() method.
     */
    check_point_node_path: function(x, y) {
        if (this.context.isPointInPath) {
            this.prepare_path();
            return this.context.isPointInPath(x, y);
        } else {
            var A_x = this.margin, A_y = this.margin;
            var C_x = this.width - this.margin, C_y = this.height - this.margin;

            return (x >= A_x) && (x <= C_x) && (y >= A_y) && (y <= C_y);
        }
    },

    /**
     * Checks whether the given point (x, y) is within the path for the given outbound
     * connector. The connector is specified by its index. This method will return
     * <code>true</code>, if the given point is within the highlighted radius --
     * {@link #out_connector_radius_highlighted} from the connector center. It will
     * return <code>false</code> otherwise.
     */
    check_point_outbound_connector_path: function(connector_index, x, y) {
        var connectors = this.get_outbound_connectors();

        var d_x = x - connectors[connector_index].x;
        var d_y = y - connectors[connector_index].y;

        var distance = Math.sqrt((d_x * d_x) + (d_y * d_y));

        return distance < this.out_connector_radius_highlighted;
    },

    /**
     * Checks whether the given point (x, y) in the node coordinate space fits into the toolbar
     * area. This method returns <code>true</code> if it does, <code>false</code> otherwise.
     */
    check_point_toolbar_path: function(x, y) {
        var x1 = this.toolbar_x;
        var y1 = this.toolbar_y;

        var x2 = x1, y2 = y1;

        var items = this.__get_toolbar_items();
        for (var i = 0; i < items.length; i++) {
            x2 -= items[i].icon_width + this.toolbar_spacing_x;

            if (y2 < this.toolbar_y + items[i].icon_height) {
                y2 = this.toolbar_y + items[i].icon_height;
            }
        }

        return (x >= x2) && (x <= x1) && (y >= y1) && (y <= y2);
    },

    // Positionining ---------------------------------------------------------------------
    position: function() {
        this.canvas.width = this.width;
        this.canvas.height = this.height;

        if (this.canvas.firstChild) {
            Element.extend(this.canvas.firstChild);

            this.canvas.firstChild.setStyle({
                width: this.width + "px",
                height: this.height + "px"
            });
        }

        this.element.style.width = this.width + "px";
        this.element.style.height = this.height + "px";

        var coords = this.node.graph.ui.get_node_graph_coordinates(this);

        this.element.style.left = coords.x + "px";
        this.element.style.top = coords.y + "px";
    },

    move_to: function(x, y) {
        this.node.set_ui_properties({
            x: x,
            y: y
        });

        this.repaint();

        // If there node is a child of someone, thi someone should also be repainted.
        if (this.node.parent) {
            this.node.parent.ui.repaint();
        }
    },

    // Connectors ------------------------------------------------------------------------
    get_inbound_connectors: function() {
        var connectors = this.node.get_inbound_connectors();

        connectors[0].x = this.margin;
        connectors[0].y = this.height / 2;
        connectors[0].type = Ws.Graph.NODE_CONNECTOR_WEST;

        connectors[1].x = this.width / 2;
        connectors[1].y = this.margin;
        connectors[1].type = Ws.Graph.NODE_CONNECTOR_NORTH;

        return connectors;
    },

    get_inbound_connector: function(index) {
        return this.get_inbound_connectors()[index];
    },
    
    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();

        connectors[0].x = this.width - this.margin;
        connectors[0].y = this.height / 2;
        connectors[0].type = Ws.Graph.NODE_CONNECTOR_EAST;

        connectors[1].x = this.width / 2;
        connectors[1].y = this.height - this.margin;
        connectors[1].type = Ws.Graph.NODE_CONNECTOR_SOUTH;

        return connectors;
    },

    get_outbound_connector: function(index) {
        return this.get_outbound_connectors()[index];
    },

    // Drag source -----------------------------------------------------------------------
    ds_accept_drag: function(x, y, event) {
        if (this.read_only) {
            return false;
        }

        this.__drag_last_x = x;
        this.__drag_last_y = y;

        var point = this.__get_point_relative_to_node(x, y);

        var connectors = this.get_outbound_connectors();
        for (var i = 0; i < connectors.length; i++) {
            if (this.check_point_outbound_connector_path(i, point.x, point.y)) {
                var connector = connectors[i];

                // Marker variable which holds the indicator of whether we should accept
                // drag from this connector (effecting in a new link being created) or
                // not.
                var skip_connector = false;

                // Check if there is a link going from this connector already. If there
                // is, we cannot create a new one here by simply dragging the connector,
                // so we skip.
                var links = this.node.graph.get_outbound_links_by_node(this.node);
                for (var j = 0; j < links.length; j++) {
                    if (links[j].start_node_connector == i) {
                        skip_connector = true;
                        break;
                    }
                }

                if (!skip_connector) {
                    var link_type = connector.default_link_type ?
                            connector.default_link_type : Ws.Graph.DEFAULT_LINK_TYPE;

                    this.__drag_new_link = true;
                    this.__drag_new_link_connector = i;
                    this.__drag_new_link_type = link_type;

                    return true;
                }
            }
        }

        if (this.check_point_node_path(point.x, point.y)) {
            return true;
        }

        return false;
    },

    ds_drag_started: function(x, y, event) {
        if (this.__drag_new_link) {
            var link = Ws.Graph.LinkFactory.new_link(this.__drag_new_link_type, {
                start_node: this.node,
                start_node_connector: this.__drag_new_link_connector,
                end_node: null,
                end_node_connector: 0,
                properties: {},
                ui_properties: {}
            });

            link.ui.dragged_end = true;
            link.ui.end_x = this.__get_point_relative_to_graph(x, y).x;
            link.ui.end_y = this.__get_point_relative_to_graph(x, y).y;

            this.node.graph.add_link(link);

            // Quick hack to subsitute the dragged entity in the graph ui. We need this
            // in order to make sure that the newly created link receives the dragged_to
            // events and that other nodes are aware of the fact that it is a link that
            // is being dragged. Yet the way to access graph ui is a bit awkward (we
            // modify a private field, which is not so good).
            this.node.graph.ui.__dragged = link.ui;
            link.ui.ds_drag_started();

            this.__drag_new_link = false;
            return;
        }

        // If the drag was started with the meta key down and the node is a child of its
        // parent, we make the child a top-level node, deleting all connections to the
        // children of the parent, if the current policy prohibits such links.
        if (event.metaKey && this.node.parent) {
            var links = this.node.get_links();
            for (var i = 0; i < links.length; i++) {
                this.node.graph.remove_link(links[i]);
            }

            this.x = this.x + this.node.parent.ui.x + this.margin;
            this.y = this.y + this.node.parent.ui.y + this.margin;

            var parent = this.node.parent;

            this.node.parent = null;
            parent.ui.repaint();
        }

        // Lastly, once the node started being dragged, we do want to select it. That's
        // fair.
        this.graph_ui.select_node(this);

        // set Link state d&d
        var links = this.node.get_links();
        for (var i = 0; i < links.length; i++) {
            links[i].ui.ds_drag_started();
        }

    },

    ds_dragged_to: function(x, y, event) {
        this.x += x - this.__drag_last_x;
        this.y += y - this.__drag_last_y;

        this.__drag_last_x = x;
        this.__drag_last_y = y;

        this.repaint();
        this.graph_ui.repaint_node_parent_with_links(this);
        this.graph_ui.repaint_node_children_with_links(this);
        this.graph_ui.repaint_links(this);
    },

    ds_drag_finished: function(x, y, event) {
        this.ds_dragged_to(x, y);

        this.node.set_ui_properties({
            x: this.x,
            y: this.y
        });

        // set Link state d&d
        var links = this.node.get_links();
        for (var i = 0; i < links.length; i++) {
            links[i].ui.ds_drag_finished();
        }
    },

    start_animated_links: function() {
        var links = this.node.get_links();
        for (var i = 0; i < links.length; i++) {
            links[i].ui.animated_start();
        }
    },

    stop_animated_links: function() {
        var links = this.node.get_links();
        for (var i = 0; i < links.length; i++) {
            links[i].ui.animated_stop();
        }
    },

    // Selectable ------------------------------------------------------------------------
    sl_accept_selection: function(x, y) {
        var point = this.__get_point_relative_to_node(x, y);

        if (this.check_point_node_path(point.x, point.y)) {
            return true;
        }

        // If this is already selected, we should not lose selection when a click comes to the
        // toolbar. Importantly, the latter can be located outside of the main path.
      //  if (this.__is_selected) {
            return this.check_point_toolbar_path(point.x, point.y);
       // }

        return false;
    },

    sl_select: function() {
        this.__is_selected = true;

        this.repaint();
    },

    sl_deselect: function() {
        this.__is_selected = false;

        this.repaint();
    },

    // Drop target -----------------------------------------------------------------------
    dt_accept_drop: function(drag_source, x, y) {
        var point = this.__get_point_relative_to_node(x, y);
        var link;
        var connectors, connector, radius;

        // We will NOT accept anything that is not a graph element.
        if (!drag_source.__graph_ui || !drag_source.__graph_ui.__dragged) {
            return false;
        }

        if (drag_source.__graph_ui.__dragged.node) {
            // Default node does not accept other nodes dropped on it.
        }

        if (drag_source.__graph_ui.__dragged.link) {
            link = drag_source.__graph_ui.__dragged.link;

            if (link.ui.dragged_start) {
                connectors = this.get_outbound_connectors();

                for (var i = 0; i < connectors.length; i++) {
                    connector = connectors[i];

                    radius = this.out_connector_radius_highlighted +
                            this.out_connector_radius_highlighted_active;

                    if (this.__check_link_vs_connector(link, connector, i, false) &&
                            (point.x > connector.x - radius) &&
                            (point.x < connector.x + radius) &&
                            (point.y > connector.y - radius) &&
                            (point.y < connector.y + radius)) {

                        return true;
                    }
                }
            }

            if (link.ui.dragged_end) {
                connectors = this.get_inbound_connectors();

                for (var j = 0; j < connectors.length; j++) {
                    connector = connectors[j];

                    radius = this.in_connector_radius_highlighted +
                            this.in_connector_radius_highlighted_active;

                    if (this.__check_link_vs_connector(link, connector, i, true) &&
                            (point.x > connector.x - radius) &&
                            (point.x < connector.x + radius) &&
                            (point.y > connector.y - radius) &&
                            (point.y < connector.y + radius)) {

                        return true;
                    }
                }
            }
        }

        return false;
    },

    dt_handle_drop: function(drag_source, x, y) {
        var point = this.__get_point_relative_to_node(x, y);

        if (!drag_source.__graph_ui || !drag_source.__graph_ui.__dragged) {
            return;
        }

        if (drag_source.__graph_ui.__dragged.link) {
            var link = drag_source.__graph_ui.__dragged.link;

            if (link.ui.dragged_start) {
                var out_connectors = this.get_outbound_connectors();

                for (var i = 0; i < out_connectors.length; i++) {
                    var out_connector = out_connectors[i];

                    var out_radius = this.out_connector_radius_highlighted;

                    if ((point.x > out_connector.x - out_radius) &&
                        (point.x < out_connector.x + out_radius) &&
                        (point.y > out_connector.y - out_radius) &&
                        (point.y < out_connector.y + out_radius)) {

                        link.start_node = this.node;
                        link.start_node_connector = i;
                        this.node.graph.notify_listeners({
                            type: Ws.Graph.EVENT_LINK_UPDATED,
                            data: {
                                link: link
                            }
                        });

                        link.ui.repaint();
                    }
                }
            }

            if (link.ui.dragged_end) {
                var in_connectors = this.get_inbound_connectors();

                for (var j = 0; j < in_connectors.length; j++) {
                    var in_connector = in_connectors[j];

                    var in_radius = this.in_connector_radius_highlighted;

                    if ((point.x > in_connector.x - in_radius) &&
                        (point.x < in_connector.x + in_radius) &&
                        (point.y > in_connector.y - in_radius) &&
                        (point.y < in_connector.y + in_radius)) {

                        link.end_node = this.node;
                        link.end_node_connector = j;
                        this.node.graph.notify_listeners({
                            type: Ws.Graph.EVENT_LINK_UPDATED,
                            data: {
                                link: link
                            }
                        });

                        link.ui.repaint();
                    }
                }
            }
        }
    },

    dt_drag_started: function(drag_source, x, y) {
        if (drag_source.__graph_ui && drag_source.__graph_ui.__dragged &&
                drag_source.__graph_ui.__dragged.link) {

            var link = drag_source.__graph_ui.__dragged.link;

            this.__link_is_dragged = true;

            if (link.ui.dragged_start) {
                this.__highlight_out_connectors = true;
            }

            if (link.ui.dragged_end) {
                this.__highlight_in_connectors = true;
            }

            this.repaint();
        }
    },

    dt_dragged_to: function(drag_source, x, y) {
        var distance;

        if (drag_source.__graph_ui &&
                drag_source.__graph_ui.__dragged &&
                drag_source.__graph_ui.__dragged.link) {

            var link_ui = drag_source.__graph_ui.__dragged;

            var self_xy = this.graph_ui.get_node_graph_coordinates(this);

            // If we're told to animate the inbound connectors and it is the link's end
            // being dragged -- check whether the link's end is currently within the
            // defined distance ('connector_animation_trigger_offset') from the node.
            // If it is -- repaint.
            if (this.in_connector_highlight_animated && link_ui.dragged_end) {
                distance = this.connector_animation_trigger_offset;

                if ((link_ui.x + link_ui.end_x > self_xy.x - distance) &&
                        (link_ui.x + link_ui.end_x < self_xy.x + this.width + distance) &&
                        (link_ui.y + link_ui.end_y > self_xy.y - distance) &&
                        (link_ui.y + link_ui.end_y < self_xy.y + this.height + distance)) {

                    this.repaint();
                }
            }

            // The same for outbound connectors.
            if (this.out_connector_highlight_animated && link_ui.dragged_start) {
                distance = this.connector_animation_trigger_offset;

                if ((link_ui.x + link_ui.start_x > self_xy.x - distance) &&
                        (link_ui.x + link_ui.start_x < self_xy.x + this.width + distance) &&
                        (link_ui.y + link_ui.start_y > self_xy.y - distance) &&
                        (link_ui.y + link_ui.start_y < self_xy.y + this.height + distance)) {

                    this.repaint();
                }
            }
        }
    },

    dt_drag_finished: function(drag_source, x, y) {
        this.__link_is_dragged = false;
        this.__highlight_in_connectors = false;
        this.__highlight_out_connectors = false;

        this.repaint();
    },

    // Clickable -------------------------------------------------------------------------
    cl_accept_click: function(x, y, event) {
      //  if (this.__is_selected) {
            var point = this.__get_point_relative_to_node(x, y);

            var x1 = this.toolbar_x;
            var y1 = this.toolbar_y;

            var x2 = x1
            var y2 = y1;

            var items = this.__get_toolbar_items();
            for (var i = 0; i < items.length; i++) {
                x1 = x2;

                x2 = x2 - items[i].icon_width - this.toolbar_spacing_x;
                y2 = this.toolbar_y + items[i].icon_height;

                if ((point.x > x2) && (point.x < x1) &&
                        (point.y > y1) && (point.y < y2)) {

                    return true;
                }
            }
   //     }
        return false;
    },

    cl_handle_click: function(x, y, event, options) {
        var point = this.__get_point_relative_to_node(x, y);

        var x1 = this.toolbar_x;
        var y1 = this.toolbar_y;

        var x2 = x1
        var y2 = y1;

        var items = this.__get_toolbar_items();
        for (var i = 0; i < items.length; i++) {
            x1 = x2;

            x2 = x2 - items[i].icon_width - this.toolbar_spacing_x;
            y2 = this.toolbar_y + items[i].icon_height;

            if ((point.x > x2) && (point.x < x1) &&
                    (point.y > y1) && (point.y < y2)) {

                items[i].callback(event, options);
                break;
            }
        }
    },

    cl_accept_doubleclick: function(x, y, event) {
        if (this.sl_accept_selection(x, y, event)) {
            return true;
        }

        return false;
    },

    cl_handle_doubleclick: function(x, y, event, options) {
        this.__tbar_edit_properties(options);
    },

    // Miscellanea -----------------------------------------------------------------------
    get_toolbar_item_coordinates: function(codename_or_index) {
        var items = this.__get_toolbar_items();

        var item_by_index = null;
        var item_by_name = null;

        var x = this.toolbar_x;
        var y = this.toolbar_y;

        for (var i = 0; i < items.length; i++) {
            if (items[i].name == codename_or_index) {
                item_by_name = items[i];
                break;
            }

            if (i == codename_or_index) {
                item_by_index = items[i];
                break;
            }

            x = x - this.toolbar_spacing_x - items[i].icon_width;
        }

        if (item_by_name) {
            return {
                x: x,
                y: y,
                width: item_by_name.icon_width,
                height: item_by_name.icon_height
            }
        }

        if (item_by_index) {
            return {
                x: x,
                y: y,
                width: item_by_index.icon_width,
                height: item_by_index.icon_height
            }
        }

        return null;
    },

    show_properties_sheet: function() {
        this.__tbar_edit_properties({});
    },

    // Protected -------------------------------------------------------------------------
    /**
     * Calculates the absolute coordinates of a point located on the line which is
     * orthogonal to the node shape and intersects it at the given point. The distance of
     * the calculated point to the shape (the point of intersection) is given as a
     * parameter.
     */
    __get_orth_point: function(point_on_shape, orth_length) {
        var cp1_x, cp1_y, cp2_x, cp2_y;

        var PI_2 = 2 * Math.PI;
        var angle_step = Math.PI / 36; // 5 degrees
        var search_radius = 5;

        var x = point_on_shape.x;
        var y = point_on_shape.y;

        var points_within_shape = false;

        if (this.check_point_node_path(x + search_radius, y)) {
            points_within_shape = true;
        }

        for (var angle = 0; angle < PI_2; angle += angle_step) {
            if (this.check_point_node_path(
                    x + (search_radius * Math.cos(angle)),
                    y + (search_radius * Math.sin(angle)))) {

                if (!points_within_shape) {
                    if (!cp1_x) {
                        cp1_x = x + (search_radius * Math.cos(angle));
                        cp1_y = y + (search_radius * Math.sin(angle));
                    } else {
                        cp2_x = x + (search_radius * Math.cos(angle - angle_step));
                        cp2_y = y + (search_radius * Math.sin(angle - angle_step));

                        break;
                    }

                    points_within_shape = true;
                }
            } else {
                if (points_within_shape) {
                    if (!cp1_x) {
                        cp1_x = x + (search_radius * Math.cos(angle));
                        cp1_y = y + (search_radius * Math.sin(angle));
                    } else {
                        cp2_x = x + (search_radius * Math.cos(angle - angle_step));
                        cp2_y = y + (search_radius * Math.sin(angle - angle_step));

                        break;
                    }

                    points_within_shape = false;
                }
            }
        }

        var x_ab = (cp1_x + cp2_x) / 2;
        var y_ab = (cp1_y + cp2_y) / 2;

        var r = Math.sqrt(
                Math.pow((cp1_x - cp2_x) / 2, 2) + Math.pow((cp1_y - cp2_y) / 2, 2));

        var cos = (points_within_shape ? 1 : -1) * ((x_ab - cp2_x) / r);
        var sin = (points_within_shape ? 1 : -1) * ((y_ab - cp2_y) / r);

        return {
           x: orth_length * sin + x_ab,
           y: -(orth_length * cos) + y_ab
        };
    },

    /**
     * For the given absolute (browser window) point, returns the coordinates relative to
     * the node element. I.e. if the node is located at (100; 200), the point (150, 100)
     * will be translated into (50, -100).
     *
     * @see #__get_point_relative_to_graph()
     */
    __get_point_relative_to_node: function(x, y) {
        if (this.node.parent) {
            var parent = this.node.parent;
            var point = parent.ui.__get_point_relative_to_node(x, y);

            return {
                x: point.x - parent.ui.margin - parent.ui.inner_margin - this.x,
                y: point.y - parent.ui.margin - parent.ui.inner_margin - this.y
            }
        } else {
            var offset = Element.cumulativeOffset(this.element);
            var scroll_offset = Element.cumulativeScrollOffset(this.element);

            return {
                x: x - offset.left + scroll_offset.left,
                y: y - offset.top + scroll_offset.top
            };
        }
    },

    /**
     * For the given absolute (browser window) point, returns the coordinates relative to
     * the graph element.
     *
     * @see #__get_point_relative_to_node()
     */
    __get_point_relative_to_graph: function(x, y) {
        var offset = Element.cumulativeOffset(this.graph_ui.element);
        var scroll_offset = Element.cumulativeScrollOffset(this.graph_ui.element);

        return {
            x: x - offset.left + scroll_offset.left,
            y: y - offset.top + scroll_offset.top
        };
    },

    /**
     * Handles the Delete action of the standard node toolbar. Effectively this means
     * that the node this UI belongs to is removed from the graph.
     */
    __tbar_delete: function() {
        if (this.node.graph.ui.tooltip) {
            this.node.graph.ui.tooltip.hide();
        }
        this.node.graph.remove_node(this.node);

        // If there is a properties sheet (either open or closed/hidden), we need to
        // remove it as well.
        if (this.node.__ws_psheet_window) {
            this.node.__ws_psheet_window.ws_fwindow.remove();
            document.body.removeChild(this.node.__ws_psheet_window);
        }
    },

    /**
     * Handles the Edit Properties action of the stardard node toolbar. It shows the
     * properties sheet for the node in a floating window (windowed properties sheet, to
     * be precise), unless it is already shown.
     */
    __tbar_edit_properties: function(options) {
        var dimensions = Ws.Utils.get_window_dimensions();

        var x = (dimensions.width / 2) - 250;
        var y = (dimensions.height / 2) - 200;

        if (x < 0) x = 0;
        if (y < 0) y = 0;

        if (y < 40) y = 40;

        if (this.node.__ws_psheet) {
            this.node.__ws_psheet_window.select(".-wsfw-header")[0].innerHTML =
                    "Properties &mdash; " + this.node.ui_properties.display_name;
            
            this.node.__ws_psheet_window.setStyle({
                display: "block"
            });

            this.node.__ws_psheet.reset();
        } else {
            var psheet_window = new Element("div");

            psheet_window.addClassName("-ws-psheet-window");
            psheet_window.setStyle({
                top: y + "px",
                left: x + "px"
            });

            var html =
                "<div class=\"-wsfw-header\">" +
                    "Properties &mdash; " + this.node.ui_properties.display_name +
                "</div>" +
                "<div class=\"-wsfw-contents\">" +
                "</div>" +
                "<div class=\"-wsfw-footer\">";

            if (this.read_only) {
                html +=
                    "<input type=\"button\" class=\"Close\" value=\"Close\"/>";
            } else {
                html +=
                    "<input type=\"button\" class=\"Save\" value=\"Save\"/>" +
                    "<input type=\"button\" class=\"Cancel\" value=\"Cancel\"/>";
            }

            html += "</div>";

            psheet_window.innerHTML = html;
            document.body.appendChild(psheet_window);

            new Ws.WindowedPropertiesSheet(psheet_window, this.node, options);

            if (this.read_only) {
                psheet_window.select("input.Close")[0].onclick = function() {
                    psheet_window.setStyle({
                        display: "none"
                    });
                }
            } else {
                psheet_window.select("input.Save")[0].onclick = function() {
                    psheet_window.ws_psheet.save();
                    psheet_window.setStyle({
                        display: "none"
                    });
                }
                psheet_window.select("input.Cancel")[0].onclick = function() {
                    psheet_window.setStyle({
                        display: "none"
                    });
                }
            }

            // Below we take into account that Ws.FloatinWindow uses Ws.RoundedRectangle
            // internally, which would wrap the contents of psheet_window into yet
            // another <div>.
            this.node.__ws_psheet_window = psheet_window;
            this.node.__ws_psheet = psheet_window.ws_psheet;

            var psheet_id = psheet_window.ws_psheet.element.id;

            if (this.read_only) {
                psheet_window.select("input.Close")[0].id = psheet_id + "-close-button";
            } else {
                psheet_window.select("input.Save")[0].id = psheet_id + "-save-button";
                psheet_window.select("input.Cancel")[0].id = psheet_id + "-cancel-button";
            }
        }
    },

    /**
     * Handles the Create Shortcut action of the standard node toolbar. It creates a
     * shortcut node, somewhere near the original one. The position is not defined
     * precisely, we just look for a vacant place.
     */
    __tbar_create_shortcut: function() {
        var node = this.node.graph.create_and_add_node(
                this.node.type,
                {
                    shortcut_to: this.node
                });
        node.ui.move_to(
                this.x + this.width - this.margin,
                this.y);
    },

    /**
     * Draws a non-highlighted inbound connector. This method uses both the settings
     * in the UI object (mostly connector drawing style) as well as those passed in via
     * the connector object (connector position, etc).
     *
     * @see #__draw_out_connector()
     * @see #__draw_highlighted_in_connector()
     */
    __draw_in_connector: function(connector, index) {
        this.context.fillStyle = this.in_connector_fill_style;
        this.context.strokeStyle = this.in_connector_stroke_style;
        this.context.lineWidth = this.in_connector_line_width;

        this.context.beginPath();
        this.context.arc(
            connector.x,
            connector.y,
            this.in_connector_radius,
            0,
            Math.PI * 2,
            true);
        this.context.fill();
        this.context.stroke();
    },

    /**
     * Draws a highlighted inbound connector.
     *
     * @see #__draw_in_connector()
     * @see #__draw_highlighted_out_connector()
     */
    __draw_highlighted_in_connector: function(connector, index) {
        var inner_radius = this.in_connector_radius;
        var outer_radius = this.in_connector_radius_highlighted;

        var animation_proportion = -1;

        // If we're told to animate the connector highlight (i.e. add fill color as the
        // link is approaching.)
        if (this.in_connector_highlight_animated && this.__link_is_dragged) {
            var link_ui = this.node.graph.ui.__dragged;
            var self_xy = this.graph_ui.get_node_graph_coordinates(this);

            if (link_ui.dragged_end) {
                // Get the link's end and connector's coordinates within graph's space.
                // Then calculate the distance between the two.
                var d_x = (link_ui.x + link_ui.end_x) - (self_xy.x + connector.x);
                var d_y = (link_ui.y + link_ui.end_y) - (self_xy.y + connector.y);

                animation_proportion = (1 - (Math.sqrt(d_x * d_x + d_y * d_y) /
                        this.in_connector_highlight_animated_distance));
            }
        }

        // Animate the outer radius of the connector.
        if (animation_proportion > 0) {
            outer_radius +=
                    this.in_connector_radius_highlighted_active * animation_proportion;
        }

        // Draw the connector itself (a small circle within a bigger one.)
        this.context.fillStyle = this.in_connector_fill_style_highlighted;
        this.context.strokeStyle = this.in_connector_stroke_style_highlighted;
        this.context.lineWidth = this.in_connector_line_width_highlighted;

        this.context.beginPath();
        this.context.arc(
            connector.x,
            connector.y,
            inner_radius,
            0,
            Math.PI * 2,
            true);
        this.context.fill();
        this.context.stroke();

        this.context.beginPath();
        this.context.arc(
            connector.x,
            connector.y,
            outer_radius,
            0,
            Math.PI * 2,
            true);
        this.context.stroke();

        // Animate the fill of the connector.
        if (animation_proportion > 0) {
            var alpha = 
                    animation_proportion * this.in_connector_highlight_animated_opacity;

            var old_alpha = this.context.globalAlpha;

            this.context.globalAlpha = alpha;
            this.context.fill();
            this.context.globalAlpha = old_alpha;
        }

        // Draw a link stub, if there are no links coming to this connector
        if (this.node.get_inbound_links(index).length == 0) {
            var arrow_length = this.in_connector_arrow_length;
            var arrow_head_length = this.in_connector_arrow_head_length;
            var arrow_head_height = this.in_connector_arrow_head_height;

            // Arrow stub:
            //       C\
            // A-------B
            //       D/

            switch (connector.type) {
                case Ws.Graph.NODE_CONNECTOR_WEST:
                    A_x = connector.x - outer_radius - arrow_length;
                    A_y = connector.y;
                    B_x = connector.x - outer_radius;
                    B_y = connector.y;
                    C_x = connector.x - outer_radius - arrow_head_length;
                    C_y = connector.y - arrow_head_height;
                    D_x = connector.x - outer_radius - arrow_head_length;
                    D_y = connector.y + arrow_head_height;
                    break;
                case Ws.Graph.NODE_CONNECTOR_EAST:
                    A_x = connector.x + outer_radius + arrow_length;
                    A_y = connector.y;
                    B_x = connector.x + outer_radius;
                    B_y = connector.y;
                    C_x = connector.x + outer_radius + arrow_head_length;
                    C_y = connector.y - arrow_head_height;
                    D_x = connector.x + outer_radius + arrow_head_length;
                    D_y = connector.y + arrow_head_height;
                    break;
                case Ws.Graph.NODE_CONNECTOR_NORTH:
                    A_x = connector.x;
                    A_y = connector.y - outer_radius - arrow_length;
                    B_x = connector.x;
                    B_y = connector.y - outer_radius;
                    C_x = connector.x - arrow_head_height;
                    C_y = connector.y - outer_radius - arrow_head_length;
                    D_x = connector.x + arrow_head_height;
                    D_y = connector.y - outer_radius - arrow_head_length;
                    break;
                case Ws.Graph.NODE_CONNECTOR_SOUTH:
                    A_x = connector.x;
                    A_y = connector.y + outer_radius + arrow_length;
                    B_x = connector.x;
                    B_y = connector.y + outer_radius;
                    C_x = connector.x - arrow_head_height;
                    C_y = connector.y + outer_radius + arrow_head_length;
                    D_x = connector.x + arrow_head_height;
                    D_y = connector.y + outer_radius + arrow_head_length;
                    break;
                case Ws.Graph.NODE_CONNECTOR_ORTHOGONAL:
                case Ws.Graph.NODE_CONNECTOR_FREEFORM:
                default:
                    var cp = this.__get_orth_point(
                            connector, outer_radius + arrow_length);

                    var sin = (cp.y - connector.y) / (outer_radius + arrow_length);
                    var cos = (cp.x - connector.x) / (outer_radius + arrow_length);

                    var p = {
                        x: connector.x + (outer_radius * cos),
                        y: connector.y + (outer_radius * sin)
                    }

                    var head_points = Ws.Utils.get_arrow_head_coordinates(
                            cp, p, arrow_head_length, arrow_head_height);

                    A_x = cp.x;
                    A_y = cp.y;
                    B_x = p.x;
                    B_y = p.y;
                    C_x = head_points.C.x;
                    C_y = head_points.C.y;
                    D_x = head_points.D.x;
                    D_y = head_points.D.y;

                    break;
            }

            this.context.beginPath();
            this.context.moveTo(A_x, A_y);
            this.context.lineTo(B_x, B_y);
            this.context.lineTo(C_x, C_y);
            this.context.lineTo(D_x, D_y);
            this.context.lineTo(B_x, B_y);
            this.context.fill();
            this.context.stroke();
        }
    },

    /**
     * Draws a non-highlighted outbound connector.
     *
     * @see #__draw_in_connector()
     * @see #__draw_highlighted_out_connector()
     */
    __draw_out_connector: function(connector, index) {
        this.context.fillStyle = this.out_connector_fill_style;
        this.context.strokeStyle = this.out_connector_stroke_style;
        this.context.lineWidth = this.out_connector_line_width;

        this.context.beginPath();
        this.context.arc(
            connector.x,
            connector.y,
            this.out_connector_radius,
            0,
            Math.PI * 2,
            true);
        this.context.fill();
        this.context.stroke();
    },

    /**
     * Draws a highlighted outbound connector.
     *
     * @see #__draw_out_connector()
     * @see #__draw_highlighted_in_connector()
     */
    __draw_highlighted_out_connector: function(connector, index) {
        var inner_radius = this.out_connector_radius;
        var outer_radius = this.out_connector_radius_highlighted;

        var animation_proportion = -1;

        // If we're told to animate the connector highlight (i.e. add fill color as the
        // link is approaching.)
        if (this.in_connector_highlight_animated && this.__link_is_dragged) {
            var link_ui = this.node.graph.ui.__dragged;
            var self_xy = this.graph_ui.get_node_graph_coordinates(this);

            if (link_ui.dragged_start) {
                // Get the link's end and connector's coordinates within graph's space.
                // Then calculate the distance between the two.
                var d_x = (link_ui.x + link_ui.start_x) - (self_xy.x + connector.x);
                var d_y = (link_ui.y + link_ui.start_y) - (self_xy.y + connector.y);

                animation_proportion = (1 - (Math.sqrt(d_x * d_x + d_y * d_y) /
                        this.out_connector_highlight_animated_distance));
            }
        }

        // Animate the outer radius of the connector.
        if (animation_proportion > 0) {
            outer_radius +=
                    this.out_connector_radius_highlighted_active * animation_proportion;
        }

        this.context.fillStyle = this.out_connector_fill_style_highlighted;
        this.context.strokeStyle = this.out_connector_stroke_style_highlighted;
        this.context.lineWidth = this.out_connector_line_width_highlighted;

        this.context.beginPath();
        this.context.arc(
            connector.x,
            connector.y,
            inner_radius,
            0,
            Math.PI * 2,
            true);
        this.context.fill();
        this.context.stroke();

        this.context.beginPath();
        this.context.arc(
            connector.x,
            connector.y,
            outer_radius,
            0,
            Math.PI * 2,
            true);
        this.context.stroke();

        // Animate the fill of the connector.
        if (animation_proportion > 0) {
            var alpha =
                    animation_proportion * this.out_connector_highlight_animated_opacity;

            var old_alpha = this.context.globalAlpha;

            this.context.globalAlpha = alpha;
            this.context.fill();
            this.context.globalAlpha = old_alpha;
        }

        // Draw the link stub if there are no links going out of the connector.
        if (this.node.get_outbound_links(index).length == 0) {
            var arrow_length = this.out_connector_arrow_length;
            var arrow_head_length = this.out_connector_arrow_head_length;
            var arrow_head_height = this.out_connector_arrow_head_height;

            // Arrow stub:
            //       C\
            // A-------B
            //       D/

            switch (connector.type) {
                case Ws.Graph.NODE_CONNECTOR_WEST:
                    A_x = connector.x - outer_radius
                    A_y = connector.y;
                    B_x = connector.x - outer_radius - arrow_length;
                    B_y = connector.y;
                    C_x = connector.x - outer_radius - arrow_length + arrow_head_length;
                    C_y = connector.y - arrow_head_height;
                    D_x = connector.x - outer_radius - arrow_length + arrow_head_length;
                    D_y = connector.y + arrow_head_height;
                    break;
                case Ws.Graph.NODE_CONNECTOR_EAST:
                    A_x = connector.x + outer_radius;
                    A_y = connector.y;
                    B_x = connector.x + outer_radius + arrow_length;
                    B_y = connector.y;
                    C_x = connector.x + outer_radius + arrow_length - arrow_head_length;
                    C_y = connector.y - arrow_head_height;
                    D_x = connector.x + outer_radius + arrow_length - arrow_head_length;
                    D_y = connector.y + arrow_head_height;
                    break;
                case Ws.Graph.NODE_CONNECTOR_NORTH:
                    A_x = connector.x;
                    A_y = connector.y - outer_radius;
                    B_x = connector.x;
                    B_y = connector.y - outer_radius - arrow_length;
                    C_x = connector.x - arrow_head_height;
                    C_y = connector.y - outer_radius - arrow_length + arrow_head_length;
                    D_x = connector.x + arrow_head_height;
                    D_y = connector.y - outer_radius - arrow_length + arrow_head_length;
                    break;
                case Ws.Graph.NODE_CONNECTOR_SOUTH:
                    A_x = connector.x;
                    A_y = connector.y + outer_radius;
                    B_x = connector.x;
                    B_y = connector.y + outer_radius + arrow_length;
                    C_x = connector.x - arrow_head_height;
                    C_y = connector.y + outer_radius + arrow_length - arrow_head_length;
                    D_x = connector.x + arrow_head_height;
                    D_y = connector.y + outer_radius + arrow_length - arrow_head_length;
                    break;
                case Ws.Graph.NODE_CONNECTOR_ORTHOGONAL:
                case Ws.Graph.NODE_CONNECTOR_FREEFORM:
                default:
                    var cp = this.__get_orth_point(
                            connector, outer_radius + arrow_length);
                    var sin = (cp.y - connector.y) / (outer_radius + arrow_length);
                    var cos = (cp.x - connector.x) / (outer_radius + arrow_length);

                    var p = {
                        x: connector.x + (outer_radius * cos),
                        y: connector.y + (outer_radius * sin)
                    }

                    var head_points = Ws.Utils.get_arrow_head_coordinates(
                            p, cp, arrow_head_length, arrow_head_height);

                    A_x = p.x;
                    A_y = p.y;
                    B_x = cp.x;
                    B_y = cp.y;
                    C_x = head_points.C.x;
                    C_y = head_points.C.y;
                    D_x = head_points.D.x;
                    D_y = head_points.D.y;

                    break;
            }

            this.context.beginPath();
            this.context.moveTo(A_x, A_y);
            this.context.lineTo(B_x, B_y);
            this.context.lineTo(C_x, C_y);
            this.context.lineTo(D_x, D_y);
            this.context.lineTo(B_x, B_y);
            this.context.fill();
            this.context.stroke();
        }
    },

    /**
     * Checks whether the given link can be connected to the given connector (we
     * additionally supply information about the connector's index and its direction,
     * either inbound or outbound.)
     */
    __check_link_vs_connector: function(link, connector, connector_index, is_inbound) {
        // First check whether the link is valid for connecting to this node. It would
        // not be valid, if:
        // Case 1: the node has a parent, the link is *not* connected to any of
        // its children (a completely disconnected link does qualify, however)
        // and the policy is to disallow connecting to the outside world.
        // Case 2: the connector defined the list of link types compatible with it and
        // the current link type is not in that list.
        // Case 3: the connector defines a maximum number of links that can be connected
        // to it and that number was already reached.

        if (!is_inbound && link.end_node && (this.node.parent || link.end_node.parent) &&
                (this.node.parent != link.end_node.parent)) {

            return false;
        }

        if (is_inbound && link.start_node && (this.node.parent || link.start_node.parent) &&
                (this.node.parent != link.start_node.parent)) {

            return false;
        }

        if (connector.supported_link_types) {
            if (connector.supported_link_types.indexOf(link.type) == -1) {
                return false;
            }
        }

        if (connector.max_links && (connector.max_links > 0)) {
            var links_count = 0;

            if (is_inbound) {
                links_count = this.node.get_inbound_links(connector_index).length;
            } else {
                links_count = this.node.get_outbound_links(connector_index).length;
            }

            if (links_count >= connector.max_links) {
                return false;
            }
        }

        return true;
    },

    /**
     * Returns the current toolbar items for this node. The list is dynamic and depends,
     * for example on whether the node is currently read only or read-write.
     */
    __get_toolbar_items: function() {
        var self = this;
        var items = [];

        if (!this.read_only) {
            items.push({
                name: "delete",
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        items.push({
            name: "properties",
            icon: Ws.Graph.NODE_TBAR_EDIT_PROPERTIES,
            icon_d : Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE,
            icon_width: 16,
            icon_height: 16,
            tooltip: "Properties",
            callback: function(event, options) {
                self.__tbar_edit_properties(options);
            }
        });

        if (!this.read_only) {
            items.push({
                name: "create-shortcut",
                icon: Ws.Graph.NODE_TBAR_CREATE_SHORTCUT,
                icon_d : Ws.Graph.NODE_TBAR_CREATE_SHORTCUT_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Create Shortcut",
                callback: function(event) {
                    self.__tbar_create_shortcut();
                }
            });
        }

        return items;
    },

    /**
     * Truncates the given text (replacing the tail with an ellipsis) according to the
     * given measurement function.
     */
    __truncate_text: function(text, measure, text_offset, text_margin, text_align) {
        var truncated = text;

        var text_does_not_fit = true;
        while (text_does_not_fit) {
            var width, fits_left, fits_right;

            switch (text_align) {
                case "left":
                    width = measure(truncated);

                    fits_left = true;
                    fits_right =
                        text_offset + width <= this.width - this.margin - text_margin;
                    break;
                case "right":
                    width = measure(truncated);

                    fits_left = text_offset - width >= this.margin + text_margin;
                    fits_right = true;
                    break;
                default:
                    width = measure(truncated) / 2;

                    fits_left =
                            text_offset - width >= this.margin + text_margin;
                    fits_right =
                            text_offset + width <= this.width - this.margin - text_margin;
                    break;
            }

            if (fits_left && fits_right) {
                text_does_not_fit = false;
            } else {
                var space_index = truncated.lastIndexOf(" ");
                if (space_index != -1) {
                    truncated = truncated.slice(0, space_index) + "...";
                } else {
                    var ellipsis_on_end =
                            truncated.lastIndexOf("...") == truncated.length - 3;

                    if (ellipsis_on_end) {
                        // If the length of the string is 4 (i.e. the first letter and
                        // an ellypsis) return it as is, as there is nothing we can do
                        // about it.)
                        if (truncated.length == 4) {
                            return truncated;
                        }

                        truncated = truncated.slice(0, truncated.length - 4) + "...";
                    } else {
                        truncated = truncated.slice(0, truncated.length - 1) + "...";
                    }
                }
            }
        }

        return truncated;
    },

    __reset_tooltip: function() {
        this.tooltip.set_html_tooltip(this.__get_tooltip_text());
    },

    __get_tooltip_text: function() {
        return "" +
            "<table>" +
              "<tr>" +
                "<td class='name'> name: </td>" +
                "<td>" + this.display_name + "</td>" +
              "</tr>" +
              "<tr>" +
                "<td class='name'>description: </td>" +
                "<td>" + this.description + "</td>" +
              "</tr>" +
            "</table>";
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder
    }
});

//////////////////////////////////////////////////////////////////////////////////////////
// Constants
Ws.Graph.DEFAULT_NODE_TYPE = "default";

// Whether tooltips are enabled or not (it is usefult o disable them for, say, tests. ----
Ws.Graph.NODE_TOOLTIPS_ENABLED = true;

// Default toolbar buttons ---------------------------------------------------------------
Ws.Graph.NODE_TBAR_DELETE = "img/tbar-delete.png";
Ws.Graph.NODE_TBAR_EDIT_PROPERTIES = "img/tbar-properties.png";
Ws.Graph.NODE_TBAR_CREATE_SHORTCUT = "img/tbar-shortcut.png";
Ws.Graph.NODE_TBAR_DELETE_DESATURATE = "img/tbar-delete_d.png";
Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE = "img/tbar-properties_d.png";
Ws.Graph.NODE_TBAR_CREATE_SHORTCUT_DESATURATE = "img/tbar-shortcut_d.png";

// Default badges icons ------------------------------------------------------------------
Ws.Graph.NODE_BADGE_SHORTCUT = "img/badge-shortcut.png";

// Connector orientation types -----------------------------------------------------------
Ws.Graph.NODE_CONNECTOR_WEST = "west"; // horizontally to the left
Ws.Graph.NODE_CONNECTOR_EAST = "east"; // horizontally to the right
Ws.Graph.NODE_CONNECTOR_NORTH = "north"; // vertically to the top
Ws.Graph.NODE_CONNECTOR_SOUTH = "south"; // vertically to the bottom
Ws.Graph.NODE_CONNECTOR_ORTHOGONAL = "orthogonal"; // orthogonally to the node's path
Ws.Graph.NODE_CONNECTOR_FREEFORM = "freeform"; // to the other end of the link

// Text overlay styles -------------------------------------------------------------------
Ws.Graph.OVERLAY_CANVASTEXT = "canvas-text";
Ws.Graph.OVERLAY_HTML_DIV = "html-div";
Ws.Graph.OVERLAY_BROWSER_API = "browser-api";

// Toolbar directions --------------------------------------------------------------------
Ws.Graph.TOOLBAR_RIGHT_TO_LEFT = "right-to-left";
Ws.Graph.TOOLBAR_LEFT_TO_RIGHT = "left-to-right";
Ws.Graph.TOOLBAR_TOP_TO_BOTTOM = "top-to-bottom";
Ws.Graph.TOOLBAR_BOTTOM_TO_TOP = "bottom-to-top";

//////////////////////////////////////////////////////////////////////////////////////////
// Static initialization
Ws.Graph.NodeFactory.register_type(
        Ws.Graph.DEFAULT_NODE_TYPE, Ws.Graph.DefaultNode);
Ws.Graph.NodeUiFactory.register_type(
        Ws.Graph.DEFAULT_NODE_TYPE, Ws.Graph.DefaultNodeUi);
