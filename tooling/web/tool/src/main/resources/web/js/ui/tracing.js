/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */

//////////////////////////////////////////////////////////////////////////////////////////
// tracing

var tracing_enabled = false;
var tracing_data = {};
var tracing_timeout = null;
var tracing_messages_list_dialogs = {};
var tracing_message_dialogs = {};

function toggle_tracing(control) {
    if (!tracing_enabled) {
        tracing_enabled = true;
        get_tracing_data(true);
        $('tracingToolbar').setAttribute("src", "resource?img/ui/tracing/stop_tests_24.png");
    } else {
        tracing_enabled = false;
        clearTimeout(tracing_timeout);
        get_tracing_data(false, true);
        $('tracingToolbar').setAttribute("src", "resource?img/ui/tracing/run_tests_24.png");
    }

    $('diagram').ws_graph.ui.repaint();
}

function get_tracing_data(start, stop) {
    var request = {};
    if (start) {
        request.start = start;
    }
    if (stop) {
        request.stop = stop;
    }
    //request = Object.toJSON(request);

    new Ajax.Request("callbacks/get-tracing-data", {
        method: "get",
        parameters: request,

        onSuccess: function(response) {

            process_tracing_data(response.responseJSON.messages);


            if (tracing_enabled) {
                tracing_timeout = setTimeout(get_tracing_data, 1000);
            }
        },

        onFailure: function(response) {
            // Does nothing
        },

        onComplete: function(response) {
            // Does nothing
        }
    });
}

function clear_tracing_data() {
    var i, j;

    for (i in tracing_data) {
        tracing_data[i] = null;
        $('diagram').ws_graph.get_node_by_id(i).ui.repaint();
    }

    for (i in tracing_messages_list_dialogs) {
        if (tracing_messages_list_dialogs[i].inbound) {
            tracing_messages_list_dialogs[i].inbound.parentNode.removeChild(
                tracing_messages_list_dialogs[i].inbound);
        }
        if (tracing_messages_list_dialogs[i].outbound) {
            tracing_messages_list_dialogs[i].outbound.parentNode.removeChild(
                tracing_messages_list_dialogs[i].outbound);
        }
    }

    for (i in tracing_message_dialogs) {
        if (tracing_message_dialogs[i].inbound) {
            for (j = 0; j < tracing_message_dialogs[i].inbound.length; j++) {
                if (tracing_message_dialogs[i].inbound[j]) {
                    tracing_message_dialogs[i].inbound[j].parentNode.removeChild(
                        tracing_message_dialogs[i].inbound[j]);
                }
            }
        }
        if (tracing_message_dialogs[i].outbound) {
            for (j = 0; j < tracing_message_dialogs[i].outbound.length; j++) {
                if (tracing_message_dialogs[i].outbound[j]) {
                    tracing_message_dialogs[i].outbound[j].parentNode.removeChild(
                        tracing_message_dialogs[i].outbound[j]);
                }
            }
        }
    }

    tracing_data = {};
}

function process_tracing_data(messages) {
    if (messages[messages.length - 1] == null) {
        messages.pop();
    }

    for (var i = 0; i < messages.length; i++) {
        var message = messages[i];
        var service = message.service;
        var inbound = message.inbound;
        var old_debugging_current_service;

        if (!tracing_data[service]) {
            tracing_data[service] = {
                inbound: [],
                outbound: []
            }
        }

        if (inbound) {
            tracing_data[service].inbound.push(message);
            if (message.has_fault) {
                tracing_data[service].inbound.has_fault = true;
            }
            tracing_data[service].inbound.has_new = true;

            if (tracing_messages_list_dialogs[service]) {
                var dialog = tracing_messages_list_dialogs[service].inbound;
                if (dialog && dialog.visible()) {
                    dialog.messages_viewer.render();
                }
            }

            if (debugging_enabled) {
                // Check whether there is a breakpoint, or we were instructed to just
                // step-by-message.
                if ((debugging_breakpoints[service] && debugging_breakpoints[service].inbound) || debugging_step_by_message) {
                    if (debugging_current_message) {
                        old_debugging_current_service = debugging_current_message.service;
                    }

                    debugging_current_message = messages[i];
                    debug_dialog.ws_psheet.reset();

                    debugging_step_by_message = false;
                    toggle_tracing();
                }
            }
        } else {
            tracing_data[service].outbound.push(message);
            if (message.has_fault) {
                tracing_data[service].outbound.has_fault = true;
            }
            tracing_data[service].outbound.has_new = true;

            if (tracing_messages_list_dialogs[service]) {
                var dialog = tracing_messages_list_dialogs[service].outbound;
                if (dialog && dialog.visible()) {
                    dialog.messages_viewer.render();
                }
            }

            if (debugging_enabled) {
                // Check whether there is a breakpoint, or we were instructed to just
                // step-by-message.
                if ((debugging_breakpoints[service] && debugging_breakpoints[service].outbound) || debugging_step_by_message) {
                    if (debugging_current_message) {
                        old_debugging_current_service = debugging_current_message.service;
                    }

                    debugging_current_message = message;
                    debug_dialog.ws_psheet.reset();

                    debugging_step_by_message = false;
                    toggle_tracing();
                }
            }
        }

        if (old_debugging_current_service) {
            $('diagram').ws_graph.get_node_by_id(old_debugging_current_service).ui.repaint();
        }

        var node = $('diagram').ws_graph.get_node_by_id(service);
        node.ui.repaint();

        if (node.__ws_psheet) {
            node.__ws_psheet.reset();
//            node.__ws_psheet_window.windowed_ps_sheet.relayout();
        }
    }
}

function create_tracing_messages_list_container(node_id, inbound) {
    return {
        pc_get_descriptors: function() {
            var descriptor = {
                properties: [
                {
                    name: "messages",
                    type: Ws.PropertiesSheet.TABLE,

                    display_name: "Messages",
                    description: "Messages that have passed through this checkpoint.",
                    value: {
                        body_value: null
                    }
                }
                ]
            }

            var messages;

            if (inbound) {
                messages = tracing_data[node_id].inbound;
            } else {
                messages = tracing_data[node_id].outbound;
            }

            var value = descriptor.properties[0].value;

            value.body_value = [];
            for (var i = messages.length - 1; i > -1; i--) {
                var row = [
                {
                    value: "<img src=\"resource?img/ui/tracing/message_16.png\"/> " + (new Date(messages[i].time).toLocaleString()),
                    functions_change: true,
                    functions_change_params: "" + node_id + ", " + inbound + ", " + i
                }
                ]
                if (messages[i].has_fault) {
                    row[0].value = "<img src=\"resource?img/ui/tracing/message_fault_16.png\"/> "
                        + (new Date(messages[i].time).toLocaleString());
                }

                value.body_value.push(row);
            }

            return descriptor;
        },

        pc_save_properties: function(properties) {
        // Does nothing
        }
    }
}

function create_tracing_message_container(node_id, inbound, index) {
    return {
        pc_get_descriptors: function() {
            var message;

            if (inbound) {
                message = tracing_data[node_id].inbound[index];
            } else {
                message = tracing_data[node_id].outbound[index];
            }

            var descriptor = {
                properties: [
                {
                    name: "content",
                    type: Ws.PropertiesSheet.CODE,

                    display_name: "Content",
                    description: "Content of the message.",
                    value: message.content,

                    maximizable: true,
                    auto_adjust_height: true,
                    read_only: true
                }
                ]
            }

            return descriptor;
        },

        pc_save_properties: function(properties) {
        // Does nothing
        }
    }
}

function create_tracing_messages_list_dialog(node_id, inbound) {
    if (!tracing_messages_list_dialogs[node_id]) {
        tracing_messages_list_dialogs[node_id] = {
            inbound: null,
            outbound: null
        }
    }

    var create_dialog = false, dialog;

    if (inbound) {
        if (!tracing_messages_list_dialogs[node_id].inbound) {
            create_dialog = true;
        } else {
            dialog = tracing_messages_list_dialogs[node_id].inbound
        }
    } else {
        if (!tracing_messages_list_dialogs[node_id].outbound) {
            create_dialog = true;
        } else {
            dialog = tracing_messages_list_dialogs[node_id].outbound
        }
    }

    if (create_dialog) {
        dialog = new Element("div");
        dialog.addClassName("palette");
        dialog.addClassName("window_messages_viewer");

        dialog.innerHTML = "" +
        "<div class=\"-wsfw-header\">" + (inbound ? "Inbound" : "Outbound") + 
        " messages for " + $('diagram').ws_graph.get_node_by_id(node_id).ui_properties.display_name
        + "</div>" +
        "<div class=\"-wsfw-contents\"></div>" +
        "<div class=\"-wsfw-footer\">" +
    //    "<input class=\"close\" type=\"button\" value=\"Close\"/>" +
        "</div>";

        document.body.appendChild(dialog);

        var messages = {};
        if (inbound) {
            messages = tracing_data[node_id].inbound;
        } else {
            messages = tracing_data[node_id].outbound;
        }
        new Ws.MessagesViewer({
            messages:messages
        }, dialog);
//        new Ws.WindowedPropertiesSheet(
//            dialog,
//            create_tracing_messages_list_container(node_id, inbound),
//            {
//                functions_change: [{funct: create_tracing_message_dialog, type: "properties"}]
//            }
//            );

//        dialog.select("input.close")[0].onclick = function() {
//            dialog.hide();
//        }

        if (inbound) {
            tracing_messages_list_dialogs[node_id].inbound = dialog;
        } else {
            tracing_messages_list_dialogs[node_id].outbound = dialog;
        }
    } else {
        dialog.show();
        dialog.messages_viewer.render();
        //dialog.ws_psheet.reset();
        
    }
}

function create_tracing_message_dialog(node_id, inbound, index) {
    if (!tracing_message_dialogs[node_id]) {
        tracing_message_dialogs[node_id] = {
            inbound: [],
            outbound: []
        }
    }

    var create_dialog = false, dialog;

    if (inbound) {
        if (!tracing_message_dialogs[node_id].inbound[index]) {
            create_dialog = true;
        } else {
            dialog = tracing_message_dialogs[node_id].inbound[index];
        }
    } else {
        if (!tracing_message_dialogs[node_id].outbound[index]) {
            create_dialog = true;
        } else {
            dialog = tracing_message_dialogs[node_id].outbound[index];
        }
    }

    if (create_dialog) {
        dialog = new Element("div");
        dialog.addClassName("-ws-psheet-window");

        var display_name;
        if (inbound) {
            display_name = new Date(tracing_data[node_id].inbound[index].time).toLocaleString();
        } else {
            display_name = new Date(tracing_data[node_id].outbound[index].time).toLocaleString();
        }

        dialog.innerHTML = "" +
        "<div class=\"-wsfw-header\">" + display_name + "</div>" +
        "<div class=\"-wsfw-contents\"></div>" +
        "<div class=\"-wsfw-footer\">" +
        "<input class=\"close\" type=\"button\" value=\"Close\"/>" +
        "</div>";

        document.body.appendChild(dialog);

        new Ws.WindowedPropertiesSheet(
            dialog,
            create_tracing_message_container(node_id, inbound, index),
            {
            }
            );

        dialog.select("input.close")[0].onclick = function() {
            dialog.hide();
        }

        if (inbound) {
            tracing_message_dialogs[node_id].inbound[index] = dialog;
        } else {
            tracing_message_dialogs[node_id].outbound[index] = dialog;
        }
    } else {
        dialog.ws_psheet.reset();
        dialog.show();
    }
}

Event.observe(window, "load", function() {
    Ws.ImagesLoadManager.load("resource?img/ui/tracing/message_16.png");
    Ws.ImagesLoadManager.load("resource?img/ui/tracing/message_queue_16.png");
    Ws.ImagesLoadManager.load("resource?img/ui/tracing/message_fault_16.png");
    Ws.ImagesLoadManager.load("resource?img/ui/tracing/message_fault_queue_16.png");
});