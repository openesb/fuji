/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.ProgressBar = Class.create({
    num_lines: 12,
    
    fill: "rgb(0, 0, 0)",
    stroke: null,
    stroke_width: null,
    
    angle_step: (Math.PI * 2) / 150,
    
    element: null,
    
    canvas: null,
    angle: null,
    interval: null,
    
    initialize: function(element, options) {
        if (options) Object.extend(this, options);
        
        this.element = element;
        this.canvas = Ws.Utils.create_canvas();
        this.angle = 0;
        
        element.ws_progress_bar = this;
        
        this.create();
    },
    
    create: function() {
        var self = this;

        this.interval = setInterval(function() {
            self.update();
        }, 30);

        this.element.makePositioned();
        this.element.appendChild(this.canvas);
    },
    
    remove: function() {
        try {
            this.element.removeChild(this.canvas);
        } catch (e) {
            // If we get an exception here, this normally means that the canvas has 
            // already been removed, so we can safely ignore it.
        }
        
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    },

    hide: function() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.element.setStyle({
            display: "none"
        });
    },

    show: function() {
        var self = this;
        this.element.setStyle({
            display: ""
        });
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.interval = setInterval(function() {
            self.update();
        }, 30);
    },

    update: function() {
        this.angle += this.angle_step;

        if (this.angle > (Math.PI * 2)) {
            this.angle -= Math.PI * 2;
        }
        
        var width, height;
        
        if (this.width) {
            width = this.width;
        } else {
            width = this.element.offsetWidth;
        }
        
        if (this.height) {
            height = this.height;
        } else {
            height = Element.getHeight(this.element);
        }
        
        var size = height > width ? width : height;
        
        this.canvas.setStyle({
            position: "absolute",
            top: ((Element.getHeight(this.element) / 2) - (size / 2)) + "px",
            left: ((Element.getWidth(this.element) / 2) - (size / 2)) + "px"
        });
        
        Ws.Utils.draw_progress_bar(
                this.canvas, 
                size, 
                this.num_lines, 
                this.angle,
                this.fill,
                this.stroke,
                this.stroke_width);
    },
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});
