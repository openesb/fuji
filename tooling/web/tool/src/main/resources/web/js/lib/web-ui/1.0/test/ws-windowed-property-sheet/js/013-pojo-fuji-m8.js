var pojo_container = {
    pc_get_descriptors: function() {
        var container = {
            sections: [
            {
                display_name: "Basic",
                description: "Basic settings.",
                properties: [
                {
                    name: "properties_java_code",

                    display_name: "Code",
                    description: "Java code which will be executed",
                    value: "package ${serviceName};\n" +
                    "\n" +
                    "import org.glassfish.openesb.pojose.api.ErrorMessage;\n" +
                    "import org.glassfish.openesb.pojose.api.annotation.Provider;\n" +
                    "import org.glassfish.openesb.pojose.api.annotation.Resource;\n" +
                    "import org.glassfish.openesb.pojose.api.annotation.Operation;\n" +
                    "import org.glassfish.openesb.pojose.api.res.Context;\n" +
                    "\n" +
                    "@Provider(name=\"${serviceName}_endpoint\", serviceQN=\"{http://fuji.dev.java.net/application/${applicationId}}${serviceName}\",\n" +
                    "        interfaceQN=\"{http://fuji.dev.java.net/application/${applicationId}}${applicationId}_interface\")\n" +
                    "public class Service {\n" +
                    "    // POJO Context\n" +
                    "    @Resource\n" +
                    "    private Context jbiCtx;\n" +
                    "    \n" +
                    "    public Service() {\n" +
                    "    }\n" +
                    "\n" +
                    "    @Operation (outMessageTypeQN=\"{http://fuji.dev.java.net/application/${applicationId}}anyMsg\")\n" +
                    "    public String receive(String input) throws ErrorMessage {\n" +
                    "        // Enter your code goes here.\n" +
                    "        return input;\n" +
                    "    }\n" +
                    "}\n",
                    type: Ws.PropertiesSheet.CODE,
                    code_type: "java",

                    auto_adjust_height: true,
                    maximizable: true
                }
                ]
            }
            ,
            {
                display_name: "Display",
                description: "Display settings.",
                properties: [
                {
                    name: "ui_properties_display_name",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Display Name",
                    description: "Display Name description",
                    value: ""
                },
                {
                    name: "ui_properties_description",
                    type: Ws.PropertiesSheet.TEXT,

                    display_name: "Description",
                    description: "Description description",
                    value: ""
                }
                ]
            }
            ,
            {
                display_name: "Advanced",
                description: "Advanced settings.",
                properties: [
                {
                    name: "raw_warning",
                    type: Ws.PropertiesSheet.LABEL,

                    display_name: "The below properties allow editing the raw " +
                    "configuration files for this service. Please do " +
                    "this if and only if you really know what you're " +
                    "doing.",
                    description: ""
                }
                ,
                {
                    name: "raw_warning_2",
                    type: Ws.PropertiesSheet.LABEL,

                    display_name: "Note that once you click any of the Edit " +
                    "buttons, the other properties that you might " +
                    "have changed will be automatically saved.",
                    description: ""
                }
                ,
                {
                    name: "raw_edit_message_xsd",
                    type: Ws.PropertiesSheet.BUTTON,

                    display_name: "Edit message.xsd",
                    description: "The message.xsd file is used to define the data types of the message going in and out of a service.",
                    action: function(psheet) {
                        psheet.save();
                    }
                }
                ,
                {
                    name: "raw_edit_interface_wsdl",
                    type: Ws.PropertiesSheet.BUTTON,

                    display_name: "Edit interface.wsdl",
                    description: "The interface.wsdl file is used to define the service interface.",
                    action: function(psheet) {
                        psheet.save();
                    }
                }
                ,
                {
                    name: "raw_edit_binding_wsdl",
                    type: Ws.PropertiesSheet.BUTTON,

                    display_name: "Edit binding.wsdl",
                    description: "The binding.wsdl file is used to define the actual service-type specific binding of the service interface.",
                    action: function(psheet) {
                        psheet.save();
                    }
                }
                ]
            }
            ,
            {
                display_name: "Tracing",
                description: "Tracing settings.",
                properties: [
                {
                    name: "messages",
                    type: Ws.PropertiesSheet.TABLE,

                    display_name: "Messages",
                    description: "Messages used to test the message flow.",
                    value: {
                        body_caption: [
                        {
                            value: "&nbsp;Input"
                        },
                        {
                            value: "&nbsp;Output"
                        }
                        ],

                        body_value: [
                        [
                        {
                            value: "<img src=\"img/ui/tester/empty_16.png\"/> <i>No input messages.</i>",
                            functions_change: false
                        },
                        {
                            value: "<img src=\"img/ui/tester/empty_16.png\"/> <i>No output messages.</i>",
                            functions_change: false
                        }
                        ]
                        ]
                    }
                }
                ]
            }
            ]
        };

        // Correct the description height. It would be the second property in the second
        // section.
        container.sections[1].properties[1].initial_height = 500;

        return container;
    },

    pc_set_properties: function(properties) {
        var html = "";

        for (var i in properties) {
            html += "" + i + " => " + properties[i] + "<br/>";
        }

        $("logger").innerHTML = html;
    }
};
