/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.FloatingWindow = Class.create({
    header_classname: "-wsfw-header",
    contents_classname: "-wsfw-contents",
    footer_classname: "-wsfw-footer",

    animation_enabled: true,
    animation_duration: 0.5,
    
    resize_handle_stroke: "rgba(255, 255, 255, 0.9)",
    resize_handle_stroke_width: 1,
    
    resize_handle_size: 10,
    resize_handle_padding: 3,
    
    resize_callbacks: null,

    element: null,

    header: null,
    contents: null,
    footer: null,

    resize_handle: null,

    __current_height : 0,
    __current_min_height : 0,
    __current_width : 0,
    __current_min_width : 0,
    __current_position_top : 0,
    __current_position_left : 0,

    /**
     * Constructor, this method initializes the window, based on the supplied DOM 
     * element, options specific to the floating window itself and the options targeting
     * the embedded Ws.RoundedRectangle.
     */
    initialize: function(element, options, rr_options) {
        var my_window = this;
        this.resize_callbacks = [];

        if (options) {
            Object.extend(this, options);

            if (options.resize_callback) {
                this.resize_callbacks.push(options.resize_callback);
                delete this.resize_callback;
            }
        }

        if (!rr_options) {
            rr_options = {};
        }
        
        Element.extend(element);
        this.element = element;
        this.element.ws_fwindow = this;

        // Search for window header, contents and footer, then store links to the --------
        // window object in them (and vice versa).
        if (!this.header) {
            this.header = this.element.select("." + this.header_classname).reduce();
        }

        if (!this.contents) {
            this.contents = this.element.select("." + this.contents_classname).reduce();
        }

        if (!this.footer) {
            this.footer = this.element.select("." + this.footer_classname).reduce();
        }

        // Check that the window element has no more than three child elements: header,
        // contents and footer. Otherwise we'd fail at laying stuff out properly.
        var window_children = this.element.childElements();
        var window_children_length = 3;
        if (window_children.indexOf(this.contents) == -1) {
            throw "Contents element is not a direct child of the window element.";
        }
        if (this.header) {
            if (window_children.indexOf(this.header) == -1) {
                throw "Header element is not a direct child of the window element.";
            }
        } else {
            window_children_length--;
        }
        if (this.footer) {
            if (window_children.indexOf(this.footer) == -1) {
                throw "Footer element is not a direct child of the window element.";
            }
        } else {
            window_children_length--;
        }
        if (window_children_length != window_children.length) {
            throw "Some extra unexpected elements are direct children of the window " +
                    "element. This is not allowed.";
        }

        if (this.header) {
            this.header.__wsfw = this;

            if (!this.without_buttons) {
                var height;

                var img_close = new Element("img");
                img_close.setAttribute("src", Ws.FloatingWindow.WINDOW_CLOSE);
                img_close.addClassName("windowed-button-close");
                this.element.insertBefore(img_close, this.header);
                Element.extend(img_close);
                Event.observe(img_close, "click", function () {
                    my_window.element.hide();
                });

                var img_min = new Element("img");
                img_min.setAttribute("src", Ws.FloatingWindow.WINDOW_MIN);
                img_min.addClassName("windowed-button-min");
                this.element.insertBefore(img_min, this.header);
                Event.observe(img_min, "click", function () {
                    if (my_window.__is_min) {
                        img_min.setAttribute("src", Ws.FloatingWindow.WINDOW_MIN);
                        my_window.__is_min = false;

                        my_window.__normolize();
                    } else {
                        img_min.setAttribute("src", Ws.FloatingWindow.WINDOW_NORMAL);
                        my_window.__is_min = true;
                        
                        my_window.__minimize();
                    }
                });

                var img_max = new Element("img");
                img_max.setAttribute("src", Ws.FloatingWindow.WINDOW_MAX);
                img_max.addClassName("windowed-button-max");
                this.element.appendChild(img_max);
                Event.observe(img_max, "click", function () {
                    if (my_window.__is_max) {
                        img_max.setAttribute("src", Ws.FloatingWindow.WINDOW_MAX);
                        my_window.__is_max = false;

                        my_window.__normolize();
                    } else {
                        img_max.setAttribute("src", Ws.FloatingWindow.WINDOW_NORMAL);
                        my_window.__is_max = true;
                        
                        my_window.__maximize();
                    }
                });
            }
        }

        if (this.footer) {
            this.footer.__wsfw = this;
        }

        this.contents.__wsfw = this;

        // Record the old values for soem of the styles in case we will need to restore
        // the original situation.
        this.__old_element_position = this.element.style.position;
        this.__old_element_width = this.element.style.width;
        this.__old_element_height = this.element.style.height;
        this.__old_element_zIndex = this.element.style.zIndex;
        this.__old_element_left = this.element.style.left;
        this.__old_element_top = this.element.style.top;

        if (this.header) {
            this.__old_header_position = this.header.style.position;
            this.__old_header_width = this.header.style.width;
            this.__old_header_height = this.header.style.height;
            this.__old_header_top = this.header.style.top;
            this.__old_header_left = this.header.style.left;
            this.__old_header_zIndex = this.header.style.zIndex;
            this.__old_header_minWidth = this.header.style.minWidth;
        }

        if (this.footer) {
            this.__old_footer_position = this.footer.style.position;
            this.__old_footer_width = this.footer.style.width;
            this.__old_footer_height = this.footer.style.height;
            this.__old_footer_bottom = this.footer.style.bottom;
            this.__old_footer_left = this.footer.style.left;
            this.__old_footer_zIndex = this.footer.style.zIndex;
            this.__old_footer_minWidth = this.footer.style.minWidth;
        }

        this.__old_contents_position = this.contents.style.position;
        this.__old_contents_width = this.contents.style.width;
        this.__old_contents_height = this.contents.style.height;
        this.__old_contents_top = this.contents.style.top;
        this.__old_contents_left = this.contents.style.left;
        this.__old_contents_zIndex = this.contents.style.zIndex;
        this.__old_contents_minWidth = this.contents.style.minWidth;
        this.__old_contents_minHeight = this.contents.style.minHeight;

        // Create settings for the rounded rectangle -------------------------------------
        Ws.Utils.apply_defaults(
            rr_options,
            Ws.FloatingWindow.get_default_rr_options());

        // Correct the styles of the window element --------------------------------------
        var window_width = parseInt(this.element.getStyle("width"));

        if (isNaN(window_width)) {
            window_width = Ws.FloatingWindow.DEFAULT_WINDOW_WIDTH;
        }

        var window_height = parseInt(this.element.getStyle("height"));
        
        if (isNaN(window_height)) {
            window_height = Ws.FloatingWindow.DEFAULT_WINDOW_HEIGHT;
        }

        this.element.setStyle({
            position: "fixed",
            width: window_width + "px",
            height: window_height + "px",
            zIndex: Ws.FloatingWindow.last_z_index
        });

        // Find out the minimal values for the window elements' dimensions ---------------
        var min_window_width = parseInt(this.element.getStyle("min-width"));

        if (isNaN(min_window_width) || min_window_width == 0) {
            min_window_width = Ws.FloatingWindow.DEFAULT_MIN_WINDOW_WIDTH;
        }
        
        var min_window_height = parseInt(this.element.getStyle("min-height"));

        if (isNaN(min_window_height) || min_window_height == 0) {
            min_window_height = Ws.FloatingWindow.DEFAULT_MIN_WINDOW_HEIGHT;
        }
        
        var min_header_height = rr_options.radius;

        // Correct the styles of header, footer and contents -----------------------------
        var header_bmp = {};
        var header_height = 0;
        if (this.header) {
            header_bmp = Ws.Utils.get_bmp(this.header);
            min_header_height = rr_options.radius - header_bmp.get_bp_vertical();
            header_height = Element.getHeight(this.header) + header_bmp.get_margin_vertical();

            if (header_height < min_header_height) {
                header_height = min_header_height;

                this.header.setStyle({
                    height: (min_header_height - header_bmp.get_bmp_vertical()) + "px"
                });
            }

            this.header.setStyle({
                position: "absolute",
                top: "0px",
                left: "0px",
                width: (window_width - header_bmp.get_bmp_horizontal()) + "px",
                zIndex: Ws.FloatingWindow.last_z_index
            });
        }

        var footer_bmp = {};
        var footer_height = 0;
        if (this.footer) {
            footer_bmp = Ws.Utils.get_bmp(this.footer);
            footer_height = Element.getHeight(this.footer) + footer_bmp.get_margin_vertical();

            this.footer.setStyle({
                position: "absolute",
                bottom: "0px",
                left: "0px",
                width: (window_width - footer_bmp.get_bmp_horizontal()) + "px",
                zIndex: Ws.FloatingWindow.last_z_index
            });
        }

        var contents_bmp = Ws.Utils.get_bmp(this.contents);

        var contents_width = window_width - contents_bmp.get_bmp_horizontal();
        var contents_height = window_height - header_height -
        footer_height - contents_bmp.get_bmp_vertical();

        this.contents.setStyle({
            position: "absolute",
            top: header_height + "px",
            left: "0px",
            width: contents_width + "px",
            height: contents_height + "px",
            zIndex: Ws.FloatingWindow.last_z_index
        });

        if (min_window_width) {
            if (this.header) {
                this.header.setStyle({
                    minWidth: (min_window_width - header_bmp.get_bmp_horizontal()) + "px"
                });
            }

            if (this.footer) {
                this.footer.setStyle({
                    minWidth: (min_window_width - footer_bmp.get_bmp_horizontal()) + "px"
                });
            }

            var min_contents_width = min_window_width - contents_bmp.get_bmp_horizontal();

            this.contents.setStyle({
                minWidth: min_contents_width + "px"
            });
        }
        
        if (min_window_height) {
            var applied_min_height = min_window_height - header_height - footer_height - contents_bmp.get_bmp_vertical();

            if (applied_min_height < 0) {
                applied_min_height = 0;
            }

            this.contents.setStyle({
                minHeight: applied_min_height + "px"
            });
        }
        
        // Create the rounded rectangle --------------------------------------------------
        new Ws.RoundedRectangle(this.element, rr_options);
        
        // Initialize drag and resize event handler for header and footer, register 
        // these elements in Ws.DragManager ----------------------------------------------
        if (this.header) {
            this.header.ds_accept_drag = function(x, y) {
                var header_offset = Element.cumulativeOffset(this);
                var header_s_offset = Element.cumulativeScrollOffset(this);

                var window_offset =
                Element.cumulativeOffset(this.__wsfw.element);
                var window_s_offset =
                Element.cumulativeScrollOffset(this.__wsfw.element);

                var header_offset_x = header_offset.left - header_s_offset.left + window_s_offset.left;
                var header_offset_y = header_offset.top - header_s_offset.top + window_s_offset.top;


                if ((x > header_offset_x) &&
                    (x < header_offset_x + Element.getWidth(this)) &&
                    (y > header_offset_y) &&
                    (y < header_offset_y + Element.getHeight(this))) {


                    this.__drag_start_x = x - window_offset.left;
                    this.__drag_start_y = y - window_offset.top;

                    return true;
                }

                return false;
            }

            this.header.ds_drag_started = function() {
            // Does nothing, a placeholder
            }

            this.header.ds_dragged_to = function(x, y) {
                this.__wsfw.element.setStyle({
                    left: (x - this.__drag_start_x) + "px",
                    top: (y - this.__drag_start_y) + "px"
                });
            }

            this.header.ds_drag_finished = function(x, y) {
            // Does nothing, a placeholder
            }

            Ws.DragManager.add_drag_source(this.header);
        }
        
        // Add the resize handle element -------------------------------------------------
        var resize_handle_canvas = Ws.Utils.create_canvas();
        Ws.Utils.draw_resize_handle(
            resize_handle_canvas,
            this.resize_handle_size,
            this.resize_handle_stroke,
            this.resize_handle_stroke_width);

        this.resize_handle = new Element("div");
        this.resize_handle.__wsfw = this;
        this.resize_handle.addClassName("window-resizer");
        this.resize_handle.appendChild(resize_handle_canvas);
        this.resize_handle.setStyle({
            position: "absolute",
            right: this.resize_handle_padding + "px",
            bottom: this.resize_handle_padding + "px",
            zIndex: this.element.ws_rrectangle.contents_z_index,
            height: this.resize_handle_size + "px",
            width: this.resize_handle_size + "px",
            lineHeight: "0px"
        });

        this.element.appendChild(this.resize_handle);

        // This will be needed in one of the resize_handle ds_* methods. If we do not do
        // this, resize will fail on MSIE.
        Element.extend(document.body);

        this.resize_handle.ds_accept_drag = function(x, y) {
            var offset = Element.cumulativeOffset(this);
            var s_offset = Element.cumulativeScrollOffset(this);
            var w_offset = Element.cumulativeScrollOffset(this.__wsfw.element);

            var offset_x = x - (offset.left - s_offset.left + w_offset.left) ;
            var offset_y = y - (offset.top - s_offset.top + w_offset.top);

            if ((offset_x > 0) && (offset_x < Element.getWidth(this)) &&
                (offset_y > 0) && (offset_y < Element.getHeight(this))) {
                return true;
            }

            return false;
        }

        this.resize_handle.ds_drag_started = function(x, y) {
            this.__wsfw.element.setStyle({
                cursor:  "se-resize"
            });
            this.__body_content = document.body.select("#body_content").reduce();

            if (this.__body_content) {
                this.__body_content.setStyle({
                    cursor:  "se-resize"
                });
            }
     
            var window_bmp = Ws.Utils.get_bmp(this.__wsfw.element);
            var contents_bmp = Ws.Utils.get_bmp(this.__wsfw.contents);

            if (this.__wsfw.footer) {
                this.__initial_footer_width = Element.getWidth(this.__wsfw.footer) -
                Ws.Utils.get_bmp(this.__wsfw.footer).get_bp_horizontal();
                this.__initial_header_width = Element.getWidth(this.__wsfw.header) -
                Ws.Utils.get_bmp(this.__wsfw.header).get_bp_horizontal();
            }

            this.__initial_window_width = Element.getWidth(this.__wsfw.element) -
            window_bmp.get_bp_horizontal();
            this.__initial_window_height = Element.getHeight(this.__wsfw.element) -
            window_bmp.get_bp_vertical();

            this.__initial_contents_width = Element.getWidth(this.__wsfw.contents) -
            contents_bmp.get_bp_horizontal();
            this.__initial_contents_height = Element.getHeight(this.__wsfw.contents) -
            contents_bmp.get_bp_vertical();

            this.__initial_dx = this.__wsfw.element.offsetLeft +
            this.__initial_window_width - x;
            this.__initial_dy = this.__wsfw.element.offsetTop +
            this.__initial_window_height - y;
        }

        this.resize_handle.ds_dragged_to = function(x, y) {
            var offset_width = Element.getWidth(this.__wsfw.element);
            var offset_height = Element.getHeight(this.__wsfw.element);

            this.__wsfw.element.setStyle({
                width: (x - this.__wsfw.element.offsetLeft + this.__initial_dx) + "px",
                height:(y - this.__wsfw.element.offsetTop + this.__initial_dy) + "px"
            });

            var window_bmp = Ws.Utils.get_bmp(this.__wsfw.element);
            var new_offset_width = Element.getWidth(this.__wsfw.element);
            var new_offset_height = Element.getHeight(this.__wsfw.element);

            if ((new_offset_width - offset_width) != 0) {
                var d_width = new_offset_width - this.__initial_window_width -
                window_bmp.get_bp_horizontal();

                this.__wsfw.contents.setStyle({
                    width: (this.__initial_contents_width + d_width) + "px"
                });

                if (this.__wsfw.header) {
                    this.__wsfw.header.setStyle({
                        width: (this.__initial_header_width + d_width) + "px"
                    });
                }

                if (this.__wsfw.footer) {
                    this.__wsfw.footer.setStyle({
                        width: (this.__initial_footer_width + d_width) + "px"
                    });
                }
            }

            if ((new_offset_height - offset_height) != 0) {
                var d_height = new_offset_height - this.__initial_window_height
                - window_bmp.get_bp_vertical();

                this.__wsfw.contents.setStyle({
                    height: (this.__initial_contents_height + d_height) + "px"
                });
            }

            this.__wsfw.__update();

         }

        this.resize_handle.ds_drag_finished = function(x, y) {
            this.__wsfw.element.setStyle({
                cursor:  "default"
            });
            if (this.__body_content) {
                this.__body_content.setStyle({
                    cursor:  "default"
                });
            }

        }

        Ws.DragManager.add_drag_source(this.resize_handle);

        // Finally, position the window on screen.
        if (options && options.isCenter) {
            var x = isNaN(options.centerX) ? 0 : options.centerX - window_width / 2 ;
            var y = isNaN(options.centerY) ? 0 : options.centerY - window_height / 2;

            this.element.setStyle({
                position: "fixed",
                left: x + "px",
                top: y + "px",
                width: window_width + "px",
                height: window_height + "px",
                zIndex: Ws.FloatingWindow.last_z_index
            });
        }
    },

    /**
     * This method resize the height window to fit its contents. Both enlarging and
     * reducing the size are supported. The optional 'options' parameter supports the
     * following fields:
     *   - 'max_height' -- the maximum height the contents element can be, if the
     *     actual required size if bigger, it has no effect, the element's size will
     *     be set to 'max_height'.
     *   - 'min_height' -- the opposite.
     *
     * Note: this method makes use of {@link Ws.Utils.get_children_height()}, which
     * does not render correct results in all cases. Consult the documentation for that
     * method for more information.
     *
     * Note: the 'max_height' and 'min_height' options may conflict with the 'max-height'
     * and 'min-height' CSS values set on either the window element itself, or on the
     * contents element. This method does not take into account such situations and CSS
     * takes precendence.
     */
    resize_height_to_fit: function(options) {
        if (!options) options = {};

        var header_height = 0;
        if (this.header) {
            header_height = Element.getHeight(this.header) + 
            Ws.Utils.get_bmp(this.header).get_margin_vertical();
        }

        var footer_height = 0;
        if (this.footer) {
            footer_height = Element.getHeight(this.footer) +
            Ws.Utils.get_bmp(this.footer).get_margin_vertical();
        }
        
        var margin = Ws.Utils.get_bmp(this.contents).get_margin_vertical();
        var height = Ws.Utils.get_children_height(this.contents);
        if (options.max_height && (options.max_height < height)) {
            height = options.max_height;
        }
        if (options.min_height && (options.min_height > height)) {
            height = options.min_height;
        }

        this.contents.setStyle({
            height: height + "px"
        });

        this.element.setStyle({
            height: (header_height + footer_height + height + margin) + "px"
        });

        // Then we need to remeasure and resize again. In case of dissapearing
        // scrollbars, this is required to reestimate the height required by the
        // contents.
        height = Ws.Utils.get_children_height(this.contents);
        if (options.max_height && (options.max_height < height)) {
            height = options.max_height;
        }
        if (options.min_height && (options.min_height > height)) {
            height = options.min_height;
        }

        this.contents.setStyle({
            height: height + "px"
        });

        this.element.setStyle({
            height: (header_height + footer_height + height + margin) + "px"
        });

        this.__update();
    },

    /**
     * This method is expected to be called whenever the sizes of the header and the 
     * footer are changed externally. It resizes the contents element so that it occupies
     * all available real estate according to the new size of header and footer.
     */
    adjust_contents_height: function() {
        var header_height = 0;
        if (this.header) {
            var header_bmp = Ws.Utils.get_bmp(this.header);
            header_height =
            Element.getHeight(this.header) + header_bmp.get_margin_vertical();
        }

        var footer_height = 0;
        if (this.footer) {
            var footer_bmp = Ws.Utils.get_bmp(this.footer);
            footer_height =
            Element.getHeight(this.footer) + footer_bmp.get_margin_vertical();
        }

        var window_height = parseInt(this.element.getStyle("height"));
        var contents_bmp = Ws.Utils.get_bmp(this.contents);

        var contents_height = window_height - header_height -
        footer_height - contents_bmp.get_bmp_vertical();

        this.contents.setStyle({
            height: contents_height + "px"
        });
    },

    close: function(immediately) {
        if (this.animation_enabled && !immediately) {
            this.element.fade({ 
                duration: this.animation_duration
            });
        } else {
            this.element.hide();
        }
    },

    show: function(immediately) {
        if (this.animation_enabled && !immediately) {
            this.element.appear({ 
                duration: this.animation_duration
            });
        } else {
            this.element.show();
        }
    },

    remove: function() {
        // Restore the old styles.
        this.element.style.position = this.__old_element_position;
        this.element.style.width = this.__old_element_width;
        this.element.style.height = this.__old_element_height;
        this.element.style.zIndex = this.__old_element_zIndex;
        this.element.style.left = this.__old_element_left;
        this.element.style.top = this.__old_element_top;

        if (this.header) {
            this.header.style.position = this.__old_header_position;
            this.header.style.width = this.__old_header_width;
            this.header.style.height = this.__old_header_height;
            this.header.style.top = this.__old_header_top;
            this.header.style.left = this.__old_header_left;
            this.header.style.zIndex = this.__old_header_zIndex;
            this.header.style.minWidth = this.__old_header_minWidth;
        }

        if (this.footer) {
            this.footer.style.position = this.__old_footer_position;
            this.footer.style.width = this.__old_footer_width;
            this.footer.style.height = this.__old_footer_height;
            this.footer.style.bottom = this.__old_footer_bottom;
            this.footer.style.left = this.__old_footer_left;
            this.footer.style.zIndex = this.__old_footer_zIndex;
            this.footer.style.minWidth = this.__old_footer_minWidth;
        }

        this.contents.style.position = this.__old_contents_position;
        this.contents.style.width = this.__old_contents_width;
        this.contents.style.height = this.__old_contents_height;
        this.contents.style.top = this.__old_contents_top;
        this.contents.style.left = this.__old_contents_left;
        this.contents.style.zIndex = this.__old_contents_zIndex;
        this.contents.style.minWidth = this.__old_contents_minWidth;
        this.contents.style.minHeight = this.__old_contents_minHeight;

        // Remove the resize handle and the rounded rectangle.
        this.element.ws_rrectangle.detach();
        this.element.removeChild(this.resize_handle);

        // Finally remove the link to us.
        this.element.ws_fwindow = null;
    },

    /**
     * This method updates the layout of the window components. Currently it only
     * relayouts the embedded Ws.RoundedRectangle.
     */
    __update: function() {
        this.element.ws_rrectangle.relayout();

        for (var i = 0; i < this.resize_callbacks.length; i++) {
            this.resize_callbacks[i]();
        }
    },

    __minimize: function() {
        if (!this.__is_max) {
            this.__current_height = parseInt(this.contents.getStyle("height"));
            this.__min_height = parseInt(this.contents.getStyle("min-height"));
            this.__current_width = parseInt(this.contents.getStyle("width"));
            this.__min_width = parseInt(this.contents.getStyle("min-width"));
            this.__current_position_top = this.element.offsetTop;
            this.__current_position_left = this.element.offsetLeft;
        } else {
            var img_max = this.element.select("img.windowed-button-max").reduce();
            img_max.setAttribute("src", Ws.FloatingWindow.WINDOW_MAX);
            this.__is_max = false;
        }

        var header_bmp = Ws.Utils.get_bmp(this.header);
        var height = Element.getHeight(this.header) +
        header_bmp.get_margin_vertical();
        var width = Element.getWidth(this.element) -
        Ws.Utils.get_bmp(this.element).get_bp_horizontal();
        var new_left = width - this.__min_width + this.element.offsetLeft;

        this.element.setStyle({
            height    : height + "px",
            minHeight : height + "px",
            width     : this.__min_width + "px",
            minWidth  : this.__min_width + "px",
            left      : new_left + "px"
        })

        var header_bmp_hor = header_bmp.get_bmp_horizontal();
        this.header.setStyle({
            width : (this.__min_width - header_bmp_hor) + "px",
            minWidth : (this.__min_width - header_bmp_hor) + "px"
        })

        this.contents.setStyle({
            height: "0px",
            minHeight: "0px",
            display: "none"
        })
        this.footer.hide();
        this.resize_handle.hide();
        this.__update();
    },

    __maximize: function() {
        var dimensions = Ws.Utils.get_window_dimensions();
        if (!this.__is_min) {
            this.__current_height = parseInt(this.contents.getStyle("height"));
            this.__min_height = parseInt(this.contents.getStyle("min-height"));
            this.__current_width = parseInt(this.contents.getStyle("width"));
            this.__min_width = parseInt(this.contents.getStyle("min-width"));
            this.__current_position_top = this.element.offsetTop;
            this.__current_position_left = this.element.offsetLeft;
        } else {
            var img_min = this.element.select("img.windowed-button-min").reduce();
            img_min.setAttribute("src", Ws.FloatingWindow.WINDOW_MIN);
            this.__is_min = false;
        }
        var footer_bmp = Ws.Utils.get_bmp(this.footer);
        var header_bmp = Ws.Utils.get_bmp(this.header);

        var height = Element.getHeight(this.header) + header_bmp.get_margin_vertical() +
        Element.getHeight(this.footer) + footer_bmp.get_margin_vertical() +
        Ws.Utils.get_bmp(this.element).get_bp_vertical();

        this.element.setStyle({
            top: dimensions.height * 0.05 + "px",
            left : dimensions.width * 0.05 + "px",
            height: dimensions.height * 0.9 + "px",
            width: dimensions.width * 0.9 + "px"
        });
        var d_w = footer_bmp.get_bmp_horizontal();
        this.footer.setStyle({
            width: (dimensions.width * 0.9 - d_w) + "px"
        });

        d_w = header_bmp.get_bmp_horizontal();
        this.header.setStyle({
            width: (dimensions.width * 0.9 - d_w) + "px"
        });

        d_w = Ws.Utils.get_bmp(this.contents).get_bmp_horizontal() ;
        this.contents.setStyle({
            height: (dimensions.height * 0.9 - height) + "px",
            width: (dimensions.width * 0.9 - d_w) + "px",
            display: "block"
        });
        this.footer.show();
        this.resize_handle.show();
        this.__update();
    },

    __normolize: function() {
        this.footer.show();
        this.resize_handle.show();

        var content_bmp = Ws.Utils.get_bmp(this.contents);
        var height = Element.getHeight(this.header) +
        Ws.Utils.get_bmp(this.header).get_margin_vertical() +
        Element.getHeight(this.footer) +
        Ws.Utils.get_bmp(this.footer).get_margin_vertical() + content_bmp.get_bmp_vertical();
        var width_ = content_bmp.get_bmp_horizontal();

        this.element.setStyle({
            top      : this.__current_position_top + "px",
            left     : this.__current_position_left + "px",
            height   : (this.__current_height + height) + "px",
            minHeight: (this.__min_height + height) + "px",
            width    : (this.__current_width + width_) + "px"
        })

        this.footer.setStyle({
            width : this.__current_width + "px"
        });

        this.header.setStyle({
            width : this.__current_width + "px"
        });
        this.contents.setStyle({
            height: this.__current_height + "px",
            minHeight: this.__min_height + "px",
            width : this.__current_width + "px",
            display: "block"
        })

        this.__update();
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
    // Does nothing, a placeholder.
    }
});

//////////////////////////////////////////////////////////////////////////////////////////
// Static
Ws.FloatingWindow.last_z_index = 500000; // five hundred thousands

Ws.FloatingWindow.get_default_rr_options = function() {
    Ws.FloatingWindow.last_z_index += 100;

    var rr_options = {};

    for (var i in Ws.FloatingWindow.DEFAULT_RR_OPTIONS) {
        rr_options[i] = Ws.FloatingWindow.DEFAULT_RR_OPTIONS[i];
    }

    Ws.Utils.apply_defaults(rr_options, {
        background_z_index: Ws.FloatingWindow.last_z_index - 10,
        contents_z_index: Ws.FloatingWindow.last_z_index
    });

    return rr_options;
};

//////////////////////////////////////////////////////////////////////////////////////////
// Constants
Ws.FloatingWindow.DEFAULT_WINDOW_WIDTH = 200;
Ws.FloatingWindow.DEFAULT_WINDOW_HEIGHT = 300;

Ws.FloatingWindow.DEFAULT_MIN_WINDOW_WIDTH = 60;
Ws.FloatingWindow.DEFAULT_MIN_WINDOW_HEIGHT = 12;

Ws.FloatingWindow.WINDOW_CLOSE = "./img/ui/window/window_close.png";
Ws.FloatingWindow.WINDOW_NORMAL = "./img/ui/window/window_normal.png";
Ws.FloatingWindow.WINDOW_MAX = "./img/ui/window/window_max.png";
Ws.FloatingWindow.WINDOW_MIN = "./img/ui/window/window_min.png";

Ws.FloatingWindow.DEFAULT_RR_OPTIONS = function() {
    var gradient = Ws.Utils.create_linear_gradient(0, 0, 0, 8);

    gradient.addColorStop(0, "rgba(70, 70, 70, 0.8)");
    gradient.addColorStop(1, "rgba(50, 50, 50, 0.8)");
    gradient.addColorStop(1, "rgba(0, 0, 0, 0.8)");

    return {
        radius: 8,

        tl_fill: gradient,
        t_fill: gradient,
        tr_fill: gradient
    };
}();
