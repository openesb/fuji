/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var palette_updater_interval = 20000;

var palette_delayed_load_components = 0;
var palette_delayed_load_templates = 0;
var palette_delayed_load_eip = 0;

var id_palette = "palette";
//var id_components_section = "palette-section-components";
var id_templates_section = "palette-section-templates";
var id_eip_section = "palette-section-eip";

var palette;

//var components_section;
var templates_section;
var eip_section;

//var COMPONENTS_SECTION = "components";
var TEMPLATES_SECTION = "templates";
var EIP_SECTION = "eip";

Event.observe(window, "load", function() {
    new Ws.Palette($(id_palette), {
        enable_search: true,
        maximized_index: 1
    });

    palette = $(id_palette).ws_palette;

//    palette.add_section({
//        id: id_components_section,
//        display_name: "Components",
//        description: "Components and message flow snippets that " +
//                "already exist in the system and are ready to be reused.",
//        empty_text: "We're sorry, but currently there are no components " +
//                "registered in the system. Why don't you go an create a new one?",
//        no_match_text: "Unfortunately, no components match the filter criteria.",
//        proportion: 0.3,
//        items: []
//    });

    palette.add_section({
        id: id_templates_section,
        display_name: "Templates",
        description: "Templates that you can use to create new " +
                "components in your message flows.",
        empty_text: "Unfortunately there are no templates available. Why not just " +
                "reuse the existing components?",
        no_match_text: "No templates match, please try a different search.",
        proportion: 0.4,
        items: []
    });

    palette.add_section({
        id: id_eip_section,
        display_name: "EIP",
        description: "Flow control constructs inspired by Enterprise Integration " +
                "Patterns",
        empty_text: "No EIP controls are available. You'll have to use your custom " +
                "service only.",
        no_match_text: "No patterns match the filter criteria.",
        proportion: 0.3,
        items: []
    });

//    components_section = palette.get_section(id_components_section);
    templates_section = palette.get_section(id_templates_section);
    eip_section = palette.get_section(id_eip_section);

    update_palette();

//    palette_updater_interval = setInterval(function() {
//        update_palette();
//    }, palette_updater_interval);
});

function update_palette() {
//    if (components_section.update_marker_element.ws_progress_bar) {
//        components_section.update_marker_element.ws_progress_bar.create();
//    } else {
//        new Ws.ProgressBar(components_section.update_marker_element, {
//            fill: "rgb(255, 255, 255)"
//        });
//    }
    
    if (templates_section.update_marker_element.ws_progress_bar) {
        templates_section.update_marker_element.ws_progress_bar.create();
    } else {
        new Ws.ProgressBar(templates_section.update_marker_element, {
            fill: "rgb(255, 255, 255)"
        });
    }

    if (eip_section.update_marker_element.ws_progress_bar) {
        eip_section.update_marker_element.ws_progress_bar.create();
    } else {
        new Ws.ProgressBar(eip_section.update_marker_element, {
            fill: "rgb(255, 255, 255)"
        });
    }
    
    new Ajax.Request("callbacks/get-palette-items", {
        method: "get",
        
        onSuccess: function(transport) {
            var result = transport.responseJSON;
            
            result.updated.compact().each(function(item) {
                // Build the list of objects to load
                var to_load = [];
                
                to_load.push({
                    url: item.item_data.node_type,
                    load_manager: ComponentTypesLoadManager,
                    critical: true
                });
                
                if (item.icon) {
                    to_load.push({
                        url: item.icon,
                        load_manager: Ws.ImagesLoadManager,
                        critical: false
                    });
                }
                
                if (item.drag_shadow) {
                    to_load.push({
                        url: item.drag_shadow,
                        load_manager: Ws.ImagesLoadManager,
                        critical: false
                    });
                }
                
                if (item.item_data.node_data.ui_properties.icon) {
                    to_load.push({
                        url: item.item_data.node_data.ui_properties.icon,
                        load_manager: Ws.ImagesLoadManager,
                        critical: false
                    });
                }
                
                // Load the required objects and update the palette item upon loading
                _increment_delayed_load(item.section);
                
                var loader = new Ws.MultiLoadManager(to_load, function() {
                    update_palette_item(item);
                    
                    _decrement_delayed_load(item.section);
                }, function() {
                    Ws.Growl.notify_error(
                            "Cannot load '" + item.ui_properties.display_name +
                            "' because its type codenamed '" + item.type + 
                            "' failed to load.", -1, true);
                            
                    _decrement_delayed_load(item.section);
                });
                
                loader.load();
            });
            
            if (result.removed.length > 1) {
                result.removed.compact().each(function(item) {
                    remove_palette_item(item);
                });
            }
            
            _update_markers();
        },
        
        onFailure: function(transport) {
            Ws.Growl.notify_warning(
                    "Failed to update the list of components.", 5000, true);
            
            _update_markers();
        }
    });
}

function update_palette_item(item) {
    var type = ComponentTypesLoadManager.get_type(item.item_data.node_type);
    
    // Choose which section we'll be updating --------------------------------------------
    var section;
    switch (item.section) {
//        case COMPONENTS_SECTION:
//            section = components_section;
//            break;
        case TEMPLATES_SECTION:
            section = templates_section;
            break;
        case EIP_SECTION:
            section = eip_section;
            break;
        default:
            return; // We do not know which section this should go to, and thus
                    // we do not update anything.
    }
    
    // Init the palette item's properties ------------------------------------------------
    Ws.Utils.apply_default(item, type, "display_name", true);
    Ws.Utils.apply_default(item, type, "description", true);
    if (!item.icon) {
        item.icon = type.icons[0];
    }
    Ws.Utils.apply_default(item, type, "drag_shadow", false);
    
    // Extend the item's properties ------------------------------------------------------
    Ws.Utils.apply_default(
            item.item_data.node_data.ui_properties, type, "display_name", true);
    Ws.Utils.apply_default(
            item.item_data.node_data.ui_properties, type, "description", true);
    
    if (!item.item_data.node_data.ui_properties.icon) {
        item.item_data.node_data.ui_properties.icon = type.icons[0];
    }
    item.item_data.node_data.ui_addenda = {};
    item.item_data.node_data.ui_addenda.icons = type.icons;
    
    if ((section == templates_section) || (section == eip_section)) {
        var original_hook = item.item_data.node_data.pre_add_hook;

        item.item_data.node_data.pre_add_hook = function() {
            var existing = this.graph.get_nodes_by_type(this.type);
            
            var counter = 0;
            var such_node_exists = false;
            
            do {
                counter++;
                
                such_node_exists = false;
                for (var i = 0; i < existing.length; i++) {
                    if (existing[i].properties.code_name == 
                            this.properties.code_name + "" + counter) {
                        such_node_exists = true;
                        break;
                    }
                }
            } while (such_node_exists);
            
            this.set_property(
                    "code_name", this.get_property("code_name") + counter, true);
            this.set_ui_property(
                    "display_name", this.get_ui_property("display_name") + counter, true);

            if (original_hook) {
                original_hook.apply(this);
            }
        }
    }

    var existing_item = palette.get_item(item.id, section);
    if (existing_item) {
        palette.remove_item(existing_item, section);
    }

    palette.add_item(item, section);
}

function remove_palette_item(item) {
    // TODO
}

// Private -------------------------------------------------------------------------------
function _increment_delayed_load(section) {
    switch (section) {
//        case COMPONENTS_SECTION:
//            palette_delayed_load_components++;
//            break;
        case TEMPLATES_SECTION:
            palette_delayed_load_templates++;
            break;
        case EIP_SECTION:
            palette_delayed_load_eip++;
            break;
        default:
            // Do nothing.
    }
}

function _decrement_delayed_load(section) {
    switch (section) {
//        case COMPONENTS_SECTION:
//            palette_delayed_load_components--;
//            break;
        case TEMPLATES_SECTION:
            palette_delayed_load_templates--;
            break;
        case EIP_SECTION:
            palette_delayed_load_eip--;
            break;
        default:
            // Do nothing.
    }
    
    _update_markers();
}

function _update_markers() {
//    if (palette_delayed_load_components == 0) {
//        components_section.update_marker_element.ws_progress_bar.remove();
//    }
    
    if (palette_delayed_load_templates == 0) {
        templates_section.update_marker_element.ws_progress_bar.remove();
    }

    if (palette_delayed_load_eip == 0) {
        eip_section.update_marker_element.ws_progress_bar.remove();
    }
}
