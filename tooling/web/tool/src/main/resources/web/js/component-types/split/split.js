/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var CONTROL_SPLIT = "split";

//-----------------------------------------------------------------------------------
var SplitControlNode = Class.create(EipNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    CONTROL_SPLIT, this, ui_options);
        }
    
        Ws.Utils.apply_defaults(this.properties, {
            expression_type: "xpath",
            use_external_config: false,
            expression: "",
            external_config_name: "split-config"
        });
    },
    
    pc_get_descriptors: function($super) {
        var container = $super();
        var node = this;

        container.properties.push(
            {
                name: "properties_expression_type",
                type: Ws.PropertiesSheet.ENUM,
                
                display_name: "Type",
                description: "Type of the expression used to split incoming messages.",
                value: this.properties.expression_type,
                values: [
                    {
                        display_name: "XPath",
                        description: "XPath expression. Works best when used with XML data.",
                        value: "xpath"
                    },
                    {
                        display_name: "Regular Expression",
                        description: "Regular Expression. Most useful for non-XML textual data.",
                        value: "regex"
                    }
                ]
            }
            ,
            {
                name: "properties_use_external_config",
                type: Ws.PropertiesSheet.BOOLEAN,

                display_name: "Use External",
                description: "Whether the configuration data for this EIP is inlined or loaded from an external file.",
                value: this.properties.use_external_config
            }
            ,
            {
                name: "properties_expression",
                type: Ws.PropertiesSheet.CODE,
                code_type : "xml",
                display_name: "Expression",
                description: "The expression used to split incoming messages.",
                value: this.properties.expression,

                auto_adjust_height: true,
                maximizable: true,

                depends: {
                    name: "properties_use_external_config",
                    value: false
                },

                dynamic: [
                    {
                        name: "properties_expression_type",
                        value: "xpath",

                        changes: {
                            inline_description: "Enter the XPath expression which will be used to locate individual messages. There is no need to quote or escape it."
                        }
                    },
                    {
                        name: "properties_expression_type",
                        value: "regex",

                        changes: {
                            inline_description: "Enter the regular expression which will be used to locate individual messages. There is no need to quote or escape it."
                        }
                    }
                ]
            }
            ,
            {
                name: "properties_external_config_name",
                type: Ws.PropertiesSheet.STRING,

                display_name: "Config name",
                description: "Name of the extenrnal configuration.",
                value: this.properties.external_config_name,

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config_warning",
                type: Ws.PropertiesSheet.LABEL,

                display_name: "Note: once you click the Edit Configuration button, changes you made to the other properties will be automatically saved.",
                description: "",

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
            ,
            {
                name: "edit_external_config",
                type: Ws.PropertiesSheet.BUTTON,

                display_name: "Edit Configuration",
                description: "Opens the external configuragion file in the editor.",
                action: function(psheet) {
                    psheet.save();
                    edit_external_artifact(node.id, "external-config", false);
                },

                depends: {
                    name: "properties_use_external_config",
                    value: true
                }
            }
        );
        
        return container;
    }
});

//-----------------------------------------------------------------------------------
var SplitControlNodeUi = Class.create(EipNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);

        this.shape_size = 44;
        this.width = this.shape_size + (this.margin * 2);
        this.height = this.shape_size + (this.margin * 2);

        this.toolbar_x = this.width - this.margin;
    },

    prepare_path: function() {
        //  /--B
        // A   |
        // |   |
        // D   |
        //  \--C
        var A_x = this.margin, A_y = (this.height / 2) - (this.shape_size / 3);
        var B_x = this.width - this.margin, B_y = this.margin;
        var C_x = this.width - this.margin, C_y = this.height - this.margin;
        var D_x = this.margin, D_y = (this.height / 2) + (this.shape_size / 3);

        this.context.beginPath();
        this.context.moveTo(A_x, A_y);
        this.context.lineTo(B_x, B_y);
        this.context.lineTo(C_x, C_y);
        this.context.lineTo(D_x, D_y);
        this.context.lineTo(A_x, A_y);
    },

    draw_node: function($super) {
        $super();

        this.context.lineWidth = 1;
        this.context.fillStyle = this.context.strokeStyle;

        var dot_radius = 2;

        var left_dot_x = this.margin + 10;
        var left_dot_y = this.height / 2;

        var arrow_start_x = this.margin + 16;
        var arrow_start_y = this.height / 2;
        var arrow_end_x = this.margin + 28;
        var arrow_end_y = this.height / 2;

        var arrow_head_length = 5;
        var arrow_head_span = 2;

        var right_dot_1_x = this.width - this.margin - 10;
        var right_dot_1_y = (this.height / 2) - 10;

        var right_dot_2_x = this.width - this.margin - 10;
        var right_dot_2_y = this.height / 2;

        var right_dot_3_x = this.width - this.margin - 10;
        var right_dot_3_y = (this.height / 2) + 10;

        this.context.beginPath();
        this.context.arc(left_dot_x, left_dot_y, dot_radius, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(arrow_start_x, arrow_start_y);
        this.context.lineTo(arrow_end_x, arrow_end_y);
        this.context.lineTo(arrow_end_x - arrow_head_length, arrow_start_y - arrow_head_span);
        this.context.lineTo(arrow_end_x - arrow_head_length, arrow_end_y + arrow_head_span);
        this.context.lineTo(arrow_end_x, arrow_end_y);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(right_dot_1_x, right_dot_1_y, dot_radius, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(right_dot_2_x, right_dot_2_y, dot_radius, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(right_dot_3_x, right_dot_3_y, dot_radius, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();
    },

    draw_text: function() {
        // Does nothing (no text on splitter)
    },

    draw_icon: function() {
        // Does nothing (no icon on splitter)
    },

    __get_toolbar_items: function($super) {
        var self = this;
        var items = [];

        if (!this.read_only) {
            items.push({
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        items.push({
            icon: Ws.Graph.NODE_TBAR_EDIT_PROPERTIES,
            icon_d : Ws.Graph.NODE_TBAR_EDIT_PROPERTIES_DESATURATE,
            icon_width: 16,
            icon_height: 16,
            tooltip: "Properties",
            callback: function(event, options) {
                self.__tbar_edit_properties(options);
            }
        });

        return items;
    },

    noop: function() {
        // Does nothing.
    }
});

//-----------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(CONTROL_SPLIT, SplitControlNode);
Ws.Graph.NodeUiFactory.register_type(CONTROL_SPLIT, SplitControlNodeUi);
