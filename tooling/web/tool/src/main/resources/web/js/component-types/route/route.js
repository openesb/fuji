/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can ossbtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var CONTROL_ROUTE = "route";

//----------------------------------------------------------------------------------------
var ControlRouteNode = Class.create(BaseNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    CONTROL_ROUTE, this);
        }

        Ws.Utils.apply_defaults(this.properties, {
            code_name: "route"
        });
    },

    get_inbound_connectors: function($super) {
        // If we're a shortcut, we will inherit the connectors of the node we're
        // targeting.
        if (this.shortcut_to) {
            return this.shortcut_to.get_inbound_connectors();
        }

        var i, j, child, child_connectors, child_links;
        var connectors = [];

        var children = this.get_children();
        for (i = 0; i < children.length; i++) {
            child = children[i];

            // If the child in question is a shortcut to us (could be a direct or an
            // indirect one) we should skip it, or we'll go into an infinite loop and
            // start selling apples.
            if (this.graph.is_shortcut_to(child, this)) {
                continue;
            }

            child_connectors = child.get_inbound_connectors();
            child_links = child.get_inbound_links();

            for (j = 0; j < child_links.length; j++) {
                child_connectors[child_links[j].end_node_connector] = null;
            }

            for (j = 0; j < child_connectors.length; j++) {
                if (child_connectors[j] != null) {
                    connectors.push({
                        default_link_type: child_connectors[j].default_link_type,
                        max_links: child_connectors[j].max_links,
                        supported_link_types: child_connectors[j].supported_link_types,

                        __child: child,
                        __child_connector: j
                    });
                }
            }
        }

        return connectors;
    },

    get_outbound_connectors: function($super) {
        // Same as for inbound. Inherit the connectors if we're a shortcut.
        if (this.shortcut_to) {
            return this.shortcut_to.get_outbound_connectors();
        }

        var i, j, child, child_connectors, child_links;

        var connectors = [];

        var children = this.get_children();

        for (i = 0; i < children.length; i++) {
            child = children[i];

            if (this.graph.is_shortcut_to(child, this)) {
                continue;
            }

            child_connectors = child.get_outbound_connectors();
            child_links = child.get_outbound_links();

            for (j = 0; j < child_links.length; j++) {
                child_connectors[child_links[j].start_node_connector] = null;
            }

            for (j = 0; j < child_connectors.length; j++) {
                if (child_connectors[j] != null) {
                    connectors.push({
                        default_link_type: child_connectors[j].default_link_type,
                        max_links: child_connectors[j].max_links,
                        supported_link_types: child_connectors[j].supported_link_types,

                        __child: child,
                        __child_connector: j
                    });
                }
            }
        }

        return connectors;
    },

    pc_get_descriptors: function($super) {
        var container = {
            sections: [
                {
                    display_name: "Basic",
                    description: "Basic settings.",
                    properties: []
                },
                {
                    display_name: "Display",
                    description: "Display settings.",
                    properties: []
                },
                {
                    display_name: "QoS",
                    description: "Quality of Service settings.",
                    properties: [
                        {
                            name: "no_qos_label",
                            type: Ws.PropertiesSheet.LABEL,

                            display_name: "Sorry, we've run out of QoS properties, please check back in a month.",
                            description: ""
                        }
                    ]
                }
            ]
        };

        container.sections[0].properties.push({
            name: "properties_code_name",
            display_name: "Code Name",
            description: "Code name of the component.",
            value: this.properties.code_name,
            type: "string"
        });

        var super_container = $super();
        super_container.properties.each(function(property) {
            container.sections[1].properties.push(property);
        });

        return container;
    },

    noop: function() {
        // Does nothing, a placeholder.
    }
});

//----------------------------------------------------------------------------------------
var ControlRouteNodeUi = Class.create(BaseNodeUi, {
    initialize: function($super, node, options) {
        //this.width = 300;    // Intentionally commented these out to grab
        //this.height = 200;   // your attention. See how they get redefined
                               // in #position().

        this.fill_style = "rgba(230, 230, 255, 0.3)";
        this.inner_margin = -15; // Distance between the border of the route and the
                                 // child nodes. As the nodes do have margins of their
                                 // own sometimes it makes sense to have a negavite
                                 // inner margin.

        $super(node, options);
    },

    get_inbound_connectors: function() {
        var i, connectors = this.node.get_inbound_connectors();

        // If we're a shortcut, we need to position the connectors evenly across the
        // visual element. If we're not, we try to project them to the children's ones.
        for (i = 0; i < connectors.length; i++) {
            var child = connectors[i].__child;
            var child_connector = child.ui.get_inbound_connector(
                    connectors[i].__child_connector);

            this.__position_connector(child, child_connector, connectors, i);
        }

        if (this.node.shortcut_to) {
            // TODO. Ugly hack is to assume that inbound will all be on WEST and NORTH.
            var west_total = 0, north_total = 0;

            for (i = 0; i < connectors.length; i++) {
                if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_WEST) {
                    west_total++;
                } else if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_NORTH) {
                    north_total++;
                }
            }

            var west_count = 1, north_count = 1;
            for (i = 0; i < connectors.length; i++) {
                if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_WEST) {
                    connectors[i].x = this.margin;
                    connectors[i].y = this.margin + (this.height - (this.margin * 2)) * (west_count / (west_total + 1));

                    west_count++;
                } else if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_NORTH) {
                    connectors[i].x = this.margin + (this.width - (this.margin * 2)) * (north_count / (north_total + 1));
                    connectors[i].y = this.margin;
                    north_count++;
                }
            }
        }

        return connectors;
    },

    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();

        for (var i = 0; i < connectors.length; i++) {
            var child = connectors[i].__child;
            var child_connector = child.ui.get_outbound_connector(
                    connectors[i].__child_connector);

            this.__position_connector(child, child_connector, connectors, i);
        }

        // See #get_inbound_connectors()
        if (this.node.shortcut_to) {
            // TODO. Ugly hack is to assume that inbound will all be on EAST and SOUTH.
            var east_total = 0, south_total = 0;

            for (i = 0; i < connectors.length; i++) {
                if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_EAST) {
                    east_total++;
                } else if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_SOUTH) {
                    south_total++;
                }
            }

            var east_count = 1, south_count = 1;
            for (i = 0; i < connectors.length; i++) {
                if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_EAST) {
                    connectors[i].x = this.width - this.margin;
                    connectors[i].y = this.margin + (this.height - (this.margin * 2)) * (east_count / (east_total + 1));

                    east_count++;
                } else if (connectors[i].type == Ws.Graph.NODE_CONNECTOR_SOUTH) {
                    connectors[i].x = this.margin + (this.width - (this.margin * 2)) * (south_count / (south_total + 1));
                    connectors[i].y = this.height - this.margin;
                    south_count++;
                }
            }
        }

        return connectors;
    },

    repaint: function($super) {
        $super();

        if (!this.node.shortcut_to) {
            this.draw_inner_connections();
        }
    },

    position: function($super) {
        // If the hode has children, we need to adapt its position, width
        // and height to those of its children. If we're a shortcut, all this is not
        // needed at all.
        if (!this.node.shortcut_to) {
            var children = this.node.get_children();
            if (children.length != 0) {
                var i;
                var d_x = children[0].ui.x, d_y = children[0].ui.y;
                var width = 0, height = 0;

                for (i = 1; i < children.length; i++) {
                    if (children[i].ui.x < d_x) {
                        d_x = children[i].ui.x;
                    }

                    if (children[i].ui.y < d_y) {
                        d_y = children[i].ui.y;
                    }
                }

                for (i = 0; i < children.length; i++) {
                    children[i].ui.x = children[i].ui.x - d_x;
                    children[i].ui.y = children[i].ui.y - d_y;

                    var current_width = children[i].ui.x + children[i].ui.width;
                    var current_height = children[i].ui.y + children[i].ui.height;

                    if (current_width > width) width = current_width;
                    if (current_height > height) height = current_height;
                }

                this.width = width + (this.margin * 2) + (this.inner_margin * 2);
                this.height = height + (this.margin * 2) + (this.inner_margin * 2);
                this.x = this.x + d_x;
                this.y = this.y + d_y;
            } else {
                this.width = 300;
                this.height = 300;
            }
        }

        $super();
    },

    prepare_path: function($super) {
        // Basically it is a rectangle. Unless we're a shortcut, then we draw ourselves
        // as a regular node.
        if (!this.node.shortcut_to) {
            this.context.beginPath();

            this.context.moveTo(this.margin, this.margin);
            this.context.lineTo(this.width - this.margin, this.margin);
            this.context.lineTo(this.width - this.margin, this.height - this.margin);
            this.context.lineTo(this.margin, this.height - this.margin);
            this.context.closePath();
        } else {
            $super();
        }
    },

    dt_accept_drop: function($super, drag_source, x, y) {
        var point = this.__get_point_relative_to_node(x, y);
        
        // If the default node will accept the drop -- that IS good. Otherwise we'll make
        // some additional checks.

        if ($super(drag_source, x, y)) {
            return true;
        }

        // If we're a shortcut, we do not want to do anything else.
        if (this.node.shortcut_to) {
            return false;
        }

        // If the point is not above the node, we can't process it.
        if (!((point.x > this.margin) &&
            (point.x < this.width - this.margin) &&
            (point.y > this.margin) &&
            (point.y < this.height - this.margin))) {

            return false;
        }

        // If the drag_source is a node descriptor which flew in from the palette (or
        // anywhere else), we're OK with it.
        if (drag_source.node_type && drag_source.node_data) {
            return true;
        }

        // If it's not a node descriptor, we will NOT accept anything that is not a graph
        // element.
        if (!drag_source.__graph_ui || !drag_source.__graph_ui.__dragged) {
            return false;
        }

        // We will accept node drops, if the node being dragged belongs to the same
        // graph and matches the coordinates. We will not however accept drops of nodes
        // which are already children of the current one. We will not accept drops of
        // nodes that are in the parent chain of the current one. And of course we will
        // not accept drops of ourselves. :)
        if (drag_source.__graph_ui.__dragged.node) {
            var node = drag_source.__graph_ui.__dragged.node;

            if (node == this.node) {
                return false;
            }

            if (this.node.is_parent_of(node)) {
                return false;
            }

            if (node.is_parent_of(this.node)) {
                return false;
            }

            if (node.graph == this.node.graph) {
                return true;
            }
        }

        return false;
    },

    dt_handle_drop: function($super, drag_source, x, y) {
        var node;

        if (drag_source.node_type && drag_source.node_data) {
            var real_data = Ws.Utils.clone_object(drag_source.node_data);
            real_data.parent = this.node;
            
            // Manually copy the functions, as clone_object will not copy them.
            real_data.pre_add_hook = drag_source.node_data.pre_add_hook;
            real_data.post_add_hook = drag_source.node_data.post_add_hook;
            real_data.pre_remove_hook = drag_source.node_data.pre_remove_hook;
            real_data.post_remove_hook = drag_source.node_data.post_remove_hook;


            node = this.node.graph.create_and_add_node(drag_source.node_type, real_data);
            node.ui.move_to(
                    x - drag_source.drag_start_x - node.ui.margin - this.x - this.margin - this.inner_margin,
                    y - drag_source.drag_start_y - node.ui.margin - this.y - this.margin - this.inner_margin);
            
            return;
        }

        // Again, if it's not a node descriptor, we will NOT accept anything that is
        // not a graph element.
        if (!drag_source.__graph_ui || !drag_source.__graph_ui.__dragged) {
            return;
        }

        if (drag_source.__graph_ui.__dragged.node) {
            node = drag_source.__graph_ui.__dragged.node;

            node.parent = this.node;
            node.ui.move_to(
                node.ui.x - this.x - this.margin - this.inner_margin,
                node.ui.y - this.y - this.margin - this.inner_margin);
            this.graph_ui.bring_to_front(node.ui);
        } else {
            // Let the default implementation do its magic.
            $super(drag_source, x, y);
        }

    },

    draw_icon: function($super) {
        // If we're not a shortcut, we have no icon. Otherwise -- fall back to default
        // behavior.
        if (this.node.shortcut_to) {
            $super();
        }
    },

    draw_toolbar: function($super) {
        if (!this.node.shortcut_to) {
            this.toolbar_x = this.width - this.margin - 5;
            this.toolbar_y = this.margin - 10;
        }

        $super();
    },

    draw_inner_connections: function() {
        // So, here we emphasize which node the external connector
        // corresponds to.
        //
        // Rule of thumb #1: get the list of children, find those with empty
        // connectors (either inbound or outbound), map  those to the
        // borders.
        //
        // Rule of thumb #2: WEST connectors go to the west on the same
        // height as the connector itself. EAST -- same stuff, but to the
        // east. SOUTH -- to the south, on the same width as the original
        // connector. NORTH... you get the idea. ORTHOGONAL and FREEFORM --
        // to whichever side is closer.
        //
        // Rule of thumb #3: External connectors are never ORTHOGONAL or
        // FREEFORM.
        var i, j, child, connectors, connector, links;

        var children = this.node.get_children();
        for (i = 0; i < children.length; i++) {
            child = children[i];

            if (this.node.graph.is_shortcut_to(child, this.node)) {
                continue;
            }

            // The inbound connectors first.
            connectors = child.ui.get_inbound_connectors();
            links = child.get_inbound_links();

            for (j = 0; j < links.length; j++) {
                connectors[links[j].end_node_connector] = null;
            }

            this.context.strokeStyle = "rgba(128, 255, 128, 0.6)";
            this.context.lineWidth = 2;

            for (j = 0; j < connectors.length; j++) {
                if (connectors[j] != null) {
                    connector = connectors[j];

                    this.__draw_inner_connection(child, connector, true);
                }
            }

            // Then the outbound ones.
            connectors = child.ui.get_outbound_connectors();
            links = child.get_outbound_links();

            for (j = 0; j < links.length; j++) {
                connectors[links[j].start_node_connector] = null;
            }

            this.context.strokeStyle = "rgba(255, 128, 128, 0.6)";
            this.context.lineWidth = 2;

            for (j = 0; j < connectors.length; j++) {
                if (connectors[j] != null) {
                    connector = connectors[j];

                    this.__draw_inner_connection(child, connector, false);
                }
            }
        }
    },

    // Protected -------------------------------------------------------------------------
    __draw_inner_connection: function(child, connector, inbound) {
        switch (connector.type) {
            case Ws.Graph.NODE_CONNECTOR_WEST:
                this.context.beginPath();

                this.context.moveTo(
                        this.margin,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.lineTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.stroke();
                break;
            case Ws.Graph.NODE_CONNECTOR_NORTH:
                this.context.beginPath();

                this.context.moveTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.margin);
                this.context.lineTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.stroke();
                break;
            case Ws.Graph.NODE_CONNECTOR_EAST:
                this.context.beginPath();

                this.context.moveTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.lineTo(
                        this.width - this.margin,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.stroke();
                break;
            case Ws.Graph.NODE_CONNECTOR_SOUTH:
                this.context.beginPath();

                this.context.moveTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.lineTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.height - this.margin);
                this.context.stroke();
                break;
            case Ws.Graph.NODE_CONNECTOR_ORTHOGONAL:
            case Ws.Graph.NODE_CONNECTOR_FREEFORM:
            default:
                // TODO. Ugly hack -- we treat all these as EAST.
                this.context.beginPath();

                this.context.moveTo(
                        this.margin + this.inner_margin + child.ui.x + connector.x,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.lineTo(
                        this.width - this.margin,
                        this.margin + this.inner_margin + child.ui.y + connector.y);
                this.context.stroke();
                break;
        }
    },

    __position_connector: function(child, child_connector, connectors, i) {
        var margin = this.margin;
        var gross_margin = this.margin + this.inner_margin;

        switch (child_connector.type) {
            case Ws.Graph.NODE_CONNECTOR_WEST:
                connectors[i].type = Ws.Graph.NODE_CONNECTOR_WEST;
                connectors[i].x = margin;
                connectors[i].y = gross_margin + child.ui.y + child_connector.y;
                break;
            case Ws.Graph.NODE_CONNECTOR_NORTH:
                connectors[i].type = Ws.Graph.NODE_CONNECTOR_NORTH;
                connectors[i].x = gross_margin + child.ui.x + child_connector.x;
                connectors[i].y = margin;
                break;
            case Ws.Graph.NODE_CONNECTOR_EAST:
                connectors[i].type = Ws.Graph.NODE_CONNECTOR_EAST;
                connectors[i].x = this.width - margin;
                connectors[i].y = gross_margin + child.ui.y + child_connector.y;
                break;
            case Ws.Graph.NODE_CONNECTOR_SOUTH:
                connectors[i].type = Ws.Graph.NODE_CONNECTOR_SOUTH;
                connectors[i].x = gross_margin + child.ui.x + child_connector.x;
                connectors[i].y = this.height - margin;
                break;
            case Ws.Graph.NODE_CONNECTOR_ORTHOGONAL:
            case Ws.Graph.NODE_CONNECTOR_FREEFORM:
            default:
                // TODO. Ugly hack -- we treat all these as EAST.
                connectors[i].type = Ws.Graph.NODE_CONNECTOR_EAST;
                connectors[i].x = this.width - margin;
                connectors[i].y = gross_margin + child.ui.y + child_connector.y;
                break;
        }

        return child_connector;
    },

    noop: function() {
        // Does nothing. A placeholder.
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(CONTROL_ROUTE, ControlRouteNode);
Ws.Graph.NodeUiFactory.register_type(CONTROL_ROUTE, ControlRouteNodeUi);
