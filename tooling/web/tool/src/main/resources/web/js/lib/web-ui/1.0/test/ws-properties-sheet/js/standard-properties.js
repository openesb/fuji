var standard_properties_container = {
    pc_get_descriptors: function() {
        return {
            properties: [
                {
                    name: "test_string",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Test String",
                    description: "Description of String.",
                    inline_description: "Enter someting like aaaaa to show a hidden property!",
                    value: "asdasdasdasd"
                }
                ,
                {
                    name: "test_string_read_only",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Test ReadOnly",
                    description: "Description of String.",
                    inline_description: "Enter someting like aaaaa to show a hidden property!",
                    value: "Test ReadOnly",

                    read_only: true
                }
                ,
                {
                    name: "test_dependent_string",
                    type: Ws.PropertiesSheet.STRING,

                    display_name: "Dependent String",
                    description: "Description of Dependent String.",
                    value: "asdasdasdasd"

                    ,

                    depends: {
                        or: [
                            {
                                name: "test_string",
                                value: "aaaaa"
                            },
                            {
                                name: "test_string",
                                value_set: ["bbb", "ccc"]
                            }
                        ]
                    }

                }
                ,
                {
                    name: "test_integer",
                    type: Ws.PropertiesSheet.INTEGER,

                    display_name: "Test Integer",
                    description: "Description of Integer.",
                    value: "123"
                }
                ,
                {
                    name: "test_text",
                    type: Ws.PropertiesSheet.TEXT,

                    display_name: "Test Text",
                    description: "Description of Text.",
                    inline_description: "Bugaga. :-) Here you go, sir.",
                    value: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                }
                ,
                {
                    name: "test_code",
                    type: Ws.PropertiesSheet.CODE,
                    code_type: "java",

                    display_name: "Test Code",
                    description: "Description of Code.",
                    inline_description: "Bugaga. :-) Here you go, sir.",
                    value: "private static final String PI=\"3.14159\";\n public void getA() {\nreturn a;\n} ",
                    auto_adjust_height: true,
                    maximizable: true
                }
                ,
                {
                    name: "test_enum",
                    type: Ws.PropertiesSheet.ENUM,

                    display_name: "Test Enum",
                    description: "Description of Enum.",
                    value: "asdfgh",

                    values: [
                        {
                            display_name: "QWERTY",
                            description: "QWERTY Description",
                            value: "qwerty"
                        },
                        {
                            display_name: "ASDFGH",
                            description: "ASDFGH Description",
                            value: "asdfgh"
                        },
                        {
                            display_name: "ZXCVBN",
                            description: "ZXCVBN Description",
                            value: "zxcvbn"
                        }
                    ]
                }
                ,
                {
                    name: "test_boolean",
                    type: Ws.PropertiesSheet.BOOLEAN,

                    display_name: "Test Boolean",
                    description: "Description of Boolean.",
                    value: true
                }
                ,
                {
                    name: "test_text_maximizable",
                    type: Ws.PropertiesSheet.TEXT,

                    display_name: "Maximizable Text",
                    description: "Description of Text.",
                    value: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",

                    maximizable: true
                }
                ,
                {
                    name: "test_code_maximizable",
                    type: Ws.PropertiesSheet.CODE,

                    display_name: "Maximizable Code",
                    description: "Description of Code.",
                    value: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",

                    maximizable: true
                }
                ,
                {
                    name: "test_password",
                    type: Ws.PropertiesSheet.PASSWORD,

                    display_name: "Test Password",
                    description: "Description of Password.",
                    value: "secretpassword"
                }
                ,
                {
                    name: "test_array",
                    type: Ws.PropertiesSheet.ARRAY,

                    display_name: "Test Array",
                    description: "Description of Test Array",
                    value: [
                        "array value 1",
                        123
                    ],

                    display_name_suffix: " - %i",
                    description_suffix: " (Item #%i)",
                    types: [
                        Ws.PropertiesSheet.STRING,
                        Ws.PropertiesSheet.INTEGER
                    ],

                    depends: {
                        name: "test_string",
                        value: "aaaaa"
                    }
                }
                ,
                {
                    name: "test_array_2",
                    type: Ws.PropertiesSheet.ARRAY,

                    display_name: "Test Array",
                    description: "Description of Test Array",
                    value: [
                        "array value 1",
                        "array value 2"
                    ],

                    display_name_suffix: ": %i",
                    description_suffix: " (Item #%i)",
                    items_type: Ws.PropertiesSheet.STRING
                }
                ,
                {
                    name: "dynamic_enum",
                    type: Ws.PropertiesSheet.ENUM,

                    description: "Description of Dynamic Enum.",
                    value: "asdfgh",

                    dynamic: [
                        {
                            name: "test_string",
                            value_set: ["bbb", "ccc"],
                            
                            changes: {
                                values: [
                                    {
                                        display_name: "QWERTY",
                                        description: "QWERTY Description",
                                        value: "qwerty"
                                    }
                                ],
                                display_name: "bbb|ccc"
                            }
                        },
                        {
                            name: "test_string",
                            value: "aaa",

                            changes: {
                                values: [
                                    {
                                        display_name: "ZXCVBN",
                                        description: "ZXCVBN Description",
                                        value: "zxcvbn"
                                    },
                                    {
                                        display_name: "ASDFGH",
                                        description: "ASDFGH Description",
                                        value: "asdfgh"
                                    }
                                ],
                                display_name: "aaa"
                            }
                        }
                    ],

                    dynamic_defaults: {
                        values: [
                            {
                                display_name: "QWERTY",
                                description: "QWERTY Description",
                                value: "qwerty"
                            },
                            {
                                display_name: "ASDFGH",
                                description: "ASDFGH Description",
                                value: "asdfgh"
                            },
                            {
                                display_name: "ZXCVBN",
                                description: "ZXCVBN Description",
                                value: "zxcvbn"
                            }
                        ],
                        display_name: "Dynamic Enum"
                    }
                }
            ]
        };
    },

    pc_set_properties: function(properties) {
        var html = "";

        for (var i in properties) {
            html += "" + i + " => " + properties[i] + "<br/>";
        }

        $("logger").innerHTML = html;
    }
};
