/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.Palette = Class.create(Ws.Bayan, {
    enable_search: false,
    
    initialize: function($super, element, options, fw_options, rr_options) {
        if (!fw_options) {
            fw_options = {};
        }
        var self = this;
        if (fw_options.resize_callbacks) {
            fw_options.resize_callbacks.push(function() {
                self.__update_search();
            });

        } else {
            fw_options.resize_callbacks = [function() {
                self.__update_search();
            }
            ]
        }
        fw_options.without_buttons = true;

        $super(element, options, fw_options, rr_options);

        this.element.ws_palette = this;
        if (this.enable_search) {
            var wsfw_footer = this.element.select(
                "." + this.element.ws_fwindow.footer_classname).reduce();

            this.search_wrapper = wsfw_footer.select(".contents").reduce();
            this.search_icon = this.search_wrapper.select(".search-icon").reduce();
            this.search_field = this.search_wrapper.select(".search-field").reduce();
            
            
            Event.observe(this.search_field, "keyup", function() {
                self.__filter(self.search_field.value);
            });
            this.__update_search();
        }
    },

    add_item: function(item, section, skip__update) {
        // If the section we're trying to add the item to is not present in the sections
        // list of this palette.
        if (this.sections.indexOf(section) == -1) {
            return;
        }

        // Apply default values to item's properties.
        Ws.Utils.apply_defaults(item, {
            tags: []
        });

        // Add the item to the list. If the item has an offset, it will be added
        // according to it. Otherwise it will be added to the end of items list. If the
        // section already has items without offset, they will stay at the end.
        var insert_index = 0;
        if (!isNaN(item.offset)) {
            for (insert_index = 0; insert_index < section.items.length; insert_index++) {
                if (isNaN(section.items[insert_index].offset)) {
                    break;
                }

                if (section.items[insert_index].offset > item.offset) {
                    break;
                }
            }
        } else {
            insert_index = section.items.length;
        }

        // Initialize the item's elements and arrange them.
        var item_element = new Element("div");
        var item_icon_element = new Element("img");
        var item_label_element = new Element("span");

        item_element.addClassName("item");
        item_element.setAttribute("title", item.description);
        item_element.appendChild(item_icon_element);
        item_element.appendChild(item_label_element);

        item_icon_element.addClassName("icon");
        item_icon_element.src = item.icon;

        item_label_element.addClassName("label");
        item_label_element.innerHTML = item.display_name;

        item.element = item_element;

        // Add the item's element to the DOM.
        if (insert_index == section.items.length) {
            section.contents_element.appendChild(item_element);
        } else {
            var existing_item_elements = section.contents_element.select(".item");
            
            section.contents_element.insertBefore(
                    item_element, existing_item_elements[insert_index]);
        }

        // Add the item to the list of item in the model.
        if (insert_index < section.items.length) {
            section.items = section.items.slice(0, insert_index).
                    concat(item).concat(section.items.slice(insert_index, section.items.length));
        } else {
            section.items.push(item);
        }

        // If the item is a drag source, perform the required initialization, i.e. add
        // the necessary methods to the element and register it as a drag source in the
        // Ws.DragManager. Also copy the 'item_data' to the element, so it is visible to
        // the drop target.
        var palette = this;
        if (item.is_drag_source) {
            item_element.ds_accept_drag = function(x, y) {
                var p_scroll_offset = Element.cumulativeScrollOffset(palette.element);
                x = x - p_scroll_offset.left;
                y = y - p_scroll_offset.top;

                var c_offset = Element.cumulativeOffset(this);
                var c_scroll_offset = Element.cumulativeScrollOffset(this);

                var offset = {
                    x: c_offset.left - c_scroll_offset.left,
                    y: c_offset.top - c_scroll_offset.top
                };

                if ((x > offset.x) &&
                        (x < offset.x + Element.getWidth(this)) &&
                        (y > offset.y) &&
                        (y < offset.y + Element.getHeight(this))) {

                    this.drag_start_x = x - offset.x - p_scroll_offset.left;
                    this.drag_start_y = y - offset.y - p_scroll_offset.top;

                    this.drag_shadow = new Element("img");
                    this.drag_shadow.src = item.drag_shadow;
                    this.drag_shadow.setStyle({
                        zIndex: 1000000
                    });

                    return true;
                }

                return false;
            }

            item_element.ds_drag_started = function() {
                if (this.drag_shadow) {
                    document.body.appendChild(this.drag_shadow);
                }
            }

            item_element.ds_dragged_to = function(x, y) {
                if (this.drag_shadow) {
                    this.drag_shadow.setStyle({
                        position: "absolute",
                        left: (x - this.drag_start_x) + "px",
                        top: (y - this.drag_start_y) + "px"
                    });
                }
            }

            item_element.ds_drag_finished = function(x, y) {
                if (this.drag_shadow) {
                    document.body.removeChild(this.drag_shadow);

                    this.drag_start_x = null;
                    this.drag_start_y = null;
                    this.drag_shadow = null;
                }
            }

            Ws.Utils.apply_defaults(item_element, item.item_data);

            Ws.DragManager.add_drag_source(item_element);
        }
        
        // Since we've added an item, the 'empty section' element should now be hidden.
        section.empty_element.setStyle({
            display: "none"
        });

        var height = Element.getHeight(item_element) +
            Ws.Utils.get_bmp(item_element).get_margin_vertical();
        section.preferred_height += height;
        section.preferred_content_height += height;

        if (section.items.length == 1) {
            section.preferred_height -= section.min_content_height;
            section.preferred_content_height -= section.min_content_height;
        }

        // Update the palette layout if necessary.
        if (!skip__update) this.__update();
    },

    remove_item: function(item, skip__update) {
        for (var i = 0; i < this.sections.length; i++) {
            var index = this.sections[i].items.indexOf(item);

            if (index != -1) {
                this.sections[i].items = this.sections[i].items.splice(index, 1);

                if (item.is_drag_source) {
                    Ws.DragManager.remove_drag_source(item.element);
                }

                if (item.element) {
                    item.element.parentNode.removeChild(item.element);


                    var height = Element.getHeight(item.element) +
                    Ws.Utils.get_bmp(item.element).get_margin_vertical();
                    this.sections[i].preferred_height -= height;
                    this.sections[i].preferred_content_height -= height;

                    if (this.sections[i].items.length == 0) {
                        this.sections[i].preferred_height += this.sections[i].min_content_height;
                        this.sections[i].preferred_content_height += this.sections[i].min_content_height;
                    }
                }
                break;
            }
        }

        if (!skip__update) this.__update();
    },

    get_item: function(id, section) {
        for (var i = 0; i < section.items.length; i++) {
            if (section.items[i].id == id) {
                return section.items[i];
            }
        }

        return null;
    },


    __update_search: function() {
    // Last, we need to adjust the size of the search field in the palette
    // window footer. If search is enabled, of course.
        if (this.enable_search) {
            var footer_content_bmp = Ws.Utils.get_bmp(this.search_wrapper);
            var search_icon_bmp = Ws.Utils.get_bmp(this.search_icon);
            var search_field_bmp = Ws.Utils.get_bmp(this.search_field);

            var search_field_width = Element.getWidth(this.search_wrapper)  -
                    footer_content_bmp.get_bmp_horizontal() -
                    Element.getWidth(this.search_icon) -
                    search_icon_bmp.get_bmp_horizontal() -
                    search_field_bmp.get_bmp_horizontal();

            this.search_field.setStyle({
                width: search_field_width + "px"
            });
        }
    },

    __filter: function(filter_string) {
        for (var i = 0; i < this.sections.length; i++) {
            this.sections[i].no_match_element.setStyle({
                display: "none"
            });

            var some_items_matched = false;

            for (var j = 0; j < this.sections[i].items.length; j++) {
                var item = this.sections[i].items[j];

                var hide_item = true;
                if (filter_string == "") {
                    hide_item = false;
                } else {
                    for (var k = 0; k < item.tags.length; k++) {
                        if (item.tags[k].indexOf(filter_string) != -1) {
                            hide_item = false;
                            break;
                        }
                    }
                }

                if (hide_item) {
                    item.element.setStyle({
                        display: "none"
                    });
                } else {
                    item.element.setStyle({
                        display: "block"
                    });

                    some_items_matched = true;
                }
            }

            if (!some_items_matched && (this.sections[i].items.length != 0)) {
                this.sections[i].no_match_element.setStyle({
                    display: "block"
                });
            }
        }
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});
