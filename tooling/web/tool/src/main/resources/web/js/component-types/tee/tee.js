/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
var CONTROL_TEE = "tee";

//----------------------------------------------------------------------------------------
var TeeControlNode = Class.create(Ws.Graph.DefaultNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, true);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    CONTROL_TEE, this, ui_options);
        }

        // Tee has no properties
        //Ws.Utils.apply_defaults(this.properties, {
        //});
    },

    get_inbound_connectors: function() {
        return [
            {
                max_links: 1
            }
        ];
    },

    get_outbound_connectors: function() {
        return [
            {
                max_links: 1
            },
            {
                max_links: 1
            }
        ];
    }
});

//----------------------------------------------------------------------------------------
var TeeControlNodeUi = Class.create(Ws.Graph.DefaultNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);

        this.shape_size = 44;
        this.skew_size = 10;
        this.width = this.shape_size + (this.margin * 2);
        this.height = this.shape_size - 5 + (this.margin * 2);

        this.toolbar_x = this.width - this.margin;
    },

    get_inbound_connectors: function() {
        var connectors = this.node.get_inbound_connectors();

        connectors[0].x = this.margin,
        connectors[0].y = (this.height / 2) - (this.skew_size / 2) + 1,
        connectors[0].type = Ws.Graph.NODE_CONNECTOR_WEST

        return connectors;
    },

    get_outbound_connectors: function() {
        var connectors = this.node.get_outbound_connectors();

        connectors[0].x = this.width - this.margin,
        connectors[0].y = (this.height / 2) - (this.skew_size / 2) + 1,
        connectors[0].type = Ws.Graph.NODE_CONNECTOR_EAST

        connectors[1].x = this.width / 2,
        connectors[1].y = this.height - this.margin,
        connectors[1].type = Ws.Graph.NODE_CONNECTOR_SOUTH

        return connectors;
    },

    cl_accept_doubleclick: function(x, y, event) {
        // Do not accept double clicks, as there are no properties for
        // tee.
        return false;
    },

    prepare_path: function() {
        // A------B
        // |      |
        // |      |
        // C      D
        //  \    /
        //   E--F
        var A_x = this.margin, A_y = this.margin;
        var B_x = this.width - this.margin, B_y = this.margin;
        var C_x = this.margin, C_y = this.height - this.margin - this.skew_size;
        var D_x = this.width - this.margin, D_y = this.height - this.margin - this.skew_size;
        var E_x = this.margin + this.skew_size, E_y = this.height - this.margin;
        var F_x = this.width - this.margin - this.skew_size, F_y = this.height - this.margin;

        this.context.beginPath();
        this.context.moveTo(A_x, A_y);
        this.context.lineTo(C_x, C_y);
        this.context.lineTo(E_x, E_y);
        this.context.lineTo(F_x, F_y);
        this.context.lineTo(D_x, D_y);
        this.context.lineTo(B_x, B_y);
        this.context.lineTo(A_x, A_y);
    },

    draw_node: function($super) {
        $super();

        var base_x = this.width / 2;
        var base_y = (this.height / 2) - (this.skew_size / 2) + 1;

        this.context.lineWidth = 1;
        this.context.fillStyle = this.context.strokeStyle;

        this.context.beginPath();
        this.context.moveTo(base_x - 15, base_y);
        this.context.lineTo(base_x + 15, base_y);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.arc(base_x, base_y, 2, 0, 2* Math.PI, true);
        this.context.stroke();
        this.context.fill();

        this.context.beginPath();
        this.context.moveTo(base_x, base_y);
        this.context.lineTo(base_x, base_y + 15);
        this.context.lineTo(base_x - 2, base_y + 10);
        this.context.lineTo(base_x + 2, base_y + 10);
        this.context.lineTo(base_x, base_y + 15);
        this.context.stroke();
        this.context.fill();
    },

    draw_text: function() {
        // Does nothing (no text on tee)
    },

    draw_icon: function() {
        // Does nothing (no icon on tee)
    },

    __get_toolbar_items: function($super) {
        var self = this;
        var items = [];

        if (!this.read_only) {
            items.push({
                icon: Ws.Graph.NODE_TBAR_DELETE,
                icon_d : Ws.Graph.NODE_TBAR_DELETE_DESATURATE,
                icon_width: 16,
                icon_height: 16,
                tooltip: "Delete",
                callback: function(event) {
                    self.__tbar_delete();
                }
            });
        }

        return items;
    },

    noop: function() {
        // Does nothing.
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(CONTROL_TEE, TeeControlNode);
Ws.Graph.NodeUiFactory.register_type(CONTROL_TEE, TeeControlNodeUi);
