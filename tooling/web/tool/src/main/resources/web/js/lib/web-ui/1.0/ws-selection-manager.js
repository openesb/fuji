/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
/////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.SelectionManager = {
    selectables: [],

    __ready_to_select: null,
    __selected: null,

    __last_selection_event: null,
    
    initialize: function() {
        // We add the document as a fake selectable which will consume the selection in
        // case nothing else will.

        document.sl_accept_selection = function() { return true; };
        document.sl_select = function() { };
        document.sl_deselect = function() { };
        document.z_index = -1;

        this.add_selectable(document);
    },
    
    add_selectable: function(selectable) {
        this.selectables.push(selectable);

        Event.observe(selectable, "mousedown", this.__mousedown);
        Event.observe(selectable, "mousemove", this.__mousemove);
        Event.observe(selectable, "mouseup", this.__mouseup);
    },
    
    remove_selectable: function(selectable) {
        for (var i = 0; i < this.selectables.length; i++) {
            if (this.selectables[i] == selectable) {
                this.selectables.splice(i, 1);
                break;
            }
        }

        Event.stopObserving(selectable, "mousedown", this.__mousedown);
        Event.stopObserving(selectable, "mousemove", this.__mousemove);
        Event.stopObserving(selectable, "mouseup", this.__mouseup);
    },

    __mousedown: function(event) {
        // First of all check whether the event wants us to ignore it.
        if (event.ws_selection_manager_ignore) {
            return;
        }

        // Hack for MSIE, if there is no event.currentTarget, replace it with 'this'. Or,
        // it might should have been 'this' from the very beginning.
        if (!event.currentTarget) {
            event.currentTarget = this;
            event.target = event.srcElement;
            event.is_ie = true;
        }

        Ws.SelectionManager.__ready_to_select = true;
    },
    
    __mousemove: function(event) {
        // First of all check whether the event wants us to ignore it.
        if (event.ws_selection_manager_ignore) {
            return;
        }

        // Hack for MSIE, if there is no event.currentTarget, replace it with 'this'. Or,
        // it might should have been 'this' from the very beginning.
        if (!event.currentTarget) {
            event.currentTarget = this;
            event.target = event.srcElement;
            event.is_ie = true;
        }

        Ws.SelectionManager.__ready_to_select = false;
    },
    
    __mouseup: function(event) {
        // First of all check whether the event wants us to ignore it.
        if (event.ws_selection_manager_ignore) {
            return;
        }

        // Hack for MSIE, if there is no event.currentTarget, replace it with 'this'. Or,
        // it might should have been 'this' from the very beginning.
        if (!event.currentTarget) {
            event.currentTarget = this;
            event.target = event.srcElement;
            event.is_ie = true;
        }

        var x = event.clientX;
        var y = event.clientY;

        // If we've recorded a selection event, check whether it is the same event that
        // we are currently handling. If it is -- we simply skip processing, because this
        // means that it was already processed by a different selectable.
        if (event.is_ie) {
            var last = Ws.SelectionManager.__last_selection_event;

            if (last && (last.srcElement == event.srcElement) &&
                    (last.x == event.clientX) && (last.y == event.clientY)) {
                
                return;
            }
        } else {
            if (Ws.SelectionManager.__last_selection_event == event) {
                return;
            }
        }

        if (Ws.SelectionManager.__ready_to_select) {
            var selected_sl = null;
            var selected_z_index = -1;

            // First check the current target of the mouse event. If it accepts the 
            // selection, we don't need to look any further. Otherwise we should iterate
            // over the selectables picking the one we want.
            if (event.currentTarget.sl_accept_selection(x, y, event)) {
                selected_sl = event.currentTarget;
            } else {
                for (var i = 0; i < Ws.SelectionManager.selectables.length; i++) {
                    var selectable = Ws.SelectionManager.selectables[i];

                    if (selectable.sl_accept_selection(x, y, event)) {
                        var z_index = Ws.Utils.get_z_index(selectable);

                        if ((selected_sl == null) || (z_index > selected_z_index)) {
                            selected_sl = selectable;
                            selected_z_index = z_index;
                        }
                    }
                }
            }

            if (selected_sl) {
                // Note that we _always_ go through the deselect/select stage, even if
                // the selected object stays the same. The reason (one of) for that is
                // in Ws.Graph there is only one selectable (the graph itself), which
                // thansfers the selection events to the nodes and links. When we need to
                // switch the selected node, we need to send an event to the graph so
                // that it deselects the node first.

                if (Ws.SelectionManager.__selected) {
                    Ws.SelectionManager.__selected.sl_deselect(x, y, event, selected_sl);
                    Ws.SelectionManager.__selected = null;
                }

                Ws.SelectionManager.__selected = selected_sl;
                selected_sl.sl_select(x, y, event);

                // Since the event was handled by the selection manager, we should
                // cancel the default action. Unless we're selecting the document
                // itself. In this case we gracefully lose the selection.
                if (selected_sl != document) {
                    event.preventDefault();
                }

                // Save the event, so that we can ignore it later. If we're in MSIE,
                // we'll save the event's source element, x and y position, thinking
                // that this will allow us to identify the event.
                if (event.is_ie) {
                    Ws.SelectionManager.__last_selection_event = {
                        srcElement: event.srcElement,
                        x: event.clientX,
                        y: event.clientY
                    }
                } else {
                    Ws.SelectionManager.__last_selection_event = event;
                }
            }
        }
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
};

//////////////////////////////////////////////////////////////////////////////////////////
// Static initialization
Event.observe(window, "load", function() {
    Ws.SelectionManager.initialize();
});
