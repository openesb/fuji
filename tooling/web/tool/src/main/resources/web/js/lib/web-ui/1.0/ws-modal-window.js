/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.ModalWindow = Class.create({
    element: null,
    window_element: null,

    animation_enabled: true,
    animation_duration: 0.5,

    background_fill: "rgba(0, 0, 0, 0.9)",
    
    initialize: function(element, options) {
        if (options) Object.extend(this, options);
        
        Element.extend(element);
        
        this.element = element;
        this.element.ws_mwindow = this;
        
        // Create the modal window element and initialize it as parent to the given
        // element.
        var width = Element.getWidth(this.element);
        var height = Element.getHeight(this.element);
        
        this.window_element = new Element("div");
        this.window_element.setStyle({
            position: "fixed",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            zIndex: "10000001" // ten millions and one
        });
        
        var window_background_element = new Element("div");
        window_background_element.setStyle({
            position: "absolute",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            zIndex: "10000000", // ten millions
            overflow: "hidden"
        });
        
        var window_background_canvas = Ws.Utils.create_canvas();
        window_background_element.appendChild(window_background_canvas);

        var window_contents_element = new Element("div");
        window_contents_element.setStyle({
            position: "absolute",
            top: "50%",
            left: "50%",
            width: width + "px",
            height: height + "px",
            zIndex: "10000001", // ten millions and one
            marginTop: "-" + (height / 2) + "px",
            marginLeft: "-" + (width / 2) + "px"
        });

        this.window_element.appendChild(window_background_element);
        this.window_element.appendChild(window_contents_element);
        this.window_element.appendChild(window_background_canvas);

        document.body.appendChild(this.window_element);

        if (this.element.parentNode) {
            this.element.parentNode.removeChild(this.element);
        }
        
        window_contents_element.appendChild(this.element);

        // We need to draw last, when the canvas is already in the DOM. Otherwise it
        // will not work in MSIE.
        Ws.Utils.draw_rr_center(window_background_canvas, this.background_fill);
    },

    show: function(immediately) {
        if (this.window_element) {
            if (this.animation_enabled && !immediately) {
                this.window_element.appear({ duration: this.animation_duration });
            } else {
                this.window_element.show();
            }
            if (this.show_callback) {
                this.show_callback();
            }
        } else {
            throw "Operation not permitted -- the window is not " +
                    "yet initialized or has been removed."
        }
    },

    close: function(immediately) {
        if (this.window_element) {
            if (this.animation_enabled && !immediately) {
                this.window_element.fade({ duration: this.animation_duration });
            } else {
                this.window_element.hide();
            }
            if (this.close_callback) {
                this.close_callback();
            }
        } else {
            throw "Operation not permitted -- the window is not " +
                    "yet initialized or has been removed."
        }
    },

    remove: function() {
        if (this.window_element) {
            this.window_element.parentNode.removeChild(this.window_element);
            this.window_element = null;
        } else {
            throw "Operation not permitted -- the window is not " +
                    "yet initialized or has been removed."
        }
    },
    
    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
});

//----------------------------------------------------------------------------------------
