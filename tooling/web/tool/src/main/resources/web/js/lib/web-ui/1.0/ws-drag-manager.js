/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
//////////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

//////////////////////////////////////////////////////////////////////////////////////////
Ws.DragManager = {
    drag_sources: [],
    drop_targets: [],

    __active_ds: null,

    initialize: function() {
        Event.observe(document, "mousemove", Ws.DragManager.__mousemove);
        Event.observe(document, "mouseup", Ws.DragManager.__mouseup);
    },
    
    add_drag_source: function(drag_source) {
        Ws.DragManager.drag_sources.push(drag_source);

        Event.observe(drag_source, "mousedown", Ws.DragManager.__mousedown);
    },
    
    remove_drag_source: function(drag_source) {
        for (var i = 0; i < Ws.DragManager.drag_sources.length; i++) {
            if (Ws.DragManager.drag_sources[i] == drag_source) {
                Ws.DragManager.drag_sources.splice(i, 1);
                break;
            }
        }

        Event.stopObserving(drag_source, "mousedown", Ws.DragManager._mousedown);
    },
    
    add_drop_target: function(drop_target) {
        Ws.DragManager.drop_targets.push(drop_target);
    },
    
    remove_drop_target: function(drop_target) {
        for (var i = 0; i < Ws.DragManager.drop_targets.length; i++) {
            if (Ws.DragManager.drop_targets[i] == drop_target) {
                Ws.DragManager.drop_targets.splice(i, 1);
                break;
            }
        }
    },

    __mousedown: function(event) {
        // First of all check whether the event wants us to ignore it.
        if (event.ws_drag_manager_ignore) {
            return;
        }

        // Hack for MSIE, if there is no event.currentTarget, replace it with 'this'. Or,
        // it might should have been 'this' from the very beginning.
        if (!event.currentTarget) {
            event.currentTarget = this;
            event.target = event.srcElement;
            event.is_ie = true;
        }

        var i;

        var x = event.clientX;
        var y = event.clientY;
        
        var selected_ds = null;
        var selected_z_index = -1;

        // First we should try to find out, whether the element that received the event
        // is a drag source, which is OK to be dragged. If it is, we'll use it as the
        // selected one, otherwise will search throught the list of registered drag
        // sources and try to pick one which can be dragged.
        if ((Ws.DragManager.drag_sources.indexOf(event.currentTarget) != -1) &&
                event.currentTarget.ds_accept_drag(x, y, event)) {

            selected_ds = event.currentTarget;
        } else {
            // Try to find a suitable drag source. The first (in list) drag source with
            // the maximum zIndex will be used.
            for (i = 0; i < Ws.DragManager.drag_sources.length; i++) {
                var ds = Ws.DragManager.drag_sources[i];

                if (ds.ds_accept_drag(x, y, event)) {
                    var z_index = Ws.Utils.get_z_index(ds);

                    if ((selected_ds == null) || (z_index > selected_z_index)) {
                        selected_ds = ds;
                        selected_z_index = z_index;
                    }
                }
            }
        }

        if (selected_ds) {
            Ws.DragManager.__active_ds = selected_ds;

            // Since the event was handled by the drag manager, we should cancel the
            // default action.
            event.preventDefault();
        }
    },
    
    __mousemove: function(event) {
        // First of all check whether the event wants us to ignore it.
        if (event.ws_drag_manager_ignore) {
            return;
        }

        // Hack for MSIE, if there is no event.currentTarget, replace it with 'this'. Or,
        // it might should have been 'this' from the very beginning.
        if (!event.currentTarget) {
            event.currentTarget = this;
            event.target = event.srcElement;
            event.is_ie = true;
        }

        var x = event.clientX;
        var y = event.clientY;
        
        if (Ws.DragManager.__active_ds) {
            if (!Ws.DragManager.drag_started) {
                Ws.DragManager.drag_started = true;
                
                if (Ws.DragManager.__active_ds.ds_drag_started) {
                    Ws.DragManager.__active_ds.ds_drag_started(x, y, event);
                }
                
                // Notify all drop targets that something started being dragged
                for (var i = 0; i < Ws.DragManager.drop_targets.length; i++) {
                    var drop_target = Ws.DragManager.drop_targets[i];
                    
                    if (drop_target.dt_drag_started) {
                        drop_target.dt_drag_started(
                                Ws.DragManager.__active_ds, x, y, event);
                    }
                }

                // tooltip delay
                if (Ws.Tooltip) {
                    Ws.Tooltip.SLEEP_TIMEOUT = Ws.Tooltip.SLEEP_TIMEOUT * 3;
                }
            }
            
            if (Ws.DragManager.__active_ds.ds_dragged_to) {
                Ws.DragManager.__active_ds.ds_dragged_to(x, y, event);
            }
            
            // Notify the potential drop targets that something is being 
            // dragged
            for (var j = 0; j < Ws.DragManager.drop_targets.length; j++) {
                if (Ws.DragManager.drop_targets[j].dt_dragged_to) {
                    Ws.DragManager.drop_targets[j].dt_dragged_to(
                            Ws.DragManager.__active_ds, x, y, event);
                }
            }
            
            // Since the event was handled by the drag manager, we should cancel the
            // default action.
            event.preventDefault();
        }
    },
    
    __mouseup: function(event) {
        // First of all check whether the event wants us to ignore it.
        if (event.ws_drag_manager_ignore) {
            return;
        }

        // Hack for MSIE, if there is no event.currentTarget, replace it with 'this'. Or,
        // it might should have been 'this' from the very beginning.
        if (!event.currentTarget) {
            event.currentTarget = this;
            event.target = event.srcElement;
            event.is_ie = true;
        }

        var i;

        var x = event.clientX;
        var y = event.clientY;

        if (Ws.DragManager.drag_started) {
            var selected_dt, selected_z_index;

            // Look for a potential drop target, select the one with the biggest z index.
            for (i = 0; i < Ws.DragManager.drop_targets.length; i++) {
                var dt = Ws.DragManager.drop_targets[i];

                if (dt.dt_accept_drop(Ws.DragManager.__active_ds, x, y, event)) {
                    var z_index = Ws.Utils.get_z_index(dt);

                    if ((selected_dt == null) || (z_index > selected_z_index)) {
                        selected_dt = dt;
                        selected_z_index = z_index;
                    }
                }
            }

            // If we found something that can handle the drop, instruct it to do so.
            if (selected_dt) {
                selected_dt.dt_handle_drop(Ws.DragManager.__active_ds, x, y, event);
            }

            // Notify all drop targets that something finished being dragged.
            for (i = 0; i < Ws.DragManager.drop_targets.length; i++) {
                if (Ws.DragManager.drop_targets[i].dt_drag_finished) {
                    Ws.DragManager.drop_targets[i].dt_drag_finished(
                            Ws.DragManager.__active_ds, x, y, event);
                }
            }
            
            // Notify the dragged object that drag finished.
            if (Ws.DragManager.__active_ds.ds_drag_finished) {
                Ws.DragManager.__active_ds.ds_drag_finished(x, y, event);
            }

            // Since the event was handled by the drag manager, we should cancel the
            // default action.
            event.preventDefault();

            // tooltip delay
                if (Ws.Tooltip) {
                    Ws.Tooltip.SLEEP_TIMEOUT = Ws.Tooltip.SLEEP_TIMEOUT / 3;
                }
        }
        
        Ws.DragManager.drag_started = false;
        Ws.DragManager.__active_ds = null;
    },

    // No-op -----------------------------------------------------------------------------
    noop: function() {
        // Does nothing, a placeholder
    }
};

//////////////////////////////////////////////////////////////////////////////////////////
// Static initialization
Event.observe(window, "load", function() {
    Ws.DragManager.initialize();
});
