/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
/////////////////////////////////////////////////////////////////////////////////////
// Init namespacing variable
var Ws;

if (typeof Ws == "undefined") {
    Ws = {};
}

Ws.MessagesViewer = Class.create({
    element : {},
    bayan : null,
    container : null,
    selected_section : null,
    __editarea_load : false,

    initialize: function(container, element, options) {
        if (options) Object.extend(this, options);

        if (element) {
            Element.extend(element);
        } else {
            element = new Element("div");
        }
        this.element = element;

        if (!this.element.id) {
            this.element.id = "ws-message-viewer-" + Ws.MessagesViewer.counter++;
        }

        this.container = container;
        this.bayan = new Ws.Bayan(element);
        element.messages_viewer = this;

        this.bayan.add_section(                    {
            id: "section-1-list",
            display_name: "Messages List",
            description: "A very descriptive description of section 1.",
            empty_text: "",
            proportion: 0.5,
            items: []
        }, true);

        var self = this;

        var view_type = new Element("div");
        view_type.addClassName("view_type");
        view_type.addClassName("treeView");
        var setcion2 = {
            id: "section-2-content",
            display_name: "content",
            description: "A very descriptive description of section 2.",
            empty_text: "",
            proportion: 0.5,
            items: [],
            view_type : Ws.MessagesViewer.TreeView,
            resize_callback : function(height) {
                self.__update_section_2(height);
            }
        };
        this.bayan.add_section(setcion2, true, {header_buttons: [view_type]});

        view_type.onclick = function() {
            if (setcion2.view_type ==  Ws.MessagesViewer.TreeView) {
                setcion2.view_type = Ws.MessagesViewer.ColorView;
            } else {
                setcion2.view_type = Ws.MessagesViewer.TreeView;
            }
            element.messages_viewer.show_content(
            element.messages_viewer.selected_section,
            element.messages_viewer.bayan.get_section("section-1-list").
            contents_element.select("tr.selected")[0]);
        }
        this.render();
    },

    __update_section_2 : function(height) {
        var section = this.bayan.get_section("section-2-content");

        if (section.view_type == Ws.MessagesViewer.ColorView) {
            var element = $("frame_" + "textarea_" + this.element.id);
            if (element && element != null) {
                element.style.height = (height - 4) + "px";
            }
        }
    },

    render: function() {
        var section = this.bayan.get_section("section-1-list");
        section.preferred_content_height = 1000;
        section.preferred_height = 1000;
        var html = "<div class=\"messages_table\"><table><tbody>";
        for (var i = this.container.messages.length - 1; i > -1; i--) {
            var message = this.container.messages[i];

            html = html + "<tr onclick=\"$('" + this.element.id + "').\n\
                messages_viewer.show_content(" + i + ", this)\" class=\"";
            if (i % 2 == 0 ) {
                html += "A";
            } else {
                html += "B";
            }
            if (i == this.selected_section) {
                html += " selected"
            }
            html +="\""

            html += " onmouseover=\" color_temp = this.style.background;" +
            " this.style.background='#CCCCFF'\";" +
            " onmouseout=\"this.style.background = color_temp\"" +
            "><td>";

            if (message.has_fault) {
               html = html + "<img src=\"resource?img/ui/tracing/message_fault_16.png\"/> "
                + (new Date(message.time).toLocaleString());
            } else {
               html = html + "<img src=\"resource?img/ui/tracing/message_16.png\"/> " +
                   (new Date(message.time).toLocaleString());
            }
            html = html + "</td>"

            html = html + "</tr>"
        }
        html = html +"</tbody></table></div>";
        section.contents_element.innerHTML = html;

        section = this.bayan.get_section("section-2-content");
        section.preferred_content_height = 1000;
        section.preferred_height = 1000;

        if (this.selected_section != null) {
            section.contents_element.innerHTML = "";
            var tr = this.bayan.get_section("section-1-list").
                contents_element.select("tr.selected")[0];
            this.__editarea_load = false;
            this.show_content(this.selected_section, tr);
            html += this.container.messages[this.selected_section].content;
        }
        
        this.bayan.__update();
    },

    show_content : function(selected_index, trElement) {
        var message = this.container.messages[selected_index];
        if (!message) {
            return;
        }
        var section = this.bayan.get_section("section-2-content");
        var textarea = section.contents_element.select("textarea")[0];

        if (message.type) {
            if (section.view_type == Ws.MessagesViewer.TreeView) {
                message.xml_content = (new DOMParser()).
                parseFromString(message.content, "text/xml");
                var tree= [];
                for (var i = 0; i < message.xml_content.childNodes.length; i++) {
                    var node_name = "<span class=\"tag\"><</span>"
                    + message.xml_content.childNodes[i].nodeName;
                    if (message.xml_content.childNodes[i].attributes) {
                        for (var j = 0; j < message.xml_content.childNodes[i].attributes.length; j++) {
                            var attr = message.xml_content.childNodes[i].attributes[j];
                            node_name += " <span class='attribute_name'>" + attr.name +
                            "</span>=<span class='attribute'>\"" + attr.value + "\"</span>";
                        }
                    }
                    node_name += "<span class=\"tag\">/></span>";
                    var node = {
                        name : node_name,
                        data : [],
                        children : [],
                        type : "xml_" + message.xml_content.childNodes[i].nodeType,
                        isExpanded : true
                    }
                    //this.__copy_tree(node, message.xml_content.childNodes[i].attributes);
                
                    if (message.xml_content.childNodes[i].childNodes.length > 0) {
                        this.__copy_tree(node, message.xml_content.childNodes[i].childNodes);
                    }

                    tree.push(node);
                }
            
                new Ws.TreeTable(tree, section.contents_element);
                this.__editarea_load = false;
                if (textarea) {
                    textarea.hide();
                }
            }

            if (section.view_type == Ws.MessagesViewer.ColorView) {
                if (textarea) {
                    textarea.value = message.content;
                } else {
                    var html = "<textarea id='textarea_" + this.element.id  +
                    "'  readonly class='messages_viewer'>";
                    html += this.container.messages[selected_index].content;
                    html += "</textarea>"
                    section.contents_element.innerHTML = html;
                    textarea = section.contents_element.select("textarea")[0];
                }

                if (this.__editarea_load) {
                    editAreaLoader.setValue("textarea_" + this.element.id,
                        message.content);
                } else {
                    var height = Element.getHeight(section.contents_element) -
                        Ws.Utils.get_bmp(section.contents_element).get_border_vertical();
                    textarea.style.height = (height - 4) + "px";
                    editAreaLoader.init({
                        id : "textarea_" + this.element.id,
                        min_height : 5,
                        min_width: 5,
                        syntax: message.type,
                        toolbar: "",
                        start_highlight: true,
                        allow_toggle: false,
                        allow_resize: "no",
                        is_editable : false,
                        cursor_position: ""
                    });
                    this.__editarea_load = true;
                }
            }
        } else {
            if (textarea && !this.__editarea_load) {
                textarea.show();
                textarea.value = message.content;
            } else {
                var html = "<textarea id='textarea_" + this.element.id  +
                "'  readonly class='messages_viewer'>";
                html += this.container.messages[selected_index].content;
                html += "</textarea>"
                section.contents_element.innerHTML = html;
                this.__editarea_load = false;
            }
        }
        this.selected_section = selected_index;

        this.element.select("div.messages_table tr.selected").each(function(element) {
            element.removeClassName("selected");
        });
        trElement.addClassName("selected");
    },

    __copy_tree: function(node, originaltree) {
        for (var i = 0; i < originaltree.length; i++) {
            var node_type = originaltree[i].nodeType;
            var node_name = "<span class='tag'><</span>" + originaltree[i].nodeName;

            if (originaltree[i].attributes) {
                for (var j = 0; j < originaltree[i].attributes.length; j++) {
                    var attr = originaltree[i].attributes[j];
                    node_name += " <span class='attribute_name'>" + attr.name +
                        "</span>=<span class='attribute'>\"" + attr.value + "\"</span>";
                }
  //              this.__copy_tree(node_child, originaltree[i].attributes);
            }
            node_name += "<span class='tag'>/></span>"

            if (node_type == Node.TEXT_NODE) {
                node_name = originaltree[i].textContent.
                    replace(/( ?\r?\n?)/g, "");
         //      replace(" ", "", "gm").replace("\n\r", "", "gm").replace("\n", "", "gm").replace("\r", "", "gm");
                if (node_name.length <= 0) {
                    continue;
                }
                node_name = originaltree[i].textContent;
                //node_name = originaltree[i].textContent.replace(/(\n)/g, "<br/>");
            }

            var node_child = {
                name : node_name,
                data : [],
                children : [],
                type : "xml_" + node_type
            }
          
            if (originaltree[i].childNodes.length > 0) {
                this.__copy_tree(node_child, originaltree[i].childNodes);
            }
            node.children.push(node_child);
        }
    },

    __do_color : function() {
        if (this.selected_section == null) {
            return;
        }
        var message = this.container.messages[this.selected_section];
        if (!message || !message.type) {
            return;
        }
        editAreaLoader.init({
            id : "textarea_" + this.element.id,
            min_height : 5,
            min_width: 5,
            syntax: message.type,
            toolbar: "",
            start_highlight: true,
            allow_toggle: false,
            allow_resize: "no",
            is_editable : false,
            cursor_position: ""
        });
        this.__editarea_load = true;
    }
});

Ws.MessagesViewer.counter = 0;

Ws.MessagesViewer.TreeView = 1;
Ws.MessagesViewer.TreeViewAttributeNode = 2;
Ws.MessagesViewer.ColorView = 3;
