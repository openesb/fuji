/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tooling.web.utils;

import java.awt.Point;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.glassfish.openesb.tooling.web.model.graph.Graph;
import org.glassfish.openesb.tooling.web.model.graph.Link;
import org.glassfish.openesb.tooling.web.model.graph.Node;
import org.glassfish.openesb.tooling.web.model.graph.Property;
import org.glassfish.openesb.tooling.web.model.graph.PropertySet;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ksorokin
 */
public final class Utils {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Logger logger = 
            Logger.getLogger("org.glassfish.openesb.tooling.web");

    // Graph /////////////////////////////////////////////////////////////////////////////
    // We do two things:
    // 1) Make sure all items on the graph have different ids.
    // 2) Make sure all items on the graph do have 'x' and 'y' ui properties set.
    // 2.1) If 'display_name' and 'description' are not set -- we set 'display_name' to
    //      'code_name' and 'description' -- to an empty string.
    public static void normalize(
            final Graph graph) {

        int id = 10;

        final List<Integer> usedIds = new ArrayList<Integer>(100);
        for (Node node: graph.getNodes()) {
            if (usedIds.contains(node.getId()) || (node.getId() == 0)) {
                while (usedIds.contains(id)) {
                    id += 10;
                }
                node.setId(id);
            }

            usedIds.add(node.getId());
        }
        for (Link link: graph.getLinks()) {
            if (usedIds.contains(link.getId()) || (link.getId() == 0)) {
                while (usedIds.contains(id)) {
                    id += 10;
                }
                link.setId(id);
            }

            usedIds.add(link.getId());
        }

        for (Node node: graph.getNodes()) {
            if (node.getUiProperty(Graph.UI_PROPERTY_DISPLAY_NAME) == null) {
                node.setUiProperty(
                        Graph.UI_PROPERTY_DISPLAY_NAME,
                        node.getProperty(Graph.PROPERTY_CODE_NAME));
            }

            if (node.getUiProperty(Graph.UI_PROPERTY_DESCRIPTION) == null) {
                node.setUiProperty(Graph.UI_PROPERTY_DESCRIPTION, "");
            }
        }
    }

    public static void layoutGraph(
            final Graph graph) {

        Point start = new Point(0, 0);

        for (Node node: graph.getStartingPoints()) {
            if (node.getUiProperty(Graph.UI_PROPERTY_X) == null) {
                start = layoutNode(node, new Point(0, start.y));
            } else {
                Point point = new Point(
                        (int) Double.parseDouble(node.getUiProperty(Graph.UI_PROPERTY_X).toString()),
                        (int) Double.parseDouble(node.getUiProperty(Graph.UI_PROPERTY_Y).toString()));

                start = layoutNode(node, point);
            }

            start.y += 100;
        }
    }

    public static Point layoutNode(
            final Node node,
            final Point tentativePoint) {

        final Point point = new Point(tentativePoint);

        if (node.getUiProperty(Graph.UI_PROPERTY_X) == null) {
            node.setUiProperty(Graph.UI_PROPERTY_X, point.x);
            node.setUiProperty(Graph.UI_PROPERTY_Y, point.y);
        } else {
            point.x = (int) Double.parseDouble(
                    node.getUiProperty(Graph.UI_PROPERTY_X).toString());
            point.y = (int) Double.parseDouble(
                    node.getUiProperty(Graph.UI_PROPERTY_Y).toString());
        }

        // We'll be returning the lower/right corner of this branch of the graph.
        final Point result = new Point(point);
        for (int i = 0; i < node.getOutboundLinks().size(); i++) {
            final Link link = node.getOutboundLinks().get(i);

            if (link.getEndNode() != null) {
                final Point candidate = layoutNode(
                        link.getEndNode(),
                        new Point(point.x + 150, point.y + i*100));

                result.x = result.x > candidate.x ? result.x : candidate.x;
                result.y = result.y > candidate.y ? result.y : candidate.y;
            }
        }

        return result;
    }

    // MD5 ///////////////////////////////////////////////////////////////////////////////
    public static String md5(
            final byte[] bytes) {

        try {
            final byte[] digest =
                    MessageDigest.getInstance("MD5").digest(bytes);

            final StringBuilder builder = new StringBuilder();

            for (int i = 0; i < digest.length; i++) {
                builder.append(
                        Integer.toHexString((digest[i] >>> 4) & 0x0f) +
                        Integer.toHexString(digest[i] & 0x0f));
            }

            return builder.toString();
        } catch (NoSuchAlgorithmException e) {
            // Normally this should not happen, unless we're running on some exotic
            // incompatible JVM. We'll log the exception and return null.
            logger.log(Level.SEVERE,
                    "The MD5 message digest algorithm is not available.", e);

            return null;
        }
    }

    public static String md5(
            final String string) {
        try {
            return md5(string, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Normally this should not happen, unless we're running on some exotic
            // incompatible JVM. We'll log the exception and return null.
            logger.log(Level.SEVERE,
                    "UTF-8 encoding is not supported.", e);

            return null;
        }
    }

    public static String md5(
            final String string,
            final String charset) throws UnsupportedEncodingException {

        return md5(string.getBytes(charset));
    }

    // Files /////////////////////////////////////////////////////////////////////////////
    public static void deleteDirectory(
            final File directory) throws IOException {

        if (!directory.exists()) {
            return;
        }

        File[] children = directory.listFiles();

        if ((children != null) && (children.length != 0)) {
            for (File child : children) {
                if (child.isDirectory()) {
                    deleteDirectory(child);
                } else {
                    if (!child.delete()) {
                        throw new IOException(
                                "Failed to delete file: " + child.getAbsolutePath());
                    }
                }
            }
        }

        if (!directory.delete()) {
            throw new IOException(
                    "Failed to delete directory: " + directory.getAbsolutePath());
        }
    }

    // JSON and properties handling //////////////////////////////////////////////////////
    public static String toJson(
            final Object object) {

        if (object == null) {
            return "null";
        }

        if (object instanceof String) {
            String temp = (String) object;

            temp = temp.replace("\\", "\\\\");
            temp = temp.replace("\n", "\\n");
            temp = temp.replace("\r", "\\r");
            temp = temp.replace("\"", "\\\"");

            return "\"" + temp + "\"";
        }

        return object.toString();
    }

    public static String toJson(
            final Property property) {

        if (property.getType().equals(PropertySet.STRING)) {
            return toJson(property.getValue());
        } else if (property.getType().equals(PropertySet.NULL)) {
            return "null";
        }

        return property.getValue();
    }

    public static Properties toProperties(
            final JSONObject json) throws JSONException {

        final Properties properties = new Properties();

        for (String key : JSONObject.getNames(json)) {
            if (!"noop".equals(key)) {
                properties.put(key, json.get(key));
            }
        }

        return properties;
    }

    public static PropertySet toPropertySet(
            final Properties properties) {

        final PropertySet set = new PropertySet();

        for (Object key : properties.keySet()) {
            set.set(key.toString(), properties.get(key));
        }

        return set;
    }

    public static Property toProperty(
            final String name,
            final Object value) {

        String type = PropertySet.STRING;

        if (value instanceof Integer) {
            type = PropertySet.INTEGER;
        }
        if (value instanceof Boolean) {
            type = PropertySet.BOOLEAN;
        }

        return new Property(name, value.toString(), type);
    }

    // Archives //////////////////////////////////////////////////////////////////////////
    public static void archive(
            final File input,
            final File archive) throws IOException {


        archive(input, archive, true);
    }

    public static void archive(
            final File input,
            final File archive,
            final boolean includeRoot) throws IOException {

        if (archive.exists() && archive.isDirectory()) {
            throw new IOException("Trying to create an archive in " + // NOI18N
                    "place of an existing directory."); // NOI18N
        }

        final File archiveParent = archive.getParentFile();
        if (!archiveParent.exists() && !archiveParent.mkdirs()) {
            throw new IOException("Cannot create directory structure " + // NOI18N
                    "for " + archiveParent); // NOI18N
        }

        if (!input.exists()) {
            throw new IOException("Trying to create an archive out " + // NOI18N
                    "of a non-existing file."); // NOI18N
        }

        ZipOutputStream zipOutput = null;

        try {
            zipOutput = new ZipOutputStream(new FileOutputStream(archive));

            final FileFilter filter = new FileFilter() {

                public boolean accept(final File file) {
                    return true;
                }
            };

            if (input.isDirectory() && !includeRoot) {
                final File[] children = input.listFiles();

                if ((children != null) && (children.length > 0)) {
                    for (File child : children) {
                        addToArchive(child, child.getName(), zipOutput, filter);
                    }
                }
            } else {
                addToArchive(input, input.getName(), zipOutput, filter);
            }
        } finally {
            try {
                if (zipOutput != null) {
                    zipOutput.close();
                }
            } catch (IOException e) {
                logger.log(Level.WARNING, "Failed to close an output stream.", e);
            }
        }
    }

    public static void addToArchive(
            final File input,
            final String name,
            final ZipOutputStream zipOutput) throws IOException {

        addToArchive(input, name, zipOutput, new FileFilter() {

            public boolean accept(final File file) {
                return true;
            }
        });
    }

    public static void addToArchive(
            final File input,
            final String name,
            final ZipOutputStream zipOutput,
            final FileFilter filter) throws IOException {

        if (!input.exists()) {
            throw new IOException("Trying to add a non-existing file to " +
                    "the archive."); // NOI18N
        }

        zipOutput.putNextEntry(createZipEntry(input, name));

        if (input.isDirectory()) {
            final File[] children = input.listFiles(filter);

            if ((children != null) && (children.length > 0)) {
                for (File child : children) {
                    final String childName =
                            (name + "/" + child.getName()).replace("//", "/");

                    addToArchive(child, childName, zipOutput, filter);
                }
            }
        } else {
            FileInputStream fileInput = null;

            try {
                fileInput = new FileInputStream(input);

                transferStream(fileInput, zipOutput);
            } finally {
                try {
                    if (fileInput != null) {
                        fileInput.close();
                    }
                } catch (IOException e) {
                    logger.log(
                            Level.WARNING,
                            "Failed to close an input stream.", e);
                }
            }
        }
    }

    public static ZipEntry createZipEntry(
            final File input,
            final String name) {

        assert input.exists();

        final String realName;
        if (input.isDirectory() && !name.endsWith("/")) {
            realName = name + "/";
        } else {
            realName = name;
        }

        final ZipEntry entry = new ZipEntry(realName);
        entry.setSize(input.length());
        entry.setTime(input.lastModified());

        return entry;
    }

    // Streams ///////////////////////////////////////////////////////////////////////////
    public static void transferStream(
            final InputStream input,
            final OutputStream output) throws IOException {

        byte[] buffer = new byte[8 * 1024];
        int length = -1;

        while ((length = input.read(buffer)) != -1) {
            output.write(buffer, 0, length);
        }
    }

    public static void transferStream(
            final InputStream input,
            final OutputStream output,
            final long limit) throws IOException {

        long remaining = limit;

        int bufferSize = 8 * 1024;
        byte[] buffer = new byte[bufferSize];
        int length = -1;

        do {
            if (remaining > bufferSize) {
                length = input.read(buffer);
            } else {
                length = input.read(buffer, 0, (int) remaining);
            }

            if (length == -1) {
                logger.info("Input stream finished prematurely: read " +
                        (limit - remaining) + " bytes out of " + limit + ".");
                return;
            }

            remaining -= length;
            output.write(buffer, 0, length);
        } while (remaining > 0);
    }

    // Miscellanea ///////////////////////////////////////////////////////////////////////
    public static boolean parseBoolean(
            final Object object) {

        return ((object != null) && object.toString().equalsIgnoreCase("true"));
    }

    public String escapeEntitites(
            final String string) {

        String escaped = string;

        escaped = escaped.replace("&", "&amp;");
        escaped = escaped.replace("\"", "&quot;");
        escaped = escaped.replace("<", "&lt;");
        escaped = escaped.replace(">", "&gt;");

        return escaped;
    }

    public String unescapeEntities(
            final String escaped) {

        String string = escaped;

        string = string.replace("&lt;", "<");
        string = string.replace("&gt;", ">");
        string = string.replace("&quot;", "\"");
        string = string.replace("&amp;", "&");

        return string;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private Utils() {
        // Does nothing.
    }
}
