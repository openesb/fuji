/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.io.File;

public class Util {
    public static final String OSGI_FELIX = "felix";
    public static final String OSGI_EQUINOX = "equinox";
    public static final String OSGI_UNKNOWN = "unknown";
    
	public static String getOSGiPlatformType(String installPath) {
		String osgiImpl = OSGI_UNKNOWN;
		File felixJar = new File(installPath, "bin/felix.jar");
		File equinoxJar = new File(installPath, "equinox.jar");
		if ( felixJar.exists()) {
			osgiImpl = OSGI_FELIX;
		} else if ( equinoxJar.exists()) {
			osgiImpl = OSGI_EQUINOX;
		}
		return osgiImpl;
	}
	
    public static boolean isFelixDistribution(File rootDir) {
        File mainJarPath = new File(rootDir, "bin/felix.jar");
        return mainJarPath.exists();
    }

    public static boolean isEquinoxDistribution(File rootDir) {
        File mainJarPath = new File(rootDir, "equinox.jar");
        return mainJarPath.exists();
    }

    public static boolean isGlassfishDistribution(File rootDir) {
        File mainJarPath = new File(rootDir, "modules/admin-cli.jar");
        return mainJarPath.exists();
    }

    public static boolean isFujiInstallDir(String root) {
        boolean isFujiDir = false;
        File rootDir = new File(root);
        if (isFelixDistribution(rootDir) ||
            isEquinoxDistribution(rootDir) ||
            isGlassfishDistribution(rootDir) ) {
            isFujiDir = true;
        }
        return isFujiDir;
    }
	
}
