/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server;

import org.eclipse.osgi.util.NLS;

public class Messages {
	public static String errorDefaultDialogTitle;
	public static String wizardTitle;
	public static String wizardDescription;
	public static String runtimeName;
	public static String installDir;
	public static String browse;
	public static String install;
	public static String selectInstallDir;
	public static String installedJRE;
	public static String installedJREs;
	public static String runtimeDefaultJRE;
	public static String installDirNotValid;
	
	public static String adminPort;
	
	static {
		NLS.initializeMessages(FujiServerPlugin.PLUGIN_ID + ".Messages", Messages.class);
	}
}
