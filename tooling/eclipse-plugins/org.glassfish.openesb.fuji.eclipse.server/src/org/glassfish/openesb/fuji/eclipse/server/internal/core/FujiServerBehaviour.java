/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.wst.server.core.IRuntime;
import org.eclipse.wst.server.core.IRuntimeWorkingCopy;
import org.eclipse.wst.server.core.IServer;
import org.eclipse.wst.server.core.model.ServerBehaviourDelegate;

public class FujiServerBehaviour extends ServerBehaviourDelegate implements IFujiServerBehaviour {
    protected static final Logger LOG = Logger.getLogger(FujiServerBehaviour.class.getName());
    
    private static final String LAUNCH_ATTR_STOP = "stop-server";
	// the thread used to ping the server to check for startup
	protected transient StartUpThread ping = null;
	protected transient IProcess process;
	protected transient IDebugEventSetListener processListener;
	
	// the thread used to ping the server to check for jmx admin
	protected transient FujiPing ioPing = null;
	
	protected transient String osgiPlatformType = null;
	
	@Override
	public void stop(boolean force) {
		if (force) {
			terminate();
			return;
		}
		int state = getServer().getServerState();
		if (state == IServer.STATE_STOPPED) {
			return;
		} else if (state == IServer.STATE_STARTING || state == IServer.STATE_STOPPING) {
			terminate();
			return;
		}
		
		try {
			LOG.info("Stopping Fuji"); //TODO change to finer
			
			if (state != IServer.STATE_STOPPED) {
				setServerState(IServer.STATE_STOPPING);
			}
			
			// We don't launch another program to stop. Try using fuji admin shutdown or just process terminate.
			try {
				String adminPort = this.getFujiRuntime().getAdminPort();
				FujiAdmin fujiAdmin = new FujiAdmin("localhost", adminPort, "", "");
				fujiAdmin.shutdown();
				if ( Util.OSGI_EQUINOX.equals(osgiPlatformType)) {
					terminate();
				}
			} catch (Exception ex) {
				terminate();
			}
			
		} catch (Exception e) {
			LOG.log(Level.SEVERE,"Error stopping Fuji", e);
		}
	}

	public String getRuntimeClass() {
		// TODO Auto-generated method stub
		return "";
	}

	public void setProcess(final IProcess newProcess) {
		if (process != null)
			return;

		process = newProcess;
		if (processListener != null)
			DebugPlugin.getDefault().removeDebugEventListener(processListener);
		if (newProcess == null)
			return;
		
		processListener = new IDebugEventSetListener() {
			public void handleDebugEvents(DebugEvent[] events) {
				if (events != null) {
					int size = events.length;
					for (int i = 0; i < size; i++) {
						if (process != null && process.equals(events[i].getSource()) && events[i].getKind() == DebugEvent.TERMINATE) {
							DebugPlugin.getDefault().removeDebugEventListener(this);
							stopImpl();
						}
					}
				}
			}
		};
		DebugPlugin.getDefault().addDebugEventListener(processListener);
		
		try {
			if ( ioPing != null ) {
				ioPing.stop();
			}
			ioPing = new FujiPing(getFujiRuntime(), process, 25);
			if ( Util.OSGI_EQUINOX.equals(osgiPlatformType)) {
				ioPing.setCliCmd("ss");
			}
		} catch(Exception ex) {
			LOG.log(Level.INFO, ex.getMessage(), ex); //TODO change to fine.
		}
	}

	public void setupLaunch(ILaunch launch, String launchMode,
			IProgressMonitor monitor) throws CoreException {
		if ("true".equals(launch.getLaunchConfiguration().getAttribute(LAUNCH_ATTR_STOP, "false"))) {
			return;
		}
		
		IStatus status = getFujiRuntime().validate();
		if (status != null && status.getSeverity() == IStatus.ERROR) {
			throw new CoreException(status);	
		}
		
		//TODO: collect any used ports and throw  Ports in use error.
		
		setServerRestartState(false);
		setServerState(IServer.STATE_STARTING);
		setMode(launchMode);	
		
		// ping server to check for startup
		try {
			ping = new StartUpThread(getServer(), this, -1);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Can't ping for Fuji startup.");
		}		
	}
	
	@Override
	public void setupLaunchConfiguration(
			ILaunchConfigurationWorkingCopy workingCopy,
			IProgressMonitor monitor) throws CoreException {
		FujiRuntime runtime = getFujiRuntime();
		String adminPort = runtime.getAdminPort();
		String adminPortArg = "-Dcom.sun.jbi.fuji.admin.jmx.connectorPort=" + adminPort;
		String installRoot = runtime.getRuntime().getLocation().toOSString();
		installRoot = installRoot.replace("\\", "/");
		String cwd = installRoot;
		
		osgiPlatformType = Util.getOSGiPlatformType(installRoot);
		
		String runJarArg = "-jar bin/felix.jar";	
		String programArgs = "";
		if ( Util.OSGI_EQUINOX.equals(osgiPlatformType)) {
			runJarArg = "-jar equinox.jar";
			programArgs = "-console";
		}
		String vmArgs = adminPortArg + " " + runJarArg;
		// update working directory
		workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_WORKING_DIRECTORY, cwd);
		// update program args
		workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, programArgs);
		// update vm args
		workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, vmArgs);	
		// setup jre path
		IVMInstall vmInstall = runtime.getVMInstall();
		if (vmInstall != null) {
			workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_JRE_CONTAINER_PATH, JavaRuntime.newJREContainerPath(vmInstall).toPortableString());
		}
		// update classpath
		ArrayList list = new ArrayList();
		workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_CLASSPATH, list);
		workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_DEFAULT_CLASSPATH, false);	
	}
	
	public FujiServer getFujiServer()  {
		FujiServer fujiServer = null;
		IServer server = getServer();
		if ( server != null ) {
			fujiServer = (FujiServer)server.loadAdapter(FujiServer.class, null);
		}
		return fujiServer;
	}
	
	public FujiRuntime getFujiRuntime() {
		FujiRuntime fujiRuntime = null;
		IServer server = getServer();
		IRuntime runtime = null;
		if ( server != null) {
			runtime = server.getRuntime();
		}
		if ( runtime != null) {
			fujiRuntime = (FujiRuntime)runtime.loadAdapter(FujiRuntime.class, null);
		}
		return fujiRuntime;
	}
	
	protected void stopImpl() {
		if (ping != null) {
			ping.stop();
			ping = null;
		}
		if ( ioPing != null ) {
			ioPing.stop();
			ioPing = null;
		}
		if (process != null) {
			process = null;
			DebugPlugin.getDefault().removeDebugEventListener(processListener);
			processListener = null;
		}
		setServerState(IServer.STATE_STOPPED);
	}
	
	protected void setServerStarted() {
		setServerState(IServer.STATE_STARTED);
	}
	
	/**
	 * Terminates the server.
	 */
	protected void terminate() {
		if (getServer().getServerState() == IServer.STATE_STOPPED)
			return;

		try {
			setServerState(IServer.STATE_STOPPING);
			LOG.info("Killing the Tomcat process"); //TODO change to fine.
			if (process != null && !process.isTerminated()) {
				process.terminate();
				stopImpl();
			}
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error killing the process", e);
		}
	}	
	
}
