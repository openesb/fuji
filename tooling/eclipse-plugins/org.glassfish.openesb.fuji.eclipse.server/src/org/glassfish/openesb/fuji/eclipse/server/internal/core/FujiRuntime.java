/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.IVMInstallType;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.wst.server.core.model.RuntimeDelegate;

public class FujiRuntime extends RuntimeDelegate implements IFujiRuntime, IFujiRuntimeWorkingCopy {
	public static final String ID = "org.glassfish.openesb.fuji.eclipse.server.runtime";
	protected static final String PROP_VM_INSTALL_TYPE_ID = "vm-install-type-id";
	protected static final String PROP_VM_INSTALL_ID = "vm-install-id";
	protected static final String PROP_ADMIN_PORT = "adminPort";
	
	public FujiRuntime() {
		super();
	}

	protected String getVMInstallTypeId() {
		return getAttribute(PROP_VM_INSTALL_TYPE_ID, (String)null);
	}

	protected String getVMInstallId() {
		return getAttribute(PROP_VM_INSTALL_ID, (String)null);
	}
	
	public List<IRuntimeClasspathEntry> getRuntimeClasspath() {
		// IRuntimeClasspathEntry entry = null;
		return new ArrayList<IRuntimeClasspathEntry>();
	}

	public boolean isUsingDefaultJRE() {
		return getVMInstallTypeId() == null;
	}

	public IVMInstall getVMInstall() {
		if (getVMInstallTypeId() == null)
			return JavaRuntime.getDefaultVMInstall();
		try {
			IVMInstallType vmInstallType = JavaRuntime.getVMInstallType(getVMInstallTypeId());
			IVMInstall[] vmInstalls = vmInstallType.getVMInstalls();
			int size = vmInstalls.length;
			String id = getVMInstallId();
			for (int i = 0; i < size; i++) {
				if (id.equals(vmInstalls[i].getId()))
					return vmInstalls[i];
			}
		} catch (Exception e) {
			// ignore
		}
		return null;
	}

	public void setVMInstall(IVMInstall vmInstall) {
		if ( vmInstall == null) {
			setVMInstall(null, null);
		} else {
			setVMInstall(vmInstall.getVMInstallType().getId(), vmInstall.getId());
		}
		 
	}
	
	protected void setVMInstall(String typeId, String id) {
			setAttribute(PROP_VM_INSTALL_TYPE_ID, typeId);
			setAttribute(PROP_VM_INSTALL_ID, id);
	}

	public String getAdminPort() {
		return getAttribute(PROP_ADMIN_PORT, "8699");
	}

	public void setAdminPort(String port) {
		setAttribute(PROP_ADMIN_PORT, port);
	}
	
}
