/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.ui;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.wst.server.core.IRuntimeWorkingCopy;
import org.eclipse.wst.server.core.TaskModel;
import org.eclipse.wst.server.ui.wizard.IWizardHandle;
import org.eclipse.wst.server.ui.wizard.WizardFragment;
import org.glassfish.openesb.fuji.eclipse.server.FujiServerPlugin;
import org.glassfish.openesb.fuji.eclipse.server.internal.core.Util;

public class NewFujiRuntimeWizardFragment  extends WizardFragment {
	private NewFujiRuntimePanel panel;
	
	public NewFujiRuntimeWizardFragment() {
		super();
	}

	@Override
	public Composite createComposite(Composite parent, IWizardHandle handle) {
		panel = new NewFujiRuntimePanel(parent, handle);
		return panel;
	}

	@Override
	public boolean hasComposite() {
		return true;
	}

	@Override
	public boolean isComplete() {
		IRuntimeWorkingCopy runtime = (IRuntimeWorkingCopy) getTaskModel().getObject(TaskModel.TASK_RUNTIME);
		
		if (runtime == null)
			return false;
		IStatus status = runtime.validate(null);
		if ( status == null || status.isOK()) {
			// check if the root is a valid fuji root.
			IPath path = runtime.getLocation();
			if (path == null || !Util.isFujiInstallDir(path.toFile().getAbsolutePath()) ) {
				status = UIUtil.createErrorStatus("Not a valid Fuji Installation directory");
			} 
		}		
		return (status == null || status.getSeverity() != IStatus.ERROR);
	}

	@Override
	public void enter() {
		if (panel != null) {
			IRuntimeWorkingCopy runtime = (IRuntimeWorkingCopy) getTaskModel().getObject(TaskModel.TASK_RUNTIME);
			panel.setRuntime(runtime);
		}
	}

	@Override
	public void exit() {
		IRuntimeWorkingCopy runtime = (IRuntimeWorkingCopy) getTaskModel().getObject(TaskModel.TASK_RUNTIME);
		IPath path = runtime.getLocation();
		if (runtime.validate(null).getSeverity() != IStatus.ERROR) {
			FujiServerPlugin.setPreference("location" + runtime.getRuntimeType().getId(), path.toString());
		}
	}

}
