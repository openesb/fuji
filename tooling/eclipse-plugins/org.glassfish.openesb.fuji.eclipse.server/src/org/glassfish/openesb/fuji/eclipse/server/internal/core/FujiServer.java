/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.net.URL;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.wst.server.core.IModule;
import org.eclipse.wst.server.core.model.ServerDelegate;
import org.glassfish.openesb.fuji.eclipse.server.FujiServerPlugin;

public class FujiServer  extends ServerDelegate implements IFujiServer, IFujiServerWorkingCopy {
	
	public static final String ID = "org.glassfish.openesb.fuji.eclipse.server.server";

	public static final String PROP_PORT = "port";
	
	/**
	 * FujiServer.
	 */
	public FujiServer() {
		super();
	}	
	/**
	 * Returns true if the given project is supported by this server, and false
	 * otherwise.
	 */
	@Override
	public IStatus canModifyModules(IModule[] add, IModule[] remove) {
		return new Status(IStatus.OK, FujiServerPlugin.PLUGIN_ID, 0, "JBI Modules can be modified", null);
	}
	/*
	 * Returns the child module(s) of this module.
	 */
	@Override
	public IModule[] getChildModules(IModule[] module) {
		return new IModule[0];
	}
	/*
	 * Returns the root module(s) of this module.
	 */
	@Override
	public IModule[] getRootModules(IModule module) throws CoreException {
		return new IModule[] { module };
	}

	@Override
	public void modifyModules(IModule[] add, IModule[] remove,
			IProgressMonitor monitor) throws CoreException {
		// NOOP
	}
	
	public String toString() {
		return "Fuji Server";
	}
	
	public URL getModuleRootURL(IModule module) {
		if ( module == null ) {
			return null;
		}
		// return module.getProject().getLocationURI().toURL();
		return null;  //TODO find the file url for the module to install
	}

}
