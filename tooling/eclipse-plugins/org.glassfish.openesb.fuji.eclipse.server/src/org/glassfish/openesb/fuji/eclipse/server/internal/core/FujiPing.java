/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

import org.eclipse.debug.core.model.IProcess;
import org.eclipse.wst.server.core.IServer;
/**
 * Thread used to ping server to test when it is started.
 */
public class FujiPing {
	private static final Logger LOG = Logger.getLogger(FujiPing.class.getName());
	// delay before pinging starts
	private static final int PING_DELAY = 2000;

	// delay between pings
	private static final int PING_INTERVAL = 2500;

	// maximum number of pings before giving up
	private int maxPings;

	private boolean stop = false;
	private IProcess process;
	private IFujiRuntime runtime;
	
	private String adminPort = "8699";
	
	private String cliCmd = "ps";
	/**
	 * Create a new PingThread.
	 * 
	 * @param server
	 * @param url
	 * @param maxPings the maximum number of times to try pinging, or -1 to continue forever
	 * @param behaviour
	 */
	public FujiPing(IFujiRuntime runtime, IProcess process, int maxPings) {
		super();
		this.maxPings = maxPings;
		this.process = process;
		this.runtime = runtime;
		this.adminPort = this.runtime.getAdminPort();
		Thread t = new Thread("Fuji IO Ping Thread") {
			public void run() {
				ping();
			}
		};
		t.setDaemon(true);
		t.start();
	}
	public void setCliCmd(String cmd) {
		cliCmd = cmd;
	}
	protected void pingJMX() throws Exception {
		FujiAdmin.JMXManager jmxMgr = new FujiAdmin.JMXManager(adminPort);
		if (!jmxMgr.isJMXAdminAvailable()) {
			process.getStreamsProxy().write("\n");
			throw new Exception("Fuji Admin JMX Connection not available");
		} else {
			process.getStreamsProxy().write(cliCmd + "\n");
		}
	}
	
	/**
	 * Ping the server until it is started. Then set the server
	 * state to STATE_STARTED.
	 */
	protected void ping() {
		int count = 0;
		try {
			Thread.sleep(PING_DELAY);
		} catch (Exception e) {
			// ignore
		}
		while (!stop) {
			try {
				if (count == maxPings) {
//					try {
//						server.stop(false);
//					} catch (Exception e) {
//						LOG.info("Ping: could not stop server"); //TODO change to finest
//					}
					stop = true;
					break;
				}
				count++;
				
				LOG.info( "Ping: pinging " + count); //TODO change to finest
				pingJMX();
	
				// ping worked - server is up
				if (!stop) {
					LOG.info("Ping: success"); //TODO change to finest
					Thread.sleep(200);
//					behaviour.setServerStarted();
				}
				stop = true;
			} catch (FileNotFoundException fe) {
				try {
					Thread.sleep(200);
				} catch (Exception e) {
					// ignore
				}
//				behaviour.setServerStarted();
				stop = true;
			} catch (Exception e) {
				LOG.info("Ping: failed"); //TODO change to finest
				// pinging failed
				if (!stop) {
					try {
						Thread.sleep(PING_INTERVAL);
					} catch (InterruptedException e2) {
						// ignore
					}
				}
			}
		}
	}

	/**
	 * Tell the pinging to stop.
	 */
	public void stop() {
		LOG.info("Ping: stopping"); //TODO change to finest
		stop = true;
	}
}
