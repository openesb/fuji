/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.io.FileNotFoundException;
import java.util.logging.Logger;

import org.eclipse.wst.server.core.IServer;
/**
 * Thread used to ping server to test when it is started.
 */
public class StartUpThread {
	private static final Logger LOG = Logger.getLogger(StartUpThread.class.getName());
	// delay before pinging starts
	private static final int PING_DELAY = 2000;

	// delay between pings
	private static final int PING_INTERVAL = 250;

	// maximum number of pings before giving up
	private int maxPings;

	private boolean stop = false;
	private IServer server;
	private FujiServerBehaviour behaviour;
	
	private int fakePingCount = 0;

	/**
	 * Create a new PingThread.
	 * 
	 * @param server
	 * @param maxPings the maximum number of times to try pinging, or -1 to continue forever
	 * @param behaviour
	 */
	public StartUpThread(IServer server, FujiServerBehaviour behaviour, int maxPings) {
		super();
		this.server = server;
		this.maxPings = maxPings;
		this.behaviour = behaviour;
		Thread t = new Thread("Fuji Start Ping Thread") {
			public void run() {
				ping();
			}
		};
		t.setDaemon(true);
		t.start();
	}
	
	protected void fakePing() throws Exception {
		if ( fakePingCount < 10 ) {
			++fakePingCount;
			throw new Exception("fake server not yet started exception"); //TODO replace with real jmx connection ping.
		} else {
			fakePingCount = 0;
			return; // fake server start ping success.
		}
	}
	
	/**
	 * Ping the server until it is started. Then set the server
	 * state to STATE_STARTED.
	 */
	protected void ping() {
		int count = 0;
		try {
			Thread.sleep(PING_DELAY);
		} catch (Exception e) {
			// ignore
		}
		while (!stop) {
			try {
				if (count == maxPings) {
					try {
						server.stop(false);
					} catch (Exception e) {
						LOG.info("Ping: could not stop server"); //TODO change to finest
					}
					stop = true;
					break;
				}
				count++;
				
				LOG.info( "Ping: pinging " + count); //TODO change to finest
				fakePing();
	
				// ping worked - server is up
				if (!stop) {
					LOG.info("Ping: success"); //TODO change to finest
					Thread.sleep(200);
					behaviour.setServerStarted();
				}
				stop = true;
			} catch (FileNotFoundException fe) {
				try {
					Thread.sleep(200);
				} catch (Exception e) {
					// ignore
				}
				behaviour.setServerStarted();
				stop = true;
			} catch (Exception e) {
				LOG.info("Ping: failed"); //TODO change to finest
				// pinging failed
				if (!stop) {
					try {
						Thread.sleep(PING_INTERVAL);
					} catch (InterruptedException e2) {
						// ignore
					}
				}
			}
		}
	}

	/**
	 * Tell the pinging to stop.
	 */
	public void stop() {
		LOG.info("Ping: stopping"); //TODO change to finest
		stop = true;
	}
}
