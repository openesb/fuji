package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class FujiAdmin {
	
    private static Logger LOG = Logger.getLogger(FujiAdmin.class.getName());
    private JMXManager mJmxMgr;

    public FujiAdmin(JMXManager jmxMgr) {
        this.mJmxMgr = jmxMgr;
    }

    public FujiAdmin(String host, String port, String username, String password) {
        this.mJmxMgr = new JMXManager(host, port, username, password);
    }

    public Logger getLogger() {
        return LOG;
    }

    public String deploy(String bundlePath, boolean startOnDeploy, boolean reDeploy) throws Exception {
        getLogger().info("Deploying Bundle " + bundlePath);
        Object result = null;
        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "installBundle", bundlePath, startOnDeploy, reDeploy);
        return (String) result;
    }

    public String start(String bundlePath) throws Exception {
        //TODO: this should be based on bundle path.
        getLogger().info("Starting Bundle " + bundlePath);
        Object result = null;
        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "startBundle", bundlePath);
        return (String) result;
    }

    public String stop(String bundlePath) throws Exception {
        //TODO: this should be based on bundle path.
        getLogger().info("Stopping Bundle " + bundlePath);
        Object result = null;
        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "stopBundle", bundlePath);
        return (String) result;
    }

    public String undeploy(String bundlePath) throws Exception {
        getLogger().info("Undeploying Bundle " + bundlePath);
        Object result = null;
        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "uninstallBundle", bundlePath);
        return (String) result;
    }
    
    public void shutdown() throws Exception {
        getLogger().info("Shutting down the fuji runtime... ");
        ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
        this.mJmxMgr.invokeMBeanOperation(mbeanOName, "shutdown");
    }
    
    /**
     * JMX interface to connect to the fuji admin mbeans to perfrom the admin tasks
     * correpsonding to the admin goals in tis mojo.
     */
    public static class JMXManager {

        private static Logger LOG = Logger.getLogger(JMXManager.class.getName());
        public static final String FUJI_ADMIN_OBJECT_NAME = "con.sun.jbi.fuji:runtime=OSGi,type=admin,service=admin";
        private static ObjectName sAdminMBeanOName;
        private MBeanServerConnection mMBS;
        private JMXConnector mConnector;
        private String mHost;
        private String mPort;
        private String mUsername;
        private String mPassword;

        public JMXManager() {
            this("localhost", "8699", "", "");
        }

        public JMXManager(String port) {
            this("localhost", port, "", "");
        }

        public JMXManager(String port, String username, String password) {
            this("localhost", port, username, password);
        }

        public JMXManager(String host, String port, String username, String password) {
            this.mHost = host;
            this.mPort = port;
            this.mUsername = username;
            this.mPassword = password;
        }

        public String getHost() {
            return mHost;
        }

        public void setHost(String host) {
            this.mHost = host;
        }

        public String getPassword() {
            return mPassword;
        }

        public void setPassword(String password) {
            this.mPassword = password;
        }

        public String getPort() {
            return mPort;
        }

        public void setPort(String port) {
            this.mPort = port;
        }

        public String getUsername() {
            return mUsername;
        }

        public void setUsername(String username) {
            this.mUsername = username;
        }

        public MBeanServerConnection getMBeanServerConnection() {
            return mMBS;
        }

        protected String getURL() {

            String host = getHost();
            String port = getPort();


            if (host == null) {
                host = "localhost";
            }
            if (port == null) {
                port = "8699";
            }

            String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
            return url;
        }

        protected Map<String, Object> getEnvironment() {
            Map<String, Object> env = new HashMap<String, Object>();
            String username = getUsername();
            String password = getPassword();
            if (username == null) {
                username = "";
            }
            if (password == null) {
                password = "";
            }
            // LOG.info("Connection Credentials user:" + username + " password:" + password.length());
            env.put(JMXConnector.CREDENTIALS, new String[]{username, password});
            return env;
        }

        /**
         * JMX Agent connection
         * 
         */
        protected void connect() throws Exception {
            // Create JMX Agent URL
            String url = getURL();
            LOG.info("Conneting to FUJI Server:  JMX Connetion URL " + url);
            JMXServiceURL jmxServiceURL = new JMXServiceURL(url);
            // Connect the JMXConnector
            mConnector = JMXConnectorFactory.connect(jmxServiceURL, getEnvironment());
            // Get the MBeanServerConnection
            mMBS = mConnector.getMBeanServerConnection();
        }

        protected void close() throws Exception {

            //Close the connection
            try {
                if ( mConnector != null ) {
                    mConnector.close();
                }
            } finally {
                mConnector = null;
                mMBS = null;
            }

        }

        public static ObjectName getAdminMBeanOjectName() {

            try {
                if (sAdminMBeanOName == null) {
                    sAdminMBeanOName = new ObjectName(FUJI_ADMIN_OBJECT_NAME);
                }
            } catch (Exception ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
            return sAdminMBeanOName;
        }

        public boolean isJMXAdminAvailable() {
            boolean available = false;
            try {
                this.connect();
            } catch (Exception ex) {
                return false; // can not connect.

            }
            try {
                available = this.getMBeanServerConnection().isRegistered(JMXManager.getAdminMBeanOjectName());
            } catch (Exception ex) {
                // ignore
            } finally {
                try {
                    this.close();
                } catch (Exception ex) {
                    // ingore.
                }
            }
            return available;
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName,
                Object[] params, String[] signature) throws Exception {
            Object resultObject = null;
            this.connect();
            try {
                resultObject = getMBeanServerConnection().invoke(objName, operationName, params, signature);
            } catch (MBeanException ex) {
                Exception targetEx = ex.getTargetException();
                if (targetEx == null) {
                    targetEx = ex;
                }
                throw targetEx;
            } finally {
                try {
                    this.close();
                } catch (Exception ex) {
                    // ingore.
                }
            }
            return resultObject;
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName) throws Exception {
            Object[] params = new Object[0];
            String[] signature = new String[0];
            return invokeMBeanOperation(objName, operationName, params, signature);
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName, String param) throws Exception {
            Object[] params = {param};
            String[] signature = {"java.lang.String"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2) throws Exception {
            Object[] params = {param1, Boolean.valueOf(param2)};
            String[] signature = {"java.lang.String", "boolean"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }
        public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2, boolean param3) throws Exception {
            Object[] params = {param1, Boolean.valueOf(param2), Boolean.valueOf(param2)};
            String[] signature = {"java.lang.String", "boolean", "boolean"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }
        public Object invokeMBeanOperation(ObjectName objName, String operationName, long param) throws Exception {
            Object[] params = {Long.valueOf(param)};
            String[] signature = {"long"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }
    }
}
