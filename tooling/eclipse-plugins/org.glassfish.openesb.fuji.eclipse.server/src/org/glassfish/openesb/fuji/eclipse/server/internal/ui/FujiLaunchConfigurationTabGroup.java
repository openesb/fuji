/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.ui;

import java.util.ArrayList;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.wst.server.ui.ServerLaunchConfigurationTab;

public class FujiLaunchConfigurationTabGroup extends AbstractLaunchConfigurationTabGroup {

	/*
	 * @see ILaunchConfigurationTabGroup#createTabs(ILaunchConfigurationDialog, String)
	 */
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		
		ArrayList<ILaunchConfigurationTab> tabList = new ArrayList<ILaunchConfigurationTab>();
		
		ILaunchConfigurationTab tab = null;
		
		// server tab
		tab = new ServerLaunchConfigurationTab(new String[] { "org.glassfish.openesb.fuji.eclipse.server.server" });
		tab.setLaunchConfigurationDialog(dialog);
		tabList.add(tab);
		
		// java args tab
//		tab = new JavaArgumentsTab();
//		tab.setLaunchConfigurationDialog(dialog);
//		tabList.add(tab);
		
//		// java classpath tab
//		tab = new JavaClasspathTab();
//		tab.setLaunchConfigurationDialog(dialog);
//		tabList.add(tab);		

//		// java source tab
//		tab = new SourceLookupTab();
//		tab.setLaunchConfigurationDialog(dialog);
//		list.add(tab);	
		
		// java env tab
//		tab = new EnvironmentTab();
//		tab.setLaunchConfigurationDialog(dialog);
//		tabList.add(tab);			
		
		// java common tab
		tab = new CommonTab();
		tab.setLaunchConfigurationDialog(dialog);
		tabList.add(tab);	
		

		setTabs(tabList.toArray(new ILaunchConfigurationTab[0]));		
	}

}
