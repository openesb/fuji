/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.IVMInstallType;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wst.server.core.IRuntimeWorkingCopy;
import org.eclipse.wst.server.ui.wizard.IWizardHandle;
import org.glassfish.openesb.fuji.eclipse.server.FujiServerPlugin;
import org.glassfish.openesb.fuji.eclipse.server.Messages;
import org.glassfish.openesb.fuji.eclipse.server.internal.core.IFujiRuntimeWorkingCopy;
import org.glassfish.openesb.fuji.eclipse.server.internal.core.Util;

public class NewFujiRuntimePanel extends Composite {
	protected IRuntimeWorkingCopy runtimeWC;
	protected IFujiRuntimeWorkingCopy runtime;
	
	protected IWizardHandle wizard;
	
	protected Text installDir;
	protected Text name;
	protected Combo combo;
	protected List<IVMInstall> installedJREs;
	protected String[] jreNames;
	
	protected Text adminPort;
	/**
	 * NewFujiRuntimeComposite constructor comment.
	 * 
	 * @param parent the parent composite
	 * @param wizard the wizard handle
	 */
	protected NewFujiRuntimePanel(Composite parent, IWizardHandle wizard) {
		super(parent, SWT.NONE);
		this.wizard = wizard;
		
		wizard.setTitle(Messages.wizardTitle);
		wizard.setDescription(Messages.wizardDescription);
		wizard.setImageDescriptor(FujiServerPlugin.getImageDescriptor("icons/fuji-wiz.png"));
		
		createControl();
	}
	protected void setRuntime(IRuntimeWorkingCopy newRuntime) {
		runtimeWC = newRuntime;
		runtime = null;
		if ( newRuntime != null ) {
			runtime = (IFujiRuntimeWorkingCopy) newRuntime.loadAdapter(IFujiRuntimeWorkingCopy.class, null);
		}
		init();
		validate();
	}	
	/**
	 * Provide a wizard page to change the Fuji installation directory.
	 */
	protected void createControl() {
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		setLayout(layout);
		setLayoutData(new GridData(GridData.FILL_BOTH));
		// PlatformUI.getWorkbench().getHelpSystem().setHelp(this, ContextIds.RUNTIME_COMPOSITE); //TODO
		
		Label label = new Label(this, SWT.NONE);
		label.setText(Messages.runtimeName);
		GridData data = new GridData();
		data.horizontalSpan = 2;
		label.setLayoutData(data);
		
		name = new Text(this, SWT.BORDER);
		data = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(data);
		name.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				runtimeWC.setName(name.getText());
				validate();
			}
		});
	
		label = new Label(this, SWT.NONE);
		label.setText(Messages.installDir);
		data = new GridData();
		data.horizontalSpan = 2;
		label.setLayoutData(data);
	
		installDir = new Text(this, SWT.BORDER);
		data = new GridData(GridData.FILL_HORIZONTAL);
		installDir.setLayoutData(data);
		installDir.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				runtimeWC.setLocation(new Path(installDir.getText()));
				validate();
			}
		});
		
		Button browse = UIUtil.createButton(this, Messages.browse);
		browse.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent se) {
				DirectoryDialog dialog = new DirectoryDialog(NewFujiRuntimePanel.this.getShell());
				dialog.setMessage(Messages.selectInstallDir);
				dialog.setFilterPath(installDir.getText());
				String selectedDirectory = dialog.open();
				if (selectedDirectory != null)
					installDir.setText(selectedDirectory);
			}
		});
		
		label = new Label(this, SWT.NONE);
		label.setText(Messages.adminPort);
		data = new GridData();
		data.horizontalSpan = 2;
		label.setLayoutData(data);
		
		adminPort = new Text(this, SWT.BORDER);
		data = new GridData(GridData.FILL_HORIZONTAL);
		adminPort.setLayoutData(data);
		adminPort.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				runtime.setAdminPort(adminPort.getText());
				validate();
			}
		});		
		
//		final IInstallableRuntime ir = ServerPlugin.findInstallableRuntime("org.glassfish.openesb.fuji.eclipse.server.runtime");
//		if (ir != null) {
//			label = new Label(this, SWT.NONE);
//			
//			Button install = SWTUtil.createButton(this, Messages.install);
//			install.addSelectionListener(new SelectionAdapter() {
//				public void widgetSelected(SelectionEvent se) {
//					DirectoryDialog dialog = new DirectoryDialog(NewFujiRuntimeComposite.this.getShell());
//					dialog.setMessage(Messages.selectInstallDir);
//					dialog.setFilterPath(installDir.getText());
//					String selectedDirectory = dialog.open();
//					if (selectedDirectory != null) {
//						ir.install(new Path(selectedDirectory));
//						installDir.setText(selectedDirectory);
//					}
//				}
//			});
//		}
		
		updateJREs();
		
		// JDK location
		label = new Label(this, SWT.NONE);
		label.setText(Messages.installedJRE);
		data = new GridData();
		data.horizontalSpan = 2;
		label.setLayoutData(data);
		
		combo = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setItems(jreNames);
		data = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		combo.setLayoutData(data);
		
		combo.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				int sel = combo.getSelectionIndex();
				IVMInstall vmInstall = null;
				if (sel > 0)
					vmInstall = (IVMInstall) installedJREs.get(sel - 1);
				
			runtime.setVMInstall(vmInstall);
				validate();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		
		Button button = UIUtil.createButton(this, Messages.installedJREs);
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				String currentVM = combo.getText();
				if (showPreferencePage()) {
					updateJREs();
					combo.setItems(jreNames);
					combo.setText(currentVM);
					if (combo.getSelectionIndex() == -1)
						combo.select(0);
					validate();
				}
			}
		});
		
		init();
		validate();
		
		Dialog.applyDialogFont(this);
		
		name.forceFocus();		
	}
	
	protected void updateJREs() {
		// get all installed JVMs
		installedJREs = new ArrayList<IVMInstall>();
		IVMInstallType[] vmInstallTypes = JavaRuntime.getVMInstallTypes();
		int size = vmInstallTypes.length;
		for (int i = 0; i < size; i++) {
			IVMInstall[] vmInstalls = vmInstallTypes[i].getVMInstalls();
			int size2 = vmInstalls.length;
			for (int j = 0; j < size2; j++) {
				installedJREs.add(vmInstalls[j]);
			}
		}
		
		// get names
		size = installedJREs.size();
		jreNames = new String[size+1];
		jreNames[0] = Messages.runtimeDefaultJRE;
		for (int i = 0; i < size; i++) {
			IVMInstall vmInstall = (IVMInstall) installedJREs.get(i);
			jreNames[i+1] = vmInstall.getName();
		}
	}

	protected boolean showPreferencePage() {
		String id = "org.eclipse.jdt.debug.ui.preferences.VMPreferencePage";
		
		// should be using the following API, but it only allows a single preference page instance.
		// see bug 168211 for details
		//PreferenceDialog dialog = PreferencesUtil.createPreferenceDialogOn(getShell(), id, new String[] { id }, null);
		//return (dialog.open() == Window.OK);		
		
		PreferenceManager manager = PlatformUI.getWorkbench().getPreferenceManager();
		IPreferenceNode node = manager.find("org.eclipse.jdt.ui.preferences.JavaBasePreferencePage").findSubNode(id);
		PreferenceManager manager2 = new PreferenceManager();
		manager2.addToRoot(node);
		PreferenceDialog dialog = new PreferenceDialog(getShell(), manager2);
		dialog.create();
		return (dialog.open() == Window.OK);
	}

	protected void init() {
		if (name == null || runtime == null) {
			return;
		}
		
		if (runtimeWC.getName() != null) {
			name.setText(runtimeWC.getName());
		} else {
			name.setText("");
		}
		
		if (runtimeWC.getLocation() != null) {
			installDir.setText(runtimeWC.getLocation().toOSString());
		} else {
			installDir.setText("");
		}
		
		adminPort.setText(runtime.getAdminPort());
		
		// set selection
		if (runtime.isUsingDefaultJRE()) {
			combo.select(0);
		} else {
			boolean found = false;
			int size = installedJREs.size();
			for (int i = 0; i < size; i++) {
				IVMInstall vmInstall = (IVMInstall) installedJREs.get(i);
				if (vmInstall.equals(runtime.getVMInstall())) {
					combo.select(i + 1);
					found = true;
				}
			}
			if (!found) {
				combo.select(0);
			}
		}
	}

	protected void validate() {
		if (runtime == null) {
			wizard.setMessage("", IMessageProvider.ERROR);
			return;
		}
		
		IStatus status = runtimeWC.validate(null);
		if ( status == null || status.isOK()) {
			// check if the root is a valid fuji root.
			IPath path = runtimeWC.getLocation();
			if (path == null || !Util.isFujiInstallDir(path.toFile().getAbsolutePath()) ) {
				status = UIUtil.createErrorStatus("Not a valid Fuji Installation directory");
			} 
		}
		if (status == null || status.isOK()){
			wizard.setMessage(null, IMessageProvider.NONE);
		} else if (status.getSeverity() == IStatus.WARNING) {
			wizard.setMessage(status.getMessage(), IMessageProvider.WARNING);
		} else {
			wizard.setMessage(status.getMessage(), IMessageProvider.ERROR);
		}
		wizard.update();
	}
	
}
