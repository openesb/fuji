/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.fuji.eclipse.server.internal.core;

import java.util.List;

import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jst.server.core.IJavaRuntime;

public interface IFujiRuntime  extends IJavaRuntime {
	/**
	 * Returns <code>true</code> if this server is using the default JRE, and
	 * <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if this server is using the default JRE, and
	 *    <code>false</code> otherwise
	 */
	public boolean isUsingDefaultJRE();

	/**
	 * Returns the runtime classpath that is used by this runtime.
	 * 
	 * @return the runtime classpath
	 */
	public List<IRuntimeClasspathEntry> getRuntimeClasspath();
	/**
	 * admin jmx port
	 * @return
	 */
	public String getAdminPort();
}
