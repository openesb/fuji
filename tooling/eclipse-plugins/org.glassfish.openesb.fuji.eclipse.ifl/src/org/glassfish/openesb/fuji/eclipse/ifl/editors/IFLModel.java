/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.eclipse.ifl.editors;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
/**
 * 
 * @author chikkala
 *
 */
public class IFLModel {
	public static IFLModel getModel(IDocument document, IProgressMonitor monitor) {
		return new IFLModel();
	}
	public void getContentProposals(String prefix, String indent, int offset, List<ICompletionProposal> result) {
		for (String keyword : IFLScanner.KEYWORDS ) {
			result.add(new IFLCompletionProposal(keyword));
		}
	}	
	
	public static class IFLCompletionProposal implements ICompletionProposal {
		private String keyword;
		public IFLCompletionProposal(String keyword) {
			this.keyword = keyword;
		}
		public void apply(IDocument doc) {
			// TODO Auto-generated method stub
			
		}

		public String getAdditionalProposalInfo() {
			// TODO Auto-generated method stub
			return null;
		}

		public IContextInformation getContextInformation() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getDisplayString() {
			// TODO Auto-generated method stub
			return this.keyword;
		}

		public Image getImage() {
			// TODO Auto-generated method stub
			return null;
		}

		public Point getSelection(IDocument doc) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
}
