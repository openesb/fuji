/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.eclipse.ifl.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;
/**
 * 
 * @author chikkala
 *
 */
public class IFLScanner extends RuleBasedScanner {

	public final static String KEYWORDS[] = new String[] { 
		"do", 
		"end",
		"to",
		"from",
		"route", 
		"broadcast", 
		"split", 
		"aggregate", 
		"filter", 
		"tee",
		"select",
		"when",
		"else"
		};

	public IFLScanner(ColorManager colorMgr) {

		List<IRule> ruleList = new ArrayList<IRule>();

		// keywords rule
		WordRule wordRule = new WordRule(new IWordDetector() {
			public boolean isWordStart(char c) {
				return Character.isJavaIdentifierStart(c);
			}

			public boolean isWordPart(char c) {
				return Character.isJavaIdentifierPart(c);
			}
		});
		for (String keyword : KEYWORDS ) {
			wordRule.addWord(keyword,
					new Token(new TextAttribute(colorMgr.getColor(IFLColorConstants.KEYWORD), null, SWT.BOLD))
					);
		}
		ruleList.add(wordRule);
		
		// string rule
		ruleList.add(new SingleLineRule("\"", "\"",
				new Token(new TextAttribute(colorMgr
						.getColor(IFLColorConstants.STRING))), '\\'));
		// comments rule
		ruleList.add(new EndOfLineRule("#", new Token(new TextAttribute(
				colorMgr.getColor(IFLColorConstants.COMMENT)))));
		ruleList.add(new EndOfLineRule("//", new Token(new TextAttribute(
				colorMgr.getColor(IFLColorConstants.COMMENT)))));
		// whitespace rule
		ruleList.add(new WhitespaceRule(new IFLWhitespaceDetector()));

		setRules(ruleList.toArray(new IRule[0]));
		
	}
}
