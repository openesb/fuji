/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.eclipse.ifl.editors;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;

/**
 * 
 * @author chikkala
 *
 */
public class IFLCompletionProcessor implements IContentAssistProcessor {
	static Logger LOG = Logger.getLogger(IFLCompletionProcessor.class.getName());
	private final IContextInformation[] NO_CONTEXTS = new IContextInformation[0];
	private final char[] PROPOSAL_ACTIVATION_CHARS = new char[] { 'd', 'e',	'r', 'b', 's', 'a', 'f', 't'};
	private ICompletionProposal[] NO_COMPLETIONS = new ICompletionProposal[0];

	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer,
			int offset) {
		LOG.info("IFL Editor Compute CompletionProposal called");
		try {
			IDocument document = viewer.getDocument();
			String prefix = lastWord(document, offset);
			String indent = lastIndent(document, offset);
			IFLModel model = IFLModel.getModel(document, null);
			List<ICompletionProposal> result = new ArrayList<ICompletionProposal>();
			model.getContentProposals(prefix, indent, offset, result);
			return (ICompletionProposal[]) result.toArray(new ICompletionProposal[result.size()]);
		} catch (Throwable e) {
			// e.printStackTrace();
			return NO_COMPLETIONS;
		}
	}

	private String lastWord(IDocument doc, int offset) {
		try {
			for (int n = offset - 1; n >= 0; n--)
				if (!Character.isJavaIdentifierPart(doc.getChar(n)))
					return doc.get(n + 1, offset - n - 1);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return "";
	}

	private String lastIndent(IDocument doc, int offset) {
		try {
			int start = offset - 1;
			while (start >= 0 && doc.getChar(start) != '\n')
				start--;
			int end = start;
			while (end < offset && Character.isSpaceChar(doc.getChar(end)))
				end++;
			return doc.get(start + 1, end - start - 1);
		} catch (BadLocationException e) {
			// e.printStackTrace();
		}
		return "";
	}

	public IContextInformation[] computeContextInformation(ITextViewer viewer,
			int offset) {
		return NO_CONTEXTS;
	}

	public char[] getCompletionProposalAutoActivationCharacters() {
		return PROPOSAL_ACTIVATION_CHARS;
	}

	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}

	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

	public String getErrorMessage() {
		return null;
	}

}
