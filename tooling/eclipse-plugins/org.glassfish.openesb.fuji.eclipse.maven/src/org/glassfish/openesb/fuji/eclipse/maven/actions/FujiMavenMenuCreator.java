/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.eclipse.maven.actions;

import org.eclipse.jface.action.IMenuManager;
import org.maven.ide.eclipse.actions.AbstractMavenMenuCreator;
import org.maven.ide.eclipse.actions.SelectionUtil;
/**
 * 
 * @author chikkala
 *
 */
public class FujiMavenMenuCreator extends AbstractMavenMenuCreator {

	@Override
	public void createMenu(IMenuManager menuMgr) {
		// TODO Auto-generated method stub
		// menuMgr.
	    int selectionType = SelectionUtil.getSelectionType(selection);
	    if(selectionType == SelectionUtil.UNSUPPORTED) {
	      return;
	    }

//	    if(selectionType == SelectionUtil.PROJECT_WITH_NATURE) {
//	    	ExecutePomAction action = 
//	      menuMgr.appendToGroup(OPEN, getAction(new FooAction(), FooAction.ID, "Foo"));
//	    }

	}	

}
