package org.glassfish.openesb.fuji.eclipse.maven.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.debug.ui.RefreshTab;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.glassfish.openesb.fuji.eclipse.maven.FujiMavenPlugin;
import org.maven.ide.eclipse.MavenPlugin;
import org.maven.ide.eclipse.core.IMavenConstants;
import org.maven.ide.eclipse.project.IMavenProjectFacade;
import org.maven.ide.eclipse.project.MavenProjectManager;
import org.maven.ide.eclipse.project.ResolverConfiguration;


public class FujiExecutePomAction implements ILaunchShortcut,
		IExecutableExtension {

	private String mGoal;

	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		this.mGoal = (String) data;
	}

	public void launch(IEditorPart editor, String mode) {
		IEditorInput editorInput = editor.getEditorInput();
		if (editorInput instanceof IFileEditorInput) {
			launch(((IFileEditorInput) editorInput).getFile().getParent(), mode);
		}
	}

	public void launch(ISelection selection, String mode) {
		if (!(selection instanceof StructuredSelection)) {
			return;
		}
		IStructuredSelection structuredSelection = (IStructuredSelection) selection;
		Object object = structuredSelection.getFirstElement();

		IContainer basedir = null;

		if (object instanceof IProject || object instanceof IFolder) {
			basedir = (IContainer) object;
		} else if (object instanceof IFile) {
			basedir = ((IFile) object).getParent();
		} else if (object instanceof IAdaptable) {
			IAdaptable adaptable = (IAdaptable) object;
			Object adapter = adaptable.getAdapter(IProject.class);
			if (adapter != null) {
				basedir = (IContainer) adapter;
			} else {
				adapter = adaptable.getAdapter(IFolder.class);
				if (adapter != null) {
					basedir = (IContainer) adapter;
				} else {
					adapter = adaptable.getAdapter(IFile.class);
					if (adapter != null) {
						basedir = ((IFile) object).getParent();
					}
				}
			}
		}

		launch(basedir, mode);
	}

	@SuppressWarnings("deprecation")
	private void launch(IContainer basecon, String mode) {
		if (basecon == null) {
			return;
		}

		IContainer basedir = findPomXmlBasedir(basecon);

		ILaunchConfiguration launchConfiguration = getLaunchConfiguration(
				basedir, mode);
		if (launchConfiguration == null) {
			return;
		}

		DebugUITools.launch(launchConfiguration, mode);

	}

	private IContainer findPomXmlBasedir(IContainer dir) {
		if (dir == null) {
			return null;
		}

		try {
			// loop upwards through the parents as long as we do not cross the
			// project boundary
			while (dir.exists() && dir.getProject() != null
					&& dir.getProject() != dir) {
				// see if pom.xml exists
				if (dir.getType() == IResource.FOLDER) {
					IFolder folder = (IFolder) dir;
					if (folder.findMember(IMavenConstants.POM_FILE_NAME) != null) {
						return folder;
					}
				} else if (dir.getType() == IResource.FILE) {
					if (((IFile) dir).getName().equals(
							IMavenConstants.POM_FILE_NAME)) {
						return dir.getParent();
					}
				}
				dir = dir.getParent();
			}
		} catch (Exception e) {
			return dir;
		}
		return dir;
	}

	private ILaunchConfiguration getLaunchConfiguration(IContainer basedir,
			String mode) {
		if (this.mGoal != null) {
			return createLaunchConfiguration(basedir, this.mGoal);
		} else {
			FujiMavenPlugin.getDefault().logError(
					"No Goal specified to execute fuji project pom");
			return null;
		}
	}

	private ILaunchConfiguration createLaunchConfiguration(IContainer basedir,
			String goal) {
		try {
			ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
			
			ILaunchConfigurationType launchConfigurationType = launchManager
					.getLaunchConfigurationType(MavenLaunchConstants.LAUNCH_CONFIGURATION_TYPE_ID);

			ILaunchConfigurationWorkingCopy workingCopy = launchConfigurationType
					.newInstance(null, //
							"Executing " + goal + " in "
									+ basedir.getLocation());
			workingCopy.setAttribute(MavenLaunchConstants.ATTR_POM_DIR, basedir
					.getLocation().toOSString());
			workingCopy.setAttribute(MavenLaunchConstants.ATTR_GOALS, goal);

			workingCopy.setAttribute(RefreshTab.ATTR_REFRESH_SCOPE,	"${project}");
			workingCopy.setAttribute(RefreshTab.ATTR_REFRESH_RECURSIVE, true);

			setProjectConfiguration(workingCopy, basedir);

			IPath path = getJREContainerPath(basedir);
			if (path != null) {
				workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_JRE_CONTAINER_PATH,
								path.toPortableString());
			}

			// TODO when launching Maven with debugger consider to add the
			// following property
			// -Dmaven.surefire.debug="-Xdebug
			// -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000
			// -Xnoagent -Djava.compiler=NONE"

			return workingCopy;
		} catch (CoreException ex) {
			FujiMavenPlugin.getDefault().logError(ex);
		}
		return null;
	}
	
	  private void setProjectConfiguration(
			ILaunchConfigurationWorkingCopy workingCopy, IContainer basedir) {
		MavenProjectManager projectManager = MavenPlugin.getDefault()
				.getMavenProjectManager();
		IFile pomFile = basedir
				.getFile(new Path(IMavenConstants.POM_FILE_NAME));
		IMavenProjectFacade projectFacade = projectManager.create(pomFile,
				false, new NullProgressMonitor());
		if (projectFacade != null) {
			ResolverConfiguration configuration = projectFacade
					.getResolverConfiguration();

			String activeProfiles = configuration.getActiveProfiles();
			if (activeProfiles != null && activeProfiles.length() > 0) {
				workingCopy.setAttribute(MavenLaunchConstants.ATTR_PROFILES,
						activeProfiles);
			}

			if (configuration.shouldResolveWorkspaceProjects()) {
				workingCopy.setAttribute(
						MavenLaunchConstants.ATTR_WORKSPACE_RESOLUTION, true);
			}
		}
	}

	// TODO ideally it should use MavenProject, but it is faster to scan IJavaProjects 
	private IPath getJREContainerPath(IContainer basedir) throws CoreException {
		IProject project = basedir.getProject();
		if (project != null && project.hasNature(JavaCore.NATURE_ID)) {
			IJavaProject javaProject = JavaCore.create(project);
			IClasspathEntry[] entries = javaProject.getRawClasspath();
			for (int i = 0; i < entries.length; i++) {
				IClasspathEntry entry = entries[i];
				if (JavaRuntime.JRE_CONTAINER
						.equals(entry.getPath().segment(0))) {
					return entry.getPath();
				}
			}
		}
		return null;
	}
		  
}
