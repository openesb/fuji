/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.fuji.eclipse.maven;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author chikkala
 */
public class FujiMavenPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.glassfish.openesb.fuji.eclipse.maven";

	// The shared instance
	private static FujiMavenPlugin plugin;

	/**
	 * The constructor
	 */
	public FujiMavenPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static FujiMavenPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	private void updateFujiMavenBuildLaunchers() {
		// DebugPlugin.
	}

	public void logInfo(Object logEntry) {
		log(logEntry, IStatus.INFO);
	}
	
	public void logWarning(Object logEntry) {
		log(logEntry, IStatus.WARNING);
	}
	
	public void logError(Object logEntry) {
		log(logEntry, IStatus.ERROR);
	}
	
	public void log(Object logEntry, int level) {
		IStatus status;
		if (logEntry instanceof IStatus) {
			status = (IStatus) logEntry;
			getLog().log(status);
		} else {
			if (logEntry == null) {
				logEntry = new RuntimeException("NullLogEntry_exception")
						.fillInStackTrace();
			}

			if (logEntry instanceof Throwable) {
				Throwable throwable = (Throwable) logEntry;

				// System.err.println("Logged throwable: --------------------");
				// throwable.printStackTrace();

				String message = throwable.getLocalizedMessage();
				if (message == null) {
					message = "";
				}

				getLog().log(
						new Status(level, getBundle().getSymbolicName(), 0,
								message, throwable));
			} else {
				// System.err.println("Logged throwable: --------------------");
				// throwable.printStackTrace();

				getLog().log(
						new Status(level, getBundle().getSymbolicName(), 0,
								logEntry.toString(), null));
			}
		}
	}

}
