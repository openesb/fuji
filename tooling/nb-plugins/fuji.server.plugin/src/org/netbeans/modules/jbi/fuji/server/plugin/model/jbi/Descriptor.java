/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Descriptor.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author kcbabo
 */
public abstract class Descriptor {
    
    public static final String NS_URI =
            "http://java.sun.com/xml/ns/jbi";
    
    protected Element element_;
    
    protected Descriptor(Element element) {
        element_ = element;
        
        // Check to see if we need to create a new element structure
        if (!element.hasChildNodes() && !element.hasAttributes()) {
            initialize();
        }
    }
    
    Element getChildElement(String name) {
        Element element = null;
        NodeList nodes = element_.getElementsByTagNameNS(NS_URI, name);
        
        if (nodes.getLength() > 0) {
            element = (Element)nodes.item(0);
        }
        
        return element;
    }
    
    List<Element> getChildElements(String name) {
        ArrayList<Element> list = new ArrayList<Element>();
        NodeList nodes = element_.getElementsByTagNameNS(NS_URI, name);
        
        for (int i = 0; i < nodes.getLength(); i++) {
            list.add((Element)nodes.item(i));
        }
        
        return list;
    }
    
    public DocumentFragment getExtensionData() {
        DocumentFragment frag = 
                element_.getOwnerDocument().createDocumentFragment();
        NodeList nodes = element_.getChildNodes();
        
        // Go through the list of child nodes and pick out non-JBI elements
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE &&
                    !node.getNamespaceURI().equals(NS_URI)) {
                frag.appendChild(node);
            }
        }
        
        return frag;
    }
    
    
    protected Element createChildElement(String name) {
        return createChildElement(element_, name);
    }
    
    protected Element createChildElement(Element parent, String name) {
        Element ele = parent.getOwnerDocument().createElementNS(NS_URI, name);
        parent.appendChild(ele);
        return ele;
    }
    
    protected Element createChildElementBefore(String name, String beforeName) {
        Element ele = element_.getOwnerDocument().createElementNS(NS_URI, name);
        element_.insertBefore(ele, getChildElement(beforeName));
        return ele;
    }
    
    /**
     * This method is called when a result object is being created from scratch,
     * as opposed to starting with a pre-populated element structure.  
     * Implementing classes should create the underlying element structure when
     * this method is called.
     */
    protected abstract void initialize();
    
    public Element getElement() {
        return element_;
    }
    
    public void writeTo(OutputStream output) 
            throws Exception {
        Transformer transformer = 
                TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(element_), new StreamResult(output));
    }
    
    public String asString() throws Exception {
        StringWriter sw = new StringWriter();
        
        Transformer transformer = 
                TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(element_), new StreamResult(sw));
        
        return sw.toString();
    }
}
