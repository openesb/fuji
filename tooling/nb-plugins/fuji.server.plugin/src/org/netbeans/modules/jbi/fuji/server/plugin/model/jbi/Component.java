/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Component.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;

/**
 * Represents <code>component</code> element in jbi.xml.
 * @author kcbabo
 */
public class Component extends Descriptor {
    
    public static final String ELEMENT_NAME =
                "component";
    private static final String TYPE =
            "type";
    private static final String COMPONENT_CL_DELEGATION =
            "component-class-loader-delegation";
    private static final String BOOTSTRAP_CL_DELEGATION =
            "bootstrap-class-loader-delegation";
    private static final String COMPONENT_CLASS_NAME =
            "component-class-name";
    private static final String BOOTSTRAP_CLASS_NAME =
            "bootstrap-class-name";
    private static final String COMPONENT_CLASS_PATH =
            "component-class-path";
    private static final String BOOTSTRAP_CLASS_PATH =
            "bootstrap-class-path";
    private static final String SHARED_LIBRARY =
            "shared-library";

    public enum ComponentType {
        BINDING ("binding-component"),
        ENGINE  ("service-engine");
        private String type;
        ComponentType(String type) {
            this.type = type;
        }
        public String getType() {
            return type;
        }
    }

    public Component(Element element) {
        super(element);
    }
    
    public String getType() {
        return element_.getAttribute(TYPE);
    }
    
    public String getComponentClassLoaderDelegation() {
        return element_.getAttribute(COMPONENT_CL_DELEGATION);
    }
    
    public String getBootstrapClassLoaderDelegation() {
        return element_.getAttribute(BOOTSTRAP_CL_DELEGATION);
    }
    
    public String getBootstrapClassName() {
        String rtnString = getChildElement(BOOTSTRAP_CLASS_NAME).getTextContent();
        if (rtnString == null) {
            return null;
        }

        return rtnString.trim();
    }
    
    public String getComponentClassName() {
        String rtnString = getChildElement(COMPONENT_CLASS_NAME).getTextContent();
        if (rtnString == null) {
            return null; 
        }   
        
        return rtnString.trim();
    }
    
    public Identification getIdentification() {
        return new Identification(getChildElement(Identification.ELEMENT_NAME));
    }
    
    public ClassPath getComponentClassPath() {
        return new ClassPath(getChildElement(COMPONENT_CLASS_PATH));
    }
    
    public ClassPath getBootstrapClassPath() {
        return new ClassPath(getChildElement(BOOTSTRAP_CLASS_PATH));
    }
    
    public List<String> getSharedLibraries() {
        List<String> slList = new ArrayList<String>();
        for (Element e : getChildElements(SHARED_LIBRARY)) {
            slList.add(e.getTextContent());
        }
        
        return slList;
    }
    
    @Override
    protected void initialize() {
    }
}
