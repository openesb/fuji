/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

/**
 *
 * @author chikkala
 */
public interface OSGiConstants {

    public static final String SERVICE_DESCRIPTION = "service.description";
    public static final String SERVICE_ID = "service.id";
    public static final String SERVICE_PID = "service.pid";
    public static final String SERVICE_RANKING = "service.ranking";
    public static final String SERVICE_VENDOR = "service.vendor";
    public static final String SERVICE_BUNDLELOCATION = "service.bundleLocation";
    public static final String SERVICE_FACTORYPID = "service.factoryPid";
}
