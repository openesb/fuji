/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author chikkala
 */
public class StopServerAction extends NodeAction {
   
    protected static Logger getLogger() {
        return Logger.getLogger(StopServerAction.class.getName());
    }
    
    @Override
    protected void performAction(Node[] nodes) {
        if (nodes == null || nodes.length != 1) {
            getLogger().fine("no node is selected for stopping the fuji server");
            return;
        }
        FujiServerInstance instance = nodes[0].getLookup().lookup(FujiServerInstance.class);
        performAction(instance);
    }
    
    protected static void performAction(FujiServerInstance instance) {
        if ( instance == null ) {
            getLogger().fine("fuji server instance is NULL");
            return; 
        }
        if (!instance.isStarted() ) {
            getLogger().fine("fuji server instance " + instance.getName() +" not started");
            return;
        }
        //TODO: stop the server.
        getLogger().info("Stopping the server instance " + instance.getName());
        try {
            instance.stop();
        } catch (Exception ex) {
            UiUtils.showMessage(ex);
        }
    }
    
    @Override
    protected boolean enable(Node[] nodes) {
        if (nodes != null && nodes.length == 1) {
            FujiServerInstance instance = nodes[0].getLookup().lookup(FujiServerInstance.class);
            return ((instance != null) && (instance.isStarted()));
        }
        return true;
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(StopServerAction.class, "LBL_StopServerAction");
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(StopServerAction.class);
    }
    
    @Override
    protected boolean asynchronous() {
        return false;
    }

    /** This action will be displayed in the server output window */
    public static class OutputAction extends AbstractAction implements ChangeListener {

        private static final String ICON =
            "org/netbeans/modules/jbi/fuji/server/plugin/resources/stop.png"; // NOI18N
        private static final String PROP_ENABLED = "enabled"; // NOI18N
        private final FujiServerInstance instance;

        public OutputAction(FujiServerInstance instance) {
            super(NbBundle.getMessage(StartServerAction.class, "LBL_StopServerOutputAction"),
                new ImageIcon(ImageUtilities.loadImage(ICON)));
            putValue(SHORT_DESCRIPTION, NbBundle.getMessage(StartServerAction.class, "LBL_StopServerOutputActionDesc"));
            this.instance = instance;

            // start listening to changes
            ChangeListener cl = WeakListeners.change(this, this.instance);
            this.instance.addChangeListener(cl);
        }

        public void actionPerformed(ActionEvent e) {
            performAction(instance);
        }

        @Override
        public boolean isEnabled() {
            return ((instance != null) && (instance.isStarted()));
        }
        // ServerInstance.StateListener implementation --------------------------
        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == instance) {
                Mutex.EVENT.readAccess(new Runnable() {
                    public void run() {
                        firePropertyChange(
                            PROP_ENABLED,
                            null,
                            isEnabled() ? Boolean.TRUE : Boolean.FALSE);
                    }
                });
            }
        }
    }    
    
}
