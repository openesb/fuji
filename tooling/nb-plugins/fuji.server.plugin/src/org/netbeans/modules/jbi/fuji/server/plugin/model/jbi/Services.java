/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Services.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public class Services extends Descriptor {
    
    public static final String ELEMENT_NAME =
            "services";
    
    public static final String BINDING_COMPONENT = 
            "binding-component";
    
    public Services(Element element) {
        super(element);
    }
    
    public boolean isBindingComponent() {
        return Boolean.valueOf(element_.getAttribute(BINDING_COMPONENT));
    }
    
    public void setBindingComponent(boolean isBinding) {
        element_.setAttribute(BINDING_COMPONENT, String.valueOf(isBinding));
    }
    
    public List<Provides> getProvides() {
        List<Provides> provides = new ArrayList<Provides>();
        List<Element> eleList = getChildElements(Provides.ELEMENT_NAME);
        
        for (Element ele : eleList) {
            provides.add(new Provides(ele));
        }
        
        return provides;
    }
    
    public List<Consumes> getConsumes() {
        List<Consumes> consumes = new ArrayList<Consumes>();
        List<Element> eleList = getChildElements(Consumes.ELEMENT_NAME);
        
        for (Element ele : eleList) {
            consumes.add(new Consumes(ele));
        }
        
        return consumes;
    }
    
    public Provides addProvides() {
        return new Provides(createChildElementBefore(
                Provides.ELEMENT_NAME, Consumes.ELEMENT_NAME),
                getProvides().size());
    }
    
    public Consumes addConsumes() {
        return new Consumes(createChildElement(Consumes.ELEMENT_NAME),
                getProvides().size());
    }
    
    @Override
    protected void initialize() {
        setBindingComponent(true);
    }
}
