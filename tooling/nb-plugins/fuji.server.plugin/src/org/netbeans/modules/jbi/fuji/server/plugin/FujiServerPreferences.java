/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin;

import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.modules.jbi.fuji.server.plugin.model.AbstractFujiServerInstance;
import org.openide.util.NbPreferences;

/**
 *
 * @author chikkala
 */
public class FujiServerPreferences {
    public static final String SERVER_INSTANCE_PREF_NAME = "ServerInstance";
    public static final String PROP_LAST_SERVER_LOC = "lastServerLocation";
    public static final String PROP_LAST_BUNDLE_LOC = "lastBundleLocation";
    private static final Logger LOG = Logger.getLogger(FujiServerPreferences.class.getName());
    
    private static Preferences getPreferences() {
        return NbPreferences.forModule(FujiServerPreferences.class);
    }
    
    private static Preferences getPreferences(String instancePrefName) {
       return  (getPreferences()).node(instancePrefName);
    }
    
    public static Preferences newInstancePreferences() {
        try {
            Preferences prefs = getPreferences();
            String[] children = prefs.childrenNames();
            List<String> list = Arrays.asList(children);
            String baseName = SERVER_INSTANCE_PREF_NAME;
            String prefName = baseName;
            for (int i = 0; list.contains(prefName) && i < 100; ++i) {
                prefName = baseName + i;
            }
            return prefs.node(prefName);
        } catch (BackingStoreException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static void saveInstancePreferences(FujiServerInstance instance) {
        Preferences prefs = getPreferences(instance.getPreferenceName()); 
        instance.save(prefs);
    }
    
    public static List<FujiServerInstance> loadServerInstances() {
        List<FujiServerInstance> list = new ArrayList<FujiServerInstance>();
        try {
            Preferences prefs = getPreferences();
            String[] children = prefs.childrenNames();
            for (String prefName : children) {
                Preferences instancePrefs = prefs.node(prefName);
                FujiServerInstance instance = AbstractFujiServerInstance.newInstance(instancePrefs);
                if ( instance != null) {
                    list.add(instance);
                }
            }
        } catch (BackingStoreException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public static void removeInstancePreferences(FujiServerInstance instance) {
        try {
            Preferences prefs = getPreferences();
            if (prefs.nodeExists(instance.getPreferenceName())) {
                Preferences instPrefs = prefs.node(instance.getPreferenceName());
                instPrefs.removeNode();
            }
        } catch (BackingStoreException ex) {
            LOG.log(Level.FINE, null, ex);
        }
    }    
    
    public static String getLastServerLocation() {
        Preferences prefs = getPreferences();
        return prefs.get(PROP_LAST_SERVER_LOC, "");
    }
    
    public static void setLastServerLocation(String lastServerLocation) {
        Preferences prefs = getPreferences();
        prefs.put(PROP_LAST_SERVER_LOC, lastServerLocation);
    }
    
    public static String getLastBundleLocation() {
        Preferences prefs = getPreferences();
        return prefs.get(PROP_LAST_BUNDLE_LOC, "");
    }
    
    public static void setLastBundleLocation(String lastBundleLocation) {
        Preferences prefs = getPreferences();
        prefs.put(PROP_LAST_BUNDLE_LOC, lastBundleLocation);
    }    
    
}
