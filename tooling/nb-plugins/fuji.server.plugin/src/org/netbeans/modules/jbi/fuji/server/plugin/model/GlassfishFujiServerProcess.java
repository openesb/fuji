/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.jbi.fuji.server.plugin.io.FujiLogWriter;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.windows.InputOutput;

public class GlassfishFujiServerProcess extends AbstractFujiServerProcess {

    private boolean mGlassfishStopped = false;

    public GlassfishFujiServerProcess(FujiServerInstance instance) {
        super(instance);
    }

    protected String getInitCommand() {
        return "\n\n";
    }

    protected String getMainJarPath() {
        File rootDir = new File(getFujiServerInstance().getRoot());
        File mainJarPath = new File(rootDir, "modules/admin-cli.jar");
        return mainJarPath.getAbsolutePath();
    }

    protected List<String> getExecutableClassArgs() {
        List<String> classArgs = new ArrayList<String>();
        classArgs.add("start-domain");
        //TODO: if there is a debug option, specify here.
        return classArgs;
    }

    protected List<String> getExecutableClass() {
        List<String> options = new ArrayList<String>();
        String mainJarPath = getMainJarPath();
        options.add("-jar");
        // check if the jar path has spaces. if yes, include it in quotes. only works on windows.
        // does not work on mac os x .
        if (mainJarPath.contains(" ")) {
            mainJarPath = "\"" + mainJarPath + "\"";
        }
        options.add(mainJarPath);
////        String mainClass = "org.apache.felix.main.Main";
////        options.add(mainClass);
        return options;
    }

    protected List<String> getJavaClassPath() {
        List<String> options = new ArrayList<String>();
////        options.add("-classpath");
////        String mainJarPath = getMainJarPath();
////        if (mainJarPath.contains(" ")) {
////            mainJarPath = "\"" + mainJarPath + "\"";
////        }
////        options.add(mainJarPath);
        return options;
    }

    protected List<String> getJavaOptions() {
        List<String> options = new ArrayList<String>();
        // add classpath option. see getExecutableClass for details.
        options.addAll(getJavaClassPath());
        return options;
    }

    @Override
    protected List<String> getDebugOptions() {
        List<String> options = new ArrayList<String>();
        return options;
    }

    @Override
    protected File getServerLogFile() {
        File rootDir = new File(this.getFujiServerInstance().getRoot());
        File logFile = new File(rootDir, "domains/domain1/logs/server.log");
        return logFile;
    }

    protected void waitFor() {

        Process process = this.getProcess();
        if (process != null) {
            try {
                //Hang this thread until the process exits
                process.waitFor();
            } catch (InterruptedException ex) {
                //  ErrorManager.getDefault().notify(ex);
                ex.printStackTrace();
            }
        }
        waitForGlassfishToTerminate();
        mGlassfishStopped = false;
    }

    private void stopGlassfish() {

        List<String> cmdList = new ArrayList<String>();
        cmdList.add(getJavaExePath());
        cmdList.addAll(getJavaOptions());
        cmdList.addAll(getExecutableClass());
        cmdList.add("stop-domain");

        final ProcessBuilder processBuilder = new ProcessBuilder(cmdList);
        processBuilder.directory(getCWD());

        Runnable stopThread = new Runnable() {

            public void run() {
                if (EventQueue.isDispatchThread()) {
                    throw new IllegalStateException("Glassfish stop cannot be called from EDT");
                }
                try {
                    Process stopProcess = processBuilder.start();
                    try {
                        stopProcess.waitFor();
                        mGlassfishStopped = true;
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        RequestProcessor.getDefault().post(stopThread, 0, Thread.currentThread().getPriority());
        Process process = this.getProcess();
        if (process != null) {
            process.destroy();
        }
    }

    private void waitForGlassfishToTerminate() {
        //TODO: find a better way to check glassfish connectivity. May be, check for status file?
        Process process = this.getProcess();
        FujiServerInstance instance = this.getFujiServerInstance();

        if (process == null || instance == null || mGlassfishStopped) {
            return;
        }
        boolean connected = false;
        // wait for start up 
        for (int startupTry = 0; startupTry < 12; ++startupTry) {
            if (instance.isJMXAdminAvailable()) {
                connected = true;
                try {
                    instance.refresh();
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
                break;
            } else {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    break;
                }
            }
            if (mGlassfishStopped) {
                break;
            }
        }
        if (!connected || mGlassfishStopped) {
            // failure to startup
            return;
        }
        LOG.info("glassfish started");
        // wait for termination.
        int nTry = 0;
        while (true) {
            connected = instance.isJMXAdminAvailable();
            if (connected) {
                nTry = 0;
            } else if (!connected && nTry > 12) {
                break; // terminated

            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
            }
            ++nTry;
            if (mGlassfishStopped) {
                break;
            }
        }
    }

    public boolean isStarted() {
        return this.isInstanceStarted();
    }
    
    public void stop() throws Exception {
        stopGlassfish();
        mGlassfishStopped = false;
    }
}
