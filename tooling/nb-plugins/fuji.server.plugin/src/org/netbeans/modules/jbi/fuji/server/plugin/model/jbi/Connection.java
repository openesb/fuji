/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Connection.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import javax.xml.namespace.QName;
import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public class Connection extends Descriptor {
    
    public static final String ELEMENT_NAME =
            "connection";
    private static final String CONSUMER_ELEMENT_NAME = 
            "consumer";
    private static final String PROVIDER_ELEMENT_NAME = 
            "provider";
    private static final String SERVICE_NAME = 
            "service-name";
    private static final String ENDPOINT_NAME = 
            "endpoint-name";
    private static final String INTERFACE_NAME   = 
            "interface-name";
    
    private int namespaceIdx;
    
    public Connection(Element element) {
        super(element);
    }
    
    public Consumer getConsumer() {
        return new Consumer(getChildElement(CONSUMER_ELEMENT_NAME));
    }
    
    public Provider getProvider() {
        return new Provider(getChildElement(PROVIDER_ELEMENT_NAME));
    }
    
    public abstract class Connected {
        
        private Element element_;
        
        Connected(Element element) {
            element_ = element;
        }

        public QName getServiceName() {
            return getQName(element_.getAttribute(SERVICE_NAME));
        }

        public QName getInterfaceName() {
            return getQName(element_.getAttribute(INTERFACE_NAME));
        }

        public String getEndpointName() {
            return element_.getAttribute(ENDPOINT_NAME);
        }
        
        public void setServiceName(QName serviceName) {
            String prefix = addNSDeclaration(serviceName);
            element_.setAttribute(SERVICE_NAME, 
                    prefix + ":" + serviceName.getLocalPart());
            
        }
        
        public void setInterfaceName(QName interfaceName) {
            String prefix = addNSDeclaration(interfaceName);
            element_.setAttribute(INTERFACE_NAME, 
                    prefix + ":" + interfaceName.getLocalPart());
        }
        
        public void setEndpointName(String endpointName) {
            element_.setAttribute(ENDPOINT_NAME, endpointName);
        }
    }

    public class Provider extends Connected {
        Provider(Element element) {
            super(element);
        }
    }
    
    public class Consumer extends Connected {
        Consumer(Element element) {
            super(element);
        }
    }

    
    private QName getQName(String qnameStr) {
        QName qname;
        int colIdx;
        
        if (qnameStr == null || qnameStr.length() == 0) {
            return null;
        }
        else if ((colIdx = qnameStr.indexOf(":")) >= 0) {
            String ns = qnameStr.substring(0, colIdx);
            String lp = qnameStr.substring(colIdx + 1, qnameStr.length());
            String uri = element_.lookupNamespaceURI(ns);
            qname = new QName(uri, lp);
        }
        else {
            qname = new QName(qnameStr);
        }
        
        return qname;
    }
    
    private String addNSDeclaration(QName name) {
        Element connections = (Element)element_.getParentNode();
        String prefix = name.getPrefix();
        if (prefix == null || prefix.length() == 0) {
            prefix = "ns" + namespaceIdx++;
        }
        connections.setAttribute("xmlns:" + prefix, name.getNamespaceURI());
        
        return prefix;
    }

    @Override
    protected void initialize() {
        createChildElement(CONSUMER_ELEMENT_NAME);
        createChildElement(PROVIDER_ELEMENT_NAME);
    }
}
