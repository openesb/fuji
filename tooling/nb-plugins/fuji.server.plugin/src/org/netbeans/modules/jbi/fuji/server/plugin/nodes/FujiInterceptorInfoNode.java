/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.model.AbstractFujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiInterceptorInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiConfigInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class FujiInterceptorInfoNode extends AbstractNode implements PropertyChangeListener {

    private static final Logger LOG = Logger.getLogger(FujiInterceptorInfoNode.class.getName());
    private static final Image SERVICE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/osgi-service.png", true); // NOI18N

    private FujiServerInstance mInstance;
    private FujiInterceptorInfo mInfo;
    private OSGiConfigPropertySet mPropSet;

    public FujiInterceptorInfoNode(FujiServerInstance instance, FujiInterceptorInfo info) {
        super(new FujiInterceptorNodeChildren(instance, info), Lookups.fixed(instance, info));
        this.mInstance = instance;
        this.mInfo = info;
        String dispName = null;
        dispName = this.mInfo.getDisplayName();
        this.setName("[FujiInterceptorInfoNode]" + dispName);
        this.setDisplayName(dispName);
        String desc = this.mInfo.getServiceInfo().getDescription();
        if (desc != null) {
            this.setShortDescription(desc);
        }
    }

    @Override
    public Image getIcon(int type) {
        Image img = computeIcon(false, type);
        return (img != null) ? img : super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        Image img = computeIcon(true, type);
        return (img != null) ? img : super.getIcon(type);
    }

    private Image computeIcon(boolean opened, int type) {
        return SERVICE_ICON;
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] baseActions = super.getActions(context);
        List<Action> actions = new ArrayList<Action>();
        actions.addAll(Arrays.asList(baseActions));
        return actions.toArray(new Action[actions.size()]);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        LOG.info("Property Change event received....");
        if (ReadWriteProperty.OSGI_PROP_INFO_PROP.equals(evt.getPropertyName())) {
            String pid = this.mInfo.getConfigInfo().getPid();
            AbstractFujiServerInstance.updateConfigProperty(this.mInstance, pid, evt);
        }
    }

    protected Sheet.Set createConfigPropertiesSet(OSGiConfigInfo info) {
        Set<String> readOnlyPropNames = new HashSet<String>();
        // readOnlyPropNames.add("status");
        readOnlyPropNames.add("name");
        readOnlyPropNames.add("method");
        // readOnlyPropNames.add("priority");

        if (info != null) {
            LOG.fine("Creating Config Properties Set....");
            this.mPropSet = new OSGiConfigPropertySet(info, readOnlyPropNames);
            this.mPropSet.addPropertyChangeListener(WeakListeners.propertyChange(this, this.mPropSet));
            return this.mPropSet.getPropertySet();
        } else {
            LOG.fine("Null OSGiConfigInfo for creating Config Properties Set....");
            return null;
        }
    }

    private Sheet.Set createInterceptorSet() {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName("FujiInterceptorInfo");
        set.setDisplayName("Interceptor");
        set.setShortDescription("Interceptor information");
        try {
            Property prop = new PropertySupport.ReadOnly<String>("Name", String.class, "Name", "Interceptor Name") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getDisplayName();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }

        try {
            Property prop = new PropertySupport.ReadOnly<String>("Status", String.class, "Status", "Status") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getServiceInfo().getPropertyValue("status");
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }

        return set;
    }

    private void addSheetSet(Sheet sheet, Sheet.Set set) {
        if ( set != null ) {
            sheet.put(set);
        } else {
            LOG.fine("PropertySet is NULL for the Fuji InterceptorInfo Node");
        }
    }
    
    @Override
    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        addSheetSet(sheet,createInterceptorSet());

        addSheetSet(sheet,createConfigPropertiesSet(this.mInfo.getConfigInfo()));

        OSGiServicePropertySet propSheetSet = new OSGiServicePropertySet(this.mInfo.getServiceInfo());
        addSheetSet(sheet,propSheetSet.getServiceSet());
        addSheetSet(sheet,propSheetSet.getServicePropertiesSet());

        return sheet;
    }

    private static final class FujiInterceptorNodeChildren extends Children.Keys <String>
            implements ChangeListener {

        private FujiServerInstance mInstance;

        public FujiInterceptorNodeChildren(FujiServerInstance instance, FujiInterceptorInfo info) {
            this.mInstance = instance;
            ChangeListener cl = WeakListeners.change(this, this.mInstance);
            this.mInstance.addChangeListener(cl);
        }

        private void updateKeys() {
            String[] keys = {};
            setKeys(keys);
        }

        protected Node[] createNodes(String key) {
            return new Node[0];
        }

        @Override
        protected void addNotify() {
            super.addNotify();
            updateKeys();
        }

        @Override
        protected void removeNotify() {
            java.util.List<String> emptyList = Collections.emptyList();
            setKeys(emptyList);
            super.removeNotify();
        }

        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == this.mInstance) {
                updateKeys();
            }
        }
    }
}
