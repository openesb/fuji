/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.modules.jbi.fuji.server.plugin.FujiServerPreferences;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.util.MvnUtils;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.netbeans.spi.project.ui.support.ProjectChooser;
import org.openide.filesystems.FileUtil;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author chikkala
 */
public class InstallBundleAction extends NodeAction {
    private static Logger sLog;
    protected Logger getLogger() {
        if ( sLog == null ) {
            sLog = Logger.getLogger(InstallBundleAction.class.getName());
        }
        return sLog;
    }

    @Override
    protected void performAction(Node[] nodes) {
        if (nodes == null || nodes.length != 1) {
            getLogger().fine("no node is selected for install bundle");
            return;
        }
        FujiServerInstance instance = nodes[0].getLookup().lookup(FujiServerInstance.class);
        if (instance == null) {
            getLogger().fine("fuji server instance is NULL");
            return;
        }
        if (!instance.isStarted()) {
            getLogger().fine("fuji server instance " + instance.getName() + " not started");
            return;
        }
        //TODO: stop the server.
        getLogger().info("Installing bundle on the server instance " + instance.getName());

        if (!instance.isJMXAdminAvailable()) {
            String canNotUseUI = "JMX Administration bundle is not available on runtime\n. " +
                "Please use command line from Output window to install bundle.\n";
            UiUtils.showMessage(canNotUseUI, false);
            return;
        }
        String[] bundlePaths = browseForBundles();
        if (bundlePaths == null || bundlePaths.length == 0) {
            return;
        }
        
        Map<String, Exception> errors = new HashMap<String, Exception>();
        for (String bundlePath : bundlePaths) {
            // get it from file dialog
            try {
                String result = instance.installBundle(bundlePath);
                getLogger().info(bundlePath + " Installed. Result: " + result);
            } catch (Exception ex) {
                // ex.printStackTrace();
                errors.put(bundlePath, ex);
                // UiUtils.showMessage(ex);
            }
        }
        if (errors.size() > 0 ) {
            StringBuffer out = new StringBuffer();
            out.append("Bundle installation failed");
            for ( String bundlePath : errors.keySet()) {
                Exception ex = errors.get(bundlePath);
                out.append("\n  Bundle: " + bundlePath + "\n    Error:" + ex.getMessage());
            }
            UiUtils.showMessage(out.toString(), true);
        }

    }

    @Override
    protected boolean enable(Node[] nodes) {
        if (nodes != null && nodes.length == 1) {
            FujiServerInstance instance = nodes[0].getLookup().lookup(FujiServerInstance.class);
            return ((instance != null) && (instance.isStarted()));
        }
        return true;
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(InstallBundleAction.class, "LBL_InstallBundleAction");
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(InstallBundleAction.class);
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    private String getInitialBundleLocation() {
        File prjFolder = ProjectChooser.getProjectsFolder();
        String currentDir = prjFolder.getAbsolutePath();
        Project mainPrj = OpenProjects.getDefault().getMainProject();
        Project fujiPrj = null;
        if (mainPrj != null && MvnUtils.UTILS.isFujiMavenProject(mainPrj)) {
            fujiPrj = mainPrj;
        }
        if (fujiPrj == null) {
            Project[] openPrjs = OpenProjects.getDefault().getOpenProjects();
            for (Project prj : openPrjs) {
                if (MvnUtils.UTILS.isFujiMavenProject(prj)) {
                    fujiPrj = prj;
                    break;
                }
            }
        }
        if (fujiPrj != null) {
            currentDir = FileUtil.toFile(fujiPrj.getProjectDirectory()).getAbsolutePath();
        }
        return currentDir;
    }

    private String[] browseForBundles() {
        String title = "Select OSGi Bundle";
        String currentDir = FujiServerPreferences.getLastBundleLocation();
        if (currentDir == null || currentDir.trim().length() == 0) {
            // get the current dir from the avaialble fuji projects
            currentDir = getInitialBundleLocation();
        }
        JFileChooser chooser = new JFileChooser();
        FileUtil.preventFileChooserSymlinkTraversal(chooser, null);
        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setMultiSelectionEnabled(true);
        chooser.addChoosableFileFilter(chooser.getAcceptAllFileFilter());
        chooser.addChoosableFileFilter(
            new FileFilter() {

                @Override
                public boolean accept(File f) {
                    return f.isDirectory() ||
                        f.getName().toLowerCase().endsWith(".zip") || // NOI18N
                        f.getName().toLowerCase().endsWith(".jar"); // NOI18N                    
                }

                @Override
                public String getDescription() {
                    String desc = "Archive files (*.zip, *.jar)";
                    return desc;
                }
            });

        if (currentDir != null && currentDir.length() > 0) {
            File f = new File(currentDir);
            if (f.exists() && f.isDirectory()) {
                chooser.setCurrentDirectory(f);
            }
        }
        String selectedPath = null;
        List<String> paths = new ArrayList<String>();

        if (JFileChooser.APPROVE_OPTION == chooser.showDialog(null, "Select")) {
            File[] selectedFiles = chooser.getSelectedFiles();
            for ( File selectedFile : selectedFiles ) {
                selectedPath = FileUtil.normalizeFile(selectedFile).getAbsolutePath();
                paths.add(selectedPath);
            }
            try {
                currentDir = chooser.getCurrentDirectory().getAbsolutePath();
                FujiServerPreferences.setLastBundleLocation(currentDir);
            } catch (Exception ex) {
            }
        }
        return paths.toArray(new String[0]);
    }
}
