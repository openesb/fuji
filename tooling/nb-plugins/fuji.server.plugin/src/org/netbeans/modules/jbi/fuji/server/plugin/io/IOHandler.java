/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.RequestProcessor;
import org.openide.util.RequestProcessor.Task;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

/**
 * handling of stdin, stdout and stderr from the fuji instance started  
 * from the services tab. (derived from the mavenide code) 
 * @author chikkala 
 */
public class IOHandler {

    private static final String IN_LINE_PREFIX = "&^#INCOMPLINE:";
    private static final long TASK_FINISH_TIMEOUT = 5000;
    private static final RequestProcessor PROCESSOR = new RequestProcessor("Fuji Instance Process IO Handlers", 100, true); //NOI18N
    private static final Logger LOG = Logger.getLogger(IOHandler.class.getName());
    private InputOutput mIO;
    private OutputWriter stdOut;
    private OutputWriter stdErr;
    private Task mOutTask;
    private Task mErrTask;
    private Task mInTask;
    private InHandler mInHandler;
    private String mInitCmd = "\n\n\n";
    private IOHandler() {
    }

    protected IOHandler(InputOutput io) {
        this();
        // System.err.println("Creating IO Handler....");
        this.mIO = io;
        stdOut = this.mIO.getOut();
        stdErr = this.mIO.getErr();
    }

    public IOHandler(InputOutput io, Process process, String initCmd) {
        this(io);
        this.mInitCmd = initCmd;
        startHandlers(process);
    }
    
    public static void logDebug(Object obj) {
        // when debugging set this to info or set it to fine
        if ( obj != null ) {
//            LOG.info(obj.toString());
             LOG.fine(obj.toString());
        } else  {
//            LOG.info("NULL");
            LOG.fine("NULL");
        }
    }
    public void writeInput(String text) {
        if ( mInHandler != null ) {
            mInHandler.writeInput(text);
        }
    }
    
    public void startHandlers(Process process) {
        //Get the standard out
        InputStream out = new BufferedInputStream(process.getInputStream(), 8192);
        //Get the standard err
        InputStream err = new BufferedInputStream(process.getErrorStream(), 8192);
        //Get the standard in
        //TODO: set the stdin processing optional
        OutputStream in = process.getOutputStream();

        mInHandler = new InHandler(in, mIO, mInitCmd);
        mInTask = PROCESSOR.post(mInHandler, 5000);

        mOutTask = PROCESSOR.post(new OutHandler(out, stdOut));

        mErrTask = PROCESSOR.post(new OutHandler(err, stdErr));
    }

    private void stopTask(Task task) {
        try {
            boolean finished = false;
            finished = task.waitFinished(TASK_FINISH_TIMEOUT);
            if (!finished) {
                task.cancel();
            }
        } catch (InterruptedException ex) {
            task.cancel();
        }
    }
    
    public void stopHandlers() {
        if (mInHandler != null) {
            mInHandler.stopInput();
        }
        stopTask(mInTask);
        stopTask(mOutTask);
        stopTask(mErrTask);
    }

    protected final void processMultiLine(String input, OutputWriter writer) {
        if (input == null) {
            return;
        }
        String[] strs = splitMultiLine(input);
        for (int i = 0; i < strs.length; i++) {
            processLine(strs[i], writer);
        }
    }

    protected final void processLine(String input, OutputWriter writer) {
        writer.println(input);
    }

    public static String[] splitMultiLine(String input) {
        List<String> list = new ArrayList<String>();
        String[] strs = input.split("\\r|\\n"); //NOI18N
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].length() > 0) {
                list.add(strs[i]);
            }
        }
        return list.toArray(new String[0]);
    }

    private class OutHandler implements Runnable {

        private BufferedReader mInReader;
        private boolean mSkiptLF = false;
        OutputWriter mOutWriter;

        public OutHandler(InputStream inStream, OutputWriter outWriter) {
            mInReader = new BufferedReader(new InputStreamReader(inStream));
            this.mOutWriter = outWriter;
        }

        private String readLine() throws IOException {
            char[] char1 = new char[1];
            boolean isReady = true;
            StringBuffer buf = new StringBuffer();
            while (isReady) {
                int ret = mInReader.read(char1);
                if (ret != 1) {
                    if (ret == -1 && buf.length() == 0) {
                        return null;
                    }
                    return buf.toString();
                }
                if (mSkiptLF) {
                    mSkiptLF = false;
                    if (char1[0] == '\n') {
                        continue;
                    }
                }
                if (char1[0] == '\n') {
                    return buf.toString();
                }
                if (char1[0] == '\r') {
                    mSkiptLF = true;
                    return buf.toString();
                }
                buf.append(char1[0]);
                isReady = mInReader.ready();
                if (!isReady) {
                    synchronized (this) {
                        try {
                            wait(500);
                        } catch (InterruptedException ex) {
                            LOG.log(Level.FINE, ex.getMessage(), ex);
                        } finally {
                            if (!mInReader.ready()) {
                                break;
                            }
                            isReady = true;
                        }
                    }

                }
            }
            return IN_LINE_PREFIX + buf.toString();

        }

        public void run() {
            logDebug("IOHandler.OutHandler begin");
            try {
                String line = readLine();
                while (line != null) {
                    if (line.startsWith(IN_LINE_PREFIX)) {
                        mOutWriter.print(line.substring(IN_LINE_PREFIX.length()));
                        line = readLine();
                        continue;
                    }
                    processLine(line, mOutWriter); //NOI18N
                    line = readLine();
                }
            } catch (IOException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            } finally {
                try {
                    mInReader.close();
                } catch (IOException ex) {
                    logDebug(ex);
                }
            }
            logDebug("IOHandler.OutHandler end");
        }
    }

    private static class InHandler implements Runnable {

        private InputOutput mIO;
        private OutputStream mOutStr;
        private boolean stopIn = false;
        private String mInitCmd = "\n\n";

        public InHandler(OutputStream out, InputOutput inputOutput, String initCmd) {
            mOutStr = out;
            mIO = inputOutput;
            mInitCmd = initCmd;
        }

        public void stopInput() {
            stopIn = true;
        }

        public void writeInput(String text) {
          try {
            if ( mOutStr != null ) {
                mOutStr.write(text.getBytes());
                mOutStr.flush();
            }
          } catch (Exception ex) {
              // ignore.
          }
        }

        public void run() {
            logDebug("IOHandler.InHandler begin");
            Reader in = mIO.getIn();
            try {
                try {
                    String empty = mInitCmd;
                    mOutStr.write(empty.getBytes());
                    mOutStr.flush();
                } catch (Exception ex) {
                    // ignore
                }
                while (true) {
                    int read = in.read();
                    if (read != -1) {
                        mOutStr.write(read);
                        mOutStr.flush();
                    } else {
                        mOutStr.close();
                        LOG.info("#### IOHandler.InHandler end: read -1");
                        return;
                    }
                    if (stopIn) {
                        LOG.info("#### IOHandler.InHandler end: stopped");
                        return;
                    }
                }

            } catch (IOException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            } finally {
                try {
                    mOutStr.close();
                } catch (IOException ex) {
                    logDebug(ex);
                }
            }
            LOG.info("#### IOHandler.InHandler end");
        }
    }
}
