/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.event.ChangeListener;

/**
 *
 * @author chikkala
 */
public interface FujiServerInstance {
    //
    public static final String DEFAULT_DEBUG_JVM_OPTION = "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=localhost:8000";
    //
    public static final String PROP_SERVER_STATE = "ServerState";
    public static final String PROP_SERVER_INSTANCE = "ServerInstance";
    public static final String PROP_ROOT = "root";
    public static final String PROP_WORK_DIR = "workDir";
    public static final String PROP_NAME = "name";
    public static final String PROP_ADMIN_HOST = "adminHost";
    public static final String PROP_ADMIN_PORT = "adminPort";
    public static final String PROP_ADMIN_USER = "adminUser";
    public static final String PROP_ADMIN_PASSWORD = "adminPassword";
    public static final String PROP_STOP_ON_IDE_EXIT = "stopOnIdeExit";
    public static final String PROP_DEBUG_OPTION = "debugOption";

    public String getPreferenceName();
    
    public String getName();
    public void setName(String name);
    
    public String getRoot();
    public void setRoot(String root);    
    
    public String getWorkDir();
    public void setWorkDir(String workDir);
    
    public String getAdminHost();
    public void setAdminHost(String host);
    
    public String getAdminPort();
    public void setAdminPort(String adminPort);
    
    public String getAdminPassword();
    public void setAdminPassword(String adminPassword);
    
    public String getAdminUser();
    public void setAdminUser(String adminUser);

    public boolean getStopOnIdeExit();
    public void setStopOnIdeExit(boolean stopOnIdeExit);

    public String getDebugOption();
    public void setDebugOption(String debugOption);

//    public String getAdditionalOption(String key, String defValue);
//    public void setAdditionalOption(String key, String value);

    public void save(Preferences prefs);
    public void load(Preferences prefs);

    public void addChangeListener(ChangeListener l);
    public void removeChangeListener(ChangeListener l);

    public void addPropertyChangeListener(PropertyChangeListener l);
    public void removePropertyChangeListener(PropertyChangeListener l);

    public void connect() throws Exception;
    
    public boolean isStarted();
    public void start() throws Exception;

    public void startDebug() throws Exception;
    public boolean isDebugging();

    public void stop() throws Exception;
    public void refresh() throws Exception;
    
    public List<OSGiBundleInfo> listBindings();

    public List<OSGiBundleInfo> listEngines();

    public List<OSGiBundleInfo> listServiceAssemblies();
    
    public List<OSGiBundleInfo> listSharedLibraries();
    
    public List<FujiInterceptorInfo> listInterceptors();

    public List<OSGiBundleInfo> listAllBundles();
    
    public List<OSGiBundleInfo> listOtherBundles();
    
    public List<OSGiServiceInfo> listServiceRefs();
    
    public List<OSGiConfigInfo> listConfigurations();
    
    public String updateConfiguration(String pid, List<OSGiPropertyInfo> props) throws Exception;

    public String installBundle(String bundlePath) throws Exception;
    
    public String installBundle(String bundlePath, boolean start, boolean reInstall) throws Exception;
    
    public String startBundle(OSGiBundleInfo info) throws Exception;

    public String stopBundle(OSGiBundleInfo info) throws Exception;
    
    public String uninstallBundle(OSGiBundleInfo info) throws Exception;

    public void showConsoleWindow();

    public void showServerLogWindow();

    public boolean isJMXAdminAvailable();

    public boolean shutdownRemoteInstance();
    
    public void printOutput(String msg);
}