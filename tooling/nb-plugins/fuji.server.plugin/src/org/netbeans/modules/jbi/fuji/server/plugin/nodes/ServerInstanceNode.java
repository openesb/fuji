/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StartServerDebugAction;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.CustomizeServerAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.InstallBundleAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.RefreshServerAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.RemoveServerAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.ServerIOAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.ServerLogAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StartServerAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StopServerAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class ServerInstanceNode extends AbstractNode implements ChangeListener , PropertyChangeListener {

    private static final Image RUNNING_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/running.png", true); // NOI18N
    private static final Image DEBUGGING_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/debugging.png", true); // NOI18N
    private static final Image SERVER_INSTANCE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/fuji-server.png", true); // NOI18N

    private static Logger LOG = Logger.getLogger(ServerInstanceNode.class.getName());
    public static final String VIEW_TYPE_PROP = "instanceView";
    public static final String VIEW_ALL = "All";
    public static final String VIEW_JBI = "JBI";
    public static final String VIEW_OSGI = "OSGi";

    private FujiServerInstance mInstance;

    public ServerInstanceNode(FujiServerInstance instance) {
        super(new ServerInstanceNodeChildren(instance), Lookups.singleton(instance));
        this.mInstance = instance;
        this.setName(this.mInstance.getPreferenceName());
        this.setDisplayName(this.mInstance.getName());
        ChangeListener cl = WeakListeners.change(this, this.mInstance);
        this.mInstance.addChangeListener(cl);

        PropertyChangeListener pcl = WeakListeners.propertyChange(this, this.mInstance);
        this.mInstance.addPropertyChangeListener(pcl);
    }

    @Override
    public Image getIcon(int type) {
        Image img = computeIcon(false, type);
        return (img != null) ? img : super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        Image img = computeIcon(true, type);
        return (img != null) ? img : super.getIcon(type);
    }

    private Image computeIcon(boolean opened, int type) {
        Image img =  SERVER_INSTANCE_ICON;
        if (this.mInstance.isStarted()) {
            if ( this.mInstance.isDebugging()) {
                img = ImageUtilities.mergeImages(SERVER_INSTANCE_ICON, DEBUGGING_BADGE_ICON, 12, 0);
            } else {
                img = ImageUtilities.mergeImages(SERVER_INSTANCE_ICON, RUNNING_BADGE_ICON, 12, 0);
            }
        }
        return img;
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] baseActions = super.getActions(context);
        List<Action> actions = new ArrayList<Action>();
        actions.add(SystemAction.get(InstallBundleAction.class));
        actions.add(null);
        actions.add(SystemAction.get(StartServerAction.class));
        actions.add(SystemAction.get(StartServerDebugAction.class));
        actions.add(SystemAction.get(StopServerAction.class));
        actions.add(SystemAction.get(RefreshServerAction.class));
        actions.add(null);
        actions.add(SystemAction.get(RemoveServerAction.class));
        actions.add(null);
        actions.add(SystemAction.get(ServerIOAction.class));
        actions.add(SystemAction.get(ServerLogAction.class));
        actions.add(null);
        actions.add(SystemAction.get(CustomizeServerAction.class));
        actions.addAll(Arrays.asList(baseActions));
        return actions.toArray(new Action[actions.size()]);
    }
    
    @Override
    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set set = Sheet.createPropertiesSet();
        
        final FujiServerInstance info = getLookup().lookup(FujiServerInstance.class);

        try {
            Property prop = new PropertySupport.ReadOnly<String>("Name", String.class, "Name", "Fuji Server Instance name") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getName();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }
        
         try {
            Property prop = new PropertySupport.ReadOnly<String>("Port", String.class, "Admin Port", "Admin Port") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getAdminPort();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }      
        
         try {
            Property prop = new PropertySupport.ReadOnly<String>("Root", String.class, "Root Dir", "Root Directory") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getRoot();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }   
        
         try {
            Property prop = new PropertySupport.ReadOnly<String>("WorkDir", String.class, "Work Dir", "Working Directory") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getWorkDir();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }   
        
        sheet.put(set);
        return sheet;
    }

    public void stateChanged(ChangeEvent e) {
        Object source = e.getSource();
        if (source == this.mInstance) {
            // updateKeys();
            this.fireIconChange();
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        // LOG.info("#### PropertyChanged " + evt.getPropertyName());
        Children children = this.getChildren();
        ServerInstanceNodeChildren instanceChildren = (ServerInstanceNodeChildren) children;
        if ( VIEW_TYPE_PROP.equals(evt.getPropertyName()) ) {
            if ( instanceChildren != null ) {
                instanceChildren.updateKeys((String)evt.getNewValue());
            }
        } else if ( FujiServerInstance.PROP_SERVER_STATE.equals(evt.getPropertyName())) {
            if ( instanceChildren != null ) {
                instanceChildren.updateKeys();
            }
        }

    }

    private static final class ServerInstanceNodeChildren extends Children.Keys<String>
        implements ChangeListener {
        private String[] jbiKeys = {
                OSGiBundleInfoFolderNode.SE_FOLDER_KEY,
                OSGiBundleInfoFolderNode.BC_FOLDER_KEY,
                OSGiBundleInfoFolderNode.SL_FOLDER_KEY,
                OSGiBundleInfoFolderNode.SA_FOLDER_KEY,
                FujiInterceptorFolderNode.FOLDER_KEY
        };
        private String[] osgiKeys = {
                OSGiBundleInfoFolderNode.BL_FOLDER_KEY,
                OSGiServiceInfoFolderNode.FOLDER_KEY,
                OSGiConfigInfoFolderNode.FOLDER_KEY
        };
        private FujiServerInstance mInstance;

        private String mViewType = VIEW_ALL;

        public ServerInstanceNodeChildren(FujiServerInstance instance) {
            this.mInstance = instance;
            ChangeListener cl = WeakListeners.change(this, this.mInstance);
            this.mInstance.addChangeListener(cl);
        }
        protected void updateKeys(String view) {
            this.mViewType = view;
            if ( this.mViewType == null ) {
                this.mViewType = VIEW_ALL;
            }
            updateKeys();
        }

        protected void updateKeys() {
            String[] keys = new String[0];
            if (this.mInstance != null && this.mInstance.isStarted()) {
                List<String> keyList = new ArrayList<String>();
                if (VIEW_ALL.equals(mViewType) || VIEW_JBI.equals(mViewType)) {
                    for (String key : jbiKeys) {
                        // LOG.info("### Adding JBI Folder " + key);
                        keyList.add(key);
                    }
                }
                if (VIEW_ALL.equals(mViewType) || VIEW_OSGI.equals(mViewType)) {
                    for (String key : osgiKeys) {
                        // LOG.info("### Adding OSGi Folder " + key);
                        keyList.add(key);
                    }
                }
                keys = keyList.toArray(new String[0]);
            }
//            String[] keys = {
//                OSGiBundleInfoFolderNode.SE_FOLDER_KEY,
//                OSGiBundleInfoFolderNode.BC_FOLDER_KEY,
//                OSGiBundleInfoFolderNode.SL_FOLDER_KEY,
//                OSGiBundleInfoFolderNode.SA_FOLDER_KEY,
//                FujiInterceptorFolderNode.FOLDER_KEY,
//                OSGiBundleInfoFolderNode.BL_FOLDER_KEY,
//                OSGiServiceInfoFolderNode.FOLDER_KEY,
//                OSGiConfigInfoFolderNode.FOLDER_KEY
//            };
            setKeys(keys);
        }

        protected Node[] createNodes(String key) {
            if (OSGiServiceInfoFolderNode.FOLDER_KEY.equals(key)) {
                return new Node[]{new OSGiServiceInfoFolderNode(this.mInstance)};
            } else if (FujiInterceptorFolderNode.FOLDER_KEY.equals(key)) {
                return new Node[]{new FujiInterceptorFolderNode(this.mInstance)};
            } else if (OSGiConfigInfoFolderNode.FOLDER_KEY.equals(key)) {
                return new Node[]{new OSGiConfigInfoFolderNode(this.mInstance)};                
            } else {
                return new Node[]{new OSGiBundleInfoFolderNode(this.mInstance, key)};
            }
        }

        @Override
        protected void addNotify() {
            super.addNotify();
            updateKeys();
        }

        @Override
        protected void removeNotify() {
            java.util.List<String> emptyList = Collections.emptyList();
            setKeys(emptyList);
            super.removeNotify();
        }

        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == this.mInstance) {
                // updateKeys();
            }
        }
    }
}
