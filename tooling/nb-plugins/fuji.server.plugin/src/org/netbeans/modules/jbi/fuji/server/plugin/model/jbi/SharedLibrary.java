/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SharedLibrary.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public class SharedLibrary extends Descriptor {
    
    public static final String ELEMENT_NAME =
            "shared-library";
    private static final String VERSION =
            "version";
    private static final String CLASS_LOADER_DELEGATION =
            "class-loader-delegation";
    private static final String SHARED_LIBRARY_CLASS_PATH =
            "shared-library-class-path";
    
    public SharedLibrary(Element element) {
        super(element);
    }
    
    public String getVersion() {
        return element_.getAttribute(VERSION);
    }
    
    public String getClassLoaderDelegation() {
        return element_.getAttribute(CLASS_LOADER_DELEGATION);
    }
    
    public Identification getIdentification() {
        return new Identification(getChildElement(Identification.ELEMENT_NAME));
    }
    
    public ClassPath getSharedLibraryClassPath() {
        return new ClassPath(getChildElement(SHARED_LIBRARY_CLASS_PATH));
    }
    
    @Override
    protected void initialize() {
    }
}
