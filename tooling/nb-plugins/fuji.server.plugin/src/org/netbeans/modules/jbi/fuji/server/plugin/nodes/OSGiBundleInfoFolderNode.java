/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiBundleInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class OSGiBundleInfoFolderNode extends AbstractNode {

    private static final Image FOLDER_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/folder.png", true); // NOI18N
    private static final Image BC_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/BindingComponentBadge.png", true); // NOI18N
    private static final Image SE_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/ServiceEngineBadge.png", true); // NOI18N
    private static final Image SA_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/ServiceAssemblyBadge.png", true); // NOI18N
    private static final Image SL_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/SharedLibraryBadge.png", true); // NOI18N
    private static final Image BL_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/osgi-bundle-badge.png", true); // NOI18N
    public static final String BC_FOLDER_KEY = "BindingFolderNode";
    public static final String SE_FOLDER_KEY = "EngineFolderNode";
    public static final String SA_FOLDER_KEY = "ServiceAssemblyFolderNode";
    public static final String SL_FOLDER_KEY = "SharedLibraryFolderNode";
    public static final String BL_FOLDER_KEY = "OtherBundleFolderNode";
    private FujiServerInstance mInstance;
    private String mFolderType;

    public OSGiBundleInfoFolderNode(FujiServerInstance instance, String folderType) {
        super(new BundleInfoFolderNodeChildren(instance, folderType), Lookups.singleton(instance));
        this.mInstance = instance;
        this.mFolderType = folderType;
        this.setName(this.mFolderType);
    }

    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(OSGiBundleInfoFolderNode.class, "LBL_" + this.mFolderType);
    }

    @Override
    public Image getIcon(int type) {
        Image img = computeIcon(false, type);
        return (img != null) ? img : super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        Image img = computeIcon(true, type);
        return (img != null) ? img : super.getIcon(type);
    }

    private Image computeIcon(boolean opened, int type) {
        Image badge = null;
        if (BC_FOLDER_KEY.equals(this.mFolderType)) {
            badge = BC_BADGE_ICON;
        } else if (SE_FOLDER_KEY.equals(this.mFolderType)) {
            badge = SE_BADGE_ICON;
        } else if (SA_FOLDER_KEY.equals(this.mFolderType)) {
            badge = SA_BADGE_ICON;
        } else if (SL_FOLDER_KEY.equals(this.mFolderType)) {
            badge = SL_BADGE_ICON;
        } else if (BL_FOLDER_KEY.equals(this.mFolderType)) {
            badge = BL_BADGE_ICON;
        }

        if (badge == null) {
            return FOLDER_ICON;
        } else {
            Image img = ImageUtilities.mergeImages(FOLDER_ICON, badge, 8, 8);
            return img;
        }
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] baseActions = super.getActions(context);
        List<Action> actions = new ArrayList<Action>();
        // actions.add(SystemAction.get(RefreshAction.class));
        actions.addAll(Arrays.asList(baseActions));
        return actions.toArray(new Action[actions.size()]);
    }

    private static final class BundleInfoFolderNodeChildren extends Children.Keys <OSGiBundleInfo>
        implements ChangeListener {

        private FujiServerInstance mInstance;
        private String mFolderType;

        public BundleInfoFolderNodeChildren(FujiServerInstance instance, String type) {
            this.mInstance = instance;
            this.mFolderType = type;
            ChangeListener cl = WeakListeners.change(this, this.mInstance);
            this.mInstance.addChangeListener(cl);
        }
        
        private OSGiBundleInfo[] getSortedKeys(List<OSGiBundleInfo> list) {
            OSGiBundleInfo[] keys = new OSGiBundleInfo[0];
            List<OSGiBundleInfo> sortedList = new LinkedList<OSGiBundleInfo>();
            sortedList.addAll(list);
            try {
                Collections.sort(sortedList, new Comparator<OSGiBundleInfo>() {

                    public int compare(OSGiBundleInfo o1, OSGiBundleInfo o2) {
                        return o1.getSymbolicName().compareTo(
                            o2.getSymbolicName());
                    }
                });

                keys = sortedList.toArray(new OSGiBundleInfo[0]);
            } catch (Exception ex) {
                keys = list.toArray(new OSGiBundleInfo[0]);
            }

            return keys;
        }
        
        private void updateKeys() {
            List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
            
            if (BC_FOLDER_KEY.equals(this.mFolderType)) {
                list = this.mInstance.listBindings();
            } else if (SE_FOLDER_KEY.equals(this.mFolderType)) {
                list = this.mInstance.listEngines();
            } else if (SA_FOLDER_KEY.equals(this.mFolderType)) {
                list = this.mInstance.listServiceAssemblies();
            } else if (SL_FOLDER_KEY.equals(this.mFolderType)) {
                list = this.mInstance.listSharedLibraries();
            } else if (BL_FOLDER_KEY.equals(this.mFolderType)) {
                list = this.mInstance.listOtherBundles();
            }
            OSGiBundleInfo[] keys = getSortedKeys(list);
            setKeys(keys);
        }

        protected Node[] createNodes(OSGiBundleInfo key) {
           // OSGiBundleInfo info = (OSGiBundleInfo) key;
            return new Node[]{new OSGiBundleInfoNode(this.mInstance, key)};
        }

        @Override
        protected void addNotify() {
            super.addNotify();
            updateKeys();
        }

        @Override
        protected void removeNotify() {
            java.util.List<OSGiBundleInfo> emptyList = Collections.emptyList();
            setKeys(emptyList);
            super.removeNotify();
        }

        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == this.mInstance) {
                updateKeys();
            }
        }
    }
}
