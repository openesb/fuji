/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiBundleInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.Component;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.Connection;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.Identification;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.Jbi;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.ServiceAssembly;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.ServiceUnit;
import org.netbeans.modules.jbi.fuji.server.plugin.model.jbi.SharedLibrary;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;

/**
 *
 * @author chikkala
 */
public class JBIDescriptorPropertySet {

    private OSGiBundleInfo mInfo;
    private Jbi mJbiInfo;
    private Sheet.Set mPropSet;

    public JBIDescriptorPropertySet(OSGiBundleInfo info) {
        this.mInfo = info;
    }

    public Sheet.Set getPropertySet() {
        if (this.mPropSet == null) {
            this.mPropSet = createSheetSet();
            addProperties(this.mPropSet);
        }
        return this.mPropSet;
    }

    private Sheet.Set createSheetSet() {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName("JBIDescriptorInfo");
        set.setDisplayName("JBI Descriptor");
        set.setShortDescription("JBI Descriptor details");
        return set;
    }

    private void addProperties(Sheet.Set set) {

        try {
            Property prop = new PropertySupport.ReadOnly<String>("JbiArtifactType", String.class, "JBI Artifact", "JBI Artifact type in this bundle") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getJbiArtifactType();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
        addJbiProperties(set);
        try {
            Property prop = new PropertySupport.ReadOnly<String>("JbiDescriptor", String.class, "JBI Descriptor XML", "JBI Descriptor XML") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getJbiDescriptor();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
    }

    private void addJbiProperties(Sheet.Set set) {
        String jbiXml = this.mInfo.getJbiDescriptor();
        if (jbiXml == null || jbiXml.trim().length() == 0) {
            return;
        }
        try {
            this.mJbiInfo = Jbi.newJbi(jbiXml);
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        String type = this.mInfo.getJbiArtifactType();
        if (OSGiBundleInfo.JBI_BC.equals(type) || OSGiBundleInfo.JBI_SE.equals(type)) {
            addJbiComponentProperties(set, this.mJbiInfo.getComponent());
        } else if (OSGiBundleInfo.JBI_SA.equals(type)) {
            addJbiServiceAssemblyProperties(set, this.mJbiInfo.getServiceAssembly());
        } else if (OSGiBundleInfo.JBI_SL.equals(type)) {
            addJbiSharedLibProperties(set, this.mJbiInfo.getSharedLibrary());
        }

    }

    private void addJbiIdentification(Sheet.Set set, final Identification id) {
        try {
            Property prop = new PropertySupport.ReadOnly<String>("Name", String.class, "Name", id.getName()) {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return id.getName();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
        try {
            Property prop = new PropertySupport.ReadOnly<String>("Description", String.class, "Description", id.getDescription()) {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return id.getDescription();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
    }

    private void addReadOnlyProperty(Sheet.Set set, final String value, String name, String displayName, String description) {
        
        try {
            Property prop = new PropertySupport.ReadOnly<String>(name, String.class, displayName, description) {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return value;
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }    
    }
    
    private void addReadOnlyProperty(Sheet.Set set, final String[] value, String name, String displayName, String description) {
        String[] arrayType = new String[0];
        try {
            Property prop = new PropertySupport.ReadOnly(name, arrayType.getClass(), displayName, description) {

                @Override
                public Object getValue() throws IllegalAccessException, InvocationTargetException {
                    return value;
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }    
    }
    
    private void addJbiComponentProperties(Sheet.Set set, final Component jbiComp) {
        if (jbiComp == null) {
            return;
        }
        addJbiIdentification(set, jbiComp.getIdentification());
//        addReadOnlyProperty(set, jbiComp.getComponentClassLoaderDelegation(), 
//                "ComponentClassLoaderDelegation", 
//                "Component ClassLoader Delegation", 
//                "Component ClassLoader Delegation" );
        addReadOnlyProperty(set, jbiComp.getComponentClassName(), 
                "ComponentClassName", 
                "Component Class Name", 
                "Component Class Name" );
        addReadOnlyProperty(set, jbiComp.getComponentClassPath().getPathElements(),
                "ComponentClassPath",
                "Component Class Path",
                "Component Class Path");
        addReadOnlyProperty(set, jbiComp.getSharedLibraries().toArray(new String[0]),
                "SharedLibraries",
                "Shared Libraries",
                "Shared Libraries");        
        
    }
    
    private void addJbiSharedLibProperties(Sheet.Set set, final SharedLibrary jbiSL) {
        if (jbiSL == null) {
            return;
        }
        addJbiIdentification(set, jbiSL.getIdentification());
//        try {
//            Property prop = new PropertySupport.ReadOnly<String>("Version", String.class, "Version", jbiSL.getVersion()) {
//
//                @Override
//                public String getValue() throws IllegalAccessException, InvocationTargetException {
//                    return jbiSL.getVersion();
//                }
//            };
//            set.put(prop);
//        } catch (Exception ex) {
//        }

//        try {
//            Property prop = new PropertySupport.ReadOnly<String>("ClassLoaderDelegation", String.class, "ClassLoaderDelegation", jbiSL.getClassLoaderDelegation()) {
//
//                @Override
//                public String getValue() throws IllegalAccessException, InvocationTargetException {
//                    return jbiSL.getClassLoaderDelegation();
//                }
//            };
//            set.put(prop);
//        } catch (Exception ex) {
//        }

        String[] classPathType = new String[0];
        try {
            Property prop = new PropertySupport.ReadOnly("ClassPath", classPathType.getClass(), "Class Path", "Class Path") {

                @Override
                public Object getValue() throws IllegalAccessException, InvocationTargetException {
                    return jbiSL.getSharedLibraryClassPath().getPathElements();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
    }

    private void addJbiServiceAssemblyProperties(Sheet.Set set, final ServiceAssembly jbiSA) {
        if (jbiSA == null) {
            return;
        }
        addJbiIdentification(set, jbiSA.getIdentification());
        // SUs
        List<ServiceUnit> suList = jbiSA.getServiceUnits();
        List<String> suNames = new ArrayList<String>();
        for (ServiceUnit su : suList) {
            String suName = su.getIdentification().getName() + "[" + su.getComponentName() + "]";
            suNames.add(suName);
        }
        final String[] suArray = suNames.toArray(new String[0]);
        String[] suType = new String[0];
        try {
            Property prop = new PropertySupport.ReadOnly("ServiceUnits", suType.getClass(), "Service Units", "Service Units") {

                @Override
                public Object getValue() throws IllegalAccessException, InvocationTargetException {
                    return suArray;
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
        // Connections
        List<Connection> connList = jbiSA.getConnections();
        List<String> connNames = new ArrayList<String>();
        for (Connection conn : connList) {
            QName ifQName1 = conn.getConsumer().getInterfaceName();
            QName servQName1 = conn.getConsumer().getServiceName();
            QName ifQName2 = conn.getProvider().getInterfaceName();
            QName servQName2 = conn.getProvider().getServiceName();
            QName cQName = servQName1 != null ? servQName1 : ifQName1;
            QName sQName = servQName2 != null ? servQName2 : ifQName2;
            String connName = new StringBuffer().append(cQName).append("->").append(sQName).toString();
            connNames.add(connName);
        }
        final String[] connArray = connNames.toArray(new String[0]);
        String[] connType = new String[0];
        try {
            Property prop = new PropertySupport.ReadOnly("Connections", suType.getClass(), "Connections", "Connections") {

                @Override
                public Object getValue() throws IllegalAccessException, InvocationTargetException {
                    return connArray;
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
    }
}
