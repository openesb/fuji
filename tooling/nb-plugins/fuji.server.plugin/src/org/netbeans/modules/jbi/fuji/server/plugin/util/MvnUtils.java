/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author chikkala
 */
public class MvnUtils {
    public static final String FUJI_PACKAGE_TYPE = "fuji-app";
    public static final String FUJI_GROUP_ID = "open-esb.fuji";
    public static final String FUJI_PLUGIN_ID = "maven-fuji-plugin";
    
    private static DocumentBuilder docBuilder = null;
    private static XPathExpression jbiFrameworkDepXpathExpr1 = null;
    private static XPathExpression jbiFrameworkDepXpathExpr2 = null;
    private static XPathExpression artifactIdXpathExpr = null;
    private static XPathExpression versionXpathExpr = null;

    private static XPathExpression mavenFujiPluginXpathExpr = null;
    
    private static Logger  LOG = Logger.getLogger(MvnUtils.class.getName());
    
    public static MvnUtils UTILS = new MvnUtils();
    
    public MvnUtils() {
        
    }
    
    public String getProjectBuildArtifact(Project prj) {
        String bundlePath = null;
        File buildDir = new File(FileUtil.toFile(prj.getProjectDirectory()), "target");
        FileObject pomXml = prj.getProjectDirectory().getFileObject("pom.xml");
        if ( buildDir == null || !buildDir.exists() || pomXml == null) {
            return null;
        }
        Document pomDoc = getPOMDocument(pomXml);
        String artifactId = getProjectArtifactId(pomDoc);
        String version = getProjectVersion(pomDoc);
        String buildArtifact = artifactId + "-" + version + ".jar";        
        File buildArtifactFile = new File(buildDir, buildArtifact);
        if (!buildArtifactFile.exists()) {
            // fallback to <artifactid>.jar
            buildArtifact = artifactId + ".jar";
            buildArtifactFile = new File(buildDir, buildArtifact);
        }
        if ( buildArtifactFile.exists()) {
            bundlePath = buildArtifactFile.getAbsolutePath();
        }
        LOG.info("Build Artifact " + buildArtifact);
        return bundlePath;
    }
    
    private DocumentBuilder getDocumentBuidler() {
        if (docBuilder == null) {
            // check if the pom.xml contains the fuji maven plugin.
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            // domFactory.setNamespaceAware(true);
            domFactory.setNamespaceAware(false);
            try {
                docBuilder = domFactory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
        }
        return docBuilder;
    }

    private Document getPOMDocument(FileObject pomFO) {
        Document doc = null;
        try {
            doc = (getDocumentBuidler()).parse(FileUtil.toFile(pomFO));
        } catch (SAXException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        } catch (IOException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return doc;
    }
    
    public String getProjectArtifactId(Document pomDoc) {
        String artifactId = null;
        try {
            if (artifactIdXpathExpr == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                artifactIdXpathExpr = xpath.compile("/project/artifactId");
            }
            Object result = artifactIdXpathExpr.evaluate(pomDoc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            if (nodes != null && nodes.getLength() > 0) {
                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    artifactId = node.getTextContent();   
                    break;
                    //LOG.info(node.getNodeName() + ":" + artifactId);
                }
            }

        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return artifactId;
    }
    
    public String getProjectVersion(Document pomDoc) {
        String version = null;
        try {
            if (versionXpathExpr == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                versionXpathExpr = xpath.compile("/project/version");
            }
            Object result = versionXpathExpr.evaluate(pomDoc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            if (nodes != null && nodes.getLength() > 0) {
                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    version = node.getTextContent();
                    break;
                    // LOG.info(node.getNodeName() + ":" + version);
                }
            }

        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return version;
    }    

    public boolean findMavemFujiPlugin(Document pomDoc) {
        boolean foundMavenFujiPlugin = false;
        try {
            if (mavenFujiPluginXpathExpr == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                mavenFujiPluginXpathExpr = xpath.compile("/project/build/plugins/plugin[groupId='open-esb.fuji' and artifactId='maven-fuji-plugin']");
            }
            Object result = null;
            result = mavenFujiPluginXpathExpr.evaluate(pomDoc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;

            if (nodes != null && nodes.getLength() > 0) {
                foundMavenFujiPlugin = true;
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return foundMavenFujiPlugin;
    }

    public boolean findJbiFrameworkDependency(Document pomDoc) {
        boolean foundJbiFramework = false;
        try {
            if (jbiFrameworkDepXpathExpr1 == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                jbiFrameworkDepXpathExpr1 = xpath.compile("/project/dependencies/dependency[groupId='open-esb.fuji' and artifactId='jbi-framework']");
            }
            if (jbiFrameworkDepXpathExpr2 == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                jbiFrameworkDepXpathExpr2 = xpath.compile("/project/dependencies/dependency[groupId='open-esb.fuji' and artifactId='framework']");
            }
            Object result = null;
            result = jbiFrameworkDepXpathExpr1.evaluate(pomDoc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            if ( nodes == null || nodes.getLength() == 0 ) {
                result = jbiFrameworkDepXpathExpr2.evaluate(pomDoc, XPathConstants.NODESET);
                nodes = (NodeList) result;
            }
            if (nodes != null && nodes.getLength() > 0) {
                foundJbiFramework = true;
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return foundJbiFramework;
    }

    public boolean isFujiPOM(FileObject pomFO) {
        boolean iappProject = false;
        try {
            Document doc = getPOMDocument(pomFO);
            if (doc != null) {
                if ( findMavemFujiPlugin(doc) || findJbiFrameworkDependency(doc)) {
                    iappProject = true;
                }
            } else {
                LOG.fine("FUJI POM XML DOC is NULL");
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return iappProject;
    }
    
    public boolean isFujiMavenProject(Project prj) {
        boolean fujiPrj = false;
        if (prj != null) {
            FileObject pomXml = prj.getProjectDirectory().getFileObject("pom.xml");
            if (pomXml != null) {
                fujiPrj = isFujiPOM(pomXml);
            }
        }
        return fujiPrj;
    }    
}
