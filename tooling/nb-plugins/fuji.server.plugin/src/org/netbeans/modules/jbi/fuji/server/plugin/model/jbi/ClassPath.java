/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ClassPath.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Represents <code>class-path</code> element in jbi.xml.
 * @author kcbabo
 */
public class ClassPath extends Descriptor {
    
    /** element name **/
    private static final String PATH_ELEMENT = 
            "path-element";
    
    /**
     * Create new Classpath from element. 
     * @param element <code>class-path</code> node
     */
    public ClassPath(Element element) {
        super(element);
    }
    
    /**
     * Returns list of all class path elements as String array.
     * @return list of clas path definitions
     */
    public String[] getPathElements() {
        ArrayList<String> pathList = new ArrayList<String>();
        NodeList paths = element_.getElementsByTagNameNS(NS_URI, PATH_ELEMENT);
        for (int i = 0; i < paths.getLength(); i++) {
            Element e = (Element)paths.item(i);
            pathList.add(e.getTextContent());
        }
        
        return pathList.toArray(new String[0]);
    }
    
    @Override
    protected void initialize() {
    }
}
