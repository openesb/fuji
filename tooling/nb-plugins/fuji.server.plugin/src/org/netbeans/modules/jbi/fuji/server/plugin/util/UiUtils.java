/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.util;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

/**
 *
 * @author chikkala
 */
public class UiUtils {
    private static final Logger LOG = Logger.getLogger(UiUtils.class.getName());
    
    public static void showMessage(String msg, boolean error) {
        int msgType = (error) ? NotifyDescriptor.ERROR_MESSAGE : NotifyDescriptor.INFORMATION_MESSAGE;        
        NotifyDescriptor msgDialog = new NotifyDescriptor.Message(msg, msgType);
        Object result = DialogDisplayer.getDefault().notify(msgDialog);
    }

    public static void showMessage(Exception ex) {
        LOG.log(Level.FINE, ex.getMessage(), ex);
        showMessage(ex.getMessage(), true);
    }    

    public static boolean isFelixDistribution(File rootDir) {
        File mainJarPath = new File(rootDir, "bin/felix.jar");
        return mainJarPath.exists();
    }

    public static boolean isEquinoxDistribution(File rootDir) {
        File mainJarPath = new File(rootDir, "equinox.jar");
        return mainJarPath.exists();
    }

    public static boolean isGlassfishDistribution(File rootDir) {
        File mainJarPath = new File(rootDir, "modules/admin-cli.jar");
        return mainJarPath.exists();
    }

    public static boolean isFujiInstallDir(String root) {
        boolean isFujiDir = false;
        File rootDir = new File(root);
        if (isFelixDistribution(rootDir) ||
            isEquinoxDistribution(rootDir) ||
            isGlassfishDistribution(rootDir) ) {
            isFujiDir = true;
        }
        return isFujiDir;
    }

    public static boolean isEditableType(String type) {
        boolean editable = false;

        // check for the primitive types
        // boolean, byte, char, short, int, long, float, double and void
        //TODO: for now only basic types and strings are editable.
        //TODO: allow editing arrays and vectors of basic types or string.
        if ( "java.lang.String".equals(type)) {
            editable = true;
        } else if (Boolean.TYPE.getName().equals(type) ||
                    Byte.TYPE.getName().equals(type) ||
                    Character.TYPE.getName().equals(type) ||
                    Short.TYPE.getName().equals(type) ||
                    Integer.TYPE.getName().equals(type) ||
                    Long.TYPE.getName().equals(type) ||
                    Float.TYPE.getName().equals(type) ||
                    Double.TYPE.getName().equals(type) ){
            editable = true;
        } else if (type.equals("boolean") ||
                type.equals("byte") ||
                type.equals("char") ||
                type.equals("short") ||
                type.equals("int") ||
                type.equals("long") ||
                type.equals("float") ||
                type.equals("double") ){
            editable = true;
        }
        return editable;
    }
}
