/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jbi.fuji.server.plugin.FujiServerManager;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.mvn.NbFujiSettings;
import org.netbeans.modules.jbi.fuji.server.plugin.ui.ListFujiInstancesPanel;
import org.netbeans.modules.jbi.fuji.server.plugin.util.MvnUtils;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;
import org.openide.windows.OutputWriter;

/**
 *
 * @author chikkala
 */
public class DeployAction extends AbstractAction implements ContextAwareAction {

    private static Logger  LOG = Logger.getLogger(DeployAction.class.getName());
    
    public DeployAction() {
        super();

//        Image img = Utilities.loadImage("org/netbeans/modules/jbi/fuji/ifl/actions/start.png");
//        ImageIcon icon = new ImageIcon(img);
//        this.putValue(SMALL_ICON, icon);
//        LOG.info("#### Deploy Action instantiated....");
//        System.err.println("#### Deploy Action instantiated....");
    }

    public void actionPerformed(ActionEvent e) {
        assert false;
    }

    public Action createContextAwareInstance(Lookup context) {
        return new ContextAction(context);
    }

    private boolean enable(Project p) {
//        LOG.info("Trying to enable deploy action....");
        assert p != null;
        // TODO for which projects action should be enabled
        FileObject pomFO = p.getProjectDirectory().getFileObject("pom.xml");
        if (pomFO == null) {
//            LOG.info("## No pom in the project");
            return false;
        }
        FileObject nbPrjFO = p.getProjectDirectory().getFileObject("nbproject");
        FileObject prjXml = p.getProjectDirectory().getFileObject("nbproject/project.xml");
        if (nbPrjFO != null && prjXml != null) {
//            LOG.info("### Nbproject dir present in mvn prj");
            return false;
        }
        // check if the pom.xml contains the fuji maven plugin. if yes, then enable this
        boolean isFujiPrj = MvnUtils.UTILS.isFujiMavenProject(p);
//        LOG.info("IsFujiMavenProject " + isFujiPrj);
        return isFujiPrj;
    }

    private String labelFor(Project p) {
        assert p != null;
        // TODO menu item label with optional mnemonics
        return NbBundle.getMessage(DeployAction.class, "LBL_DeployAction");
    }

    private static String getBundlePath(Project prj) {
        String bundlePath = "target/app.jar";
        File prjDir = FileUtil.toFile(prj.getProjectDirectory());
        File bundleFile = new File(prjDir, bundlePath);
        if ( bundleFile.exists()) {
            return bundleFile.getAbsolutePath();
        } else {
            return MvnUtils.UTILS.getProjectBuildArtifact(prj);
        }
    }
    
    private static NbFujiSettings getDeployOptions(File prjDir) {
        NbFujiSettings settings = null;
        try {
            settings = NbFujiSettings.load(prjDir);
        } catch (IOException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }        
        return settings;
    }
    /**
     * //TODO: move selection of server instance to FujiServerManager
     * @return
     */
    private static FujiServerInstance getSelectedServerInstance(NbFujiSettings settings) {
        FujiServerInstance inst = null;
        
            inst = settings.getTargetServerInstance();
            if (inst != null) {
                return inst;
            }

        List<FujiServerInstance> list = FujiServerManager.getInstance().listServerInstances();
        if ( list.size() > 0 ) {
            if ( list.size() == 1) {
                inst = list.get(0);
            } else {
                // show select server dialog
                // inst = list.get(0); // return first server instance for time being
                inst = ListFujiInstancesPanel.showServerInstanceList();
            }
        } else {
            String canNotUseUI = "No Fuji Server Instance Found for deployment.\n";
            UiUtils.showMessage(canNotUseUI, false);            
        }
        if ( inst != null && settings != null ) {
            settings.setTargetServerInstance(inst);
            try {
                settings.save();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return inst;
    }
    
    private void printFileObject(PrintWriter out, FileObject fo) {
        out.println("Name : " + fo.getPath());
        Enumeration<String> attrNames = fo.getAttributes();
        for ( ;attrNames.hasMoreElements();) {
            String attr = attrNames.nextElement();
            out.println(" " + attr + "=" + fo.getAttribute(attr));
        }
        FileObject[] children = fo.getChildren();
        if ( children != null ) {
            for ( FileObject child : children ) {
                printFileObject(out, child);
            }
        }
    }
    
    private void printLayerFiles() {
        FileObject root = FileUtil.getConfigRoot();
        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(writer);
        printFileObject(out, root);
        out.close();
        System.out.println(writer.getBuffer().toString());
    }
    
    public static void perform(Project prj, OutputWriter out, OutputWriter err) {
        assert prj != null;
        // printLayerFiles();
        File prjDir = FileUtil.toFile(prj.getProjectDirectory());
        // perform deploy from this project
        String bundlePath = getBundlePath(prj);
        LOG.info("Deploying bundle: " + bundlePath);
        NbFujiSettings settings = getDeployOptions(prjDir);
        FujiServerInstance inst = getSelectedServerInstance(settings);

        if (inst == null) {
//            String canNotUseUI = "Fuji Server Instance Not Selected.\n " +
//                "Please select the instance before deploy/install bundle.\n";
//            UiUtils.showMessage(canNotUseUI, false);            
            return;
        }
        if (!inst.isStarted()) {
            // get whether server should be started automatically or not.
            String canNotUseUI = "Fuji Server Instance [" + inst.getName() + "] Not Started.\n " +
                    "Please start the instance before deploy/install bundle.\n";
            UiUtils.showMessage(canNotUseUI, false);
            return;
        }
        if (!inst.isJMXAdminAvailable()) {
            String canNotUseUI = "Can not connect to runtime using JMX.\n " +
                    "Please use command line to deploy/install bundle.\n";
            UiUtils.showMessage(canNotUseUI, false);
            return;
        }

        if (bundlePath == null || !(new File(bundlePath)).exists()) {
            String canNotUseUI = "Bundle not found for deployment. Please build the project before deploying. ";
            UiUtils.showMessage(canNotUseUI, false);
            return;
        }
        
        boolean startOnDeploy = settings.isStartOnDeploy();
        boolean reDeploy = settings.isReDeploy();

        inst.printOutput("Deploying " + bundlePath + "\n");
        if ( out != null ) {
            out.println("Deploying " + bundlePath);
        }
        String result = null;
        try {
            result = inst.installBundle(bundlePath, startOnDeploy, reDeploy);
            inst.printOutput("Deploy Results: " + result + "\n");
            if ( out != null ) {
                out.println("Deploy Results: " + result);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            UiUtils.showMessage(ex.getMessage(), true);
            if (err != null) {
                err.println("Exception occured during deployment: " + ex.getMessage());
            }
        }
    }
    
    private final class ContextAction extends AbstractAction implements Presenter.Popup {

        private final Project p;

        public ContextAction(Lookup context) {
            Project _p = context.lookup(Project.class);
            p = (_p != null && enable(_p)) ? _p : null;

        }

        public void actionPerformed(ActionEvent e) {
            perform(p, null, null);
        }

        public JMenuItem getPopupPresenter() {
            class Presenter extends JMenuItem implements DynamicMenuContent {

                public Presenter() {
                    super(ContextAction.this);
                }

                public JComponent[] getMenuPresenters() {
                    if (p != null) {
                        Mnemonics.setLocalizedText(this, labelFor(p));
                        return new JComponent[]{this};
                    } else {
                        return new JComponent[0];
                    }
                }

                public JComponent[] synchMenuPresenters(JComponent[] items) {
                    return getMenuPresenters();
                }
            }
            return new Presenter();
        }
    }
}
