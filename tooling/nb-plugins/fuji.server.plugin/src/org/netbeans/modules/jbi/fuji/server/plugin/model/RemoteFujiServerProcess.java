/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.openide.windows.InputOutput;

/**
 *
 * @author chikkala
 */
public class RemoteFujiServerProcess extends AbstractFujiServerProcess {

    private AbstractFujiServerProcess mProcess;
    private boolean remoteServerStopped = false;

    public RemoteFujiServerProcess(FujiServerInstance instance) {
        super(instance);
        this.mProcess = (AbstractFujiServerProcess) AbstractFujiServerProcess.newFujiServerProcess(instance);
    }

    @Override
    protected String getInitCommand() {
        return this.mProcess.getInitCommand();
    }

    @Override
    protected String getMainJarPath() {
        return this.mProcess.getMainJarPath();
    }

    @Override
    protected List<String> getExecutableClassArgs() {
        return this.mProcess.getExecutableClassArgs();
    }

    @Override
    protected List<String> getExecutableClass() {
        return this.mProcess.getExecutableClass();
    }

    @Override
    protected List<String> getJavaClassPath() {
        return this.mProcess.getJavaClassPath();
    }

    @Override
    protected List<String> getJavaOptions() {
        return this.mProcess.getJavaOptions();
    }

    @Override
    protected List<String> getDebugOptions() {
        List<String> options = new ArrayList<String>();
        return options;
    }

    @Override
    protected File getServerLogFile() {
        return this.mProcess.getServerLogFile();
    }

    @Override
    protected void waitFor() {
        waitForRemoteServerToTerminate();
        remoteServerStopped = false;
    }

    @Override
    protected void startProcess() throws IOException {
        this.setInstanceStarted(true);
    }

    @Override
    protected void printStartMessage() {
        InputOutput io = null;
        io = getConsoleIO();

        try {
            io = getConsoleIO();
            io.getOut().println("Connecting to Fuji instance [" + getFujiServerInstance().getName() + "]....");
            io.getOut().println();
            io.getErr().println("NOTE: CLI interface in IDE is not available when connected to remote Fuji Instance. " +
                    "\n If you need CLI interface in IDE, stop and restart the server from IDE");
            io.getOut().println(); //XXXd
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public boolean isStarted() {
        return this.isInstanceStarted();
    }

    public void stop() throws Exception {
        stopRemoteServer();
    }

    private void stopRemoteServer() {
        boolean shutdown = this.getFujiServerInstance().shutdownRemoteInstance();
        remoteServerStopped = true; // flag the server stop so that any waiting processes will terminate.
    }

    private void waitForRemoteServerToTerminate() {

        FujiServerInstance instance = this.getFujiServerInstance();

        if (instance == null) {
            return;
        }
        boolean connected = false;
        int nTry = 0;
        while (true) {
            connected = instance.isJMXAdminAvailable();
            if (connected) {
                nTry = 0;
            } else if (!connected && nTry > 12) {
                break; // terminated
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
            }
            ++nTry;
            if (remoteServerStopped) {
                remoteServerStopped = false; //reset flag
                break;
            }
        }
    }


}
