/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiBundleInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StartBundleAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StopBundleAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.UninstallBundleAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class OSGiBundleInfoNode extends AbstractNode {

    private static final Image RUNNING_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/running.png", true); // NOI18N
    private static final Image INSTALLED_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/installed.png", true); // NOI18N
    private static final Image RESOLVED_BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/resolved.png", true); // NOI18N
    
    
    private static final Image BUNDLE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/osgi-bundle.png", true); // NOI18N
    private static final Image BC_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/BindingComponent.png", true); // NOI18N
    private static final Image SE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/ServiceEngine.png", true); // NOI18N
    private static final Image SA_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/ServiceAssembly.png", true); // NOI18N
    private static final Image SL_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/SharedLibrary.png", true); // NOI18N
    private static final Image BL_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/osgi-bundle.png", true); // NOI18N
   
    private static Logger LOG = Logger.getLogger(OSGiBundleInfoNode.class.getName());
    
    private FujiServerInstance mInstance;
    private OSGiBundleInfo mBundleInfo;

    public OSGiBundleInfoNode(FujiServerInstance instance, OSGiBundleInfo info) {
        super(new BundleInfoNodeChildren(instance, info), Lookups.fixed(instance, info));
        this.mInstance = instance;
        this.mBundleInfo = info;
        this.setName(this.mBundleInfo.getBundleId());
        this.setDisplayName(this.mBundleInfo.getSymbolicName());
    }

    @Override
    public Image getIcon(int type) {
        Image img = computeIcon(false, type);
        return (img != null) ? img : super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        Image img = computeIcon(true, type);
        return (img != null) ? img : super.getIcon(type);
    }

    private Image computeIcon(boolean opened, int type) {
        Image img = BUNDLE_ICON;
        Image badge = null;
        
        String state = this.mBundleInfo.getState();
        if ( OSGiBundleInfo.STATE_INSTALLED.equalsIgnoreCase(state)) {
            badge = INSTALLED_BADGE_ICON;
        } else if ( OSGiBundleInfo.STATE_RESOLVED.equalsIgnoreCase(state)) {
            badge = RESOLVED_BADGE_ICON;
        } else {
            // System.out.println("#### Bundle Info State is not mactching for badged icon... = #" + state + "#" );
            badge = null;
        }

        if (OSGiBundleInfo.JBI_BC.equals(this.mBundleInfo.getJbiArtifactType())) {
            img = BC_ICON;
        } else if (OSGiBundleInfo.JBI_SE.equals(this.mBundleInfo.getJbiArtifactType())) {
            img = SE_ICON;
        } else if (OSGiBundleInfo.JBI_SL.equals(this.mBundleInfo.getJbiArtifactType())) {
            img = SL_ICON;
        } else if (OSGiBundleInfo.JBI_SA.equals(this.mBundleInfo.getJbiArtifactType())) {
            img = SA_ICON;
        } else {
            img = BL_ICON;
            // badge = null; 
        }
        Image mergedImg = img;
        if ( badge != null ) {
            mergedImg = ImageUtilities.mergeImages(img, badge, 15, 8);
        }
        return mergedImg;
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] baseActions = super.getActions(context);
        List<Action> actions = new ArrayList<Action>();
        actions.add(SystemAction.get(StartBundleAction.class));
        actions.add(SystemAction.get(StopBundleAction.class));
        actions.add(SystemAction.get(UninstallBundleAction.class));
        actions.add(null);
        actions.addAll(Arrays.asList(baseActions));
        return actions.toArray(new Action[actions.size()]);
    }

    protected Sheet.Set createHeadersSet(final OSGiBundleInfo info) {
        Sheet.Set set = ReadOnlyProperty.createSheetSet(
                info.getHeaders(), 
                "OSGiBundleHeaders",
                "OSGi Bundle Headers",
                "OSGi Bundle Headers");
        return set;
    }
    
    @Override
    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName("OSGiBundleInfo");
        set.setDisplayName("OSGi Bundle Details");
        set.setShortDescription("OSGi Bundle information");
        
        final OSGiBundleInfo info = getLookup().lookup(OSGiBundleInfo.class);

        try {
            Property prop = new PropertySupport.ReadOnly<String>("SymbolicName", String.class, "Symbolic Name", "Symbolic name of the bundle") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getSymbolicName();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }
        
         try {
            Property prop = new PropertySupport.ReadOnly<String>("BundleId", String.class, "Bundle Id", "Bundle Id of the bundle") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getBundleId();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }      
        
         try {
            Property prop = new PropertySupport.ReadOnly<String>("State", String.class, "State", "State of the bundle") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getState();
                }
            };
            set.put(prop);
        } catch (Exception ex) { }   

        try {
            Property prop = new PropertySupport.ReadOnly<String>("Location", String.class, "Location", "Location of the bundle") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return info.getLocation();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
        
         try {
            Property prop = new PropertySupport.ReadOnly<String>("LastModified", String.class, "Last Modifed Time", "Last Modified Time") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {           
                    String lastModifTime = null;
                    try {                    
                     lastModifTime = DateFormat.getDateTimeInstance().format(new Date(Long.valueOf(info.getLastModified())));
                    } catch ( NumberFormatException ex) {
                        LOG.log(Level.FINE, ex.getMessage(), ex);
                    }
                    return lastModifTime;
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
        
        if (!OSGiBundleInfo.JBI_UNKNOWN.equals(info.getJbiArtifactType()) ){
            JBIDescriptorPropertySet jbiSet = new JBIDescriptorPropertySet(info);
            sheet.put(jbiSet.getPropertySet());
        }   
        
        sheet.put(set);
        sheet.put(createHeadersSet(info));
        return sheet;
    }

    private static final class BundleInfoNodeChildren extends Children.Keys <String>
        implements ChangeListener {

        private FujiServerInstance mInstance;
        private OSGiBundleInfo mBundleInfo;

        public BundleInfoNodeChildren(FujiServerInstance instance, OSGiBundleInfo info) {
            this.mInstance = instance;
            ChangeListener cl = WeakListeners.change(this, this.mInstance);
            this.mInstance.addChangeListener(cl);
        }

        private void updateKeys() {
            String[] keys = {};
            setKeys(keys);
        }

        protected Node[] createNodes(String key) {
            return new Node[0];
        }

        @Override
        protected void addNotify() {
            super.addNotify();
            updateKeys();
        }

        @Override
        protected void removeNotify() {
            java.util.List<String> emptyList = Collections.emptyList();
            setKeys(emptyList);
            super.removeNotify();
        }

        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == this.mInstance) {
                updateKeys();
            }
        }
        
    }
    
}
