/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.ui;

import java.awt.Component;
import java.awt.Dialog;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.swing.JComponent;
import org.netbeans.modules.jbi.fuji.server.plugin.FujiServerManager;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;

/**
 *
 * @author chikkala
 */
public abstract class ServerWizard {
    public static final String PROP_WIZ_TYPE = "WizardType";
    public static final String WIZ_TYPE_NEW = "NewWizard";
    public static final String WIZ_TYPE_CUSTOMIZER = "Customizer";

    private WizardDescriptor.Panel<WizardDescriptor>[] panels;

    public ServerWizard() {
    }

    /**
     * Initialize panels representing individual wizard's steps and sets
     * various properties for them influencing wizard appearance.
     */
    private WizardDescriptor.Panel<WizardDescriptor>[] getPanels() {
        if (panels == null) {
            panels = new WizardDescriptor.Panel[]{
                    new NewServerWizardPanel1()
                };
            String[] steps = new String[panels.length];
            for (int i = 0; i < panels.length; i++) {
                Component c = panels[i].getComponent();
                // Default step name to component name of panel. Mainly useful
                // for getting the name of the target chooser to appear in the
                // list of steps.
                steps[i] = c.getName();
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    // Sets step number of a component
                    jc.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                    // Sets steps names for a panel
                    jc.putClientProperty("WizardPanel_contentData", steps);
                    // Turn on subtitle creation on each step
                    jc.putClientProperty("WizardPanel_autoWizardStyle", Boolean.TRUE);
                    // Show steps on the left side with the image on the background
                    jc.putClientProperty("WizardPanel_contentDisplayed", Boolean.TRUE);
                    // Turn on numbering of all steps
                    jc.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
                }
            }
        }
        return panels;
    }

    public boolean showWizard() {
        WizardDescriptor wizardDescriptor = new WizardDescriptor(getPanels());
        // {0} will be replaced by WizardDesriptor.Panel.getComponent().getName()
        initWizard(wizardDescriptor);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(wizardDescriptor);
        dialog.setVisible(true);
        dialog.toFront();
        boolean finished = (wizardDescriptor.getValue() == WizardDescriptor.FINISH_OPTION);
        if (finished) {
            performAction(wizardDescriptor);
        }
        return finished;
    }

    protected abstract void initWizard(WizardDescriptor wizDesc);

    protected abstract void performAction(WizardDescriptor wizDesc);

    protected void loadInstanceSettings(WizardDescriptor wizDesc, FujiServerInstance instance) {

        wizDesc.putProperty(FujiServerInstance.PROP_NAME, instance.getName());
        wizDesc.putProperty(FujiServerInstance.PROP_ROOT, instance.getRoot());
        wizDesc.putProperty(FujiServerInstance.PROP_WORK_DIR, instance.getWorkDir());
        wizDesc.putProperty(FujiServerInstance.PROP_ADMIN_PORT, instance.getAdminPort());
        wizDesc.putProperty(FujiServerInstance.PROP_ADMIN_USER, instance.getAdminUser());
        wizDesc.putProperty(FujiServerInstance.PROP_ADMIN_PASSWORD, instance.getAdminPassword());
        wizDesc.putProperty(FujiServerInstance.PROP_STOP_ON_IDE_EXIT, new Boolean(instance.getStopOnIdeExit()));
        wizDesc.putProperty(FujiServerInstance.PROP_DEBUG_OPTION, instance.getDebugOption());

        
    }
    
    protected void saveInstanceSettings(WizardDescriptor wizDesc, FujiServerInstance instance) {

        String name = (String) wizDesc.getProperty(FujiServerInstance.PROP_NAME);
        String rootDir = (String) wizDesc.getProperty(FujiServerInstance.PROP_ROOT);
        String workDir = (String) wizDesc.getProperty(FujiServerInstance.PROP_WORK_DIR);
        String adminPort = (String) wizDesc.getProperty(FujiServerInstance.PROP_ADMIN_PORT);
        String adminUser = (String) wizDesc.getProperty(FujiServerInstance.PROP_ADMIN_USER);
        String adminPassword = (String) wizDesc.getProperty(FujiServerInstance.PROP_ADMIN_PASSWORD);
        Boolean stopOnIdeExit = (Boolean) wizDesc.getProperty(FujiServerInstance.PROP_STOP_ON_IDE_EXIT);
        String debugOption = (String) wizDesc.getProperty(FujiServerInstance.PROP_DEBUG_OPTION);

        instance.setName(name);
        instance.setRoot(rootDir);
        instance.setWorkDir(workDir);
        instance.setAdminPort(adminPort);
        instance.setAdminUser(adminUser);
        instance.setAdminPassword(adminPassword);
        instance.setStopOnIdeExit(stopOnIdeExit);
        instance.setDebugOption(debugOption);

        FujiServerManager.getInstance().saveServerInstance(instance);
    }

    public static class NewServerWizard extends ServerWizard {

        @Override
        protected void initWizard(WizardDescriptor wizDesc) {
            wizDesc.setTitleFormat(new MessageFormat("{0}"));
            wizDesc.setTitle("Add Fuji Server Instance");
            wizDesc.putProperty(PROP_WIZ_TYPE, WIZ_TYPE_NEW);
        }

        @Override
        protected void performAction(WizardDescriptor wizDesc) {

            FujiServerInstance instance = FujiServerManager.getInstance().newServerInstance();
            saveInstanceSettings(wizDesc, instance);
        }
    }

    public static class CustomizeServerWizard extends ServerWizard {

        private FujiServerInstance mInstance;

        public CustomizeServerWizard(FujiServerInstance instance) {
            this.mInstance = instance;
        }

        @Override
        protected void initWizard(WizardDescriptor wizDesc) {
            wizDesc.setTitleFormat(new MessageFormat("{0}"));
            wizDesc.setTitle("Fuji Server Instance Customizer");
            wizDesc.putProperty(PROP_WIZ_TYPE, WIZ_TYPE_CUSTOMIZER);
            loadInstanceSettings(wizDesc, this.mInstance);
        }

        @Override
        protected void performAction(WizardDescriptor wizDesc) {
            saveInstanceSettings(wizDesc, this.mInstance);
        }
    }
}
