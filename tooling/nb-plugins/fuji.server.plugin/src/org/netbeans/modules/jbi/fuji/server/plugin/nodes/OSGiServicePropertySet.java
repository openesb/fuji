/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiPropertyInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiServiceInfo;
import org.openide.nodes.Node.Property;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;

/**
 *
 * @author chikkala
 */
public class OSGiServicePropertySet implements PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(OSGiServicePropertySet.class.getName());
    private OSGiServiceInfo mInfo;
    private Sheet.Set mServSet;
    private Sheet.Set mPropSet;
    private List<PropertyChangeListener> listeners = Collections.synchronizedList(new LinkedList<PropertyChangeListener>());
    
    public OSGiServicePropertySet(OSGiServiceInfo info) {
        this.mInfo = info;
    }
    
    public Sheet.Set getServiceSet() {
        if (this.mServSet == null) {
            this.mServSet = createServiceSet();
            addServiceProperties(this.mServSet);
        }
        return this.mServSet;
    }
    public Sheet.Set getServicePropertiesSet() {
        if (this.mPropSet == null) {
            this.mPropSet = createServicePropertiesSet();
            addServicePropertiesProperties(this.mPropSet);
        }
        return this.mPropSet;
    }
    private Sheet.Set createServiceSet() {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName("OSGiService");
        set.setDisplayName("OSGi Service");
        set.setShortDescription("OSGi Service Information");
        return set;
    }
     private Sheet.Set createServicePropertiesSet() {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName("OSGiServiceProperties");
        set.setDisplayName("OSGi Service Properties");
        set.setShortDescription("OSGi Service Properties Information");
        return set;
    }   
     
    private void addServiceProperties(Sheet.Set set) {
        try {
            Property prop = new PropertySupport.ReadOnly<String>("Id", String.class, "Service ID", "Service ID") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getId();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }

        try {
            Property prop = new PropertySupport.ReadOnly<String>("Bundle", String.class, "Bundle", "Bundle that registed this service") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getBundle();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }

        try {
            Property prop = new PropertySupport.ReadOnly<String>("Description", String.class, "Description", "Description of this service") {

                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getDescription();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }

        String[] objClassesType = new String[0];
        try {
            Property prop = new PropertySupport.ReadOnly("ObjectClasses", objClassesType.getClass(), "Service Objects", "Service Objects") {

                @Override
                public Object getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getObjectClasses();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }
        
        String[] usingBundleType = new String[0];
        try {
            Property prop = new PropertySupport.ReadOnly("UsingBundles", usingBundleType.getClass(), "Using Bundles", "Using Bundles") {

                @Override
                public Object getValue() throws IllegalAccessException, InvocationTargetException {
                    return mInfo.getUsingBundles();
                }
            };
            set.put(prop);
        } catch (Exception ex) {
        }        
    }
    
    private void addServicePropertiesProperties(Sheet.Set set) {
        
//        List<OSGiPropertyInfo> propList = this.mInfo.getProperties();
//
//        for ( OSGiPropertyInfo propInfo : propList ) {
//           set.put(new ReadOnlyProperty(propInfo));
//        }
        
        Map<String, OSGiPropertyInfo> propMap = this.mInfo.getPropertiesMap();
        SortedSet<String> keySet = new TreeSet<String>(propMap.keySet());
        for (String key : keySet) {
           set.put(new ReadOnlyProperty(propMap.get(key)));
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        listeners.add(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        listeners.remove(pcl);
    }

    private void fire(PropertyChangeEvent evt) {
 
        PropertyChangeListener[] pcls =  listeners.toArray(new PropertyChangeListener[0]);
        for (int i = 0; i < pcls.length; i++) {
            pcls[i].propertyChange(evt);
        }
    }    
    
    public void propertyChange(PropertyChangeEvent evt) {
        LOG.fine("OSGiServiceInfo change event firing...");
        fire(evt);
    }
}
