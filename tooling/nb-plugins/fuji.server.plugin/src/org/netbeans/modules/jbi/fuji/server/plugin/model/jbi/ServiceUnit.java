/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnit.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.server.plugin.model.jbi;

import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public class ServiceUnit extends Descriptor {
    
    public static final String ELEMENT_NAME =
            "service-unit";
    public static final String ARTIFACTS_ZIP = 
            "artifacts-zip";
    public static final String COMPONENT_NAME = 
            "component-name";
    public static final String TARGET =
            "target";
    
    public ServiceUnit(Element element) {
        super(element);
    }
    
    public void setArtifactsZip(String artifactsZip) {
        getChildElement(ARTIFACTS_ZIP).setTextContent(artifactsZip);
    }
    
    public void setComponentName(String componentName) {
        getChildElement(COMPONENT_NAME).setTextContent(componentName);
    }
    
    public String getArtifactsZip() {
        return getChildElement(ARTIFACTS_ZIP).getTextContent();
    }
    
    public String getComponentName() {
        return getChildElement(COMPONENT_NAME).getTextContent();
    }
    
    public Identification getIdentification() {
        return new Identification(getChildElement(Identification.ELEMENT_NAME));
    }
    
    @Override
    protected void initialize() {
        createChildElement(Identification.ELEMENT_NAME);
        Element target = createChildElement(TARGET);
        createChildElement(target, ARTIFACTS_ZIP);
        createChildElement(target, COMPONENT_NAME);
    }

}
