/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.RefreshAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.NewServerAction;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.Action;
import org.netbeans.modules.jbi.fuji.server.plugin.FujiServerManager;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;

/**
 *
 * @author chikkala
 */
public class FujiServerManagerNode extends AbstractNode {

    private static final Image FUJI_SERVERS_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/fuji-server.png", true); // NOI18N

    public FujiServerManagerNode() {
        super(new FujiServerManagerChildren());
        this.setName("FUJI_SERVERS");
    }

    @Override
    public Image getIcon(int type) {
        Image img = computeIcon(false, type);
        return (img != null) ? img : super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        Image img = computeIcon(true, type);
        return (img != null) ? img : super.getIcon(type);
    }

    private Image computeIcon(boolean opened, int type) {
        return FUJI_SERVERS_ICON;
    }

    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(FujiServerManagerNode.class, "LBL_FujiServers"); //NOI18N

    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] baseActions = super.getActions(context);
        List<Action> actions = new ArrayList<Action>();
        actions.add(SystemAction.get(NewServerAction.class));
        actions.add(SystemAction.get(RefreshAction.class));
        actions.addAll(Arrays.asList(baseActions));
        return actions.toArray(new Action[actions.size()]);
    }

    private static final class FujiServerManagerChildren extends Children.Keys <FujiServerInstance>
        implements PropertyChangeListener, ChangeListener {

        public FujiServerManagerChildren() {
            FujiServerManager mgr = FujiServerManager.getInstance();
            PropertyChangeListener l = WeakListeners.propertyChange(this, mgr);
            mgr.addPropertyChangeListener(l);
        }

        private void updateKeys() {
            List<FujiServerInstance> list = FujiServerManager.getInstance().listServerInstances();
            FujiServerInstance[] keys = list.toArray(new FujiServerInstance[0]);
            setKeys(keys);
        }

        protected Node[] createNodes(FujiServerInstance key) {
            // FujiServerInstance obj = (FujiServerInstance) key;
//            ChangeListener cl = WeakListeners.change(this, obj);
//            obj.addChangeListener(cl);
////            PropertyChangeListener l = WeakListeners.propertyChange(this, obj);
////            obj.addPropertyChangeListener(l);
            return new Node[]{new ServerInstanceNode(key)};
        }

        @Override
        protected void addNotify() {
            super.addNotify();
            updateKeys();
        }

        @Override
        protected void removeNotify() {
            java.util.List<FujiServerInstance> emptyList = Collections.emptyList();
            setKeys(emptyList);
            super.removeNotify();
        }

        public void propertyChange(PropertyChangeEvent evt) {
            FujiServerManager mgr = FujiServerManager.getInstance();
            Object source = evt.getSource();
            if (source == mgr) {
                updateKeys();
//                if (evt.getNewValue() != null) {
//                    refreshKey(evt.getNewValue());
//                } else if (evt.getOldValue() != null) {
//                    refreshKey(evt.getOldValue());
//                }
            } else if ( source != null && source instanceof FujiServerInstance ) {
                refreshKey((FujiServerInstance)source);
            }
        }

        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if ( source != null && source instanceof FujiServerInstance) {
                refreshKey((FujiServerInstance)source);
            }
        }
    }
}
