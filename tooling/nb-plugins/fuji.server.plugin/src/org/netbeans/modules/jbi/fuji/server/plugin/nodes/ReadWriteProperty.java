/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiPropertyInfo;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.WeakListeners;

/**
 *
 * @author chikkala
 */
public class ReadWriteProperty extends PropertySupport.ReadWrite<String> {
    public static final String OSGI_PROP_INFO_PROP = "OSGiPropertyInfo";
    private static final Logger LOG = Logger.getLogger(ReadWriteProperty.class.getName());
    private OSGiPropertyInfo info;
    
    private List<PropertyChangeListener> listeners = Collections.synchronizedList(new LinkedList<PropertyChangeListener>());
    
    public ReadWriteProperty(OSGiPropertyInfo info) {
        super(info.getName(), String.class, info.getName(), info.getName());
        this.info = info;
    }

    @Override
    public String getValue() throws IllegalAccessException, InvocationTargetException {
        return this.info.getValue();
    }

    @Override
    public void setValue(String value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        // info.setValue(value);
        fire(info, value);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        listeners.add(pcl);
        LOG.fine("ReadWriteProperty.addPCL");
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        listeners.remove(pcl);
        LOG.fine("ReadWriteProperty.removePCL");
    }

    private void fire(OSGiPropertyInfo prop, String value) {
 
        PropertyChangeListener[] pcls = listeners.toArray(new PropertyChangeListener[0]);
        for (int i = 0; i < pcls.length; i++) {
            pcls[i].propertyChange(new PropertyChangeEvent(this, OSGI_PROP_INFO_PROP, prop, value));
        }
    }
    
    public static Sheet.Set createSheetSet(List<OSGiPropertyInfo> propList, String name, String displayName, String shortDescription, PropertyChangeListener pcl) {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName(name);
        set.setDisplayName(displayName);
        set.setShortDescription(shortDescription);
        for (OSGiPropertyInfo propInfo : propList) {
            ReadWriteProperty prop = new  ReadWriteProperty(propInfo);
            if ( pcl != null ) {
                prop.addPropertyChangeListener(WeakListeners.propertyChange(pcl, prop));
            }
            set.put(prop);
        }
        return set;
    }
}
