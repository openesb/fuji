/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiConfigInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiConstants;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiPropertyInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.openide.nodes.Sheet;
import org.openide.util.WeakListeners;

/**
 *
 * @author chikkala
 */
public class OSGiConfigPropertySet implements PropertyChangeListener {

    private static final Logger LOG = Logger.getLogger(OSGiConfigPropertySet.class.getName());
    private OSGiConfigInfo mInfo;
    private Sheet.Set mPropSet;
    private Set<String> mReadOnlyProps;
    private List<PropertyChangeListener> listeners = Collections.synchronizedList(new LinkedList<PropertyChangeListener>());

    public OSGiConfigPropertySet(OSGiConfigInfo info) {
        this(info, new HashSet<String>());
    }

    public OSGiConfigPropertySet(OSGiConfigInfo info, Set<String> readOnlyProps) {
        this.mInfo = info;
        this.mReadOnlyProps = new HashSet<String>();
        this.mReadOnlyProps.add(OSGiConstants.SERVICE_ID);
        this.mReadOnlyProps.add(OSGiConstants.SERVICE_PID);
        this.mReadOnlyProps.add(OSGiConstants.SERVICE_FACTORYPID);
        if (readOnlyProps != null) {
            this.mReadOnlyProps.addAll(readOnlyProps);
        }
    }

    public Sheet.Set getPropertySet() {
        if (this.mPropSet == null) {
            this.mPropSet = createSheetSet();
            addProperties(this.mPropSet);
        }
        return this.mPropSet;
    }

    private Sheet.Set createSheetSet() {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName("OSGiConfiguration");
        set.setDisplayName("Configuration");
        set.setShortDescription("Configuraiton Properties");
        return set;
    }

    private SortedSet<OSGiPropertyInfo> createOSGiPropertyInfoSortedSet() {
        SortedSet<OSGiPropertyInfo> propSet = new TreeSet<OSGiPropertyInfo>(new Comparator<OSGiPropertyInfo>() {
            public int compare(OSGiPropertyInfo o1, OSGiPropertyInfo o2) {
                return o1.getName().compareTo(o2.getName());
            }          
        });
        return propSet;        
    }
    
    private void addProperties(Sheet.Set set) {
        if ( this.mInfo == null ) {
            LOG.log(Level.FINE, "Configuration Info object is NULL", new Exception());
            return;
        }
        List<OSGiPropertyInfo> allProps = this.mInfo.getProperties();
        SortedSet<OSGiPropertyInfo> readOnly = createOSGiPropertyInfoSortedSet();
        SortedSet<OSGiPropertyInfo> readWrite = createOSGiPropertyInfoSortedSet();
        for (OSGiPropertyInfo propInfo : allProps) {
            LOG.info("OSGiConfig Prop : Name=" + propInfo.getName() + " Type=" + propInfo.getType());
            String type = propInfo.getType();
            if (!UiUtils.isEditableType(type) || this.mReadOnlyProps.contains(propInfo.getName())) {
                readOnly.add(propInfo);
            } else {
                readWrite.add(propInfo);
            }
        }

        for (OSGiPropertyInfo propInfo : readWrite) {
            ReadWriteProperty rwProp = new ReadWriteProperty(propInfo);
            LOG.fine("addPropertyChangeListener...");
            rwProp.addPropertyChangeListener(WeakListeners.propertyChange(this, rwProp));
            set.put(rwProp);
        }
        
        for (OSGiPropertyInfo propInfo : readOnly) {
            set.put(new ReadOnlyProperty(propInfo));
        }       
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        listeners.add(pcl);
        LOG.fine("ConfigPropertySet.addPCL");
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        listeners.remove(pcl);
        LOG.fine("ConfigPropertySet.removePCL");
    }

    private void fire(PropertyChangeEvent evt) {

        PropertyChangeListener[] pcls = listeners.toArray(new PropertyChangeListener[0]);
        for (int i = 0; i < pcls.length; i++) {
            pcls[i].propertyChange(evt);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        LOG.fine("OSGiPropertyInfo change event firing...");
        fire(evt);
    }
}
