/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author chikkala
 */
public class FujiInterceptorInfo {

    public static final String INTERCEPTOR_CLASS = "com.sun.jbi.interceptors.Interceptor";
    public static final String INTERCEPTOR_NAME_PROP = "name";
    public static final String INTERCEPTOR_METHOD_PROP = "method";
    public static final String INTERCEPTOR_SERVIE_PID = "service.pid";
    
    private static final Logger LOG = Logger.getLogger(FujiInterceptorInfo.class.getName());
    
    private OSGiServiceInfo serviceInfo;
    private OSGiConfigInfo configInfo;    
    private String displayName;

    public FujiInterceptorInfo() {
    }

    public FujiInterceptorInfo(OSGiServiceInfo serviceInfo, OSGiConfigInfo configInfo) {
        this.serviceInfo = serviceInfo;
        this.configInfo = configInfo;
    }

    public OSGiConfigInfo getConfigInfo() {
        return configInfo;
    }

    public void setConfigInfo(OSGiConfigInfo configInfo) {
        this.configInfo = configInfo;
    }

    public OSGiServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(OSGiServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public String getDisplayName() {
        if ( this.displayName == null ) {
            this.displayName = computeDisplayName();
        }
        return this.displayName;
    }
    
    private String computeDisplayName() {
        // get name from interceptor name, service name, service impl
        String name = null;
        if ( this.configInfo != null ) {
            name = this.configInfo.getPropertyValue(INTERCEPTOR_NAME_PROP);
        }
        if (name == null && this.serviceInfo != null) {
            name = this.serviceInfo.getPropertyValue(INTERCEPTOR_NAME_PROP);
            if (name == null) {
                String[] objClasses = this.serviceInfo.getObjectClasses();
                if (objClasses != null && objClasses.length > 0) {
                    name = objClasses[0];
                } else {
                    name = this.serviceInfo.getId();
                }
            }
        }
        return name;
    }

    public static List<FujiInterceptorInfo> createFujiInterceptorInfoList(List<OSGiServiceInfo> serviceList, List<OSGiConfigInfo> configList) {
        List<FujiInterceptorInfo> list = new ArrayList<FujiInterceptorInfo>();
        Map<String, OSGiConfigInfo> nameConfigMap = new HashMap<String, OSGiConfigInfo>();
        Map<String, OSGiConfigInfo> methodConfigMap = new HashMap<String, OSGiConfigInfo>();
        for (OSGiConfigInfo cfgInfo : configList) {
            String name = cfgInfo.getPropertyValue(INTERCEPTOR_NAME_PROP);
            String method = cfgInfo.getPropertyValue(INTERCEPTOR_METHOD_PROP);
            if (name != null) {
                nameConfigMap.put(name, cfgInfo);
            }
            if (method != null) {
                methodConfigMap.put(method, cfgInfo);
            }
        }
        for (OSGiServiceInfo srvInfo : serviceList) {
            String[] objClasses = srvInfo.getObjectClasses();
            for (String objClass : objClasses) {
                if (INTERCEPTOR_CLASS.equals(objClass)) {
                    String name = srvInfo.getPropertyValue(INTERCEPTOR_NAME_PROP);
                    String method = srvInfo.getPropertyValue(INTERCEPTOR_METHOD_PROP);
                    OSGiConfigInfo cfgInfo = null;
                    if ( name != null ) {
                        cfgInfo = nameConfigMap.get(name);
                    } else  {
                        // fall back to find it using "method"
                        if ( method != null ) {
                            cfgInfo = methodConfigMap.get(method);
                        }
                    }
                    if ( cfgInfo == null ) {
                        LOG.warning("Unable to find configuration for interceptor service " + srvInfo);
                    }
                    list.add(new FujiInterceptorInfo(srvInfo, cfgInfo));
                    break;
                }
            }
        }
        return list;
    }
}
