/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.util.HashMap;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StartServerDebugAction;
import org.netbeans.modules.jbi.fuji.server.plugin.io.FujiLogWriter;
import org.netbeans.modules.jbi.fuji.server.plugin.io.IOHandler;
import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StartServerAction;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.StopServerAction;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

/**
 *
 * @author chikkala
 */
public abstract class AbstractFujiServerProcess implements FujiServerProcess, Runnable, PropertyChangeListener {
    
    protected static final Logger LOG = Logger.getLogger(AbstractFujiServerProcess.class.getName());

    private static HashMap<String, InputOutput> sIOMap = new HashMap<String, InputOutput>();

    private FujiServerInstance mInstance;
    private InputOutput mIO;
    private IOHandler mIOHandler;
    private boolean mInstanceStarted = false;
    private Process mProcess;

    private InputOutput mServerLogIO;
    private boolean mDebugMode = false;

    private AbstractFujiServerProcess() {
    }

    protected AbstractFujiServerProcess(FujiServerInstance instance) {
        this.mInstance = instance;
    }

    protected FujiServerInstance getFujiServerInstance() {
        return this.mInstance;
    }

    protected abstract String getInitCommand();

    public static FujiServerProcess newFujiServerProcess(FujiServerInstance instance) {
        FujiServerProcess serverProcess = null;
        File rootDir = new File(instance.getRoot());
        if ( UiUtils.isFelixDistribution(rootDir)) {
            serverProcess = new FelixFujiServerProcess(instance);
        } else if (UiUtils.isEquinoxDistribution(rootDir)) {
            serverProcess = new EquinoxFujiServerProcess(instance);
        } else if (UiUtils.isGlassfishDistribution(rootDir)) {
            serverProcess = new GlassfishFujiServerProcess(instance);
        } else {
            LOG.fine("Can not find a server process object for " + instance);
        }
        return serverProcess;
    }

    public boolean isDebugging() {
        return this.mDebugMode;
    }

    protected void setDebugMode(boolean debugMode) {
        this.mDebugMode = debugMode;
    }

    protected boolean isDebugMode() {
        return this.mDebugMode;
    }

    public void start(boolean debug) throws Exception {
        if (!isStarted()) {
            setDebugMode(debug);
            RequestProcessor.getDefault().post(this);
        }
    }

    public void start() throws Exception {
        start(false);
    }

    protected Action[] getActions() {
        Action[] actions = new Action[]{
            new StartServerAction.OutputAction(mInstance),
            new StartServerDebugAction.OutputAction(mInstance),
            new StopServerAction.OutputAction(mInstance)
        };
        return actions;
    }

    synchronized private static InputOutput getIO(String name, Action[] actions) {
        InputOutput io = null;
        if ( sIOMap == null ) {
            sIOMap = new HashMap<String, InputOutput>();
        }
        io = sIOMap.get(name);
        if ( io == null ) {
            IOProvider ioProvider = IOProvider.getDefault();
            io = ioProvider.getIO(name, actions);
            if ( io != null ) {
                sIOMap.put(name, io);
            }
        }
        return io;
    }

    protected InputOutput createConsoleIO() {
        String name = mInstance.getName();
        InputOutput io = getIO(name, getActions());
        return io;
    }

    public InputOutput getConsoleIO() {
        if (mIO == null) {
            mIO = createConsoleIO();
        }
        return mIO;
    }

    public InputOutput openConsole() {
        InputOutput io = getConsoleIO();
        if (io.isClosed()) {
            // remove io handler before recreating it.            
            if ( this.mIOHandler != null) {
                //TODO:  current handler should restart
//                LOG.info("#### Closing the current IO Hnadler to re-creating IOHandler for CLI input when io console is opened again.");
//                this.mIOHandler.stopHandlers();
//                this.mIOHandler = null;
            }
        }
        if (this.mIOHandler == null && this.mProcess != null) {
            String initCmd = this.getInitCommand();
            this.mIOHandler = new IOHandler(io, this.mProcess, initCmd);
        }
        io.select();
        return io;
    }
    /**
     *  override this and just return the creteInputOutput if both console
     *  and the server log io are same.
     * @return
     */
    protected InputOutput createServerLogIO() {
        String name = mInstance.getName();
        name = name + " - Log";
        IOProvider ioProvider = IOProvider.getDefault();
        // System.err.println("IOProvider : " + ioProvider.getClass().getName());
        return ioProvider.getIO(name, false);
    }

    public InputOutput getServerLogIO() {
        if (mServerLogIO == null) {
            mServerLogIO = createServerLogIO();
        }
        return mServerLogIO;
    }

    public InputOutput openServerLog() {
        InputOutput io = getServerLogIO();
        io.select();
        return io;
    }

    /**
     * override this if you have different server log path
     * @return
     */
    protected File getServerLogFile() {
        File serverLog = new File(this.mInstance.getRoot(), "logs/fuji.log"); //TODO: get it from conf properties
        return serverLog;
    }

    protected FujiLogWriter createLogWriter() {
        InputOutput io = getServerLogIO();
        return FujiLogWriter.createInstance(io, this.getFujiServerInstance().getName());
    }

    protected void stopLogWriter() {
        FujiLogWriter logWriter = createLogWriter();
        if (logWriter.isRunning()) {
            logWriter.stop();
        }
    }

    private void startLogWriter(final FujiLogWriter logWriter, final File logFile) {

        Runnable starter = new Runnable() {

            public void run() {
                int nTry = 0;
                while (true) {
                    if (nTry >= 10 || logWriter.isRunning()) {
                        return;
                    }
                    if (logFile.exists()) {
                        try {
                            logWriter.start(logFile);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        return;
                    } else {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            break;
                        }
                    }
                    ++nTry;
                }
            }
        };
        RequestProcessor.getDefault().post(starter, 5000, Thread.currentThread().getPriority());
    }

    protected void startLogWriter() {
        FujiLogWriter logWriter = createLogWriter();
        if (logWriter.isRunning()) {
            logWriter.stop();
        }
        File logFile = getServerLogFile();
        LOG.info("### Starting Fuji Server Log reader on" + logFile.getAbsolutePath());
        // logWriter.start(logFile);
        startLogWriter(logWriter, logFile);
    }

    protected final File getCWD() {
        File cwd = new File(this.mInstance.getWorkDir());
        return cwd;
    }

    protected final String getDefaultJavaExePath() {
        String cmdline = "";
        String javaHome = System.getProperty("java.home");
        if (javaHome != null) {
            File binDir = new File(javaHome, "bin");
            if (binDir.exists() && binDir.isDirectory()) {
                cmdline += binDir.getAbsolutePath() + File.separator;
            }
        }
        cmdline += "java";
        return cmdline;
    }

    protected final String getJavaExePath() {
        String javaExePath = "";
        JavaPlatform platform = JavaPlatform.getDefault();
        FileObject javaExeFO = platform.findTool("java");
        if (javaExeFO == null) {
            LOG.info("Cannot find the java.exe tool in java platform" + platform.getDisplayName());
            javaExePath = getDefaultJavaExePath();
        } else {
            LOG.info("Found java.exe tool in java platform " + javaExeFO.getPath());
            File javaExeFile = FileUtil.toFile(javaExeFO);
            javaExePath = javaExeFile.getAbsolutePath();
////            Map<String, String> sysProps = platform.getSystemProperties();
////            for (String sysProp : sysProps.keySet()) {
////                LOG.info(sysProp + "=" + sysProps.get(sysProp));
////            }
        }
        return javaExePath;
    }

    protected abstract String getMainJarPath();

    protected abstract List<String> getExecutableClassArgs();

    protected abstract List<String> getExecutableClass();

    protected abstract List<String> getJavaClassPath();

    protected abstract List<String> getJavaOptions();

    /**
     * override this if the process does not support vm options. e.g. for glassfish
     * debug option is to pass executable class args?
     * @return
     */
    protected List<String> getDebugOptions() {
        List<String> options = new ArrayList<String>();
        if (this.isDebugMode()) {
            if ( this.mInstance != null ) {
                String debugOption = this.mInstance.getDebugOption();
                if ( debugOption == null || debugOption.trim().length() == 0 ) {
                    debugOption = FujiServerInstance.DEFAULT_DEBUG_JVM_OPTION;
                }
                options.add(debugOption);
            }
        }
        return options;
    }

    protected ProcessBuilder createProcessBuilder() {

        List<String> cmdList = new ArrayList<String>();
        cmdList.add(getJavaExePath());
        cmdList.addAll(getJavaOptions());
        cmdList.addAll(getExecutableClass());
        cmdList.addAll(getExecutableClassArgs());

        ProcessBuilder processBuilder = new ProcessBuilder(cmdList);
        processBuilder.directory(getCWD());

        return processBuilder;
    }

    protected boolean isInstanceStarted() {
        return this.mInstanceStarted;
    }
    
    protected void setInstanceStarted(boolean started) {
        this.mInstanceStarted = started;
    }
    protected FujiServerInstance getFujiInstance() {
        return this.mInstance;
    }
    protected Process getProcess() {
        return this.mProcess;
    }
    
    protected abstract void waitFor();

    protected void startProcess() throws IOException {
        ProcessBuilder processBuilder = createProcessBuilder();
        this.mProcess = processBuilder.start();
        if (mProcess != null) {
            this.mInstanceStarted = true;
        }
    }

    protected void printStartMessage() {
        ProcessBuilder processBuilder = createProcessBuilder();
        List<String> cmds = processBuilder.command();
        InputOutput io = null;
        try {
            io = getConsoleIO();
            io.getOut().println("Starting Fuji instance [" + this.mInstance.getName() + "]....");
            StringBuffer outBuff = new StringBuffer();
            for (String cmd : cmds) {
                outBuff.append(cmd).append(" "); //XXX

            }
            LOG.info(outBuff.toString());
            io.getOut().println();
            io.getOut().println("cwd=" + processBuilder.directory().getAbsolutePath());
            io.getOut().println(); //XXXd
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void printStopMessage() {
        InputOutput io = null;
        io = getConsoleIO();
        String statusMsg = "Fuji instance [" + this.mInstance.getName() + "] stopped.";
//        int exitValue = mProcess.exitValue();
//        if (exitValue != 0) {
//            statusMsg += " [exit code=" + exitValue + "]";
//        }
        if ( io != null ) {
            io.getOut().println(statusMsg);
        }
        StatusDisplayer.getDefault().setStatusText(statusMsg);
    }

    protected void launchProcess() throws IOException {

        if (EventQueue.isDispatchThread()) {
            throw new IllegalStateException("FujiServerProcess cannot be called from EDT");
        }

        startProcess();

        printStartMessage();

//        List<String> cmds = processBuilder.command();
//
//        InputOutput io = null;
//        try {
//            io = getConsoleIO();
//            io.getOut().println("Starting Fuji instance [" + this.mInstance.getName() + "]....");
//            StringBuffer outBuff = new StringBuffer();
//            for (String cmd : cmds) {
//                outBuff.append(cmd).append(" "); //XXX
//
//            }
//            LOG.info(outBuff.toString());
//            io.getOut().println();
//            io.getOut().println("cwd=" + processBuilder.directory().getAbsolutePath());
//            io.getOut().println(); //XXXd
//
//            openConsole();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

        openConsole();
        refreshGUI(); //TODO refresh after some time in request processor.
        startLogWriter();
        waitFor();
        stopLogWriter();

        LOG.info("FUJI Process end");
        if (this.mIOHandler != null) {
            this.mIOHandler.stopHandlers();
            this.mIOHandler = null;
        }

        printStopMessage();
//        String statusMsg = "Fuji instance [" + this.mInstance.getName() + "] stopped.";
////        int exitValue = mProcess.exitValue();
////        if (exitValue != 0) {
////            statusMsg += " [exit code=" + exitValue + "]";
////        }
//        io.getOut().println(statusMsg);
//        StatusDisplayer.getDefault().setStatusText(statusMsg);


        this.mProcess = null;
        this.mInstanceStarted = false;
        this.mIOHandler = null;
        try {
            this.mInstance.refresh();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    public void run() {
//        File cwd = this.getCWD();
//        String cmdLine = this.getCommandLine();
        try {
            this.launchProcess();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void pingServer() {
       if ( mIOHandler != null ) {
           mIOHandler.writeInput("\n");
       }
    }
    
    private void refreshGUI() {
        Runnable refresher = new Runnable() {

            public void run() {
                int nTry = 0;
                while (true) {
                    if (nTry >= 10 || mProcess == null || mInstance == null) {
                        return;
                    }
                    if (mInstance.isJMXAdminAvailable()) {
                        try {
                            mInstance.refresh();
                        } catch (Exception ex) {
                            Exceptions.printStackTrace(ex);
                        }
                        return;
                    } else {
                        pingServer();
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            break;
                        }
                    }
                    ++nTry;
                }
            }
        };
        RequestProcessor.getDefault().post(refresher, 5000, Thread.currentThread().getPriority());
    }

    public void propertyChange(PropertyChangeEvent evt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
    
