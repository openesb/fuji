/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.netbeans.modules.jbi.fuji.server.plugin.util.UiUtils;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.util.actions.NodeAction;

/**
 *
 * @author chikkala
 */
public class StartServerAction extends NodeAction {

    private static final String CONNECT_OPTION = "Connect";
    private static final String RESTART_OPTION = "Restart";

    protected static Logger getLogger() {
        return Logger.getLogger(StartServerAction.class.getName());
    }

    @Override
    protected void performAction(Node[] nodes) {
        if (nodes == null || nodes.length != 1) {
            getLogger().fine("no node is selected for starting the fuji server");
            return;
        }
        FujiServerInstance instance = nodes[0].getLookup().lookup(FujiServerInstance.class);
        startInstance(instance); // run it in a separate thread.
    }

    private static Object showConnectOrRestartDialog(FujiServerInstance instance) {
        boolean continueStart = false;
        String msg = NbBundle.getMessage(StartServerAction.class, "start.server.action.confirm.start.options.msg");
        NotifyDescriptor configDialog = new NotifyDescriptor.Confirmation(msg);
        configDialog.setOptions(new Object[] {RESTART_OPTION, CONNECT_OPTION, NotifyDescriptor.CANCEL_OPTION});
        Object result = DialogDisplayer.getDefault().notify(configDialog);
        return result;
    }

    public static void startInstance(final FujiServerInstance instance) {
        if (instance == null) {
            getLogger().fine("fuji server instance is NULL");
            return;
        }
        if (instance.isStarted()) {
            getLogger().fine("fuji server instance " + instance.getName() + " already started");
            return;
        }
        //TODO: start the server.
        getLogger().info("Starting the server instance " + instance.getName());
        
        Runnable startThread = new Runnable() {
            public void run() {
                try {
                    if (instance.isJMXAdminAvailable()) {
                        // already running an instance with this method. display notify dialog and return.
                        Object result = showConnectOrRestartDialog(instance);
                        if (CONNECT_OPTION.equals(result)) {
                            instance.connect();
                        } else if (RESTART_OPTION.equals(result)) {
                            instance.shutdownRemoteInstance();
                            instance.start();
                        } else {
                            // do nothing.
                        }
                    } else {
                        instance.start();
                    }
                } catch (Exception ex) {
                    // Exceptions.printStackTrace(ex);
                    UiUtils.showMessage(ex);
                }
            }         
        };
        RequestProcessor.getDefault().post(startThread, 0, Thread.currentThread().getPriority());        
        
    }

    @Override
    protected boolean enable(Node[] nodes) {
        if (nodes != null && nodes.length == 1) {
            FujiServerInstance instance = nodes[0].getLookup().lookup(FujiServerInstance.class);
            return ((instance != null) && (!instance.isStarted()));
        }
        return true;
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(StartServerAction.class, "LBL_StartServerAction");
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(StartServerAction.class);
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    /** This action will be displayed in the server output window */
    public static class OutputAction extends AbstractAction implements ChangeListener {

        private static final String ICON =
            "org/netbeans/modules/jbi/fuji/server/plugin/resources/start.png"; // NOI18N
        private static final String PROP_ENABLED = "enabled"; // NOI18N
        private final FujiServerInstance instance;

        public OutputAction(FujiServerInstance instance) {
            super(NbBundle.getMessage(StartServerAction.class, "LBL_StartServerOutputAction"),
                new ImageIcon(ImageUtilities.loadImage(ICON)));
            putValue(SHORT_DESCRIPTION, NbBundle.getMessage(StartServerAction.class, "LBL_StartServerOutputActionDesc"));
            this.instance = instance;

            // start listening to changes
            ChangeListener cl = WeakListeners.change(this, this.instance);
            this.instance.addChangeListener(cl);
        }

        public void actionPerformed(ActionEvent e) {
            startInstance(instance);
        }

        @Override
        public boolean isEnabled() {
            return ((instance != null) && (!instance.isStarted()));
        }
        // ServerInstance.StateListener implementation --------------------------
        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == instance) {
                Mutex.EVENT.readAccess(new Runnable() {

                    public void run() {
                        firePropertyChange(
                            PROP_ENABLED,
                            null,
                            isEnabled() ? Boolean.TRUE : Boolean.FALSE);
                    }
                });
            }
        }
    }
}
