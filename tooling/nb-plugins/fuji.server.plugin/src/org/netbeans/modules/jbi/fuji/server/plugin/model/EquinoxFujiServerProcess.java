/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.jbi.fuji.server.plugin.io.FujiLogWriter;

public class EquinoxFujiServerProcess extends AbstractFujiServerProcess {

    public EquinoxFujiServerProcess(FujiServerInstance instance) {
        super(instance);
    }

    protected String getInitCommand() {
        return "\nss\n";
    }

    protected String getMainJarPath() {
        File rootDir = new File(getFujiServerInstance().getRoot());
        File mainJarPath = new File(rootDir, "equinox.jar");
        return mainJarPath.getAbsolutePath();
    }

    protected List<String> getExecutableClassArgs() {
        List<String> classArgs = new ArrayList<String>();
        classArgs.add("-console");
        return classArgs;
    }

    protected List<String> getExecutableClass() {
        List<String> options = new ArrayList<String>();
        String mainJarPath = getMainJarPath();
        options.add("-jar");
        // check if the jar path has spaces. if yes, include it in quotes. only works on windows.
        // does not work on mac os x .
        if (mainJarPath.contains(" ")) {
            mainJarPath = "\"" + mainJarPath + "\"";
        }
        options.add(mainJarPath);
////        String mainClass = "org.apache.felix.main.Main";
////        options.add(mainClass);
        return options;
    }

    protected List<String> getJavaClassPath() {
        List<String> options = new ArrayList<String>();
////        options.add("-classpath");
////        String mainJarPath = getMainJarPath();
////        if (mainJarPath.contains(" ")) {
////            mainJarPath = "\"" + mainJarPath + "\"";
////        }
////        options.add(mainJarPath);
        return options;
    }

    protected List<String> getJavaOptions() {
        List<String> options = new ArrayList<String>();
        
        options.addAll(getDebugOptions());

        String adminPort = getFujiServerInstance().getAdminPort();
        if (adminPort != null && adminPort.trim().length() == 0) {
            adminPort = null;
        } else {
            adminPort = adminPort.trim();
        }
        if (adminPort != null) {
            options.add("-D" + CONNECTOR_PORT + "=" + adminPort);
        }
        // add classpath option. see getExecutableClass for details.
        options.addAll(getJavaClassPath());

        return options;
    }

    protected void waitFor() {
        Process process = this.getProcess();
        if (process != null) {
            try {
                //Hang this thread until the process exits
                process.waitFor();
            } catch (InterruptedException ex) {
                //  ErrorManager.getDefault().notify(ex);
                ex.printStackTrace();
            }
        }
    }
    
    public boolean isStarted() {
        return getProcess() != null && this.isInstanceStarted();
    }
    
    public void stop() throws Exception {
        this.getFujiServerInstance().shutdownRemoteInstance();
        // force.  since equinox uninstall main bundle does not shutdown the process.
        if (this.getProcess() != null) {
            this.getProcess().destroy();
        }
    }
}
