/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OSGiServiceInfo.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

/**
 * Bean that represents the osgi service info and has a serialization/deserialization 
 * code to/from jmx open types that represent the data.
 * 
 * @author chikkala
 */
public class OSGiServiceInfo {
    // property names
    public static final String PROP_BUNDLE = "bundle";
    public static final String PROP_ID = "id";
    public static final String PROP_DESCRIPTION = "description";
    public static final String PROP_OBJECT_CLASSES = "objectClasses";
    public static final String PROP_USING_BUNDLES = "usingBundles";
    public static final String PROP_PROPERTIES = "properties";
    // types
    public static final String[] ITEM_NAMES = {
        PROP_BUNDLE,
        PROP_ID,
        PROP_DESCRIPTION,
        PROP_OBJECT_CLASSES,
        PROP_USING_BUNDLES,
        PROP_PROPERTIES
    };
    // property values
    private String bundle;
    private String id;
    private String description;
    private String[] objectClasses;
    private String[] usingBundles;
    private OSGiPropertyInfoMap properties;

    public OSGiServiceInfo() {
        this.objectClasses = new String[0];
        this.usingBundles = new String[0];
        this.properties = new OSGiPropertyInfoMap();
    }

    public String getBundle() {
        return bundle;
    }

    public void setBundle(String bundle) {
        this.bundle = bundle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getObjectClasses() {
        return objectClasses;
    }

    public void setObjectClasses(String[] objectClasses) {
        if (objectClasses != null) {
            this.objectClasses = objectClasses;
        }
    }

    public String[] getUsingBundles() {

        return usingBundles;
    }

    public void setUsingBundles(String[] usingBundles) {
        if (usingBundles != null) {
            this.usingBundles = usingBundles;
        }
    }
    public List<OSGiPropertyInfo> getProperties() {
        return this.properties.getProperties();
    }

    public void setProperties(List<OSGiPropertyInfo> properties) {
        this.properties.setProperties(properties);
    }
    public Map<String, OSGiPropertyInfo> getPropertiesMap() {
        return this.properties.getPropertiesMap();
    }
    public OSGiPropertyInfo getProperty(String name) {
        return this.properties.getProperty(name);
    }
    public String getPropertyValue(String name) {
        return this.properties.getPropertyValue(name);
    }
    public String print() {
        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(writer);
        out.println("--- OSGI Service Reference ----");
        out.println("Bundle: " + getBundle());
        out.println("Id: " + getId());
        out.println("Desc: " + getDescription());
        out.println("Object Classes:");
        for (String clazz : getObjectClasses()) {
            out.println("  " + clazz);
        }
        out.println("Using Bundles:");
        for (String usingBundle : getUsingBundles()) {
            out.println("  " + usingBundle);
        }
        out.println("--- OSGI Service Reference ----");
        out.close();
        return writer.getBuffer().toString();
    }

    @Override
    public String toString() {
        return print();
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiServiceInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemDescs = ITEM_NAMES;
        ArrayType objectClassesType = new ArrayType(1, SimpleType.STRING);
        ArrayType usingBundlesType = new ArrayType(1, SimpleType.STRING);
        TabularType propertiesType = OSGiPropertyInfo.getTabularType();

        OpenType[] itemTypes = {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            objectClassesType,
            usingBundlesType,
            propertiesType
        };
        
        type = new CompositeType(typeName, typeDesc, ITEM_NAMES, itemDescs, itemTypes);
        return type;
    }

    public CompositeData toCompositeData() throws OpenDataException {
        CompositeData data = null;
        CompositeType type = getCompositeType();
        TabularData propertiesData = OSGiPropertyInfo.toTabularData(this.getProperties());
        Object[] itemValues = {
            this.getBundle(),
            this.getId(),
            this.getDescription(),
            this.getObjectClasses(),
            this.getUsingBundles(),
            propertiesData
        };
        data = new CompositeDataSupport(type, ITEM_NAMES, itemValues);
        return data;
    }

    public static OSGiServiceInfo toOSGiServiceInfo(CompositeData data) throws OpenDataException {
        OSGiServiceInfo info = new OSGiServiceInfo();
        info.setBundle((String) data.get(PROP_BUNDLE));
        info.setId((String) data.get(PROP_ID));
        info.setDescription((String) data.get(PROP_DESCRIPTION));
        info.setObjectClasses((String[]) data.get(PROP_OBJECT_CLASSES));
        info.setUsingBundles((String[]) data.get(PROP_USING_BUNDLES));

        TabularData propData = (TabularData) data.get(PROP_PROPERTIES);
        List<OSGiPropertyInfo> propList = new ArrayList<OSGiPropertyInfo>();
        if (propData != null) {
            propList = OSGiPropertyInfo.toOSGiPropertyInfoList(propData);
        }
        info.setProperties(propList);

        return info;
    }

    public static List<OSGiServiceInfo> toOSGiServiceInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiServiceInfo> list = new ArrayList<OSGiServiceInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>)tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiServiceInfo info = OSGiServiceInfo.toOSGiServiceInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiServiceInfo> list) throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiServiceInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiServiceInfo.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiServiceInfo.ITEM_NAMES);
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiServiceInfo info : list) {
            tdata.put(info.toCompositeData());
        }
        return tdata;
    }
}
