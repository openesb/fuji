/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.mvn;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import org.netbeans.modules.jbi.fuji.server.plugin.FujiServerManager;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;

/**
 *
 * @author chikkala
 */
public class NbFujiSettings {

    private static final String NB_FUJI_SETTINGS = ".nb-fuji-settings";
    
    private static final String TARGET_SERVER_ROOT = "fuji.server.root";
    private static final String TARGET_SERVER_NAME = "fuji.server.name";
    
    private static final String START_ON_DEPLOY = "fuji.deploy.start.on.deploy";
    private static final String RE_DEPLOY = "fuji.deploy.redeploy";
    
    private Properties mProps = null;
    private File mSettingsFile = null;

    private NbFujiSettings() {
        this.mProps = new Properties();
    }

    private File getSettingsFile(File dir) throws IOException {
        if (!dir.exists()) {
            throw new IOException("Directory for Fuji settings not exists " + dir);
        }
        File file = new File(dir, NB_FUJI_SETTINGS);
        file.createNewFile();
        return file;
    }

    public static NbFujiSettings load(File dir) throws IOException {
        NbFujiSettings settings = new NbFujiSettings();
        settings.loadSettings(dir);
        return settings;
    }

    private void loadSettings(File dir) throws IOException {
        this.mSettingsFile = getSettingsFile(dir);
        FileInputStream inStream = null;
        try {
            inStream = new FileInputStream(this.mSettingsFile);
            this.mProps.load(inStream);
        } finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    public void save() throws IOException {
        
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(this.mSettingsFile);
            this.mProps.store(outStream, "");
        } finally {
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    
    public String getProperty(String prop, String defaultValue) {
        return this.mProps.getProperty(prop, defaultValue);
    }
    public String getProperty(String prop) {
        return this.mProps.getProperty(prop, "");
    }    
    public void setProperty(String prop, String value) {
        this.mProps.setProperty(prop, value);
    }
    
    public FujiServerInstance getTargetServerInstance() {
        String root = this.mProps.getProperty(TARGET_SERVER_ROOT);
        String name = this.mProps.getProperty(TARGET_SERVER_NAME);
        FujiServerInstance inst = null;
        inst = FujiServerManager.getInstance().findServerInstance(name);  
        return inst;
    }
    
    public void setTargetServerInstance(FujiServerInstance inst) {
        this.mProps.setProperty(TARGET_SERVER_ROOT, inst.getRoot());
        this.mProps.setProperty(TARGET_SERVER_NAME, inst.getPreferenceName());
        // save it here.
    }
    
    public boolean isStartOnDeploy() {
        boolean startOnDeploy = true; // default
        String value = this.mProps.getProperty(START_ON_DEPLOY, Boolean.toString(startOnDeploy));
        startOnDeploy = Boolean.valueOf(value);
        return startOnDeploy;
    }
    public void setStartOnDeploy(boolean startOnDeploy) {
        this.mProps.setProperty(START_ON_DEPLOY, Boolean.toString(startOnDeploy));
    }
    
    public boolean isReDeploy() {
        boolean reDeploy = true; // default;
        String value = this.mProps.getProperty(RE_DEPLOY, Boolean.toString(reDeploy));
        reDeploy = Boolean.valueOf(value);        
        return reDeploy;
    }
    
    public void setReDeploy(boolean reDeploy) {
        this.mProps.setProperty(RE_DEPLOY, Boolean.toString(reDeploy));
    }
    
}
