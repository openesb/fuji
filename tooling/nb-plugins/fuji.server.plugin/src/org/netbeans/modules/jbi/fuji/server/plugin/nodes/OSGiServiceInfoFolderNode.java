/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiServiceInfo;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class OSGiServiceInfoFolderNode extends AbstractNode {

    private static final Image FOLDER_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/folder.png", true); // NOI18N
    private static final Image BADGE_ICON = ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/server/plugin/resources/osgi-service-badge.png", true); // NOI18N
    public static final String FOLDER_KEY = "OSGiServiceInfoFolderNode";

    private FujiServerInstance mInstance;

    public OSGiServiceInfoFolderNode(FujiServerInstance instance) {
        super(new OSGiServiceInfoFolderNodeChildren(instance), Lookups.singleton(instance));
        this.mInstance = instance;
        this.setName(FOLDER_KEY);
    }

    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(OSGiServiceInfoFolderNode.class, "LBL_OSGiServiceFolderNode");
    }

    @Override
    public Image getIcon(int type) {
        Image img = computeIcon(false, type);
        return (img != null) ? img : super.getIcon(type);
    }

    @Override
    public Image getOpenedIcon(int type) {
        Image img = computeIcon(true, type);
        return (img != null) ? img : super.getIcon(type);
    }

    private Image computeIcon(boolean opened, int type) {
        Image badge = null;
        badge = BADGE_ICON;

        if (badge == null) {
            return FOLDER_ICON;
        } else {
            Image img = ImageUtilities.mergeImages(FOLDER_ICON, badge, 8, 8);
            return img;
        }
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] baseActions = super.getActions(context);
        List<Action> actions = new ArrayList<Action>();
        // actions.add(SystemAction.get(RefreshAction.class));
        actions.addAll(Arrays.asList(baseActions));
        return actions.toArray(new Action[actions.size()]);
    }

    private static final class OSGiServiceInfoFolderNodeChildren extends Children.Keys <OSGiServiceInfo>
        implements ChangeListener {

        private FujiServerInstance mInstance;

        public OSGiServiceInfoFolderNodeChildren(FujiServerInstance instance) {
            this.mInstance = instance;
            ChangeListener cl = WeakListeners.change(this, this.mInstance);
            this.mInstance.addChangeListener(cl);
        }
        private OSGiServiceInfo[] getSortedKeys(List<OSGiServiceInfo> list) {
            OSGiServiceInfo[] keys = new OSGiServiceInfo[0];
            List<OSGiServiceInfo> sortedList = new LinkedList<OSGiServiceInfo>();
            sortedList.addAll(list);
            try {
                Collections.sort(sortedList, new Comparator<OSGiServiceInfo>() {

                    public int compare(OSGiServiceInfo o1, OSGiServiceInfo o2) {
                        return o1.getObjectClasses()[0].compareTo(
                            o2.getObjectClasses()[0]);
                    }
                });
                keys = sortedList.toArray(new OSGiServiceInfo[0]);
            } catch (Exception ex) {
                keys = list.toArray(new OSGiServiceInfo[0]);
            }

            return keys;
        }
        private void updateKeys() {
            List<OSGiServiceInfo> list = new ArrayList<OSGiServiceInfo>();
            list = this.mInstance.listServiceRefs();
            OSGiServiceInfo[] keys = getSortedKeys(list);
            setKeys(keys);
        }

        protected Node[] createNodes(OSGiServiceInfo key) {
            OSGiServiceInfo info = (OSGiServiceInfo) key;
            return new Node[]{new OSGiServiceInfoNode(this.mInstance, info)};
        }

        @Override
        protected void addNotify() {
            super.addNotify();
            updateKeys();
        }

        @Override
        protected void removeNotify() {
            java.util.List<OSGiServiceInfo> emptyList = Collections.emptyList();
            setKeys(emptyList);
            super.removeNotify();
        }

        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source == this.mInstance) {
                updateKeys();
            }
        }
    }
}
