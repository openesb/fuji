/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.server.plugin.nodes;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import org.netbeans.modules.jbi.fuji.server.plugin.model.OSGiPropertyInfo;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;

public class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

    private OSGiPropertyInfo info;

    public ReadOnlyProperty(OSGiPropertyInfo info) {
        super(info.getName(), String.class, info.getName(), info.getName());
        this.info = info;
    }

    @Override
    public String getValue() throws IllegalAccessException, InvocationTargetException {
        return info.getValue();
    }
    
    public static SortedSet<OSGiPropertyInfo> createSortedSet(List<OSGiPropertyInfo> propList) {
        SortedSet<OSGiPropertyInfo> propSet = new TreeSet<OSGiPropertyInfo>(new Comparator<OSGiPropertyInfo>() {
            public int compare(OSGiPropertyInfo o1, OSGiPropertyInfo o2) {
                return o1.getName().compareTo(o2.getName());
            }          
        });
        propSet.addAll(propList);
        return propSet;
    }
    
    public static Sheet.Set createSheetSet(List<OSGiPropertyInfo> propList, String name, String displayName, String shortDescription) {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName(name);
        set.setDisplayName(displayName);
        set.setShortDescription(shortDescription);
        
//        for ( OSGiPropertyInfo propInfo : propList ) {
//           set.put(new ReadOnlyProperty(propInfo));
//        } 
        
        SortedSet<OSGiPropertyInfo> propSet = createSortedSet(propList);
        
        for ( OSGiPropertyInfo propInfo : propSet ) {
           set.put(new ReadOnlyProperty(propInfo));
        } 
        
        return set;        
    }
}
