/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.server.plugin.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.management.ObjectName;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.TabularData;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jbi.fuji.server.plugin.jmx.JMXManager;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;

/**
 *
 * @author chikkala
 */
public class AbstractFujiServerInstance implements FujiServerInstance {

    protected static final Logger LOG = Logger.getLogger(AbstractFujiServerInstance.class.getName());
    private String mPreferenceName;
    protected String mName;
    private String mRoot;
    private String mWorkDir;
    private String mAdminHost;
    private String mAdminPort;
    private String mAdminUser = "";
    private String mAdminPassword = "";
    private boolean mStopOnIdeExit = true;
    private String mDebugOption= "";
    private ChangeSupport mChangeSupport;
    private PropertyChangeSupport mPropSupport;
    private JMXManager mJmxMgr;
    private List<OSGiBundleInfo> mBundleList;
    private List<OSGiServiceInfo> mServiceList;
    private List<OSGiConfigInfo> mConfigList;
    private FujiServerProcess mProcess;

    private AbstractFujiServerInstance() {
        this(null);
    }

    protected AbstractFujiServerInstance(Preferences prefs) {
        this.mPreferenceName = prefs.name();
        this.mChangeSupport = new ChangeSupport(this);
        this.mPropSupport = new PropertyChangeSupport(this);
        this.mJmxMgr = new JMXManager();
        this.mBundleList = new ArrayList<OSGiBundleInfo>();
        this.mServiceList = new ArrayList<OSGiServiceInfo>();
        this.mConfigList = new ArrayList<OSGiConfigInfo>();

        this.load(prefs);
        // create process after the prefs are loaded.
        this.mProcess = AbstractFujiServerProcess.newFujiServerProcess(this);

    }

    /**
     * Get the value of adminPort
     *
     * @return the value of adminPort
     */
    public String getAdminPort() {
        return mAdminPort;
    }

    /**
     * Set the value of adminPort
     *
     * @param adminPort new value of adminPort
     */
    public void setAdminPort(String adminPort) {
        this.mAdminPort = adminPort;
    }

    public String getAdminPassword() {
        return mAdminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.mAdminPassword = adminPassword;
    }

    public String getAdminUser() {
        return mAdminUser;
    }

    public void setAdminUser(String adminUser) {
        this.mAdminUser = adminUser;
    }

    /**
     * Get the value of preferenceName
     *
     * @return the value of preferenceName
     */
    public String getPreferenceName() {
        return mPreferenceName;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return mName;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * Get the value of workDir
     *
     * @return the value of workDir
     */
    public String getWorkDir() {
        return mWorkDir;
    }

    /**
     * Set the value of workDir
     *
     * @param workDir new value of workDir
     */
    public void setWorkDir(String workDir) {
        this.mWorkDir = workDir;
    }

    /**
     * Get the value of root
     *
     * @return the value of root
     */
    public String getRoot() {
        return mRoot;
    }

    /**
     * Set the value of root
     *
     * @param root new value of root
     */
    public void setRoot(String root) {
        this.mRoot = root;
    }

    public boolean getStopOnIdeExit() {
        return this.mStopOnIdeExit;
    }

    public void setStopOnIdeExit(boolean stopOnIdeExit) {
        this.mStopOnIdeExit = stopOnIdeExit;
    }

    public String getDebugOption() {
        return this.mDebugOption;
    }

    public void setDebugOption(String debugOption) {
        this.mDebugOption = debugOption;
    }

    @Override
    public String toString() {

        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(writer);
        out.println("--Server Instance--");
        out.println("Pref Name: " + this.getPreferenceName());
        out.println(" Name: " + this.getName());
        out.println(" Root: " + this.getRoot());
        out.println(" WorkDir: " + this.getWorkDir());
        out.println(" AdminPort: " + this.getAdminPort());
        out.println(" AdminUser: " + this.getAdminUser());
        String password = this.getAdminPassword();
        if (password != null && password.trim().length() > 0) {
            password = "****";
        } else {
            password = "";
        }
        out.println(" AdminPassword: " + password);
        out.println(" StopOnIdeExit: " + this.getStopOnIdeExit());
        out.println(" DebugOption: " + this.getDebugOption());
        out.println("-------------------");
        out.close();
        return writer.getBuffer().toString();

    }

    public void save(Preferences prefs) {
        prefs.put(PROP_NAME, getName());
        prefs.put(PROP_ROOT, getRoot());
        prefs.put(PROP_WORK_DIR, getWorkDir());
        prefs.put(PROP_ADMIN_PORT, getAdminPort());
        prefs.put(PROP_ADMIN_USER, getAdminUser());
        prefs.put(PROP_ADMIN_PASSWORD, getAdminPassword());
        prefs.putBoolean(PROP_STOP_ON_IDE_EXIT, getStopOnIdeExit());
        prefs.put(PROP_DEBUG_OPTION, getDebugOption());
        
        this.mJmxMgr.setPort(getAdminPort());
        this.mJmxMgr.setUsername(getAdminUser());
        this.mJmxMgr.setPassword(getAdminPassword());

        boolean needRestart = false;
        if (this.mProcess != null && this.mProcess.isStarted()) {
            needRestart = true;
            try {
                this.mProcess.stop();
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        this.mProcess = AbstractFujiServerProcess.newFujiServerProcess(this);
        if (needRestart) {
            try {
                this.mProcess.start();
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    public void load(Preferences prefs) {
        setName(prefs.get(PROP_NAME, ""));
        setRoot(prefs.get(PROP_ROOT, ""));
        setWorkDir(prefs.get(PROP_WORK_DIR, ""));
        setAdminPort(prefs.get(PROP_ADMIN_PORT, ""));
        setAdminUser(prefs.get(PROP_ADMIN_USER, ""));
        setAdminPassword(prefs.get(PROP_ADMIN_PASSWORD, ""));
        setStopOnIdeExit(prefs.getBoolean(PROP_STOP_ON_IDE_EXIT, true));
        setDebugOption(prefs.get(PROP_DEBUG_OPTION, ""));
        
        this.mJmxMgr.setPort(getAdminPort());
        this.mJmxMgr.setUsername(getAdminUser());
        this.mJmxMgr.setPassword(getAdminPassword());
    }

    public static AbstractFujiServerInstance newInstance(Preferences prefs) {
        AbstractFujiServerInstance instance = new AbstractFujiServerInstance(prefs);
        return instance;
    }

    public void addChangeListener(ChangeListener l) {
        this.mChangeSupport.addChangeListener(l);
    }

    protected void fireChange() {
        this.mChangeSupport.fireChange();
    }

    public void removeChangeListener(ChangeListener l) {
        this.mChangeSupport.removeChangeListener(l);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        // new Exception("addPropertyChangeListener trace").printStackTrace();
//        if (l != null) {
//            LOG.info("Add PCL for instance " + l.getClass().getName());
//        } else {
//            LOG.info("Add PCL for instance NULL" );
//        }
        this.mPropSupport.addPropertyChangeListener(l);
    }

    protected void firePropertyChange(AbstractFujiServerInstance oldValue, AbstractFujiServerInstance newValue) {
//        LOG.info("Firing Property Change");
        PropertyChangeEvent evt = new PropertyChangeEvent(this,PROP_SERVER_INSTANCE,oldValue, newValue);
        this.mPropSupport.firePropertyChange(evt);
    }
    protected void fireServerStatePropertyChange(AbstractFujiServerInstance instance) {
        PropertyChangeEvent evt = new PropertyChangeEvent(this,PROP_SERVER_STATE, null, instance);
        this.mPropSupport.firePropertyChange(evt);
    }
    public void removePropertyChangeListener(PropertyChangeListener l) {
//        if (l != null) {
//            LOG.info("Removing PCL for instance " + l.getClass().getName());
//        } else {
//            LOG.info("Removing PCL for instance NULL");
//        }
        this.mPropSupport.removePropertyChangeListener(l);
    }

    public boolean isStarted() {
        return this.mProcess.isStarted();
    }

    public void connect() throws Exception {
        this.mProcess = new RemoteFujiServerProcess(this);
        start(false, true);
    }

    public void start() {
        start(false, false);
    }

    protected void start(boolean debug, boolean remote) {
        
        if (!remote && this.mProcess instanceof RemoteFujiServerProcess ) {
            // set that back to the process startup object
            this.mProcess = AbstractFujiServerProcess.newFujiServerProcess(this);
        }

        if (!isStarted()) {
            try {
                this.mProcess.start(debug);
                updateBundleList();
                this.mChangeSupport.fireChange();
                this.firePropertyChange(null, this);
                this.fireServerStatePropertyChange(this);
            } catch (Exception ex) {
                LOG.log(Level.FINE, null, ex);
                ex.printStackTrace();
            }
        }
    }
    
    public boolean isDebugging() {
        return this.mProcess.isDebugging();
    }

    public void startDebug() {
        start(true, false);
    }

    public void stop() {
        if (isStarted()) {
            try {
                this.mProcess.stop();
                clearBundleList();
                this.mChangeSupport.fireChange();
                this.firePropertyChange(null, this);
                this.fireServerStatePropertyChange(this);
            } catch (Exception ex) {
                LOG.log(Level.FINE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void refresh() {
        try {
            updateBundleList();
            this.mChangeSupport.fireChange();
            this.firePropertyChange(null, this);
            this.fireServerStatePropertyChange(this);
        } catch (Exception ex) {
            LOG.log(Level.FINE, null, ex);
            ex.printStackTrace();
        }
    }

    private List<OSGiBundleInfo> getBundleList() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        TabularData data = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "listBundles");
            data = (TabularData) result;
        } catch (Exception ex) {
            // ex.printStackTrace();
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        if (data != null) {
            try {
                list = OSGiBundleInfo.toOSGiBundleInfoList(data);
            } catch (OpenDataException ex) {
                ex.printStackTrace();
            }
        }
        return list;
    }

    /**
     * updateBundleList which modifies the list of objects obtained from the
     * server. This methods gets called from multiple threads during the
     * server startup that may corrupt the list. synchronizing at method level
     * to ensure that multiple thread (only at startup)  can populate the data
     * sequentially.
     */
    synchronized private void updateBundleList() {
        this.mBundleList.clear();
        this.mServiceList.clear();
        this.mConfigList.clear();

        List<OSGiBundleInfo> list = getBundleList();
        this.mBundleList.addAll(list);

        List<OSGiServiceInfo> slist = getServiceRefs();
        this.mServiceList.addAll(slist);

        List<OSGiConfigInfo> cList = getConfigurations();
        this.mConfigList.addAll(cList);
    }

    private void clearBundleList() {
        this.mBundleList.clear();
        this.mServiceList.clear();
        this.mConfigList.clear();
    }

    private List<OSGiBundleInfo> listBundles() {
        return this.mBundleList;
    }

    private List<OSGiServiceInfo> getServiceRefs() {
        List<OSGiServiceInfo> list = new ArrayList<OSGiServiceInfo>();
        TabularData data = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "listServiceRefs");
            data = (TabularData) result;
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        if (data != null) {
            try {
                list = OSGiServiceInfo.toOSGiServiceInfoList(data);
            } catch (OpenDataException ex) {
                ex.printStackTrace();
            }
        }
        return list;
    }

    public List<OSGiServiceInfo> listServiceRefs() {
        return this.mServiceList;
    }

    private List<OSGiConfigInfo> getConfigurations() {
        List<OSGiConfigInfo> list = new ArrayList<OSGiConfigInfo>();
        TabularData data = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "listConfigurations");
            data = (TabularData) result;
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        if (data != null) {
            try {
                list = OSGiConfigInfo.toOSGiConfigInfoList(data);
            } catch (OpenDataException ex) {
                ex.printStackTrace();
            }
        }
        return list;
    }

    public List<OSGiConfigInfo> listConfigurations() {
        return this.mConfigList;
    }

    public List<OSGiBundleInfo> listBindings() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> all = listBundles();
        for (OSGiBundleInfo info : all) {
            if (OSGiBundleInfo.JBI_BC.equals(info.getJbiArtifactType())) {
                list.add(info);
            }
        }
        return list;
    }

    public List<OSGiBundleInfo> listEngines() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> all = listBundles();
        for (OSGiBundleInfo info : all) {
            if (OSGiBundleInfo.JBI_SE.equals(info.getJbiArtifactType())) {
                list.add(info);
            }
        }
        return list;
    }

    public List<OSGiBundleInfo> listServiceAssemblies() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> all = listBundles();
        for (OSGiBundleInfo info : all) {
            if (OSGiBundleInfo.JBI_SA.equals(info.getJbiArtifactType())) {
                list.add(info);
            }
        }
        return list;
    }

    public List<OSGiBundleInfo> listSharedLibraries() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> all = listBundles();
        for (OSGiBundleInfo info : all) {
            if (OSGiBundleInfo.JBI_SL.equals(info.getJbiArtifactType())) {
                list.add(info);
            }
        }
        return list;
    }

    public List<OSGiBundleInfo> listOtherBundles() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        List<OSGiBundleInfo> all = listBundles();
        for (OSGiBundleInfo info : all) {
            if (null == info.getJbiArtifactType() || "Unknown".equalsIgnoreCase(info.getJbiArtifactType())) {
                list.add(info);
            }
        }
        return list;
    }

    public List<FujiInterceptorInfo> listInterceptors() {
        List<FujiInterceptorInfo> list = new ArrayList<FujiInterceptorInfo>();
        List<OSGiServiceInfo> serviceList = listServiceRefs();
        List<OSGiConfigInfo> configList = listConfigurations();
        list = FujiInterceptorInfo.createFujiInterceptorInfoList(serviceList, configList);
        return list;
    }

    public String installBundle(String bundlePath) throws Exception {
        return installBundle(bundlePath, true, true);
    }

    public String installBundle(String bundlePath, boolean start, boolean reInstall) throws Exception {
        LOG.info("Installing Bundle " + bundlePath + "With start= " + start + " reInstall=" + reInstall);
        Object result = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "installBundle", bundlePath, start, reInstall);
        } finally {
            updateBundleList();
            fireChange();
        }
        return (String) result;
    }

    public String startBundle(OSGiBundleInfo info) throws Exception {
        Long bundleId = Long.valueOf(info.getBundleId());
        LOG.info("Starting Bundle " + bundleId);
        Object result = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "startBundle", bundleId);
        } finally {
            updateBundleList();
            fireChange();
        }
        return (String) result;
    }

    public String stopBundle(OSGiBundleInfo info) throws Exception {
        Long bundleId = Long.valueOf(info.getBundleId());
        LOG.info("Stoping Bundle " + bundleId);
        Object result = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "stopBundle", bundleId);
        } finally {
            updateBundleList();
            fireChange();
        }
        return (String) result;
    }

    public String uninstallBundle(OSGiBundleInfo info) throws Exception {
        Long bundleId = Long.valueOf(info.getBundleId());
        LOG.info("Uninstall Bundle " + bundleId);
        Object result = null;
        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "uninstallBundle", bundleId);
        } finally {
            updateBundleList();
            fireChange();
        }
        return (String) result;
    }

    public void showConsoleWindow() {
        try {
            this.mProcess.openConsole();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showServerLogWindow() {
        try {
            this.mProcess.openServerLog();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isJMXAdminAvailable() {
        return this.mJmxMgr.isJMXAdminAvailable();
    }

    public boolean shutdownRemoteInstance() {
        boolean shutdown = false;
        if (isJMXAdminAvailable()) {
            try {
                ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
                this.mJmxMgr.invokeMBeanOperation(mbeanOName, "shutdown");
                shutdown = true;                // wait for some time.

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return shutdown;
    }

    public void printOutput(String msg) {
        try {
            this.mProcess.getConsoleIO().getOut().print(msg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getAdminHost() {
        return this.mAdminHost;
    }

    public void setAdminHost(String host) {
        this.mAdminHost = host;
    }

    public List<OSGiBundleInfo> listAllBundles() {
        return listBundles();
    }

    public String updateConfiguration(String pid, List<OSGiPropertyInfo> props) throws Exception {
        LOG.info("updating OSGi Cofiguration...");
        javax.management.openmbean.TabularData data = null;
//        try {
            ObjectName mbeanOName = this.mJmxMgr.getAdminMBeanObjectName();
            data = OSGiPropertyInfo.toTabularData(props);
            Object[] params = {pid, data};
            String[] signature = {"java.lang.String", "javax.management.openmbean.TabularData"};
            Object result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "updateConfiguration", params, signature);
//        } finally {
//            updateBundleList();
//            fireChange();
//        }
        return pid;
    }
    
    public static String updateConfigProperty(FujiServerInstance instance, String pid, OSGiPropertyInfo prop, String value) {
        String result = null;
        OSGiPropertyInfo updateProp = new OSGiPropertyInfo(prop.getName(), value);
        updateProp.setType(prop.getType());
        List<OSGiPropertyInfo> updatePropList = new ArrayList<OSGiPropertyInfo>();
        updatePropList.add(updateProp);
        LOG.info("Updating config prop name="+updateProp.getName() + " value="+updateProp.getValue());
        try {
            instance.updateConfiguration(pid, updatePropList);
            prop.setValue(value);
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
        return result;
    }
    public static String updateConfigProperty(FujiServerInstance instance, String pid, PropertyChangeEvent evt) {
            OSGiPropertyInfo prop = (OSGiPropertyInfo) evt.getOldValue();
            String value = (String) evt.getNewValue();
            return updateConfigProperty(instance, pid, prop, value);
    }

}
