/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.server.plugin.jmx;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 *
 * @author chikkala
 */
public class JMXManager implements PropertyChangeListener {

    private static Logger LOG = Logger.getLogger(JMXManager.class.getName());
    public static final String DEP_FUJI_ADMIN_OBJECT_NAME = "con.sun.jbi.fuji:runtime=OSGi,type=admin,service=admin";
    public static final String FUJI_JMX_DOMAIN_NAME = "com.sun.jbi.fuji";
    public static final String FUJI_ADMIN_OBJECT_NAME = FUJI_JMX_DOMAIN_NAME + ":runtime=OSGi,type=admin,service=admin";
    private ObjectName mAdminMBeanOName;
    private MBeanServerConnection mMBS;
    private JMXConnector mConnector;
    private String mHost;
    private String mPort;
    private String mUsername;
    private String mPassword;

    public JMXManager() {
        this("localhost", "8699", "", "");
    }

    public JMXManager(String port) {
        this("localhost", port, "", "");
    }

    public JMXManager(String port, String username, String password) {
        this("localhost", port, username, password);
    }

    public JMXManager(String host, String port, String username, String password) {
        this.mHost = host;
        this.mPort = port;
        this.mUsername = username;
        this.mPassword = password;
    }

    public String getHost() {
        return mHost;
    }

    public void setHost(String host) {
        this.mHost = host;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    public String getPort() {
        return mPort;
    }

    public void setPort(String port) {
        this.mPort = port;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public MBeanServerConnection getMBeanServerConnection() {
        return mMBS;
    }

    protected String getURL() {

        String host = getHost();
        String port = getPort();


        if (host == null) {
            host = "localhost";
        }
        if (port == null) {
            port = "8699";
        }

        String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
        return url;
    }

    protected Map<String, Object> getEnvironment() {
        Map<String, Object> env = new HashMap<String, Object>();
        String username = getUsername();
        String password = getPassword();
        if (username == null) {
            username = "";
        }
        if (password == null) {
            password = "";
        }
        // LOG.info("Connection Credentials user:" + username + " password:" + password.length());
        env.put(JMXConnector.CREDENTIALS, new String[]{username, password});
        return env;
    }

    /**
     * JMX Agent connection
     * 
     */
    protected void connect() throws Exception {
        String url = null;
        try {
            // Create JMX Agent URL
            url = getURL();
            JMXServiceURL jmxServiceURL = new JMXServiceURL(url);
            // Connect the JMXConnector
            mConnector = JMXConnectorFactory.connect(jmxServiceURL, getEnvironment());
            // Get the MBeanServerConnection
            mMBS = mConnector.getMBeanServerConnection();
        } catch (Exception ex) {
            LOG.info("Can not connect to FUJI Server with JMX Connetion URL " + url);
            throw ex;
        }
    }

    protected void close() throws Exception {

        //Close the connection
        try {
            mConnector.close();
        } finally {
            mConnector = null;
            mMBS = null;
        }

    }

    /**
     * assume that the jmx connection already established.
     */
    private ObjectName findAdminMBeanObjectName(boolean connect) {

        boolean available = false;
        ObjectName adminMBeanObjName = null;

        try {
            if ( connect ) {
                this.connect();
            }
        } catch (Exception ex) {
            return null; // can not connect.
        }

        try {
            adminMBeanObjName = new ObjectName(FUJI_ADMIN_OBJECT_NAME);
            available = this.getMBeanServerConnection().isRegistered(adminMBeanObjName);
            if (!available) {
                LOG.info(FUJI_ADMIN_OBJECT_NAME + " not found. trying to find the old admin mbean object name...");
                // test for admin mbean with old domain name
                adminMBeanObjName = new ObjectName(DEP_FUJI_ADMIN_OBJECT_NAME);
                available = this.getMBeanServerConnection().isRegistered(adminMBeanObjName);
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }

        if (!available) {
            adminMBeanObjName = null;
        }

        try {
            if ( connect ) {
                this.close();
            }
        } catch (Exception ex) {
            // ingore.
        }

        return adminMBeanObjName;
    }

    public ObjectName getAdminMBeanObjectName() {
        if (this.mAdminMBeanOName == null) {
            this.mAdminMBeanOName = findAdminMBeanObjectName(true);
        }
        return this.mAdminMBeanOName;
    }

    public boolean isJMXAdminAvailable() {
        boolean available = false;
        try {
            this.connect();
        } catch (Exception ex) {
            return false; // can not connect.
        }
        try {
            ObjectName adminMBeanObjName = findAdminMBeanObjectName(false);
            if ( adminMBeanObjName != null) {
                available = true;
            }
        } catch (Exception ex) {
            // ignore
        } finally {
            try {
                this.close();
            } catch (Exception ex) {
                // ingore.
            }
        }
        return available;
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName,
            Object[] params, String[] signature) throws Exception {
        Object resultObject = null;
        this.connect();
        try {
            resultObject = getMBeanServerConnection().invoke(objName, operationName, params, signature);
        } catch (MBeanException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            Exception targetEx = ex.getTargetException();
            if (targetEx == null) {
                targetEx = ex;
            }
            throw targetEx;
        } finally {
            try {
                this.close();
            } catch (Exception ex) {
                // ingore.
            }
        }
        return resultObject;
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName) throws Exception {
        Object[] params = new Object[0];
        String[] signature = new String[0];
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, String param) throws Exception {
        Object[] params = {param};
        String[] signature = {"java.lang.String"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, long param) throws Exception {
        Object[] params = {Long.valueOf(param)};
        String[] signature = {"long"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2) throws Exception {
        Object[] params = {param1, Boolean.valueOf(param2)};
        String[] signature = {"java.lang.String", "boolean"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2, boolean param3) throws Exception {
        Object[] params = {param1, Boolean.valueOf(param2), Boolean.valueOf(param2)};
        String[] signature = {"java.lang.String", "boolean", "boolean"};
        return invokeMBeanOperation(objName, operationName, params, signature);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
