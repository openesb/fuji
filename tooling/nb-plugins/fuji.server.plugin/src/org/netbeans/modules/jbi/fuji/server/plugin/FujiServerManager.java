/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.server.plugin;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.jbi.fuji.server.plugin.model.FujiServerInstance;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import org.netbeans.modules.jbi.fuji.server.plugin.model.AbstractFujiServerInstance;
import org.openide.util.Exceptions;

/**
 *
 * @author chikkala
 */
public class FujiServerManager {
    private static final Logger LOG = Logger.getLogger(FujiServerManager.class.getName());
    private static FujiServerManager sMgr;
    private Map<String, FujiServerInstance> mInstanceMap;
    
    PropertyChangeSupport mChangeSupport;
    
    protected FujiServerManager() {
        this.mInstanceMap = new HashMap<String, FujiServerInstance>();
        loadServerInstances();
        this.mChangeSupport = new PropertyChangeSupport(this);
    }
    
    public static synchronized FujiServerManager getInstance() {
        if ( sMgr == null ) {
            sMgr = new FujiServerManager();
        }
        return sMgr;
    }

    /**
     * used to stop all servers when IDE is exiting.
     */
    public static void stopServerInstancesOnIdeExit() {
        LOG.info("Stopping Fuji Server Instances....");
        if ( sMgr == null ) {
            return;
        }
        List<FujiServerInstance> instList = sMgr.listServerInstances();
        for ( FujiServerInstance instance : instList) {
            //TODO: check if the instance is started from the IDE &&
            // stopOnIDEExit true.
            if ( instance.isStarted() && instance.getStopOnIdeExit()) {
                try {
                    instance.stop();
                } catch (Exception ex) {
                    LOG.log(Level.INFO, ex.getMessage(), ex);
                }
            }
        }
    }

    protected void loadServerInstances() {
        List<FujiServerInstance> instanceList = FujiServerPreferences.loadServerInstances();
        for ( FujiServerInstance inst : instanceList ) {
            String root = inst.getRoot();
            if ( root != null && root.trim().length() > 0 && new File(root).exists() ) {
                this.mInstanceMap.put(inst.getPreferenceName(), inst);
            }
        }
    }
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.mChangeSupport.addPropertyChangeListener(l);
    }
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.mChangeSupport.removePropertyChangeListener(l);
    }
    protected void firePropertyChange(FujiServerInstance oldValue, FujiServerInstance newValue) {
        this.mChangeSupport.firePropertyChange(FujiServerInstance.PROP_SERVER_INSTANCE, oldValue, newValue);
    }
    public FujiServerInstance findServerInstance(String preferenceName) {
        return this.mInstanceMap.get(preferenceName);
    }
    public List<FujiServerInstance> listServerInstances() {
        List<FujiServerInstance> instanceList = new ArrayList<FujiServerInstance>();
        instanceList.addAll(this.mInstanceMap.values());
        return instanceList;
    }

    public Set<String> getServerInstanceNames() {
        Set<String> names = new HashSet<String>();
        for ( FujiServerInstance inst : this.mInstanceMap.values()) {
            names.add(inst.getName());
        }
        return names;
    }

    public Set<String> getServerInstancePorts() {
        Set<String> ports = new HashSet<String>();
        for ( FujiServerInstance inst : this.mInstanceMap.values()) {
            ports.add(inst.getAdminPort());
        }
        return ports;
    }

    public Set<String> getServerInstanceRoots() {
        Set<String> roots = new HashSet<String>();
        for ( FujiServerInstance inst : this.mInstanceMap.values()) {
            roots.add(inst.getRoot());
        }
        return roots;
    }

    public void saveServerInstance(FujiServerInstance instance) {
        FujiServerInstance oldInstance = this.mInstanceMap.put(instance.getPreferenceName(), instance);
        FujiServerPreferences.saveInstancePreferences(instance);
        FujiServerPreferences.setLastServerLocation(instance.getRoot());
        if ( oldInstance == null ) {
            // a new instance is added. so, fire the property change event.
            firePropertyChange(null, instance);
        }
        // notify change.
    }
    
    public FujiServerInstance newServerInstance() {
        Preferences prefs = FujiServerPreferences.newInstancePreferences();
        FujiServerInstance instance = AbstractFujiServerInstance.newInstance(prefs);
        return instance;
    }
    
    public void removeServerInstance(FujiServerInstance instance) {
        this.mInstanceMap.remove(instance.getPreferenceName());
        FujiServerPreferences.removeInstancePreferences(instance);
        firePropertyChange(instance, null);
    }
}
