/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.ifl.config;

import com.sun.jbi.fuji.maven.util.AppBuildManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;


/**
 *
 * @author chikkala
 */
public class ServiceConfigModel {
    private static final Logger LOG = Logger.getLogger(ServiceConfigModel.class.getName());
    private ServiceDescriptor mSD;
    private String mServiceName;
    private String mServiceType;

    private Configuration mDefConfig = null;
    private Configuration mCurrConfig = null;
    private Configuration mSelectedConfig = null;

    private ServiceConfigModel() {

    }
    
    private ServiceConfigModel(ServiceDescriptor sd, String serviceName) {
        this.mSD = sd;
        this.mServiceName = serviceName;
        this.mServiceType = this.mSD.getServiceType();
    }

    public ServiceDescriptor getServiceDescriptor() {
        return mSD;
    }

    public void setServiceDescriptor(ServiceDescriptor serviceDescriptor) {
        this.mSD = serviceDescriptor;
    }

    public String getServiceName() {
        return mServiceName;
    }

    public void setServiceName(String serviceName) {
        this.mServiceName = serviceName;
    }

    public String getServiceType() {
        return mServiceType;
    }

    public void setServiceType(String serviceType) {
        this.mServiceType = serviceType;
    }

    public Configuration getDefaultConfiguration() {
        if ( this.mDefConfig == null ) {
            this.mDefConfig = getServiceDescriptor().getDefaultConfiguration();
        }
        return this.mDefConfig;
    }

    public Configuration getCurrentConfiguration() {
        return this.mCurrConfig;
    }

    public void setSelectedConfiguration(Configuration config) {
        this.mSelectedConfig = config;
        //TODO: notify listeners that the selected configuration is changed.
    }

    public Configuration getSelectedConfiguration() {
        if ( this.mSelectedConfig == null ) {
            this.mSelectedConfig = this.getCurrentConfiguration();
            if ( this.mSelectedConfig == null) {
                this.mSelectedConfig = this.getDefaultConfiguration();
            }
        }
        return this.mSelectedConfig;
    }

    public Properties getSelectedConfigurationProperties() {
        Configuration config = getSelectedConfiguration();
        String configName = config.getName();
        Properties configProps = config.getProperties();
        configProps.setProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, configName);
        return configProps;
    }
    
    public List<Configuration> getAvailableConfigurations() {
        List<Configuration> configList = new ArrayList<Configuration>();
        configList = getServiceDescriptor().getConfigurations();
        Map<String, Configuration> configMap = new HashMap<String, Configuration>();
        this.mDefConfig = getServiceDescriptor().getDefaultConfiguration();
        for ( Configuration config : configList ) {
            configMap.put(config.getName(), config);
        }
        // add default configuration if not in the list.
        if ( configMap.get(this.mDefConfig.getName()) == null ) {
            configMap.put(this.mDefConfig.getName(), this.mDefConfig);
        }
        configList = new ArrayList<Configuration>();
        configList.addAll(configMap.values());
        return configList;
    }
    /**
     * load the existing configuration if exists.
     * @param appBuilder
     * @return the configuration mode for the existing configuraiton. null if no configuration exists 
     */
    public String loadCurrentConfiguration(AppBuildManager appBuilder) {
        LOG.info("### Trying to load existing configuration.... for " + this.mServiceName);
        String  currentMode = null;
        
        ServiceConfigurationHelper helper = new ServiceConfigurationHelper(this.mSD);
        Properties currentConfigProps = helper.loadConfigurationFromFile(appBuilder.getServiceProjectModel(this.mServiceType), this.mServiceName);
        if (currentConfigProps == null) {
            return null;
        }
        currentMode = currentConfigProps.getProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, null);
        List<Configuration> configList = getAvailableConfigurations();
      
        for ( Configuration config : configList ) {
            if (config.getName().equals(currentMode)) {
                this.mCurrConfig = config;
                List<Property> propList = config.getPropertyList();
                for (Property prop : propList ) {
                    String name = prop.getName();
                    String value = currentConfigProps.getProperty(name, null);
                    if ( value != null ) {
                        prop.setValue(value);
                    }
                }
                break;
            }
        }        
        // set the selected configuration to the existing configuration.
        if ( this.mCurrConfig != null) {
            this.setSelectedConfiguration(this.mCurrConfig);
        }
        return currentMode;
    }

    public static ServiceConfigModel newInstance(ServiceDescriptor sd, String serviceName, AppBuildManager appBuilder) {
        ServiceConfigModel model;
        model = new ServiceConfigModel(sd, serviceName);
        if ( appBuilder != null) {
            model.loadCurrentConfiguration(appBuilder);
        }
        return model;
    }

    private void dumpProperties(String msg, Properties props) {
        if (LOG.isLoggable(Level.FINE)) {
            if (props == null) {
                LOG.fine("---Null Props for -- " + msg);
                return;
            }
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            props.list(out);
            LOG.fine("----- " + msg);
            LOG.fine(writer.getBuffer().toString());
        }
    }
}
