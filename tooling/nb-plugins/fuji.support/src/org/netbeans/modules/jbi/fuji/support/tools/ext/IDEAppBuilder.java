/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.modules.jbi.fuji.support.tools.ext;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.Utils;
import org.netbeans.modules.jbi.fuji.support.mvn.MavenPlugin;
import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.maven.util.AppBuildManager;
import java.io.File;
import java.io.StringReader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.openide.loaders.DataObject;

/**
 *
 * @author chikkala
 */
public class IDEAppBuilder extends AppBuildManager {
    private static Logger  LOG = Logger.getLogger(IDEAppBuilder.class.getName());
    private MavenPlugin mPlugin;
    private IDEServiceToolsLocator mToolsLocator;

    public IDEAppBuilder(MavenPlugin plugin) {
        super(plugin.getProjectBaseDir(), plugin.getProjectArtifactId(), plugin.getFujiVersion());
        this.mPlugin = plugin;
         ToolsLookup lookup = new IDEToolsLookup(plugin);
//        ToolsLookup lookup = new DefaultToolsLookup();
        this.mToolsLocator = new IDEServiceToolsLocator(lookup, plugin);
    }

    public IDEServiceToolsLocator getToolsLocator() {
        return this.mToolsLocator;
    }
    
    /**
     * //TODO: should go into the app build manager.
     * //TODO: add a flag failOnInvalid to validate the IFL Model
     * reads the ifl file from the app.
     * @return
     * @throws Exception
     */
    public IFLModel getIFLModel() throws Exception {

        ApplicationProjectModel appPrj = this.getAppProjectModel();

        IFLModel prjIFLModel = null;

        File iflDir = new File(appPrj.getBaseDir(), ApplicationProjectModel.IFL_SRC_DIR);
        if (iflDir.exists()) {
            try {
                prjIFLModel = new IFLReader().read(iflDir);
            } catch (Exception ex) {
                LOG.log(Level.INFO, ex.getMessage(), ex);
            }
        }
        if (prjIFLModel == null) {
            // create empty.
            //TODO. Decide whether to fail or continue when we can not create the ifl model from the editing file.
            LOG.info("### Invalid IFL Source.  Creating an empty model");
            StringReader iflReader = new StringReader("# empty IFL model");
            prjIFLModel = new IFLReader().read(iflReader);
        }
        return prjIFLModel;
    }

    public void executeGenerateServiceArtifacts(String serviceType, String serviceName, Properties config) throws Exception {
         LOG.info("#### Generating service for " + serviceName + "[" + serviceType + "]");
         ServiceProjectModel servicePrjModel = this.getServiceProjectModel(serviceType);
         LOG.info("Service Project base dir " + servicePrjModel.getBaseDir().getAbsolutePath());
         IFLModel iflModel = getIFLModel();
         ServiceGenerator serviceGen = this.getToolsLocator().getServiceGenerator(serviceType);
         serviceGen.generateServiceArtifacts(servicePrjModel, iflModel, serviceName, config);
    }

    public void executeGenerateServiceConfiguration(ServiceDescriptor sd, String serviceName, Properties config) throws Exception {
         String serviceType = sd.getServiceType();
         LOG.info("#### saving Service Configuration " + serviceName + "[" + serviceType + "]");
         ServiceProjectModel servicePrjModel = this.getServiceProjectModel(serviceType);
         LOG.info("Service Project base dir " + servicePrjModel.getBaseDir().getAbsolutePath());
         IFLModel iflModel = getIFLModel();
         ServiceConfigurationHelper helper = new ServiceConfigurationHelper(sd);
         helper.generateServiceConfiguration(servicePrjModel, iflModel, serviceName, config);
    }

    public void executeGenerateGroupedServiceArtifacts(String serviceType, String groupName, List<String> providerList, List<String> consumerList) throws Exception {
          LOG.info("#### Generating service for group " + groupName + "[" + serviceType + "]");
         ServiceProjectModel servicePrjModel = this.getServiceProjectModel(serviceType);
         LOG.info("Service Project base dir " + servicePrjModel.getBaseDir().getAbsolutePath());
         IFLModel iflModel = getIFLModel();

        Map<String, Properties> providersConfigMap = createServiceConfigMap(servicePrjModel, iflModel, serviceType, providerList);
        Map<String, Properties> consumersConfigMap = createServiceConfigMap(servicePrjModel, iflModel, serviceType, consumerList);
        // invoke service artifacts generation.
        ServiceGenerator serviceGen = this.getToolsLocator().getServiceGenerator(serviceType);
        serviceGen.generateServiceArtifacts(servicePrjModel, iflModel, groupName, providersConfigMap, consumersConfigMap);
    }

    private Map<String, Properties> createServiceConfigMap(ServiceProjectModel servicePrjModel, IFLModel iflModel, String serviceType, List<String> services) throws Exception {
        Map<String, Properties> serviceConfigMap = new HashMap<String, Properties>();
        for (String service : services) {
            File configFile = new File(servicePrjModel.getConfigDir(service), ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
            if (!configFile.exists()) {
                throw new IOException("Service Configuration file not found for service " + service);
            }
            Properties configProps = Utils.readProperties(configFile);
            serviceConfigMap.put(service, configProps);
        }
        return serviceConfigMap;
    }

    public static IDEAppBuilder newInstance(DataObject iflDO) {
        MavenPlugin plugin = MavenPlugin.getMavenPlugin(iflDO);
        IDEAppBuilder appBuilder = new IDEAppBuilder(plugin);
        return appBuilder;
    }


}
