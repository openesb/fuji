/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.ifl.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.text.JTextComponent;
import org.netbeans.api.languages.ASTNode;
import org.netbeans.api.languages.ParseException;
import org.netbeans.api.languages.ParserManager;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport;

/**
 *
 * @author chikkala
 */
public abstract class IFLTextAction  extends BaseAction {

    private static final Logger LOG = Logger.getLogger(IFLTextAction.class.getName());

    public IFLTextAction() {
        super("IFLAction");
        putValue(Action.NAME, getName());
    }

    public abstract String getName();
    public abstract boolean isEnabled(ASTNode node, JTextComponent comp);
    public abstract void actionPerformed(ASTNode node, JTextComponent comp);

    @Override
    public final boolean isEnabled() {
        boolean isEnabled = false;
        JTextComponent comp = null;
        ASTNode node = null;

        comp = getTextComponent(null);
        node = getASTNode(comp);

        if ( comp != null && node != null ) {
            isEnabled = isEnabled(node, comp);
        } else {
            isEnabled = super.isEnabled();
        }
        return isEnabled;
    }

    @Override
    public final void actionPerformed(ActionEvent evt, JTextComponent target) {
        ASTNode node = getASTNode(target);
        if (node != null) {
            actionPerformed(node, target);
        }
    }

    protected final ASTNode getASTNode(JTextComponent comp) {
        ASTNode node = null;
        if ( comp != null ) {
           ParserManager mgr =  ParserManager.get(comp.getDocument());
           if (mgr != null ) {
                try {
                    node = mgr.getAST();
                } catch (ParseException ex) {
                    LOG.log(Level.INFO, ex.getMessage(), ex);
                }
           }
        }
        return node;
    }

    protected List<String> getServiceNames(ASTNode serviceNode, int offset) {
        List<String> serviceNames = new ArrayList<String>();
        String serviceGroupName = IFLSupport.getServiceName(serviceNode, true);
        String serviceName = null;
        serviceName = IFLSupport.getServiceNameInServiceGroup(serviceNode, offset, true);        
        if ( serviceName != null ) {
            serviceNames.add(serviceName);
        } else {
            Set<String> groupedServiceNames = IFLSupport.getGroupedServices(serviceNode, true);
            if (groupedServiceNames.size() > 0 ) {
                serviceNames.addAll(groupedServiceNames);
            } else {
                //group name is the service name.
                serviceNames.add(serviceGroupName);
            }
        }
        LOG.info("#### Service names at offset " + offset + " " + serviceNames);
        return serviceNames;
    }
}
