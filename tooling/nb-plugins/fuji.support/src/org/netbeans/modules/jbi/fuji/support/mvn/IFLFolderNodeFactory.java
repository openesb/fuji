/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.support.mvn;

import java.awt.Image;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

/**
 *
 * @author chikkala
 */
public class IFLFolderNodeFactory implements NodeFactory {

    private final static String IFL_FOLDER = "src/main/ifl";

    public IFLFolderNodeFactory() {
    }

    public NodeList createNodes(Project prj) {
        FileObject prjDirFO = prj.getProjectDirectory();
        FileObject iflDirFO = prjDirFO.getFileObject(IFL_FOLDER);
        if (iflDirFO == null) {
            return NodeFactorySupport.fixedNodeList();
        }

        DataFolder iflDirFolder = DataFolder.findFolder(iflDirFO);
        if (iflDirFolder == null) {
            return NodeFactorySupport.fixedNodeList();
        }

        Node originalNode = iflDirFolder.getNodeDelegate().cloneNode();
        Children nodeChildren = iflDirFolder.createNodeChildren(VisibleDataFilter.VISIBLE_DATA_FILTER);
        FilterNode nd = new IFLFolderFilterNode(originalNode, nodeChildren);
        
//        FilterNode nd = new IFLFolderFilterNode(iflDirFolder.getNodeDelegate());

        return NodeFactorySupport.fixedNodeList(nd);
    }

    public static class IFLFolderFilterNode extends FilterNode {

        private static final Image IFL_NODE_BADGE =
                ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/support/resources/ifl16.png", true); // NOI18N


        public IFLFolderFilterNode(Node original) {
            super(original);
            this.disableDelegation(DELEGATE_SET_DISPLAY_NAME | DELEGATE_GET_DISPLAY_NAME);
            this.setDisplayName("IFL Files");
        }

        public IFLFolderFilterNode(Node original, org.openide.nodes.Children children) {
            super(original, children);
            this.disableDelegation(DELEGATE_SET_DISPLAY_NAME | DELEGATE_GET_DISPLAY_NAME);
            this.setDisplayName("IFL Files");
        }

        @Override
        public Image getIcon(int type) {
            Image img = computeIcon(false, type);
            return (img != null) ? img : super.getIcon(type);
        }

        @Override
        public Image getOpenedIcon(int type) {
            Image img = computeIcon(true, type);
            return (img != null) ? img : super.getIcon(type);
        }

        private Image computeIcon(boolean opened, int type) {
            Image image = super.getIcon(type); // NOI18N

            image = ImageUtilities.mergeImages(image, IFL_NODE_BADGE, 6, 2);
            // image = Utilities.mergeImages(image, NB_PROJECT_BADGE, 8, 0);
            return image;
        }
    }
}
