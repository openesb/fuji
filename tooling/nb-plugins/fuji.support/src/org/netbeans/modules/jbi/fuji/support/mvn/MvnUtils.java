/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */


package org.netbeans.modules.jbi.fuji.support.mvn;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.netbeans.api.project.Project;
import org.netbeans.modules.maven.indexer.api.RepositoryIndexer;
import org.netbeans.modules.maven.indexer.api.RepositoryInfo;
import org.netbeans.modules.maven.indexer.api.RepositoryPreferences;
import org.netbeans.modules.maven.indexer.api.RepositoryQueries;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



/**
 *
 * @author chikkala
 */
public class MvnUtils {

    public static final String LOCAL_REPO_ID = "local";
    public static final String FUJI_REPO_ID = "open-esb";
    public static final String FUJI_REPO_URL= "http://download.java.net/maven/esb/";
    public static final String FUJI_GROUP_ID = "open-esb.fuji";
    public static final String FUJI_COMP_ARCHETYPES_GROUP_ID = "open-esb.fuji.components.archetypes";
    public static final String FUJI_COMP_PLUGINS_GROUP_ID = "open-esb.fuji.components.plugins";
    public static final String FUJI_COMP_INSTALLERS_GROUP_ID = "open-esb.fuji.components.installers";

    public static final String ARCHETYPE_SUFFIX = "-archetype";
    public static final String SERVICE_PLUGIN_SUFFIX = "service-plugin";
    
    public static final Logger LOG = Logger.getLogger(MvnUtils.class.getName());

    public static final String FUJI_PACKAGE_TYPE = "fuji-app";
    public static final String FUJI_PLUGIN_ID = "maven-fuji-plugin";

    private static DocumentBuilder docBuilder = null;
    private static XPathExpression jbiFrameworkDepXpathExpr1 = null;
    private static XPathExpression jbiFrameworkDepXpathExpr2 = null;

    private static XPathExpression mavenFujiPluginXpathExpr = null;
    
    public static MvnUtils UTILS = new MvnUtils();

    public static Set<String> findServiceTypesInMavenRepositories() {
        Set<String> serviceTypes = new HashSet<String>();
        Set<String> artifacts  = new HashSet<String>();
        artifacts = RepositoryQueries.getArtifacts(FUJI_COMP_ARCHETYPES_GROUP_ID, new RepositoryInfo[0]);
        if ( artifacts == null ) {
             LOG.info("Archetypes Set is NULL");
        } else {
            for (String artifact : artifacts ) {
//                LOG.info("Artifact: " + artifact);
                if ( artifact.endsWith(ARCHETYPE_SUFFIX)) {
                    int idx = artifact.lastIndexOf(ARCHETYPE_SUFFIX);
                    String serviceType = artifact.substring(0, idx);
                    serviceTypes.add(serviceType);
                } else if ( artifact.endsWith(SERVICE_PLUGIN_SUFFIX)) {
                    int idx = artifact.lastIndexOf(SERVICE_PLUGIN_SUFFIX);
                    String serviceType = artifact.substring(0, idx);
                    serviceTypes.add(serviceType);
                }
            }
        }
//        for ( String serviceType : serviceTypes) {
//            LOG.info("ServiceType: " + serviceType);
//        }
        return serviceTypes;
    }

    public static void updateRepositoryIndexes(boolean updateLocalIndex, boolean updateFujiIndex) {
        // LOG.info("updating repository indexes.....");
        RepositoryPreferences prefs = RepositoryPreferences.getInstance();
        if (updateLocalIndex ) {
           LOG.info("Updating the local repository....");
           RepositoryInfo localInfo =  prefs.getRepositoryInfoById(LOCAL_REPO_ID);
           RepositoryIndexer.indexRepo(localInfo);
        }
        if ( updateFujiIndex ) {
            LOG.info("Updating the fuji java.net repository....");
            RepositoryInfo fujiInfo =  prefs.getRepositoryInfoById(FUJI_REPO_ID);
            RepositoryIndexer.indexRepo(fujiInfo);
        }
    }

    private DocumentBuilder getDocumentBuidler() {
        if (docBuilder == null) {
            // check if the pom.xml contains the fuji maven plugin.
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            // domFactory.setNamespaceAware(true);
            domFactory.setNamespaceAware(false);
            try {
                docBuilder = domFactory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
        }
        return docBuilder;
    }

    private Document getPOMDocument(FileObject pomFO) {
        Document doc = null;
        try {
            doc = (getDocumentBuidler()).parse(FileUtil.toFile(pomFO));
        } catch (SAXException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        } catch (IOException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return doc;
    }

    public boolean findMavemFujiPlugin(Document pomDoc) {
        boolean foundMavenFujiPlugin = false;
        try {
            if (mavenFujiPluginXpathExpr == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                mavenFujiPluginXpathExpr = xpath.compile("/project/build/plugins/plugin[groupId='open-esb.fuji' and artifactId='maven-fuji-plugin']");
            }
            Object result = null;
            result = mavenFujiPluginXpathExpr.evaluate(pomDoc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;

            if (nodes != null && nodes.getLength() > 0) {
                foundMavenFujiPlugin = true;
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return foundMavenFujiPlugin;
    }

    public boolean findJbiFrameworkDependency(Document pomDoc) {
        boolean foundJbiFramework = false;
        try {
            if (jbiFrameworkDepXpathExpr1 == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                jbiFrameworkDepXpathExpr1 = xpath.compile("/project/dependencies/dependency[groupId='open-esb.fuji' and artifactId='jbi-framework']");
            }
            if (jbiFrameworkDepXpathExpr2 == null) {
                XPathFactory factory = XPathFactory.newInstance();
                XPath xpath = factory.newXPath();
                jbiFrameworkDepXpathExpr2 = xpath.compile("/project/dependencies/dependency[groupId='open-esb.fuji' and artifactId='framework']");
            }
            Object result = null;
            result = jbiFrameworkDepXpathExpr1.evaluate(pomDoc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            if ( nodes == null || nodes.getLength() == 0 ) {
                result = jbiFrameworkDepXpathExpr2.evaluate(pomDoc, XPathConstants.NODESET);
                nodes = (NodeList) result;
            }
            if (nodes != null && nodes.getLength() > 0) {
                foundJbiFramework = true;
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return foundJbiFramework;
    }

    public boolean isFujiPOM(FileObject pomFO) {
        boolean iappProject = false;
        try {
            Document doc = getPOMDocument(pomFO);
            if (doc != null) {
                if ( findMavemFujiPlugin(doc) || findJbiFrameworkDependency(doc)) {
                    iappProject = true;
                }
            } else {
                LOG.fine("FUJI POM XML DOC is NULL");
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return iappProject;
    }

    public boolean isFujiMavenProject(Project prj) {
        boolean fujiPrj = false;
        if (prj != null) {
            FileObject pomXml = prj.getProjectDirectory().getFileObject("pom.xml");
            if (pomXml != null) {
                fujiPrj = isFujiPOM(pomXml);
            }
        }
        return fujiPrj;
    }

}
