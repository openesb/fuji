/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.support.ifl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.languages.ASTItem;
import org.netbeans.api.languages.ASTNode;
import org.netbeans.api.languages.ASTPath;
import org.netbeans.api.languages.ASTToken;
import org.netbeans.api.languages.Language;
import org.netbeans.api.languages.LanguagesManager;
import org.netbeans.api.languages.ParseException;
import org.netbeans.api.languages.ParserManager;
import org.netbeans.api.languages.SyntaxContext;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.languages.Context;
import org.netbeans.api.languages.CompletionItem;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.jbi.fuji.support.ifl.actions.IFLGenerateAction;
import org.netbeans.modules.jbi.fuji.support.mvn.MvnUtils;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.EditCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.RequestProcessor;

/**
 * 
 * @author chikkala 
 */
public class IFLSupport {
    // tokens
    private static final String TKN_KEYWORD = "keyword";
    private static final String TKN_WHITESPACE = "whitespace";
    private static final String TKN_STRING = "string";
    private static final String TKN_IDENTIFIER = "identifier";
    private static final String TKN_OPERATOR = "operator";
    private static final String TKN_COMMENT = "comment";
    private static final String TKN_LIN_COMMENT = "line_comment";
    // keywords
    private static final String KW_ROUTE = "route";
    private static final String KW_BROADCAST = "broadcast";
    private static final String KW_FROM = "from";
    private static final String KW_TO = "to";
    private static final String KW_DO = "do";
    private static final String KW_END = "end";
    private static final String KW_SPLIT = "split";
    private static final String KW_AGGREGATE = "aggregate";
    private static final String KW_FILTER = "filter";
    private static final String KW_TEE = "tee";
    private static final String KW_WHEN = "when";
    private static final String KW_ELSE = "else";
    private static final String KW_SELECT = "select";
    // Non terminals
    private static final String NT_S = "S";
    private static final String NT_STMT = "Statement";
    private static final String NT_SERVICE_NAME = "ServiceName";
    private static final String NT_SERVICE_LIST = "ServiceList";
    private static final String NT_PROVIDER_SERVICE_LIST = "ProviderServiceList";
    private static final String NT_CONSUMER_SERVICE_LIST = "ConsumerServiceList";
    private static final String NT_SERVICE_STMT = "ServiceStatement";
    private static final String NT_TO_STMT = "ToStatement";
    private static final String NT_FROM_STMT = "FromStatement";
    private static final String NT_TO_ROUTE = "ToRoute";
    private static final String NT_TO_ENDPOINT = "ToEndpoint";
    private static final String NT_ROUTE_STMT_LINE = "RouteStatementLine";
    private static final String NT_ROUTE_STMT_BLOCK = "RouteStatementBlock";
    private static final String NT_NAMED_ROUTE_STMT = "NamedRouteStatement";
    private static final String NT_ROUTE_STMTS = "RouteStatemens";
    private static final String NT_BROADCAST_STMT = "BroadcastStatement";
    private static final String NT_FLOW_CONFIG = "FlowConfiguration";
    private static final String NT_SPLIT_STMT = "SplitStatement";
    private static final String NT_AGGREGATE_STMT = "AggregateStatement";
    private static final String NT_FILTER_STMT = "FilterStatement";
    private static final String NT_TEE_STMT = "TeeStatement";
    private static final String NT_WHEN_BLOCK = "WhenBlock";
    private static final String NT_ELSE_BLOCK = "ElseBlock";
    private static final String NT_SELECT_STMT = "SelectStatement";
    private static final String NT_CONFIG_FLOW_STMTS = "ConfigurableFlowStatements";
    private static final String NT_FLOW_STMTS = "FlowStatements";
    /** Keys used in Flow definition map */
    public enum FlowKeys {NAME, CONFIG_TYPE, CONFIG_VALUE, INLINE_CONFIG, INDEX, OFFSET};
    //
    private static final Logger LOG = Logger.getLogger(IFLSupport.class.getName());
    private static final String lineSeparator = System.getProperty("line.separator", "\n");
    /** service config file map for the service types which config file is not named as service.* */
    private static Map<String, String> serviceConfigFileMap = createServiceConfigFileMap();
    // code completion .........................................................
    // if the ide can not obtain the service types from maven, these are the default service types provided for code completion.
    // so, component developer don't need to update this list.
    private static String[] defaultServiceTypes = {
        "database",
        "file",
        "ftp",
        "http",
        "jms",
        "jruby",
        "rss",
        "email",
        "scheduler",
        "rest",
        "xmpp",
        "xslt",
        "bpel",
        "java"
    };
    private static String[] knownConfigProcessors = {
        "xpath",
        "java",
        "xsl",
        "regexp"
    };
    private static String[] knownRuleProcessors = {
        "xpath",
        "xsl",
        "regexp",
        "drools",
        "database"
    };

    private static boolean fetchingServiceTypes = false;
    
    private static Set<String> serviceTypes = new HashSet<String>();

    private static List<CompletionItem> emptyCList = new ArrayList<CompletionItem>();
    private static List<CompletionItem> keywordCList = defaultCompletionItems();
    private static List<CompletionItem> routeCList = routeBlockCompletionItems();
    private static List<CompletionItem> routeLineCList = routeLineCompletionItems();
    private static List<CompletionItem> toCList = toCompletionItems();
    private static List<CompletionItem> broadcastCList = broadcastCompletionItems();
    private static List<CompletionItem> doList = doCompletionItems();
    private static List<CompletionItem> endList = endCompletionItems();
    private static List<CompletionItem> serviceTypeCList = serviceTypeCompletionItems();
    private static List<CompletionItem> configProcessorsCList = configProcessorsCompletionItems();
    private static List<CompletionItem> ruleProcessorsCList = ruleProcessorsCompletionItems();
    private static List<CompletionItem> rootCList = rootCompletionItems();
    private static List<CompletionItem> selectCList = selectCompletionItems();

    public static Set<String> getKnownServiceTypes() {
        return serviceTypes;
    }
    
    public static void setKnownServiceTypes(Set<String> types) {
        serviceTypes  = new HashSet<String>();
        if ( types != null ) {
            serviceTypes.addAll(types);
        }
        serviceTypeCList = serviceTypeCompletionItems();
        rootCList = rootCompletionItems();
    }

    private static void updateKnownServiceTypes() {
        LOG.info("Updating service types....");

        RequestProcessor.getDefault().post(new Runnable() {

            public void run() {
                if (!fetchingServiceTypes) {
                    fetchingServiceTypes = true;
                    LOG.info("Finding service types from maven....");
                    Set<String> types = MvnUtils.findServiceTypesInMavenRepositories();
                    IFLSupport.setKnownServiceTypes(types);
                    fetchingServiceTypes = false;
                }
            }
        });
    }
    
    private static List<CompletionItem> rootCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.addAll(serviceTypeCList);
        result.add(CompletionItem.create(KW_ROUTE));
        return result;
    }

    private static List<CompletionItem> selectCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_WHEN));
        result.add(CompletionItem.create(KW_ELSE));
        return result;
    }

    private static List<CompletionItem> serviceTypeCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        SortedSet<String> sortedSet = new TreeSet<String>();
        Set<String> types = getKnownServiceTypes();
        if ( types == null || types.size() == 0 ) {
            updateKnownServiceTypes();
            types = getKnownServiceTypes();
        }
        types.addAll(Arrays.asList(defaultServiceTypes));
        sortedSet.addAll(types);
        for (String serviceType : sortedSet) {
            result.add(CompletionItem.create(serviceType));
        }
        return result;
    }

    private static List<CompletionItem> configProcessorsCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        SortedSet<String> sortedSet = new TreeSet<String>();
        sortedSet.addAll(Arrays.asList(knownConfigProcessors));
        for (String serviceType : sortedSet) {
            result.add(CompletionItem.create(serviceType));
        }
        return result;
    }

    private static List<CompletionItem> ruleProcessorsCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        SortedSet<String> sortedSet = new TreeSet<String>();
        sortedSet.addAll(Arrays.asList(knownRuleProcessors));
        for (String serviceType : sortedSet) {
            result.add(CompletionItem.create(serviceType));
        }
        return result;
    }

    private static List<CompletionItem> defaultCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_ROUTE));
        result.add(CompletionItem.create(KW_DO));
        result.add(CompletionItem.create(KW_FROM));
        result.add(CompletionItem.create(KW_TO));
        result.add(CompletionItem.create(KW_END));
        result.add(CompletionItem.create(KW_BROADCAST));
        result.add(CompletionItem.create(KW_SPLIT));
        result.add(CompletionItem.create(KW_AGGREGATE));
        result.add(CompletionItem.create(KW_FILTER));
        result.add(CompletionItem.create(KW_TEE));
        result.add(CompletionItem.create(KW_WHEN));
        result.add(CompletionItem.create(KW_ELSE));
        result.add(CompletionItem.create(KW_SELECT));
        return result;

    }

    private static List<CompletionItem> endCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_ROUTE));
        result.add(CompletionItem.create(KW_BROADCAST));
        return result;
    }

    private static List<CompletionItem> doCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_ROUTE));
        result.add(CompletionItem.create(KW_FROM));
        result.add(CompletionItem.create(KW_TO));
        result.add(CompletionItem.create(KW_END));
        result.add(CompletionItem.create(KW_BROADCAST));
        result.add(CompletionItem.create(KW_SPLIT));
        result.add(CompletionItem.create(KW_AGGREGATE));
        result.add(CompletionItem.create(KW_FILTER));
        return result;
    }

    private static List<CompletionItem> routeBlockCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_DO));
        result.add(CompletionItem.create(KW_FROM));
        result.add(CompletionItem.create(KW_TO));
        result.add(CompletionItem.create(KW_END));
        result.add(CompletionItem.create(KW_SPLIT));
        result.add(CompletionItem.create(KW_AGGREGATE));
        result.add(CompletionItem.create(KW_FILTER));
        result.add(CompletionItem.create(KW_BROADCAST));
        result.add(CompletionItem.create(KW_TEE));
        result.add(CompletionItem.create(KW_SELECT));
        return result;
    }

    private static List<CompletionItem> toCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_TO));
        result.add(CompletionItem.create(KW_SPLIT));
        result.add(CompletionItem.create(KW_AGGREGATE));
        result.add(CompletionItem.create(KW_FILTER));
        result.add(CompletionItem.create(KW_BROADCAST));
        result.add(CompletionItem.create(KW_TEE));
        result.add(CompletionItem.create(KW_SELECT));
        return result;
    }

    private static List<CompletionItem> routeLineCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_FROM));
        result.add(CompletionItem.create(KW_TO));
        return result;
    }

    private static List<CompletionItem> broadcastCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_ROUTE));
        result.add(CompletionItem.create(KW_DO));
        result.add(CompletionItem.create(KW_END));
        return result;
    }

    private static List<CompletionItem> teeCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
//        result.add(CompletionItem.create(KW_ROUTE));
//        result.add(CompletionItem.create(KW_DO));
//        result.add(CompletionItem.create(KW_END));
        //TODO populate with list of services
        return result;
    }

    private static List<CompletionItem> splitCompletionItems() {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.add(CompletionItem.create(KW_ROUTE));
        result.add(CompletionItem.create(KW_DO));
        result.add(CompletionItem.create(KW_END));
        return result;
    }

    private static void logDebug(Object obj) {
        LOG.fine(obj.toString());
    }
//    public static void myPrint1(String s) {
//      //  System.err.println(s);
//    }    

    public static List<CompletionItem> getPreviousStringLiterals(Context context) {
        Set<String> services = getDeclaredServices(context);
        TokenSequence ts = getTokenSequence(context);
        Set<String> names = new TreeSet<String>();
        List<CompletionItem> list = new ArrayList<CompletionItem>();
        int current = ts.index();
        try {
            do {
                Token token = previousToken(ts);
                if (token != null && token.id().name().endsWith(TKN_STRING)) {
                    names.add(token.text().toString());
                // list.add(CompletionItem.create(token.text().toString()));
                }
            } while (ts.index() > 0);
        } catch (Exception ex) {
            // ignore
        } finally {
            try {
                ts.moveIndex(current);
            } catch (Exception ex) {
            }
        }
        names.removeAll(services);
        for (String name : names) {
            list.add(CompletionItem.create(name));
        }
        return list;
    }

    public static List<CompletionItem> getPreviousStringLiterals(TokenSequence ts) {
        Set<String> names = new TreeSet<String>();
        List<CompletionItem> list = new ArrayList<CompletionItem>();
        int current = ts.index();
        try {
            do {
                Token token = previousToken(ts);
                if (token != null && token.id().name().endsWith(TKN_STRING)) {
                    names.add(token.text().toString());
                // list.add(CompletionItem.create(token.text().toString()));
                }
            } while (ts.index() > 0);
        } catch (Exception ex) {
            // ignore
        } finally {
            try {
                ts.moveIndex(current);
            } catch (Exception ex) {
            }
        }
        for (String name : names) {
            list.add(CompletionItem.create(name));
        }
        return list;
    }

//    public static void printTokenSequence(TokenSequence ts) {
//        myPrint("-------- Printing Token Seqence");
//        myPrint("Token count" + ts.tokenCount());
//        int current = ts.index();
//        myPrint("Token current index" + current);
//        try {
//        do {
//            Token token = previousToken(ts);
//            if ( token != null ) {
//            myPrint("Token:" + token);
//            myPrint("Token id :" + token.id().name());
//            }
//        } while( ts.index() > 0 ); 
//        } catch (Exception ex) {
//            
//        } finally {
//            try {
//            ts.moveIndex(current);
//            } catch (Exception ex ) {}
//        }
//        myPrint("-------- ");
//        
//    }

    public static Set<String> getGroupedServices(ASTNode serviceStmtASTNode, boolean trim) {
        Set<String> providerServices = new HashSet<String>();
        Set<String> consumerServices = new HashSet<String>();
        Set<String> services = new HashSet<String>();

        providerServices = getGroupedProviderServices(serviceStmtASTNode, trim);
        consumerServices = getGroupedConsumerServices(serviceStmtASTNode, trim);
        if ( providerServices.size() == 0 ) {
            // add group name as service
            String serviceName = serviceStmtASTNode.getTokenTypeIdentifier(TKN_STRING);
            if (serviceName != null) {
                serviceName = serviceName.trim();
                if (trim) {
                    serviceName = serviceName.substring(1, serviceName.length() - 1).trim();
                }
                services.add(serviceName);
            }
        }
        services.addAll(providerServices);
        services.addAll(consumerServices);
        
        return services;
    }

    public static Set<String> getGroupedServices(ASTNode serviceStmtASTNode, String nt, boolean trim) {
        Set<String> services = new HashSet<String>();
        List<ASTNode> serviceListNodes = new ArrayList<ASTNode>();
        findASTNodes(serviceStmtASTNode, nt, serviceListNodes);
        ASTNode serviceListASTNode = null;
        if ( serviceListNodes.size() > 0 ) {
            serviceListASTNode = serviceListNodes.get(0);
        }
        if (serviceListASTNode == null ) {
            return services;
        }
        List<ASTNode> serviceNameNodes = new ArrayList<ASTNode>();
        findASTNodes(serviceListASTNode, NT_SERVICE_NAME, serviceNameNodes);
        for (ASTNode serviceNameNode : serviceNameNodes) {
            String serviceName = serviceNameNode.getTokenTypeIdentifier(TKN_STRING);
            if (serviceName != null) {
                serviceName = serviceName.trim();
                if (trim) {
                    serviceName = serviceName.substring(1, serviceName.length() - 1).trim();
                }
                services.add(serviceName);
            }
        }
        return services;
    }

    public static Set<String> getGroupedProviderServices(ASTNode serviceStmtASTNode, boolean trim) {
        return getGroupedServices(serviceStmtASTNode, NT_PROVIDER_SERVICE_LIST, trim);
    }
    
    public static Set<String> getGroupedConsumerServices(ASTNode serviceStmtASTNode, boolean trim) {
        return getGroupedServices(serviceStmtASTNode, NT_CONSUMER_SERVICE_LIST, trim);
    }

    public static Set<String> getDeclaredServices(Context context) {
        Set<String> services = new HashSet<String>();
        ASTNode root = getASTRoot(context);
        if (root == null) {
            return services;
        }
        // get the group/service name list
        List<ASTNode> serviceStmtNodes = new ArrayList<ASTNode>();
        findASTNodes(root, NT_SERVICE_STMT, serviceStmtNodes);
        for (ASTNode serviceStmtNode : serviceStmtNodes) {
            Set<String> groupedServices = getGroupedServices(serviceStmtNode, false);
            if (groupedServices.size() > 0 ) {
                // this is a group statement. add grouped services to the list
                services.addAll(groupedServices);
                continue;
            }
            // else add the group name as service name.
            String serviceName = serviceStmtNode.getTokenTypeIdentifier(TKN_STRING);
            if (serviceName != null) {
                serviceName = serviceName.trim();
                // serviceName = serviceName.substring(1, serviceName.length() - 1).trim();
                services.add(serviceName);
            }
        }
        return services;
    }

    public static Set<String> getNamedRoutes(Context context) {
        Set<String> namedRoutes = new HashSet<String>();
        ASTNode root = getASTRoot(context);
        List<ASTNode> namedRouteASTNodes = new ArrayList<ASTNode>();
        if ( root != null) {
            findASTNodes(root, NT_NAMED_ROUTE_STMT, namedRouteASTNodes);
        }
        // LOG.info("Number of Named Routes Found " + namedRouteASTNodes.size());
        for ( ASTNode namedRouteNode : namedRouteASTNodes) {
               String routeName =  namedRouteNode.getTokenTypeIdentifier(TKN_STRING);
               namedRoutes.add(routeName);
        //       LOG.info(routeName);
        }

        return namedRoutes;
    }


    public static List<CompletionItem> getDeclaredServicesCompletionItems(Context context) {
        Set<String> declaredServices = getDeclaredServices(context);
        List<CompletionItem> list = new ArrayList<CompletionItem>();
        for (String service : declaredServices) {
            list.add(CompletionItem.create(service));
        }
        return list;
    }
    /**
     * return the service name portion of the service referecne. A service
     * reference can contain ns prefix (my:xyz) or ns ({http://abc}xyz ) and the operation name (zyz.myop)
     * @param serviceRef service refernce string
     * @return service name.
     */
    public static String getServiceNameFromReferece(String serviceRef) {

        String service = serviceRef.trim();
        if ( service.startsWith("\"")) {
            // remove quotes.  assume it starts and ends with quotes
            service = service.substring(1, serviceRef.length()-1);
        }
        // remove operation
        int dotIdx = service.indexOf(".");
        if (dotIdx > 0 ) {
            service = service.substring(0, dotIdx);
        }
        // remove ns prefix or ns
        int nsIdx = service.indexOf(":");
        if ( nsIdx < 0 ) {
            // prefix not found. check if the there is a ns defined as {ns}service
            nsIdx = service.indexOf("}");
        }
        if ( nsIdx > 0 ) {
            service = service.substring(nsIdx+1);
        }
        // add quotes back if exists
        if ( serviceRef.startsWith("\"")) {
            service = "\"" + service + "\"";
        }
        return service;
    }

    public static boolean isUndeclaredService(Context context) {
        Set<String> declaredServices = getDeclaredServices(context);
        ASTNode root = getASTRoot(context);
        String refService = null;
        int offset = context.getOffset();
        ASTPath path = root.findPath(offset);
        ASTItem leaf = path.getLeaf();
        if (leaf instanceof ASTToken) {
            ASTToken leafTok = (ASTToken) leaf;
            refService = leafTok.getIdentifier();
        }
//        LOG.info("Refereced Service: " + refService);
//        if ( refService != null ) {
//            refService = refService.trim();
//            refService = refService.substring(1, refService.length() - 1).trim();            
//        }
        // return !declaredServices.contains(refService);

//        LOG.info("### Service Ref " + refService);
//        LOG.info("#### declared services " + declaredServices);
        
        String serviceName = getServiceNameFromReferece(refService);

        boolean found = declaredServices.contains(serviceName);
        if (!found) {
            // check if it is a declared named route
            Set<String> namedRoutes = getNamedRoutes(context);
            found = namedRoutes.contains(serviceName);
        }
        return !found; // not found means undeclared service or named route
    }

    /**
     * check the value of string identifier is a service name or a route name.
     * @param context
     * @return true if there is no service name or named route with the value
     * of string identifier. else false.
     */
    public static boolean isUndeclaredEndpoint(Context context) {
        Set<String> declaredServices = getDeclaredServices(context);
        ASTNode root = getASTRoot(context);
        String refService = null;
        int offset = context.getOffset();
        ASTPath path = root.findPath(offset);
        ASTItem leaf = path.getLeaf();
        if (leaf instanceof ASTToken) {
            ASTToken leafTok = (ASTToken) leaf;
            refService = leafTok.getIdentifier();
        }
//        LOG.info("### Service Ref " + refService);
//        LOG.info("#### declared services " + declaredServices);
        String serviceName = getServiceNameFromReferece(refService);
        boolean found = declaredServices.contains(serviceName);
        if (!found) {
            // check if it is a declared named route
            Set<String> namedRoutes = getNamedRoutes(context);
            found = namedRoutes.contains(serviceName);
        }
        return !found; // not found means undeclared service or named route
    }

    public static void findASTNodes(ASTNode node, String nt, List<ASTNode> list) {
        if (nt.equals(node.getNT())) {
            list.add(node);
        }
        for (ASTItem item : node.getChildren()) {
            if (item instanceof ASTNode) {
                ASTNode child = (ASTNode) item;
                findASTNodes(child, nt, list);
            }
        }
    }

    public static Set<String> getFlowConfigurations(Context context) {
        Set<String> flowConfigs = new HashSet<String>();
        ASTNode root = getASTRoot(context);
        if (root == null) {
            return flowConfigs;
        }
        List<ASTNode> configNodes = new ArrayList<ASTNode>();
        findASTNodes(root, NT_FLOW_CONFIG, configNodes);
        for (ASTNode configNode : configNodes) {
            String config = null;
            for (ASTItem item : configNode.getChildren()) {
                if (item instanceof ASTToken) {
                    ASTToken token = (ASTToken) item;
                    String type = token.getTypeName();
                    String value = token.getIdentifier();
                    if (TKN_WHITESPACE.equals(type) || TKN_IDENTIFIER.equals(type) || TKN_OPERATOR.equals(type)) {
                        continue;
                    } else if (TKN_STRING.equals(type)) {
                        config = value;
                        break;
                    } else {
                        break;
                    }
                }
            }
            if (config != null) {
                flowConfigs.add(config);
            }
        }

        return flowConfigs;
    }

    public static List<CompletionItem> getFlowConfigurationsCompletionItems(Context context) {
        Set<String> configSet = getFlowConfigurations(context);
        List<CompletionItem> list = new ArrayList<CompletionItem>();
        for (String config : configSet) {
            list.add(CompletionItem.create(config));
        }
        return list;
    }

    public static List<CompletionItem> completionItemsSimple(Context context) {

        List<CompletionItem> result = new ArrayList<CompletionItem>();
        result.addAll(keywordCList);
        AbstractDocument document = (AbstractDocument) context.getDocument();
        document.readLock();
        try {
            TokenSequence tokenSequence = getTokenSequence(context);
            result.addAll(getPreviousStringLiterals(tokenSequence));
            return merge(result);
        } finally {
            document.readUnlock();
        }
    }

    private static ASTNode getASTRoot(Context context) {
        ASTNode root = null;
        if (context instanceof SyntaxContext) {
            // LOG.info("getASTRoot context is SyntaxContext");
            ASTPath path = ((SyntaxContext) context).getASTPath();
            root = (ASTNode) path.getRoot();
        } else {
            // LOG.info("getASTRoot context is NOT SyntaxContext");
            try {
                ParserManager mgr = ParserManager.get(context.getDocument());
                root = mgr.getAST();
            } catch (ParseException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
        }

        if (root == null) {
            // LOG.info("getASTRoot root is NULL. parsing the document....");
            int offset = context.getOffset();
            Language lang = null;
            try {
                lang = LanguagesManager.get().getLanguage("text/x-ifl");
                Document doc = context.getDocument();
                String buff =
                        context.getDocument().getText(0, doc.getLength() - 1);
                ASTNode node = lang.parse(new ByteArrayInputStream(buff.getBytes()));
                root = node;
            // printASTItemTree(node);
            } catch (Exception ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
        }
        if (root == null) {
            //  LOG.info("Can not find ASTRoot");
        }
        // printASTItemTree(root);
        return root;
    }

    private static ASTNode parseForASTRoot(Context context) {
        ASTNode root = null;
        int offset = context.getOffset();
        Language lang = null;
        try {
            lang = LanguagesManager.get().getLanguage("text/x-ifl");
            Document doc = context.getDocument();
            String buff =
                    context.getDocument().getText(0, doc.getLength() - 1);
            root = lang.parse(new ByteArrayInputStream(buff.getBytes()));

        // printASTItemTree(node);
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return root;
    }

    private static String getPathName(ASTPath path) {
        StringBuffer pathNameBuff = new StringBuffer();
        int size = path.size();
        for (int i = 0; i < size; ++i) {
            ASTItem item = path.get(i);
            if (item instanceof ASTNode) {
                if (i > 0) {
                    pathNameBuff.append(".");
                }
                pathNameBuff.append(((ASTNode) item).getNT());
            }
        }

        String pathName = pathNameBuff.toString();
        return pathName;
    }

    private static String getNewServiceName(String serviceType, Context context) {
        String newServiceName = serviceType;
        Set<String> declaredServicesSet = getDeclaredServices(context);
        for (int i = 1; i < 100; ++i) {
            newServiceName = "\"" + serviceType + i + "\"";
//            LOG.info("checking for new service name " + newServiceName);
            if (!declaredServicesSet.contains(newServiceName)) {
                break;
            }
        }
        return newServiceName;
    }

    private static List<CompletionItem> completionSuggestion(Context context) {
        List<CompletionItem> result = new ArrayList<CompletionItem>();
        ASTNode root = getASTRoot(context);
        if (root == null) {
            // LOG.info("### Can not find the AST Root for IFL");
            result.addAll(rootCList);
            // result.addAll(emptyCList);
            return result;
        }
        int offset = context.getOffset();
        ASTPath path = root.findPath(offset);

        if (path == null) {
            root = parseForASTRoot(context);
            if (root != null) {
                path = root.findPath(offset);
            }
        }

        if (path == null) {
            // LOG.info("#### Path to the offset in IFL file is null. offset "  + offset);
            result.addAll(rootCList);
            // result.addAll(emptyCList);
            return result;
        }
        String pathName = null;
        ASTItem leaf = path.getLeaf();
        ASTNode lastASTNode = null;
        // get the path name and the last AST Node.
        StringBuffer pathNameBuff = new StringBuffer();
        int size = path.size();
        for (int i = 0; i < size; ++i) {
            ASTItem item = path.get(i);
            if (item instanceof ASTNode) {
                lastASTNode = (ASTNode) item;
                if (i > 0) {
                    pathNameBuff.append(".");
                }
                pathNameBuff.append(((ASTNode) item).getNT());
            }
        }
        pathName = pathNameBuff.toString();

        // LOG.info("#### AST PATH at offset : " + offset + " Path: " + pathName);

        boolean isNewLine = isWhitespaceWithNewLine(leaf);
        boolean isWhitespace = false;
        boolean isKeyword = false;
        boolean isNT = false;
        boolean isIdentifier = false;
        boolean isString = false;
        boolean isOperator = false;
        String tokenValue = null;
        String typeName = null;
        String nt = null;
        ASTToken leafToken = null;
        ASTNode leafNode = null;


        if (leaf instanceof ASTToken) {
            leafToken = (ASTToken) leaf;
            tokenValue = leafToken.getIdentifier();
            typeName = leafToken.getTypeName();
            if (TKN_WHITESPACE.equals(typeName)) {
                isWhitespace = true;
            } else if (TKN_KEYWORD.equals(typeName)) {
                isKeyword = true;
            } else if (TKN_IDENTIFIER.equals(typeName)) {
                isIdentifier = true;
            } else if (TKN_STRING.equals(typeName)) {
                isString = true;
            } else if (TKN_OPERATOR.equals(typeName)) {
                isOperator = true;
            }
        } else if (leaf instanceof ASTNode) {
            leafNode = (ASTNode) leaf;
            isNT = true;
            nt = leafNode.getNT();
        }

////        if ( LOG.isLoggable(Level.INFO)) {
////            StringBuffer outBuff = new StringBuffer();
////            outBuff.append("Path=").append(pathName).append("\n")
////                    .append("lastASTNode=").append(lastASTNode).append("\n")
////                    .append("NT=").append(nt).append("\n")
////                    .append("isNT=").append(isNT).append("\n")
////                    .append("token type=").append(typeName).append("\n")
////                    .append("token value=").append(tokenValue).append("\n")
////                    .append("isNewLine=").append(isNewLine).append("\n")
////                    .append("isWhitespace=").append(isWhitespace).append("\n")
////                    .append("isKeyword=").append(isKeyword).append("\n")
////                    .append("isIdentifier=").append(isIdentifier).append("\n")
////                    .append("isString=").append(isString).append("\n")
////                    .append("isOperator=").append(isOperator).append("\n");
////
////            LOG.info(outBuff.toString());
////        }

        if (NT_S.equals(pathName)) {
            String serviceType = null;
            String serviceName = null;
            if (lastASTNode != null) {
                ASTNode stmtNode = lastASTNode.getNode(NT_STMT);
                ASTNode servNode = null;
                if (stmtNode != null) {
                    // LOG.info("service declaration " + stmtNode);
                    servNode = stmtNode.getNode(NT_SERVICE_STMT);
                }
                if (servNode != null) {
                    serviceType = servNode.getTokenTypeIdentifier(TKN_IDENTIFIER);
                    serviceName = servNode.getTokenTypeIdentifier(TKN_STRING);
                }
            // LOG.info(" in complete service declaration " + serviceType);
            }
            if (serviceType != null && serviceName == null) {
                String newServiceName = getNewServiceName(serviceType, context);
                if (newServiceName != null) {
                    result.add(CompletionItem.create(newServiceName));
                } else {
                    result.addAll(rootCList);
                }
            } else {
                result.addAll(rootCList);
            }
        } else if (pathName.endsWith(NT_SERVICE_STMT)) {
            String serviceType = getServiceType(lastASTNode);
            String serviceName = getServiceName(lastASTNode, false);
//            LOG.info("ServiceType=" + serviceType + " Name=" + serviceName);
            if (isNewLine || isWhitespace) {
                if (serviceName == null) {
                    String newServiceName = getNewServiceName(serviceType, context);
                    result.add(CompletionItem.create(newServiceName));
                } else {
                    result.addAll(rootCList);
                }
            }
        } else if (pathName.endsWith(NT_TO_STMT) || pathName.endsWith(NT_FROM_STMT)) {
            if (lastASTNode != null) {
                // LOG.info("to or from ast node " + lastASTNode.getAsText());
                String refServiceName = getReferencedServiceName(lastASTNode, false);
                //  LOG.info("to or from service ref " + refServiceName);
                if (isNewLine || isWhitespace) {
                    if (refServiceName == null) {
                        result.addAll(getDeclaredServicesCompletionItems(context));
                    } else {
                        if (!pathName.contains(NT_ROUTE_STMT_LINE)) {
                            result.addAll(toCList);
                        }
                    }
                }
            } else {
                result.addAll(toCList);
            }
        } else if (pathName.endsWith(NT_FLOW_CONFIG)) {
            if (isNewLine) {
                result.addAll(toCList);
            } else if (isWhitespace) {
                if (pathName.contains(NT_SELECT_STMT)) {
                    result.addAll(ruleProcessorsCList);
                } else {
                    result.addAll(configProcessorsCList);
                }
            } else if (isIdentifier) {
                result.addAll(getPreviousStringLiterals(context));
            } else {
                if (pathName.contains(NT_SELECT_STMT)) {
                    result.addAll(ruleProcessorsCList);
                    result.addAll(selectCList);
                } else {
                    result.addAll(configProcessorsCList);
                }
                result.addAll(getFlowConfigurationsCompletionItems(context));
            }
        } else if (pathName.endsWith(NT_ROUTE_STMT_BLOCK)) {
            result.addAll(routeCList);

        } else if (pathName.endsWith(NT_ROUTE_STMT_LINE)) {
            result.addAll(routeLineCList);
        } else if (pathName.endsWith(NT_BROADCAST_STMT)) {
            result.addAll(broadcastCList);
        } else if (pathName.endsWith(NT_SPLIT_STMT) ||
                pathName.endsWith(NT_AGGREGATE_STMT) ||
                pathName.endsWith(NT_FILTER_STMT)) {
            result.addAll(configProcessorsCList);
            result.addAll(getFlowConfigurationsCompletionItems(context));
        } else if (pathName.endsWith(NT_TEE_STMT)) {
            result.addAll(teeCompletionItems());
        } else if (pathName.endsWith(NT_SELECT_STMT)) {
            result.addAll(ruleProcessorsCList);
            result.addAll(selectCList);
        } else if (pathName.endsWith(NT_WHEN_BLOCK)) {
            result.add(CompletionItem.create(KW_ROUTE));
            result.addAll(selectCList);
        } else if (pathName.endsWith(NT_ELSE_BLOCK)) {
            result.add(CompletionItem.create(KW_ROUTE));
            result.addAll(selectCList);
        } else if (pathName.endsWith(NT_TO_ROUTE)) {
            if (pathName.contains(NT_WHEN_BLOCK)) {
                result.addAll(selectCList);
            }
        }

        return result;
    }

    public static List<CompletionItem> completionItems(Context context) {
        return completionSuggestion(context);
    }

    private static TokenSequence getTokenSequence(Context context) {
        TokenHierarchy tokenHierarchy = TokenHierarchy.get(context.getDocument());
        TokenSequence ts = tokenHierarchy.tokenSequence();
        while (true) {
            ts.move(context.getOffset());
            if (!ts.moveNext()) {
                return ts;
            }
            TokenSequence ts2 = ts.embedded();
            if (ts2 == null) {
                return ts;
            }
            ts = ts2;
        }
    }

    private static List<CompletionItem> merge(List<CompletionItem> items) {
        List<CompletionItem> merged = new ArrayList<CompletionItem>();
        merged.addAll(items);
        return merged;
    }

    private static Token previousToken(TokenSequence ts) {
        do {
            // if (!ts.movePrevious ()) return ts.token ();
            if (!ts.movePrevious()) {
                return null;
            }
        } while (ts.token().id().name().endsWith(TKN_WHITESPACE) ||
                ts.token().id().name().endsWith(TKN_COMMENT) ||
                ts.token().id().name().endsWith(TKN_LIN_COMMENT));
        return ts.token();
    }

    public static Map<String, String> createServiceConfigFileMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("xslt", "transform.xsl");
        map.put("bpel", "process.bpel");
        return map;
    }

    public static boolean enabledEditConfig(ASTNode node, JTextComponent comp) {
       // return true;
        boolean enabled = true;
        
        int offset = comp.getCaretPosition();
        if (node == null) {
            return false;
        }
        ASTNode configASTNode = null;
        configASTNode = node.findNode(NT_SERVICE_NAME, offset);

        if (configASTNode == null ) {
            configASTNode = node.findNode(NT_SERVICE_STMT, offset);
        }
        if (configASTNode == null ) {
            configASTNode = node.findNode(NT_CONFIG_FLOW_STMTS, offset);
        }
        if ( configASTNode == null ) {
            enabled = false;
        }
        return enabled;
    }

    private static void printASTItem(ASTItem item) {
        if (item == null) {
            LOG.info("Null ASTItem for printing");
            return;
        }
        if (item instanceof ASTNode) {
            ASTNode node = (ASTNode) item;
            LOG.info("ASTNode[" + node.getNT() + "]");
        } else if (item instanceof ASTToken) {
            ASTToken token = (ASTToken) item;
            LOG.info("ASTToken[" +
                    "identifier=" + token.getIdentifier() +
                    ",TypeName=" + token.getTypeName() +
                    ",TypeId=" + token.getTypeID() +
                    "]");
        } else {
            LOG.info("ASTItem[" + item.getClass().getName() + "]");
        }
    }

    private static void printASTItemChildren(ASTItem item) {
        if (item == null) {
            LOG.info("Null ASTItem for printing its children");
            return;
        }
        LOG.info("ITEM--------");
        printASTItem(item);
        List<ASTItem> items = item.getChildren();
        LOG.info("ITEM CHILDREN--------");
        for (ASTItem i : items) {
            printASTItem(i);
        }
    }

    private static void printASTPath(ASTPath path) {
        int size = path.size();
        LOG.info("AST PATH-----------");
        LOG.info("\nASTPath Root:");
        printASTItem(path.getRoot());
        LOG.info("\nASTPath Leaf:");
        printASTItem(path.getLeaf());
        LOG.info("\nASTPath Items:");
        for (int i = 0; i < size; ++i) {
            ASTItem item = path.get(i);
            printASTItem(item);
        }
    }

    private static void printASTItemTree(ASTItem root) {
        if (root == null) {
            return;
        }
        printASTItem(root);
        List<ASTItem> items = root.getChildren();
        for (ASTItem item : items) {
            printASTItemTree(item);
        }
    }

    private static boolean isWhitespaceWithNewLine(ASTItem leaf) {
        boolean whitespaceWithNewLine = false;
        if (leaf instanceof ASTToken) {
            ASTToken token = (ASTToken) leaf;
            if (TKN_WHITESPACE.equals(token.getTypeName())) {
                String value = token.getIdentifier();
//                LOG.info("Checking for new line " + newLine);
                if (value != null &&
                        (value.contains("\n") || value.contains(lineSeparator))) {
//                    LOG.info("In New LINE....");
                    whitespaceWithNewLine = true;
                } else {
//                    LOG.info("NO new line found in " + value);
                }
            }
        }
        return whitespaceWithNewLine;
    }

    public static ASTNode getServiceNode(ASTNode rootNode, int offset) {
        return rootNode.findNode(IFLSupport.NT_SERVICE_STMT, offset);
    }

    public static ASTNode getServiceNameNodeInServiceGroup(ASTNode rootNode, int offset) {
        return rootNode.findNode(IFLSupport.NT_SERVICE_NAME, offset);
    }

    public static String getServiceNameInServiceGroup(ASTNode rootNode, int offset, boolean trim) {
        String serviceName = null;
        ASTNode serviceNameNode = rootNode.findNode(IFLSupport.NT_SERVICE_NAME, offset);
        if (serviceNameNode != null ) {
            serviceName = serviceNameNode.getTokenTypeIdentifier(TKN_STRING);
            if (serviceName != null) {
                serviceName = serviceName.trim();
                if (trim) {
                  serviceName = serviceName.substring(1, serviceName.length() - 1).trim();
                }
            }
        }
        return serviceName;
    }

    public static String getServiceType(ASTNode serviceNode) {
        String serviceType = null;
        List<ASTItem> items = serviceNode.getChildren();
        for (ASTItem item : items) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                String typeName = token.getTypeName();
                if (TKN_IDENTIFIER.equals(typeName)) {
                    serviceType = token.getIdentifier();
                    break;
                } else if (TKN_WHITESPACE.equals(typeName)) {
                    continue;
                } else {
                    break;
                }
            }
        }
        return serviceType;
    }

    public static String getReferencedServiceName(ASTNode serviceRefNode, boolean trim) {
        String serviceName = null;
        List<ASTItem> items = serviceRefNode.getChildren();
        for (ASTItem item : items) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                String typeName = token.getTypeName();
                String typeValue = token.getIdentifier();
                LOG.info("to or from svc typeName " + typeName + " typeValue " + typeValue);
                if (TKN_KEYWORD.equals(typeName)) {
                    if (KW_TO.equals(typeValue) || KW_FROM.equals(typeValue)) {
                        continue;
                    } else {
                        break;
                    }
                } else if (TKN_WHITESPACE.equals(typeName)) {
                    continue;
                } else if (TKN_STRING.endsWith(typeName)) {
                    serviceName = token.getIdentifier();
                    LOG.info("to or from svc name " + serviceName);
                    break;
                } else {
                    break;
                }
            }
        }
        if (trim && serviceName != null) { // trim, remove quotes and trim

            serviceName = serviceName.trim();
            serviceName = serviceName.substring(1, serviceName.length() - 1).trim();
        }
        return serviceName;
    }

    public static String getServiceName(ASTNode serviceNode, boolean trim) {
        String serviceName = null;
        List<ASTItem> items = serviceNode.getChildren();
        for (ASTItem item : items) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                String typeName = token.getTypeName();
                if (TKN_IDENTIFIER.equals(typeName)) {
                    continue;
                } else if (TKN_WHITESPACE.equals(typeName)) {
                    continue;
                } else if (TKN_STRING.endsWith(typeName)) {
                    serviceName = token.getIdentifier();
                    break;
                } else {
                    break;
                }
            }
        }
        if (trim && serviceName != null) { // trim, remove quotes and trim

            serviceName = serviceName.trim();
            serviceName = serviceName.substring(1, serviceName.length() - 1).trim();
        }
        return serviceName;
    }

    public static String getServiceConfigPath(ASTNode serviceNode) {
        String configPath = null;
        String serviceType = getServiceType(serviceNode);
        String serviceName = getServiceName(serviceNode, true);
        if (serviceName != null && serviceType != null) {
            configPath = serviceType + "/" + serviceName;
        }
        return configPath;
    }

    public static String getServiceConfigPath(ASTNode node, int offset) {
        String configPath = null;
        ASTNode serviceNode = null;
        serviceNode = node.findNode(NT_SERVICE_STMT, offset);
        if (serviceNode != null) {
            configPath = getServiceConfigPath(serviceNode);
        }
        return configPath;
    }

    /**
     * 
     * @param node
     * @param offset
     * @return
     */
    public static String getFlowConfigPath(ASTNode node, int offset) {

        String configPath = null;
        ASTNode confgurableFlowsNode = null;
        ASTNode flowNode = null;
        ASTNode configNode = null;
        String flowName = null;
        String configType = null;
        String configName = null;
        confgurableFlowsNode = node.findNode(NT_CONFIG_FLOW_STMTS, offset);
//        printASTItemChildren(confgurableFlowsNode);
        if (confgurableFlowsNode == null) {
            return null;
        }
        // get the Flow ASTNode
        List<ASTItem> children = confgurableFlowsNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTNode) {
                flowNode = (ASTNode) item;
                break;
            }
        }
//        printASTItemChildren(flowNode);
        if (flowNode == null) {
            return null;
        }
        // get the FlowConfig ASTNode
        children = flowNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTNode) {
                configNode = (ASTNode) item;
                break;
            }
        }
//        printASTItemChildren(configNode);
        if (configNode == null) {
            return null;
        }
        // init flow name
        children = flowNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                if (TKN_KEYWORD.equals(token.getTypeName())) {
                    flowName = token.getIdentifier();
                    break;
                }
            }
        }
        // init config type and name.
        children = configNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                if (TKN_IDENTIFIER.equals(token.getTypeName())) {
                    configType = token.getIdentifier();
                } else if (TKN_OPERATOR.equals(token.getTypeName())) {
                    break; // no named config.

                } else if (TKN_STRING.equals(token.getTypeName())) {
                    configName = token.getIdentifier();
                    if (configName != null) { // trim, remove quotes and trim

                        configName = configName.trim();
                        configName = configName.substring(1, configName.length() - 1).trim();
                    }
                    break;
                }
            }
        }

        if (flowName != null && configName != null) {
            configPath = flowName + "/" + configName;
        }
        return configPath;
    }

    public static void showConfigEditHelpDialog() {
        String msg = "<html><p>Not a Service or Flow statement with configuration.<br>" +
                "<p>To edit the configuration file for the Service or Flow statement,<br>" +
                "place the cursor on the Service or Flow statement and <br>" +
                "invoke the \"Edit Configuration\" action.";
        NotifyDescriptor msgDialog = new NotifyDescriptor.Message(msg, NotifyDescriptor.INFORMATION_MESSAGE);
        Object result = DialogDisplayer.getDefault().notify(msgDialog);
    }

    public static void showConfigNotFoundDialog() {
        String msg = "<html><b>Configuration file not found.</b><br>" +
                "<p>Please execute Generate Artifacts action before editing the configuration";
        NotifyDescriptor msgDialog = new NotifyDescriptor.Message(msg, NotifyDescriptor.INFORMATION_MESSAGE);
        Object result = DialogDisplayer.getDefault().notify(msgDialog);
    }

    public static boolean confirmGenerateArtifactsExecution() {
        String confirmMsg = "<html><b>Configuration File not found.</b><br>" +
                "<p>May require Generating Artifacts before editing the configuration.<br>" +
                "<p><b>Do you want to run Generate Artifacts Action?";
        NotifyDescriptor confirmDialog = new NotifyDescriptor.Confirmation(confirmMsg, NotifyDescriptor.YES_NO_OPTION);
        Object result = DialogDisplayer.getDefault().notify(confirmDialog);
        return NotifyDescriptor.YES_OPTION.equals(result);
    }

    public static void confirmGenerateArtifactsExecutionForEditCodeAction(Project prj) {
        String confirmMsg = "<html><b>Source file not found.</b><br>" +
                "<p>May require Generating Artifacts before editing the code.<br>" +
                "<p><b>Do you want to run Generate Artifacts Action?";
        NotifyDescriptor confirmDialog = new NotifyDescriptor.Confirmation(confirmMsg, NotifyDescriptor.YES_NO_OPTION);
        Object result = DialogDisplayer.getDefault().notify(confirmDialog);
        boolean generate = NotifyDescriptor.YES_OPTION.equals(result);

        if (generate && prj != null) {
            IFLGenerateAction.executeGenerateGoal(prj);
        }
    }

    public static boolean performEditConfigAfterGenerate(final ASTNode node, final JTextComponent comp, int offset) {
        DataObject dobj = null;
        String configPath = null;
        configPath = getServiceConfigPath(node, offset);
        if (configPath == null) {
            configPath = getFlowConfigPath(node, offset);
            if (configPath != null) {
                dobj = findFlowConfigFile(comp, configPath);
            }
        } else {
            dobj = findServiceConfigFile(comp, configPath);

        }
        if (dobj != null) {
            editFile(dobj);
            return true;
        } else {
            return false;
        }
    }

    public static void performEditConfig(final ASTNode node, final JTextComponent comp) {

        if ( node != null ) {
            LOG.info("### Edit Configuration on NT " + node.getNT());
        } else {
            LOG.info("### ASTNode is NULL oon edit config");
        }
        DataObject dobj = null;
        String configPath = null;
        int offset = comp.getCaretPosition();
        
        ASTPath path = node.findPath(offset);
//        printASTPath(path);
        if ( path == null) {
            return;
        }
        boolean find = (isWhitespaceWithNewLine(path.getLeaf())) ? false : true;
        if (!find) {
            showConfigEditHelpDialog();
            return; // show dialog.
        }

        configPath = getServiceConfigPath(node, offset);
        if (configPath == null) {
            configPath = getFlowConfigPath(node, offset);
            if (configPath != null) {
                dobj = findFlowConfigFile(comp, configPath);
            }
        } else {
            dobj = findServiceConfigFile(comp, configPath);
        }
        if ( configPath == null) {
            showConfigEditHelpDialog();
            return;
        }
        if (dobj != null) {
            editFile(dobj);
            return;
        }
        if (configPath != null && dobj == null) {
            boolean generate = false;
            generate = confirmGenerateArtifactsExecution();
            if (generate) {
                DataObject iflDO = getEditingDataObject(comp);
                if (iflDO != null) {
                    IFLGenerateAction.executeGenerateGoal(iflDO);
                    //TODO: perform the below code only when you know the maven build is done with success
//                    boolean edited = performEditConfigAfterGenerate(node, comp, offset);
//                    if (!edited) {
//                        showConfigNotFoundDialog();
//                    }
                }
            }
        }
    }

    public static DataObject getEditingDataObject(JTextComponent comp) {
        DataObject dobj = null;
        dobj = NbEditorUtilities.getDataObject(comp.getDocument());
        return dobj;
    }

    public static Project findProject(DataObject iflDO) {
        return FileOwnerQuery.getOwner(iflDO.getPrimaryFile());
    }

    public static DataObject findDataObject(Project prj, String path) {
        DataObject dobj = null;
        FileObject prjDirFO = prj.getProjectDirectory();
        FileObject fo = prjDirFO.getFileObject(path);
        try {
            dobj = DataObject.find(fo);
        } catch (DataObjectNotFoundException ex) {
            // Exceptions.printStackTrace(ex);
            // ex.printStackTrace();
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return dobj;
    }

    public static void editFile(DataObject dobj) {
        if (dobj == null) {
            LOG.fine("dataobject was NULL for editing");
            return;
        }
        EditCookie editC = dobj.getLookup().lookup(EditCookie.class);
        if (editC == null) {
            // try with getCookies
            editC = dobj.getCookie(EditCookie.class);
        }
        if (editC == null) {
            LOG.fine("edit cookies is NULL");
            OpenCookie openC = dobj.getLookup().lookup(OpenCookie.class);
            if (openC == null) {
                openC = dobj.getCookie(OpenCookie.class);
            }
            if (openC != null) {
                openC.open();
            } else {
                LOG.fine("Both open and edit cookies are NULL");
            }
        } else {
            editC.edit();
        }
    }

    /**
     * finds the dataobject for the service config file.
     * @param comp text component of the editor that is editing a file belongs to the project.
     * @param relPath  relative path to the folder that contains the service
     * configuration artifacts. <service-type>/<service-name>
     * @return
     */
    public static DataObject findServiceConfigFile(JTextComponent comp, String relPath) {

        DataObject iflDO = getEditingDataObject(comp);
        Project prj = findProject(iflDO);
        return findServiceConfigFile(prj, relPath);
    }
    /**
     * finds the dataobject for the service config file.
     * @param prj project
     * @param relPath  relative path to the folder that contains the service 
     * configuration artifacts. <service-type>/<service-name>
     * @return
     */
    public static DataObject findServiceConfigFile(Project prj, String relPath) {
        if ( prj == null) {
            return null;
        }
        String configPath = "src/main/config/" + relPath;
        FileObject configPathFO = null;
        configPathFO = prj.getProjectDirectory().getFileObject(configPath);
        if (configPathFO == null) {
            configPath = "src/main/" + relPath;
            configPathFO = prj.getProjectDirectory().getFileObject(configPath);
        }
        if (configPathFO == null) {
            return null; // no config
        }
        FileObject configFO = null;
        FileObject[] files = configPathFO.getChildren();
        for (FileObject fo : files) { // lookup for standard service.* configuration file

            if ("service".equalsIgnoreCase(fo.getName())) {
                configFO = fo;
                break;
            }
        }
        if (configFO == null) {
            // non standard service configuration name.
            // lookup in the mapper for the configuration file for this service
            LOG.fine("Looking up for non standard service config file in " + relPath);
            String serviceType = "";
            int idx = relPath.indexOf("/");
            if (idx > 0) {
                serviceType = relPath.substring(0, idx);
                LOG.fine("Looking up for service config file for " + serviceType);
                String configFile = IFLSupport.serviceConfigFileMap.get(serviceType);
                LOG.fine("Service config file = " + configFile);
                if (configFile != null) {
                    configFO = configPathFO.getFileObject(configFile);
                }
                if (configFO != null) {
                    LOG.fine("Found non standard config file " + configFO.getPath() + " for " + serviceType);
                }
            }
        }

        try {
            return DataObject.find(configFO);
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            return null;
        }
    }
    /**
     *
     * @param comp  text component of the editor that is editing a file belongs to the project.
     * @param path relative path of the flow configuration <flow-name>/<flow-config-name>
     * @return
     */
    public static DataObject findFlowConfigFile(JTextComponent comp, String path) {
        DataObject iflDO = getEditingDataObject(comp);
        Project prj = findProject(iflDO);
        if (prj == null) {
            return null;
        }
        return findFlowConfigFile(prj, path);
    }

    public static DataObject findFlowConfigFile(Project prj, String path) {
        if (prj == null) {
            return null;
        }
        String configPath = "src/main/" + path;
        FileObject configPathFO = null;
        configPathFO = prj.getProjectDirectory().getFileObject(configPath);
        if (configPathFO == null) {
            return null;
        }
        FileObject configFO = null;
        FileObject[] files = configPathFO.getChildren();
        for (FileObject fo : files) {
            if ("flow".equals(fo.getName())) {
                configFO = fo;
                break;
            }
        }
        try {
            return DataObject.find(configFO);
        } catch (DataObjectNotFoundException ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * creates the flowdata without the count key. count key is only required
     * for inline configuration and so should be explicitly calculated when required.
     * @param flowNode
     * @return
     */
    private static Map<FlowKeys, String> createConfigurableFlowData(ASTNode flowNode) {
        ASTNode configNode = null;
        String flowName = null;
        String configType = null;
        String configValue = null;
        boolean inline = false;

        Map<FlowKeys,String> flowData = new HashMap<FlowKeys,String>();
//        printASTItemChildren(flowNode);
        if (flowNode == null) {
            LOG.info("NULL Flow ASTNode");
            return null;
        }
        // get the FlowConfig ASTNode
        List<ASTItem> children = flowNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTNode) {
                configNode = (ASTNode) item;
                break;
            }
        }
//        printASTItemChildren(configNode);
        if (configNode == null) {
            return null;
        }
        // init flow name
        children = flowNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                if (TKN_KEYWORD.equals(token.getTypeName())) {
                    flowName = token.getIdentifier();
                    break;
                }
            }
        }
        // init config type and name.
        children = configNode.getChildren();
        for (ASTItem item : children) {
            if (item instanceof ASTToken) {
                ASTToken token = (ASTToken) item;
                if (TKN_IDENTIFIER.equals(token.getTypeName())) {
                    configType = token.getIdentifier();
                } else if (TKN_OPERATOR.equals(token.getTypeName())) {
                    // break; // no named config.
                    inline = true;
                } else if (TKN_STRING.equals(token.getTypeName())) {
                    configValue = token.getIdentifier();
                    if (configValue != null) { // trim, remove quotes and trim

                        configValue = configValue.trim();
                        configValue = configValue.substring(1, configValue.length() - 1).trim();
                    }
                    break;
                }
            }
        }
        flowData.put(FlowKeys.NAME, flowName);
        flowData.put(FlowKeys.CONFIG_TYPE, configType);
        flowData.put(FlowKeys.CONFIG_VALUE, configValue);
        flowData.put(FlowKeys.INLINE_CONFIG, Boolean.toString(inline));
        flowData.put(FlowKeys.OFFSET, Integer.toString(flowNode.getOffset()) );

        return flowData;

    }

    private static List<Map<FlowKeys, String>> getConfigurableFlowDataList(ASTNode node, int offset) {

        List<Map<FlowKeys, String>> flows = new ArrayList<Map<FlowKeys, String>>();
        Map<FlowKeys,String> flowData = new HashMap<FlowKeys,String>();

        ASTNode root = (ASTNode) node.findPath(offset).getRoot();
        if ( root == null) {
            return flows;
        }
        List<ASTNode> flowNodes = new ArrayList<ASTNode>();
        findASTNodes(root, NT_SPLIT_STMT, flowNodes);
        findASTNodes(root, NT_AGGREGATE_STMT, flowNodes);
        findASTNodes(root, NT_FILTER_STMT, flowNodes);
        findASTNodes(root, NT_SELECT_STMT, flowNodes);

       // LOG.info("FindASTNodes " + flowNodes.size());

        for ( ASTNode flowNode : flowNodes) {
            if ( offset < 0 || flowNode.getOffset() <= offset) {
                flowData = createConfigurableFlowData(flowNode);
                if ( flowData != null ) {
                    flows.add(flowData);
                }
            }
        }
        return flows;
    }

    private static Map<FlowKeys, String> getAggregateFlowData(ASTNode node, int offset, boolean ignoreIndex) {
        ASTNode flowNode = null;
        boolean inline = false;

        Map<FlowKeys,String> flowData = new HashMap<FlowKeys,String>();
        // get the Flow ASTNode
        flowNode = node.findNode(NT_AGGREGATE_STMT, offset);
        if (flowNode == null) {
            return null;
        }
        flowData = createConfigurableFlowData(flowNode);
        if ( flowData == null) {
            return null;
        }
        inline = Boolean.valueOf(flowData.get(FlowKeys.INLINE_CONFIG));
        if (!ignoreIndex && inline) {
            // add the count
            // ASTNode cfgflowNode = node.findNode(NT_CONFIG_FLOW_STMTS, offset);
            int index = 0;
            int flowOffset = Integer.valueOf(flowData.get(FlowKeys.OFFSET));

            List<Map<FlowKeys, String>> list = getConfigurableFlowDataList(node, offset);
            // LOG.info("Number of flows found " + list.size());
            for ( Map<FlowKeys, String> data : list ) {
               if (KW_AGGREGATE.equals(data.get(FlowKeys.NAME)) ) {
                   ++index;
                   int tOffset = Integer.valueOf(data.get(FlowKeys.OFFSET));
                   if ( tOffset == flowOffset) {
                       flowData.put(FlowKeys.INDEX, Integer.toString(index));
                   }
               }
            }
        }

        return flowData;
    }

    private static boolean isAggregateWithJavaType(ASTNode node, int offset) {
        Map<FlowKeys,String> flowData = getAggregateFlowData(node, offset, true);
        if ( flowData == null ) {
            return false;
        }
        String configType = flowData.get(FlowKeys.CONFIG_TYPE);
        return "java".equals(configType);
    }

    public static boolean enabledEditCode(ASTNode node, JTextComponent comp) {
        // LOG.info("Enabling/Disabling Edit Code Action....");
        // new Exception("Enabling/Disabling Edit Code Action....").printStackTrace();

        int offset = comp.getCaretPosition();
        ASTPath path = node.findPath(offset);
        if ( path == null ) {
            return false;
        }
        boolean isNewLine = (isWhitespaceWithNewLine(path.getLeaf())) ? true : false;
        if (isNewLine) {
            // LOG.info("enableEditCode.isNewLine");
            return false;
        }
        boolean enabled = isAggregateWithJavaType(node, offset);
        if (enabled ) {
            return enabled;
        }

        ASTNode serviceNode = null;
        serviceNode = IFLSupport.getServiceNode(node, offset);
        if ( serviceNode == null) {
            return false;
        }
        String serviceType = IFLSupport.getServiceType(serviceNode);
        String serviceName = IFLSupport.getServiceName(serviceNode, true);
        if (serviceName == null || serviceType == null) {
            return false;
        }
        if ("java".equals(serviceType) ||
                "jruby".equals(serviceType) ||
                "bpel".equals(serviceType) ||
                "xslt".equals(serviceType) ) {
            return true;
        } else {
            return false;
        }
    }

    public static void performServiceCodeEdit(ASTNode node,
            JTextComponent comp,
            DataObject iflDO, Project prj) {
        if (prj == null) {
            LOG.info("Null project");
            return;
        }
        int offset = comp.getCaretPosition();
        ASTNode serviceNode = null;
        serviceNode = IFLSupport.getServiceNode(node, offset);
        if (serviceNode == null) {
            return;
        }
        String serviceType = IFLSupport.getServiceType(serviceNode);
        String serviceName = IFLSupport.getServiceName(serviceNode, true);
        if (serviceName == null || serviceType == null) {
            return;
        }
        String codePath = null; //project relative path.
        if ("java".equals(serviceType)) {
            codePath = "src/main/java/pojo/Svc" + serviceName + ".java";
        } else if ("jruby".equals(serviceType)) {
            codePath = "src/main/jruby/" + serviceName + "/Service.rb";
        } else if ("bpel".equals(serviceType) ) {
            codePath = "src/main/bpel/" + serviceName + ".bpel";
        } else if("xslt".equals(serviceType)) {
            codePath = "src/main/xslt/" + serviceName + "/transform.xsl";
        } 

        if (codePath != null) {
            try {
                FileObject codeFO = prj.getProjectDirectory().getFileObject(codePath);
                DataObject codeDO = null;
                if (codeFO != null) {
                    codeDO = DataObject.find(codeFO);
                }
                editFile(codeDO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void performEditCode(final ASTNode node, final JTextComponent comp) {
        // LOG.info("Perfroming Edit Code Action....");
        DataObject iflDO = getEditingDataObject(comp);
        Project prj = findProject(iflDO);
        if (prj == null) {
            LOG.info("Null project");
            return;
        }
        int offset = comp.getCaretPosition();
        if ( isAggregateWithJavaType(node, offset) ) {
            Map<FlowKeys, String> flowData = getAggregateFlowData(node, offset, false);
            if (flowData != null) {
//            LOG.info("------ Aggregate Data ------");
//            for ( FlowKeys key : flowData.keySet() ) {
//                LOG.info(key + "=" + flowData.get(key));
//            }
                openAggregateCode(prj, flowData);
            } else {
                LOG.info("No Aggregate java code found");
            }
            // openJavaFile(prj, "aggregate.mytest", "Aggregator");
        } else {
            // open source code other service types.

            performServiceCodeEdit(node, comp, iflDO, prj);
        }
       
    }

    private static Properties readProperties(FileObject propFO) {
        Properties props = new Properties();
        InputStream in = null;
        try {

            in = propFO.getInputStream();
            props.load(in);
        } catch (Exception ex) {
            // any exception
            // log
            LOG.log(Level.FINE, ex.getMessage(), ex);
        } finally {
            if ( in != null ) {
                try {
                    in.close();
                } catch (IOException ex) {
                    // ignore.
                }
            }
        }
        return props;
    }

    private static Properties readProperties(String propsText) {
        Properties props = new Properties();
        try {
            if (propsText == null) {
                return props;
            }
            String[] propsList = propsText.split(",");
            for (String propData : propsList) {
                String[] propSplit = propData.split("=");
                if (propSplit.length >= 2) {
                    String name = propSplit[0];
                    String value = propSplit[1];
                    props.setProperty(name.trim(), value.trim());
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return props;
    }

    private static void openAggregateCode(Project prj, Map<FlowKeys, String> flowData) {
        String flowName = flowData.get(FlowKeys.NAME);
        String configValue = flowData.get(FlowKeys.CONFIG_VALUE);
        boolean inline = Boolean.valueOf(flowData.get(FlowKeys.INLINE_CONFIG));
        Properties configProps = new Properties();
        String pkgNameKey = "packagename";
        String classNameKey = "classname";
        String packageName = null;
        String className = null;
        String defClassName = "Aggregator";

        if (!inline) {
            // read the package and classname from flow.properties under
            // src/main/config/aggregate/<configValue>/flow.properties
            String configPath = flowName + "/" + configValue;
            DataObject configDO = findFlowConfigFile(prj, configPath);
            if (configDO != null) {
                FileObject configFO = configDO.getPrimaryFile();
                configProps = readProperties(configFO);
            }
            packageName = configProps.getProperty(pkgNameKey);
            className = configProps.getProperty(classNameKey);
            if (packageName == null) {
                packageName = flowName + "." + configValue;
                configProps.put(pkgNameKey, packageName);
            }
            if (className == null) {
                className = defClassName;
                configProps.put(classNameKey, className);
            }

        } else {
            // parse inline value and get the package and classnmae.
            configProps = readProperties(configValue);
            packageName = configProps.getProperty(pkgNameKey);
            className = configProps.getProperty(classNameKey);
            if (packageName == null) {
                try {
                    int index = Integer.valueOf(flowData.get(FlowKeys.INDEX));
                    packageName = flowName + "." + flowName + "_" + index;
                    configProps.put(pkgNameKey, packageName);
                } catch (Exception ex) {
                    LOG.finest(ex.getMessage());
                // ingore
                }
            }
            if (className == null) {
                className = defClassName;
                configProps.put(classNameKey, className);
            }
        }
        packageName = configProps.getProperty(pkgNameKey);
        className = configProps.getProperty(classNameKey);

        if (!openJavaFile(prj, packageName, className)) {
            confirmGenerateArtifactsExecutionForEditCodeAction(prj);
        }
    }

    private static boolean openJavaFile(Project prj, String packageName, String className) {
        String javaSrcDir = "src/main/java";
        String packagePath = packageName.replace(".", "/");
        String javaFile = className + ".java";
        String javaFilePath = javaSrcDir + "/" + packagePath + "/" + javaFile;
        FileObject javaFO = null;
        javaFO = prj.getProjectDirectory().getFileObject(javaFilePath);
        boolean opened = false;
        DataObject javaDO = null;
        try {
           javaDO = DataObject.find(javaFO);
           if ( javaDO != null ) {
                editFile(javaDO);
                opened = true;
           }
        } catch (Exception ex) {
            //TODO log errror.
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return opened;
    }


}
