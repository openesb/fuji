#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

# definition of tokens
TOKEN:keyword:( 
    "route" | 
    "broadcast" | 
    "do" | 
    "from" | 
    "to" | 
    "end" | 
    "split" | 
    "aggregate" | 
    "filter" | 
    "tee" |
    "select" |
    "when" |
    "else" |
    "calls" |
    "namespace"
)
TOKEN:nsprefix:("base-ns"|"public-ns"|"private-ns")
TOKEN:operator:( "{" | "}" | "(" | ")" | "[" | "]" | ",")
TOKEN:identifier:( ["a"-"z"] ["a"-"z" "0"-"9"]* )
TOKEN:whitespace:( [" " "\t" "\n" "\r"]+ )
TOKEN:string: (
    "\"" ( 
        [^ "\\" "\"" "\n" "\r"] |
        ("\\" (
            . |
            (["0"-"7"] ["0"-"7"] ["0"-"7"]) |
            ("x" ["0"-"9" "a"-"f" "A"-"F"] ["0"-"9" "a"-"f" "A"-"F"]) |
            ("u" ["0"-"9" "a"-"f" "A"-"F"] ["0"-"9" "a"-"f" "A"-"F"] ["0"-"9" "a"-"f" "A"-"F"] ["0"-"9" "a"-"f" "A"-"F"])
        ))
    )* 
    "\""
)
TOKEN:comment: ("/*"):<IN_COMMENT>
<IN_COMMENT> {
    TOKEN:comment: (.)
    TOKEN:keyword: ("author")
    TOKEN:comment: ("*/"):<DEFAULT>
}
TOKEN:line_comment: ( ("#"|"//") [^ "\n" "\r"]* )

# parser should ignore whitespaces
SKIP:whitespace
SKIP:comment
SKIP:line_comment

# definition of grammar
S = (Statement)*;
Statement = NamespaceStatement|ServiceStatement|RouteStatements;
NamespaceStatement="namespace" ("base-ns"|"public-ns"|"private-ns"|<identifier>) <string>;
ServiceName = <string> ;
ServiceList = ServiceName ("," ServiceName)* ;
ProviderServiceList = "[" ServiceList "]";
ConsumerServiceList = "calls" ServiceList;
ServiceStatement = <identifier> <string> [ProviderServiceList] [ConsumerServiceList] ;
FromStatement = "from" <string> ;
ToStatement = "to" <string> ;
ToRoute = "to" <string> ;
ToEndpoint = "to" <string> ;
RouteStatementLine = "route" (((FromStatement ToStatement*)|ToStatement+)) ;
RouteStatementBlock = "route" "do" ((FromStatement (FlowStatements|ToStatement)*)|(FlowStatements|ToStatement)+) "end";
NamedRouteStatement = "route" <string> "do" ((FlowStatements|ToStatement)+)  "end";
RouteStatements = RouteStatementBlock|NamedRouteStatement|RouteStatementLine;
BroadcastStatement = "broadcast" "do" (RouteStatementBlock|RouteStatementLine)+ "end";
FlowConfiguration = <identifier> (<string>|("("<string>")"));
SplitStatement = "split" FlowConfiguration ;
AggregateStatement = "aggregate" FlowConfiguration ;
FilterStatement = "filter" FlowConfiguration;
TeeStatement = "tee" [<string>|("("<string>")")] ToEndpoint ;
WhenBlock = "when" (<string>|(<identifier> "(" <string> ")")) (ToEndpoint|("do" (ToEndpoint*) "end" ));
ElseBlock = "else" (ToEndpoint|("do" (ToEndpoint*) "end" ));
SelectStatement = "select" [<string>|FlowConfiguration] ["do" WhenBlock* [ElseBlock] "end"];
ConfigurableFlowStatements = SplitStatement|AggregateStatement|FilterStatement|SelectStatement;
FlowStatements = BroadcastStatement|TeeStatement|ConfigurableFlowStatements;

# syntax colors
COLOR:line_comment: {
    foreground_color: "gray";
}

COLOR:nsprefix: {
    foreground_color: "green";
}

COLOR:ToStatement.string : {
    color_name: "undeclared_service";
    default_coloring: "default";
    condition:org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport.isUndeclaredService;
    font_type: "bold";
    foreground_color: "0x737373";
}

COLOR:FromStatement.string : {
    color_name: "undeclared_service";
    default_coloring: "default";
    condition:org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport.isUndeclaredService;
    font_type: "bold";
    foreground_color: "0x737373";
}

COLOR:ToRoute.string : {
    color_name: "undeclared_service";
    default_coloring: "default";
    condition:org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport.isUndeclaredService;
    font_type: "bold";
    foreground_color: "0x737373";
}

 COLOR:ToEndpoint.string : {
    color_name: "undeclared_endpoint";
    default_coloring: "default";
    condition:org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport.isUndeclaredEndpoint;
    font_type: "bold";
    foreground_color: "0x737373";
}

# code folding
# FOLD:RouteStatements : "route $string$$FromStatement$$ToStatement$..."
FOLD:NamedRouteStatement : "route $string$ $ToStatement$..."
FOLD:RouteStatementBlock : "route $FromStatement$$ToStatement$..."
FOLD:BroadcastStatement : "broadcast $FromStatement$$ToStatement$..."
# FOLD:TeeStatement : "tee $string$$ToStatement$..."
FOLD:SelectStatement : "select $FlowConfiguration$..."

NAVIGATOR:NamespaceStatement: {
   display_name: "Namespace[$nsprefix$$identifier$] $string$ ";
}

NAVIGATOR:ServiceName: {
   display_name: "$string$ ";
}

NAVIGATOR:ProviderServiceList: {
  display_name: "Provider Services:";
}

NAVIGATOR:ConsumerServiceList: {
  display_name: "Consumer Services:";
}

NAVIGATOR:ServiceStatement: {
   display_name: "Service: $string$ [$identifier$]";
}

NAVIGATOR:ToStatement: {
    display_name: "to $string$";
}
NAVIGATOR:ToEndpoint: {
    display_name: "to $string$";
}
NAVIGATOR:NamedRouteStatement: {
    display_name: "Route $string$";
}
NAVIGATOR:RouteStatementBlock: {
    display_name: "Route $string$ $FromStatement$";
}
NAVIGATOR:RouteStatementLine: {
    display_name: "Route $FromStatement$";
}
NAVIGATOR:BroadcastStatement: {
    display_name: "Broadcast";
}
NAVIGATOR:SplitStatement: {
    display_name: "Split $FlowConfiguration$";
}
NAVIGATOR:AggregateStatement: {
    display_name: "Aggregate $FlowConfiguration$";
}
NAVIGATOR:FilterStatement: {
    display_name: "Filter $FlowConfiguration$";
}
NAVIGATOR:TeeStatement: {
    display_name: "Tee $ToEndpoint$";
}
NAVIGATOR:WhenBlock: {
    display_name: "when $string$ $ToRoute$";
}
NAVIGATOR:ElseBlock: {
    display_name: "else $ToRoute$";
}
NAVIGATOR:SelectStatement: {
    display_name: "Select $FlowConfiguration$";
}
# brace completion
COMPLETE "{:}"
COMPLETE "(:)"
COMPLETE "\":\""
COMPLETE "do:\nend"

# brace matching
BRACE "{:}"
BRACE "(:)"
BRACE "do:end"

# indentation support
INDENT "{:}"
INDENT "(:)"
INDENT "do:end"
# INDENT "\\s*(((if|while)\\s*\\(|else\\s*|else\\s+if\\s*\\(|for\\s*\\(.*\\))[^{;]*)"


# code completion
COMPLETION:keyword, operator, identifier, whitespace: {
      text1:org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport.completionItems;
}

#
#


