/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.ifl.config;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class ServiceConfigNode extends AbstractNode {
    private static final Logger LOG = Logger.getLogger(ServiceConfigNode.class.getName());
    private Configuration mModel;
    public ServiceConfigNode(Configuration model) {
        super(Children.LEAF, Lookups.fixed(model));
        this.mModel = model;
        String name = this.mModel.getName();
        this.setName(name);
        String dispName = this.mModel.getInfo().getDisplayName();
        if ( dispName == null ) {
            dispName = name;
        }
        this.setDisplayName(dispName);
        String shortDesc = this.mModel.getInfo().getShortDescription();
        if ( shortDesc == null) {
            shortDesc = name;
        }
        this.setShortDescription(shortDesc);
    }
    
    public Configuration getConfiguration() {
        return this.mModel;
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        List<Sheet.Set> sheetSetList = createPropertySets();
        for ( Sheet.Set set : sheetSetList ) {
            sheet.put(set);
        }
        return sheet;
    }

    protected List<Sheet.Set> createPropertySets() {

        Comparator<PropertyGroup> comp = new Comparator<PropertyGroup>() {
                    public int compare(PropertyGroup o1,
                            PropertyGroup o2) {

                        return o1.getInfo().getPosition() - o2.getInfo().getPosition();
                    }
                };
        List<PropertyGroup> groupList = new ArrayList<PropertyGroup>();
        groupList.addAll(this.mModel.getPropertyGroupList());
        Collections.sort(groupList, comp);

        List<Sheet.Set> sheetSetList = new ArrayList<Sheet.Set>();

        for ( PropertyGroup propGroup : groupList ) {
            sheetSetList.add(createPropertySet(propGroup));
        }
        return sheetSetList;
    }

    protected Sheet.Set createPropertySet(PropertyGroup propGroup) {
        Sheet.Set set = Sheet.createPropertiesSet();
        String name = propGroup.getName();
        String dispName = propGroup.getInfo().getDisplayName();
        String shortDesc = propGroup.getInfo().getShortDescription();
        if ( dispName == null ) {
            dispName = name;
        }
        if (shortDesc == null) {
            shortDesc = name;
        }
        set.setName(name);
        set.setDisplayName(dispName);
        set.setShortDescription(shortDesc);

        Comparator<org.glassfish.openesb.tools.common.descriptor.Property> comp =
                new Comparator<org.glassfish.openesb.tools.common.descriptor.Property>() {

                    public int compare(org.glassfish.openesb.tools.common.descriptor.Property o1,
                            org.glassfish.openesb.tools.common.descriptor.Property o2) {

                        return o1.getInfo().getPosition() - o2.getInfo().getPosition();
                    }
                };

        List<org.glassfish.openesb.tools.common.descriptor.Property> propList =
                new ArrayList<org.glassfish.openesb.tools.common.descriptor.Property>();

        propList.addAll(propGroup.getPropertyList());
        Collections.sort(propList, comp);

        for (org.glassfish.openesb.tools.common.descriptor.Property prop : propList ) {
            set.put(createPropertySupport(prop));
        }
        return set;
    }

    public static PropertySupport createPropertySupport(org.glassfish.openesb.tools.common.descriptor.Property prop) {
        return new ReadWriteProperty(prop);
    }
    public static class ReadOnlyProperty extends PropertySupport.ReadOnly<String> {

        private org.glassfish.openesb.tools.common.descriptor.Property info;

        public ReadOnlyProperty(org.glassfish.openesb.tools.common.descriptor.Property prop) {
            super(prop.getName(), String.class, prop.getInfo().getDisplayName(), prop.getInfo().getShortDescription());
            this.info = prop;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return info.getValue();
        }
    }

    public static class ReadWriteProperty extends PropertySupport.ReadWrite<String> {

        private org.glassfish.openesb.tools.common.descriptor.Property info;

        public ReadWriteProperty(org.glassfish.openesb.tools.common.descriptor.Property prop) {
            super(prop.getName(), String.class, prop.getInfo().getDisplayName(), prop.getInfo().getShortDescription());
            this.info = prop;
        }

        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return info.getValue();
        }

        @Override
        public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            this.info.setValue(val);
        }
    }
}
