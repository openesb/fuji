/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.ifl.actions;

import java.util.logging.Logger;
import javax.swing.text.JTextComponent;
import org.netbeans.api.languages.ASTNode;
import org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport;
import org.openide.util.NbBundle;

/**
 *
 * @author chikkala
 */
public class IFLEditCodeAction  extends IFLTextAction {

    private static final Logger LOG = Logger.getLogger(IFLEditCodeAction.class.getName());

    public IFLEditCodeAction() {
        super();
    }

    public String getName() {
        return NbBundle.getMessage(IFLEditCodeAction.class, "CTL_IFLEditCodeAction");
    }

    public boolean isEnabled(ASTNode node, JTextComponent comp) {
        return IFLSupport.enabledEditCode(node, comp);
    }

    public void actionPerformed(ASTNode node, JTextComponent comp) {
        IFLSupport.performEditCode(node, comp);
    }

}
