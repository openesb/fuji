/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.support.ifl.actions;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.JTextComponent;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.netbeans.api.languages.ASTNode;
import org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport;
import org.netbeans.modules.jbi.fuji.support.ifl.config.ServiceConfigEditor;
import org.netbeans.modules.jbi.fuji.support.tools.ext.IDEAppBuilder;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle;

/**
 * This action generates a single service artifacts. it invokes the service configuration dialog if needed before
 * generating the artifacts. 
 * 
 * @author chikkala 
 */
public final class IFLGenerateServiceAction extends IFLTextAction {

    private static final Logger LOG = Logger.getLogger(IFLGenerateServiceAction.class.getName());

    public IFLGenerateServiceAction() {
        super();
        LOG.setLevel(Level.FINEST);
    }

    public String getName() {
        return NbBundle.getMessage(IFLGenerateServiceAction.class, "CTL_IFLGenerateServiceAction");
    }

    public boolean isEnabled(ASTNode node, JTextComponent comp) {

        int offset = comp.getCaretPosition();
        ASTNode serviceNode = null;
        serviceNode = IFLSupport.getServiceNode(node, offset);
        if (serviceNode == null) {
            return false;
        }
        String serviceType = IFLSupport.getServiceType(serviceNode);
        String serviceName = IFLSupport.getServiceName(serviceNode, true);
        if (serviceName == null || serviceType == null) {
            return false;
        }
        return true;
    }

    public void actionPerformed(ASTNode node, JTextComponent comp) {
        performGenerateServiceArtifacts(node, comp);
    }

    private void performGenerateServiceArtifacts(ASTNode node, JTextComponent comp) {
        LOG.info("############## Generate service action...");
        DataObject iflDO = IFLSupport.getEditingDataObject(comp);
        int offset = comp.getCaretPosition();
        ASTNode serviceNode = null;
        serviceNode = IFLSupport.getServiceNode(node, offset);
        if (serviceNode == null) {
            LOG.info("NO Service Selected");
            return;
        }
        String serviceType = IFLSupport.getServiceType(serviceNode);
        LOG.info("Service Type : " + serviceType);
        String serviceGroupName = IFLSupport.getServiceName(serviceNode, true);
        LOG.info("Service Group: " + serviceGroupName);
        String serviceName = IFLSupport.getServiceNameInServiceGroup(serviceNode, offset, true);
        if (serviceName != null) {
            LOG.info("Generate Action invoked on Service Name " + serviceName);
            generateServiceArtifacts(iflDO, serviceType, serviceName);
        } else {
            LOG.info("Generate Action invoked on Service Group....");
            Set<String> providers = IFLSupport.getGroupedProviderServices(serviceNode, true);
            Set<String> consumers = IFLSupport.getGroupedConsumerServices(serviceNode, true);
            if (providers.size() == 0) {
                // a signle service delcaration with/without calls.  service group name is the service name.
                serviceName = serviceGroupName;
                generateServiceArtifacts(iflDO, serviceType, serviceName);
            } else {
                // use group service generation. which will fail if the group does not have all the
                LOG.info("Providers: " + providers.size());
                for (String provider : providers) {
                    LOG.info(provider);
                }
                LOG.info("Consumers: " + consumers.size());
                for (String consumer : consumers) {
                    LOG.info(consumer);
                }
                if (providers.size() == 0) {
                    // if the group does not have providers, group name becomes the provider.
                    providers = new HashSet<String>();
                    providers.add(serviceGroupName);
                }
                // generate artifacts of all the providers or consumer services in the group.
                generateGroupedServiceArtifacts(iflDO, serviceType, serviceGroupName, providers, consumers);
            }
        }
        LOG.info("##############");
    }
    
    public void generateServiceArtifacts(DataObject iflDO, String serviceType, String serviceName) {
        LOG.info("generating Service Artifacts for a single service " + serviceName);
        Properties configProps = null;
        try {
            IDEAppBuilder appBuilder = IDEAppBuilder.newInstance(iflDO);
            File configFile = new File(appBuilder.getServiceProjectModel(serviceType).getConfigDir(serviceName),
                    ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
            if (configFile.exists()) {
                configProps = Utils.readProperties(configFile);
                LOG.fine("Generating the service artifacts using exiting service configuration");
                dumpProperties("Configuration properties to generate service", configProps);
                appBuilder.executeGenerateServiceArtifacts(serviceType, serviceName, configProps);
                return;
            } else {
                ServiceConfigEditor.edit(serviceType, serviceName, iflDO, true);
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            showActionErrorDialog(ex.getMessage());
        }
    }

    public void generateGroupedServiceArtifacts(DataObject iflDO, String serviceType, String groupName, Set<String> providers, Set<String> consumers) {
        LOG.info("generating GroupedServiceArtifacts group: " + groupName  + " Consumers ");
        LOG.info("Providers " + providers);
        LOG.info("Consumers " + consumers);
        try {
            IDEAppBuilder appBuilder = IDEAppBuilder.newInstance(iflDO);
            List<String> pList = new ArrayList();
            pList.addAll(providers);
            List<String> cList = new ArrayList();
            cList.addAll(consumers);
            appBuilder.executeGenerateGroupedServiceArtifacts(serviceType, groupName, pList, cList);
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            showActionErrorDialog(ex.getMessage());
        }
    }

    public static void showActionErrorDialog(String error) {
        String msg = "<html>The following error occured in Generating the Service Artifacts.<br>" +
                "<p><b>Error: " + error + "</b>" +
                "<p>Please make sure all the required Service Configuration files exist before executing this action again." +
                "<p><i>Note that the Service Configuration files can be generated by using edit configuration action on each service";
        NotifyDescriptor msgDialog = new NotifyDescriptor.Message(msg, NotifyDescriptor.ERROR_MESSAGE);
        Object result = DialogDisplayer.getDefault().notify(msgDialog);
    }

    private static void dumpProperties(String msg, Properties props) {
        if (LOG.isLoggable(Level.FINE)) {
            if (props == null) {
                LOG.fine("---Null Props for -- " + msg);
                return;
            }
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            props.list(out);
            LOG.fine("----- " + msg);
            LOG.fine(writer.getBuffer().toString());
        }
    }
}
