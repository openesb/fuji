/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.support.mvn;

import java.util.Collections;
import java.util.logging.Logger;
import org.netbeans.modules.maven.indexer.api.NBVersionInfo;
import org.netbeans.modules.maven.indexer.api.RepositoryInfo;
import org.netbeans.modules.maven.indexer.api.RepositoryPreferences;
import org.netbeans.modules.maven.indexer.api.RepositoryQueries;
import org.netbeans.modules.maven.api.archetype.Archetype;
import org.netbeans.modules.maven.api.archetype.ArchetypeProvider;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.jbi.fuji.support.FujiPreferences;

/**
 * 
 * @author chikkala 
 */
public class FujiArchetypeProvider implements ArchetypeProvider {
    private static final Logger LOG = Logger.getLogger(FujiArchetypeProvider.class.getName());
    public static final String DEF_ARCHETYPE_VERSION = "1.0-SNAPSHOT";
    public static final String SNAPSHOT_SUFFIX = "-SNAPSHOT";
    
    public static final String FUJI_REPO_ID = "open-esb";
    
    public static final String FUJI_GROUP_ID = "open-esb.fuji";
    public static final String FUJI_APP_ARTIFACT_ID = "app-archetype";

    /** Creates a new instance of MockArchetypeProvider */
    public FujiArchetypeProvider() {
    }

    private List<Archetype> getRemoteArchetypes() {
        List<Archetype> lst = new ArrayList<Archetype>();
////GroupId: open-esb.fuji
////ArtifactId: app-archetype
////Version: 1.0-SNAPSHOT        
//        Archetype simple = new Archetype(false);
//        simple.setRepository("http://download.java.net/maven/esb");
//        simple.setVersion("1.0-SNAPSHOT"); //NOI18N
//        simple.setArtifactId("app-archetype"); //NOI18N
//        simple.setGroupId("open-esb.fuji"); //NOI18N
//        simple.setName("Fuji Integration Application Archetype");
//        simple.setDescription("Creates Maven Project for Fuji Integration Application ");
//
//        lst.add(simple);
        
        return lst;
    }

    public static String getCurrentVersion() {
        String version = FujiPreferences.getArchetypeVersion();
        if ( version == null || version.trim().length() == 0 ) {
            version = getDefaulAppArchetypeVersion();
            // save the version preferecen
            // FujiPreferences.setArchetypeVersion(version);
        }
        return version;
    }
    /**
     * returns the default version among the list of available version. it
     * checks for release version(e.g. 1.0), release snapshot (1.0-SNAPSHOT)
     * or latest milestone versions (e.g. 1.0-M2-SNAPSHOT)
     * 
     * @return
     */
    public static String getDefaulAppArchetypeVersion() {
        String version = DEF_ARCHETYPE_VERSION;
        List<String> versionList = getAppArchetypeVersions();
        List<String> releaseList = new ArrayList<String>();
        List<String> snapshotList = new ArrayList<String>();
        
        for ( String ver : versionList ) {
            if (ver.endsWith(SNAPSHOT_SUFFIX)) {
                snapshotList.add(ver);
            } else {
                releaseList.add(ver);
            }
        }
        Collections.sort(releaseList);
        Collections.sort(snapshotList);
        
        int idx = releaseList.size()-1;
        if (idx >= 0 ) {
            version = releaseList.get(idx);
            int mIdx = version.indexOf("-");
            if ( mIdx > 0 ) {
                String rVer = version.substring(0, mIdx);
                if ( releaseList.contains(rVer)) {
                    version = rVer;
                }
            }
        } else {
            idx = snapshotList.size()-1;
            if ( idx >= 0 ) {
                version = snapshotList.get(idx);
            }
        }
        return version;
    }
    
    public static List<String> getAppArchetypeVersions() {
        List<String> list = new ArrayList<String>();
        List<RepositoryInfo> infos = RepositoryPreferences.getInstance().getRepositoryInfos();
        for (RepositoryInfo info : infos) {
            if (FUJI_REPO_ID.equals(info.getId())) {
                List<NBVersionInfo> verList = RepositoryQueries.getVersions(FUJI_GROUP_ID, FUJI_APP_ARTIFACT_ID, info);
                for ( NBVersionInfo ver : verList ) {
                    list.add(ver.getVersion());
                }
                break;
            }
        }
        return list;
    }
    
    public List<Archetype> getArchetypes() {
        List<Archetype> lst = new ArrayList<Archetype>();
 
        String currentVersion = getCurrentVersion();
        List<RepositoryInfo> infos = RepositoryPreferences.getInstance().getRepositoryInfos();
        for (RepositoryInfo info : infos) {
            if (FUJI_REPO_ID.equals(info.getId())) {
                List<NBVersionInfo> archs = RepositoryQueries.findArchetypes(info);
                if (archs == null) {
                    continue;
                }
                for (NBVersionInfo art : archs) {
                    // LOG.info("fuji artifact " + art.getProjectName() + " Version " + art.getVersion());
                    if (!currentVersion.equals(art.getVersion()) ) {
                        continue;
                    }
                    //TODO FINDout  how to get contain matadata 
                    // boolean ng = artifact.getFiles().contains("META-INF/maven/archetype-metadata.xml");
                    Archetype arch = ("maven-archetype".equalsIgnoreCase(art.getPackaging())) ? //NOI18N
                            new Archetype(true) : new Archetype();
                    arch.setArtifactId(art.getArtifactId());
                    arch.setGroupId(art.getGroupId());
                    arch.setVersion(art.getVersion());
                    arch.setName(art.getProjectName());
                    arch.setDescription(art.getProjectDescription());
                    arch.setRepository(info.getRepositoryUrl());
                    lst.add(arch);
                }
                break;
            }
        }
        
        if ( lst.size() == 0 ) {
            // if no repository is configured, return the preconfigred remote archetypes list.
            lst.addAll(getRemoteArchetypes());
        }
        
        return lst;
    }
}
