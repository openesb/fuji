/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.modules.jbi.fuji.support.mvn;

import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

/**
 * This class provides a short cut view to the service configurations folder
 * @author chikkala
 */
public class ServiceConfigFolderNodeFactory implements NodeFactory {

    private final static String CONFIG_DIR_NAME = "config";
    private final static String MAIN_FOLDER = "src/main";
    private final static String IFL_FOLDER = "src/main/ifl";
    private final static String SVC_CONFIG_FOLDER = "src/main/config";

    public ServiceConfigFolderNodeFactory() {
    }

    public NodeList createNodes(Project prj) {
        FileObject prjDirFO = prj.getProjectDirectory();
        FileObject configDirFO = prjDirFO.getFileObject(SVC_CONFIG_FOLDER);
        DataFolder configDirFolder = null;

        FileObject iflFolderFO = prjDirFO.getFileObject(IFL_FOLDER);
        if (iflFolderFO == null) {
            // Not a Integration app. so don't show this node
            return NodeFactorySupport.fixedNodeList();
        }

        if (configDirFO == null) {
            try {
                configDirFO = FileUtil.createFolder(prjDirFO, SVC_CONFIG_FOLDER);
            } catch (IOException ex) {
                // Exceptions.printStackTrace(ex);
            }
        }

        if (configDirFO != null) {
            configDirFolder = DataFolder.findFolder(configDirFO);
        }

        if (configDirFolder == null) {
            return NodeFactorySupport.fixedNodeList();
        }

        Node originalNode = configDirFolder.getNodeDelegate().cloneNode();
        Children nodeChildren = configDirFolder.createNodeChildren(VisibleDataFilter.VISIBLE_DATA_FILTER);        
        FilterNode nd = new ServiceConfigFolderNode(originalNode, nodeChildren);

////        FilterNode nd = new ServiceConfigFolderNode(configDirFolder.getNodeDelegate());

////        Node originalNode = configDirFolder.getNodeDelegate();
////        FilterNode nd = new ServiceConfigFolderNode(originalNode, new FileFilteredChildren(originalNode));

        return NodeFactorySupport.fixedNodeList(nd);
    }

    public static class ServiceConfigFolderNode extends FilterNode {

        private static final Image SVC_CONFIG_NODE_BADGE =
                ImageUtilities.loadImage("org/netbeans/modules/jbi/fuji/support/resources/config-badge.gif", true); // NOI18N


        public ServiceConfigFolderNode(Node original) {
            super(original);
            this.disableDelegation(DELEGATE_SET_DISPLAY_NAME | DELEGATE_GET_DISPLAY_NAME);
            this.setDisplayName("Service Config Files");
        }

        public ServiceConfigFolderNode(Node original, org.openide.nodes.Children children) {
            super(original, children);
            this.disableDelegation(DELEGATE_SET_DISPLAY_NAME | DELEGATE_GET_DISPLAY_NAME);
            this.setDisplayName("Service Config Files");
        }

        public ServiceConfigFolderNode(Node original, FileFilteredChildren children) {
            super(original, children);
            this.disableDelegation(DELEGATE_SET_DISPLAY_NAME | DELEGATE_GET_DISPLAY_NAME);
            this.setDisplayName("Service Config Files");
        }

        @Override
        public Image getIcon(int type) {
            Image img = computeIcon(false, type);
            return (img != null) ? img : super.getIcon(type);
        }

        @Override
        public Image getOpenedIcon(int type) {
            Image img = computeIcon(true, type);
            return (img != null) ? img : super.getIcon(type);
        }

        private Image computeIcon(boolean opened, int type) {
            Image image = super.getIcon(type); // NOI18N

            image = ImageUtilities.mergeImages(image, SVC_CONFIG_NODE_BADGE, 8, 8);
            // image = Utilities.mergeImages(image, NB_PROJECT_BADGE, 8, 0);
            return image;
        }
    }

    public static class FileFilteredChildren extends FilterNode.Children {

        public FileFilteredChildren(Node owner) {
            super(owner);
        }

        @Override
        protected Node[] createNodes(Node key) {
            List<Node> result = new ArrayList<Node>();

            DataObject dataObject = key.getLookup().lookup(DataObject.class);
            if (VisibleDataFilter.VISIBLE_DATA_FILTER.acceptDataObject(dataObject)) {
                FileObject fo = dataObject.getPrimaryFile();
                if ( fo.isFolder()) {
                    DataFolder folder = DataFolder.findFolder(fo);
                    Node orignal = folder.getNodeDelegate();
                    Children chld = folder.createNodeChildren(VisibleDataFilter.VISIBLE_DATA_FILTER);                    
                    result.add(new FilterNode(orignal, chld));
                } else {
                    result.add(new FilterNode(dataObject.getNodeDelegate()));
                }
            }
            return result.toArray(new Node[0]);
        }
    }
}
