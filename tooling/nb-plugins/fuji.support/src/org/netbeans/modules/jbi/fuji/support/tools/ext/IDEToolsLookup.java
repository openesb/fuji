/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.netbeans.modules.jbi.fuji.support.tools.ext;

import java.net.URL;
import org.netbeans.modules.jbi.fuji.support.mvn.MavenPlugin;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.artifact.Artifact;
import org.glassfish.openesb.tools.common.DefaultToolsLookup;

/**
 *
 * @author chikkala
 */
public class IDEToolsLookup extends DefaultToolsLookup {
    private static final Logger LOG = Logger.getLogger(IDEToolsLookup.class.getName());
    private static final String FUJI_GROUP_ID = "open-esb.fuji"; // NOI18N
    // final location for the service plugins (a.k.a service descriptors) with the
    // bundle name as <type>-service-plugin
    private static final String SERVICE_PLUGIN_GROUP_ID = FUJI_GROUP_ID + ".components.plugins"; //NOI18N
    private static final String SERVICE_PLUGIN_SUFFIX = "-service-plugin";

    private MavenPlugin mMvnPlugin;

    public IDEToolsLookup(MavenPlugin mvnPlugin) {
        this.mMvnPlugin = mvnPlugin;
    }

    private File locateServiceDescriptor(String serviceType) throws IOException {
        String artifactId = serviceType.trim().toLowerCase() + SERVICE_PLUGIN_SUFFIX;
        String version = null;
        try {
            version = this.mMvnPlugin.getFujiVersion();
            Artifact artifact =
                    mMvnPlugin.resolveArtifact(SERVICE_PLUGIN_GROUP_ID, artifactId, version);
            return artifact.getFile();
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            throw new IOException("Unable to locate service descriptor archive for service type: " + serviceType);
        }
    }
    
    @Override
    protected URL[] findClassLoaderURLs(String serviceType) {
        try {
            File sdJar = this.locateServiceDescriptor(serviceType);
            return new URL[]{sdJar.toURI().toURL()};
        } catch (Exception ex) {
            LOG.info("Unable to locate service descriptor bundle file for " + serviceType);
            return null;
        }
    }

}
