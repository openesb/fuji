/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.ifl.config;

import com.sun.jbi.fuji.maven.util.ServiceDescriptorWrapper;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport;
import org.netbeans.modules.jbi.fuji.support.ifl.actions.IFLGenerateAction;
import org.netbeans.modules.jbi.fuji.support.tools.ext.IDEAppBuilder;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;

/**
 *
 * @author chikkala
 */
public class ServiceConfigEditor {
    private static final Logger LOG = Logger.getLogger(ServiceConfigEditor.class.getName());

    public static final String GENERATE_ARTIFACTS_ACTION = "GenerateArtifacts";
    public static final String SAVE_CONFIG_ACTION = "SaveConfiguration";
    public static final String CANCEL_ACTION = "Cancel";

    public static String show(ServiceConfigModel model) {
        return show(model, false);
    }
    public static String show(ServiceConfigModel model, boolean forceGenerate) {
        ServiceConfigPanel panel = new ServiceConfigPanel(model, forceGenerate);
        DialogDescriptor configDialog = new DialogDescriptor(panel, "Service Configuration");
        Object result = DialogDisplayer.getDefault().notify(configDialog);
        String action = CANCEL_ACTION;
        if (NotifyDescriptor.OK_OPTION.equals(result)) {
            if (panel.isGenerateArtifactsSelected()) {
                action = GENERATE_ARTIFACTS_ACTION;
            } else {
                action = SAVE_CONFIG_ACTION;
            }
        }
        return action;
    }

    public static boolean edit(String serviceType, String serviceName, DataObject iflDO, boolean generateAction) {
        boolean edited = true;
        try {
            IDEAppBuilder appBuilder = IDEAppBuilder.newInstance(iflDO);
            ServiceDescriptor sd = appBuilder.getToolsLocator().getServiceDescriptor(serviceType);
            // check if the service configuration descriptor is not supported for this service type.
            // if not, then prompt for generate default service configuration optionally and then
            // open the configuration in a text editor.
            if (sd instanceof ServiceDescriptorWrapper ) {
                // open if exists or use maven to generate
                LOG.info("No ServiceDescriptor. Using maven plugin to generate configuration");
                try {
                    File configFile = new File(appBuilder.getServiceProjectModel(serviceType).getConfigDir(serviceName),
                            ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
                    if (configFile.exists()) {
                        FileObject configFO = FileUtil.toFileObject(configFile);
                        DataObject configDO = DataObject.find(configFO);
                        IFLSupport.editFile(configDO);
                    } else {
                        tryGenerateServiceConfigFileUsingMaven(iflDO, serviceType, serviceName);
                    }
                } catch (Exception ex) {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.log(Level.FINER, ex.getMessage(), ex);
                    }
                }

                return true;
            }
            //check if the description has no configuration. If yes. generate default.
            boolean configExists = true;
            if (sd.getConfigurations().size() == 0 ) {
                Configuration defConfig = sd.getDefaultConfiguration();
                if ( defConfig == null ) {
                    configExists = false;
                } else {
                    Properties defProps = defConfig.getProperties();
                    if (defProps == null || defProps.size() == 0) {
                        configExists = false;
                    }
                }
            }
            if (!configExists) {
                // generate default
                appBuilder.executeGenerateServiceConfiguration(sd, serviceName, new Properties());
                // use text editor to edit.
                try {
                    File configFile = new File(appBuilder.getServiceProjectModel(serviceType).getConfigDir(serviceName),
                            ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
                    if (configFile.exists() && !generateAction) {
                        FileObject configFO = FileUtil.toFileObject(configFile);
                        DataObject configDO = DataObject.find(configFO);
                        IFLSupport.editFile(configDO);
                    }
                } catch (Exception ex) {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.log(Level.FINER, ex.getMessage(), ex);
                    }
                }
                if (generateAction) {
                   appBuilder.executeGenerateServiceArtifacts(serviceType, serviceName, new Properties());
                }
                return true;
            }
            // mutliple configurations exists. so select one.
            edited = true;
            // show the supported configuration.
            ServiceConfigModel model = ServiceConfigModel.newInstance(sd, serviceName, appBuilder);
            String action = null;

            action = ServiceConfigEditor.show(model);

            if (ServiceConfigEditor.GENERATE_ARTIFACTS_ACTION.equals(action)) {

               Properties configProps = model.getSelectedConfigurationProperties();
               dumpProperties("Configuration properties to generate service", configProps);
               appBuilder.executeGenerateServiceConfiguration(sd, serviceName, configProps);
               appBuilder.executeGenerateServiceArtifacts(serviceType, serviceName, configProps);
            } else if ( ServiceConfigEditor.SAVE_CONFIG_ACTION.equals(action)) {
                Properties configProps = model.getSelectedConfigurationProperties();
                dumpProperties("Configuration properties to generate service", configProps);
                appBuilder.executeGenerateServiceConfiguration(sd, serviceName, configProps);
            }
            return edited;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void tryGenerateServiceConfigFileUsingMaven(DataObject iflDO, String serviceType, String serviceName) {
        boolean generate = IFLSupport.confirmGenerateArtifactsExecution();
        if (generate) {
            IFLGenerateAction.executeGenerateGoal(iflDO);
        }
    }
    
    private static void dumpProperties(String msg, Properties props) {
        if (LOG.isLoggable(Level.FINE)) {
            if (props == null) {
                LOG.fine("---Null Props for -- " + msg);
                return;
            }
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            props.list(out);
            LOG.fine("----- " + msg);
            LOG.fine(writer.getBuffer().toString());
        }
    }
}
