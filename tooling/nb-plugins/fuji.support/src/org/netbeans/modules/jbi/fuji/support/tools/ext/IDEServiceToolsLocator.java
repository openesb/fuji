/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.netbeans.modules.jbi.fuji.support.tools.ext;

import org.netbeans.modules.jbi.fuji.support.mvn.MavenPlugin;
import com.sun.jbi.fuji.maven.util.AbstractServiceToolsLocator;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.artifact.Artifact;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 * @author chikkala
 */
public class IDEServiceToolsLocator extends AbstractServiceToolsLocator {

    private static final Logger LOG = Logger.getLogger(IDEServiceToolsLocator.class.getName());

    private MavenPlugin mMvnPlugin;

    public IDEServiceToolsLocator(
            ToolsLookup toolsLookup,
            MavenPlugin mvnPlugin) {        
        super(toolsLookup);
        this.mMvnPlugin = mvnPlugin;
    }

    public List<ServiceDescriptor> getAvailableServiceDescriptors(){
        return this.getToolsLookup().getAvailableServiceDescriptors();
    }

    protected URL locateServiceArchetype(String serviceType) throws IOException {
        
        final String artifactId = serviceType.toLowerCase() + ARCHETYPE_SUFFIX;

        try {
            final Artifact artifact =
                    this.mMvnPlugin.resolveArtifact(COMP_ARCHETYPE_GROUP_ID, artifactId, this.mMvnPlugin.getFujiVersion());

            return artifact.getFile().toURI().toURL();
        } catch (Exception e) {
            LOG.log(Level.FINE, e.getMessage(), e);
            throw new IOException("Unable to locate archetype for service: " + serviceType);
        }
    }

    protected URL locateServicePlugin(String serviceType) throws IOException {

        final String artifactId = MAVEN_PREFIX + serviceType.toLowerCase() + PLUGIN_SUFFIX;

        try {
            final Artifact artifact =
                    this.mMvnPlugin.resolveArtifact(COMP_PLUGIN_GROUP_ID, artifactId, this.mMvnPlugin.getFujiVersion());

            return artifact.getFile().toURI().toURL();
        } catch (Exception e) {
            LOG.log(Level.FINE, e.getMessage(), e);
            throw new IOException("Unable to locate maven plugin for service: " + serviceType);
        }
    }

    protected URL locateServiceInstaller(String componentName) throws IOException {

        try {
            final Artifact artifact =
                    this.mMvnPlugin.resolveArtifact(COMP_INSTALLER_GROUP_ID, componentName, this.mMvnPlugin.getFujiVersion());
            return artifact.getFile().toURI().toURL();
        } catch (Exception e) {
            LOG.log(Level.FINE, e.getMessage(), e);
            throw new IOException("Unable to locate jbi component installer for: " + componentName);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String BASE_GROUP_ID =
            "open-esb.fuji"; // NOI18N
    private static final String COMP_INSTALLER_GROUP_ID =
            BASE_GROUP_ID + ".components.installers"; // NOI18N
    private static final String COMP_ARCHETYPE_GROUP_ID =
            BASE_GROUP_ID + ".components.archetypes"; // NOI18N
    private static final String COMP_PLUGIN_GROUP_ID =
            BASE_GROUP_ID + ".components.plugins"; // NOI18N
    private static final String MAVEN_PREFIX = 
            "maven-"; // NOI18N
    private static final String ARCHETYPE_SUFFIX = 
            "-archetype"; // NOI18N
    private static final String PLUGIN_SUFFIX = 
            "-plugin"; // NOI18N
}
