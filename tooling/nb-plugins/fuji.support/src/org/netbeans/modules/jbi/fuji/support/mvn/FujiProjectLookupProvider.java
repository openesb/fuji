/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */

package org.netbeans.modules.jbi.fuji.support.mvn;

import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.LookupProvider;
import org.netbeans.spi.project.ui.PrivilegedTemplates;
import org.netbeans.spi.project.ui.RecommendedTemplates;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author chikkala
 */
public class FujiProjectLookupProvider implements LookupProvider {

    public Lookup createAdditionalLookup(Lookup baseContext) {
        //TODO: check if the maven project is a fuji project. if not return an
        // empty lookup
        Project project = baseContext.lookup(Project.class);
        if (project == null) {
            //log warning
            Logger.getLogger("").warning("Lookup " + baseContext + " does not contain a Project. Some Fuji Lookup provider items will be disabled.");
        }
        Lookup fixedLookup = Lookups.fixed(new Object[]{
                new RecommendedTemplatesImpl(project),
                new FujiExecutionChecker(project)
            });
        return fixedLookup;
    }

    /**
     * @see org.netbeans.spi.project.ui.RecommendedTemplates
     * @see org.netbeans.spi.project.ui.PrivilegedTemplates
     */
    private static final class RecommendedTemplatesImpl implements RecommendedTemplates, PrivilegedTemplates {

        private Project mPrj;        // List of primarily supported templates
        private static final String[] TYPES = new String[]{
            /* TODO: add any other recommended templates 
            "java-classes",        // NOI18N
            "ejb-types",            // NOI18N
            "java-beans",           // NOI18N
            "oasis-XML-catalogs",   // NOI18N
            "XML",                  // NOI18N
            "ant-script",           // NOI18N
            "ant-task",             // NOI18N
            "simple-files"          // NOI18N
             */
            "fuji-classes"        // NOI18N
//            "java-beans",           // NOI18N           
//            "Camel",
//            "SOA",
//            "XML",                  // NOI18N
//            "wsdl",                 // NOI18N
//            "junit",                // NOI18N
//            "simple-files"          // NOI18N
        };
        private static final String[] PRIVILEGED_NAMES = new String[]{
            /* TODO: add any other privileged names */
            "Templates/Fuji/Interceptor.java", // NOI18N
            "Templates/Fuji/ServiceConsumerImpl.java", // NOI18N
            "Templates/Fuji/ServiceProviderImpl.java", // NOI18N
            "Templates/Fuji/ServiceMessageImpl.java", // NOI18N
            "Templates/Fuji/ServiceMessageFactoryImpl.java"   // NOI18N       
        };

        public RecommendedTemplatesImpl(Project prj) {
            this.mPrj = prj;
        }


        public boolean isFujiMavenProject() {
           return MvnUtils.UTILS.isFujiMavenProject(this.mPrj);
        }

        public String[] getRecommendedTypes() {
            return TYPES;
            // return new String[0];
        }

        public String[] getPrivilegedTemplates() {
            if (isFujiMavenProject()) {
                return PRIVILEGED_NAMES;
            } else {
                return new String[0];
            }
        }
    }
}
