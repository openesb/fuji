/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.modules.jbi.fuji.support.options;

import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;

public final class FujiAdvancedOption extends AdvancedOption {

    public String getDisplayName() {
        return NbBundle.getMessage(FujiAdvancedOption.class, "AdvancedOption_DisplayName_Fuji");
    }

    public String getTooltip() {
        return NbBundle.getMessage(FujiAdvancedOption.class, "AdvancedOption_Tooltip_Fuji");
    }

    public OptionsPanelController create() {
        return new FujiOptionsPanelController();
    }
}
