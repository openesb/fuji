/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.ifl.actions;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.JTextComponent;
import org.netbeans.api.languages.ASTNode;
import org.netbeans.modules.jbi.fuji.support.ifl.IFLSupport;
import org.netbeans.modules.jbi.fuji.support.ifl.config.ServiceConfigEditor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.loaders.DataObject;
import org.openide.util.NbBundle;

/**
 *
 * @author chikkala
 */
public class IFLEditConfigAction  extends IFLTextAction {

    private static final Logger LOG = Logger.getLogger(IFLEditConfigAction.class.getName());

    public IFLEditConfigAction() {
        super();
        LOG.setLevel(Level.FINEST);
    }

    public String getName() {
        return NbBundle.getMessage(IFLEditConfigAction.class, "CTL_IFLEditConfigAction");
    }

    public boolean isEnabled(ASTNode node, JTextComponent comp) {
        return IFLSupport.enabledEditConfig(node, comp);
    }

    public void actionPerformed(ASTNode node, JTextComponent comp) {
        if (!performEditServiceConfig(node, comp)) {
            // just use old style that invokes the text editor.
            IFLSupport.performEditConfig(node, comp);
        }
    }

    public boolean performEditServiceConfig(ASTNode node, JTextComponent comp) {
        boolean edited = false;
        DataObject iflDO = IFLSupport.getEditingDataObject(comp);
        int offset = comp.getCaretPosition();
        ASTNode serviceNode = null;
        serviceNode = IFLSupport.getServiceNode(node, offset);
        if ( serviceNode == null) {
            LOG.info("NO Service Selected");
            return false;
        }

        String serviceType = IFLSupport.getServiceType(serviceNode);
        LOG.info("Service Type : " + serviceType);
        String serviceGroupName = IFLSupport.getServiceName(serviceNode, true);
        LOG.info("Service Group: " + serviceGroupName);
        if (serviceType == null || serviceGroupName == null ) {
            // not a service node. return false;
            return false;
        }
        String serviceName = IFLSupport.getServiceNameInServiceGroup(serviceNode, offset, true);
        if ( serviceName == null ) {
            // on service group. so. allow it only if the group is same as the provider (i.e. provider list is zero.
            Set<String> providers = IFLSupport.getGroupedProviderServices(serviceNode, true);
            if ( providers.size() > 0 ) {
                showChooseOneServiceInfoDialog();
                return true;
            } else {
                // make the service group as the service
                serviceName = serviceGroupName;
            }
        }
        edited = ServiceConfigEditor.edit(serviceType, serviceName, iflDO, false);
        if (!edited) {
            // just open the configuration in text editor if it exists. or ask user to run generate artifacts.
            ServiceConfigEditor.tryGenerateServiceConfigFileUsingMaven(iflDO, serviceType, serviceName);
        }
        return true;
    }

    public static void showChooseOneServiceInfoDialog() {
        String msg = "<html><p>Can not perform edit configuration on Service Group Name.  <br>" +
                "<p>To edit the configuration for the Service in a Group, <br>" +
                "place the cursor on a specific service name in a group and <br>" +
                "invoke the \"Edit Configuration\" action.";
        NotifyDescriptor msgDialog = new NotifyDescriptor.Message(msg, NotifyDescriptor.INFORMATION_MESSAGE);
        Object result = DialogDisplayer.getDefault().notify(msgDialog);
    }


}
