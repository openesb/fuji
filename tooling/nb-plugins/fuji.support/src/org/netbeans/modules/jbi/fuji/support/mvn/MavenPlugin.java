/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package org.netbeans.modules.jbi.fuji.support.mvn;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.embedder.MavenEmbedder;
import org.apache.maven.model.Plugin;
import org.apache.maven.project.MavenProject;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.maven.api.NbMavenProject;
import org.netbeans.modules.maven.embedder.EmbedderFactory;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;

/**
 * This class provides the resources related to maven project environment to generate, build, package
 * service artifacts for a specific service type.
 *
 * It is basically a bridge between maven ide environment and the fuji tools 
 * 
 * @author chikkala
 */
public class MavenPlugin {
    private static Logger  LOG = Logger.getLogger(MavenPlugin.class.getName());
    // since 1.0-M9-SNAPSHOT, so default.
    public static final String DEF_FUJI_VERSION = "1.0-M9-SNAPSHOT";
    public static final String FUJI_GROUP_ID = "open-esb.fuji";
    public static final String FUJI_PLUGIN_ID = "maven-fuji-plugin";

    private MavenEmbedder mMvnEmbedder;
    private ArtifactRepository mLocalRepository;
    private List<ArtifactRepository> mRemoteRepositories;

    private String mFujiVersion;

    private File mPrjBaseDir;
    private String mPrjName;
    private String mPrjArtifactId;    

    private MavenPlugin() {
        this(DEF_FUJI_VERSION);
    }

    private MavenPlugin(String fujiVersion) {       
        this.mFujiVersion = fujiVersion;
        if ( this.mFujiVersion == null ) {
            LOG.info("Fuji Version to create plugin is NULL. using default fuji version " + DEF_FUJI_VERSION);
            this.mFujiVersion = DEF_FUJI_VERSION;
        }
        LOG.info("#####!!!!! creating fuji maven plugin for version " + this.mFujiVersion);
        this.mMvnEmbedder = EmbedderFactory.getOnlineEmbedder();
        this.mLocalRepository = mMvnEmbedder.getLocalRepository();
        this.mRemoteRepositories = new ArrayList<ArtifactRepository>();
        ArtifactRepository fujiRepo = EmbedderFactory.createRemoteRepository(mMvnEmbedder, MvnUtils.FUJI_REPO_URL, MvnUtils.FUJI_REPO_ID);
        if ( fujiRepo != null) {
            this.mRemoteRepositories.add(fujiRepo);
        }
    }

    public Artifact resolveArtifact(
            String groupId,
            String artifactId,
            String version) throws Exception {

        Artifact artifact = this.mMvnEmbedder.createArtifactWithClassifier(
                groupId, artifactId, version, "jar", null);
        this.mMvnEmbedder.resolve(artifact,
                this.mRemoteRepositories, this.mLocalRepository);
        return artifact;
    }

    public File getProjectBaseDir() {
        return this.mPrjBaseDir;
    }

    protected void setProjectBaseDir(File baseDir) {
        this.mPrjBaseDir = baseDir;
    }

    public String getProjectName() {
        return this.mPrjName;
    }

    protected void setProjectName(String name) {
        this.mPrjName = name;
    }

    public String getProjectArtifactId() {
        return this.mPrjArtifactId;
    }

    public void setProjectArtifactId(String artifactId) {
        this.mPrjArtifactId = artifactId;
    }
    public String getFujiVersion() {
        return this.mFujiVersion;
    }

    public static MavenPlugin newInstance(File baseDir, String projectName, String projectArtifactId, String fujiVersion) {

        MavenPlugin plugin = new MavenPlugin(fujiVersion);

        plugin.setProjectBaseDir(baseDir);
        plugin.setProjectName(projectName);
        plugin.setProjectArtifactId(projectArtifactId);

        LOG.info("Created Fuji Maven IDE Plugin");
        LOG.info("Basedir: " + plugin.getProjectBaseDir().getAbsolutePath());
        LOG.info("Project name: " + plugin.getProjectName());
        LOG.info("Artifact ID: " + plugin.getProjectArtifactId());
        LOG.info("Fuji Version: " + plugin.getFujiVersion());
        
        return plugin;
    }

    public static MavenPlugin getMavenPlugin(DataObject iflDO) {
        MavenPlugin plugin = null;
        MavenProject mvnPrj = null;

        File baseDir = null;
        String prjName = null;
        String prjArtifactId = null;
        String fujiVersion = DEF_FUJI_VERSION;

        Project nbPrj = FileOwnerQuery.getOwner(iflDO.getPrimaryFile());
        if ( nbPrj != null ) {
            NbMavenProject nbMvnPrj = nbPrj.getLookup().lookup(NbMavenProject.class);
            if ( nbMvnPrj != null ) {
                mvnPrj = nbMvnPrj.getMavenProject();
            }
        }
        
        if ( mvnPrj != null ) {
           mvnPrj.getPluginManagement().getPluginsAsMap();
           Plugin mvnFujiPlugin = mvnPrj.getPlugin(Plugin.constructKey(FUJI_GROUP_ID, FUJI_PLUGIN_ID));

           fujiVersion = mvnFujiPlugin.getVersion();
           baseDir = mvnPrj.getBasedir();
           prjName = mvnPrj.getName();
           prjArtifactId = mvnPrj.getArtifactId();
        } else {
            // No maven project. Use IDE prj to initialize the project settings.
            if (nbPrj != null) {
                fujiVersion = DEF_FUJI_VERSION;
                baseDir = FileUtil.toFile(nbPrj.getProjectDirectory());
                prjName = ProjectUtils.getInformation(nbPrj).getName();
                prjArtifactId = prjName;
            } else {
                // init based on ifl dataobject parent directory
                fujiVersion = DEF_FUJI_VERSION;
                baseDir = FileUtil.toFile(iflDO.getPrimaryFile().getParent());
                prjName = baseDir.getName();
                prjArtifactId = prjName;
            }
        }

        plugin = newInstance(baseDir, prjName, prjArtifactId, fujiVersion);
        return plugin;
    }

}
