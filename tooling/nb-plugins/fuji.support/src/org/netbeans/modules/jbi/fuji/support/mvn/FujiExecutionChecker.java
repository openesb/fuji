/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.netbeans.modules.jbi.fuji.support.mvn;

import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jbi.fuji.server.plugin.actions.DeployAction;
import org.netbeans.modules.maven.api.execute.ExecutionContext;
import org.netbeans.modules.maven.api.execute.ExecutionResultChecker;
import org.netbeans.modules.maven.api.execute.PrerequisitesChecker;
import org.netbeans.modules.maven.api.execute.RunConfig;
import org.openide.windows.OutputWriter;

/**
 *
 * @author chikkala
 */
public class FujiExecutionChecker  implements ExecutionResultChecker, PrerequisitesChecker {
    private static final Logger LOG = Logger.getLogger(FujiExecutionChecker.class.getName());
    public static final String PROP_NB_FUJI_DEPLOY = "netbeans.fuji.deploy"; //NOI18N
    Project mPrj;
    public FujiExecutionChecker(Project prj) {
        this.mPrj = prj;
    }
    public void executionResult(RunConfig config, ExecutionContext res, int resultCode) {
        LOG.info("Fuji Execution result code " + resultCode);
        boolean deploy = Boolean.parseBoolean(config.getProperties().getProperty(PROP_NB_FUJI_DEPLOY));
        if (deploy && resultCode == 0) {
            OutputWriter err = res.getInputOutput().getErr();
            OutputWriter out = res.getInputOutput().getOut();
            DeployAction.perform(config.getProject(), out, err);
        }
    }

    public boolean checkRunConfig(RunConfig config) {
        boolean deploy = Boolean.parseBoolean(config.getProperties().getProperty(PROP_NB_FUJI_DEPLOY));
        return true;
    }

}
