require 'java'
require 'XPath'

@@id_map = {"ABC123"=>"{12DJE23JDKCL229011}"}


def process(inMsg)
  doc = inMsg.getPayload
  # your code goes here
  shipperId = XPath.findString(doc, "//Container/Shipper/Id")
  if @@id_map[shipperId]
    shipper = XPath.findNode(doc, "//Container/Shipper")
    euid = shipper.getOwnerDocument.createElement("EUID")
    euid.setTextContent(@@id_map[shipperId])
    shipper.getParentNode.insertBefore(euid, shipper)
  end
  # return message  
  inMsg
end