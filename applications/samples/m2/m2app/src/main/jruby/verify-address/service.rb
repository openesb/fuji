require 'java'
require 'XPath'
require 'ESB'
include_class "javax.xml.namespace.QName"

@@service = QName.new "http://fuji.dev.java.net/application/m2mdm", "mdm-service"

def process(inMsg)
  doc = inMsg.getPayload
  ESB.invoke(@@service, "executeMatch", XPath.findNode(doc,"//Shipper"))
  # return enriched message  
  inMsg
end