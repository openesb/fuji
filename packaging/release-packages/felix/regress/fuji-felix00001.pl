$testname = "fuji-felix00001";

#where are we?
if ( -d "target" ) {
    $project_build_directory = "target";
} elsif ( -d "../target" ) {
    $project_build_directory = "../target";
} else {
    printf "%s:  cannot locate %s - abort test\n", $testname, '${project.build.directory}';
    exit 1;
}

$mavenProperties = "$project_build_directory/jregress-results/maven.properties";
printf "mavenProperties is '%s'\n", $mavenProperties;
if ( !(-r $mavenProperties ) ) {
    printf "%s:  cannot locate %s - abort test\n", $testname, '${mavenProperties}';
    exit 1;
}

if (!open(MVNPROPS, $mavenProperties)) {
    printf "%s:  cannot read maven properties file '%s' - abort test\n", $testname, $mavenProperties;
    exit 1;
}

#######
#inject maven properties into this script:
#######
while (<MVNPROPS>) {
    chomp($_);
    $_ =~ s/\\/\//g;  #convert dos path separators
    eval(sprintf("%s%s='%s';", '$', split("=", $_, 2)));
}
close MVNPROPS;

#by convention we are testing the parent artifact, not the jregress artifact:
$artifactId =~ s/-jregress//;
$finalName = "$build_directory/$artifactId-$version.$packaging";

$testtmpdir = sprintf("%s/%s", $jregressOutputDirectory, $testname);
mkdir $testtmpdir;

#important to be in the right place before we unjar the bundle:
if (!chdir($testtmpdir)) {
    printf "%s:  cannot read cd to test tmp dir '%s' - abort test\n", $testname, $testtmpdir;
    exit 1;
}

#get jar and unjar it:
system("jar xf $finalName");
chdir "fuji";

sub send_command
{
    my ($cmd) = @_;
    print $cmd . "\r";
    sleep 2;
}

#open a pipe, and send some commands:
$felixshell = "java  -Xmx400m -jar bin/felix.jar";
    #NOTE:  equivalent to:  java -cp bin/felix.jar org.apache.felix.main.Main

if (!open(FELIX, "| $felixshell")) {
    printf "%s:  cannot open pipe to command '%s' - abort test\n", $testname, $felixshell;
    exit 1;
}

#this unbuffers i/o on our pipe.  required!
select FELIX; $| = 1;

#give felix time to start oscar (basic shell bundle - see http://oscar-osgi.sourceforge.net/):
sleep 40;

&send_command("version");
&send_command("help");
&send_command("ps");
&send_command("ps -l");
&send_command("shutdown");

close FELIX;
exit 0;
