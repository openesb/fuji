Build Number : Build${BUILD_NUMBER}
Release      : ${RELEASE_NAME}

1. Howdy!  
Thanks for downloading the lightwieght distribution of Project Fuji.  This 
README provides some high-level details on what's in the distribution and 
where to go for more information.  If you haven't checked it out yet, please 
go to the Fuji website for late breaking developments and other goodies at:

   https://fuji.dev.java.net/

2. Overview

   The main purpose of this distribution is to provide a smaller footprint Fuji
   runtime in an OSGi environment so that users can quickly download and use it
   as a Fuji runtime as well as an OSGi platform with less overhead.

   This distribution includes only the Fuji framework and Apache Felix OSGi 
   framework and supporting technologies (e.g. Shell UI, Event Admin, etc). 
   The OSGi based JBI Components and Fuji web-ui that come with the standard 
   distribution are not included in this distribution. 
   
   Depending on your needs, you can add more OSGi bundles or OSGi based JBI 
   Components to the runtime using using standard OSGi bundle installation 
   commands from CLI (shell ui) or IDE based tools (e.g. Fuji server manager
   in NetBeans IDE).

3. Contents

3.1 Included in the distribution:
   * Fuji framework 
        NMR, Simple API, Fuji EIP support(IFL) and JBI Component management.
   * Fuji Admin bundles 
        * Fuji/OSGi runtime adminitration via JMX to manage deployment and 
          runtime lifecycle of OSGi bundles, JBI Components, JBI Service 
          Assemblies (Fuji apps) and Fuji interceptors.
        * Fuji CLI using Felix shell interface for Fuji commands such as 
          logger settings, log file manamagement, configuration settings. 
   * Apache Felix OSGi framework and supporting technologies (e.g. Shell UI and 
     Event Admin)

3.2 File Layout

   fuji/
      bin/               # location of felix framework
      bundle/            # fuji bundles
      conf/              # fuji configuration
      README.txt         # no one ever reads this


4. Useful Links

    4.1 Fuji portal
            https://fuji.dev.java.net/
    4.2 HowTo
            http://wiki.open-esb.java.net/Wiki.jsp?page=FujiHowTo
    4.2 Technical Details
            http://wiki.open-esb.java.net/Wiki.jsp?page=FujiDetails
    4.3 Screencasts & Demos
            http://wiki.open-esb.java.net/Wiki.jsp?page=FujiScreenCastsDemos
    4.4 Milestone Details
            http://wiki.open-esb.java.net/Wiki.jsp?page=FujiMilestones
    4.5 Gregor Hohpe's excellent text on Enterprise Integration Patterns
            http://enterpriseintegrationpatterns.com/
