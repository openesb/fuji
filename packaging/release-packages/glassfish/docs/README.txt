Build Number : Build${BUILD_NUMBER}
Release      : ${RELEASE_NAME}

Thanks for downloading this distribution of Project Fuji. 

For details on what is included, please visit this page and click on the
link for the appropriate milestone:
http://wiki.open-esb.java.net/Wiki.jsp?page=FujiMilestones

Basic instructions for running Fuji on Felix:

Startup

* Make sure you are in the "fuji" directory of the distribution 
* Enter this command:  java -jar bin/felix.jar
* When you see the message "Welcome to Felix", Fuji is up and running

Shutdown

* From the Felix command prompt, simply enter the "shutdown" command

Running the Web UI

* After startup has completed, you can access the Web UI using this URL:

  http://localhost:8080/fuji/editor


If you run into problems with Fuji, please file an issue here:

  https://open-esb.dev.java.net/servlets/ProjectIssues

File the issues under component "fuji" and choose the appropriate
sub-component.
