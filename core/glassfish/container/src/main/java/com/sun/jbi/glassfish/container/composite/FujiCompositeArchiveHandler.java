/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCompositeArchiveHandler.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.composite;


import java.io.IOException;

import org.glassfish.api.deployment.archive.ReadableArchive;
import org.glassfish.api.deployment.archive.WritableArchive;
import org.glassfish.api.deployment.archive.CompositeHandler;
import org.glassfish.api.deployment.DeploymentContext;
import org.glassfish.api.deployment.DeployCommandParameters;
import org.glassfish.api.deployment.archive.ArchiveHandler;
import com.sun.enterprise.util.io.FileUtils;
import org.glassfish.internal.deployment.Deployment;
import org.glassfish.internal.deployment.ExtendedDeploymentContext;
import org.jvnet.hk2.annotations.Service;
import org.jvnet.hk2.annotations.Inject;

import com.sun.enterprise.deploy.shared.ArchiveFactory;
import org.jvnet.hk2.component.Habitat;
import org.glassfish.api.admin.ServerEnvironment;
import org.glassfish.api.ActionReport;
import org.glassfish.deployment.common.DeploymentContextImpl;
import org.xml.sax.SAXParseException;

import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.sun.enterprise.deploy.shared.AbstractArchiveHandler;
import com.sun.enterprise.deployment.util.ModuleDescriptor;
import com.sun.enterprise.deployment.deploy.shared.InputJarArchive;
import com.sun.enterprise.deployment.deploy.shared.Util;
import com.sun.enterprise.config.serverbeans.Domain;
//import com.sun.enterprise.loader.EJBClassLoader;

import com.sun.jbi.glassfish.container.FujiArchiveHandler;

import com.sun.jbi.glassfish.container.utils.FujiUtils;
import com.sun.jbi.glassfish.container.utils.LocalStringKeys;
import com.sun.jbi.glassfish.container.utils.StringTranslator;

import com.sun.enterprise.util.io.FileUtils;


/**
 * FujiCompositeArchiveHandler handles the service assembly archive
 */
@Service(name="FujiCompositeArchiveHandler")
public class FujiCompositeArchiveHandler extends AbstractArchiveHandler implements CompositeHandler {

    /**
     * deployment
     */
    @Inject
    Deployment deployment;

    /**
     * habitat
     */
    @Inject
    Habitat habitat;

    /**
     * archive factory
     */
    @Inject
    ArchiveFactory archiveFactory;

    /**
     * server environment
     */
    @Inject
    ServerEnvironment env;

    /**
     * domain
     */
    @Inject
     Domain domain;

    /**
     * logger
     */
    Logger logger_ = Logger.getLogger(this.getClass().getPackage().getName());

    /**
     * String translator
     */
    StringTranslator translator_ = new StringTranslator("com.sun.jbi.glassfish.container.utils", null);

    /**
     * get the archive type
     * @return the archive type
     */
    public String getArchiveType() {
        return "FujiComposite";
    }

    /**
     * Returns true if the passed file or directory is recognized by this
     * instance.
     *
     * @param location the file or directory to explore
     * @param loader class loader for this application
     * @return true if this sniffer handles this application type
     */
    public boolean handles(ReadableArchive source){//, ClassLoader loader) {
        try {
            if (source.exists(FujiUtils.JBI_XML)) {
                return FujiUtils.isApplication(source) || FujiUtils.isComponent(source) || FujiUtils.isSharedLibrary(source);
            }
        } catch (IOException e) {
            // ignore
        }
        return false;   
    }

    /**
     * expand the arvhive
     */
    @Override
    public void expand(ReadableArchive source,
                      WritableArchive target,
                      DeploymentContext context) throws IOException
    {

        if (FujiUtils.isApplication(source)) {
            expandSA(source,target,context);
        } else {
            expandComponent(source, target, context);
        }
    }

    /**
     * expand the arvhive
     */
    public void expandComponent(ReadableArchive source,
                      WritableArchive target,
                      DeploymentContext context) throws IOException
    {
        for (ArchiveHandler handler : habitat.getAllByContract(ArchiveHandler.class)) {
            if (handler instanceof FujiArchiveHandler) {
                handler.expand(source, target, context);

            }
        }
    }

    /**
     * expand the arvhive
     */
    public void expandSA(ReadableArchive source,
                      WritableArchive target,
                      DeploymentContext context) throws IOException
    {
                
        super.expand(source, target, context);
        WritableArchive serviceAssembly = null;
        WritableArchive subTarget = null;
        ReadableArchive subArchive = null;
        ReadableArchive source2 = null;
        try {
            source2 = archiveFactory.openArchive(target.getURI());

            //this will create and set metadata in context
            FujiCompositeApplicationHolder holder = getApplicationHolder(source2, context, false);


            for (ModuleDescriptor md : holder.app.getModules()) {

                if(md.getModuleType().equals(FujiModuleType.FUJI_COMP)) {
                    //lookup FujiArchiveHandler and hand it off
                    String saRoot =
                            domain.getApplicationRoot() + File.separator +
                            holder.app.getName() + File.separator + 
                            FileUtils.makeFriendlyFilename(holder.app.getName())+FujiUtils.CONTAINER_CREATED_SA_NAME;
                    subArchive = archiveFactory.openArchive(new File(saRoot));
                    //while extracting you have to deal with special chars like . in 1.0-SNAPSHOT
                    subTarget = target.createSubArchive(FileUtils.makeFriendlyFilename(subArchive.getName()));
                    java.util.Collection test = habitat.getAllByContract(ArchiveHandler.class);

                    for (ArchiveHandler handler : habitat.getAllByContract(ArchiveHandler.class)) {
                        if (handler instanceof FujiArchiveHandler) {
                            handler.expand(subArchive, subTarget, context);

                        }
                    }
                }
                else {
                    String moduleUri = md.getArchiveUri();
                    subArchive = source.getSubArchive(moduleUri);                     
                    ArchiveHandler subHandler = deployment.getArchiveHandler(subArchive);
                    if (subHandler!=null) {
                        subTarget = target.createSubArchive(
                            //FileUtils.makeFriendlyFilename(subArchive.getName()));
                            subArchive.getName());
                        subHandler.expand(subArchive, subTarget, context);                       

                    }
                }
            }
        }catch(Exception ex) {
            String msg = translator_.getString(LocalStringKeys.COMPOSITE_APP_EXPAND_FAILED);
            logger_.log(Level.WARNING, msg, ex);

        } finally {
            if (subArchive != null) {
                subArchive.close();
            }
            if (subTarget != null) {
                subTarget.close();
            }
        }
    }

    /**
     * This method is used to find out if this archive handler accepts an archive
     * @param source
     * @param entryName
     * @return
     */
    public boolean accept(ReadableArchive source, String entryName) {
        // I am hiding everything but the metadata.
        return entryName.startsWith("META-INF");

    }

    /**
     * This method is used to get the classloader associated with the archive handler
     */
    public ClassLoader getClassLoader(ClassLoader parent, DeploymentContext context) {

        if (FujiUtils.isApplication(context.getSource())) {
            return getSAClassLoader(parent, context);
        } else {
            return getComponentClassloader(parent, context);
        }

    }


    /**
     * This method is used to get the classloader associated with the archive handler
     */
    public ClassLoader getComponentClassloader(ClassLoader parent, DeploymentContext context) {
 
        for (ArchiveHandler handler : habitat.getAllByContract(ArchiveHandler.class)) {
            if (handler instanceof FujiArchiveHandler) {
                return handler.getClassLoader(parent, context);

            }
        }
        //TODO print a warning here
        return null;
    }


    /**
     *
     */
    
    /**
     * This method is used to get the classloader associated with the archive handler
     */
    public ClassLoader getSAClassLoader(ClassLoader parent, DeploymentContext context) {

        // TODO we are using a EJB classloader because it is simple
        java.net.URLClassLoader cloader;
        try { 
	    java.net.URL[] urls = { context.getSource().getURI().toURL() };
            cloader = new java.net.URLClassLoader(urls, parent);
        } catch(MalformedURLException e) {

            String msg = translator_.getString(LocalStringKeys.COMPOSITE_ARCHIVE_HANDLER_CLASS_LOADER_FAILED);
            logger_.log(Level.WARNING, msg, e);
            return null;
        }

        FujiCompositeApplicationHolder holder =
                context.getModuleMetaData(FujiCompositeApplicationHolder.class);

        for (ModuleDescriptor md : holder.app.getModules()) {

            String moduleURI = md.getArchiveUri();
            ReadableArchive subArchive = null;
              
            try {
                if(md.getModuleType().equals(FujiModuleType.FUJI_COMP)) {

                    String saRoot = domain.getApplicationRoot() + File.separator +
                            holder.app.getName() + File.separator + 
                            FileUtils.makeFriendlyFilename(holder.app.getName())+"-container-created-sa";
                    subArchive = archiveFactory.openArchive(new File(saRoot));

                    for (ArchiveHandler handler : habitat.getAllByContract(ArchiveHandler.class)) {
                        if (handler instanceof FujiArchiveHandler) {
                            context.getModuleArchiveHandlers().put(moduleURI, handler);
                        }
                    }

                }  else {
                    subArchive = context.getSource().getSubArchive(moduleURI);

                }
            } catch (IOException ex) {
                String msg = translator_.getString(LocalStringKeys.COMPOSITE_ARCHIVE_HANDLER_CLASS_LOADER_FAILED);
                logger_.log(Level.WARNING, msg, ex);
            }


             if (subArchive!=null) {
                 try {
                     ArchiveHandler handler =
                         context.getModuleArchiveHandlers().get(moduleURI);
                     if (handler == null) {
                         handler = deployment.getArchiveHandler(subArchive);
                         context.getModuleArchiveHandlers().put(
                             moduleURI, handler);
                     }

                     if (handler!=null) {
                         ActionReport subReport =
                             context.getActionReport().addSubActionsReport();
                         // todo : this is a hack, once again,
                         // the handler is assuming a file:// url
                         ExtendedDeploymentContext subContext =
                             new DeploymentContextImpl(subReport,
                             context.getLogger(),
                             subArchive,
                             context.getCommandParameters(
                                 DeployCommandParameters.class), env) {

                             @Override
                             public File getScratchDir(String subDirName) {
                                 String modulePortion = Util.getURIName(
                                     getSource().getURI());
                                 return (new File(super.getScratchDir(
                                     subDirName), modulePortion));
                             }
                         };

                         // sub context will store the root archive handler also
                         // so we can figure out the enclosing archive type
                         subContext.setArchiveHandler(context.getArchiveHandler());
                         subArchive.setParentArchive(context.getSource());
                         ClassLoader subCl = handler.getClassLoader(cloader, subContext);
                         //cloader.addModuleClassLoader(moduleURI, subCl);
                     }
                } catch (IOException ex) {
                    String msg = translator_.getString(LocalStringKeys.COMPOSITE_ARCHIVE_HANDLER_CLASS_LOADER_FAILED);
                    logger_.log(Level.WARNING, msg, ex);
                }

             }
        }

        return cloader;
    }



    /**
     * This method is used to get an instance of FujiCompositeApplicationHolder
     */
    private FujiCompositeApplicationHolder getApplicationHolder(
            ReadableArchive source,
            DeploymentContext context,
            boolean isDirectory)
    {
        FujiCompositeApplicationHolder holder = context.getModuleMetaData(FujiCompositeApplicationHolder.class);
        if (holder == null || holder.app == null) {
            try {
                long start = System.currentTimeMillis();
                FujiCompositeApplicationArchivist archivist = habitat.getComponent(FujiCompositeApplicationArchivist.class);
                //not handling directory deployment yet - so no second isDirectory param
                holder = new FujiCompositeApplicationHolder(archivist.createApplication(source));
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (SAXParseException e) {
                throw new RuntimeException(e);
            }
            context.addModuleMetaData(holder);
        }

        if (holder.app == null) {
            String msg = translator_.getString(LocalStringKeys.FAILED_TO_CREATE_APPLICATION_HOLDER);
            throw new RuntimeException(msg);
        }
        return holder;
    }
} 
