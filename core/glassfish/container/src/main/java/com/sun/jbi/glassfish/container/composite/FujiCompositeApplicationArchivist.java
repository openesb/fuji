/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCompositeApplicationArchivist.java
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.composite;

import com.sun.enterprise.deploy.shared.ArchiveFactory;
import com.sun.enterprise.deploy.shared.FileArchive;
import com.sun.enterprise.deployment.Application;
import com.sun.enterprise.deployment.BundleDescriptor;
import com.sun.enterprise.deployment.RootDeploymentDescriptor;
import com.sun.enterprise.deployment.annotation.introspection.EjbComponentAnnotationScanner;
import com.sun.enterprise.deployment.deploy.shared.InputJarArchive;
import com.sun.enterprise.deployment.io.ApplicationDeploymentDescriptorFile;
import com.sun.enterprise.deployment.io.DeploymentDescriptorFile;
import com.sun.enterprise.deployment.io.runtime.ApplicationRuntimeDDFile;
import com.sun.enterprise.deployment.archivist.CompositeArchivist;
import com.sun.enterprise.deployment.archivist.Archivist;
import com.sun.enterprise.deployment.archivist.AppClientArchivist;
import com.sun.enterprise.deployment.archivist.EjbArchivist;
import com.sun.enterprise.deployment.util.ModuleDescriptor;
import com.sun.enterprise.deployment.util.AnnotationDetector;

import com.sun.enterprise.util.io.FileUtils;
import com.sun.enterprise.util.shared.ArchivistUtils;
import org.glassfish.api.deployment.archive.ReadableArchive;
import org.glassfish.api.deployment.archive.WritableArchive;
import org.jvnet.hk2.annotations.Inject;
import org.jvnet.hk2.annotations.Scoped;
import org.jvnet.hk2.annotations.Service;
import org.jvnet.hk2.component.Habitat;
import org.jvnet.hk2.component.PerLookup;
import org.xml.sax.SAXParseException;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipException;

import com.sun.jbi.glassfish.container.utils.StringTranslator;
import com.sun.jbi.glassfish.container.utils.LocalStringKeys;
import com.sun.jbi.glassfish.container.utils.FujiUtils;
import com.sun.enterprise.deployment.util.XModuleType;

@Service(name="FujiCompositeApplicationArchivist")
@Scoped(PerLookup.class)
public class FujiCompositeApplicationArchivist  extends Archivist<FujiCompositeApplication> {

    /**
     * habitat
     */
    @Inject
    Habitat habitat;

    /**
     * archive factory
     */
    @Inject
    ArchiveFactory archiveFactory; 

    /**
     * logger
     */
    Logger logger_ = Logger.getLogger(this.getClass().getPackage().getName());

    /**
     * String translator
     */
    StringTranslator translator_ = new StringTranslator("com.sun.jbi.glassfish.container.utils", null);

    /**
     * This method creates a top level Application object for a composite application.
     * @param archive the archive for the application
     */
    public FujiCompositeApplication createApplication(ReadableArchive archive) 
    throws IOException, SAXParseException
    {

        //TODO - starting point for adding support for directory deployment isDirectory

        boolean directory = false;
        FujiCompositeApplication app = new FujiCompositeApplication(habitat);
        String appRoot = archive.getURI().getSchemeSpecificPart(); 
        if (appRoot.endsWith(File.separator)) {
            appRoot = appRoot.substring(0, appRoot.length() - 1);
        }
        List<ReadableArchive> unknowns = new ArrayList<ReadableArchive>();
        ReadableArchive subArchive = null;

        //TODO name of the file - get from jbi.xml
        String appName = appRoot.substring(appRoot.lastIndexOf(File.separatorChar) + 1);
        app.setName(appName);

        //Create a new module that corresponds to the service assembly inside the composite application
        ModuleDescriptor<BundleDescriptor> serviceAssembly = new ModuleDescriptor<BundleDescriptor>();
        serviceAssembly.setArchiveUri(appName+FujiUtils.CONTAINER_CREATED_SA_NAME);
        serviceAssembly.setModuleType(FujiModuleType.FUJI_COMP);
        app.addModule(serviceAssembly);

        WritableArchive createdSA = 
                archiveFactory.createArchive(new File(appRoot,
                FileUtils.makeFriendlyFilename(appName) + FujiUtils.CONTAINER_CREATED_SA_NAME));

        List<ReadableArchive> subs = new ArrayList<ReadableArchive>();
        File[] files = getEligibleEntries(new File(appRoot), directory);

        try {
            InputStream is = archive.getEntry(FujiUtils.JBI_XML);
            OutputStream os = null;
         if (is != null) {
               try {
                   os = createdSA.putNextEntry(FujiUtils.JBI_XML);
               } catch (ZipException ze) {
                   // this is a duplicate...
               }
               ArchivistUtils.copyWithoutClose(is, os);
         }
        } catch (Exception ex)  {
            String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED_NO_JBI_XML);
            logger_.log(Level.WARNING, msg, ex);
        }

        try {
            OutputStream os = createdSA.putNextEntry(JarFile.MANIFEST_NAME);
         if (os != null) {
               try {
                   Manifest out = new Manifest(archive.getManifest());
                   out.write(os);
               } catch (ZipException ze) {
                   // this is a duplicate...
               }
         }
        } catch (Exception ex)  {
            String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED_NO_MANIFEST);
            logger_.log(Level.WARNING, msg, ex);
        }


        for (File subModule : files) {
            if (subModule.getName().endsWith(FujiUtils.CONTAINER_CREATED_SA_NAME)) {
                //do not process the SA that we are cooking up
                continue;
            }

            String name = subModule.getName();
            String uri = deriveArchiveUri(appRoot, subModule, directory);

            try {
                try {
                    subArchive = archiveFactory.openArchive(subModule);
                } catch (IOException ex) {
                    String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED);
                    logger_.log(Level.WARNING, msg, ex);
                }
               
                if (subArchive.exists(FujiUtils.JBI_XML)) {
                    WritableArchive subTarget = createdSA.createSubArchive(uri);
                    copyArchive(subArchive,subTarget,new HashSet());
                } else if(name.endsWith(".war")) {
                    String contextRoot =
                            uri.substring(uri.lastIndexOf('/') + 1, uri.lastIndexOf('.'));
                    ModuleDescriptor<BundleDescriptor> md = new ModuleDescriptor<BundleDescriptor>();
                    md.setArchiveUri(uri);
                    md.setModuleType(XModuleType.WAR);
                    md.setContextRoot(contextRoot);
                    app.addModule(md);
                } else if (name.endsWith(".ear")) {
                    //THIS IS VERY IMPORTANT THIS IS WHERE YOU DO DEPLOYCOMMAND RUNNER
                    ModuleDescriptor<BundleDescriptor> md = new ModuleDescriptor<BundleDescriptor>();
                    md.setArchiveUri(uri);
                    md.setModuleType(XModuleType.EAR);
                    app.addModule(md);
                } else if (name.endsWith(".jar")) {

                    try {
                        //Section EE.8.4.2.1.d.i //TODO - not sure if app client will ever be in a composite application
                        AppClientArchivist acArchivist = new AppClientArchivist();
                        if (acArchivist.hasStandardDeploymentDescriptor(subArchive)
                                || acArchivist.hasRuntimeDeploymentDescriptor(subArchive)
                                || acArchivist.getMainClassName(subArchive.getManifest()) != null) {

                            ModuleDescriptor<BundleDescriptor> md = new ModuleDescriptor<BundleDescriptor>();
                            md.setArchiveUri(uri);
                            md.setModuleType(XModuleType.CAR);
                            md.setManifest(subArchive.getManifest());
                            app.addModule(md);
                            continue;
                        }

                        //Section EE.8.4.2.1.d.ii
                        EjbArchivist ejbArchivist = new EjbArchivist();
                        if (ejbArchivist.hasStandardDeploymentDescriptor(subArchive)
                                || ejbArchivist.hasRuntimeDeploymentDescriptor(subArchive)) {

                            ModuleDescriptor<BundleDescriptor> md = new ModuleDescriptor<BundleDescriptor>();
                            md.setArchiveUri(uri);
                            md.setModuleType(XModuleType.EJB);
                            app.addModule(md);
                            continue;
                        }
                    } catch (IOException ex) {
                        String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED);
                        logger_.log(Level.WARNING, msg, ex);
                    }

                    //Still could not decide between an ejb and a library
                    unknowns.add(subArchive);
                }  else {
                    //ignored
                }   

            } catch (Exception ex) {
                String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED);
                logger_.log(Level.WARNING, msg, ex);
            }
                finally {

                if (subArchive != null) {
                    try {
                        subArchive.close();
                    } catch (IOException ioe) {
                        String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED);
                        logger_.log(Level.WARNING, msg, ioe);
                    }
                }
            }
        }

        try
        {
        createdSA.close();
        }
        catch (Exception ex) {
            String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED);
            logger_.log(Level.WARNING, msg, ex);
        }

        
        if (unknowns.size() > 0) {
            AnnotationDetector detector =
                    new AnnotationDetector(new EjbComponentAnnotationScanner());
            for (int i = 0; i < unknowns.size(); i++) {
                File jarFile = new File(unknowns.get(i).getURI().getSchemeSpecificPart());
                try {
                  if (detector.hasAnnotationInArchive(unknowns.get(i))) {
                        String uri = deriveArchiveUri(appRoot, jarFile, directory);
                        //Section EE.8.4.2.1.d.ii, alas EJB
                        ModuleDescriptor<BundleDescriptor> md = new ModuleDescriptor<BundleDescriptor>();
                        md.setArchiveUri(uri);
                        md.setModuleType(XModuleType.EJB);
                        app.addModule(md);
                   }
                } catch (IOException ex) {
                    logger.log(Level.WARNING, ex.getMessage());
                }
            }
        }

        return app;
    }


    /**
     * This method is used to copy an archive into another archvie.
     * @source the source to be written to
     * @target the target into which the source has to be written
     * @set excludeList list of files to be excluded
     */
     void copyArchive(ReadableArchive source, WritableArchive target, Set excludeList) {

        for (Enumeration e = source.entries(); e.hasMoreElements();) {
            String entryName = String.class.cast(e.nextElement());
            try {
                copy(source, target, entryName);
            } catch (IOException ioe) {
                // duplicate, we ignore
            }
        }

        try {
            OutputStream os = target.putNextEntry(JarFile.MANIFEST_NAME);
            if (os != null) {
               try {
                   Manifest out = new Manifest(source.getManifest());
                   out.write(os);
                   target.closeEntry();
               } catch (ZipException ze) {
                  String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED_NO_MANIFEST);
                  logger_.log(Level.WARNING, msg, ze);
               }
            }
        } catch (Exception ex)  {
            String msg = translator_.getString(LocalStringKeys.CREATE_SA_FAILED_NO_MANIFEST);
            logger_.log(Level.WARNING, msg, ex);
        }

    }


    /**
     * copy the entryName element from the source abstract archive into
     * the target abstract archive
     */
    static void copy(ReadableArchive source, WritableArchive target, String entryName)
    throws IOException {

        InputStream is = null;
        OutputStream os = null;
        try {
            is = source.getEntry(entryName);
            if (is != null) {
                try {
                    os = target.putNextEntry(entryName);
                } catch (ZipException ze) {
                    // this is a duplicate...
                    return;
                }
                ArchivistUtils.copyWithoutClose(is, os);
            } else {
                // This may be a directory specification if there is no entry
                // in the source for it...for example, a directory expression
                // in the Class-Path entry from a JAR's manifest.
                //
                // Try to copy all entries from the source that have the
                // entryName as a prefix.
                for (Enumeration e = source.entries(entryName); e.hasMoreElements();) {
                    copy(source, target, (String) e.nextElement());
                }
            }
        } catch (IOException ioe) {
            throw ioe;
        } finally {
            IOException closeEntryIOException = null;
            if (os != null) {
                try {
                    target.closeEntry();
                } catch (IOException ioe) {
                    closeEntryIOException = ioe;
                }
            }
            if (is != null) {
                is.close();
            }

            if (closeEntryIOException != null) {
                throw closeEntryIOException;
            }
        }
    }

    /** TODO - dummy method?
     * @return
     */
    protected String getArchiveExtension() {
        return "zip";
    }


    /**
     * This method retrieves a list of eligible entries from an archive
     * @param appRoot application
     * @param deployDir true if directory deploy
     */
    private static File[] getEligibleEntries(File appRoot, boolean deploydir) {

        //For deploydir, all modules are exploded at the top of application root
        if (deploydir) {
            return appRoot.listFiles(new DirectoryIntrospectionFilter());
        }

        //For archive deploy, recursively search the entire package
        Vector<File> files = new Vector<File>();
        getListOfFiles(appRoot, files,
                new ArchiveIntrospectionFilter(appRoot.getAbsolutePath()));
        return (File[]) files.toArray(new File[files.size()]);
    }

    /**
     * This method retrieves a list of eligible entries from an archive
     * @param directory
     * @param files
     * @param fileter
     */
    private static void getListOfFiles(
            File directory, Vector<File> files, FilenameFilter filter) {

        File[] list = directory.listFiles(filter);
        for (int i = 0; i < list.length; i++) {
            if (!list[i].isDirectory()) {
                files.add(list[i]);
            } else {
                getListOfFiles(list[i], files, filter);
            }
        }
    }


    /**
     * This method derives archive uri for a module
     * @param appRoot
     * @param subModule
     * @param deploydir
     */
    private static String deriveArchiveUri(
            String appRoot, File subModule, boolean deploydir) {

        //if deploydir, revert the name of the directory to
        //the format of foo/bar/voodoo.ext (where ext is war/rar/jar)
        if (deploydir) {
            return FileUtils.revertFriendlyFilename(subModule.getName());
        }

        // convert appRoot to canonical path so it would work on windows platform
        String aRoot = null;
        try {
            aRoot = (new File(appRoot)).getCanonicalPath();
        } catch (IOException ex) {
            aRoot = appRoot;
        } 

        //if archive deploy, need to make sure all of the directory
        //structure is correctly included
        String uri = subModule.getAbsolutePath().substring(aRoot.length() + 1);
        return uri.replace(File.separatorChar, '/');
    }   
    
    /**
     * utility class
     */
     private static class ArchiveIntrospectionFilter implements FilenameFilter {
        private String libDir;

        ArchiveIntrospectionFilter(String root) {
            libDir = root + File.separator + "lib" + File.separator;
        }

        public boolean accept(File dir, String name) {

            File currentFile = new File(dir, name);
            if (currentFile.isDirectory()) {
                return true;
            }

            //For ".war" and ".rar", check all files in the archive
            if (name.endsWith(".war") || name.endsWith(".rar")) {
                return true;
            }

            String path = currentFile.getAbsolutePath();
            if (!path.startsWith(libDir) && path.endsWith(".jar")) {
                return true;
            }

            return false;
        }
    }

    /**
     * utility class
     */
     private static class DirectoryIntrospectionFilter implements FilenameFilter {

        DirectoryIntrospectionFilter() {
        }

        public boolean accept(File dir, String name) {

            File currentFile = new File(dir, name);
            if (!currentFile.isDirectory()) {
                return false;
            }

            // now we are supporting directory names with
            // ".suffix" and "_suffix"
            if (resemblesTopLevelSubmodule(name)) {
                return true;
            }

            return false;
        }
    } 

    /**
     * no deployment descriptor
     */
    public DeploymentDescriptorFile getConfigurationDDFile() {
        //return new ApplicationRuntimeDDFile();
        return null;
    }

    /**
     * Returns whether the entry name appears to be that of a submodule at
     * the top level of an enclosing application.
     * <p>
     * Judge an entry to be a top-level submodule if it ends with _war, _jar,
     * _rar, or .war, .jar, or .rar (MyEclipse uses latter pattern.)
     *
     * @param entryName
     * @return
     */
    private static boolean resemblesTopLevelSubmodule(final String entryName) {
        return (entryName.endsWith("_war")
                || entryName.endsWith("_jar")
                || entryName.endsWith("_rar")
                || entryName.endsWith(".war")
                || entryName.endsWith(".jar")
                || entryName.endsWith(".rar"));
    }

    /**
     * This method is used to return te module type
     */
    public com.sun.enterprise.deployment.util.XModuleType getModuleType() {
        return FujiModuleType.FUJI_COMP;
    }


    /**
     * post handles
     */
    public boolean postHandles(org.glassfish.api.deployment.archive.ReadableArchive source ) {
        return true;

    }

    /**
     * Create a new application descriptor
     */
    public FujiCompositeApplication getDefaultBundleDescriptor() {
        return new FujiCompositeApplication(habitat);
    }

        
    /**
     * no deployment descriptor
     * @return
     */
    public DeploymentDescriptorFile getStandardDDFile() {
        return null;
    }

    /**
     * This method overrides the one in Archivist. 
     * Fix for issue 2234
     */
    public boolean hasStandardDeploymentDescriptor(ReadableArchive archive)
            throws IOException {
        return false;
    }

    /**
     * This method overrides the one in Archivist.
     * Fix for issue 2234
     */
    public boolean hasRuntimeDeploymentDescriptor(ReadableArchive archive)
            throws IOException {        
        return false;
    }


 
}