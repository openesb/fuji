package com.sun.jbi.glassfish.container.composite;

import com.sun.enterprise.deployment.util.XModuleType;

public class FujiModuleType extends XModuleType {



    final Integer index = 0;

    protected FujiModuleType(int value) {
        super(value);
    }

    public static XModuleType create(int index) {
        return XModuleType.create(index);
    }


    //this is temporary back - dont expect the numbers to be available
    public static final XModuleType FUJI_COMP = create(100);

    public static final XModuleType FUJI_SU = create(101);

}
