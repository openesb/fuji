/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCompositeSniffer.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.composite;

import java.io.IOException;

import org.jvnet.hk2.annotations.Service;
import org.glassfish.api.deployment.archive.ReadableArchive;
import org.glassfish.internal.deployment.GenericCompositeSniffer;

import com.sun.jbi.glassfish.container.utils.FujiUtils;

/**
 *  FujiCompositeSniffer is used to look out for Service Assembly deployments
 */
@Service(name="FujiCompositeSniffer")
public class FujiCompositeSniffer extends GenericCompositeSniffer {


    public FujiCompositeSniffer() {
        super("FujiComposite", FujiUtils.JBI_XML, null);
    }

    public String[] getContainersNames() {
        return new String[] { "FujiCompositeContainer" };
    }                                                                           
    /**
     * Returns true if the passed file or directory is recognized by this
     * instance.
     *
     * @param location the file or directory to explore
     * @param loader class loader for this application
     * @return true if this sniffer handles this application type
     */
    public boolean handles(ReadableArchive source, ClassLoader loader) {
        try {
            if (source.exists(FujiUtils.JBI_XML)) {
                return FujiUtils.isApplication(source) || FujiUtils.isComponent(source) || FujiUtils.isSharedLibrary(source);
            }
        } catch (IOException e) {
            // ignore
        }
        return false;   
    }

}

