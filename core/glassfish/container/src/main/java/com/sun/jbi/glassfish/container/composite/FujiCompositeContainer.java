/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCompositeContainer.java
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.composite;
        
import org.jvnet.hk2.annotations.Service;
import org.glassfish.api.container.Container;
import org.glassfish.api.deployment.Deployer;

/**
 * FujiCompositeContainer
 */
@Service(name="FujiCompositeContainer")
public class FujiCompositeContainer implements Container {

    /**
     * This method is used to get to the FujiCompositeDeployer
     * @return
     */
    public Class<? extends Deployer> getDeployer() {
        return FujiCompositeDeployer.class;
    }

    /**
     * This method provides the name of the composite container
     * @return
     */
    public String getName() {
        return "com.sun.jbi.glassfish.container.composite.FujiCompositeContainer"; 
    }                                                                     
}
