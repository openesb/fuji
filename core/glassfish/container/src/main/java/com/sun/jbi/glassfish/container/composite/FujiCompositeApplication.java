/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCompositeApplication.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.composite;

import java.util.Set;
import java.util.HashSet;

import org.jvnet.hk2.component.Habitat;
import com.sun.enterprise.deployment.BundleDescriptor;
import com.sun.enterprise.deployment.RootDeploymentDescriptor;
import com.sun.enterprise.deployment.util.ModuleDescriptor;
import com.sun.enterprise.deployment.util.XModuleType;

/**
 * This class describes a FujiCompositeApplication
 */
public class FujiCompositeApplication extends RootDeploymentDescriptor {

    /**
     * habitat
     */
    private final Habitat habitat;

    /**
     * application name
     */
    private String name;
         
    /**
     * Set of modules in this application
     */
    private Set<ModuleDescriptor<BundleDescriptor>> modules = 
            new HashSet<ModuleDescriptor<BundleDescriptor>>();
            
    /**
     * Constructor
     * @param habitat
     */
    public FujiCompositeApplication(Habitat habitat) {
        this.habitat = habitat;
    }

    /**
     * Returns the name of the application
     * @return the application name
     */
    public String getName() {
        return name;
    }
    
    /**
     * sets the name of the application
     * @param name the application name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * adds a module to the module list
     * @param md - the module descriptor
     */
    public void addModule(ModuleDescriptor md){
        modules.add(md);
        
    }

    /**
     * returns a list of modules in the application
     * @return a list of modules
     */
    public Set<ModuleDescriptor<BundleDescriptor>> getModules() {
        return modules;
    }

    
    /**
     * This method returns false as this is not a Java EE application
     * @return
     */
    public boolean isApplication() {
        return false;
    }


    /**
     * @return the class loader associated with this application
     */
    public ClassLoader getClassLoader() {
        //TODO ? fix this if there are special class loading for libraries
        return null;
    }
     
    /**
     * @return the module type for this descriptor
     */
    public XModuleType getModuleType() {
        return FujiModuleType.FUJI_COMP;
    }
        
    /**
     * returns true if this application has no modules
     */
    public boolean isEmpty() {
        return modules.isEmpty();
    }

    /**
     * No spec version
     * @return
     */
    public String getDefaultSpecVersion() {
        return null;
    }

    /**
     * Module ID for this application - corresponds to the HK2 module
     * @return
     */
    public String getModuleID() {
        return moduleID;
    }
}
