/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCompositeDeployer.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.composite;

import org.glassfish.api.container.Container;
import org.glassfish.api.deployment.archive.ArchiveHandler;
import org.glassfish.api.container.Sniffer;

import org.glassfish.api.deployment.*;
import org.glassfish.api.deployment.archive.ReadableArchive;


import org.glassfish.api.ActionReport;
import org.glassfish.api.event.Events;
import org.glassfish.api.admin.ServerEnvironment;
import org.glassfish.deployment.common.DownloadableArtifacts;
import org.glassfish.internal.deployment.Deployment;
import org.glassfish.internal.deployment.ExtendedDeploymentContext;
import org.glassfish.internal.data.*;
import org.glassfish.deployment.common.DeploymentContextImpl;
import org.jvnet.hk2.annotations.Service;
import org.jvnet.hk2.annotations.Inject;
import org.jvnet.hk2.component.Habitat;
import com.sun.enterprise.deployment.util.ModuleDescriptor;
import com.sun.enterprise.deployment.util.XModuleType;
import com.sun.enterprise.deploy.shared.ArchiveFactory;
import com.sun.enterprise.deployment.deploy.shared.OutputJarArchive;
import com.sun.enterprise.deployment.deploy.shared.Util;
import com.sun.enterprise.config.serverbeans.Domain;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.IOException;
import java.io.File;
import org.glassfish.deployment.common.DeploymentException;
import org.glassfish.deployment.common.DummyApplication;
import org.glassfish.javaee.full.deployment.EarClassLoader;
import com.sun.enterprise.util.io.FileUtils;

import com.sun.jbi.glassfish.container.utils.FujiUtils;
import com.sun.jbi.glassfish.container.utils.LocalStringKeys;
import com.sun.jbi.glassfish.container.utils.StringTranslator;

import com.sun.jbi.glassfish.container.FujiSniffer;
import com.sun.jbi.glassfish.container.FujiArchiveHandler;

/**
 *  FujiCompositeDeployer
 */
@Service(name="FujiCompositeDeployer")
public class FujiCompositeDeployer implements Deployer {

    /**
     * habitat
     */
    @Inject
    Habitat habitat;

    /**
     * deployment
     */
    @Inject
    Deployment deployment;

    /**
     * ServerEnvironment
     */
    @Inject
    ServerEnvironment env;

    /**
     * ApplicationRegistry
     */
    @Inject
    ApplicationRegistry appRegistry;

    /**
     * ArchiveFactory
     */
    @Inject
    ArchiveFactory archiveFactory;

    /**
     * events
     */
    @Inject
    Events events;

    /**
     * DownloadableArtifacts
     */
    @Inject
    private DownloadableArtifacts artifacts;

    /**
     * domain
     */
    @Inject
    Domain domain;


    /**
     * logger
     */
    Logger logger_ = Logger.getLogger(this.getClass().getPackage().getName());


    /**
     * String translator
     */
    StringTranslator translator_ = new StringTranslator("com.sun.jbi.glassfish.container.utils", null);

    /**
     * get metadata
     */
    public MetaData getMetaData() {
        return new MetaData(false, null, new Class[] { FujiCompositeApplication.class});
    }

    
    /**
     * load metadata
     */
    public Object loadMetaData(Class type, DeploymentContext context) {
        return null;
    }

    /**
     * prepare for deployment
     * @param context
     * @return
     */
    public boolean prepare(final DeploymentContext context) {

        if (FujiUtils.isApplication(context.getSource())) {
            return prepareSA(context);
        } else {
            return prepareComponent(context);
        }

    }


    /**
     * prepare for deployment
     * @param context
     * @return
     */
    public boolean prepareComponent(final DeploymentContext context) {
        List<EngineInfo> orderedContainers = null;
        ArchiveHandler componentHandler = null;
        List<Sniffer> componentSniffer = new ArrayList<Sniffer>();
        try {
            // let's get the list of containers interested in this module
            for (ArchiveHandler handler : habitat.getAllByContract(ArchiveHandler.class)) {
                if (handler instanceof FujiArchiveHandler) {
                   componentHandler = handler;
                   break;
                }
            }

            for (Sniffer sniffer : habitat.getAllByContract(Sniffer.class)) {
                if (sniffer instanceof FujiSniffer) {
                    componentSniffer.add(sniffer);
                    break;
                }
            }
            orderedContainers = deployment.setupContainerInfos(componentHandler, componentSniffer, context);
            ProgressTracker tracker = new ProgressTracker() {
                public void actOn(Logger logger) {
                    //TODO fix this
                    /* for (EngineRef module : get("prepared", EngineRef.class)) {
                        module.clean(bundleContext);
                      } */
                }

            };

            deployment.prepareModule(orderedContainers, context.getSource().getURI().toString(),context, tracker);
            return true;

        } catch(Exception ex) {
            String msg = translator_.getString(LocalStringKeys.PREPARING_SA_DEPLOYMENT_FAILED);
            logger_.log(Level.WARNING, msg, ex);
            return false;
        }


    }


    /**
     * prepare for deployment
     * @param context
     * @return
     */
    public boolean prepareSA(final DeploymentContext context) {

         
        final FujiCompositeApplication application =
                context.getModuleMetaData(FujiCompositeApplication.class);

        DeployCommandParameters deployParams =
                context.getCommandParameters(DeployCommandParameters.class);

        final String appName = deployParams.name();
        

        try {
            final ApplicationInfo appInfo =
                    new CompositeApplicationInfo(events, application, context.getSource(), appName);

            for (Object m : context.getModuleMetadata()) {
                appInfo.addMetaData(m);
            }

            for (ModuleDescriptor  module : application.getModules()) {
                if (module.getModuleType().equals(XModuleType.EAR)) {
                    //TODO - use deploymentfacility to deploy the EAR
                    logger_.finer("Calling runnable.doCommandRunner()");
                } else if (module.getModuleType().equals(FujiModuleType.FUJI_COMP)) {

                    ReadableArchive subArchive;
                    try {
                        String name = application.getName();
                        String saRoot =
                                domain.getApplicationRoot() + File.separator +
                                application.getName() + File.separator +
                                FileUtils.makeFriendlyFilename(application.getName()) + FujiUtils.CONTAINER_CREATED_SA_NAME;
                        subArchive = archiveFactory.openArchive(new File(saRoot));
                        subArchive.setParentArchive(context.getSource());
                        ModuleInfo mInfo =
                                prepareBundle(application, module, context, subArchive,
                                            module.getArchiveUri());
                        appInfo.addModule(mInfo);
                        
                    } catch(Exception ioe) {
                        String msg = translator_.getString(LocalStringKeys.PREPARING_SA_DEPLOYMENT_FAILED);
                        logger_.log(Level.WARNING, msg, ioe);
                    }

                } else {
                    try {
                        //TODO - not yet tested
                        ModuleInfo mInfo =
                                prepareBundle(application, module, context, context.getSource().getSubArchive(module.getArchiveUri()),module.getArchiveUri() );
                        appInfo.addModule(mInfo);

                    } catch(IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }
            context.addModuleMetaData(appInfo);
            
        
        } catch(Exception ex) {
            String msg = translator_.getString(LocalStringKeys.PREPARING_SA_DEPLOYMENT_FAILED);
            logger_.log(Level.WARNING, msg, ex);
        } 
        return true;

    }

    
    /**
     * prepare bundle
     * @param application
     * @param md
     * @param bundleContext
     * @param archive
     * @param moduleURI
     * @return
     * @throws java.lang.Exception
     */
    private ModuleInfo prepareBundle(
            FujiCompositeApplication application,
            final ModuleDescriptor md,
            final DeploymentContext bundleContext,
            ReadableArchive archive, String moduleURI)
    throws Exception
    {

        List<EngineInfo> orderedContainers = null;
        ExtendedDeploymentContext subContext =   subContext(application, bundleContext,archive, moduleURI);
        try {
            // let's get the list of containers interested in this module
            orderedContainers = deployment.setupContainerInfos(subContext);
        } catch(Exception ex) {
            String msg = translator_.getString(LocalStringKeys.PREPARING_SA_DEPLOYMENT_FAILED);
            logger_.log(Level.WARNING, msg, ex);
        }
        

        ProgressTracker tracker = new ProgressTracker() {
            public void actOn(Logger logger) {
                //TODO fix this
                /* for (EngineRef module : get("prepared", EngineRef.class)) {
                    module.clean(bundleContext);
                  } */
            }

        };

        return deployment.prepareModule(orderedContainers, md.getArchiveUri(),subContext, tracker);
    }

    public ApplicationContainer load(Container container, DeploymentContext context) {
        return new DummyApplication();
    }

    public void unload(ApplicationContainer appContainer, DeploymentContext context) {
        // nothing to do
    }

    public void clean(DeploymentContext context) {
        // nothing to do
    }


    /**
     * Get an instance of ExtendedDeploymentContext that has the SubArchive specific
     * information in the deployment context
     */
    private ExtendedDeploymentContext subContext(
            final FujiCompositeApplication application, 
            final DeploymentContext context,
            final ReadableArchive subArchive,
            final String moduleUri)
    {

        ExtendedDeploymentContext moduleContext = ((ExtendedDeploymentContext)context).getModuleDeploymentContexts().get(moduleUri);
        if (moduleContext != null) {
            return moduleContext;
        }


        final Properties moduleProps =  getModuleProps(context, application.getName()+ FujiUtils.CONTAINER_CREATED_SA_NAME);
        
        ActionReport subReport = context.getActionReport().addSubActionsReport();

        moduleContext = new DeploymentContextImpl(subReport,
                                         logger_, context.getSource(),
                                         context.getCommandParameters(OpsParams.class), env) {

                    @Override
                    public ClassLoader getClassLoader() {
                        return context.getClassLoader();
                    }

                    @Override
                    public ClassLoader getFinalClassLoader() {
                        return context.getClassLoader();
                    }
                    @Override
                    public ReadableArchive getSource() {
                        return subArchive;
                    }

                    @Override
                    public Properties getAppProps() {
                        return context.getAppProps();
                    }

                    @Override
                    public <U extends OpsParams> U getCommandParameters(Class<U> commandParametersType) {
                        return context.getCommandParameters(commandParametersType);
                    }

                    @Override
                    public void addTransientAppMetaData(String metaDataKey,
                        Object metaData) {
                        context.addTransientAppMetaData(metaDataKey,
                            metaData);
                    }

                    @Override
                    public  <T> T getTransientAppMetaData(String metaDataKey,
                        Class<T> metadataType) {
                        return context.getTransientAppMetaData(metaDataKey,
                            metadataType);
                    }

                    @Override
                    public Properties getModuleProps() {
                        return moduleProps;
                    }

                    @Override
                    public ReadableArchive getOriginalSource() {
                        return subArchive;
                    }

                    @Override
                    public File getScratchDir(String subDirName) {
                        String modulePortion = Util.getURIName(
                            getSource().getURI());
                        return (new File(super.getScratchDir(subDirName),
                            modulePortion));
                    }

                    @Override
                    public <T> T getModuleMetaData(Class<T> metadataType) {
                        //TODO return proper metadata
                        return null;
                    }
                };

                ((ExtendedDeploymentContext)context).getModuleDeploymentContexts().put(moduleUri, moduleContext);
                moduleContext.getAppProps().put("module-name", FileUtils.makeFriendlyFilename(application.getName())+"-container-created-sa");
                return moduleContext;
    }


    /**
     * get module properties from context
     * @param context
     * @param moduleUri
     * @return
     */
    private Properties getModuleProps(DeploymentContext context,
        String moduleUri) {
        Map<String, Properties> modulePropsMap = context.getModulePropsMap();
        Properties moduleProps = modulePropsMap.get(moduleUri);
        if (moduleProps == null) {
            moduleProps = new Properties();
            modulePropsMap.put(moduleUri, moduleProps);
        }
        return moduleProps;
    }


    /**
     * CompositeApplicationInfo class
     */

    private class CompositeApplicationInfo extends ApplicationInfo {

        final FujiCompositeApplication application;
             ReadableArchive source = null;

        private CompositeApplicationInfo(
                Events events,
                FujiCompositeApplication application,
                ReadableArchive source,
                String name)
        {
            super(events, source, name);
            this.application = application;
            this.source=source;
        }

        @Override
        protected ExtendedDeploymentContext getSubContext(ModuleInfo module, ExtendedDeploymentContext context) {
            
            if (module.getName().endsWith(FujiUtils.CONTAINER_CREATED_SA_NAME))    {
                try
                {
                     String saRoot = domain.getApplicationRoot() + File.separator +
                             application.getName() +
                             File.separator + application.getName() + FujiUtils.CONTAINER_CREATED_SA_NAME;
                     ReadableArchive subArchive = archiveFactory.openArchive(new File(saRoot));
                     subArchive.setParentArchive(context.getSource());
                     return subContext(application, context, subArchive, module.getName());
                } catch (IOException ex) {
                    String msg = translator_.getString(LocalStringKeys.FAILED_CREATE_SERVICE_ASSEMBLY_SUB_CONTEXT);
                    logger_.log(Level.WARNING, msg, ex);
                    return null;
                }
            } else{

                try
                {
                    return subContext(application, context, source.getSubArchive(module.getName()), module.getName());
                }catch (IOException ex) {
                    String msg = translator_.getString(LocalStringKeys.FAILED_TO_CREATE_MODULE_SUB_CONTEXT);
                    logger_.log(Level.WARNING, msg, ex);
                    return null;

                }
            }
        }

     }

}
