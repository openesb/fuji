/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiSniffer.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.glassfish.container;
        
import org.glassfish.extras.osgicontainer.OSGiSniffer;
import org.glassfish.api.deployment.archive.ReadableArchive;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Logger;
import org.jvnet.hk2.annotations.Service;
import org.jvnet.hk2.annotations.Inject;
import com.sun.enterprise.module.Module;
import com.sun.enterprise.module.ModulesRegistry;

import com.sun.jbi.glassfish.container.utils.FujiUtils;



/**
 * A simple sniffer for Fuji that maps archives with META-INF/jbi.xml
 * to the FujiContainer
 * @author annies
 */
@Service(name="FujiSniffer")
public class FujiSniffer extends OSGiSniffer {
    
    @Inject     
    ModulesRegistry registry;
    
    /**
     * Returns true if the passed file or directory is recognized by this
     * sniffer.
     * @param source the file or directory abstracted as an archive
     * @param loader if the class loader capable of loading classes and
     * resources from the source archive.
     * @return true if the location is recognized by this sniffer
     */
    @Override
    public boolean handles(ReadableArchive source, ClassLoader loader) {
        
        try {
            if (source.exists(FujiUtils.JBI_XML)) {
                return FujiUtils.isApplication(source) || FujiUtils.isComponent(source) || FujiUtils.isSharedLibrary(source);
            }
        } catch (IOException e) {
            // ignore
        }
        return false;

    }

    /**
     * Returns the list of Containers that this Sniffer enables.
     *
     * The runtime will look up each container implementing
     * using the names provided in the habitat.
     *
     * @return list of container names known to the habitat for this sniffer
     */
    public String[] getContainersNames() {
        return new String[] { "FujiContainer" };
    }
    
   /**
     * Sets up the container libraries so that any imported bundle from the
     * connector jar file will now be known to the module subsystem
     *
     * This method returns a {@link Module}s for the module containing
     * the core implementation of the container. That means that this module
     * will be locked as long as there is at least one module loaded in the
     * associated container.
     *
     * @param containerHome is where the container implementation resides
     * @param logger the logger to use
     * @return the module definition of the core container implementation.
     *
     * @throws java.io.IOException exception if something goes sour
     */
    public Module[] setup(String containerHome, Logger logger) throws IOException {


       Collection<Module> modules = registry.getModules();
       boolean jbiFrameworkDeteted = false;
       for ( Module module: modules) {
           if (module.getName().equals("JBI OSGi framework")) {
               jbiFrameworkDeteted = true;
               break;
           }
       }
       
       //TODO find out how to percolate this message to the admin interface.
       /*if (!jbiFrameworkDeteted) {           
           logger.warning("JBI Framework is not available");
       }*/
       return null;
        
    }

}