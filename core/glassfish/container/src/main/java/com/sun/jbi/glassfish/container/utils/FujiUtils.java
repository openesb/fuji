/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiUtils.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.utils;

import com.sun.jbi.framework.descriptor.Jbi;
import org.glassfish.api.deployment.archive.ReadableArchive;


public class FujiUtils {

    /**
     * the name of the SA archive created by the container
     */
    public static final String CONTAINER_CREATED_SA_NAME = "-container-created-sa.jar";

    /**
     * jbi descriptor
     */
     public static final String JBI_XML = "META-INF/jbi.xml";

     /**
      * returns true if this archive is a Fuji application
      */
    public static boolean isApplication(ReadableArchive archive) {
        

        try {
            Jbi jbiXml = Jbi.newJbi(archive.getEntry("META-INF/jbi.xml"));
            return (jbiXml.getServiceAssembly() != null);    

        } catch (Exception ex) {
            //log
            return false;
        }
        
    
    }

    /**
     * returns true if his archive is a Fuji component
     * @param archive
     * @return
     */
    public static boolean isComponent(ReadableArchive archive) {


        try {
            Jbi jbiXml = Jbi.newJbi(archive.getEntry("META-INF/jbi.xml"));
            return (jbiXml.getComponent() != null);

        } catch (Exception ex) {
            //log
            return false;
        }


    }


    /**
     * returns true if his archive is a Fuji shared library
     * @param archive
     * @return
     */
    public static boolean isSharedLibrary(ReadableArchive archive) {


        try {
            Jbi jbiXml = Jbi.newJbi(archive.getEntry("META-INF/jbi.xml"));
            return (jbiXml.getSharedLibrary() != null);

        } catch (Exception ex) {
            //log
            return false;
        }


    }
}