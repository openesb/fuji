/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.glassfish.container.utils;

/** i18n Message Keys.
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{

    String CREATE_SA_FAILED_NO_JBI_XML  = "CREATE_SA_FAILED_NO_JBI_XML";
    String CREATE_SA_FAILED_NO_MANIFEST = "CREATE_SA_FAILED_NO_MANIFEST";
    String CREATE_SA_FAILED = "CREATE_SA_FAILED";
    String PREPARING_SA_DEPLOYMENT_FAILED ="PREPARING_SA_DEPLOYMENT_FAILED";
    String FAILED_CREATE_SERVICE_ASSEMBLY_SUB_CONTEXT = "FAILED_CREATE_SERVICE_ASSEMBLY_SUB_CONTEXT";
    String FAILED_TO_CREATE_MODULE_SUB_CONTEXT = "FAILED_TO_CREATE_MODULE_SUB_CONTEXT";
    String COMPOSITE_APP_EXPAND_FAILED = "COMPOSITE_APP_EXPAND_FAILED";
    String COMPOSITE_ARCHIVE_HANDLER_CLASS_LOADER_FAILED = "COMPOSITE_ARCHIVE_HANDLER_CLASS_LOADER_FAILED";
    String FAILED_TO_CREATE_APPLICATION_HOLDER = "FAILED_TO_CREATE_APPLICATION_HOLDER";

}