/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiArchiveHandler.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.glassfish.container;
        
import org.glassfish.extras.osgicontainer.OSGiArchiveHandler;
import org.glassfish.api.deployment.archive.ReadableArchive;
import org.jvnet.hk2.annotations.Service;
 
import java.io.IOException;
import com.sun.jbi.glassfish.container.utils.FujiUtils;


/**
 * This is a simple archive handler for Fuji. This handles simple
 * archives - single level archives like JBI Components.
 */
@Service(name="FujiArchiveHandler")

public class FujiArchiveHandler extends OSGiArchiveHandler {

   /**
    * returns the type associated with this archive
    */
    public String getArchiveType() {
        return "Fuji";
    }
        
    /**
     * handles archives that contain META-INF/jbi.xml
     * @param archive the source archive
     * @return true if it is handled by Fuji container false otherwise
     */
    public boolean handles(ReadableArchive archive) throws IOException {
        
        try {
            if (archive.exists("META-INF/jbi.xml")) {
                return FujiUtils.isApplication(archive) || FujiUtils.isComponent(archive) || FujiUtils.isSharedLibrary(archive);
            }
        } catch (IOException e) {
            // ignore
        }
        return false;
    }        
}
