/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiContainer.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.glassfish.container;
        
import org.glassfish.extras.osgicontainer.OSGiContainer;
import org.jvnet.hk2.annotations.Service;

/**
 * This is a simple container for Fuji. The actual container 
 * implementation is in a platform agnostic implementation. The
 * job of this container is to verify if the jbi-framework is
 * installed and available and if not direct the users to get
 * it from update center or set it up dynamically.(?) 
 */
@Service(name="FujiContainer")
public class FujiContainer extends OSGiContainer { 
    
    /**
     * Returns a human redeable name for this container, this name is not used for
     * identifying the container but can be used to display messages belonging to
     * the container.
     *
     * @return a human readable name for this container.
     */
    public String getName() { 
        return "com.sun.jbi.glassfish.container.FujiContainer"; 
    } 
    
    /**
     * Returns the Deployer implementation capable of deploying applications to this
     * container.
     *
     * @return the Deployer implementation
     */
   
    public Class getDeployer() { 
        return FujiDeployer.class; 
    } 

    
} 
