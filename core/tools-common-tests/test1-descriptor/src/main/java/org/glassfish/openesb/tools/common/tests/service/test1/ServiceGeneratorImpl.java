/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.tools.common.tests.service.test1;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;
import org.glassfish.openesb.tools.common.templates.TemplateException;

/**
 *
 * @author chikkala
 */
public class ServiceGeneratorImpl implements ServiceGenerator {

    public void generateServiceUnit(ServiceProjectModel prjModel, IFLModel ifl) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) throws IOException {
        File configDir = prjModel.getConfigDir(serviceName);
        configDir.mkdirs();
        File serviceConfigFile = new File(configDir, "service.properties");
        URL serviceConfigURL = this.getClass().getResource("templates/service.properties");
        try {
            TemplateEngine.getDefault().processTemplate(serviceConfigURL, serviceConfigFile, serviceConfig);
        } catch (TemplateException ex) {
            throw new IOException(ex.getMessage());
        }

    }

    public void deleteUnusedArtifacts(ServiceProjectModel prjModel, IFLModel ifl) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void deleteServiceArtifacts(ServiceProjectModel prjModel, String serviceName, Properties serviceConfig) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, Map<String, Properties> providersServiceConfigMap, Map<String, Properties> consumersServiceConfigMap) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
