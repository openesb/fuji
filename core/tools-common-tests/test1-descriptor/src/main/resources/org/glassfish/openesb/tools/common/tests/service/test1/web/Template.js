{
    id: "test1-template",

    display_name: "Test1 Adapter",
    description: "Basic template for Test1 adapters.",
    icon: null,
    tags: ["template", "adapter", "test1"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10200,

    section: "templates",

    item_data: {
        node_type: "test1",
        node_data: {
            properties: {
                code_name: "test1-",
                is_shared: false,

                test1_param1: null,
                test1_param2: null,
                test1_param3: null,

                noop: null
            },

            ui_properties: {
                display_name: "Test1 ",
                description: "Test1 description",
                icon: null,

                noop: null
            }
        }
    }
}