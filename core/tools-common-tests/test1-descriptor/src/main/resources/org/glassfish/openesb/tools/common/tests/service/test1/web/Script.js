/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var TEST1_ADAPTER = "test1"; // NOI18N

//----------------------------------------------------------------------------------------
var Test1AdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    TEST1_ADAPTER, this);
        }

        Ws.Utils.apply_defaults(this.properties, {
            test1_param1: "value1",
            test1_param2: "value2",
            test1_param3: "value3"
        });
    },

    pc_get_descriptors: function($super) {
        var container = $super();

        container.sections[0].properties.push({
            name: "properties_test1_param1",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Param1",
            description: "Should get it from the descriptor",
            value: this.properties.test1_param1
        });

        container.sections[0].properties.push({
            name: "properties_test1_param2",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Param2",
            description: "Should get it from the descriptor",
            value: this.properties.test1_param2
        });

        container.sections[0].properties.push({
            name: "properties_test1_param3",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Param3",
            description: "Should get it from the descriptor",
            value: this.properties.test1_param3
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var Test1AdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(TEST1_ADAPTER, Test1AdapterNode);
Ws.Graph.NodeUiFactory.register_type(TEST1_ADAPTER, Test1AdapterNodeUi);
