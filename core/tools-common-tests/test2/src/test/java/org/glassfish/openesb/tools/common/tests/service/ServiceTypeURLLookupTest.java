/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.tests.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 *
 * @author chikkala
 */
public class ServiceTypeURLLookupTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ServiceTypeURLLookupTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ServiceTypeURLLookupTest.class);
    }
    /**
     * Rigourous Test :-)
     */
    public void testServiceType() throws Exception {
        String groupIdPath = "open-esb/fuji/components/descriptors";
        String test1ArtifactId = "test1-descriptor";
        String test2ArtifactId = "test2-descriptor";
        String version = System.getProperty("junit.fuji.version");
        String localRepoPath = System.getProperty("localRepository");
        
        File localRepo = new File(localRepoPath);
        File groupPath = new File (localRepo, groupIdPath);
        File test1Jar = new File(groupPath, test1ArtifactId + "/" + version + "/" + test1ArtifactId + "-" + version  + ".jar");
        File test2Jar = new File(groupPath, test2ArtifactId + "/" + version + "/" + test2ArtifactId + "-" + version  + ".jar");
        System.out.println("Test1 jar  " + test1Jar.getAbsolutePath());
         System.out.println("Test2 jar  " + test2Jar.getAbsolutePath());
        if (!test1Jar.exists()) {
            System.out.println("Test1 jar not found " + test1Jar.getAbsolutePath());
        }

        if (!test2Jar.exists()) {
            System.out.println("Test2 jar not found " + test2Jar.getAbsolutePath());
        }

        List<URL> jarURLs = new ArrayList<URL>();
        jarURLs.add(test1Jar.getAbsoluteFile().toURI().toURL());
        jarURLs.add(test2Jar.getAbsoluteFile().toURI().toURL());

        String test1ServiceType = "test1";
        String test2ServiceType = "test2";

        Test2Utils utils = new Test2Utils();
        ServiceDescriptor test1SD = utils.getServiceDescriptor(new URL[] {test1Jar.getAbsoluteFile().toURI().toURL()}, test1ServiceType);

        ServiceDescriptor test2SD = utils.getServiceDescriptor(jarURLs.toArray(new URL[jarURLs.size()]), test2ServiceType);

        test2SD = utils.getServiceDescriptor(jarURLs.toArray(new URL[jarURLs.size()]), test2ServiceType);

        assertEquals(test1ServiceType, test1SD.getServiceType());
        assertEquals(test2ServiceType, test2SD.getServiceType());
        loadSPIProviders(test1SD);
    }

    private void loadSPIProviders(ServiceDescriptor sd) throws Exception {
        System.out.println("Loading service SPI implemenations for " + sd.getServiceType());
        // service spi loading test
        ServiceGenerator sg = sd.getServiceGenerator();
        assertNotNull("ServiceGenerator implementation is expected", sg);
        ServiceBuilder sb = sd.getServiceBuilder();
        assertNotNull("Service Builder implemenation is expected", sb);
        ServicePackager sp = sd.getServicePackager();
        assertNotNull("Service Packager implementation is expected", sp);
//        System.getProperties().list(System.out);
        System.out.println("test.target.dir= " + System.getProperty("test.project.dir"));
        System.out.println("basedir= " + System.getProperty("basedir"));

         File testTargetDir = new File(System.getProperty("test.target.dir"));
         File testAppDir = new File(testTargetDir, "testapp");
//        File targetDir = new File(System.getProperty("basedir"), "target");
//        File testAppDir = new File(targetDir, "testapp");

        String testAppDirPath = testAppDir.getAbsolutePath();
        System.out.println("Test App Dir " + testAppDirPath);
        if ( testAppDirPath.contains("${")) {
            System.out.println("Invalid testapp directory");
            fail("Invalid testapp directory" + testAppDirPath);
        }
        testAppDir.mkdirs();
        ApplicationProjectModel appPrj = new ApplicationProjectModel(testAppDir.getAbsolutePath(), new Properties());
        ServiceProjectModel suPrj = ServiceProjectModel.newInstance(sd.getServiceType(), appPrj);
        Properties configProps = sd.getDefaultConfiguration().getProperties();
        configProps.setProperty("service.configuration.mode", sd.getDefaultConfiguration().getName());
        StringReader iflReader = new StringReader("# empty ifl model");
        IFLModel ifl = new IFLReader().read(iflReader);
        sg.generateServiceArtifacts(suPrj, ifl, "t1", configProps);
    }
}

