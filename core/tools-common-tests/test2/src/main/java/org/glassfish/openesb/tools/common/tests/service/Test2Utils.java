/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.tools.common.tests.service;

import java.net.URL;
import org.glassfish.openesb.tools.common.DefaultToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 *
 * @author chikkala
 */
public class Test2Utils {
    DefaultToolsLookup  mLookup;
    public ServiceDescriptor getServiceDescriptor(URL[] classpathURLs, String serviceType) throws Exception {
        if ( this.mLookup == null ) {
            this.mLookup = new DefaultToolsLookup();
        }
        this.mLookup.addClassLoaderURLs(classpathURLs);
        ServiceDescriptor sd = this.mLookup.getServiceDescriptor(serviceType);
        return sd;
    }
}
