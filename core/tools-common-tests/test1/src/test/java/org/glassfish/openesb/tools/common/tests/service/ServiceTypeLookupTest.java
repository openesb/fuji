/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.tests.service;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 *
 * @author chikkala
 */
public class ServiceTypeLookupTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ServiceTypeLookupTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ServiceTypeLookupTest.class);
    }
    /**
     * Rigourous Test :-)
     */
    public void testServiceTyper() throws Exception {
        String test1ServiceType = "test1";
        String test2ServiceType = "test2";

        ServiceDescriptor test1SD = Test1Utils.getServiceDescriptor(test1ServiceType);
        ServiceDescriptor test2SD = Test1Utils.getServiceDescriptor(test2ServiceType);

        assertEquals(test1ServiceType, test1SD.getServiceType());
        assertEquals(test2ServiceType, test2SD.getServiceType());

        loadSPIProviders(test1SD);
    }

    private void loadSPIProviders(ServiceDescriptor sd) throws Exception {
        System.out.println("Loading service SPI implemenations for " + sd.getServiceType());
        // service spi loading test
        ServiceGenerator sg = sd.getServiceGenerator();
        assertNotNull("ServiceGenerator implementation is expected", sg);
        ServiceBuilder sb = sd.getServiceBuilder();
        assertNotNull("Service Builder implemenation is expected", sb);
        ServicePackager sp = sd.getServicePackager();
        assertNotNull("Service Packager implementation is expected", sp);
    }
}

