/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.tools.common.tests.service;

import org.glassfish.openesb.tools.common.DefaultToolsLookup;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 *
 * @author chikkala
 */
public class Test1Utils {

    public static ServiceDescriptor getServiceDescriptor(String serviceType) throws Exception {
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor(serviceType);
        return sd;
    }
}
