/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common.descriptor;

/**
 * This class provides the details about the property including its localized
 * description and the type.
 *
 * @author chikkala
 */
public interface PropertyInfo extends FeatureInfo {
    /**
     * represents data type of the  value of the property. In case the value of the
     * property is a container of set of values, the data type of the object in
     * the container can be specified by this enum.
     */
    public static enum DataType {
        STRING("string"),
        BOOLEAN("boolean"),
        INT("int"),
        LONG("long"),
        FLOAT("float"),
        DATE("date"),
        FILE("file"),
        REGEXP("regexp");

        private String xsdEnum;

        DataType(String xsdEnum) {
            this.xsdEnum = xsdEnum;
        }

        public String toXsdEnum() {
            return this.xsdEnum;
        }

        public static DataType fromXsdEnum(String xsdEnum) {
            if (xsdEnum == null ) {
                return null;
            } else {
                return DataType.valueOf(xsdEnum.toUpperCase());
            }
        }
    }
    /**
     * format of the property value that can be used to contruct object for the
     * string value of the property.
     */
    public static enum FormatType {
        SCALAR("scalar"),
        ARRAY("array"),
        LIST("list"),
        MAP("map");

        private String xsdEnum;

        FormatType(String xsdEnum) {
            this.xsdEnum = xsdEnum;
        }

        public String toXsdEnum() {
            return this.xsdEnum;
        }

        public static FormatType fromXsdEnum(String xsdEnum) {
            return FormatType.valueOf(xsdEnum.toUpperCase());
        }
    }
    /**
     * get name of the property
     * @return
     */
    String getName();
    /**
     * default value of the property
     * @return
     */
    String getDefaultValue();
    /**
     * format of the container that represents the value
     * @return
     */
    FormatType getFormat();
    /**
     * type of the value of the property. In case the value is a
     * container of set of values, it is the data type of the object in the container.
     * @return
     */
    DataType getDataType();
    /**
     * get name of group in which this property will be displayed/used.
     * @return
     */
    String getGroup();
    /**
     * return an integer representing the position. May be used in the ui layout for this
     * property.
     * @return
     */
    int getPosition();
}
