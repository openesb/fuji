/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service.spi;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * A provider of given service type can implement this interface to perform
 * processing related to packaging service artifacts in an application
 * archive.
 *
 * This is an optional interface. Service type providers should only implement
 * this interface if they have specific requirements which are not met
 * by the default service packaging implementation in the application packaging
 * plugin.
 *
 * @see ServiceProjectModel
 * @see ApplicationProjectModel
 * @see IFLModel
 *
 * @author chikkala
 */
public interface ServicePackager {

    /**
     * packages all the services artifacts into a service unit. Application
     * builder calls this method to package the service artifacts into service
     * unit archive file.
     *
     * @param prjModel project view for this service type
     * @param services list of services of this service type used in the ifl.
     * @param ifl IFL Model in which the services are used.
     * @return service unit archive file
     * @throws java.io.IOException
     */
    public File packageServiceUnit(
            ServiceProjectModel prjModel,
            List<String> services,
            IFLModel ifl) throws IOException;
    /**
     * This method allows you to play around with service resources before
     * the application packager creates the application archive.
     *
     * @param prjModel project view for this service type
     * @param services list of services of this service type used in the ifl.
     * @throws java.io.IOException throwing an exception will abrubtly end
     * the application packaging goal, resulting in a failed build.
     */
    public void beforeServicePackaging(
            ServiceProjectModel prjModel,
            List<String> services) throws IOException;
    /**
     * This method allows you to play with the application bits before they
     * are packaged into a service assembly.
     *
     * @param prjModel project view for this service type
     * @param services list of services of this service type used in the ifl.
     * @param appPrjModel application project view
     * @throws java.io.IOException  throwing an exception will abrubtly end
     * the application packaging goal, resulting in a failed build.
     */
    public void beforeApplicationPackaging(
            ServiceProjectModel prjModel,
            List<String> services,
            ApplicationProjectModel appPrjModel) throws IOException;
}
