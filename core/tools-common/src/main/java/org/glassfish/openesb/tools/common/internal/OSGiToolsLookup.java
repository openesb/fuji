/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.tools.common.internal;

import java.io.File;
import java.io.FileFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.service.spi.Provider;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

/**
 * This class provides the concrete implementation of the ToolsLookup for handling
 * the service descriptor in a bundle from the runtime.
 *
 * It provides the additional helper methods to install the service descriptor
 * bundles into the runtime and lookup the service descriptor provider implemenation
 * from the bundle.
 *
 * When a tools bundle activator starts the tools bundle, it registers this implemenation
 * as a OSGi service and invokes the loadToolsExtensions method to install all the
 * service descritpor bundles in an extension directory and then loads the
 * service descriptor providers from the bundles.
 * 
 * @see ToolsBundelActivator
 *
 * @author chikkala
 */
public class OSGiToolsLookup extends ToolsLookup {

    private static final Logger LOG = Logger.getLogger(OSGiToolsLookup.class.getName());
    /**
     * install root prop of the gfv3
     */
    private static final String PROP_GFV3_INSTALL_ROOT_DIR = "com.sun.aas.installRoot";
    /**
     * under glassfish v3, we look for tools ext at ${com.sun.aas.installRoot}/fuji/tools-ext
     */
    private static final String DEF_GFV3_FUJI_TOOLS_EXT_DIR_PATH = "fuji/tools-ext";
    /**
     * the system property for tools extension dir
     */
    private static final String PROP_FUJI_TOOLS_EXT_DIR = "fuji.tools.ext.dir";
    /**
     * the default log dir
     */
    private static final String DEF_FUJI_TOOLS_EXT_DIR_NAME = "tools-ext";

    /**
     * <bundleId, serviceType> map to track load and unload providers.
     */
    private Map<Long, String> mBundleMap;

    public OSGiToolsLookup() {
        super();
        this.mBundleMap = new HashMap<Long, String>();
    }

    @Override
    protected Provider findServiceDescriptorProvider(String serviceType) {
        //TODO: may be, we can  kick of the search for bundles.
        LOG.info("findServiceDescriptorProvider not supported by OSGiToolslookup. " +
                "It will automatically load the service descriptor provider on descritpor bundle activation");
        return null;
    }

    private void dumpSystemProperties() {
        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(writer);
        System.getProperties().list(out);
        LOG.info(writer.getBuffer().toString());
    }
    private String getGFV3FujiToolsExtensionDir() {
        String extDirValue = null;
        String gfv3InstallRoot = System.getProperty(PROP_GFV3_INSTALL_ROOT_DIR);
        if (gfv3InstallRoot != null ) {
            File gfv3ExtDir = new File(gfv3InstallRoot, DEF_GFV3_FUJI_TOOLS_EXT_DIR_PATH);
            extDirValue = gfv3ExtDir.getAbsolutePath();
        }
        return extDirValue;
    }
    
    private File getExtensionDir() {
        File extDir = null;
        String defDirValue = System.getProperty("user.dir") + File.separator + DEF_FUJI_TOOLS_EXT_DIR_NAME;
        String extDirValue = System.getProperty(PROP_FUJI_TOOLS_EXT_DIR);
        if ( extDirValue == null ) {
            extDirValue = getGFV3FujiToolsExtensionDir();
        }
        if ( extDirValue == null ) {
            extDirValue = defDirValue;
        }
        extDir = new File(extDirValue);
        LOG.info("Fuji Tools Extension Directory: " + extDir.getAbsolutePath());
        return extDir;
    }

    private List<File> findExtensionBundleFiles(BundleContext ctx) {
        List<File> extFiles = new ArrayList<File>();
        File extDir = getExtensionDir();
        if (extDir == null || !extDir.exists() || !extDir.isDirectory()) {
            LOG.info("Tools Extension dir does not exists or invalid " + extDir);
            return extFiles;
        }
        File[] extBundleFiles = extDir.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return (!pathname.isDirectory() && pathname.getName().endsWith(".jar"));
            }
        });
        if (extBundleFiles != null) {
            extFiles = Arrays.asList(extBundleFiles);
        }
        return extFiles;
    }

    public List<String> getAvailableServiceTypes() {
       List<String> serviceTypes =  new ArrayList<String>();
       serviceTypes.addAll(this.getServiceDescriptorProviders().keySet());
       return serviceTypes;
    }
    /**
     * loads service descriptor provider from a bundle. This will be used in
     * loading all service descriptors from a
     * @param extBundle
     * @return
     */
    public Provider loadToolsExtension(Bundle extBundle) {
        Provider provider = null;
        try {
            String providerMetaInfRes = Utils.META_INF_SERVICES + Provider.class.getName();
            URL providerConfigURL = extBundle.getEntry(providerMetaInfRes);
            if (providerConfigURL == null) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("No jar service configuration found for Service descriptor Provider in bundle " + extBundle.getSymbolicName());
                }
                return null;
            }
            String implClass = Utils.readJarServiceConfig(providerConfigURL);
            if (implClass == null) {
                LOG.fine("No Implemenation class name found in jar service configuration for Service descriptor Provider in bundle " + extBundle.getSymbolicName());
                return null;
            }
            LOG.fine("Found Service Descriptor Provider class name " + implClass);
            provider = instantiateServiceDescriptorProvider(extBundle, implClass);
            if (provider != null) {
                this.addServiceDescriptorProvider(provider.getServiceType(), provider);
                this.mBundleMap.put(extBundle.getBundleId(), provider.getServiceType());
            } else {
                LOG.info("Can not instantiate provider object with " + implClass);
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, null, ex);
        }
        return provider;
    }

    public void unloadToolsExtension(Bundle extBundle) {
        Long bundleId = extBundle.getBundleId();
        String serviceType = this.mBundleMap.get(bundleId);
        if (serviceType != null) {
            this.removeServiceDescriptorProvider(serviceType);
        }
    }

    public void loadToolsExtensions(BundleContext ctx) {

        List<File> extFiles = findExtensionBundleFiles(ctx);

        LOG.info("Installing Tools extension bundles.... " + extFiles.size());
        for (File extFile : extFiles) {
            try {
                String bundleURL = extFile.getAbsoluteFile().toURI().toURL().toString();
                installExtensionBundle(ctx, bundleURL);
            } catch (Exception ex) {
                LOG.log(Level.INFO, null, ex);
            }
        }
        LOG.info("Loading the service descriptor providers...");

        // load the service descriptor provider from any bundle.
        Bundle[] allBundles = ctx.getBundles();
        for (Bundle bundle : allBundles) {
            switch(bundle.getState()) {
                case Bundle.RESOLVED:
                case Bundle.ACTIVE:
                    // LOG.info("Looking up bundle " + bundle.getSymbolicName() + " for service descriptor provider...");
                    loadToolsExtension(bundle);
                    break;
            }
        }
    }


    private Bundle installExtensionBundle(BundleContext ctx, String bundleURL) {
        Bundle bundle = null;
        try {
            bundle = ctx.installBundle(bundleURL);
        } catch (BundleException ex) {
            String warningMsg = "Unable to install bundle " + bundleURL + ". Exception: " + ex.getMessage();
            LOG.warning(warningMsg);
            LOG.log(Level.FINE, warningMsg, ex);
        }
        if (bundle != null) {
            String result = Long.toString(bundle.getBundleId());
            try {
                bundle.start();
            } catch (Exception ex) {
                // log the exception and print the warning to console.
                String warningMsg = "Unable to start bundle " + result + ". Exception: " + ex.getMessage();
                LOG.warning(warningMsg);
                LOG.log(Level.FINE, warningMsg, ex);
            }
        }
        return bundle;
    }

    public static String createToolsLookupServiceFilter() {
        String filter = null;
            filter = "(" + Constants.OBJECTCLASS + "=" + ToolsLookup.class.getName() + ")";
        return filter;
    }

    protected final Provider instantiateServiceDescriptorProvider(Bundle bundle, String implClass) {
        Provider provider = null;
        try {
            Class providerClass = Provider.class;
            Class instClass = bundle.loadClass(implClass);
            if (!providerClass.isAssignableFrom(instClass)) {
                LOG.info(clazzToString(instClass) + " not a subclass of " + clazzToString(providerClass));
            } else {
                // instantiate the class
                provider = (Provider) instClass.newInstance();
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }

        return provider;
    }
}
