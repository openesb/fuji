/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceEntry.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.tools.common.jbi.descriptor;

import javax.xml.namespace.QName;
import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public abstract class ServiceEntry extends Descriptor {
    
    private static final String SERVICE_NAME = 
            "service-name";
    private static final String ENDPOINT_NAME = 
            "endpoint-name";
    private static final String INTERFACE_NAME   = 
            "interface-name";
    
    private String namespacePrefix_;
    
    
    public ServiceEntry(Element element) {
        super(element);
    }
    
    public ServiceEntry(Element element, int serviceIdx) {
        super(element);
        namespacePrefix_ = "ns" + serviceIdx;
    }
    
    public QName getServiceName() {
        return getQName(element_.getAttribute(SERVICE_NAME));
    }

    public QName getInterfaceName() {
        return getQName(element_.getAttribute(INTERFACE_NAME));
    }

    public String getEndpointName() {
        return element_.getAttribute(ENDPOINT_NAME);
    }
    
    public void setServiceName(QName serviceName) {
        String prefix = serviceName.getPrefix();
        if (prefix == null || prefix.length() == 0) {
            prefix = "s" + namespacePrefix_;
        }
        element_.setAttribute(SERVICE_NAME, 
                prefix + ":" + serviceName.getLocalPart());
        element_.getOwnerDocument().getDocumentElement().setAttribute(
                "xmlns:" + prefix, serviceName.getNamespaceURI());
    }

    public void setInterfaceName(QName interfaceName) {
        String prefix = interfaceName.getPrefix();
        if (prefix == null || prefix.length() == 0) {
            prefix = "i" + namespacePrefix_;
        }
        element_.setAttribute(INTERFACE_NAME, 
                prefix + ":" + interfaceName.getLocalPart());
        element_.getOwnerDocument().getDocumentElement().setAttribute(
                "xmlns:" + prefix, interfaceName.getNamespaceURI());
    }

    public void setEndpointName(String endpointName) {
        element_.setAttribute(ENDPOINT_NAME, endpointName);
    }
    
    private QName getQName(String qnameStr) {
        QName qname;
        int colIdx;
        
        if (qnameStr == null || qnameStr.length() == 0) {
            return null;
        }
        else if ((colIdx = qnameStr.indexOf(":")) >= 0) {
            String ns = qnameStr.substring(0, colIdx);
            String lp = qnameStr.substring(colIdx + 1, qnameStr.length());
            String uri = element_.lookupNamespaceURI(ns);
            qname = new QName(uri, lp);
        }
        else {
            qname = new QName(qnameStr);
        }
        
        return qname;
    }

    @Override
    protected void initialize() {
    }
}
