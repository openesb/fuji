/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.internal;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.internal.service.ServiceDescriptorImpl;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;
import org.glassfish.openesb.tools.common.templates.TemplateException;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.SynchronousBundleListener;

/**
 * When the tools common code is installed in the OSGi container, osgi related
 * tools specific code is initialized here.
 *
 * During initialization, it creates the concrete implementation of the <code>ToolsLookup</code>
 * and registers it as a OSGi service so that other parts of the tools ( such as webui runtime)
 * can get the ToolsLookup interface from a OSGi service and use it like
 *
 * <pre>
 *       ServiceReference ref = ctx.getServiceReference(ToolsLookup.class.getName());
 *       ToolsLookup lkup = (ToolsLookup) ctx.getService(ref);
 *       ServiceDescriptor sd = lkup.getServiceDescriptor("foo");
 * </pre>
 *
 * It also installs the service descriptor bundles from the tools extension directory (defaults
 * is a "tools-ext" under fuji root directory and loads the service descriptor providers from them.
 * So, to make any service type's service descriptor available to the tools, the service descriptor
 * bundle of the service type can be placed under the tools extension directory and restart the server.
 * 
 * @see OSGiToolsLookup
 * 
 * @author chikkala
 */
public class ToolsBundleActivator implements BundleActivator, SynchronousBundleListener {

    private static final Logger LOG = Logger.getLogger(ToolsBundleActivator.class.getName());
    ServiceRegistration mSrvReg;
    OSGiToolsLookup mLookup;
    BundleContext mCtx;

    public void start(BundleContext ctx) throws Exception {
        this.mCtx = ctx;
        init();

    }

    public void stop(BundleContext ctx) throws Exception {
        unregisterToolsLookup(ctx);
        ctx.removeBundleListener(this);
    }

    /**
     * Observe bundle events to see if Tools extension bundles are changing state.
     * @param event the bundle event
     */
    public void bundleChanged(BundleEvent event) {
        //  LOG.info("Handling bundle Change " + event.getType() + " For " + event.getBundle().getSymbolicName());
        switch (event.getType()) {
            case BundleEvent.RESOLVED:
                loadToolsExtension(event.getBundle());
                break;
            case BundleEvent.UNINSTALLED:
                unloadToolsExtension(event.getBundle());
                break;
            case BundleEvent.STARTED:
                break;
            case BundleEvent.STOPPED:
                break;
            case BundleEvent.UPDATED:
//                loadToolsExtension(event.getBundle());
                break;
        }
    }

    private void init() {
        try {
            // template engine test
            initServiceDescriptorJAXB();
            initTemplateEngine();
            initToolsExtensions();

        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    private void initServiceDescriptorJAXB() {
        // load default service descriptor impl to test the jaxb model works at runtime
        try {
            ServiceDescriptor defSD = ServiceDescriptorImpl.getDefaultServiceDescriptor();
//            LOG.info("Default Service Descriptor: service type " + defSD.getServiceType());
//            LOG.info("Default Service Descriptor: component name " + defSD.getComponentName());
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    private void initTemplateEngine() {
        try {
            String template = "TemplateEngine initialized. Hello $jbi.runtime !!!";
            StringReader reader = new StringReader(template);
            StringWriter writer = new StringWriter();
            Properties props = new Properties();
            props.setProperty("jbi.runtime", "Fuji");
            TemplateEngine te = TemplateEngine.getDefault();
            te.processTemplate(reader, writer, props);
            String result = writer.getBuffer().toString();
            LOG.info(result);
        } catch (TemplateException ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    private void initToolsExtensions() {
        if (this.mCtx == null) {
            LOG.info("Bundle Context is not initialized");
            return;
        }
        registerToolsLookup(this.mCtx);
        registerToolsLookupServiceListener();
        // load extensions and register bundle listener
        Thread t = new Thread(new Runnable() {

            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    LOG.log(Level.INFO, null, ex);
                }
                loadToolsExtensions(mCtx);
                registerBundleListener(mCtx);
                listToolsExtensions(mCtx);
            }
        });
        t.start();
    }

    private void registerBundleListener(BundleContext ctx) {
        ctx.addBundleListener(this);
    }

    private void registerToolsLookup(BundleContext ctx) {
        try {
            this.mLookup = new OSGiToolsLookup();
            Dictionary serviceProps = new Hashtable();
            List<String> availableServiceTypes = new ArrayList<String>();
            serviceProps.put(OSGiToolsLookup.PROP_AVAILABLE_SERVICE_TYPES, availableServiceTypes);
            String lookupServiceClasss = ToolsLookup.class.getName();
            this.mSrvReg = ctx.registerService(lookupServiceClasss, this.mLookup, serviceProps);
            LOG.info("ToolsLookup Service registered.");
        } catch (Exception ex) {
            LOG.log(Level.INFO, "OSGi Service registration for ToolsLookup Failed", ex);
        }
    }

    private void unregisterToolsLookup(BundleContext ctx) {
        try {
            if (this.mSrvReg != null) {
                this.mSrvReg.unregister();
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, "OSGi Service unregistration for ToolsLookup Failed", ex);
        } finally {
            this.mSrvReg = null;
        }
    }

    private void updateToolsLookupServiceProperties() {
        Dictionary serviceProps = new Hashtable();
        List<String> availableServiceTypes = new ArrayList<String>();
        if (this.mLookup != null) {
            availableServiceTypes = this.mLookup.getAvailableServiceTypes();
        }
        serviceProps.put(OSGiToolsLookup.PROP_AVAILABLE_SERVICE_TYPES, availableServiceTypes);
        if (this.mCtx != null) {
            try {
                this.mSrvReg.setProperties(serviceProps);
            } catch (Exception ex) {
                LOG.log(Level.INFO, "Unable to update ToolsLookup OSGI service properties", ex);
            }
        }
    }

    private void loadToolsExtensions(BundleContext ctx) {
        if (this.mLookup != null) {
            this.mLookup.loadToolsExtensions(ctx);
            updateToolsLookupServiceProperties();
        } else {
            LOG.info("### Tools Lookup has not been initialized to Load the tools extensions");
        }
    }

    private void loadToolsExtension(Bundle extBundle) {
        if (this.mLookup != null) {
            this.mLookup.loadToolsExtension(extBundle);
            updateToolsLookupServiceProperties();
        } else {
            LOG.info("### Tools Lookup has not been initialized to Load the tools extensions");
        }
    }

    private void unloadToolsExtension(Bundle extBundle) {
        if (this.mLookup != null) {
            this.mLookup.unloadToolsExtension(extBundle);
            updateToolsLookupServiceProperties();
        } else {
            LOG.info("### Tools Lookup has not been initialized to Unload the tools extensions");
        }
    }

    private void listToolsExtensions(BundleContext ctx) {
        try {
            ServiceReference ref = ctx.getServiceReference(ToolsLookup.class.getName());
            ToolsLookup lkup = (ToolsLookup) ctx.getService(ref);
            List<ServiceDescriptor> sdList = lkup.getAvailableServiceDescriptors();
            LOG.info("#### List of Avaibale Service descriptors " + sdList.size());
            for (ServiceDescriptor sd : sdList) {
                LOG.info("Service type: " + sd.getServiceType() +
                        " Component name: " + sd.getComponentName());
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }

    private void registerToolsLookupServiceListener() {
        if (this.mCtx == null) {
            LOG.info("Cannot register ToolsLookupServiceListener: Bundle Context is NULL.");
            return;
        }
        String filter = OSGiToolsLookup.createToolsLookupServiceFilter();
        try {
            this.mCtx.addServiceListener(new ServiceListener() {

                public void serviceChanged(ServiceEvent evt) {
                    if (ServiceEvent.MODIFIED == evt.getType()) {
                        // TODO: log it at fine level
                        LOG.info("Service Modified " + evt.getServiceReference().getProperty(ToolsLookup.PROP_AVAILABLE_SERVICE_TYPES));
                    // TODO: update web ui for deteted service type or the new service type added.
                    }
                }
            }, filter);
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
    }
}
