/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.templates;

import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

/**
 *
 * @author chikkala
 */
public class VelocityTemplateEngine extends TemplateEngine {

    private static final Logger LOG = Logger.getLogger(VelocityTemplateEngine.class.getName());
    private VelocityEngine mVE;

    private VelocityTemplateEngine() {
        initVelocityEngine();
    }

    public static VelocityTemplateEngine newInstance() {
        return new VelocityTemplateEngine();
    }

    private void initVelocityEngine() {
        try {
            this.mVE = new VelocityEngine();
            String veRuntimeLogClass = "org.apache.velocity.runtime.log.JdkLogChute";
            this.mVE.setProperty(VelocityEngine.RUNTIME_LOG_LOGSYSTEM_CLASS,veRuntimeLogClass);
            this.mVE.init();
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
            this.mVE = null;
        }
    }

    @Override
    public void processTemplate(Reader template, Writer output, Properties context) throws TemplateException {
        if (this.mVE != null) {
            try {
                String logTag = "FujiVelocityEngineLog";
                VelocityContext vCtx = new VelocityContext();
                // populate velocity context with the template context
                // convert the template context to property map to support dot separated property names.
                Map<String, Object> propNodeMap = PropertyMap.convertProperties(context);
                for (String key : propNodeMap.keySet() ) {
                    vCtx.put((String)key, propNodeMap.get(key));
                }
                this.mVE.evaluate(vCtx, output, logTag, template);
            } catch (Exception ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
                throw new TemplateException(ex);
            }
        } else {
            throw new TemplateException("Velocity Template Engine is not initialized properly. Check log for details");
        }
    }
}
