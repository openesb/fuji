/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Configuration.UsageHint;
import org.glassfish.openesb.tools.common.descriptor.Template;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;

/**
 * This class provides helper methods to build the service configuration properties
 * and also defines the standard/required properties that should present in the 
 * in memory model (service configuation properties object) of service configuration
 * to use it as input in generate, build and package phases of the service.
 * 
 * @author chikkala
 */
public class ServiceConfigurationHelper {

    private static final Logger LOG = Logger.getLogger(ServiceConfigurationHelper.class.getName());
    /**
     * standard property to define the service type in the configuration parameters.
     */
    public static final String PROP_SERVICE_TYPE = "service.type";
    /**
     * standard property to define the service name in the configuration parameters.
     * <p>
     * service name is the name of the service declared in the IFL.
     */
    public static final String PROP_SERVICE_NAME = "service.name";
    /**
     * standard property to define the service group in the configuration parameters.
     * <p>
     * service group is the group name given to a set of services defines as a group in IFL.
     * <p>
     * This property may be optionally used in custom code generation, build and packaging
     */
    public static final String PROP_SERVICE_GROUP = "service.group";
    /**
     * standard property to define the list of consumers in the service group in the configuration parameters.
     * <p>
     * String with comma(,) separated service names defined in the consumers list of the
     * service group in IFL. for example, "c1,c2" from IFL declaration bpel "mybpel" ["p1","p2"] calls ["c1, "c2"]
     * <p>
     * This property may be optionally used in custom code generation, build and packaging
     */
    public static final String PROP_SERVICE_GROUP_CONSUMES = "service.group.consumes";
    /**
     * standard property to define the list of providers in the service group in the configuration parameters.
     * <p>
     * String with comma(,) separated service names defined in the providers list of the
     * service group in IFL. for example, "p1,p2" from IFL declaration bpel "mybpel" ["p1","p2"] calls ["c1, "c2"]
     * <p>
     * This property may be optionally used in custom code generation, build and packaging
     */
    public static final String PROP_SERVICE_GROUP_PROVIDES = "service.group.provides";
    /**
     * standard property to define the service configuration mode in the configuration parameters.
     * <p>
     * configuration mode is the name of the configuration defined in the descriptor.
     * for each configuration mode, there will be set of configuration properties defined in
     * the service descriptor which should be present in the sevice configuration properties.
     */
    public static final String PROP_SERVICE_CONFIG_MODE = "service.configuration.mode";
    /**
     * standard property to define the service invocation type in the configuration parameters
     * <p>
     * invocation type is "consumer" or "provider" which will be determined by the use of the
     * service in the IFL. If the property not present, the default value is "provides"
     * <p>
     * This property may be optionally used in custom code generation, build and packaging
     */
    public static final String PROP_SERVICE_MESSAGE_EXCHANGE_ROLE = "service.message.exchange.role";
    /**
     * standard property to define the type of message exchanges a service should support in the configuration parameters.
     * <p>
     * message exchange type is "inonly", "inout" or "any" which will be determined by the use of the
     * service in the IFL. If the  property not present, the default value is "any".
     * <p>
     * This property may be optionally used in custom code generation, build and packaging
     */
    public static final String PROP_SERVICE_MESSAGE_EXCHANGE_TYPE = "service.message.exchange.type";
    /**
     * property that provides the usage of the service in the IFL flow.
     * valid values for this property are the enumerated values for the "usageHint" attribute in the
     * configuration option in the service descriptor xml schema. which are in,out,inout,outin,ondemand,any.
     */
    public static final String PROP_SERVICE_USAGE_HINT = "service.usage.hint";

    public static final String PROP_SERVICE_NAMESPACE = "service.ns";


    /**
     * default configuration mode. if no configuration mode is defined
     */
    public static final String DEF_SERVICE_CONFIG_MODE = "default";
    /**
     *
     */
    public static final String PROVIDER_ROLE = "provider";
    /**
     *
     */
    public static final String CONSUMER_ROLE = "consumer";
    /**
     *
     */
    public static final String IN_ONLY_MESSAGE_EXCHANGE_TYPE = "inonly";
    /**
     *
     */
    public static final String IN_OUT_MESSAGE_EXCHANGE_TYPE = "inout";
    /**
     * 
     */
    public static final String ANY_MESSAGE_EXCHANGE_TYPE = "any";

    /**
     * Default name for service configuration file
     */
    public static final String DEF_SERVICE_CONFIG_FILE = "service.properties";
    /**
     *
     */
    private ServiceDescriptor mSD;

    public ServiceConfigurationHelper(ServiceDescriptor sd) {
        this.mSD = sd;
        // LOG.setLevel(Level.FINE);
    }

    public ServiceDescriptor getServiceDescriptor() {
        return this.mSD;
    }
    /**
     * return the default configuration mode for the service type.
     * if the default mode is in the descriptor, return its name or
     * return the value "default"
     * @return
     */
    public String getDefaultConfigurationMode() {
        String defConfigMode = null;
        Configuration config = null;
        config = this.mSD.getDefaultConfiguration();
        if (config != null) {
            defConfigMode = config.getName();
        }
        if ( defConfigMode == null) {
            defConfigMode = DEF_SERVICE_CONFIG_MODE;
        }
        return defConfigMode;
    }

    /**
     * loads the configuration from service descriptor.
     * @param configMode name of the configuration to load. null for loading default
     * configuration properties.
     * @return return set of properties loaded from descriptor that represents the
     * configuration mode passed. null if the configuration mode is not found in
     * the descriptor.
     */
    public Properties loadConfigurationFromDescriptor(String configMode) {
        Properties configProps = null;
        Configuration config = null;
        if (configMode == null || DEF_SERVICE_CONFIG_MODE.equalsIgnoreCase(configMode)) {
            config = this.mSD.getDefaultConfiguration();
            LOG.fine("Defalt configuration properties loaded from descriptor " + this.mSD.getServiceType());
        } else {
            for (Configuration tmpConfig : this.mSD.getConfigurations()) {
                dumpProperties(tmpConfig.getName(), tmpConfig.getProperties());
                if (configMode.equals(tmpConfig.getName())) {
                    config = tmpConfig;
                    break;
                }
            }
        }
        if (config == null) {
            LOG.fine("Did not find the properties for the configuration mode " + configMode + this.mSD.getServiceType());
            return null;  // No configuration found.
        }
        // load the properties
        configProps = new Properties();
        configProps.putAll(config.getProperties());
        // set the configuration mode.
        String configName = config.getName();
        if (configName == null) {
            configName = DEF_SERVICE_CONFIG_MODE;
        }
        configProps.setProperty(PROP_SERVICE_CONFIG_MODE, configName);

        UsageHint usageHint = config.getUsageHint();
        if ( usageHint != null) {
            configProps.setProperty(PROP_SERVICE_USAGE_HINT, usageHint.toXsdEnum());
        }

        return configProps;
    }

    /**
     * loads the configuraiton defined in the service descriptor based on the
     * usage hint provided.
     * <p>
     * This will be used when user did not provide any configuration mode, but
     * the hint is derived from the IFL usage of the service.
     * 
     * @param usageHint usage hint.
     * @return configuration properties read from the configuration in the
     * descriptor.
     */
    public Properties loadConfigurationFromDescriptor(UsageHint usageHint) {
        Properties configProps = null;
        Configuration config = null;
        LOG.fine("Loading configuration from descriptor using usage Hint " + usageHint);
        if (usageHint == null) {
            config = this.mSD.getDefaultConfiguration();
        } else {
            for (Configuration tmpConfig : this.mSD.getConfigurations()) {
                if (usageHint.equals(tmpConfig.getUsageHint())) {
                    config = tmpConfig;
                    break;
                }
            }
            if (config == null) {
                LOG.fine("Did not find the configuration for usage  " + usageHint + " " + this.mSD.getServiceType());
                if (UsageHint.ANY.equals(usageHint)) {
                    // just load the default configuration.
                    config = this.mSD.getDefaultConfiguration();
                }
            }
        }
        if (config == null) {
            // if sd does not have default configuration. return null.
            return null;  // No configuration found.
        }
        // load the properties
        configProps = new Properties();
        configProps.putAll(config.getProperties());
        // set the configuration mode.
        String configName = config.getName();
        if (configName == null) {
            configName = DEF_SERVICE_CONFIG_MODE;
        }
        configProps.setProperty(PROP_SERVICE_CONFIG_MODE, configName);
        
        if ( usageHint != null) {
            configProps.setProperty(PROP_SERVICE_USAGE_HINT, usageHint.toXsdEnum());
        }

        return configProps;

    }

    /**
     * reads the service configuration file existing for the service in the project source.
     * @param prjModel
     * @param serviceName
     * @return loads the properties from the default service.properties file of the service.
     *         return null if the file does not exist.
     */
    public Properties loadConfigurationFromFile(ServiceProjectModel prjModel, String serviceName) {
        Properties configProps = null;
        try {
            File serviceConfigFile = new File(prjModel.getConfigDir(serviceName), DEF_SERVICE_CONFIG_FILE);
            if (serviceConfigFile.exists()) {
                configProps = Utils.readProperties(serviceConfigFile);
            } else {
                LOG.fine("No service configuration file found for service " + serviceName);
            }
        } catch (Exception ex) {
            LOG.fine("Can not read existing service configuration properties");
        }
        return configProps;
    }

    /**
     * saves the given properties to service.properties file of the service. 
     * If the file exists, it overwrites the existing file. It will use the
     * configuration metadata to filter out the additional properties passed to
     * it from the in memory model of the configuration.
     * <p>
     * when filter parameter is set to true, it assumes the config Properties parameter
     * contains addtional in memory model which need to be filtered using the configuration
     * metadata in the service descriptor so that only the properties in the configuration
     * metadata in the service descriptor will be saved to the service.properties file on disk.
     *
     * <p>
     * if the filter parameter is set to false, it assumes that the configuration
     * properties passed to this method are already filtered using the template. so it
     * will save all the passed properties to the service.properties file on disk.
     * <p>
     * This may save the properties along with their configuration metadata such as short description
     * to the file. It may also can use the configuration metadata in the service descriptor to
     * sort the properties into groups.
     * 
     * @param prjModel  service project model
     * @param serviceName service name for which the configuration properties are being saved.
     * @param configProps properties contain the configuration parameters that can be saved to the service.properties
     * @param filter indicate whether the configProps passed are already generated from template or they need to be filtered through
     * configuration from the service descriptor.
     * @return the service configuration file that is created or overwritten.
     * @throws java.io.IOException
     */
    public File saveConfigurationToFile(ServiceProjectModel prjModel, String serviceName, Properties configProps, boolean filter) throws IOException {
        Properties saveProps = configProps;
        if ( filter ) {
            LOG.fine("filtering input props using config in service descriptor...");
            String configMode = configProps.getProperty(PROP_SERVICE_CONFIG_MODE);
            saveProps = loadConfigurationFromDescriptor(configMode);
            if (saveProps == null ) {
                LOG.fine("Can not find the configuration properties for service descriptor. saving the input props without filtering " + configMode);
                saveProps = configProps;
            } else {
                // update the values of config properties from descriptor.
                for ( Object key : saveProps.keySet()) {
                    Object value = configProps.get(key);
                    if ( value != null ) {
                        saveProps.put(key, value);
                    }
                }
            }
        } else {
            LOG.fine("saveing the input props without filtering...");
        }

        File serviceConfigFile = new File(prjModel.getConfigDir(serviceName), DEF_SERVICE_CONFIG_FILE);
        if (!serviceConfigFile.getParentFile().exists()) {
            serviceConfigFile.getParentFile().mkdirs();
        }
        ServiceConfigurationSerializer serializer = new ServiceConfigurationSerializer(this.mSD);
        serializer.save(serviceConfigFile, saveProps, serviceName);

        return serviceConfigFile;
    }
    /**
     * creates the configuration by merging configuration from descriptor, existing configuration
     * and user input. This can be used as a template data to create service artifacts from templates
     * @param prjModel
     * @param serviceName
     * @param serviceConfig external input for the configuration properties which will override the default value.
     * @return
     */
    public Properties createConfiguration(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties userConfig) {
        // 1. read configuration mode
        String userConfigMode = userConfig.getProperty(PROP_SERVICE_CONFIG_MODE);
       
        // 1.load the default values for the config mode if exists in the descriptor.
        Properties defConfig = null;
        UsageHint usageHint = null;
        if ( userConfigMode == null ) {
            LOG.fine("No user configuration mode. finding the mode from IFL usage....");
            // use IFL usage hint to find the right configuration.
            IFLHelper iflHelper = new IFLHelper();            
            try {
                usageHint = iflHelper.getServiceUsageHint(ifl, serviceName);
                LOG.fine("IFL Usage hint " + usageHint);
            } catch (Exception ex) {
                // don't fail not able to find the usage hint. just use default one.
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
            if ( usageHint != null ) {
                LOG.fine("Loading configuration for IFL usage hint " + usageHint);
                defConfig = loadConfigurationFromDescriptor(usageHint);
            } else {
                LOG.fine("Can not determine the usage hint from ifl.");
            }
            if ( defConfig == null) {
                userConfigMode = DEF_SERVICE_CONFIG_MODE;
                defConfig = loadConfigurationFromDescriptor(userConfigMode);
            }
        } else {        
            defConfig = loadConfigurationFromDescriptor(userConfigMode);
        }

        dumpProperties("Default Configuration------ ", defConfig);

        // 2. load the existing configuration from file if exists.
        Properties currentConfig = loadConfigurationFromFile(prjModel, serviceName);
        dumpProperties("Existing Configuration-------- ", currentConfig);

        Properties newConfig = new Properties();
        String newConfigMode = userConfigMode;

        if (defConfig != null) {
            newConfigMode = defConfig.getProperty(PROP_SERVICE_CONFIG_MODE);
        }
        // 3. load default properties
        if (defConfig != null) {
            newConfig.putAll(defConfig);
        }
        // 4. add any project model specific properties
        newConfig.putAll(prjModel.getProperties());

        // 5. override the default properties with existing properties.
        if (currentConfig != null) {
            String currentConfigMode = currentConfig.getProperty(PROP_SERVICE_CONFIG_MODE);
            if (currentConfigMode == null ) {
                LOG.fine("No configuration mode found in the current configuration on disk... assuming this as default configuration...");
                // no mode defined is assumes the settings are for default mode. The default
                // mode value can be "default" or a user defined default name in the descriptor.
                // for service types that does not support configuration from descriptor,
                // the mode is always "default".
                //  So, check if the newConfig is either "default" or equal to user defined default
                // and then apply.

                // find the default mode from the descriptor if supported.
                String defConfigMode = getDefaultConfigurationMode();
                if (newConfigMode.equals(defConfigMode)){
                    LOG.fine("Adding the current configuration from disk...");
                    newConfig.putAll(currentConfig);
                }
            } else if (currentConfigMode.equals(newConfigMode)) {
                // add previously edited values.
                LOG.fine("Adding the current configuration from disk...");
                newConfig.putAll(currentConfig);
            }
        }
        // 6. overrid with user input properties.
        for (Object key : userConfig.keySet()) {
            LOG.fine("Overriding descriptor + current config with user input...");
            Object value = userConfig.get(key);
            if (newConfig.containsKey(key)) {
                newConfig.put(key, value); // override with userinput.
            }
        }
        // finally add the standard properties
        newConfig.setProperty(PROP_SERVICE_TYPE, this.getServiceDescriptor().getServiceType());
        newConfig.setProperty(PROP_SERVICE_NAME, serviceName);
        newConfig.setProperty(PROP_SERVICE_CONFIG_MODE, newConfigMode);

        String mxRole = getMessageExchangeRoleForService(ifl, serviceName);
        newConfig.setProperty(PROP_SERVICE_MESSAGE_EXCHANGE_ROLE, mxRole);

        String mxType = userConfig.getProperty(PROP_SERVICE_MESSAGE_EXCHANGE_TYPE);
        if (mxType == null) {
            // if user did not choose what type she want to generate, find it out from ifl.
            mxType = getMessageExchangePatternForService(ifl, serviceName);
        }
        newConfig.setProperty(PROP_SERVICE_MESSAGE_EXCHANGE_TYPE, mxType);

        // add group properties
        String serviceGroupName = getServiceGroup(ifl, serviceName);
        String serviceGroupConsumesList = getServiceGroupConsumers(ifl, serviceName);
        String serviceGroupProvidesList = getServiceGroupProviders(ifl, serviceName);
        newConfig.setProperty(PROP_SERVICE_GROUP, serviceGroupName);
        newConfig.setProperty(PROP_SERVICE_GROUP_CONSUMES, serviceGroupConsumesList);
        newConfig.setProperty(PROP_SERVICE_GROUP_PROVIDES, serviceGroupProvidesList);

        dumpProperties("New Configuration------", currentConfig);

        newConfig.setProperty(PROP_SERVICE_NAMESPACE, ifl.getNamespace(serviceName));
        return newConfig;
    }

    /**
     * generates the configuration from the template by applying the configuration
     * properties created from user input, default values in descriptor and the 
     * existing configuration to the template to create a new configuration
     * 
     * @param configTemplate template file in the descriptor.
     * @param config properties that will be applied to the template if the template is
     * a velocity template.
     * @return
     */
    public Properties createConfigurationFromTemplate(Template configTemplate, Properties config) {
        Properties finalConfig = new Properties();
        try {
            Properties newConfig = new Properties();
            if (configTemplate.isFiltered()) {
                // process with template engine
                StringWriter writer = new StringWriter();
                TemplateEngine.getDefault().processTemplate(configTemplate.getURL(), writer, config);
                // LOG.info("Config Properties " + writer.getBuffer().toString());
                ByteArrayInputStream in = new ByteArrayInputStream(writer.getBuffer().toString().getBytes());
                newConfig.load(in);
            } else {
                newConfig = Utils.readProperties(configTemplate.getURL());
            }
            // make sure that the standard configuration is saved.
            finalConfig.setProperty(PROP_SERVICE_CONFIG_MODE, config.getProperty(PROP_SERVICE_CONFIG_MODE, DEF_SERVICE_CONFIG_MODE));
            finalConfig.putAll(newConfig);
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return finalConfig;
    }

    /**
     * generates the service configuration file "service.properties" in a standard directory in the config source path
     * src/main/config/<servicetype>/<serviceName>/service.properties.
     *
     * If there is service.properties template specified in the service descriptor, then merged serviceConfig properties (
     * userInput + defaults from descriptor + env (prj, ifl...) ) are applied to the tempalte to create the
     * service.properties file on disk. If not specified, it will generate the service.properties based on the configuration
     * description in the descriptor.
     *
     * @param prjModel service project model
     * @param ifl ifl model
     * @param serviceName service name for which the configuration is generated
     * @param userInput  properties that contains name of the configuration template(mode) to be used to generate the
     * configuration properties and the set(0..n) of properties whose values will be used to override the default values
     * specified in the service descriptor for the corresponding properties in that configuration.
     *
     * @return file generated.
     */
    public File generateServiceConfiguration(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties userInput) throws IOException {
        // merge the user properties with defaults from configuration and add environment properties (project, ifl etc).
        Properties serviceConfig = this.createConfiguration(prjModel, ifl, serviceName, userInput);

        List<Template> templates = this.getServiceDescriptor().getTemplates();
        File serviceConfigDir = prjModel.getConfigDir(serviceName);
        File serviceConfigFile = new File(serviceConfigDir, ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);

        Template serviceConfigTemplate = null;

        // find the service.properties template 
        for (Template template : templates) {
            String path = template.getAlias();
            if (path.startsWith(ServiceProjectModel.DEF_CONFIG_DIR)) {
                path = path.substring(ServiceProjectModel.DEF_CONFIG_DIR.length() + 1);
                File file = new File(serviceConfigDir, path);
                if (file.getAbsolutePath().endsWith(ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE)) {
                    serviceConfigTemplate = template;
                    break;
                }
            }
        }
        // generate service.properties file.
        Properties saveConfig = serviceConfig;
        boolean filtered = false;
        if (serviceConfigTemplate != null) {
            LOG.fine("##### generating service configuration from template....");
            saveConfig = this.createConfigurationFromTemplate(serviceConfigTemplate, serviceConfig);
            filtered = true;
        } else {
            LOG.fine("##### generating service configuration from memory model....");
        }

        serviceConfigFile = this.saveConfigurationToFile(prjModel, serviceName, saveConfig, !filtered);
        return serviceConfigFile;
    }

    /**
     * returns the group name defined int the ifl for this service name.
     * <p>
     * This information will be avaialble as a property in the configuration propertis.
     * <p>
     * This information may be optionally used in custom code generation, build and packaging
     * @param ifl
     * @param serviceName
     * @return
     */
    public String getServiceGroup(IFLModel ifl, String serviceName) {
        return ifl.getGroupForService(serviceName);
    }
    /**
     * returns the comma separated service names as string for the list of
     * consumer services defined in the ifl group statement belongs to this service name.
     * e.g. "c1,c2,c3". can be used in the services which require a group informatin
     * in generate, build or package phase
     * <p>
     * This information will be avaialble as a property in the configuration propertis.
     * <p>
     * This information may be optionally used in custom code generation, build and packaging
     * phases.
     * @param ifl
     * @param serviceName
     * @return
     */
    public String getServiceGroupConsumers(IFLModel ifl, String serviceName) {
        StringBuilder value = new StringBuilder();
        String serviceGroup = getServiceGroup(ifl, serviceName);
        if (serviceGroup != null && serviceGroup.length() > 0) {
            List<String> consumers = ifl.getServiceReferencesInServiceGroup(serviceGroup);
            if (consumers != null) {
                for (int i = 0; i < consumers.size(); ++i) {
                    if (i > 0) {
                        value.append(",");
                    }
                    value.append(consumers.get(i));
                }
            }
        }
        return value.toString();
    }
    /**
     * returns the comma separated service names as string for the list of
     * provider services defined in the ifl group statement belongs to this service name.
     * e.g. "p1,p2,p3". can be used in the services which require a group informatin
     * in generate, build or package phase
     * <p>
     * This information will be avaialble as a property in the configuration propertis.
     * <p>
     * This information may be optionally used in custom code generation, build and packaging
     * phases.
     * @param ifl
     * @param serviceName
     * @return
     */
    public String getServiceGroupProviders(IFLModel ifl, String serviceName) {
        StringBuilder value = new StringBuilder();
        String serviceGroup = getServiceGroup(ifl, serviceName);
        if (serviceGroup != null && serviceGroup.length() > 0) {
            Set<String> groupedServices = new TreeSet<String>();
            groupedServices.addAll(ifl.getServicesInServiceGroup(serviceGroup));
            groupedServices.removeAll(ifl.getServicesInServiceGroup(serviceGroup));
            List<String> providers = new ArrayList<String>();
            providers.addAll(groupedServices);

            if (providers != null) {
                for (int i = 0; i < providers.size(); ++i) {
                    if (i > 0) {
                        value.append(",");
                    }
                    value.append(providers.get(i));
                }
            }
        } else {
            value.append(serviceName);
        }
        return value.toString();
    }
    /**
     * finds the role of the service (as provider or consumer) based on the usage of the
     * service in
     * <p>
     * This information will be avaialble as a property in the configuration propertis.
     * <p>
     * This information may be optionally used in custom code generation, build and packaging
     * phases.
     * @param ifl
     * @param serviceName
     * @return
     */
    public String getMessageExchangeRoleForService(IFLModel ifl, String serviceName) {
        String role = PROVIDER_ROLE; // DEFAULT.
        if (ifl.isProvided(serviceName)) {
            role = PROVIDER_ROLE;
        } else if (ifl.isConsumed(serviceName)) {
            role = CONSUMER_ROLE;
        }
        return role;
    }

    /**
     * finds the required message exchange patterns that the service configuration should
     * support based on the usage of the service in ifl.
     * <p>
     * This information will be avaialble as a property in the configuration propertis.
     * @param ifl
     * @param serviceName
     * @return
     */
    public String getMessageExchangePatternForService(IFLModel ifl, String serviceName) {
        String mxType = ANY_MESSAGE_EXCHANGE_TYPE;
        // check the usage of the service in the routes and EIPs and find if just inonly is enough
        // or inout is needed etc.
        mxType = (new IFLHelper()).getMessageExchangePatternForService(ifl, serviceName);
        return mxType;
    }

    private void dumpProperties(String msg, Properties props) {
        if (LOG.isLoggable(Level.FINE)) {
            if (props == null) {
                LOG.fine("---Null Props for -- " + msg);
                return;
            }
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            props.list(out);
            LOG.fine("----- " + msg);
            LOG.fine(writer.getBuffer().toString());
        }
    }

}
