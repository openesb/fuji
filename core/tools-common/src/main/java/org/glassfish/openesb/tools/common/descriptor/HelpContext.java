/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.descriptor;

import java.net.URL;

/**
 * This class can be used by any help system that provide a help
 * on a particular data/feature.
 * 
 * @author chikkala
 */
public class HelpContext {

    private String mHelpID;
    private URL mHelpURL;

    /**
     *
     * @param helpID help id that can be used in a help system about this data.
     * @param helpURL help url that can be used in a help system about this data.
     */
    public HelpContext(String helpID, URL helpURL) {
        this.mHelpID = helpID;
        this.mHelpURL = helpURL;
    }
    /**
     * help id that can be used in a help system about this data.
     * @return
     */
    public String getHelpID() {
        return this.mHelpID;
    }
    /**
     * help url that can be used in a help system about this data.
     * @return
     */
    public URL getHelpURL() {
        return this.mHelpURL;
    }
}
