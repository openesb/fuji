/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common.service;

import java.net.URL;
import java.util.Map;

/**
 * This class can be used as the base class to implement the service spi by a specific service type.
 *
 * @see DefaultServiceGenerator
 * @see DefaultServicePackager
 * @see DefaultServiceBuilder
 * 
 * @author chikkala
 */
public class AbstractSPIProvider {

    private ServiceDescriptor mSD;
    private ServiceConfigurationHelper mSCHelper;
   /**
     * @deprecated
     */
    private URL mArchetypeURL;
    /**
     * constructor that takes the service descriptor.
     * @param sd
     */
    public AbstractSPIProvider(ServiceDescriptor sd) {
        this.mSD = sd;
        this.initArchetypeURL(sd);
    }

    protected final String getServiceType() {
        return this.mSD.getServiceType();
    }

    protected final ServiceDescriptor getServiceDescriptor() {
        return this.mSD;
    }

    protected ServiceConfigurationHelper getServiceConfigurationHelper() {
        if ( this.mSCHelper == null && this.mSD != null) {
            this.mSCHelper = new ServiceConfigurationHelper(this.mSD);
        }
        return this.mSCHelper;
    }
    /**
     * @deprecated will be removed when the archetype support is remove.
     */
    protected final URL getArchetypeURL() {
        return this.mArchetypeURL;
    }
    /**
     * @deprecated will be removed when the archetype support is remove.
     * @param sd
     */
    private void initArchetypeURL(ServiceDescriptor sd) {
        this.mArchetypeURL = null;
        Map<String, URL> resMap = this.getServiceDescriptor().getWebResources();
        for ( String key : resMap.keySet()) {
            if (key.endsWith("archetype.jar")) {
                this.mArchetypeURL = resMap.get(key);
            }
        }
    }
}
