/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.templates;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Template Engine that provides the interface to the Template processing engines. The
 * Default implemenation supports the velocity engine.
 * Usage :
 * <code>
 *     TemplateEngine te = TemplateEngine.getDefault();
 *     Properties data = new Properties();
 *     data.setProperty("useName" "chikkala");
 *     data.setProperty("userPhone" "408-888-8888");
 *     File template = new File("/templates/t1.vtl");
 *     File output = new File ("output/t1.out");
 *     te.processTemplate(template, output, data);
 * </code>
 * 
 * @author chikkala
 */
public abstract class TemplateEngine {

    private static final Logger LOG = Logger.getLogger(TemplateEngine.class.getName());
    private static TemplateEngine sDefTE;

    public static TemplateEngine getDefault() {
        if (sDefTE == null) {
            sDefTE = VelocityTemplateEngine.newInstance();
        }
        return sDefTE;
    }

    public abstract void processTemplate(Reader template, Writer output, Properties context) throws TemplateException;

    public void processTemplate(InputStream template, Writer output, Properties context) throws TemplateException {
        InputStreamReader templateReader = null;
        try {
            templateReader = new InputStreamReader(template);
            processTemplate(templateReader, output, context);
        } finally {
            closeReader(templateReader);
        }
    }

    public void processTemplate(File template, Writer output, Properties context) throws TemplateException {
        FileReader templateReader = null;
        try {
            templateReader = new FileReader(template);
            processTemplate(templateReader, output, context);
        } catch (IOException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex); // if the user ignores the exception, we will have a log.
            throw new TemplateException(ex.getMessage(), ex);
        } finally {
            closeReader(templateReader);
        }
    }

    public void processTemplate(URL template, Writer output, Properties context) throws TemplateException {
        InputStream templateInS = null;
        try {
            templateInS = template.openStream();
            processTemplate(templateInS, output, context);
        } catch (IOException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex); // if the user ignores the exception, we will have a log.
            throw new TemplateException(ex.getMessage(), ex);
        } finally {
            closeInputStream(templateInS);
        }
    }

    public void processTemplate(Reader template, File output, Properties context) throws TemplateException {
        FileWriter out = null;
        try {
            out = new FileWriter(output);
            processTemplate(template, out, context);
        } catch (IOException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex); // if the user ignores the exception, we will have a log.
            throw new TemplateException(ex.getMessage(), ex);
        } finally {
            closeWriter(out);
        }
    }

    public void processTemplate(InputStream template, File output, Properties context) throws TemplateException {
        FileWriter out = null;
        try {
            out = new FileWriter(output);
            processTemplate(template, out, context);
        } catch (IOException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex); // if the user ignores the exception, we will have a log.
            throw new TemplateException(ex.getMessage(), ex);
        } finally {
            closeWriter(out);
        }
    }

    public void processTemplate(File template, File output, Properties context) throws TemplateException {
        FileReader templateReader = null;
        try {
            templateReader = new FileReader(template);
            processTemplate(templateReader, output, context);
        } catch (IOException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex); // if the user ignores the exception, we will have a log.
            throw new TemplateException(ex.getMessage(), ex);
        } finally {
            closeReader(templateReader);
        }
    }

    public void processTemplate(URL template, File output, Properties context) throws TemplateException {
        FileWriter out = null;
        try {
            out = new FileWriter(output);
            processTemplate(template, out, context);
        } catch (IOException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex); // if the user ignores the exception, we will have a log.
            throw new TemplateException(ex.getMessage(), ex);
        } finally {
            closeWriter(out);
        }
    }

    private void closeReader(Reader in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException ex) {
                //ignore
            }
        }
    }

    private void closeInputStream(InputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException ex) {
                //ignore
            }
        }
    }

    private void closeWriter(Writer out) {
        if (out != null) {
            try {
                out.close();
            } catch (IOException ex) {
                //ignore
            }
        }
    }
}
