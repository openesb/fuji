/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Identification.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.tools.common.jbi.descriptor;

import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public class Identification extends Descriptor {
    
    public static final String ELEMENT_NAME =
            "identification";
    private static final String NAME = 
            "name";
    private static final String DESCRIPTION = 
            "description";
    
    public Identification(Element element) {
        super(element);
    }
    
    public String getName() {
        return getChildElement(NAME).getTextContent();
    }
    
    public String getDescription() {
        return getChildElement(DESCRIPTION).getTextContent();
    }
    
    public void setName(String name) {
        getChildElement(NAME).setTextContent(name);
    }
    
    public void setDescription(String name) {
        getChildElement(DESCRIPTION).setTextContent(name);
    }
    
    @Override
    protected void initialize() {
        createChildElement(NAME);
        createChildElement(DESCRIPTION);
    }
}
