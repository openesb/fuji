/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.FujiProjectModel;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;
import org.glassfish.openesb.tools.common.templates.TemplateException;

/**
 *
 * @author chikkala
 */
public class DefaultServiceBuilder extends AbstractSPIProvider implements ServiceBuilder {

    private static final Logger LOG = Logger.getLogger(DefaultServiceBuilder.class.getName());

    public DefaultServiceBuilder(ServiceDescriptor sd) {
        super(sd);
        // LOG.setLevel(Level.FINE);
    }

    public void processResources(ServiceProjectModel prjModel, List<String> services, IFLModel ifl) throws IOException {
        LOG.fine("### Processing Service unit Resources for " + this.getServiceType());
        for (String serviceName : services) {
            // check for a service-specific properties file
            File configFile = processConfigurationResources(prjModel, serviceName, ifl);
            Properties configData = createConfigurationData(prjModel, serviceName, ifl, configFile);
            processServiceResources(prjModel, serviceName, ifl, configData);
            processCommonResources(prjModel, serviceName, ifl, configData);
        }
    }

    public void compile(ServiceProjectModel prjModel, List<String> services, IFLModel ifl) throws IOException {
        // by default components will have only configuration files as resources. no code for compile
        // service engiens such as pojo, jruby, xslt might have the compilable code. They will implement
        // this method if needed.
    }

    /**
     * creates a properties that hold data that can be used in processing the resource. default this data
     * is used to process the templates in the resource filder for the service.
     * It conbineds the project properties, standard configuration properties and the derived properties
     * of the service from ifl. 
     *
     * @param prjModel
     * @param serviceName
     * @param ifl
     * @param configFile
     * @return
     * @throws java.io.IOException
     */
    protected Properties createConfigurationData(ServiceProjectModel prjModel, String serviceName, IFLModel ifl, File configFile) throws IOException {

        Properties configData = new Properties();
        // add project properties. applicationId, artifactId, etc will be in.
        configData.putAll(prjModel.getProperties());
        // add service configuration properties
        if (configFile != null && configFile.exists()) {
            configData.putAll(Utils.readProperties(configFile));
        }
        // add additional service configuration properties
        configData.setProperty(FujiProjectModel.PROP_SERVICE_NAME, serviceName);
        configData.setProperty(FujiProjectModel.PROP_SERVICE_TYPE, this.getServiceType());

        String consumedService = "";
        String providedService = "";

        if (ifl.getConsumedServices().contains(serviceName)) {
            providedService = "";
            consumedService = prjModel.getConsumerService(this.getServiceType(), serviceName);
        } else {
            consumedService = "";
            providedService = serviceName;
        }
        configData.put(FujiProjectModel.PROP_CONSUMED_SERVICE, consumedService);
        configData.setProperty(FujiProjectModel.PROP_PROVIDED_SERVICE, providedService);
        return configData;
    }

    /**
     * it copies the standard service.properties file to the output directory. if any service type need more than
     * just copy the service.properties, it should override this method and do appropriate work. for example,
     * the override may copy/generte more configuration files such as catalog.xml to output directory.
     *
     * @param prjModel
     * @param serviceName
     * @param ifl
     * @return
     * @throws java.io.IOException
     */
    protected File processConfigurationResources(ServiceProjectModel prjModel, String serviceName, IFLModel ifl) throws IOException {
        File configFile = null;
        File configFolder = prjModel.getConfigDir(serviceName);
        configFile = new File(configFolder, ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
        File outDir = prjModel.getOutputDir();
        File serviceOutDir = new File(outDir, serviceName);
        File configOutFile = new File(serviceOutDir, ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);

        if (!serviceOutDir.exists()) {
            serviceOutDir.mkdirs();
        }
        // if file exists copy it to build dir
        if (configFile.exists()) {
            Utils.copy(configFile, configOutFile);
        }
        //TODO: return the config file in source or output dir?
        return configFile;
    }

    /**
     * process service specific resources other than configuration here.
     * default configuration will process all the files under service dir.
     * @param prjModel
     * @param serviceName
     * @param ifl
     * @param configData
     * @throws java.io.IOException
     */
    protected void processServiceResources(ServiceProjectModel prjModel, String serviceName, IFLModel ifl, Properties configData) throws IOException {
        File serviceOutDir = new File(prjModel.getOutputDir(), serviceName);
        File serviceResDir = prjModel.getResourcesDir(serviceName);
        processFiles(serviceResDir, serviceOutDir, configData);
    }

    /**
     * process any optional common resources here.
     * 
     * default implemenation copies the common wsdl interfaces ( interface wsdl, message xsd)
     * @param prjModel
     * @param serviceName
     * @param ifl
     * @param configData
     * @throws java.io.IOException
     */
    protected void processCommonResources(ServiceProjectModel prjModel, String serviceName, IFLModel ifl, Properties configData) throws IOException {
        File serviceOutDir = new File(prjModel.getOutputDir(), serviceName);
        File commonWsdlDir = new File(prjModel.getBaseDir(), FujiProjectModel.DEF_COMMON_WSDL_DIR);

        //TODO: the common wsdl files should be copied to the service unit to one place from where
        // the bindings of a service can import or include.  They should not be copied to each service output.

        // for now, copy the interface wsdl and message xsd to each service output dir.
        // to make sure all required wsdl artifacts are in place and valid.

        File messageXsd = new File(serviceOutDir, FujiProjectModel.COMMON_MESSAGE_XSD);
        File interfaceWsdl = new File(serviceOutDir, FujiProjectModel.COMMON_INTERFACE_WSDL);
        File bindingWsdl = new File(serviceOutDir, FujiProjectModel.COMMON_BINDING_WSDL);

        if (!messageXsd.exists()) {
            Utils.copy(new File(commonWsdlDir, FujiProjectModel.COMMON_MESSAGE_XSD), messageXsd);
        }
        if (!interfaceWsdl.exists()) {
            Utils.copy(new File(commonWsdlDir, FujiProjectModel.COMMON_INTERFACE_WSDL), interfaceWsdl);
        }
        if (!bindingWsdl.exists()) {
            Utils.copy(new File(commonWsdlDir, FujiProjectModel.COMMON_BINDING_WSDL), bindingWsdl);
        }
    }

    /**
     * provide the list of files in the directory with some known filter
     * @param parent
     * @return
     */
    protected List<File> listFiles(File parent) {
        File[] files = parent.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                boolean valid = false;
                if (!pathname.isDirectory()) {
                    String name = pathname.getName();
                    // service dir should not have . prefix or .suffix
                    if (!name.startsWith(".")) {
                        valid = true;
                    }
                }
                return valid;
            }
        });
        if (files == null) {
            files = new File[0];
        }
        return Arrays.asList(files);
    }

    protected void copyFile(File src, File dest, Properties data, boolean template) {
        if (!template) {
            //TODO: check for last modified. copy only if the src is latest than dest.
            if (!dest.exists() || (src.lastModified() > dest.lastModified())) {
                try {
                    Utils.copy(src, dest);
                } catch (IOException ex) {
                    LOG.warning("Cannot copy resources " + src.getAbsolutePath());
                }
            }
            return;
        }
        try {
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }
            TemplateEngine.getDefault().processTemplate(src, dest, data);
        } catch (TemplateException ex) {
            LOG.log(Level.WARNING, null, ex);
        }
    }

    /**
     * extended classes can override this to suply service type specific 
     * template filter
     * @return
     */
    protected FileFilter getTemplateFilter() {
        FileFilter templateFilter = new FileFilter() {

            public boolean accept(File pathname) {
                boolean valid = false;
                if (!pathname.isDirectory()) {
                    String name = pathname.getName();
                    // process .wsdl or .properties files by default.
                    // service type specific classes can configure it with additional
                    // filters by overriding.
                    if (name.endsWith(".wsdl") || name.endsWith(".properties")) {
                        valid = true;
                    }
                }
                return valid;
            }
        };
        return templateFilter;
    }

    protected void processFiles(File srcDir, File destDir, Properties data) {
        List<File> files = listFiles(srcDir);
        FileFilter templateFilter = getTemplateFilter();
        for (File file : files) {
            File dest = new File(destDir, file.getName());
            boolean template = templateFilter.accept(file);
            copyFile(file, dest, data, template);
        }
    }
}
