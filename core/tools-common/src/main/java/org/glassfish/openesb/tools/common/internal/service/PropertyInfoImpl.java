/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common.internal.service;

import java.util.ResourceBundle;
import org.glassfish.openesb.tools.common.descriptor.PropertyInfo;
import org.glassfish.openesb.tools.common.internal.service.jaxb.Property;
import org.glassfish.openesb.tools.common.internal.service.jaxb.PropertyRef;

/**
 *
 * @author chikkala
 */
public class PropertyInfoImpl extends FeatureInfoImpl implements PropertyInfo {
    private Property mProp;
    private PropertyRef mPropRef;
    /**
     * holds the property details for property element or property reference element. In case of
     * property reference element, both property and property reference element should be passed.
     * @param i18nBundle
     * @param classLoader
     * @param prop property element 
     * @param propRef property reference element that references the actual property. Can be null if this object is
     * constructed for just for property.
     */
    public PropertyInfoImpl(ResourceBundle i18nBundle, ClassLoader classLoader, Property prop, PropertyRef propRef) {
        super(i18nBundle, classLoader, prop.getDisplayName(), prop.getShortDescription(), prop.getLongDescription());
        this.mProp = prop;
        this.mPropRef = propRef;
    }

    protected Property getJaxbProperty() {
        return this.mProp;
    }

    protected PropertyRef getJaxbPropertyRef() {
        return this.mPropRef;
    }

    public String getName() {
        return this.mProp.getName();
    }

    public String getDefaultValue() {
        String defValue = null;
        if ( this.mPropRef != null) {
            defValue = this.mPropRef.getDefaultValue();
        }
        if ( defValue == null) {
            defValue = this.mProp.getDefaultValue();
        }
        return defValue;
    }

    public FormatType getFormat() {
        String format = this.mProp.getFormat();
        return FormatType.fromXsdEnum(format);
    }

    public DataType getDataType() {
        String dataType = this.mProp.getDataType();
        return DataType.fromXsdEnum(dataType);
    }

    public String getGroup() {
        String group = null;
        if ( this.mPropRef != null) {
            group = this.mPropRef.getGroup();
        }
        if ( group == null) {
            group = this.mProp.getGroup();
        }
        return group;
    }

    public int getPosition() {
        Integer position = null;
        if ( this.mPropRef != null) {
            position = this.mPropRef.getPosition();
        }
        if ( position == null) {
            position = this.mProp.getPosition();
        }
        return position;
    }

}
