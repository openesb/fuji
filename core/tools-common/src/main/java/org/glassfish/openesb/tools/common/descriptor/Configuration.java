/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common.descriptor;

import java.util.List;
import java.util.Properties;

/**
 * This interface provides the service type configuration defined in the descriptor
 * including the localized descriptions of the configuration and its properties.
 *
 * @author chikkala
 */
public interface Configuration {
    /**
     * represents the valid usage of this configuraiton in the ifl routes/eips
     */
    public static enum UsageHint {
        IN("in"),
        OUT("out"),
        INOUT_IN("inout:in"),
        OUTIN_IN("outin:in"),
        INOUT_OUT("inout:out"),
        OUTIN_OUT("outin:out"),
        ONDEMAND("ondemand"),
        ANY("any");

        private String xsdEnum;

        UsageHint(String xsdEnum) {
            this.xsdEnum = xsdEnum;
        }
        
        public String toXsdEnum() {
            return this.xsdEnum;
        }

        public static UsageHint fromXsdEnum(String xsdEnum) {
            if (xsdEnum == null ) {
                return null;
            } else {
                return UsageHint.valueOf(xsdEnum.toUpperCase().replace(":", "_"));
            }
        }
    }
    /**
     * returns the localized description about this configuration
     * @return FeatureInfo object.
     */
    FeatureInfo getInfo();

    UsageHint getUsageHint();
    /**
     * returns the name(mode) of the configuration. which will be the value of the service.configuration.mode.
     * @return
     */
    String getName();
    /**
     * return a particular property details or null if the property with that name not exists in the configuration
     * @param name of the property
     * @return Property object
     */
    Property getProperty(String name);
    /**
     * return all the properties in the configuration.
     * @return List of Property objects.
     */
    List<Property> getPropertyList();
    /**
     * return all the properties defined in the configuration as a <code>java.util.Properties</code> object.
     * @return Properties object contain all the properties in the configuration.
     */
    Properties getProperties();

    /**
     * return all the properties in the configuration.
     * @return List of Property objects.
     */
    List<PropertyGroup> getPropertyGroupList();
    
}
