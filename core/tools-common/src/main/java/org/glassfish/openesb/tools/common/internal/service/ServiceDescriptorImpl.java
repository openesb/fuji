/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.internal.service;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.SchemaFactory;

import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Configuration.UsageHint;
import org.glassfish.openesb.tools.common.descriptor.FeatureInfo;
import org.glassfish.openesb.tools.common.descriptor.HelpContext;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;
import org.glassfish.openesb.tools.common.descriptor.PropertyInfo;
import org.glassfish.openesb.tools.common.descriptor.Template;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ConfigurationOption;
import org.glassfish.openesb.tools.common.internal.service.jaxb.Configurations;
import org.glassfish.openesb.tools.common.internal.service.jaxb.DisplayName;
import org.glassfish.openesb.tools.common.internal.service.jaxb.I18NBundle;
import org.glassfish.openesb.tools.common.internal.service.jaxb.Icons;
import org.glassfish.openesb.tools.common.internal.service.jaxb.LongDescription;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ObjectFactory;
import org.glassfish.openesb.tools.common.internal.service.jaxb.PropertyRef;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ServiceDescriptor;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ShortDescription;
import org.glassfish.openesb.tools.common.internal.service.jaxb.Templates;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ToolsSpiProviders;
import org.glassfish.openesb.tools.common.internal.service.jaxb.WebResources;
import org.glassfish.openesb.tools.common.internal.service.jaxb.Icons.Icon;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ToolsSpiProviders.SpiProvider;
import org.glassfish.openesb.tools.common.internal.service.jaxb.WebResources.UriMap;
import org.glassfish.openesb.tools.common.service.DefaultServiceBuilder;
import org.glassfish.openesb.tools.common.service.DefaultServiceGenerator;
import org.glassfish.openesb.tools.common.service.DefaultServicePackager;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;
import org.xml.sax.SAXException;

/**
 *
 * @author chikkala
 */
public class ServiceDescriptorImpl implements org.glassfish.openesb.tools.common.service.ServiceDescriptor {

    private static final Logger LOG = Logger.getLogger(ServiceDescriptorImpl.class.getName());
    private static final String SD_SCHEMA_RESOURCE_PATH = "/org/glassfish/openesb/tools/common/service/service-descriptor.xsd";
    private static final String DEF_SD_PATH = "/org/glassfish/openesb/tools/common/service/default-service-descriptor.xml";
    //
    //
    //
    private static JAXBContext sJAXBCtx;
    private static Unmarshaller sUnmarshaller;
    private static org.glassfish.openesb.tools.common.service.ServiceDescriptor sDefSD;
    //
    //
    //
    private ServiceDescriptor mJaxbSD = null;
    private ClassLoader mSDClassLoader = null;
    private Locale mLocale = null;
    private ResourceBundle mI18Bundle = null;
    private ResourceBundle mDefI18Bundle = null;
    private FeatureInfo mInfo = null;
    private Map<String, PropertyGroup.Info> mPropGroupInofMap = null;
    private Map<String, PropertyInfoImpl> mPropInfoImplMap = null;
    private Map<String, Configuration> mConfigMap = null;
    private Configuration mDefConfig = null;
    private ServiceGenerator mSG;
    private ServiceBuilder mSB;
    private ServicePackager mSP;

    private ServiceDescriptorImpl(ServiceDescriptor jaxbSD, ClassLoader sdCL, Locale sdLocale) {
        // LOG.setLevel(Level.FINE);
        this.mJaxbSD = jaxbSD;
        this.mSDClassLoader = sdCL;
        if (this.mSDClassLoader == null) {
            LOG.info("No Service descriptor specific class loader specified. Using the default class loader");
            this.mSDClassLoader = this.getClass().getClassLoader();
        }
        this.mLocale = sdLocale;
        if (this.mLocale == null) {
            this.mLocale = Locale.getDefault();
        }
        initConfigurations();
    }

    private static synchronized void initJAXB() throws JAXBException {
        if (sJAXBCtx != null && sUnmarshaller != null) {
            // already initialized.
            LOG.fine("JAXB Context is already initialized");
            return;
        }
        if (sJAXBCtx == null) {
            LOG.fine("Initializing JAXB Context...");
            String jaxbPackage = ObjectFactory.class.getPackage().getName();
            ClassLoader jaxbClassLoader = ObjectFactory.class.getClassLoader();
            sJAXBCtx = JAXBContext.newInstance(jaxbPackage, jaxbClassLoader);
        }
        sUnmarshaller = sJAXBCtx.createUnmarshaller();
        URL schemaURL = ServiceDescriptorImpl.class.getResource(SD_SCHEMA_RESOURCE_PATH);
        if (schemaURL != null) {
            try {
                sUnmarshaller.setSchema(SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(schemaURL));
            } catch (SAXException ex) {
                LOG.log(Level.INFO, ex.getMessage(), ex);
            }
        } else {
            LOG.info("Schema URL not found for service descriptor " + schemaURL);
        }
    }

    public static ServiceDescriptor unmarshallServiceDescriptor(URL sdURL) throws IOException, JAXBException {
        initJAXB();
        Object umObj = sUnmarshaller.unmarshal(sdURL);
        ServiceDescriptor sd = null;
        if (umObj instanceof ServiceDescriptor) {
            sd = (ServiceDescriptor) umObj;
        }
        return sd;
    }

    public static org.glassfish.openesb.tools.common.service.ServiceDescriptor createServiceDescriptor(URL sdURL, ClassLoader sdCL, Locale sdLocale) throws Exception {
        ServiceDescriptor jaxbSD = unmarshallServiceDescriptor(sdURL);
        return new ServiceDescriptorImpl(jaxbSD, sdCL, sdLocale);
    }

    public static org.glassfish.openesb.tools.common.service.ServiceDescriptor createServiceDescriptor(URL sdURL, ClassLoader sdCL) throws Exception {
        return createServiceDescriptor(sdURL, sdCL, null);
    }

    public static org.glassfish.openesb.tools.common.service.ServiceDescriptor createServiceDescriptor(URL sdURL) throws Exception {
        return createServiceDescriptor(sdURL, null, null);
    }

    public synchronized static org.glassfish.openesb.tools.common.service.ServiceDescriptor getDefaultServiceDescriptor() throws Exception {
        if (sDefSD == null) {
            URL sdURL = ServiceDescriptorImpl.class.getResource(DEF_SD_PATH);
            sDefSD = createServiceDescriptor(sdURL, ServiceDescriptorImpl.class.getClassLoader(), null);
        }
        return sDefSD;
    }

    public ClassLoader getClassLoader() {
        return this.mSDClassLoader;
    }

    public Locale getLocale() {
        return this.mLocale;
    }
    public ResourceBundle getDefaultResourceBundle() {
        if (this.mDefI18Bundle == null) {
            String baseName = this.getClass().getPackage().getName() + ".DefaultBundle";
            try {
                this.mDefI18Bundle = ResourceBundle.getBundle(
                        baseName, this.mLocale, this.mSDClassLoader);
            } catch (MissingResourceException resEx) {
                String msg = "Default Service descriptor resource bundle not found baseName:" +
                        baseName + " Locale:" + this.mLocale;
                LOG.fine(msg);
                LOG.log(Level.FINEST, msg, resEx);
                // Try with locale independent defaultBundle
                try {
                    this.mDefI18Bundle = ResourceBundle.getBundle(baseName, new Locale(""), this.mSDClassLoader);
                } 
                catch (Exception anyEx) {
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.log(Level.FINEST, "No locale independent default bundle found.", anyEx);
                    }
                }
            } 
            catch (NullPointerException ex) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, ex.getMessage(), ex);
                }
            }
        }
        
        return this.mDefI18Bundle;
    }
    
    public ResourceBundle getResourceBundle() {
        if (this.mI18Bundle == null) {
            String baseName = null;
            I18NBundle i18n = this.mJaxbSD.getI18NBundle();
            if (i18n != null) {
                baseName = i18n.getBaseName();
                try {
                    this.mI18Bundle = ResourceBundle.getBundle(
                            baseName, this.mLocale, this.mSDClassLoader);
                } catch (MissingResourceException resEx) {
                    String msg = "Service descriptor resource bundle not found baseName:" +
                            baseName + " Locale:" + this.mLocale;
                    LOG.fine(msg);
                    LOG.log(Level.FINEST, msg, resEx);
                    // Try with locale independent defaultBundle
                    try {
                        this.mI18Bundle = ResourceBundle.getBundle(baseName, new Locale(""), this.mSDClassLoader);
                    } catch (Exception anyEx) {
                        LOG.log(Level.FINEST, "No locale independent bundle found.", anyEx);
                    }
                } catch (NullPointerException ex) {
                    LOG.log(Level.FINE, ex.getMessage(), ex);
                }

            } else {
                LOG.fine("No resource bundle defined for Service descriptor : " + this.getServiceType());
            }
        }
        return this.mI18Bundle;
    }

    public String getServiceType() {
        return this.mJaxbSD.getServiceType();
    }

    public String getComponentName() {
        return this.mJaxbSD.getComponentName();
    }

    public boolean isBindingComponent() {
        return this.mJaxbSD.isBindingComponent();
    }

    public String getDisplayName() {
        return this.getInfo().getDisplayName();
    }

    public String getShortDescription() {
        return this.getInfo().getShortDescription();
    }

    public String getLongDescription() {
        return this.getInfo().getLongDescription();
    }

    public HelpContext getHelpContext() {
        return new HelpContext(this.getInfo().getHelpID(), this.getInfo().getHelpURL());
    }

    private URL getIcon(String base, IconType type) {
        URL iconURL = null;
        if (base == null) {
            return null;
        }
        // not found in the specified icons. So, search with base icon.
        String iconPath = base + type.getType();
        // search for .png, .jpg and .gif in that order.
        iconURL = getResource(iconPath + ".png");
        if (iconURL == null) {
            iconURL = getResource(iconPath + ".jpg");
        }
        if (iconURL == null) {
            iconURL = getResource(iconPath + ".gif");
        }
        return iconURL;
    }

    public URL getIcon(IconType type) {
        URL iconURL = null;
        Icons icons = this.mJaxbSD.getIcons();
        if (icons == null) {
            return iconURL;
        }

        for (Icon icon : icons.getIcon()) {
            String iconPath = icon.getUrl();
            String typeValue = icon.getType();
            if (typeValue == null) {
                typeValue = "";
            }
            if (type.getType().equals(typeValue)) {
                return getResource(iconPath);
            }
        }
        iconURL = getIcon(icons.getBase(), type);
        return iconURL;
    }

    public List<URL> getIcons() {
        List<URL> iconURLs = new ArrayList<URL>();
        Icons icons = this.mJaxbSD.getIcons();
        if (icons == null) {
            return iconURLs;
        }
        String base = icons.getBase();
        for (Icon icon : icons.getIcon()) {
            String iconPath = icon.getUrl();
            URL iconURL = getResource(iconPath);
            if (iconURL != null) {
                iconURLs.add(iconURL);
            }
        }
        //TODO: add icons with base name + all types also in the list;
        return iconURLs;
    }

    public List<Configuration> getConfigurations() {
        List<Configuration> configList = new ArrayList<Configuration>();
        configList.addAll(this.mConfigMap.values());
        return configList;
    }

    public Configuration getDefaultConfiguration() {
        return this.mDefConfig;
    }

    public List<Template> getTemplates() {
        List<Template> templateList = new ArrayList<Template>();
        Templates templates = this.mJaxbSD.getTemplates();
        if (templates == null) {
            return templateList;
        }

        for (org.glassfish.openesb.tools.common.internal.service.jaxb.Templates.Template template : templates.getTemplate()) {
            String name = template.getAlias();
            String path = template.getPath();
            boolean filtered = template.isFiltered();
            boolean overwrite = template.isOverwrite();
            URL tURL = getResource(path);
            if (tURL != null) {
                templateList.add(new TemplateImpl(name, tURL, filtered, overwrite));
            }
        }
        return templateList;
    }

    public Map<String, URL> getWebResources() {
        Map<String, URL> webResMap = new HashMap<String, URL>();
        WebResources webResources = this.mJaxbSD.getWebResources();
        if (webResources == null) {
            return webResMap;
        }

        for (UriMap uriMap : webResources.getUriMap()) {
            String alias = uriMap.getAlias();
            String path = uriMap.getPath();
            URL tURL = getResource(path);
            if (tURL != null) {
                webResMap.put(alias, tURL);
            }
        }

        return webResMap;
    }

    protected String clazzToString(Class clazz) {
        return clazz.getName() + "@" + clazz.getClassLoader() + ":" + clazz.getProtectionDomain().getCodeSource().getLocation(); // NOI18N
    }

    protected <T> T instantiateClass(Class<T> interfaceClass, String implClass, ClassLoader loader) {
        T provider = null;
        try {
            Class instClass = Class.forName(implClass, false, loader);
            if (!interfaceClass.isAssignableFrom(instClass)) {
                LOG.info(clazzToString(instClass) + " not a subclass of " + clazzToString(interfaceClass));
            } else {
                // instantiate the class
                // check. if it has constructor with the service descriptor as parameter. if yes,
                // call that constructor. else use default constructor.
                try {
                    Class[] params = {org.glassfish.openesb.tools.common.service.ServiceDescriptor.class};
                    org.glassfish.openesb.tools.common.service.ServiceDescriptor sd = this;
                    Constructor constructor = instClass.getConstructor(params);
                    provider = (T) constructor.newInstance(sd);
                } catch (NoSuchMethodException nsmEx) {
                    LOG.warning(implClass + " SPI Provider does not have the constructor with ServiceDescriptor as formal parameter type");
                    LOG.log(Level.FINE, nsmEx.getMessage(), nsmEx);
                    // try for default constructor with no prarams.
                    provider = (T) instClass.newInstance();
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }

        return provider;
    }

    public <T> T getSPIProvider(Class<T> spi) {
        String implClass = null;
        ToolsSpiProviders providers = this.mJaxbSD.getToolsSpiProviders();
        if (providers == null) {
            return null;
        }
        for (SpiProvider spiProvider : providers.getSpiProvider()) {
            if (spi.getName().equals(spiProvider.getInterface())) {
                implClass = spiProvider.getImpl();
                break;
            }
        }
        if (implClass != null) {
            return instantiateClass(spi, implClass, this.mSDClassLoader);
        } else {
            return null;
        }
    }

    public ServiceGenerator getServiceGenerator() {
        if (this.mSG == null) {
            // find if it has specific imple
            this.mSG = getSPIProvider(ServiceGenerator.class);
            if (this.mSG == null) {
                // fallback to default impl.
                this.mSG = new DefaultServiceGenerator(this);
            }
        }
        return this.mSG;
    }

    public ServiceBuilder getServiceBuilder() {

        if (this.mSB == null) {
            // find if it has specific imple
            this.mSB = getSPIProvider(ServiceBuilder.class);
            if (this.mSB == null) {
                // fallback to default impl.
                this.mSB = new DefaultServiceBuilder(this);
            }
        }
        return this.mSB;
    }

    public ServicePackager getServicePackager() {
        if (this.mSP == null) {
            // find if it has specific imple
            this.mSP = getSPIProvider(ServicePackager.class);
            if (this.mSP == null) {
                // fallback to default impl.
                this.mSP = new DefaultServicePackager(this);
            }
        }
        return this.mSP;
    }

    private URL getResource(String path) {
        URL resURL = null;
        String resName = path;
        if (path.startsWith("/")) {
            if (path.length() > 1) {
                resName = path.substring(1);
            } else {
                resName = "";
            }
        }
        resURL = this.getClassLoader().getResource(resName);
        if (resURL == null) {
            LOG.info("URL not found for tempalte path " + path + " in service descriptor for " + this.getServiceType());
        }
        return resURL;
    }

    private FeatureInfo getInfo() {
        if (this.mInfo == null) {
            DisplayName dispName = this.mJaxbSD.getDisplayName();
            ShortDescription shortDesc = this.mJaxbSD.getShortDescription();
            LongDescription longDesc = this.mJaxbSD.getLongDescription();
            if (dispName == null && shortDesc == null && longDesc == null ) {
                this.mInfo = FeatureInfoImpl.createFeatureInfoImpl(
                        this.getDefaultResourceBundle(),
                        this.mSDClassLoader,
                        "DN.default.sd",
                        "SD.default.sd",
                        "LD.default.sd");
            } else {
                this.mInfo = new FeatureInfoImpl(this.getResourceBundle(),
                    this.mSDClassLoader, dispName, shortDesc, longDesc);
            }
        }
        return this.mInfo;
    }

    private List<PropertyGroup> createPropertyGroupList(List<Property> propList) {

        // sort properties into groups.
        Map<String, List<Property>> propListMap = new HashMap<String, List<Property>>();
        for (Property prop : propList) {
            String group = prop.getInfo().getGroup();
            if (group == null || group.trim().length() == 0) {
                group = ""; // default group
            }
            List<Property> props = propListMap.get(group);
            if (props == null) {
                props = new ArrayList<Property>();
                propListMap.put(group, props);
            }
            props.add(prop);
        }
        List<PropertyGroup> groupList = new ArrayList<PropertyGroup>();
        for (String groupName : propListMap.keySet()) {
            PropertyGroup.Info info = this.mPropGroupInofMap.get(groupName);
            List<Property> props = propListMap.get(groupName);
            PropertyGroupImpl impl = new PropertyGroupImpl(info, props);
            groupList.add(impl);
        }
        return groupList;
    }

    private Configuration createDefaultConfiguration() {
        String name = "default";
        //TODO I18N 
        FeatureInfo info =  FeatureInfoImpl.createFeatureInfoImpl(
                        this.getDefaultResourceBundle(),
                        this.mSDClassLoader,
                        "DN.default.config",
                        "SD.default.config",
                        "LD.default.config");
        List<Property> propList = new ArrayList<Property>();
        for (PropertyInfo propInfo : this.mPropInfoImplMap.values()) {
            Property prop = new PropertyImpl(propInfo);
            propList.add(prop);
        }
        return new ConfigurationImpl(info, null, name, createPropertyGroupList(propList));
    }

    private Configuration createConfiguration(ConfigurationOption jaxbConfig) {
        String configName = jaxbConfig.getName();
        UsageHint usageHint = UsageHint.fromXsdEnum(jaxbConfig.getUsageHint());
        FeatureInfoImpl info = new FeatureInfoImpl(
                this.getResourceBundle(),
                this.getClassLoader(),
                jaxbConfig.getDisplayName(),
                jaxbConfig.getShortDescription(),
                jaxbConfig.getLongDescription());

        Map<String, Property> propMap = new HashMap<String, Property>();

        // add the reference properties
        for (PropertyRef propRef : jaxbConfig.getPropertyRef()) {
            String propName = propRef.getName();
            PropertyInfoImpl propInfoImpl = this.mPropInfoImplMap.get(propName);
            if (propInfoImpl == null) {
                LOG.info("No property found for property reference in configuration: " + configName + " prop: " + propName);
            } else {
                // LOG.info("Adding the property " + configName + " to config " + propName);
                PropertyInfoImpl refPropInfoImpl = new PropertyInfoImpl(
                                    this.getResourceBundle(),
                                    this.getClassLoader(),
                                    propInfoImpl.getJaxbProperty(),
                                    propRef);
                Property prop = new PropertyImpl(refPropInfoImpl);
                propMap.put(propName, prop);
            }
        }

        // add local properties which may override the referenced property
        for (org.glassfish.openesb.tools.common.internal.service.jaxb.Property jaxbProp : jaxbConfig.getProperty()) {

            PropertyInfoImpl propInfo = new PropertyInfoImpl(
                    this.getResourceBundle(),
                    this.getClassLoader(),
                    jaxbProp, null);
            Property prop = new PropertyImpl(propInfo);
            propMap.put(prop.getName(), prop);
        }

        List<Property> propList = new ArrayList<Property>();
        propList.addAll(propMap.values());
        return new ConfigurationImpl(info, usageHint, configName, createPropertyGroupList(propList));
    }

    private void initConfigurations() {
        // initialize the group info map.
        this.mPropGroupInofMap = new HashMap<String, PropertyGroup.Info>();
        String defaultGroupName = "";
        PropertyGroupImpl.InfoImpl defaultGroupInfo =
                PropertyGroupImpl.InfoImpl.createInfoImpl(
                this.getDefaultResourceBundle(),
                this.mSDClassLoader,
                "DN.default.prop.group",
                "SD.default.prop.group",
                "LD.default.prop.group",
                defaultGroupName);
        this.mPropGroupInofMap.put(defaultGroupName, defaultGroupInfo); // add default group.

        this.mPropInfoImplMap = new HashMap<String, PropertyInfoImpl>();
        this.mConfigMap = new HashMap<String, Configuration>();

        Configurations configs = this.mJaxbSD.getConfigurations();
        if (configs == null) {
            this.mDefConfig = createDefaultConfiguration();
            return;
        }
        // collect property groups
        List<org.glassfish.openesb.tools.common.internal.service.jaxb.PropertyGroup> jaxbPropGroups = configs.getPropertyGroup();
        for ( org.glassfish.openesb.tools.common.internal.service.jaxb.PropertyGroup jaxbPropGroup : jaxbPropGroups) {
            PropertyGroupImpl.InfoImpl infoImpl = new PropertyGroupImpl.InfoImpl(
                    this.getResourceBundle(),
                    this.getClassLoader(),
                    jaxbPropGroup
                    );
            this.mPropGroupInofMap.put(infoImpl.getName(), infoImpl);
        }

        // collect properties
        List<org.glassfish.openesb.tools.common.internal.service.jaxb.Property> jaxbProps = configs.getProperty();
        for (org.glassfish.openesb.tools.common.internal.service.jaxb.Property jaxbProp : jaxbProps) {

            PropertyInfoImpl propInfo = new PropertyInfoImpl(
                    this.getResourceBundle(),
                    this.getClassLoader(),
                    jaxbProp, null);
            this.mPropInfoImplMap.put(propInfo.getName(), propInfo);
        }
        // collect configurations
        for (ConfigurationOption jaxbConfig : configs.getConfigurationOption()) {
            Configuration config = createConfiguration(jaxbConfig);
            this.mConfigMap.put(config.getName(), config);
        }
        // find the default configureation
        String defOption = configs.getDefaultOption();
        if (defOption != null) {
            this.mDefConfig = this.mConfigMap.get(defOption);
            if (this.mDefConfig == null) {
                LOG.info("Default configuration " + defOption + " not found.");
                this.mDefConfig = createDefaultConfiguration();
            }
        }
        if (this.mDefConfig == null) {
            this.mDefConfig = createDefaultConfiguration();
        }

    }
}
