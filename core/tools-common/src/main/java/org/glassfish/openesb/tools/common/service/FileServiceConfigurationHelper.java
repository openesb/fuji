/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.tools.common.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipInputStream;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;

/**
 * For now, for backward compatibiltiy, this class currently has the
 * refactored file service prototype code for detrmining the direction and
 * other configuration.
 *
 * //TODO:
 * This really should be upgraded to make it more cleaner way of processing
 * the service configuration.
 *
 * @author Jim Fu
 * @author chikkala
 */
public class FileServiceConfigurationHelper {

    private static final Logger LOG = Logger.getLogger(FileServiceConfigurationHelper.class.getName());
    private static final String EXCHANGE_PATTERN_IN = "in";
    private static final String EXCHANGE_PATTERN_OUT = "out";
    private static final String EXCHANGE_PATTERN_ONDEMAND = "ondemand";
    private static final String EXCHANGE_PATTERN_INOUT = "inout";
    private static final String EXCHANGE_PATTERN_OUTIN = "outin";
    private static final String TEMP_CONFIG = "service.properties.tmp";
    private static final String TEMP_CONFIG_IN = "service.in.properties.tmp";
    private static final String TEMP_CONFIG_OUT = "service.out.properties.tmp";

    /**
     *
     * parent of the generated file.
     * @param dir
     * @return true if temp files exists.
     */
    public boolean isTempConfigExists(File dir) {
        // for generate service unit request from webui
        // we need to merge any changes made - so don't skip
        File staged1WyCfg = new File(dir, "service.properties.tmp");
        File staged2WyIBCfg = new File(dir, "service.in.properties.tmp");
        File staged2WyOBCfg = new File(dir, "service.out.properties.tmp");
        return (staged1WyCfg.exists() || staged2WyIBCfg.exists() || staged2WyOBCfg.exists());
    }

    public boolean generateServiceConfigFile(String serviceType, String serviceName, Properties userSettings, IFLModel ifl,
            File file, String name, ZipInputStream zipInput, long size) throws IOException {
        boolean generated = false;
        // for file service prototyping only
        // also added ftp service
        if ((serviceType.equals("file") || serviceType.equals("ftp")) && name.endsWith("service.properties")) {

            String direction = getServiceMode(ifl, serviceName);
            // apply velocity template for service.properties
            if (direction != null) {
                Properties p = new Properties();
                p.put("direction", direction);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Utils.copy(zipInput, bos, size);
                ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
                replaceTokensInStream(bis, file, p);
                generated = true;
                // now we have a service.properties containing
                // a set of default settings for a particular service mode
                // in case, this is invoked from webui, then
                // we need to merge the service.proeprties with
                // web user specified settings:
                mergeSettings(file, userSettings);
            }
        }
        return generated;

    }

    public boolean generateServiceConfigFile(String serviceType, String serviceName, IFLModel ifl,
            File file, String name, ZipInputStream zipInput, long size) throws IOException {
        boolean generated = false;
        // for file service prototyping only
        // also added ftp service
        if ((serviceType.equals("file") || serviceType.equals("ftp")) && name.endsWith("service.properties")) {

            String direction = getServiceMode(ifl, serviceName);
            // apply velocity template for service.properties
            if (direction != null) {
                Properties p = new Properties();
                p.put("direction", direction);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Utils.copy(zipInput, bos, size);
                ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
                replaceTokensInStream(bis, file, p);
                generated = true;
                // now we have a service.properties containing
                // a set of default settings for a particular service mode
                // in case, this is invoked from webui, then
                // we need to merge the service.proeprties with
                // web user specified settings: which is staged in the same
                // folder but with name:
                // service.properties.tmp - for 1 way
                // service.in.properties.tmp & service.out.properties.tmp - for 2 way
                File staged1WyCfg = new File(file.getParentFile(), "service.properties.tmp");
                File staged2WyIBCfg = new File(file.getParentFile(), "service.in.properties.tmp");
                File staged2WyOBCfg = new File(file.getParentFile(), "service.out.properties.tmp");
                // may also check that the direction derived from flow
                // is the same as from the user specified through webui
                try {
                    if (direction.equals(EXCHANGE_PATTERN_IN)) {
                        if (staged1WyCfg.exists()) {
                            // merge
                            mergeSettings(file, staged1WyCfg);
                        }
                    } else if (direction.equals(EXCHANGE_PATTERN_OUT)) {
                        if (staged1WyCfg.exists()) {
                            // merge
                            mergeSettings(file, staged1WyCfg);
                        }
                    } else if (direction.equals(EXCHANGE_PATTERN_ONDEMAND)) {
                        if (staged1WyCfg.exists()) {
                            // merge
                            mergeSettings(file, staged1WyCfg);
                        }
                    } else if (direction.equals(EXCHANGE_PATTERN_INOUT)) {
                        if (staged2WyIBCfg.exists()) {
                            // merge
                            mergeSettings(file, staged2WyIBCfg);
                        }
                        if (staged2WyOBCfg.exists()) {
                            // merge
                            mergeSettings(file, staged2WyOBCfg);
                        }
                    } else if (direction.equals(EXCHANGE_PATTERN_OUTIN)) {
                        if (staged2WyIBCfg.exists()) {
                            // merge
                            mergeSettings(file, staged2WyIBCfg);
                        }
                        if (staged2WyOBCfg.exists()) {
                            // merge
                            mergeSettings(file, staged2WyOBCfg);
                        }
                    }
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        }
        return generated;

    }

    /**
     * helper to figure out message exchange pattern
     * by examing the ifl model.
     * @return a string representing message direction
     */
    public static String getServiceMode(IFLModel ifl, String serviceName) {
        Map<String, Entry<String, List<String>>> routes = ifl.getRoutes();
        String direction = null;

        if (ifl.isConsumed(serviceName)) {
            // according to the way the ifl model built
            // it must also be in provided service list
            Entry<String, List<String>> route = getRouteOfFromService(routes, serviceName);
            if (route != null) {
                // if it is also the last of a route, it is IN-OUT
                // make sure the route is a "route", not "broadcast", "tee", "select", etc.
                if (isRouteRoute(route, serviceName) && isLastInRoute(route, serviceName)) {
                    // if the service is in a "route" route and
                    // is the last - inout
                    direction = EXCHANGE_PATTERN_INOUT;
                } else {
                    // oneway in
                    direction = EXCHANGE_PATTERN_IN;
                }
            } else {
                throw new java.lang.IllegalStateException("consumed service is not a from service...:" + serviceName);
            }
        } else {
            if (ifl.isProvided(serviceName)) {
                // a service as destination
                // if it is in a route and it is not the last
                // then it is an on-demand
                Entry<String, List<String>> route = getRouteOfToService(routes, serviceName);
                if (isRouteRoute(route, serviceName) && !isLastInRoute(route, serviceName)) {
                    // if the service is in a "route" route and
                    // is not the last, then we expect it to fetch something
                    // to service but exprects a output
                    direction = EXCHANGE_PATTERN_ONDEMAND;
                } else {
                    // oneway out
                    direction = EXCHANGE_PATTERN_OUT;
                }
            } else {
                // service declared does not appear in any EIP,
                // does not generate service.properties for the service instance,
                // if later, the service name participates in the EIPs,
                // its direction will be calculated and service.properties
                // generated then.
            }
        }
        return direction;
    }

    public static boolean isRouteRoute(Entry<String, List<String>> route, String serviceName) {
        boolean result = false;
        if ( route != null && route.getValue() != null && route.getValue().size() > 0) {
            result = route.getValue().contains(serviceName);
        }
        return result;
    }
    
    public static boolean isLastInRoute(Entry<String, List<String>> route, String serviceName) {
        boolean result = false;
        if (route != null && route.getValue() != null && route.getValue().size() > 0) {
            int index = route.getValue().lastIndexOf(serviceName);
            result = (index == route.getValue().size() - 1);
        }
        return result;
    }

    public static Entry<String, List<String>> getRouteOfFromService(Map<String, Entry<String, List<String>>> routes, String serviceName) {
        Iterator it = routes.keySet().iterator();
        Entry<String, List<String>> route = null;
        while (it.hasNext()) {
            route = routes.get(it.next().toString());
            if (route.getKey() != null && route.getKey().equals(serviceName)) {
                break;
            }
            route = null;
        }
        return route;
    }

    public static Entry<String, List<String>> getRouteOfToService(Map<String, Entry<String, List<String>>> routes, String serviceName) {
        Iterator it = routes.keySet().iterator();
        Entry<String, List<String>> route = null;
        while (it.hasNext()) {
            route = routes.get(it.next().toString());
            if (route.getValue() != null) {
                if (route.getValue().contains(serviceName)) {
                    break;
                }
            }
            route = null;
        }
        return route;
    }

    private void mergeSettings(File file, Properties userSettings) throws IOException {
        Properties serviceSettings = Utils.readProperties(file);
        Iterator it = serviceSettings.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            // user setting over write default settings
            String udParam = userSettings.getProperty(key);
            if (udParam != null) {
                // do not overwrite direction for now
                if (!key.equals("direction")) {
                    serviceSettings.setProperty(key, udParam);
                }
            }
        }
        // save back
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            serviceSettings.store(fos, "merge occurred from user settings...");
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }

    private void mergeSettings(File file, File stagedCfg) throws Exception {
        FileInputStream fis = null;
        FileInputStream fis2 = null;
        Properties serviceSettings = new Properties();
        Properties userSettings = new Properties();
        Exception ex = null;

        try {
            fis = new FileInputStream(file);
            fis2 = new FileInputStream(stagedCfg);
            serviceSettings.load(fis);
            userSettings.load(fis2);
            Iterator it = serviceSettings.keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                // user setting over write default settings
                String udParam = userSettings.getProperty(key);
                if (udParam != null) {
                    // do not overwrite direction for now
                    if (!key.equals("direction")) {
                        serviceSettings.setProperty(key, udParam);
                    }
                }
            }
        } catch (Exception e) {
            ex = e;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                    // ignore
                }
            }
            if (fis2 != null) {
                try {
                    fis2.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
        if (ex != null) {
            throw ex;
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            serviceSettings.store(fos, "merge occurred from user settings...");
        } catch (Exception e) {
            ex = e;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
        if (ex != null) {
            throw ex;
        }
    }

    public static void replaceTokensInStream(
            final InputStream input,
            final File file,
            final Properties props) throws IOException {
        try {
            StringWriter out = new StringWriter();
            TemplateEngine.getDefault().processTemplate(input, out, props);
            ByteArrayInputStream in = new ByteArrayInputStream(out.getBuffer().toString().getBytes());
            Utils.copy(in, file);
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            throw new IOException(ex.getMessage());
        }
    }
}
