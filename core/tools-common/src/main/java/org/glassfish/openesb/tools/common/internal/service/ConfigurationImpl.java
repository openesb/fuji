/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common.internal.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Configuration.UsageHint;
import org.glassfish.openesb.tools.common.descriptor.FeatureInfo;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;


/**
 *
 * @author chikkala
 */
public class ConfigurationImpl implements Configuration {
    private FeatureInfo mInfo;
    private UsageHint mUsageHint;
    private String mName;
    private Map<String, Property> mPropMap;
    private Map<String, PropertyGroup> mPropGroupMap;

    public ConfigurationImpl(FeatureInfo info, UsageHint usageHint, String name, List<PropertyGroup> propGroupList) {
       
        this.mInfo = info;
        this.mUsageHint = usageHint;
        this.mName = name;
        this.mPropMap = new HashMap<String, Property>();
        this.mPropGroupMap = new HashMap<String, PropertyGroup>();
        for ( PropertyGroup group : propGroupList ) {
            this.mPropGroupMap.put(group.getName(), group);
            for ( Property prop : group.getPropertyList() ) {
                this.mPropMap.put(prop.getName(), prop);
            }
        }
    }

    public FeatureInfo getInfo() {
        return this.mInfo;
    }

    public UsageHint getUsageHint() {
        return this.mUsageHint;
    }

    public String getName() {
        return this.mName;
    }

    public Property getProperty(String name) {
        return this.mPropMap.get(name);
    }

    public List<Property> getPropertyList() {
        ArrayList<Property> propList = new ArrayList<Property>();
        propList.addAll(this.mPropMap.values());
        return propList;
    }

    public Properties getProperties() {
        Properties props = new Properties();
        for ( Property prop : this.mPropMap.values()) {
            props.setProperty(prop.getName(), prop.getValue());
        }
        return props;
    }

    public List<PropertyGroup> getPropertyGroupList() {
         ArrayList<PropertyGroup> propGroupList = new ArrayList<PropertyGroup>();
        propGroupList.addAll(this.mPropGroupMap.values());
        return propGroupList;
    }

}
