/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import java.io.File;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.FujiProjectModel;

/**
 * This class implements the project model for a service type. It provides a
 * default filesystem layout for a service type.
 *
 * @see FujiProjectModel
 *
 * @author chikkala
 */
public class ServiceProjectModel extends FujiProjectModel {

    protected ServiceProjectModel(String serviceType, ApplicationProjectModel appPrjModel) {
        super();
        initModel(serviceType, appPrjModel);
    }

    /**
     *
     * @return source direcotry for a service of a service type.
     */
    public File getSourceDir(String serviceName) {
        return new File(getSourceDir(), serviceName);
    }

    /**
     *
     * @return configuration directory for a service of a service type.
     */
    public File getConfigDir(String serviceName) {

        return new File(getConfigDir(), serviceName);
    }

    /**
     *
     * @return resource directory for a service of a service type.
     */
    public File getResourcesDir(String serviceName) {
        return new File(getResourcesDir(), serviceName);
    }

    protected void initProjectProperties(String serviceType, ApplicationProjectModel appPrjModel) {

        String applicationId = appPrjModel.getProperty(PROP_APPLICATION_ID, DEF_APPLICATION_ID);
        String artifactId = appPrjModel.getProperty(PROP_ARTIFACT_ID, applicationId);

        String name = applicationId + "-" + serviceType;
        String displayName = name;
        String description = name + " Service Unit";

        this.setProperty(PROP_PROJECT_TYPE, SERVICE_UNIT_PROJECT_TYPE);
        this.setProperty(PROP_SERVICE_TYPE, serviceType);
        
        this.setProperty(PROP_APPLICATION_ID, applicationId);
        this.setProperty(PROP_ARTIFACT_ID, artifactId);

        this.setProperty(PROP_NAME, name);
        this.setProperty(PROP_DISPLAY_NAME, displayName);
        this.setProperty(PROP_DESCRIPTION, description);
    }

    protected void initProjectLayout(String serviceType, ApplicationProjectModel appPrjModel) {
        File baseDir = appPrjModel.getBaseDir();
        this.setBaseDir(baseDir);
        this.setSourceDir(new File(baseDir, DEF_SRC_DIR_BASE + "/" + serviceType));
        this.setConfigDir(new File(baseDir, DEF_CONFIG_DIR + "/" + serviceType));
        this.setResourcesDir(new File(baseDir, DEF_RESOURCES_DIR + "/" + serviceType));

        this.setBuildDir(new File(baseDir, DEF_BUILD_DIR));
        this.setOutputDir(new File(baseDir, DEF_OUTPUT_DIR + "/" + serviceType));
        //TODO: change the build and output directory to be
        //  buildDir = target,  outDir = ${build.dir}/classes/service-units/<servicetype>
        // for now use the old structure as target, target/classes/<servicetype>
    }

    protected void initModel(String serviceType, ApplicationProjectModel appPrjModel) {
        initProjectProperties(serviceType, appPrjModel);
        initProjectLayout(serviceType, appPrjModel);
    }

    /**
     * special case project model for the java service type
     */
    public static class JavaServiceProjectModel extends ServiceProjectModel {

        public JavaServiceProjectModel(String serviceType, ApplicationProjectModel appPrjModel) {
            super(serviceType, appPrjModel);
        }

        @Override
        protected void initProjectLayout(String serviceType, ApplicationProjectModel appPrjModel) {
            super.initProjectLayout(serviceType, appPrjModel);
            File baseDir = appPrjModel.getBaseDir();
            this.setSourceDir(new File(baseDir, DEF_SRC_DIR));
            // this.setOutputDir(new File(baseDir, DEF_OUTPUT_DIR));
        }
    }

    //TODO: provide a way to override the standard filesytem layout. For example, the
    // project fs layout can be defined in the service descriptor  xml.
    /**
     * creates an instance of the service project model.
     *
     * @param serviceType  service type for which this project model will be created.
     * @param appPrjModel application project which is the parent project for this service type project
     * @return
     */
    public static ServiceProjectModel newInstance(String serviceType, ApplicationProjectModel appPrjModel) {
        ServiceProjectModel prjModel = null;
        if ( "java".equals(serviceType)) {
            prjModel = new JavaServiceProjectModel(serviceType, appPrjModel);
        } else {
            prjModel = new ServiceProjectModel(serviceType, appPrjModel);
        }
        return prjModel;
    }
}
