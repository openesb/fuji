/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service.spi;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.IOException;
import java.util.List;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * A provider of given service type can implement this interface to perform
 * processing of the service artifacts during the build phase of the application.
 *
 * This is an optional interface. Service type providers should only
 * implement this interface if they have specific requirements which are not met
 * by the default service artifacts processing implementation in the application
 * plugin.
 *
 * @see ServiceProjectModel
 * @see IFLModel
 *
 * @author chikkala
 */
public interface ServiceBuilder {
    /**
     *
     * @param prjModel project view for this service type
     * @param services list of services of this service type used in the ifl.
     * @param ifl IFL Model in which the services are used.
     * @throws java.io.IOException
     */
    void processResources(
            ServiceProjectModel prjModel,
            List<String> services,
            IFLModel ifl) throws IOException;
    /**
     *
     * @param prjModel project view for this service type
     * @param services list of services of this service type used in the ifl.
     * @param ifl IFL Model in which the services are used.
     * @throws java.io.IOException
     */
    void compile(
            ServiceProjectModel prjModel,
            List<String> services,
            IFLModel ifl) throws IOException;
}
