/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */


package org.glassfish.openesb.tools.common.service.spi;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * A provider of given service type can implement this interface to  generate
 * the service artifacts during the application or service type code generation.
 *
 * This is an optional interface. Service type providers should only
 * implement this interface if they have specific requirements which are not met
 * by the default service artifacts generation implementation in the application
 * plugin.
 * 
 * @see ServiceProjectModel
 * @see IFLModel
 *
 * @author chikkala
 */
public interface ServiceGenerator {
    /**
     * generates the service unit artifacts including the service artifacts required
     * to build services of this service type used in the ifl.
     *
     * @param prjModel project view for this service type
     * @param ifl  IFL Model in which the services are used.
     * @throws java.io.IOException
     */
    void generateServiceUnit(ServiceProjectModel prjModel, IFLModel ifl ) throws IOException;
    /**
     * generates service artifacts required to build the service of this service type.
     * <p>
     * Service name and a set of service configuration properties will be provided by the
     * caller to generate the service artifacts. service configuration properties may contain
     * the default values retrieved from the service decriptor or the user entered values and other
     * properties like configuration mode, service group etc which can be used in generating appropriate service
     * artifacts. For example, depending on a specific configuration mode, the content of the wsdl
     * bindings can be regenerated.
     *
     * @param prjModel project model that provides the directory layout and other project related properties
     * @param ifl  IFL Model in which the services are used. could be an empty ifl model, but not null.
     * @param serviceName  service name for which the service artifacts will be generated
     * @param serviceConfig service configuration properties
     *
     * @throws java.io.IOException
     */
    void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) throws IOException;

    /**
     * Generates service artifacts required to build grouped services declared using group and call syntax for a specific service type.
     * Note1. This method can be invoked for a single service declaration (e.g.  java "hello" ) or a single service calling one or more services
     * (e.g. java "hello" calls "c1") or a grouped services declaration (e.g. java "mygroup" ["hello"] calls "p1", "p2" ). In all cases configurations
     * for the services declared in the group list and call list must exists.
     *
     * @param prjModel  project model that provides the directory layout and other project related properties
     * @param ifl ifl model in which the grouped services are declared.
     * @param groupName  group name of the services delcared in the IFL
     * @param groupedServicesConfigMap Map object with service name as key and the corresponding service configuration as a value.
     * <p>
     * Each service specified in the group list of the service group declaration (for example, java "groupName" [<b>"p1", "p2"</b>] calls "c1", "c2")
     * with "groupName" will have a corresponding configuration object in this map. If the service declaration does not have a group list, the groupName
     * itself becomes the service name and the service configuration for that service name should be present in this map.
     *
     * @param calledServicesConfigMap Map object with service name as key and the corresponding service configuration as a value.
     * <p>
     * Each service specified in the calls list of the group declaration (for example, java "pojogroup" ["p1", "p2"] calls <b>"c1", "c2"</b> )
     * with "groupName" will have a corresponding configuration object in this map. If the calls list is empty, a empty map should be used as this
     * parameter.
     *
     * @throws IOException
     */
    void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, Map<String, Properties> groupedServicesConfigMap, Map<String, Properties> calledServicesConfigMap) throws IOException;
    /**
     * delete unused service unit artifacts that are previously generated, but
     * are not used in the current IFL for this service type.
     *
     * @param prjModel project view for this service type
     * @param ifl  IFL Model in which the services are used.
     * @throws java.io.IOException
     */
    void deleteUnusedArtifacts(ServiceProjectModel prjModel, IFLModel ifl ) throws IOException;
    /**
     * delete all existing service artifacts corresponding to this service.
     * <p>
     * Service name and a set of service configuration properties will be provided by the
     * caller to generate the service artifacts. service configuration properties may contain
     * the default values retrieved from the service decriptor or the user entered values and other
     * properties like configuration mode etc which can be used in generating appropriate service
     * artifacts. For example, depending on a specific configuration mode, the content of the wsdl
     * bindings can be regenerated.
     *
     * @param prjModel project view for this service type
     * @param serviceName  service name for which the service artifacts will be deleted
     * @param serviceConfig service configuration properties
     *
     * @throws java.io.IOException
     */
    void deleteServiceArtifacts(ServiceProjectModel prjModel, String serviceName, Properties serviceConfig) throws IOException;


}
