/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit.ParserCallback;
import javax.swing.text.html.parser.ParserDelegator;
import org.glassfish.openesb.tools.common.EditableProperties;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;

/**
 *
 * @author chikkala
 */
public class ServiceConfigurationSerializer {

    private static final Logger LOG = Logger.getLogger(ServiceConfigurationSerializer.class.getName());
    private ServiceDescriptor mSD;

    public ServiceConfigurationSerializer(ServiceDescriptor sd) {
        this.mSD = sd;
        // LOG.setLevel(Level.FINE);
    }

    public ServiceDescriptor getServiceDescriptor() {
        return this.mSD;
    }

    public Configuration getConfiguration(String configMode) {
        Configuration config = null;
        if (ServiceConfigurationHelper.DEF_SERVICE_CONFIG_MODE.equals(configMode)) {
            config = this.mSD.getDefaultConfiguration();
        } else {
            List<Configuration> configList = this.mSD.getConfigurations();
            for (Configuration cfg : configList) {
                if (cfg.getName().equals(configMode)) {
                    config = cfg;
                    break;
                }
            }
        }
        return config;
    }

    public void save(File configFile, Properties configProps, String serviceName) throws IOException {
        Configuration config = null;
        String configMode = configProps.getProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE);
        if (configMode == null) {
            configMode = ServiceConfigurationHelper.DEF_SERVICE_CONFIG_MODE;
        }
        config = getConfiguration(configMode);
        save(configFile, config, configProps, serviceName);
    }

    public void save(File configFile, Configuration config, Properties configProps, String serviceName) throws IOException {
        EditableProperties saveProps = createFormattedProperties(config, configProps, getMainHeaderComments(serviceName));
        save(configFile, saveProps);
    }

    public void save(File configFile, EditableProperties configProps) throws IOException {
        if (configFile.exists()) {
            LOG.fine("Overwriting the existing Service Configuration file...");
        }
        if (!configFile.getParentFile().exists()) {
            configFile.getParentFile().mkdirs();
        }
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(configFile);
            configProps.store(out);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception ex) {
                    // ignore.
                    LOG.log(Level.FINEST, null, ex);
                }
            }
        }
    }

    /**
     * creates editable properties which can be updated at any time without losing the order or comments.
     * @param config configuration object from which default values and group and order is derived. can be null.
     * @param configProps configuration properties object which can override the default values from the
     * configuration metadata and also can contain addtional properties that can be saved without metadata. can be null.
     * @param headerComments header comments.
     * @return EditableProperties object which can be used to modify and save to file with comments and order preserved.
     */
    public EditableProperties createFormattedProperties(Configuration config, Properties configProps, String[] headerComments) {
        EditableProperties saveProps = new EditableProperties();
        String configMode = ServiceConfigurationHelper.DEF_SERVICE_CONFIG_MODE;
        if (config != null) {
            configMode = config.getName();
        } else if (configProps != null) {
            configMode = configProps.getProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, configMode);
        }
        saveProps.setProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, configMode);
        saveProps.setComment(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, headerComments, true);

        if (config != null) {
            addProperties(saveProps, config);
        }
        if (configProps != null) {
            // update the values or add new properties.
            TreeSet<Object> keySet = new TreeSet<Object>();
            keySet.addAll(configProps.keySet());
            for (Object key : keySet) {
                Object value = configProps.get(key);
                if (value == null) {
                    value = "";
                }
                saveProps.put((String) key, value.toString());
            }
        }
        // make sure that the config mode is not overwritten.
        saveProps.setProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, configMode);

        return saveProps;
    }

    private void addProperties(EditableProperties props, Configuration config) {
        String cName = config.getName();
        String cDispName = config.getInfo().getDisplayName();
        String cShortDesc = config.getInfo().getShortDescription();
        if (cDispName == null) {
            cDispName = cName;
        }
        if (cShortDesc == null) {
            cShortDesc = cName;
        }

        cDispName = html2Text(cDispName);
        cShortDesc = html2Text(cShortDesc);

        String configComments = "\nConfiguration: " + cDispName + "\n " + cShortDesc + "\n";

//        String[] configComments = createComments("\nConfiguration: {0} \n {1}",
//                config.getName(),
//                config.getInfo().getDisplayName(),
//                config.getInfo().getShortDescription(),
//                null);

        Comparator<PropertyGroup> comp = new Comparator<PropertyGroup>() {

            public int compare(PropertyGroup o1,
                    PropertyGroup o2) {

                return o1.getInfo().getPosition() - o2.getInfo().getPosition();
            }
        };
        List<PropertyGroup> groupList = new ArrayList<PropertyGroup>();
        groupList.addAll(config.getPropertyGroupList());
        Collections.sort(groupList, comp);

        boolean firstGroupAdded = false;
        for (PropertyGroup propGroup : groupList) {
            String headerComments = null;
            if (!firstGroupAdded) {
                firstGroupAdded = true;
                headerComments = configComments;
            }
            addProperties(props, propGroup, headerComments);
        }
    }

    private void addProperties(EditableProperties props, PropertyGroup propGroup, String headerComments) {

        String gName = propGroup.getName();
        String gDispName = propGroup.getInfo().getDisplayName();
        String gShortDesc = propGroup.getInfo().getShortDescription();

        gDispName = html2Text(gDispName);
        gShortDesc = html2Text(gShortDesc);

        String groupComments = null;

        if ( groupComments == null) {
            groupComments = gShortDesc;
        }
        if ( groupComments == null) {
            groupComments = gDispName;
        }
        if ( groupComments == null) {
            groupComments = gName;
        }

        if ( groupComments != null ) {
            // add new line
            groupComments = groupComments + "\n";
        }

        if (headerComments != null ) {
            groupComments = headerComments + " \n" + groupComments;
        }

        Comparator<Property> comp = new Comparator<Property>() {

            public int compare(Property o1, Property o2) {
                return o1.getInfo().getPosition() - o2.getInfo().getPosition();
            }
        };
        List<Property> propList = new ArrayList<Property>();
        propList.addAll(propGroup.getPropertyList());
        Collections.sort(propList, comp);

        boolean firstPropAdded = false;
        for (Property prop : propList) {
            String propHeaderComments = null;
            if (!firstPropAdded) {
                firstPropAdded = true;
                propHeaderComments = groupComments;
            }
            addProperties(props, prop, propHeaderComments);
        }

    }

    private void addProperties(EditableProperties props, Property prop, String headerComments) {
        boolean separator = false;
        String comments = prop.getInfo().getShortDescription();
        if (comments != null ) {
            comments = html2Text(comments);
        } else {
            comments = "";
        }
        if (headerComments != null) {
            comments = headerComments + "\n" + comments;
            separator = true;
        }
        String[] propComments = text2PropertyComments(comments);
        String propName = prop.getName();
        String propValue = prop.getValue();
        if (propValue == null) {
            propValue = "";
        }
        props.setProperty(propName, propValue);
        props.setComment(propName, propComments, separator);
    }

    private String[] getMainHeaderComments(String serviceName) {
        List<String> comments = new ArrayList<String>();
        comments.add("# " + new Date().toString());
        comments.add("# Service configuration for service " + serviceName);
        comments.add("# Do not remove or modify the  \"service.configuration.mode\" value. Required by fuji tools.");
        comments.add("# ");
        return comments.toArray(new String[comments.size()]);
    }

    private String[] createComments(String format, String cName, String cDispName, String cShortDesc, String[] headerComments) {
        if (cDispName == null) {
            cDispName = cName;
        }
        if (cShortDesc == null) {
            cShortDesc = cName;
        }
        cDispName = html2Text(cDispName);
        cShortDesc = html2Text(cShortDesc);

        String comments = MessageFormat.format(format, cDispName, cShortDesc);
        if ( headerComments != null ) {
            comments = headerComments + comments;
        }
        List<String> commentsList = new ArrayList<String>();
        commentsList.addAll(Arrays.asList(headerComments));
        commentsList.addAll(Arrays.asList(text2PropertyComments(comments)));
        return commentsList.toArray(new String[commentsList.size()]);
    }

    private String[] text2PropertyComments(String text) {

        StringReader reader = new StringReader(text);
        List<String> comments = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(reader);
            String line = null;
            while ((line = in.readLine()) != null) {
                if (!line.startsWith("#")) {
                    comments.add("# " + line);
                } else {
                    comments.add(line);
                }
            }
        } catch (IOException ex) {
            LOG.log(Level.FINE, null, ex);
        }
        return comments.toArray(new String[comments.size()]);
    }

    private String html2Text(String html) {
        if ( html == null ) {
            return null;
        }
        boolean foundHtmlTag = false;
        if ( html.length() > 6 ) {
            String htmlTag = html.substring(0, 6);
            if ("<html>".equalsIgnoreCase(htmlTag)) {
                foundHtmlTag = true;
            }
        }
        if (!foundHtmlTag) {
            return html;
        }
        StringReader reader = new StringReader(html);
        StringWriter writer = new StringWriter();
        final PrintWriter out = new PrintWriter(writer);

        ParserDelegator parserDelegator = new ParserDelegator();

        ParserCallback parserCallback = new ParserCallback() {
            @Override
            public void handleSimpleTag(Tag t, MutableAttributeSet a, int pos) {
                if ( t.breaksFlow()) {
                    out.println();
                }
            }

            @Override
            public void handleStartTag(Tag t, MutableAttributeSet a, int pos) {
                if ( t.breaksFlow()) {
                    out.println();
                }
            }

            @Override
            public void handleText(char[] data, int pos) {
                out.print(new String(data));
            }

        };
        try {
            parserDelegator.parse(reader, parserCallback, true);
            return writer.getBuffer().toString().trim();
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
            return html;
        }
    }
}
