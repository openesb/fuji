/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.descriptor;

/**
 * This class holds the information about a property (name/value pair).
 * Configuration properties in the descriptor will be accessed using this.
 *
 * The value of a property can be a scalar data or a container of set of scalar
 * data. In case, the value object is a container, it will be serialized/deserialized
 * to/from it's string value using the format specified by <code>PropertyInfo.FormatType</code>
 *
 * @see PropertyInfo
 * @see PropertyInfo.FormatType
 * @see PropertyInfo.DataType
 * 
 * @author chikkala
 */
public interface Property {
    /**
     * details about the property including type and the locale specific descriptions.
     * @see PropertyInfo
     * @return
     */
    PropertyInfo getInfo();
    /**
     * get name of the property
     * @return
     */
    String getName();
    /**
     * get value of the property in a string form.
     * @return
     */
    String getValue();
    /**
     * sets the value of the property from the string value.
     * if the property is a container of the values, the string value is
     * parsed for that container format to convert the string into the data.
     *
     * @param value
     */
    void setValue(String value);
    /**
     * get value of the property as object that represent the data.
     * @return
     */
    Object getValueObject();
    /**
     * set the value of the property.
     * @param value
     */
    void setValueObject(Object value);
}
