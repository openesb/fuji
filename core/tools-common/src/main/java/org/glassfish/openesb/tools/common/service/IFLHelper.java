package org.glassfish.openesb.tools.common.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.glassfish.openesb.tools.common.descriptor.Configuration.UsageHint;

/**
 * Helper for deriving information about the message excahnge type from the
 * use of the service in ifl.
 *
 *
 * @author Jim Fu
 * @author chikkala
 *
 */
public class IFLHelper {

    private static final String EXCHANGE_PATTERN_IN = "in";
    private static final String EXCHANGE_PATTERN_OUT = "out";
    private static final String EXCHANGE_PATTERN_ONDEMAND = "ondemand";
    // jim.fu@sun.com:
    // 2way operation's service properties generation 
    // need more evaluation since there is difference how service.properties
    // is populated: from webui, user actually configure 
    // half of the request-response separately, i.e., one node on the 
    // flow canvas represents only one way of the two way operation
    // so put these two enum value here as place holder, will address two way
    // operation configuration later.
    private static final String EXCHANGE_PATTERN_INOUT = "inout:in";
    private static final String EXCHANGE_PATTERN_OUTIN = "outin:in";

    /**
     * helper to figure out service usage in ifl.
     * by examing the ifl model.
     * @return a string representing message direction
     */
    public UsageHint getServiceUsageHint(IFLModel ifl, String serviceName) {
        UsageHint usage = UsageHint.ANY;
        Map<String, Entry<String, List<String>>> routes = ifl.getRoutes();
        String direction = null;

        if (ifl.isConsumed(serviceName)) {
            // according to the way the ifl model built
            // it must also be in provided service list
            Entry<String, List<String>> route = getRouteOfFromService(routes, serviceName);
            if (route != null) {
                // if it is also the last of a route, it is IN-OUT
                // make sure the route is a "route", not "broadcast", "tee", "select", etc.
                if (isRouteRoute(route, serviceName) && isLastInRoute(route, serviceName)) {
                    // if the service is in a "route" route and
                    // is the last - inout
                    direction = EXCHANGE_PATTERN_INOUT;
                } else {
                    // oneway in
                    direction = EXCHANGE_PATTERN_IN;
                }
            } else {
                // throw new java.lang.IllegalStateException("consumed service is not a from service...:" + serviceName);
            }
        } else {
            if (ifl.isProvided(serviceName)) {
                // a service as destination
                // if it is in a route and it is not the last
                // then it is an on-demand
                Entry<String, List<String>> route = getRouteOfToService(routes, serviceName);
                if (isRouteRoute(route, serviceName) && !isLastInRoute(route, serviceName)) {
                    // if the service is in a "route" route and
                    // is not the last, then we expect it to fetch something
                    // to service but exprects a output
                    direction = EXCHANGE_PATTERN_ONDEMAND;
                } else {
                    // oneway out
                    direction = EXCHANGE_PATTERN_OUT;
                }
            } else {
                // service declared does not appear in any EIP,
                // does not generate service.properties for the service instance,
                // if later, the service name participates in the EIPs,
                // its direction will be calculated and service.properties
                // generated then.
            }
        }
        if (direction != null ) {
            usage = UsageHint.fromXsdEnum(direction);
        }
        return usage;
    }
    /**
     * helper to figure out message exchange pattern
     * by examing the ifl model.
     * @return a string representing message exchange pattern for the use of
     * this service in ifl
     */
    public String getMessageExchangePatternForService(IFLModel ifl, String serviceName) {
        Map<String, Entry<String, List<String>>> routes = ifl.getRoutes();
        String messageExchangeType = ServiceConfigurationHelper.ANY_MESSAGE_EXCHANGE_TYPE;
        if (ifl.isConsumed(serviceName)) {
            Entry<String, List<String>> route = getRouteOfFromService(routes, serviceName);
            if (route != null) {
                if (isRouteRoute(route, serviceName) && isLastInRoute(route, serviceName)) {
                    messageExchangeType = ServiceConfigurationHelper.IN_OUT_MESSAGE_EXCHANGE_TYPE;
                } else {
                    messageExchangeType = ServiceConfigurationHelper.IN_ONLY_MESSAGE_EXCHANGE_TYPE;
                }
            } else {
                // throw new IllegalStateException("consumed service is not a from service...:" + serviceName);
            }
        } else {
            if (ifl.isProvided(serviceName)) {
                Entry<String, List<String>> route = getRouteOfToService(routes, serviceName);
                if (isRouteRoute(route, serviceName) && !isLastInRoute(route, serviceName)) {
                    messageExchangeType = ServiceConfigurationHelper.IN_OUT_MESSAGE_EXCHANGE_TYPE;
                } else {
                    messageExchangeType = ServiceConfigurationHelper.IN_ONLY_MESSAGE_EXCHANGE_TYPE;
                }
            } else {
            }
        }
        return messageExchangeType;
    }

    private boolean isRouteRoute(Entry<String, List<String>> route, String serviceName) {
        boolean result = false;
        if (route != null && route.getValue() != null && route.getValue().size() > 0) {
            result = route.getValue().contains(serviceName);
        }
        return result;
    }

    private boolean isLastInRoute(Entry<String, List<String>> route, String serviceName) {
        boolean result = false;
        if (route != null && route.getValue() != null && route.getValue().size() > 0) {
            int index = route.getValue().lastIndexOf(serviceName);
            result = (index == route.getValue().size() - 1);
        }
        return result;
    }

    private Entry<String, List<String>> getRouteOfFromService(Map<String, Entry<String, List<String>>> routes, String serviceName) {
        Iterator it = routes.keySet().iterator();
        Entry<String, List<String>> route = null;
        while (it.hasNext()) {
            route = routes.get(it.next().toString());
            if (route.getKey() != null && route.getKey().equals(serviceName)) {
                break;
            }
            route = null;
        }
        return route;
    }

    private Entry<String, List<String>> getRouteOfToService(Map<String, Entry<String, List<String>>> routes, String serviceName) {
        Iterator it = routes.keySet().iterator();
        Entry<String, List<String>> route = null;
        while (it.hasNext()) {
            route = routes.get(it.next().toString());
            if (route.getValue() != null) {
                if (route.getValue().contains(serviceName)) {
                    break;
                }
            }
            route = null;
        }
        return route;
    }
}
