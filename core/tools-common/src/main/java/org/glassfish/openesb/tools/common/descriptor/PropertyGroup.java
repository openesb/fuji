/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.descriptor;

import java.util.List;

/**
 * This class provides the details about the property group including its localized
 * description and the list of properties associated with this group.
 *
 * @author chikkala
 */
public interface PropertyGroup {

    /**
     * contains the common attributes and the locale specific information
     * @return
     */
    Info getInfo();

    /**
     * get name of the property group
     * @return
     */
    String getName();

    /**
     * return all the properties in this group.
     * @return List of Property objects.
     */
    List<Property> getPropertyList();

    public static interface Info extends FeatureInfo {
        /**
         * get name of the property group
         * @return
         */
        String getName();
        /**
         * return an integer representing the position. May be used in the ui layout for this
         * property group
         * @return
         */
        int getPosition();
    }
}
