/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.tools.common.descriptor;

import java.net.URL;

/**
 *
 * @author chikkala
 */
public interface Template {
    boolean isOverwrite();
    boolean isFiltered();
    String getAlias();
    URL getURL();
}
