/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.service.spi.Provider;

/**
 * This class provides the default implementation of the <code>ToolsLookup</code>
 * that uses the jar service provider mechanism to locate the service tye descriptor
 * related resources and classes.
 *
 * @author chikkala
 */
public class DefaultToolsLookup extends ToolsLookup {

    private static final Logger LOG = Logger.getLogger(DefaultToolsLookup.class.getName());
    private URLClassLoader mLoader = null;

    public DefaultToolsLookup() {
        super();
    }

    /**
     * find the provider using jar service lookup
     * @param serviceType
     * @return
     */
    protected Provider findServiceDescriptorProvider(String serviceType) {
        Provider provider = null;
        String providerMetaInfRes = Utils.META_INF_SERVICES + Provider.class.getName();
        ClassLoader providerLoader = findClassLoader(serviceType);
        try {
            LOG.fine("Looking up metainf services for " + providerMetaInfRes);
            Enumeration<URL> serviceURLs = providerLoader.getResources(providerMetaInfRes);
            for (URL url = null; serviceURLs.hasMoreElements();) {
                LOG.fine("Found a jar service config file....");
                url = serviceURLs.nextElement();
                String implClass = Utils.readJarServiceConfig(url);
                if (implClass == null) {
                    continue;
                }
                LOG.fine("Found Service Descriptor Provider class name " + implClass);
                provider = instantiateServiceDescriptorProvider(providerLoader, implClass);
                if (provider != null && serviceType.equals(provider.getServiceType())) {
                    // found the impl.
                    break;
                }
                // find next
                provider = null;
            }
        } catch (IOException ex) {
            LOG.log(Level.INFO, "No META-INF/services/... configuration found for service type " + serviceType, ex);
        }
        return provider;
    }

    public final void addClassLoaderURLs(URL[] classPathURLs) {
        if (this.mLoader == null) {
            this.mLoader = new URLClassLoader(new URL[0], this.getClass().getClassLoader());
        }

        List<URL> currentURLs = new ArrayList<URL>();
        ClassLoader cl = this.mLoader;
        while (cl != null) {
            if (cl instanceof URLClassLoader) {
                currentURLs.addAll(Arrays.asList(((URLClassLoader) cl).getURLs()));
            } else {
                break;
            }
            cl = cl.getParent();
        }

        List<URL> newURLs = new ArrayList<URL>();
        for (URL url : classPathURLs) {
            if (!currentURLs.contains(url)) {
                newURLs.add(url);
                LOG.fine("Adding URL to Lookup classloader... " + url);
            } else {
                LOG.fine("Current Lookup classloader already has the URL " + url);
            }
        }
        this.mLoader = new URLClassLoader(newURLs.toArray(new URL[newURLs.size()]), this.mLoader);
    }

    protected final ClassLoader findClassLoader(String serviceType) {
        if (this.mLoader == null) {
            this.mLoader = new URLClassLoader(new URL[0], this.getClass().getClassLoader());
        }
        URL[] classpathURLs = findClassLoaderURLs(serviceType);
        if (classpathURLs != null && classpathURLs.length > 0) {
            addClassLoaderURLs(classpathURLs);
        }
        return this.mLoader;
    }

    protected URL[] findClassLoaderURLs(String serviceType) {
        return null;
    }

}
