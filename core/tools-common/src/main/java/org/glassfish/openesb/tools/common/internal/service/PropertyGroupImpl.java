/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.internal.service;

import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup.Info;
import org.glassfish.openesb.tools.common.internal.service.jaxb.DisplayName;
import org.glassfish.openesb.tools.common.internal.service.jaxb.LongDescription;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ObjectFactory;
import org.glassfish.openesb.tools.common.internal.service.jaxb.PropertyGroup;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ShortDescription;

/**
 *
 * @author chikkala
 */
public class PropertyGroupImpl implements org.glassfish.openesb.tools.common.descriptor.PropertyGroup {

    private Info mInfo;
    private List<Property> mProps;

    public PropertyGroupImpl(Info info, List<Property> props) {
        this.mInfo = info;
        this.mProps = Collections.unmodifiableList(props);
    }

    public String getName() {
        return this.mInfo.getName();
    }

    public List<Property> getPropertyList() {
        return this.mProps;
    }

    public Info getInfo() {
        return this.mInfo;
    }
    
    public static class InfoImpl extends FeatureInfoImpl implements Info {

        private PropertyGroup mJaxbGroup;
        
        public InfoImpl(ResourceBundle i18NBundle, ClassLoader classLoader,
                PropertyGroup jaxbGroup) {
            super(i18NBundle, classLoader, jaxbGroup.getDisplayName(), jaxbGroup.getShortDescription(), jaxbGroup.getLongDescription());
            this.mJaxbGroup = jaxbGroup;
        }

        public String getName() {
            return this.mJaxbGroup.getName();
        }

        public int getPosition() {
            return this.mJaxbGroup.getPosition();
        }

        public static InfoImpl createInfoImpl(ResourceBundle i18NBundle, ClassLoader classLoader,
                String dispNameKey, String shortDescKey, String longDescKey, String groupName) {
            InfoImpl impl = null;
            ObjectFactory factory = new ObjectFactory();
            DisplayName dispName = factory.createDisplayName();
            dispName.setKey(dispNameKey);
            ShortDescription shortDesc = factory.createShortDescription();
            shortDesc.setKey(shortDescKey);
            LongDescription longDesc = factory.createLongDescription();
            longDesc.setKey(longDescKey);
            PropertyGroup group = factory.createPropertyGroup();

            group.setDisplayName(dispName);
            group.setShortDescription(shortDesc);
            group.setLongDescription(longDesc);
            group.setName(groupName);
            group.setPosition(new Integer(0));

            impl = new InfoImpl(i18NBundle, classLoader, group);
            return impl;
        }
    }
}
