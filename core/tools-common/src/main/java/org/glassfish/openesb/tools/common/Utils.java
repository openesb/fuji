/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Utilites class that provide common utilities used through out the tools API
 * and SPI implemenation.
 *
 * @author chikkala
 */
public class Utils {

    private static final Logger LOG = Logger.getLogger(Utils.class.getName());
    /**
     * meta-inf services file path.
     */
    public static final String META_INF_SERVICES = "META-INF/services/";

    public static void copy(InputStream src, OutputStream dest) throws IOException {
        BufferedInputStream in = null;
        BufferedOutputStream out = null;

        in = new BufferedInputStream(src);
        out = new BufferedOutputStream(dest);

        byte[] buffer = new byte[8 * 1024];
        int length = -1;

        while ((length = in.read(buffer)) != -1) {
            out.write(buffer, 0, length);
        }
        out.flush();
    }

    public static void copy(InputStream src, OutputStream dest, long limit) throws IOException {
        if (limit < 0) {
            // no limit. copy till the end of the stream
            copy(src, dest);
            return;
        }
        BufferedInputStream in = null;
        BufferedOutputStream out = null;

        in = new BufferedInputStream(src);
        out = new BufferedOutputStream(dest);

        long remaining = limit;

        int bufferSize = 8 * 1024;
        byte[] buffer = new byte[bufferSize];
        int length = -1;

        do {
            if (remaining > bufferSize) {
                length = in.read(buffer);
            } else {
                length = in.read(buffer, 0, (int) remaining);
            }
            if (length == -1) {
                LOG.info("Input stream finished prematurely: read " +
                        (limit - remaining) + " bytes out of " + limit + ".");
                return;
            }
            remaining -= length;
            out.write(buffer, 0, length);
        } while (remaining > 0);

        out.flush();
    }

    public static void copy(URL src, OutputStream dest) throws IOException {
        InputStream in = null;
        try {
            in = src.openStream();
            copy(in, dest);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ex) {
                    // ignore. or log it at finest
                }
            }
        }
    }

    public static void copy(InputStream src, File dest) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(dest);
            copy(src, out);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception ex) {
                    // ignore. or log it at finest.
                }
            }
        }
    }

    public static void copy(InputStream src, File dest, long limit) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(dest);
            copy(src, out, limit);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception ex) {
                    // ignore. or log it at finest.
                }
            }
        }
    }

    public static void copy(URL src, File dest) throws IOException {
        InputStream in = null;
        try {
            in = src.openStream();
            copy(in, dest);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ex) {
                    // ignore. or log it at finest
                }
            }
        }
    }

    public static void copy(File src, File dest) throws IOException {
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        copy(src.getAbsoluteFile().toURI().toURL(), dest);
    }

    public static Properties readProperties(URL src) throws IOException {
        Properties props = new Properties();
        InputStream in = null;
        try {
            in = src.openStream();
            props.load(in);
            return props;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception ex) {
                    // ignore. or log it at finest
                }
            }
        }
    }

    public static Properties readProperties(File src) throws IOException {
        return readProperties(src.getAbsoluteFile().toURI().toURL());
    }

    /**
     * reads the jar service configuration url and returns the class name specified
     * in the service configuration. It skips the comments (starts with #) and new
     * lines
     * @param configURL  URL to the META-INF/services/x.y.z file to read
     * @return class name that implements the interface specified as jar service.
     */
    public static String readJarServiceConfig(URL configURL) {
        String implClassName = null;
        InputStream inS = null;
        InputStreamReader inR = null;
        try {
            inS = configURL.openStream();
            inR = new InputStreamReader(inS);
            BufferedReader in = new BufferedReader(inR);
            for (String line = null; (line = in.readLine()) != null;) {
                if (line.startsWith("#")) {
                    // comment. so, skip
                    continue;
                }
                int idx = line.indexOf("#");
                if (idx > 0) {
                    implClassName = line.substring(0, idx);
                } else {
                    implClassName = line;
                }
                if (implClassName.trim().length() == 0) {
                    implClassName = null;
                    continue;
                }
                // if you reach here. you found the class.
                break;
            }

        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        } finally {
            if (inS != null) {
                try {
                    inS.close();
                } catch (IOException ex) {
                    //ignore.
                }
            }
        }
        return implClassName;
    }

    protected static String clazzToString(Class clazz) {
        return clazz.getName() + "@" + clazz.getClassLoader() + ":" + clazz.getProtectionDomain().getCodeSource().getLocation(); // NOI18N
    }

    public static <T> T instantiateClass(Class<T> interfaceClass, String implClass, ClassLoader loader) {
        T provider = null;
        try {
            Class instClass = Class.forName(implClass, false, loader);
            if (!interfaceClass.isAssignableFrom(instClass)) {
                LOG.info(clazzToString(instClass) + " not a subclass of " + clazzToString(interfaceClass));
            } else {
                // instantiate the class
                provider = (T) instClass.newInstance();
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }

        return provider;
    }

    public static class UnJar {

        private URL mJarURL;
        private JarFile mJarFile;

        public UnJar(URL jarURL) {
            this.mJarURL = jarURL;
            File urlFile = null;
            // try converting url back to file.
            try {
                urlFile = new File(this.mJarURL.toURI());
            } catch (Exception ex) {
                // ingore. can not convert the url to file
            }
            if (urlFile != null && urlFile.exists()) {
                // try converting file to jar file
                try {
                    this.mJarFile = new JarFile(urlFile);
                } catch (IOException ex) {
                    //ingore. can not convert the url to jar file.
                }
            }
        }

        public void unjar(File destDir) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void copyToFile(String path, File destFile) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void copyToDir(String path, File destDir) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        private static void copy(JarFile jarFile, ZipEntry entry, OutputStream dest) throws IOException {
            InputStream in = null;
            try {
                in = jarFile.getInputStream(entry);
                Utils.copy(in, dest);
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        LOG.log(Level.FINE, null, ex);
                    }
                }
            }
        }

        public void copy(String path, OutputStream dest) throws IOException {
            if (this.mJarFile != null) {
                ZipEntry entry = this.mJarFile.getEntry(path);
                if (entry == null) {
                    throw new IOException("Entry Not Found " + path);
                }
                copy(this.mJarFile, entry, dest);
                return;
            }
            // stream copy
            InputStream urlIS = null;
            ZipInputStream zipIS = null;
            try {
                urlIS = this.mJarURL.openStream();
                zipIS = new ZipInputStream(urlIS);
                ZipEntry entry = null;
                boolean found = false;
                while ((entry = zipIS.getNextEntry()) != null) {
                   // LOG.info(entry.getName());
                    if (entry.getName().equals(path) && !entry.isDirectory()) {
                        found = true;
                        Utils.copy(zipIS, dest, entry.getSize());
                        break;
                    }
                }
                if (!found) {
                    throw new IOException("Entry Not Found " + path);
                }
            } finally {
                try {
                    if (urlIS != null) {
                        urlIS.close();
                    }
                } catch (IOException e) {
                }
            }
        }

        public Manifest getManifest() {
            Manifest mf = null;
            if (this.mJarFile != null) {
                try {
                    mf = this.mJarFile.getManifest();
                } catch (IOException ex) {
                    LOG.log(Level.FINE, null, ex);
                }
            } else {
                InputStream in = null;
                try {
                    in = this.mJarURL.openStream();
                    JarInputStream jarIn = new JarInputStream(in);
                    mf = jarIn.getManifest();
                } catch (Exception ex) {
                    LOG.log(Level.FINE, null, ex);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException ex) {
                            LOG.log(Level.FINE, null, ex);
                        }
                    }
                }
            }
            return mf;
        }

        public List<String> getEntries() {
            List<String> entryList = new ArrayList<String>();
          
            if (this.mJarFile != null) {
                Enumeration<JarEntry> entries = this.mJarFile.entries();
                for (; entries.hasMoreElements();) {
                    entryList.add(entries.nextElement().getName());
                }
                return entryList;
            }
            // get it from stream
            InputStream urlIS = null;
            ZipInputStream zipIS = null;
            try {
                urlIS = this.mJarURL.openStream();
                zipIS = new ZipInputStream(urlIS);
                ZipEntry entry = null;
                boolean found = false;
                while ((entry = zipIS.getNextEntry()) != null) {
                    entryList.add(entry.getName());
                }
            } catch(Exception ex){
                LOG.log(Level.FINE, ex.getMessage(), ex);
            } finally {
                try {
                    if (urlIS != null) {
                        urlIS.close();
                    }
                } catch (IOException e) {
                }
            }
            return entryList;
        }
    }

    /**
     * This class has a refactored code of MavenUtil.archive to use in the
     * common tools.
     *
     */
    public static class Jar {

        private File mJarFile;
        Map<File, String> mFiles;

        public Jar(File jar) {
            this.mJarFile = jar;
            this.mFiles = new TreeMap<File, String>();
        }

        public void includeFile(File file, boolean includeRoot) {
            String pathPrefix = ""; // from root
            if (file.isFile() || (file.isDirectory() && includeRoot)) {
                pathPrefix = file.getName();
            }
            this.mFiles.put(file, pathPrefix);
        }

        public File create() throws IOException {

            File archive = this.mJarFile;
            if (archive.exists() && archive.isDirectory()) {
                throw new IOException("Trying to create an archive in " + // NOI18N
                        "place of an existing directory."); // NOI18N
            }

            final File archiveParent = archive.getParentFile();
            if (!archiveParent.exists() && !archiveParent.mkdirs()) {
                throw new IOException("Cannot create directory structure " + // NOI18N
                        "for " + archiveParent); // NOI18N
            }

            ZipOutputStream zipOutput = null;

            for (File input : this.mFiles.keySet()) {
                String pathPrefix = this.mFiles.get(input);
                if (!input.exists()) {
                    LOG.warning("Trying to create an archive out of a non-existing file." + input.getPath());
                }

                try {
                    zipOutput = new ZipOutputStream(new FileOutputStream(archive));

                    String[] excludes = {"CVS", ".svn"};

                    final List<String> exlcudesList = Arrays.asList(excludes);
                    FileFilter filter = new FileFilter() {

                        public boolean accept(File file) {
                            String name = file.getName();
                            return !exlcudesList.contains(name);
                        }
                    };
                    if (input.isFile() || (input.isDirectory() && pathPrefix.trim().length() != 0)) {
                        addToArchive(input, pathPrefix, zipOutput, filter);
                    } else {
                        // is directory and no prefix
                        final File[] children = input.listFiles(filter);
                        if ((children != null) && (children.length > 0)) {
                            for (File child : children) {
                                addToArchive(child, child.getName(), zipOutput, filter);
                            }
                        }
                    }

                } finally {
                    if (zipOutput != null) {
                        try {
                            zipOutput.close();
                        } catch (Exception ex) {
                            LOG.log(Level.FINEST, ex.getMessage(), ex);
                        }
                    }
                }
            }
            return this.mJarFile;
        }

        public void addToArchive(
                final File input,
                final String name,
                final ZipOutputStream zipOutput,
                final FileFilter filter) throws IOException {

            if (!input.exists()) {
                throw new IOException("Trying to add a non-existing file to " +
                        "the archive."); // NOI18N
            }
            if (name.trim().length() == 0 || "/".equals(name) || "\\".equals(name)) {
                throw new IOException("Invalid  zip entry " + name);
            }
            zipOutput.putNextEntry(createZipEntry(input, name));

            if (input.isDirectory()) {
                final File[] children = input.listFiles(filter);

                if ((children != null) && (children.length > 0)) {
                    for (File child : children) {
                        String childName =
                                (name + "/" + child.getName()).replace("\\", "/");
                        addToArchive(child, childName, zipOutput, filter);
                    }
                }
            } else {
                FileInputStream fileInput = null;
                try {
                    fileInput = new FileInputStream(input);
                    copy(fileInput, zipOutput);
                } finally {

                    if (fileInput != null) {
                        try {
                            fileInput.close();
                        } catch (Exception ex) {
                            LOG.log(Level.FINEST, ex.getMessage(), ex);
                        }
                    }
                }
            }
        }

        public ZipEntry createZipEntry(
                final File input,
                final String name) {

            assert input.exists();

            final String realName;
            if (input.isDirectory() && !name.endsWith("/")) {
                realName = name + "/";
            } else {
                realName = name;
            }
            final ZipEntry entry = new ZipEntry(realName);
            entry.setSize(input.length());
            entry.setTime(input.lastModified());

            return entry;
        }
    }
}
