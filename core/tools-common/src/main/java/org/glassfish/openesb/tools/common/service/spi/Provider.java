/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service.spi;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;

/**
 * Provider of a given service type should provide the implementation of this
 * abstract class to access the service descriptor xml file which provides
 * the details of the metadata, resources, templates and other SPI implementations
 * in the service descriptor archive file.
 * <p>
 * This is a required SPI that all service type providers need to implement.
 * <p>
 * The fuji application development and deployment tools (maven, ide or webui) that
 * need information about a particular service type and its related artifacts would
 * lookup this interface and gather information about the service type metadata and
 * the optional SPI implemenation to provide a service unit development and
 * deployment activities for a given service type.
 * <p>
 * The details of the concrete implementation of this class should be provided in
 * a jar service provider configuration file as specified in the Jar file specification.
 * <p>
 * For example, for a "foo" service type, if com.bar.FooProvider.java
 * is the class that extends this class and provides the concrete implementation, 
 * then the foo-service-descriptor.jar file should have a file with file name
 * <code>org.glassfish.openesb.tools.common.service.spi.Provider</code> under
 * META-INF/services like
 * <code>
 * META-INF/services/org.glassfish.openesb.tools.common.service.spi.Provider
 * </code>
 * The content of the file is a single line of text specifying the name of the concrete
 * implemenation like
 * <code>
 * com.bar.FooProvider
 * </code>
 * @see http://java.sun.com/j2se/1.3/docs/guide/jar/jar.html#Service%20Provider
 *
 *
 * @author chikkala
 */
public abstract class Provider {

    private static final Logger LOG = Logger.getLogger(Provider.class.getName());
    private static DocumentBuilder sDocBuilder = null;
    private static XPathExpression sServiceTypeXpathExpr = null;
    private String mServiceType;

    /**
     * default constructor.
     */
    public Provider() {
        init();
    }

    /**
     * Should return the URL pointing to the service descriptor xml file written
     * according to the service descriptor xml schema.
     *
     * the service descriptor xml file should be in a service descriptor jar file
     * and the URL can be obtained from the Classloader resource url. For example,
     * the path to the foo-service-descriptor.xml in the foo-service-descriptor.jar
     * is /com/bar/foo/foo-service-descriptor.xml then the implementation can be
     * <code>
     * public URL getServiceDescriptorURL() {
     *      return this.getClass().getClassLoader()
     *             .getResource("/com/bar/foo/foo-service-descriptor.xml")
     * }
     * <code>
     *
     * @return URL of the service descriptor xml
     */
    public abstract URL getServiceDescriptorURL();

    /**
     * the service type for which concrete implemenation of this class belongs.
     * @return the service type.
     */
    public final String getServiceType() {
        if (this.mServiceType == null) {
            LOG.warning("Service type is NOT set in Service Descriptor Provider " + this.getClass().getName());
        }
        return this.mServiceType;
    }

    /**
     * sets the service type. Extended classes when override the default
     * implementation of the #init() method should use this method to set the
     * service type.
     *
     * @param serviceType
     */
    protected final void setServiceType(String serviceType) {
        this.mServiceType = serviceType;
    }

    /**
     *  intializes the service type.
     *
     *  Default implementation looks up the service descriptor xml and finds the
     *  service type using xpath and sets the service type.
     *
     *  Extended classes may override this implemenation to provide a simple
     *  implementation such as directly setting the service type using string
     *  constant of the extended class.
     *
     */
    protected void init() {
        String serviceType = null;
        URL sdXmlURL = getServiceDescriptorURL();
        if (sdXmlURL == null) {
            LOG.warning("NULL service descriptor URL in " + this.getClass().getName());
            return;
        }
        serviceType = getServiceTypeFromServiceDecriptorXML(sdXmlURL);
        // use xpath to get the service type from the service descriptor xml.
        this.setServiceType(serviceType);
    }

    /**
     * This is the default implemenation to get the service type value from descriptor.
     * 
     * @param sdURL
     * @return
     */
    private String getServiceTypeFromServiceDecriptorXML(URL sdURL) {
        String serviceType = null;

        if (sDocBuilder == null) {
            // initialize Doc builder.
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            // domFactory.setNamespaceAware(true);
            domFactory.setNamespaceAware(false);
            try {
                sDocBuilder = domFactory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
        }

        if (sServiceTypeXpathExpr == null) {
            // initialize the xpath exp
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            try {
                sServiceTypeXpathExpr = xpath.compile("/service-descriptor/@serviceType");
            } catch (XPathExpressionException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
        }

        InputStream in = null;
        try {
            in = sdURL.openStream();
            Document sdDoc = sDocBuilder.parse(in);
            Object result = sServiceTypeXpathExpr.evaluate(sdDoc, XPathConstants.STRING);
            serviceType = (String) result;
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    //ignore
                }
            }
        }
        return serviceType;
    }
}
