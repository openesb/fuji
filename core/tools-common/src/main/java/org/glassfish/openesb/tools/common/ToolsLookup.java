/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.internal.service.ServiceDescriptorImpl;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import java.net.URL;
import java.util.Locale;
import org.glassfish.openesb.tools.common.service.spi.Provider;

/**
 * This class provides the lookup methods to get to the service type
 * descriptor bundle and its artifacts.
 *
 * Extended classes should provide the lookup mechanism specific to the environment.
 *
 * @author chikkala
 */
public abstract class ToolsLookup {
    /**
     * property that provides the available service types. This will be registered as a
     * OSGi Service property when this class is registered as OSGi service.
     */
    public static final String PROP_AVAILABLE_SERVICE_TYPES = "available.service.types";
    
    private static final Logger LOG = Logger.getLogger(ToolsLookup.class.getName());
    private Map<String, Provider> mProviderMap;
    
    public ToolsLookup() {
        this.mProviderMap = new HashMap<String, Provider>();
    }
    /**
     *
     * @param serviceType service type for which the service descriptor xml is needed.
     * @return URL to the service descriptor xml
     */
    public final URL getServiceDescriptorURL(String serviceType) {
        URL sdURL = null;
        Provider provider = getServiceDescriptorProvider(serviceType);
        if (provider != null) {
            sdURL = provider.getServiceDescriptorURL();
        }
        return sdURL;
    }
    /**
     * class loader used for this service descriptor to load resources or classes from the
     * service descriptor bundle for this service type.
     * @param serviceType service type for which the service descriptor will be loaded.
     * @return ClassLoader for the service descriptor bundle.
     */
    public final ClassLoader getServiceDescriptorClassLoader(String serviceType) {
        ClassLoader sdClassLoader = null;
        Provider provider = getServiceDescriptorProvider(serviceType);
        if (provider != null) {
            sdClassLoader = provider.getClass().getClassLoader();
        }
        return sdClassLoader;
    }
    /**
     * return the ServiceDescriptor interface using which user can access the resources and
     * classes in the service descriptor bundle.
     * <p>
     * The service descriptor loaded by this method uses a default Locale
     * to loading the service descriptor's i18n resources.
     * @param serviceType service type of the service descriptor to lookup.
     * @return
     */
    public final ServiceDescriptor getServiceDescriptor(String serviceType) throws Exception {
        return getServiceDescriptor(serviceType, Locale.getDefault());
    }
    /**
     * return the ServiceDescriptor interface using which user can access the resources and
     * classes in the service descriptor bundle.
     * <p>
     * The service descriptor loaded by this method uses a default Locale
     * to loading the service descriptor's i18n resources.
     * @param serviceType service type of the service descriptor to lookup.
     * @param locale locale that should be used to load the i18n resources.
     * @return
     */
    public final ServiceDescriptor getServiceDescriptor(String serviceType, Locale locale) throws Exception {
        ServiceDescriptor sd = null;
        URL sdURL = getServiceDescriptorURL(serviceType);
        ClassLoader classLoader = getServiceDescriptorClassLoader(serviceType);
        // Load service descriptor model from the url
        if ( sdURL != null ) {
            sd = ServiceDescriptorImpl.createServiceDescriptor(sdURL, classLoader, locale);
        } else {
            LOG.info("Can not find the service descriptor URL for " + serviceType);
        }
        return sd;
    }

    /**
     * loads all the available service descriptors and return
     *
     * @return List of ServiceDescriptor
     */
    public final List<ServiceDescriptor> getAvailableServiceDescriptors() {
        List<ServiceDescriptor> sdList = new ArrayList<ServiceDescriptor>();
        for ( String serviceType : this.mProviderMap.keySet()) {
            try {
                sdList.add(this.getServiceDescriptor(serviceType));
            } catch (Exception ex) {
                LOG.log(Level.INFO, null, ex);
            }
        }
        return sdList;
    }

    protected abstract Provider findServiceDescriptorProvider(String serviceType);

    protected final Provider getServiceDescriptorProvider(String serviceType) {
        Provider provider = null;
        if (serviceType == null) {
            return null;
        }
        provider = this.mProviderMap.get(serviceType);
        if (provider == null) {
            provider = findServiceDescriptorProvider(serviceType);
        }
        return provider;
    }

    protected final Provider addServiceDescriptorProvider(String serviceType, Provider provider) {
        Provider old = null;
        if (serviceType == null || provider == null ) {
            return null;
        }
        old = this.mProviderMap.put(serviceType, provider);
        return old;
    }

    protected final Provider removeServiceDescriptorProvider(String serviceType) {
        Provider provider = null;
        if (serviceType == null) {
            return null;
        }
        provider = this.mProviderMap.remove(serviceType);
        return provider;
    }

    protected final void clearAllServiceDescriptorProviders() {
        this.mProviderMap.clear();
    }
    /**
     * returns available providers
     *
     * @return List of ServiceDescriptor
     */
    protected final Map<String, Provider> getServiceDescriptorProviders() {
        return Collections.unmodifiableMap(this.mProviderMap);
    }

    protected final Provider instantiateServiceDescriptorProvider(ClassLoader loader, String implClass) {
        Provider provider = null;
        try {
            Class providerClass = Provider.class;
            Class instClass = Class.forName(implClass, false, loader);
            if (!providerClass.isAssignableFrom(instClass)) {
                LOG.info(clazzToString(instClass) + " not a subclass of " + clazzToString(providerClass));
            } else {
                // instantiate the class
                provider = (Provider) instClass.newInstance();
            }
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }

        return provider;
    }

    protected static String clazzToString(Class clazz) {
        return clazz.getName() + "@" + clazz.getClassLoader() + ":" + clazz.getProtectionDomain().getCodeSource().getLocation(); // NOI18N
    }
}
