/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.tools.common.templates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
/**
 *
 * @author chikkala
 */
public class PropertyMap {

    private static final Logger LOG = Logger.getLogger(PropertyMap.class.getName());
    Map<String, PropertyNode> mMap;

    public PropertyMap() {
        this.mMap = new HashMap<String, PropertyNode>();
    }

    public Object getProperty(String name) {
        return getProperty(name, null);
    }

    public Object getProperty(String name, Object defaultValue) {
        Object value = defaultValue;
        // split the path to node keys
        String[] keys = splitPropertyName(name);
        if (keys.length == 0) {
            return defaultValue;
        }
        // find the root node
        String rootKey = keys[0];
        PropertyNode node = this.mMap.get(rootKey);
        if (node == null) {
            // path not found.
            return defaultValue;
        }
        // find the leaf node.
        for (int i = 1; i < keys.length; ++i) {
            String key = keys[i];            
            PropertyNode child = node.mChildren.get(key);
            if (child == null) {
                // path not found
                return defaultValue;
            }
            node = child;
        }
        // get the value of the leaf node.
        value = node.mValue;
        return value;
    }

    public void setProperty(String name, Object value) {
        String[] keys = splitPropertyName(name);
        if (keys.length == 0) {
            // LOG.info("Empty split of the property ");
            return;
        }
        // LOG.info("Converting property " + name + " ....");
        String rootKey = keys[0];
        PropertyNode node = this.mMap.get(rootKey);
        if (node == null) {
            // create root node
            node = new PropertyNode();
            this.mMap.put(rootKey, node);
        }
        // create path with remaining keys.
        for (int i = 1; i < keys.length; ++i) {
            String key = keys[i];
            // PropertyNode child = null;
            // PropertyNode child = node.get(key);
            PropertyNode child = node.mChildren.get(key);
            if (child == null) {
                child = node.addChild(key);
            }
            node = child;
        }
        // set the value at the leaf node
        node.setValue(value);
    }

    public void clear() {
        this.mMap.clear();
    }

    private Map<String, Object> getPropertyNodeMap() {
        Map<String, Object> propMap = new HashMap<String, Object>();
        for ( String key : this.mMap.keySet()) {
            propMap.put(key, this.mMap.get(key));
        }
        return propMap;
    }

    private Properties path2Properties(LinkedList<Map.Entry<String, PropertyNode>> path ) {
        Properties props = new Properties();
            for (int i=0; i < path.size(); ++i) {
                Map.Entry<String, PropertyNode> entry = path.get(i);
                String value = entry.getValue().toString();
                if ( value != null ) {
                    String key = PropertyNode.pathToName(path, i);
                    props.setProperty(key, value);
                    // LOG.info("Path node " + i + " key:" + key + " Value: " + value);
                }
            }
        return props;
    }

    public Properties toProperties() {
        Properties props = new Properties();
        for (Map.Entry<String, PropertyNode> entry : this.mMap.entrySet()) {
            List<LinkedList<Map.Entry<String, PropertyNode>>> paths = entry.getValue().findPaths(entry);
            for (LinkedList<Map.Entry<String, PropertyNode>> path : paths) {
                Properties pathProps = path2Properties(path);
                for ( Object key : pathProps.keySet()) {
                    props.put(key, pathProps.get(key));
                }
            }
        }
        return props;
    }
    public void printNodes() {
        for (Map.Entry<String, PropertyNode> entry : this.mMap.entrySet()) {
            entry.getValue().printChildren(0,entry);

        }
    }
    public static String[] splitPropertyName(String name) {
        String[] keys = name.trim().split("\\.");
        ArrayList<String> keyList = new ArrayList<String>();
        for (String key : keys) {
            if ("".equals(key.trim())) {
                continue;
            } else {
                keyList.add(key.trim());
            }
        }
        return keyList.toArray(new String[keyList.size()]);
    }

    public static Map<String, Object> convertProperties(Properties props) {
        PropertyMap propMap = new PropertyMap();
        for (Object key : props.keySet()) {
            propMap.setProperty((String) key, props.get(key));
        }
        return propMap.getPropertyNodeMap();
    }

    public static class PropertyNode {

        Object mValue = null;
        Map<String, PropertyNode> mChildren;

        public PropertyNode() {
            this(null);
        }

        public PropertyNode(Object value) {
            this.mValue = value;
            this.mChildren = new HashMap<String, PropertyNode>();
        }

        public void setValue(Object value) {
            this.mValue = value;
        }

        /**
         * if the velocity template want to access the value of the property as
         * an object, they can use the method call on the PropertyNode to get
         * the value of the property as object. For example, a property "foo.bar"
         * is set as PropertyMap.setProperty("foo.bar", new Integer("123")), in
         * velocity templates, this value can be accessed as ${foo.bar} which
         * returns the string value of this object or ${foo.bar.toValue} which
         * returns the Integer object itself.
         * 
         * @return
         */
        public Object getValue() {
            return this.mValue;
        }
        /**
         * this is the interface to the velocity to evaluate the path as a velocity
         * variable like ${name1.child1.child12) which velcoity tranlates to the
         * java access like  root.get(name1).get(child1).get(child12)
         *
         * @param child name of the child node
         * @return retunr the node
         */
        public Object get(String child) {
             return this.mChildren.get(child);
        }

        public PropertyNode addChild(String child) {
            return addChild(child, null);
        }

        public PropertyNode addChild(String child, Object value) {
            PropertyNode childNode = new PropertyNode(value);
            this.mChildren.put(child, childNode);
            return childNode;
        }

        public void printChildren(int level, Map.Entry<String, PropertyNode> thisNode) {
            String tab = "";
            for ( int i=0; i < level; ++i) {
                tab += " ";
            }
            System.out.println(tab + "Node: " + thisNode.getKey() + ":" + thisNode.getValue().toString());
            if (this.mChildren.size() == 0 ) {
                return;
            }
            ++level;
            for ( Map.Entry<String, PropertyNode> entry : this.mChildren.entrySet()) {
                entry.getValue().printChildren(level, entry);
            }
        }

        public List<LinkedList<Map.Entry<String, PropertyNode>>> findPaths(Map.Entry<String, PropertyNode> thisEntry) {
            List<LinkedList<Map.Entry<String, PropertyNode>>> myPaths = new ArrayList<LinkedList<Map.Entry<String, PropertyNode>>>();
            LinkedList<Map.Entry<String, PropertyNode>> path = new LinkedList<Map.Entry<String, PropertyNode>>();
//            LOG.info("Finding path for " + thisEntry.getKey());
//            LOG.info("Size of the Node children " + this.mChildren.size());
            if (this.mChildren.size() == 0) {
                path.add(thisEntry);
                myPaths.add(path);
                return myPaths;
            }
            for (Map.Entry<String, PropertyNode> childEntry : this.mChildren.entrySet()) {
                // LOG.info("Finding path for child " + childEntry.getKey());
                List<LinkedList<Map.Entry<String, PropertyNode>>> childPaths = childEntry.getValue().findPaths(childEntry);
                for (LinkedList<Map.Entry<String, PropertyNode>> childPath : childPaths) {
                    childPath.addFirst(thisEntry);
                    myPaths.add(childPath);
                }
            }
            return myPaths;
        }

        /**
         * gets the sub path to the node at index idx inclusive.
         * @param path
         * @param idx index to the node in the path. 0 - path.size()-1
         * @return
         */
        public static String pathToName(List<Map.Entry<String, PropertyNode>> path, int idx) {
            StringBuffer buff = new StringBuffer();
            boolean isFirst = true;
            for (int i=0; i <= idx; ++i) {
                Map.Entry<String, PropertyNode> pathEntry = path.get(i);
                if (isFirst) {
                    isFirst = false;
                } else {
                    buff.append(".");
                }
                buff.append(pathEntry.getKey());
            }
            return buff.toString();
        }

        @Override
        public String toString() {

            if (this.mValue != null) {
                return this.mValue.toString();
            } else {
                return null;
            }
        }
    }
}
