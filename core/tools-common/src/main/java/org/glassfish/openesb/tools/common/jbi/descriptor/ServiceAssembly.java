/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssembly.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.tools.common.jbi.descriptor;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;

/**
 *
 * @author kcbabo
 */
public class ServiceAssembly extends Descriptor {
    
    public static final String ELEMENT_NAME =
            "service-assembly";
    
    public static final String CONNECTIONS = 
            "connections";
    
    public ServiceAssembly(Element element) {
        super(element);
    }
    
    public Identification getIdentification() {
        return new Identification(getChildElement(Identification.ELEMENT_NAME));
    }
    
    public List<ServiceUnit> getServiceUnits() {
        List<ServiceUnit> suList = new ArrayList<ServiceUnit>();
        List<Element> eleList = getChildElements(ServiceUnit.ELEMENT_NAME);
        
        for (Element ele : eleList) {
            suList.add(new ServiceUnit(ele));
        }
        
        return suList;
    }
    
    public ServiceUnit addServiceUnit() {
        return new ServiceUnit(createChildElementBefore(
                ServiceUnit.ELEMENT_NAME, CONNECTIONS));
    }
    
    public Connection addConnection() {
        return new Connection(createChildElement(
                getChildElement(CONNECTIONS), Connection.ELEMENT_NAME));
    }
    
    public List<Connection> getConnections() {
        List<Connection> connList = new ArrayList<Connection>();
        List<Element> eleList = getChildElements(Connection.ELEMENT_NAME);
        
        for (Element ele : eleList) {
            connList.add(new Connection(ele));
        }
        
        return connList;
    }
    
    @Override
    protected void initialize() {
        createChildElement(Identification.ELEMENT_NAME);
        createChildElement(CONNECTIONS);
    }
}
