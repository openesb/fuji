/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.descriptor.Template;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;
import org.glassfish.openesb.tools.common.templates.TemplateException;

/**
 * Default implementation of the service generator. This will be used when
 * there is no service type specific service generator is provided in that
 * service type descriptor bundle.
 *
 * service type specific implemenation of the SericeGenerator may extend this
 * class and override specific methods to implement the service type specific
 * implemention.
 *
 * @see ServiceGenerator
 * @see AbstractSPIProvider
 *
 * @author chikkala
 */
public class DefaultServiceGenerator extends AbstractSPIProvider implements ServiceGenerator {

    private static final Logger LOG = Logger.getLogger(DefaultServiceGenerator.class.getName());

    private void debugLog(String log) {
        // System.out.println(log);
        LOG.fine(log);
    }

    public DefaultServiceGenerator(ServiceDescriptor sd) {
        super(sd);
        // LOG.setLevel(Level.FINEST);
    }

    /**
     * creates the all the service artifacts for all the services of this type in the ifl.
     * If there is already a service configuration exists on this disk for a service, uses
     * that as the input to the generate the service artifacts so that the configuration
     * for that service is preserved.
     * 
     * @param prjModel
     * @param ifl
     * @throws java.io.IOException
     */
    public void generateServiceUnit(ServiceProjectModel prjModel, IFLModel ifl) throws IOException {
        
        // generate service artifacts using group notation.
        if (LOG.isLoggable(Level.FINE)) {
            debugLog("Generating service unit for " + this.getServiceType());
        }

        Map<String, Properties> allConfigMap = new HashMap<String, Properties>();
        Set<String> serviceGroups = new HashSet<String>();
        List<String> allServices = ifl.getServicesForType(this.getServiceType());
        if (LOG.isLoggable(Level.FINE)) {
            debugLog("Services " + allServices);
        }
        // build groups and the configurations for each service for this type.
        for (String serviceName : allServices ) {
            // System.out.println("Building group for service " + serviceName);
            String group = ifl.getGroupForService(serviceName);
            if ( group == null ) {
                throw new IOException("Can not determine the group for service " + serviceName);
            }
            serviceGroups.add(group);

            File configFile = new File(prjModel.getConfigDir(serviceName), ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
            if (!configFile.exists()) {
                if (LOG.isLoggable(Level.FINE)) {
                    debugLog("Generating configuration file for service " + serviceName);
                }
                ServiceConfigurationHelper sc = getServiceConfigurationHelper();
                // generate default configuration based on ifl usage.
                configFile = sc.generateServiceConfiguration(prjModel, ifl, serviceName, new Properties());
            }
            Properties configProps = Utils.readProperties(configFile);
            allConfigMap.put(serviceName, configProps);
        }

        
        if (LOG.isLoggable(Level.FINE)) {
            debugLog("Number of service groups. service type: " + this.getServiceType() + " Groups: " + serviceGroups);
            debugLog("All configurations " + allConfigMap);
        }
        // for each group now generate the artifacts.
        for (String group : serviceGroups) {
            // build grouped services and services references sets and then their config maps.
            Set<String> groupedServices = new TreeSet<String>();
            Set<String> serviceRefs = new TreeSet<String>();
            findServicesInServiceGroup(ifl, group, groupedServices, serviceRefs);
            if (LOG.isLoggable(Level.FINE)) {
                debugLog("Group " + group);
                debugLog("Grouped Services " + groupedServices);
                debugLog("Calls  " + serviceRefs);
            }
            
            Map<String, Properties> groupedServicesConfigMap = createServiceConfigMap(allConfigMap, groupedServices);
            Map<String, Properties> calledServicesConfigMap = createServiceConfigMap(allConfigMap,serviceRefs);
            if (LOG.isLoggable(Level.FINE)) {
                debugLog("Generating service artifacts for group " + group);
                debugLog("Provider config Map " + groupedServicesConfigMap);
                debugLog("Consumers config Map " + calledServicesConfigMap);
            }
            generateServiceArtifacts(prjModel, ifl, group, groupedServicesConfigMap, calledServicesConfigMap);
        }
    }

    private void findServicesInServiceGroup(IFLModel ifl, String group, Set<String> groupedServices, Set<String> serviceRefs) {
        groupedServices.clear();
        serviceRefs.clear();
        // build grouped services and services references sets and then their config maps.
        serviceRefs.addAll(ifl.getServiceReferencesInServiceGroup(group));
        // remove the service references (services after "calls") from the list of all services (including service refs)
        // to get the grouped services for which artifacts need to be generated.
        groupedServices.addAll(ifl.getServicesInServiceGroup(group));
        groupedServices.removeAll(serviceRefs);
    }

    private Map<String, Properties> createServiceConfigMap(Map<String, Properties> allConfigMap, Set<String> services) throws IOException {
        Map<String, Properties> serviceConfigMap = new HashMap<String, Properties>();
        if (services == null) {
            throw new IOException("Cannot create service config map from Null services list");
        }
        for (String service : services) {
            Properties configProps = allConfigMap.get(service);
            if (configProps == null) {
                throw new IOException("Service configuration not found for service " + service);
            }
            serviceConfigMap.put(service, configProps);
        }
        return serviceConfigMap;
    }

    /**
     * 
     * @param prjModel
     * @param ifl
     * @param serviceName
     * @param serviceConfig
     * @throws IOException
     */
    public void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) throws IOException {
        // find the group this service belongs and then invoke appropriate (provider or consumer) service artifacts generation method.
        String groupName = ifl.getGroupForService(serviceName);
        if (groupName == null || groupName.length() == 0 ) {
            // no group. assume serviceName same as group name and generate the service artifacts.
            generateServiceArtifacts(prjModel, ifl, serviceName, serviceName, serviceConfig);
            return;
        }
        Set<String> groupedServices = new TreeSet<String>();
        Set<String> serviceRefs = new TreeSet<String>();
        findServicesInServiceGroup(ifl, groupName, groupedServices, serviceRefs);

        if (groupedServices.contains(serviceName)) {
            generateServiceArtifacts(prjModel, ifl, groupName, serviceName, serviceConfig);
        } else {
            if (serviceRefs.contains(serviceName)) {
                generateServiceReferenceArtifacts(prjModel, ifl, groupName, serviceName, serviceConfig);
            } else {
               // just generate the service artifacts assuming the group name same as service name.
               generateServiceArtifacts(prjModel, ifl, serviceName, serviceName, serviceConfig);
            }
        }
    }
     /**
     * invoked from generateServiceArtifacts for a service. This method should generate the service artifacts required
     * for implementing the service.
     *
     * @param prjModel
     * @param ifl
     * @param groupName
     * @param serviceName
     * @param serviceConfig
     * @throws IOException
     */
    protected void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, String serviceName, Properties serviceConfig) throws IOException {
        debugLog("Generating service artifacts for group: " + groupName + " Service: " + serviceName);
        // build the final configuration.
        ServiceConfigurationHelper sc = getServiceConfigurationHelper();
        Properties finalConfig = sc.createConfiguration(prjModel, ifl, serviceName, serviceConfig);

        //TODO: remove this code when the archetype support is removed completely.
        List<Template> templates = this.getServiceDescriptor().getTemplates();
        URL archtypeURL = this.getArchetypeURL();
        if ((templates == null || templates.size() == 0) && archtypeURL != null) {
            // this is a wrapper descriptor for the archetype. So, generate the artifacts from archetype.
            debugLog("#### No artifacts templates in service descriptor. delegating artifact generation  to archetype... for service type: " + this.getServiceType());
            generateServiceArtifactsFromArchetype(prjModel, ifl, serviceName, finalConfig);
        } else {
            generateServiceArtifactsFromDescriptor(prjModel, ifl, serviceName, finalConfig);
        }
    }
    /**
     * invoked from generateServiceArtifacts for a service. This method should generate the service artifacts required
     * to link the service defined in the service group's calls list (java "pojogroup" ["p1", "p2"] calls <b>"c1", "c2"</b>)
     * to the actual provider service.
     * <p>
     * The default implemenation does nothing. Any component which need to generate service reference related artifacs should override
     * this method and generate required artifacts for that component. For example, a bpel service type may generate the partner links
     * and a java(pojo) service type may generate the consumes annotations.
     * @param prjModel
     * @param ifl
     * @param groupName
     * @param serviceName
     * @param serviceConfig
     * @throws IOException
     */
    protected void generateServiceReferenceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, String serviceName, Properties serviceConfig) throws IOException {
        //NOOP. extended classes may override it to create consumer service artifacts.
        debugLog("NOOP impl for Generating Service Reference Artifacts for  " + serviceName);
    }
    /**
     *
     * @param prjModel
     * @param ifl
     * @param groupName
     * @param groupedServicesConfigMap
     * @param calledServicesConfigMap
     * @throws IOException
     */
    public void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, Map<String, Properties> groupedServicesConfigMap, Map<String, Properties> calledServicesConfigMap) throws IOException {
        // throw new UnsupportedOperationException("Not supported yet.");
        debugLog("Generating service Artifacts for Grouped Services.... " + groupName);

        generateServiceGroupServiceArtifacts(prjModel, ifl, groupName, groupedServicesConfigMap);

        generateServiceGroupServiceReferenceArtifacts(prjModel, ifl, groupName, calledServicesConfigMap);

        generateServiceGroupCommonArtifacts(prjModel, ifl, groupName);
    }
    /**
     * invoked from generateServiceArtifacts for grouped services. This method should generate the service artifacts required
     * for the services in the group list (java "pojogroup" [<b>"p1", "p2"</b>] calls "c1", "c2") of the grouped services.
     * <p>
     * The default implemenation invokes generate  service artifacts for each service in the group list.
     *
     * @param prjModel
     * @param ifl
     * @param groupName
     * @param groupedServicesConfigMap
     * @throws IOException
     */
    protected void generateServiceGroupServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, Map<String, Properties> groupedServicesConfigMap) throws IOException {
        debugLog("Generating Artifacts for Grouped Services.... " + groupName);
        for ( String serviceName : groupedServicesConfigMap.keySet() ) {
            Properties serviceConfig = groupedServicesConfigMap.get(serviceName);
            if (serviceConfig == null ) {
                debugLog("No service configuration found in config map for service. using empty config properties " + serviceName);
                serviceConfig = new Properties();
            }
            generateServiceArtifacts(prjModel, ifl, groupName, serviceName, serviceConfig);
        }
    }

    /**
     * invoked from generateServiceArtifacts for grouped services. This method should generate the service artifacts required
     * for the services in the calls list (java "pojogroup" ["p1", "p2"] calls <b>"c1", "c2"</b>) of the grouped services.
     * <p>
     * The default implemenation inovkes generate service reference artifacts for each service in the calls list
     *
     * @param prjModel
     * @param ifl
     * @param groupName
     * @param calledServicesConfigMap
     * @throws IOException
     */
    protected void generateServiceGroupServiceReferenceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, Map<String, Properties> calledServicesConfigMap) throws IOException {
        //NOOP. extended classes may override it to create group consumer artifacts.
        debugLog("Generating  Artifacts for Grouped Service References.... " + groupName);
        for ( String serviceName : calledServicesConfigMap.keySet() ) {
            Properties serviceConfig = calledServicesConfigMap.get(serviceName);
            if (serviceConfig == null ) {
                debugLog("No service configuration found in config map for service. using empty config properties " + serviceName);
                serviceConfig = new Properties();
            }
            generateServiceReferenceArtifacts(prjModel, ifl, groupName, serviceName, serviceConfig);
        }
    }

    /**
     * implementation of this method should produce the common artifacts related to the group. 
     * This method is called fromt the generateServiceArtifacts for group. Default implemenation does nothing. Any service type
     * which extendeds the default implementation and has a requirement for a group common artifacts can override this method 
     * and implement generation of the group common artifacts. 
     * 
     * @param prjModel
     * @param ifl
     * @param groupName
     * @throws IOException
     */
    protected void generateServiceGroupCommonArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName)  throws IOException {
        //NOOP. extended classes may override it to create group common artifacts.
    }

    /**
     * generates the manifest if not exists. includes the old manifest entries
     * for component name (not needed with new spis) for backward compatibility
     *
     * @param resourcesDir resources directory of the service type. under withi
     * META-INF/MANIFEST.MF file will be created.
     * @return manifest file created or existing one.
     */
    protected final File generateServiceUnitManifest(File resourcesDir) {
        File metainfDir = new File(resourcesDir, "META-INF");
        metainfDir.mkdirs();
        File manifestFile = new File(metainfDir, "MANIFEST.MF");
        if (manifestFile.exists()) {
            return manifestFile; // no need to update it.
        }
        StringWriter buf = new StringWriter();
        PrintWriter out = new PrintWriter(buf);
        out.println("Bundle-Version: 1.0");
        out.println("Bundle-ManifestVersion: 2");
        out.println("Bundle-SymbolicName: ${artifactId}-su-file");
        out.println("Bundle-Name: ${name} - " + this.getServiceType() + " Service Unit");
        String componentName = "UNKNOWN";
        componentName = this.getServiceDescriptor().getComponentName();
        out.println("Component-Name: " + componentName);
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(buf.getBuffer().toString().getBytes());
            Utils.copy(in, manifestFile);
        } catch (Exception ex) {
            LOG.log(Level.FINE, ex.getMessage(), ex);
        }
        return manifestFile;
    }

    /**
     * generates the service resource files.
     *
     * @param prjModel service project model
     * @param ifl ifl model
     * @param serviceName service name for which the configuration is generated
     * @param serviceConfig  properties that contains the merged configuration
     * (properties from descriptor + existing properties from service.properties + user input) +
     * project model properties + standard configuration properties from the ifl usage of the service etc.
     *
     * @return generated files list.
     */
    protected List<File> generateServiceResources(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) {
        List<File> generatedFiles = new ArrayList<File>();
        List<Template> templates = this.getServiceDescriptor().getTemplates();
        File serviceResourceDir = prjModel.getResourcesDir(serviceName);
        for (Template template : templates) {
            String path = template.getAlias();
            if (path.startsWith(ServiceProjectModel.DEF_RESOURCES_DIR)) {
                path = path.substring(ServiceProjectModel.DEF_RESOURCES_DIR.length() + 1);
                File file = new File(serviceResourceDir, path);
                try {
                    createFileFromTemplate(template, file, serviceConfig);
                    generatedFiles.add(file);
                } catch (IOException ex) {
                    LOG.log(Level.FINE, "Unable to create file from template " + file.getAbsolutePath(), ex);
                }
            }
        }
        return generatedFiles;
    }

    /**
     * generates the service source files.
     *
     * @param prjModel service project model
     * @param ifl ifl model
     * @param serviceName service name for which the configuration is generated
     * @param serviceConfig  properties that contains the merged configuration
     * (properties from descriptor + existing properties from service.properties + user input) +
     * project model properties + standard configuration properties from the ifl usage of the service etc.
     *
     * @return generated files list.
     */
    protected List<File> generateServiceSources(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) {
        List<File> generatedFiles = new ArrayList<File>();
        List<Template> templates = this.getServiceDescriptor().getTemplates();
        File srcDir = prjModel.getSourceDir();
        String srcPath = srcDir.getAbsolutePath().substring(prjModel.getBaseDir().getAbsolutePath().length() + 1);
        srcPath = srcPath.replace("\\", "/");
        File serviceSrcDir = prjModel.getSourceDir(serviceName);
        for (Template template : templates) {
            String path = template.getAlias();
            if (path.startsWith(srcPath)) {
                path = path.substring(srcPath.length() + 1);
                File file = new File(serviceSrcDir, path);
                try {
                    createFileFromTemplate(template, file, serviceConfig);
                    generatedFiles.add(file);
                } catch (IOException ex) {
                    LOG.log(Level.FINE, "Unable to create file from template " + file.getAbsolutePath(), ex);
                }
            }
        }
        return generatedFiles;
    }

    /**
     * generates the service artifacts.
     * @param prjModel service project model
     * @param ifl ifl model
     * @param serviceName service name for which the configuration is generated
     * @param serviceConfig  properties that contains the merged configuration
     * (properties from descriptor + existing properties from service.properties + user input) +
     * project model properties + standard configuration properties from the ifl usage of the service etc.
     * @throws java.io.IOException
     */
    private void generateServiceArtifactsFromDescriptor(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) throws IOException {

        LOG.fine("#### Generating service artifacts using service descriptor... for service type: " + this.getServiceType());
        // now generate resources from templates
        LOG.fine("Generating resources files ...");
        generateServiceResources(prjModel, ifl, serviceName, serviceConfig);
        // now generate source files from templates
        LOG.fine("Generating source files ...");
        generateServiceSources(prjModel, ifl, serviceName, serviceConfig);
        // now generate service unit manifest if needed.
        LOG.fine("Generating service unit manifest...");
        generateServiceUnitManifest(prjModel.getResourcesDir());
    }

    /**
     * @deprecated  this method should be removed after we moved everything from archetype to service descriptor.
     *
     * @param prjModel
     * @param serviceName
     * @param serviceConfig
     * @throws java.io.IOException
     */
    private void generateServiceArtifactsFromArchetype(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) throws IOException {
        // no template processing if it is done from the archetypes - current implemenation.
        // Should change in M9 when components are ready with the wsdl templates for code generation.
        LOG.fine("#### Generating service artifacts using service archetype... for service type: " + this.getServiceType());
        InputStream urlIS = null;
        ZipInputStream zipIS = null;
        String serviceType = this.getServiceType();
        File baseDirectory = prjModel.getBaseDir();
        // temporary hack for file prototype.
        FileServiceConfigurationHelper fileConfigHelper = new FileServiceConfigurationHelper();
        try {
            urlIS = this.getArchetypeURL().openStream();
            zipIS = new ZipInputStream(urlIS);
            ZipEntry entry = null;
            while ((entry = zipIS.getNextEntry()) != null) {
                // We can't extract the archetype as is because it has a funky
                // structure.  This code massages the directory naming to match
                // the current app layout.
                if (entry.getName().startsWith("archetype-resources/src/main") &&
                        !entry.isDirectory()) {

                    // need to mangle the name a bit - this is so hacky
                    String name = entry.getName();
                    name = name.replace("archetype-resources/", "");

                    // take care of the static Fuji directories
                    if (name.startsWith("src/main/config")) {
                        name = name.replace("src/main/config",
                                "src/main/config/" + serviceType + "/" + serviceName);
                    } else if (name.startsWith("src/main/resources/META-INF")) {
                        name = name.replace("src/main/resources",
                                "src/main/resources/" + serviceType);
                    } else if (name.startsWith("src/main/resources")) {
                        name = name.replace("src/main/resources",
                                "src/main/resources/" + serviceType + "/" + serviceName);
                    } else if (name.startsWith("src/main/" + serviceType)) {
                        name = name.replace("src/main/" + serviceType,
                                "src/main/" + serviceType + "/" + serviceName);
                    }

                    final File file = new File(baseDirectory, name);

                    // New generation, need to create parent dir
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }
                    // for file service prototyping only
                    // now added ftp service also
                    if ((serviceType.equals("file") || serviceType.equals("ftp")) && name.endsWith("service.properties")) {
                        boolean fileConfigGenerated =
                                fileConfigHelper.generateServiceConfigFile(
                                serviceType, serviceName, serviceConfig, ifl, file, name, zipIS, entry.getSize());
                    } else {
                        LOG.fine("## Creating service artifact " + file.getAbsolutePath());
                        Utils.copy(zipIS, file, entry.getSize());
                    }
                }
            }
        } finally {

            try {
                if (urlIS != null) {
                    urlIS.close();
                }
            } catch (IOException e) {
            }
        }

    }

    protected void createFileFromTemplate(Template template, File dest, Properties data) throws IOException {
        dest.getParentFile().mkdirs();
        if (dest.exists() && !template.isOverwrite()) {
            LOG.fine("File existing. Skiping the create file from template " + dest.getAbsolutePath());
            return;
        }
        if (template.isFiltered()) {
            // process with template engine
            try {
                TemplateEngine.getDefault().processTemplate(template.getURL(), dest, data);
            } catch (TemplateException ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
                throw new IOException("Error creating the file from template: " + ex.getMessage());
            }
        } else {
            // copy file.
            Utils.copy(template.getURL(), dest);
        }
    }

    protected void createFilesFromTemplates(Map<File, Template> templates, Properties data) throws IOException {
        for (File file : templates.keySet()) {
            Template template = templates.get(file);
            createFileFromTemplate(template, file, data);
        }
    }

    private void dumpProperties(String msg, Properties props) {
        if (LOG.isLoggable(Level.FINE)) {
            if (props == null) {
                LOG.fine("---Null Props for -- " + msg);
                return;
            }
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            props.list(out);
            LOG.fine("----- " + msg);
            LOG.fine(writer.getBuffer().toString());
        }
    }

    /**
     * returns the list of service names by looking up the list of child directories
     * that represent the service names for this service type.
     * @param dir
     * @param serviceType
     * @return
     */
    protected List<String> listSubDirectories(File parent) {
        String[] excludes = {"CVS", ".svn"}; //TODO: add it to the utils class.
        final List<String> excludesList = Arrays.asList(excludes);
        File[] files = parent.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                if (pathname.isDirectory()) {
                    String name = pathname.getName();
                    // service dir should not have . prefix or .suffix
                    if (name.contains(".") || excludesList.contains(name)) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        });

        if (files == null) {
            files = new File[0];
        }
        List<String> existing = new ArrayList<String>();
        for (File file : files) {
            existing.add(file.getName());
        }
        return existing;
    }

    /**
     * recursively deletes all the child directories and then tries to deletes this dir.
     * if the directory or its subdirectories has SCM (CVS or svn) files, it will not delete the direcotry
     * but will delete all the files in the directory and subdirectories.
     *
     * @param dir
     */
    protected boolean deleteDir(File dir) {
        boolean deleted = false;
        if (!dir.exists()) {
            LOG.fine("Trying to delete the directory that is not existing " + dir.getPath());
            return deleted;
        }
        String[] excludes = {"CVS", ".svn"}; //TODO: add it to the utils class.
        final List<String> excludesList = Arrays.asList(excludes);
        File[] files = dir.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                if (pathname.isDirectory()) {
                    String name = pathname.getName();
                    // service dir should not have . prefix or .suffix
                    if (excludesList.contains(name)) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        });
        if (files == null) {
            files = new File[0];
        }

        for (File file : files) {
            if (file.isDirectory()) {
                deleteDir(file);
            } else {
                if (!file.delete()) {
                    LOG.fine("Cannot delete the file " + file.getAbsolutePath());
                }
            }
        }
        deleted = dir.delete();
        if (!deleted) {
            LOG.fine("Cannot delete the directory " + dir.getAbsolutePath());
        }
        return deleted;
    }

    /**
     * remove the child dir and optionally removes the parent dir if it is empty.
     * @param parent
     * @param child
     * @param removeParent
     * @return
     */
    protected boolean deleteDir(File parent, String child, boolean removeParent) {
        boolean deleted = false;
        File dir = new File(parent, child);
        if (!dir.exists()) {
            return deleted;
        }
        deleted = deleteDir(dir);
        if (deleted) {
            String[] children = parent.list();
            if (children == null || children.length == 0) {
                // empty. so. delete this also.
                if (removeParent) {
                    deleted = parent.delete();
                }
            }
        }
        return deleted;
    }

    public void deleteUnusedArtifacts(ServiceProjectModel prjModel, IFLModel ifl) throws IOException {
        List<String> usedServices = ifl.getServicesForType(this.getServiceType());
        // list all config, res, src directories for the service type.
        List<String> existingConfig = listSubDirectories(prjModel.getConfigDir());
        List<String> existingRes = listSubDirectories(prjModel.getResourcesDir());
        List<String> existingSrc = listSubDirectories(prjModel.getSourceDir());

        Set<String> unusedServices = new TreeSet<String>();
        // combine them so that the set is a union of all so that we don't miss a service which has only source or only config or only resource
        unusedServices.addAll(existingConfig);
        unusedServices.addAll(existingRes);
        unusedServices.addAll(existingSrc);
        // remove the usedServices from the set.
        unusedServices.removeAll(usedServices);
        // now we have the list of unused services for this type. 
        for (String service : unusedServices) {
            Properties serviceConfig = new Properties();
            //TODO: populate serviceConfig with required properties if ncessary.
            deleteServiceArtifacts(prjModel, service, serviceConfig);
        }

    }

    public void deleteServiceArtifacts(ServiceProjectModel prjModel, String serviceName, Properties serviceConfig) throws IOException {
        File configDir = prjModel.getConfigDir();
        File resourceDir = prjModel.getResourcesDir();
        File srcDir = prjModel.getSourceDir();
        deleteDir(configDir, serviceName, true);
        deleteDir(resourceDir, serviceName, true);
        deleteDir(srcDir, serviceName, true);
    }
}
