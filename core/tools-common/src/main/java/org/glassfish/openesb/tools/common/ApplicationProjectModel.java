/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common;

import java.io.File;
import java.util.Properties;

/**
 * This class implements the integration app's project model for the default
 * filesytem layout.
 *
 * @see FujiProjectModel
 * 
 * @author chikkala
 */
public class ApplicationProjectModel extends FujiProjectModel {


    public ApplicationProjectModel(String baseDir, Properties prjProps) {
        super();
        initModel(baseDir, prjProps);
    }

    private void initModel(String baseDirPath, Properties prjProps) {

        String applicationId = prjProps.getProperty(PROP_APPLICATION_ID, DEF_APPLICATION_ID);
        String artifactId = prjProps.getProperty(PROP_ARTIFACT_ID, applicationId);

        String name = prjProps.getProperty(PROP_NAME, applicationId);
        String displayName = prjProps.getProperty(PROP_DISPLAY_NAME,name);
        String description = prjProps.getProperty(PROP_DESCRIPTION,name);

        for (Object key : prjProps.keySet()) {
            this.setProperty((String)key, prjProps.getProperty((String)key));
        }

        this.setProperty(PROP_PROJECT_TYPE, APPLICATION_PROJECT_TYPE);
        this.setProperty(PROP_APPLICATION_ID, applicationId);
        this.setProperty(PROP_ARTIFACT_ID, artifactId);
        
        this.setProperty(PROP_NAME, name);
        this.setProperty(PROP_DISPLAY_NAME, displayName);
        this.setProperty(PROP_DESCRIPTION, description);

        File baseDir = new File(baseDirPath);
        this.setBaseDir(baseDir);
        this.setSourceDir(new File(baseDir, DEF_SRC_DIR));
        this.setConfigDir(new File(baseDir, DEF_CONFIG_DIR));
        this.setResourcesDir(new File(baseDir, DEF_RESOURCES_DIR));
        this.setBuildDir(new File(baseDir, DEF_BUILD_DIR));
        this.setOutputDir(new File(baseDir, DEF_OUTPUT_DIR));

    }

}
