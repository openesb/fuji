/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import java.net.URL;
import java.util.List;
import java.util.Map;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.HelpContext;
import org.glassfish.openesb.tools.common.descriptor.Template;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 * This is the accessor interface for the data in the service descriptor xml of
 * a service type.
 *<p>
 * This interface is not implemented by the service type providers, but may be
 * used by the service tyep SPI implementation to access the resources and the
 * metadata related to the service type from the service descriptor bundle.
 * <p>
 * The tools common code will provide the implementation of this interface and
 * the APIs to lookup this interface per service type.
 * <p>
 * The service type descriptor xml file provided by the service type providers
 * in the service descriptor bundle will provide the data required to implement
 * this interface per service type.
 *
 * @author chikkala
 */
public interface ServiceDescriptor {
    
    /**
     * Enumeration, listing all icons types that a service type descriptor should
     * support.
     *
     */
    public static enum IconType {

        /** default type*/
        ICON_DEFAULT(""),
        /** 8x8 pixels. for badges*/
        ICON_8("8"),
        /** 16x16 pixels. */
        ICON_16("16"),
        /** 24x24 pixels. */
        ICON_24("23"),
        /** 32x32 pixels. */
        ICON_32("32");
        private String type;

        IconType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    /**
     *
     * @return name of the service type. e.g. for foo service type, return "foo"
     */
    String getServiceType();

    /**
     *
     * @return jbi component name to which the service of this service type will be deployed.
     */
    String getComponentName();

    /**
     * whether this descriptor is for binding component.
     * @return
     */
    boolean isBindingComponent();

    /**
     * locale specific display name of the service type defined in the descriptor
     * @return String containing the display name.
     */
    String getDisplayName();

    /**
     * locale specific short description of the service type defined in the descriptor
     * @return String containing the short descriptor.
     */
    String getShortDescription();

    /**
     * locale specific long description of the service type defined in the descriptor
     * @return String containing the long descriptor.
     */
    String getLongDescription();

    /**
     * help context. if not defined in the decriptor, a default help content will
     * be returns.
     * @return HelpContext object
     */
    HelpContext getHelpContext();

    /**
     * return the specific icon type defined in the descriptor. If the icon type
     * is defined multiple times, the first defined on will be returned.
     * @param type  type of icon to find
     * @return URL for the icon
     */
    URL getIcon(IconType type);

    /**
     * return all the icons defined in the descriptor
     *
     * @return List of icon URLs
     */
    List<URL> getIcons();

    /**
     * returns the list of configurations defined in the descriptor.
     * @return List of Configuration objects.
     */
    List<Configuration> getConfigurations();

    /**
     * return the default configuration object
     * @return Configuration object.
     */
    Configuration getDefaultConfiguration();

    /**
     * return  all templates defined in the descriptor
     * @return List of Template objects.
     */
    List<Template> getTemplates();

    /**
     * return the Map of web resources url mapping with "alias" attribute as a key
     * @return Map of the resource URL with alias as key.
     */
    Map<String, URL> getWebResources();

    /**
     * loads the <code>ServiceGenerator</code> implemenation defined in the descriptor
     * @return ServiceGenerator
     */
    ServiceGenerator getServiceGenerator();

    /**
     * loads the <code>ServiceBuilder</code> implemenation defined in the descriptor
     * @return ServiceBuilder
     */
    ServiceBuilder getServiceBuilder();

    /**
     * loads the <code>ServicePackager</code> implemenation defined in the descriptor
     * @return ServicePackager
     */
    ServicePackager getServicePackager();
}
