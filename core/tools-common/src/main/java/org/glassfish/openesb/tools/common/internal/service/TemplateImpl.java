/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.tools.common.internal.service;

import java.net.URL;
import org.glassfish.openesb.tools.common.descriptor.Template;


/**
 *
 * @author chikkala
 */
public class TemplateImpl implements Template {
    private String mAlias;
    private URL mUrl;
    private boolean mFiltered;
    private boolean mOverwrite;

    protected TemplateImpl(String alias, URL url, boolean filtered, boolean overwrite) {
        this.mAlias = alias;
        this.mUrl = url;
        this.mFiltered = filtered;
        this.mOverwrite = overwrite;
    }
    public boolean isOverwrite() {
        return this.mOverwrite;
    }
    public boolean isFiltered() {
        return this.mFiltered;
    }

    public String getAlias() {
        return this.mAlias;
    }

    public URL getURL() {
        return this.mUrl;
    }

}
