/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.service;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.FujiProjectModel;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.jbi.descriptor.Jbi;
import org.glassfish.openesb.tools.common.jbi.descriptor.Provides;
import org.glassfish.openesb.tools.common.jbi.descriptor.ServiceEntry;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 * Default implemenatation of the service packager. This will be used when
 * there is no service type specific service packager is provided in that
 * service type descriptor bundle.
 *
 * @author chikkala
 */
public class DefaultServicePackager extends AbstractSPIProvider implements ServicePackager {

    private static final Logger LOG = Logger.getLogger(DefaultServicePackager.class.getName());

    public DefaultServicePackager(ServiceDescriptor sd) {
        super(sd);
        // LOG.setLevel(Level.FINE);
    }

    public File packageServiceUnit(ServiceProjectModel prjModel, List<String> services, IFLModel ifl) throws IOException {

        LOG.fine("### Packaging service unit for " + this.getServiceType());
        List<String> consumedServices = new ArrayList<String>();
        List<String> providedServices = new ArrayList<String>();
        Map<String,String> providedServicesNS = new HashMap();
        Map<String,String> consumedServicesNS = new HashMap();

        for (String serviceName : services) {
            if (ifl.isConsumed(serviceName)) {
                consumedServices.add(serviceName);
                consumedServicesNS.put(serviceName, ifl.getNamespace(serviceName));
            } else if (ifl.isProvided(serviceName)) {
                providedServices.add(serviceName);
                providedServicesNS.put(serviceName, ifl.getNamespace(serviceName));
            } else {
                LOG.warning("Service " + serviceName + " Is not configured as either Provider or Consumer in service unit descriptor");
            }
        }
        LOG.fine("Creating service unit descriptor for " + this.getServiceType());
        // create service unit descriptor
        Jbi jbi = createServiceUnitDescriptor(prjModel, this.getServiceType(),
                consumedServices, consumedServicesNS, providedServices, providedServicesNS);
        // save service unit descritpro in META-INF/jbi.xml
        saveServiceUnitDescriptor(jbi, prjModel);
        //create the service unit archive.
        // callback for additional pre packaging processing.
        beforeServicePackaging(prjModel, services);
        // create archive
        File archive = createServiceUnitArchive(prjModel);
        return archive;
    }

    public void beforeServicePackaging(ServiceProjectModel prjModel, List<String> services) throws IOException {
        // by default, nothing to do.
    }

    public void beforeApplicationPackaging(ServiceProjectModel prjModel, List<String> services, ApplicationProjectModel appPrjModel) throws IOException {
        // by default, nothing to do.
    }

    protected Jbi createServiceUnitDescriptor(ServiceProjectModel prjModel,
            String serviceType, List<String> consumedServices, Map<String,String> consumedNS,
            List<String> providedServices, Map<String,String> providedNS) {
        Jbi jbi = Jbi.newServiceUnitDescriptor();
        jbi.getServices().setBindingComponent(this.getServiceDescriptor().isBindingComponent());
        for (String serviceName : consumedServices) {
            ServiceEntry consumes = jbi.getServices().addConsumes();

            consumes.setServiceName(
                    prjModel.getConsumerServiceQName(serviceType, serviceName, consumedNS));
            consumes.setInterfaceName(
                    prjModel.getInterfaceQName());
            consumes.setEndpointName(
                    prjModel.getEndpointName(serviceName));
        }

        for (String serviceName : providedServices) {
            Provides provides = jbi.getServices().addProvides();

            provides.setServiceName(
                    prjModel.getProviderServiceQName(serviceName, providedNS));
            provides.setInterfaceName(
                    prjModel.getInterfaceQName());
            provides.setEndpointName(
                    prjModel.getEndpointName(serviceName));
        }

        return jbi;
    }

    protected void saveServiceUnitDescriptor(Jbi jbi, ServiceProjectModel prjModel) throws IOException {
        File metaInfDir = new File(prjModel.getOutputDir(), "META-INF");
        if (!metaInfDir.exists()) {
            metaInfDir.mkdirs();
        }
        File jbiXmlFile = new File(metaInfDir, "jbi.xml");
        jbi.writeTo(jbiXmlFile);
    }

    protected File createServiceUnitArchive(ServiceProjectModel prjModel) throws IOException {

        File suPackageOutDir = new File(prjModel.getBaseDir(), FujiProjectModel.DEF_SU_PACKAGE_OUTPUT_DIR);
        suPackageOutDir.mkdirs();
        File archive = new File(suPackageOutDir, this.getServiceType() + ".jar");

        LOG.fine("Creating service unit jar file " + archive.getAbsolutePath() + " For " + this.getServiceType());

        Utils.Jar jar = new Utils.Jar(archive);
        jar.includeFile(prjModel.getOutputDir(), false);
        jar.create();

        return archive;
    }
}
