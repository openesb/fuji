/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import javax.xml.namespace.QName;

/**
 * This class provides the mapping of the filesystem directory structure of the
 * fuji integration app project and the service type related directory structures
 * to a standard project directories.
 *
 * The main application and service type providers will use this interface to get
 * to the filesytem layout for generate, access, build and package the artifacts
 * related to the integration app and it's sub components (service unit per service
 * type)
 *
 * This model also will have a container for the property variables which can be
 * used in various stages of the app/servicetype implemenation. For example, the
 * properties in the model can be used in the velocity templates that produce the
 * app or service type artifacts in create or build phases. Project model should
 * have the standard properties mentioned below in addition to any properties
 * that are specific to the app or service type.
 *
 * @see ApplicationProjectModel
 * @see ServiceTypeProjectModel
 * 
 * @author chikkala
 */
public abstract class FujiProjectModel {
    //
    // standard properties that the projet model should contain depending on
    // the project type.
    //

    /**
     * Property name for the project type property. value of the property
     * should be "Application" or "ServiceUnit"
     */
    public static final String PROP_PROJECT_TYPE = "projectType";
    /**
     * Property name for the application id. This property might be used as
     */
    public static final String PROP_APPLICATION_ID = "applicationId";
    /**
     * name for the project. default is DEF_APPLICATION_ID
     */
    public static final String PROP_ARTIFACT_ID = "artifactId";
    /**
     * name for the project. default is DEF_APPLICATION_ID
     */
    public static final String PROP_NAME = "name";
    /**
     * display name, default is same as NAME
     */
    public static final String PROP_DISPLAY_NAME = "displayName";
    /**
     * description, default is same as NAME
     */
    public static final String PROP_DESCRIPTION = "decription";
    //
    // properties needed to build complete service artifacts
    // TODO: these properties should go into a separate class like
    // service configuration
    //
    /**
     * service type property. contains the service type this project would
     * handle.
     */
    public static final String PROP_SERVICE_TYPE = "serviceType";
    /**
     * service name property. contains the service name defined in the ifl.
     * //TODO: this name should follow the statndard property nameing conversion
     * we use (i.e. dot separted names like service.name
     * Since current code uses it. I am not changing the name.
     */
    public static final String PROP_SERVICE_NAME = "serviceName";
    /**
     * consumer endpoint service name. holds the value of the JBI service name that
     * is derived from the ifl service name and routing information.
     * wsld file template/jbi descriptor configuring the consumed service
     * elements use it's value.
     */
    public static final String PROP_CONSUMED_SERVICE = "consumedService";
    /**
     * provided endpoint service name. holds the value of the JBI service name that
     * is derived from the ifl service name and routing information.
     * wsld file template/jbi descriptor configuring the provided service
     * elements use it's value.
     */
    public static final String PROP_PROVIDED_SERVICE = "providedService";
    //
    // project type constants
    //
    public static final String APPLICATION_PROJECT_TYPE = "Application";
    public static final String SERVICE_UNIT_PROJECT_TYPE = "ServiceUnit";
    //
    // various default values
    //
    /**
     * default application id
     */
    public static final String DEF_APPLICATION_ID = "CompositeApp";
    /**
     * default source root base
     */
    public static final String DEF_SRC_DIR_BASE = "src/main";
    /**
     * default java source root
     */
    public static final String DEF_SRC_DIR = "src/main/java";
    /**
     * default configuration root
     */
    public static final String DEF_CONFIG_DIR = "src/main/config";
    /**
     * default resources root.
     */
    public static final String DEF_RESOURCES_DIR = "src/main/resources";
    /**
     * default build directory
     */
    public static final String DEF_BACKUP_DIR = "bak";
    /**
     * default build directory
     */
    public static final String DEF_BUILD_DIR = "target";
    /**
     * default output directory
     */
    public static final String DEF_OUTPUT_DIR = "target/classes";
    /**
     * default output directory
     */
    public static final String DEF_SU_PACKAGE_OUTPUT_DIR = "target/service-units";
    
    /**
     * directory name where the common wsld placed. it will be under resources dir
     */
    public static final String DEF_COMMON_WSDL_DIR = "src/main/resources/wsdl";
    /**
     * directory name where the external wsdls are placed. it will be under resources dir
     */
    public static final String DEF_EXTERNAL_WSDL_DIR = "src/main/resources/wsdl/external";
    /**
     * IFL directory
     */
    public static final String IFL_SRC_DIR = "src/main/ifl";

    /**
     * IFL main file
     */
    public static final String DEF_IFL_MAIN_FILE_PATH = "src/main/ifl/app.ifl";
    /**
     * common file name for the interface wsdl
     */
    public static final String COMMON_INTERFACE_WSDL = "interface.wsdl";
    /**
     * common file name for the binding wsdl
     */
    public static final String COMMON_BINDING_WSDL = "binding.wsdl";
    /**
     * common file name for the message xsd
     */
    public static final String COMMON_MESSAGE_XSD = "message.xsd";
    /**
     * application namespace prefix. common private namespace for the application
     * will be derived from this + applicationId
     */
    private static final String FUJI_APP_NAMESPACE_PREFIX = "http://fuji.dev.java.net/application/";
    //
    // private variables.
    //
    /**
     * properties container
     */
    private Properties mProps;
    /**
     * base directory
     */
    private File mBaseDir;
    /**
     * source directory
     */
    private File mSourceDir;
    /**
     * configuration directory
     */
    private File mConfigDir;
    /**
     * resource directory
     */
    private File mResourcesDir;
    /**
     * build directory
     */
    private File mBuildDir;
    /**
     * output directory
     */
    private File mOutputDir;

    /**
     * default constructor
     */
    public FujiProjectModel() {
        this.mProps = new Properties();
    }

    /**
     * @return project's base (root) directory
     */
    public final File getBaseDir() {
        return this.mBaseDir;
    }

    /**
     *
     * @return source root direcotry of the project
     */
    public final File getSourceDir() {
        return this.mSourceDir;
    }

    /**
     *
     * @return configuration root directory of the project
     */
    public final File getConfigDir() {
        return this.mConfigDir;
    }

    /**
     *
     * @return resource root directory of the project
     */
    public final File getResourcesDir() {
        return this.mResourcesDir;
    }

    /**
     *
     * @return build root directory of the project
     */
    public final File getBuildDir() {
        return this.mBuildDir;
    }

    /**
     *
     * @return output root directory of the project
     */
    public final File getOutputDir() {
        return this.mOutputDir;
    }

    /**
     * return the property value in the project model
     * @param name
     * @return
     */
    public final String getProperty(String name) {
        return this.mProps.getProperty(name);
    }

    /**
     * return the property value in the project model
     * @param name
     * @param defaultValue
     * @return
     */
    public final String getProperty(String name, String defaultValue) {
        return this.mProps.getProperty(name, defaultValue);
    }

    /**
     * returns the unmodifiable map of properties in this project model
     * @return
     */
    public final Map<Object, Object> getProperties() {
        Map<Object, Object> map = Collections.unmodifiableMap(this.mProps);
        return map;
    }

    public String getApplicationNamespace() {
        String applicationId = this.getProperty(PROP_APPLICATION_ID);
        String appNS = FUJI_APP_NAMESPACE_PREFIX + applicationId;
        return appNS;
    }
    /**
     * returns the application interface qname. 
     *
     * @return
     */
    public QName getInterfaceQName() {
        String applicationId = this.getProperty(PROP_APPLICATION_ID);
        String appNS = getApplicationNamespace();
        String interfaceName  = applicationId + "_interface";
        return new QName(appNS, interfaceName);
    }
    /**
     * returns qname in the application namesapce, given the local name
     * @param localPart local part of the qname.
     * @return
     */
    public QName getQName(String localPart) {
        String appNS = getApplicationNamespace();
        return new QName(appNS, localPart);

    }
    /**
     * returns provider service qname in the application namesapce.
     * @param iflServiceName serviceName defined in the IFL.
     * @return
     */
    public QName getProviderServiceQName(String iflServiceName, Map<String,String> providedNS) {
        String appNS = providedNS.get(iflServiceName);
        return new QName(appNS, iflServiceName);
    }
    /**
     * returns unique service qname in the application namesapce.
     * @param serviceType service type defined in the IFL
     * @param iflServiceName service name defined in the IFL
     * @return
     */
    public QName getConsumerServiceQName(String serviceType, String iflServiceName,
            Map<String,String> consumedNS) {
        return new QName(consumedNS.get(iflServiceName), serviceType + "_" + iflServiceName);
    }
    public String getConsumerService(String serviceType, String iflServiceName) {
        return serviceType + "_" + iflServiceName;
    }
    /**
     * returns the wsdl endpoint elements's name for a given service name defined in the IFL.
     * @param serviceName defined in the ifl
     * @return
     */
    public String getEndpointName(String iflServiceName) {
        return iflServiceName + "_endpoint";
    }

    protected final void setBaseDir(File baseDir) {
        this.mBaseDir = baseDir;
    }

    protected final void setSourceDir(File srcDir) {
        this.mSourceDir = srcDir;
    }

    protected final void setConfigDir(File configDir) {
        this.mConfigDir = configDir;
    }

    protected final void setResourcesDir(File resourceDir) {
        this.mResourcesDir = resourceDir;
    }

    protected final void setBuildDir(File buildDir) {
        this.mBuildDir = buildDir;
    }

    protected final void setOutputDir(File outputDir) {
        this.mOutputDir = outputDir;
    }

    protected final void setProperty(String name, String value) {
        this.mProps.setProperty(name, value);
    }

    protected final void setProperties(Map<Object, Object> props) {
        for (Object key : props.keySet()) {
            this.mProps.put(key, props.get(key));
        }
    }
}
