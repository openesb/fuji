/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.internal.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import org.glassfish.openesb.tools.common.descriptor.FeatureInfo;
import org.glassfish.openesb.tools.common.descriptor.HelpContext;
import org.glassfish.openesb.tools.common.internal.service.jaxb.DisplayName;
import org.glassfish.openesb.tools.common.internal.service.jaxb.LongDescription;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ObjectFactory;
import org.glassfish.openesb.tools.common.internal.service.jaxb.ShortDescription;

/**
 *
 * @author chikkala
 */
public class FeatureInfoImpl implements FeatureInfo {

    private static final Logger LOG = Logger.getLogger(FeatureInfoImpl.class.getName());

    private ResourceBundle mI18NBundle;
    private ClassLoader mClassloader;
    private DisplayName mDispName;
    private ShortDescription mShortDesc;
    private LongDescription mLongDesc;

    public FeatureInfoImpl(ResourceBundle i18NBundle, ClassLoader classLoader, DisplayName dispName, ShortDescription shortDesc, LongDescription longDesc) {
        this.mI18NBundle = i18NBundle;
        this.mClassloader = classLoader;
        if (this.mClassloader == null) {
            this.mClassloader = this.getClass().getClassLoader();
        }
        this.mDispName = dispName;
        this.mShortDesc = shortDesc;
        this.mLongDesc = longDesc;
    }

    public ResourceBundle getResourceBundle() {
        return this.mI18NBundle;
    }

    public void setResourceBundle(ResourceBundle i18NBundle) {
        this.mI18NBundle = i18NBundle;
    }

    public void setResourceBundle(String baseName, Locale locale, ClassLoader loader) {
        setResourceBundle(ResourceBundle.getBundle(baseName, locale, loader));
    }

    public String getI18NValue(String key) {
        String value = null;
        if (this.mI18NBundle != null && key != null) {
            try {
                value = this.mI18NBundle.getString(key);
            } catch (Exception ex) {
                value = key;
                LOG.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        return value;
    }

    public String getDisplayName() {

        String dispName = null;
        if (this.mDispName != null) {
            String key = this.mDispName.getKey();
            if (key != null) {
                dispName = this.getI18NValue(key);
            } else {
                dispName = this.mDispName.getValue();
            }
        }
        return dispName;
    }

    public String getShortDescription() {
        String shortDesc = null;
        if (this.mShortDesc != null) {
            String key = this.mShortDesc.getKey();
            if (key != null) {
                shortDesc = this.getI18NValue(key);
            } else {
                shortDesc = this.mShortDesc.getValue();
            }
        }
        return shortDesc;
    }

    public String getLongDescription() {
        String longDesc = null;
        if (this.mLongDesc != null) {
            String key = this.mLongDesc.getKey();
            if (key != null) {
                longDesc = this.getI18NValue(key);
            } else {
                longDesc = this.mLongDesc.getValue();
            }
        }
        return longDesc;
    }

    public String getHelpID() {
        String helpID = null;
        if (this.mLongDesc != null) {
            helpID = this.mLongDesc.getHelpID();
        }
        return helpID;
    }

    public URL getHelpURL() {
        String helpURLValue = null;
        if (this.mLongDesc != null) {
            helpURLValue = this.mLongDesc.getUrl();
        }
        URL helpURL = null;
        if (helpURLValue != null) {
            helpURL = this.mClassloader.getResource(helpURLValue);
            if ( helpURL == null ) {
                try {
                    // try create a full url
                    helpURL = new URL(helpURLValue);
                } catch (Exception ex) {
                    LOG.log(Level.FINE, "Can not create a HelpURL for the feature description: " + ex.getMessage(), ex);
                }
            }
        }
        return helpURL;
    }

    public HelpContext getHelpContext() {
        return new HelpContext(getHelpID(), getHelpURL());
    }

    public static FeatureInfoImpl createFeatureInfoImpl(ResourceBundle i18NBundle, ClassLoader classLoader,
            String dispNameKey, String shortDescKey, String longDescKey) {
        FeatureInfoImpl impl = null;
        ObjectFactory factory = new ObjectFactory();
        DisplayName dispName = factory.createDisplayName();
        dispName.setKey(dispNameKey);
        ShortDescription shortDesc = factory.createShortDescription();
        shortDesc.setKey(shortDescKey);
        LongDescription longDesc = factory.createLongDescription();
        longDesc.setKey(longDescKey);
        impl = new FeatureInfoImpl(i18NBundle, classLoader, dispName, shortDesc, longDesc);
        return impl;
    }

    public static FeatureInfoImpl createFeatureInfoImpl(
            String dispNameValue, String shortDescValue, String longDescValue) {
        FeatureInfoImpl impl = null;
        ResourceBundle i18NBundle = null;
        ClassLoader classLoader = null;
        ObjectFactory factory = new ObjectFactory();
        DisplayName dispName = factory.createDisplayName();
        dispName.setValue(dispNameValue);
        ShortDescription shortDesc = factory.createShortDescription();
        shortDesc.setValue(shortDescValue);
        LongDescription longDesc = factory.createLongDescription();
        longDesc.setValue(longDescValue);
        impl = new FeatureInfoImpl(i18NBundle, classLoader, dispName, shortDesc, longDesc);
        return impl;
    }
}
