/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.internal.service;

import java.io.PrintStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 *
 * @author chikkala
 */
public class ServiceDescriptorImplTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ServiceDescriptorImplTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ServiceDescriptorImplTest.class);
    }

    private void printConfigurations(PrintStream out, ServiceDescriptor sd) throws Exception {
        List<Configuration> list = sd.getConfigurations();
        for ( Configuration conf : list) {
            out.println("+ Configuraiton: " + conf.getName());
            List<PropertyGroup> propGroupList = conf.getPropertyGroupList();
            for ( PropertyGroup group : propGroupList) {
                out.println("-+ Group Name: " + group.getName());
                out.println("--+ Position: " + group.getInfo().getPosition());
                out.println("--+ Display Name: " + group.getInfo().getDisplayName());
                out.println("--+ Properties: ");
                List<Property> props = group.getPropertyList();
                for ( Property prop : props ) {
                    out.println("---+ " + prop.getName() + "=" + prop.getValue());
                }
            }
        }
    }
    /**
     * Rigourous Test :-)
     */
    public void testServiceDescriptorImpl() throws Exception {
        String testSD = "test-service-descriptor.xml";
        ClassLoader sdCL = this.getClass().getClassLoader();
        URL sdURL = this.getClass().getResource(testSD);
        if ( sdURL == null ) {
            System.out.println("Can not load resource as URL " + testSD );
        }
        ServiceDescriptor sd = ServiceDescriptorImpl.createServiceDescriptor(sdURL, sdCL);
        assertEquals("test", sd.getServiceType());
        assertEquals("sun-test-binding", sd.getComponentName());
        assertEquals(true, sd.isBindingComponent());

        Configuration config = sd.getDefaultConfiguration();
        System.out.println("Default configuration ");
        System.out.println(config.getName());
        Properties props = config.getProperties();
        for ( Object key : props.keySet()) {
            System.out.println(key + "=" + props.get(key));
        }

        printConfigurations(System.out, sd);
        
        String value = props.getProperty("test1.prop.param3");
        assertEquals("value3", value);


    }

    public void testDefaultServiceDescriptor() throws Exception {
        ServiceDescriptor sd = ServiceDescriptorImpl.getDefaultServiceDescriptor();
        assertEquals("jbi", sd.getServiceType());
        assertEquals("sun-jbi-runtime", sd.getComponentName());
        assertEquals(false, sd.isBindingComponent());

    }

}

