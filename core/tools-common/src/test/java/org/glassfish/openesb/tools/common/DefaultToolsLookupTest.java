/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 *
 * @author chikkala
 */
public class DefaultToolsLookupTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DefaultToolsLookupTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(DefaultToolsLookupTest.class);
    }
    /**
     * Rigourous Test :-)
     */
    public void testDefaultToolsLookup() throws Exception {

        ToolsLookup lookup = new DefaultToolsLookup();
        String serviceType = "my";
        ServiceDescriptor sd = lookup.getServiceDescriptor(serviceType);

        System.out.println("Using Tools Lookup to find service type provider for service type : " + serviceType);
        
        System.out.println("ServiceType: " + sd.getServiceType());
        System.out.println("ComponentName: " + sd.getComponentName());
        System.out.println("DisplayName: " + sd.getDisplayName());
        System.out.println("ShortDescription: " + sd.getShortDescription());
        System.out.println("Long Description: " + sd.getLongDescription());

        assertEquals(serviceType, sd.getServiceType());
        assertEquals("sun-my-engine", sd.getComponentName());
    }

}

