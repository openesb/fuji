/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.tools.common.jbi.descriptor;

import junit.framework.TestCase;

/**
 *
 * @author kcbabo
 */
public class JbiTest extends TestCase {
    
    public JbiTest(String testName) {
        super(testName);
    }

    /**
     * Test of newJbi method, of class Jbi.
     */
    public void testNewJbi() throws Exception {
    }

    /**
     * Test of getComponent method, of class Jbi.
     */
    public void testGetComponent() {
    }

    /**
     * Test of getSharedLibrary method, of class Jbi.
     */
    public void testGetSharedLibrary() {
    }

    /**
     * Test of getServiceAssembly method, of class Jbi.
     */
    public void testGetServiceAssembly() {
    }

}
