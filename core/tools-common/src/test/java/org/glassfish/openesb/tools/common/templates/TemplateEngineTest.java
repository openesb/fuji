/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common.templates;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 * @author chikkala
 */
public class TemplateEngineTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TemplateEngineTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TemplateEngineTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testTemplateEngine() throws Exception {
        String template = "Hello $user !!!";
        StringReader reader = new StringReader(template);
        StringWriter writer = new StringWriter();
        Properties props = new Properties();
        props.setProperty("user", "chikkala");
        TemplateEngine te = TemplateEngine.getDefault();
        te.processTemplate(reader, writer, props);
        String result = writer.getBuffer().toString();
        System.out.println(result);
        assertEquals("Hello chikkala !!!", result);

    }

    private StringBuffer readResource(String resourcePath) throws IOException {
        InputStream inS = null;
        PrintWriter out = null;
        try {
            inS = getResourceAsStream(resourcePath);
            InputStreamReader inReader = new InputStreamReader(inS);
            BufferedReader in = new BufferedReader(inReader);
            StringWriter writer = new StringWriter();
            out = new PrintWriter(writer);
            boolean first = true;
            for (String line = null; (line = in.readLine()) != null;) {
                if (first) {
                    first = false;
                } else {
                    out.println();
                }
                out.print(line);
            }
            return writer.getBuffer();
        } finally {
            closeInputStream(inS);
        }
    }

    private InputStream getResourceAsStream(String resourcePath) {
        return this.getClass().getResourceAsStream(resourcePath);
    }

    private void closeInputStream(InputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (Exception ex) {
            }
        }
    }

    private void printInput(String templatePath, Properties props) {
        System.out.println("--- Template : " + templatePath + " Input");
        for (Object key : props.keySet()) {
            System.out.println(" " + key + "=" + props.get(key));
        }
    }

    private StringBuffer processTemplate(String templatePath, Properties props) throws Exception {
        InputStream in = null;
        StringWriter writer = new StringWriter();
        try {
            in = getResourceAsStream(templatePath);
            InputStreamReader reader = new InputStreamReader(in);
            printInput(templatePath, props);
            TemplateEngine te = TemplateEngine.getDefault();
            te.processTemplate(reader, writer, props);
            System.out.println(writer.getBuffer().toString());
            return writer.getBuffer();
        } finally {
            closeInputStream(in);
        }

    }

    public void testTemplate1out1() throws Exception {

        StringBuffer result;

        String templatePath = "template1.txt";

        Properties props = new Properties();
        props.setProperty("user-name", "chikkala");
        props.setProperty("user-phone-type", "home");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template1_out1.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

     public void testTemplate1out2() throws Exception {

        StringBuffer result;

        String templatePath = "template1.txt";

        Properties props = new Properties();
        props.setProperty("user-name", "chikkala");
        props.setProperty("user-phone-type", "work");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template1_out2.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

     public void testTemplate1out3() throws Exception {

        StringBuffer result;

        String templatePath = "template1.txt";

        Properties props = new Properties();
        props.setProperty("user-name", "chikkala");
        props.setProperty("user-phone-type", "mobile");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template1_out3.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

    public void testTemplate2out1() throws Exception {

        StringBuffer result;

        String templatePath = "template2.txt";

        Properties props = new Properties();
        props.setProperty("userName", "chikkala");
        props.setProperty("userPhoneType", "home");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template2_out1.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

     public void testTemplate2out2() throws Exception {

        StringBuffer result;

        String templatePath = "template2.txt";

        Properties props = new Properties();
        props.setProperty("userName", "chikkala");
        props.setProperty("userPhoneType", "work");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template2_out2.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

     public void testTemplate2out3() throws Exception {

        StringBuffer result;

        String templatePath = "template2.txt";

        Properties props = new Properties();
        props.setProperty("userName", "chikkala");
        props.setProperty("userPhoneType", "mobile");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template2_out3.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

    public void testTemplate3out1() throws Exception {

        StringBuffer result;

        String templatePath = "template3.txt";

        Properties props = new Properties();
        props.setProperty("user.name", "chikkala");
        props.setProperty("user.phone.type", "home");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template3_out1.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

     public void testTemplate3out2() throws Exception {

        StringBuffer result;

        String templatePath = "template3.txt";

        Properties props = new Properties();
        props.setProperty("user.name", "chikkala");
        props.setProperty("user.phone.type", "work");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template3_out2.txt");

        assertEquals(outBuff.toString(), result.toString());

    }

     public void testTemplate3out3() throws Exception {

        StringBuffer result;

        String templatePath = "template3.txt";

        Properties props = new Properties();
        props.setProperty("user.name", "chikkala");
        props.setProperty("user.phone.type", "mobile");
        result = processTemplate(templatePath, props);

        StringBuffer outBuff = readResource("template3_out3.txt");

        assertEquals(outBuff.toString(), result.toString());

    }
}
