/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

package org.glassfish.openesb.tools.common.templates;

import java.util.Properties;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author chikkala
 */
public class PropertyMapTest extends TestCase {
    
    public PropertyMapTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(PropertyMapTest.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testPropertyMapGetSetProperty() throws Exception {

        PropertyMap propMap = new PropertyMap();

        String userProp = "user";
        String userValue = "chikkala";

        String firstNameProp = "user.name.first";
        String firstNameValue = "Srinivasan";

        String lastNameProp = "user.name.last";
        String lastNameValue = "Chikkala";

        String workPhoneProp = "user.phone.work";
        String workPhoneValue = "408-888-WORK";

        String homePhoneProp = "user.phone.home";
        String homePhoneValue = "408-888-HOME";

        String phoneProp = "user.phone";
        String phoneValue = workPhoneValue;

        propMap.setProperty(firstNameProp, firstNameValue);
        propMap.setProperty(lastNameProp, lastNameValue);
        propMap.setProperty(userProp, userValue);
        propMap.setProperty(phoneProp, phoneValue);
        propMap.setProperty(workPhoneProp, workPhoneValue);
        propMap.setProperty(homePhoneProp, homePhoneValue);

        assertEquals(firstNameValue, propMap.getProperty(firstNameProp));
        assertEquals(lastNameValue, propMap.getProperty(lastNameProp));
        assertEquals(userValue, propMap.getProperty(userProp));
        assertEquals(phoneValue, propMap.getProperty(phoneProp));
        assertEquals(workPhoneValue, propMap.getProperty(workPhoneProp));
        assertEquals(homePhoneValue, propMap.getProperty(homePhoneProp));

//        System.out.println("#### propertyMap to properties ");
//        Properties props = propMap.toProperties();
//        for ( Object key :  props.keySet()) {
//            System.out.println("key=" + key);
//        }

    }

    public void testPropertyMapToProperties() throws Exception {

        PropertyMap propMap = new PropertyMap();

        String userProp = "user";
        String userValue = "chikkala";

        String firstNameProp = "user.name.first";
        String firstNameValue = "Srinivasan";

        String lastNameProp = "user.name.last";
        String lastNameValue = "Chikkala";

        String workPhoneProp = "user.phone.work";
        String workPhoneValue = "408-888-WORK";

        String homePhoneProp = "user.phone.home";
        String homePhoneValue = "408-888-HOME";

        String phoneProp = "user.phone";
        String phoneValue = workPhoneValue;

        String addressStreetProp = "address.street";
        String addressStreetValue = "1111 Network Drive";

        String addressCityProp = "address.city";
        String addressCityValue = "Santa Clara";

        propMap.setProperty(firstNameProp, firstNameValue);
        propMap.setProperty(lastNameProp, lastNameValue);
        propMap.setProperty(userProp, userValue);
        propMap.setProperty(phoneProp, phoneValue);
        propMap.setProperty(workPhoneProp, workPhoneValue);
        propMap.setProperty(homePhoneProp, homePhoneValue);
        propMap.setProperty(addressStreetProp, addressStreetValue);
        propMap.setProperty(addressCityProp, addressCityValue);
//
//        assertEquals(userValue, propMap.getProperty(userProp));

        System.out.println("#### propertyMap to properties ");
        propMap.printNodes();

        Properties props = propMap.toProperties();
        for ( Object key :  props.keySet()) {
            System.out.println(key + "=" + props.get(key));
        }        

        assertEquals(8, props.size());

        assertEquals(firstNameValue, props.getProperty(firstNameProp));
        assertEquals(lastNameValue, props.getProperty(lastNameProp));
        assertEquals(userValue, props.getProperty(userProp));
        assertEquals(phoneValue, props.getProperty(phoneProp));
        assertEquals(workPhoneValue, props.getProperty(workPhoneProp));
        assertEquals(homePhoneValue, props.getProperty(homePhoneProp));
        assertEquals(addressStreetValue, props.getProperty(addressStreetProp));
        assertEquals(addressCityValue, props.getProperty(addressCityProp));



    }
}
