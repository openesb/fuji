/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package org.glassfish.openesb.tools.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.jar.Manifest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author chikkala
 */
public class UtilsTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public UtilsTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(UtilsTest.class);
    }

    private File getTestDataDir() throws IOException {
        File testDataDir = null;
        String baseDir = System.getProperty("basedir");
        testDataDir = new File(baseDir, "src/test/data");
        return testDataDir;
    }
    
    /**
     * Rigourous Test :-)
     */
    public void testJarPacaker() throws Exception {
        System.out.println("Executing jar packager test....");
        String baseDir = System.getProperty("basedir");
        File testSrcDir = new File(baseDir, "src/test");
        File archive1 = new File(baseDir, "target/test-src1.jar");
        File archive2 = new File(baseDir, "target/test-src2.jar");
        System.out.println("Archive file1 " + archive1.getPath());
        System.out.println("Archive file2 " + archive1.getPath());

        Utils.Jar jar1 = new Utils.Jar(archive1);
        jar1.includeFile(testSrcDir, false);
        archive1 = jar1.create();
        boolean exists1 = archive1.exists();
        assertTrue("Expecting file .../target/test-src1.jar", exists1);

        Utils.Jar jar2 = new Utils.Jar(archive2);
        jar2.includeFile(testSrcDir, true);
        archive2 = jar2.create();
        boolean exists2 = archive2.exists();
        assertTrue("Expecting file .../target/test-src2.jar", exists2);

    }

    public void testJarManifestRead() throws Exception {
          File archetype = new File(getTestDataDir(), "test-archetype.jar");
          if (!archetype.exists()) {
              throw new IOException("test-archetype.jar does not exist " + archetype.getAbsolutePath());
          }

          Utils.UnJar archetypeUnJar = new Utils.UnJar(archetype.toURI().toURL());
          Manifest mf = archetypeUnJar.getManifest();
          assertNotNull("No manifest file exists", mf);
          String mfPath = "archetype-resources/src/main/resources/META-INF/MANIFEST.MF";
          ByteArrayOutputStream outBuff = new ByteArrayOutputStream();
          archetypeUnJar.copy(mfPath, outBuff);
          System.out.println(outBuff.toString());
          Properties props = new Properties();

          props.load(new ByteArrayInputStream(outBuff.toByteArray()));
          String compName = props.getProperty("Component-Name");
          System.out.println("component name:\'" +  compName + "\'");
          assertEquals("sun-test1-binding", compName);
    }
}

