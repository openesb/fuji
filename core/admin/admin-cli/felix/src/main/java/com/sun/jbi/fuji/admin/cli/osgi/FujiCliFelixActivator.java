/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliActivator.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.osgi;

import com.sun.jbi.fuji.admin.cli.osgi.Localizer;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class FujiCliFelixActivator implements BundleActivator
{
    private transient BundleContext context_ = null;
    private Localizer sLoc_ = Localizer.get();

    public void start(BundleContext context)
    {
        context_ = context;
        try {
            context.registerService(org.apache.felix.shell.Command.class.getName(), 
                                    new FujiCliFelix(context_), 
                                    null);
        }
        catch (Exception ex) {
            String msg = sLoc_.t("0001: Failed to register the Fuji CLI bundle into the Felix Runtime.");
            System.out.println (msg);
        }
    }  

    public void stop(BundleContext context)
    {
        // Services are automatically unregistered so
        // we don't have to unregister the factory here.
    }
}
