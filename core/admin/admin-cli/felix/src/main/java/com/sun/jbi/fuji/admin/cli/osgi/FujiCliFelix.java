/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliImpl.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.osgi;

import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;
import java.io.PrintStream;
import java.util.Properties;
import org.osgi.framework.BundleContext;
import org.apache.felix.shell.Command;

public class FujiCliFelix implements Command
{
    // This is the name that is uses to run the fuji cli program"
    public static final String CLI_SHELL_NAME = "fuji";

    // The OSGi framework context
    private BundleContext context_ = null;

    // Localizer instance
    private Localizer sLoc_ = Localizer.get();


    public FujiCliFelix() {
    }

    public FujiCliFelix(BundleContext context)
    {
        this.context_ = context;
    }

    public void setContext (BundleContext context)
    {
        this.context_ = context;
    }

    public String getName()
    {
        return CLI_SHELL_NAME;
    }

    public String getUsage()
    {
        return CLI_SHELL_NAME + " <cmd> [<--options> ..] [operand]";
    }

    public String getShortDescription()
    {
        return (sLoc_.tNoID("1007: fuji CLI utility"));
    }

    public void execute (String s, PrintStream out, PrintStream err)
    {
		FujiCliImpl impl = new FujiCliImpl(this.context_);
        impl.execute(CLI_SHELL_NAME, s, out, err);
    }

}
