/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import org.osgi.framework.ServiceReference;
import java.util.HashMap;
import com.sun.jbi.clientservices.eip.EIPManager;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;
import org.ho.yaml.YamlDecoder;
import org.ho.yaml.Yaml;


public class FujiCliGenerateEIPTemplate extends FujiCliCommand
{

    public FujiCliGenerateEIPTemplate () 
    {
        FujiCliOption type = new FujiCliOption ("--type","",EIPTypes,true);
        type.setOptionalOperand(true);        
        addOption(type);
    }

    public void execute () throws Exception
    {
        String msg = "";
        validateOptions();
        String type = getOptionalOperand("--type");

        try {
            ServiceReference svcRef = context_.getServiceReference(EIPManager.class.getName());
            Object svc = context_.getService(svcRef);
            if (svcRef != null)
            {
                EIPManager mgr = (EIPManager)svc;
                HashMap map = (HashMap)mgr.generateTemplate(type);
                if (map != null)
                {
                    Yaml yaml = new Yaml();
                    String dump = yaml.dump(map);
                    dump = FujiCliUtilities.cleanUpYamlDump(dump);
                    out_.println (dump);
                }
                else
                {
                    msg = sLoc_.t("0023: Unable to generate EIP Template for type: {0}.",type);
                }

            }
        }
        catch (Throwable e) {
            msg = sLoc_.t("0029: {0}",e.getMessage());
        }

        if (msg != "")
        {
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

}
