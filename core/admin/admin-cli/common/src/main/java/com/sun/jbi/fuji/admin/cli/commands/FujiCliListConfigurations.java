/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliListConfigurations.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;


public class FujiCliListConfigurations extends FujiCliCommand
{

    public FujiCliListConfigurations () 
    {
        String typeDefault = "*";
        List<String> typeOptions = Arrays.asList("interceptor","aspect","runtime","*");
        FujiCliOption type = new FujiCliOption ("--type",typeDefault,typeOptions,false);

        String fullDefault = "true";
        List<String> fullOptions = Arrays.asList("true","false");
        FujiCliOption full = new FujiCliOption ("--full",fullDefault,fullOptions,false);
        full.setMissingValueDefault("false");

        FujiCliOption filter = new FujiCliOption ("--filter",false);

        addOption(filter);
        addOption(type);
        addOption(full);
    }

    public void execute () throws Exception
    {
        validateOptions();
        String filter = (String)optionProperties_.get("--filter");
        String type   = (String)optionProperties_.get("--type");
        String full   = (String)optionProperties_.get("--full");

        if (full.equals("true"))
        {
            String msg = sLoc_.tNoID("1001: Configuration Full PID Names");
            String underline = createFillString('-',msg.length());
            out_.println ("\n"+msg);
            out_.println (underline);
            List configurations = listConfigurations(type,filter);
            for (Iterator it=configurations.iterator(); it.hasNext();) 
            {
                FujiCliConfiguration config = (FujiCliConfiguration)it.next ();
                out_.println (config.getPid());
            }
        }

        else
        {
            String msg = sLoc_.tNoID("1003: Configuration Names");
            String underline = createFillString('-',msg.length());
            out_.println ("\n"+msg);
            out_.println (underline);

            List configurations = listConfigurations(type,filter);
            // This sort is not working, since the list of configurations contain objects.
            //Collections.sort(configurations);
            for (Iterator it=configurations.iterator(); it.hasNext();) 
            {
                FujiCliConfiguration config = (FujiCliConfiguration)it.next ();
                out_.println (config.getName());
            }
        }
    }

}
