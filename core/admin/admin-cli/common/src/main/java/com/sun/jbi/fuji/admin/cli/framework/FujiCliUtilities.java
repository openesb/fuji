/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliUtilities.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.framework;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;

public class FujiCliUtilities
{

    /**
     * Utility that will perform system environment variable substitution if the input
     * string is wrapped in "${" "}".
     * @param str input String
     */
    public static String CheckForEnvVariable (String str)
    {
        if (str.startsWith("${"))
        {
            String envVar = str.substring(2,str.lastIndexOf('}'));
            str = System.getenv(envVar);
        }
        return str;
    }

    
    /**
     * This is a utility parses the command string and places them into a property object.
     * The input string should have each entry seperated by a comma.
     * (i.e. one,two,three,four,five,si x,seven,eight will create properties values of 
     * one=two, three=four, five=s ix, seven=eight)
     * @param propString input String
     */
    public static Properties stringToProperties (String propString)
    {
        Properties properties = new Properties();
        String[] propertyArray = propString.split("(?<![\\\\]),");
        int i = 0;
        String key = "";
        String value = "";
        while (i < propertyArray.length)
        {
            key = propertyArray[i++].trim();
            if (i < propertyArray.length) {
                value = propertyArray[i++].trim();
            }
            else {
                value = "";
            }
            value = removeBackslashCharacters(value);
            key = removeBackslashCharacters(key);
            properties.put (key,value);
        }
        return properties;
    }


    /**
     * This is a utility parses the command string and places them into a property object,
     * that has all values of null.  I know, why don't we create a List, probably because
     * the calling routine needs the returning object to a of type Properties.
     * @param propString input String
     */
    public static Properties stringToPropertiesList (String propString)
    {
        Properties properties = new Properties();
        String[] propertyArray = propString.split("(?<![\\\\]),");
        for (int i=0; i<propertyArray.length; i++)
        {
            String key = (String)propertyArray[i].trim();
            key = removeBackslashCharacters(key);
            properties.put (key,"");
        }
        return properties;
    }


    /**
     * Will remove any backslashes (escape char) from the input string.
     * @param propString input String
     */
    public static String removeBackslashCharacters(String input)
    {
        StringBuffer buffer = new StringBuffer(input);
        StringBuffer output = new StringBuffer();
        for (int i = 0; i < buffer.length(); i++)
        {
            if (buffer.charAt(i) == '\\')
                ; // do nothing
            else
                output.append(buffer.charAt(i));
        }
        return output.toString();
    }


    /**
     * Will remove any quotes that may be around the input string.
     * @param str input String
     * @return string with not quotes around it
     */
    public static String removeQuotesAroundString (String str)
    {
        if (str.substring(0,1).equals("\""))
        {
            str = str.substring(1);
        }
        if (str.substring(str.length()-1).equals("\""))
        {
            str = str.substring(0,str.length()-1);
        }
        return str;
    }


    public static Enumeration getSortedPropertiesKeys (Properties prop)
    {
        Enumeration keysEnum = prop.keys();
        Vector keyList = new Vector();
        while(keysEnum.hasMoreElements()){
            keyList.add(keysEnum.nextElement());
        }
        Collections.sort(keyList);
        return keyList.elements();
    }


	public static HashMap propertiesToHashMap (Properties prop)
	{
		HashMap myMap = new HashMap();
		Enumeration en = prop.keys();
		while (en.hasMoreElements())
		{
			String key = (String)en.nextElement();
            String value = (String)prop.get(key);
            myMap.put(key,value);
        }
        return myMap;
	}   


    public static String cleanUpYamlDump (String dump)
    {
        int start = 0;
        while (start < dump.length())
        {
            if (dump.substring(start,start+1).equals("-")) {
                start++;
            }
            else {
                break;
            }
        }
        String dumpString = dump.substring(start);
        return dumpString;
    }

}
