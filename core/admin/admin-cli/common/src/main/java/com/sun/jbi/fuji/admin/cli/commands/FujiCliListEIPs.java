/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import com.sun.jbi.clientservices.eip.EIPManager;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;
import javax.xml.namespace.QName;


public class FujiCliListEIPs extends FujiCliCommand
{

    public FujiCliListEIPs () 
    {
        String fullDefault = "true";
        List<String> fullOptions = Arrays.asList("true","false");
        FujiCliOption full = new FujiCliOption ("--full",fullDefault,fullOptions,false);
        full.setMissingValueDefault("false");
        //FujiCliOption filter = new FujiCliOption ("--filter",false);
        addOption(full);
        //addOption(filter);
    }


    public void execute () throws Exception
    {
        // Retrieve the file name (can be an options or the operand)
        validateOptions();
        String filter = (String)optionProperties_.get("--filter");
        String full   = (String)optionProperties_.get("--full");

        log_.fine("DEBUG: filter: " + filter);
        log_.fine("DEBUG: full: " + full);

        try 
        {
            String type = null;
            constructEIPList(type);
            String header = sLoc_.tNoID("1016: EIP Names");
            displayHeader(header);
            Iterator iterator = FujiCliImpl.EipMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                Integer index = (Integer)entry.getKey();
                String eipId = (String)entry.getValue();

                QName qname = QName.valueOf(eipId);
                String nameSpaceURI = qname.getNamespaceURI();
                String localName    = qname.getLocalPart();

                // Pad single digit index numbers with a leading space.
                String displayIndex = "" + index;
                if (displayIndex.length() == 1)
                {
                    displayIndex = " " + displayIndex;
                }

                if (full.equalsIgnoreCase("true"))
                {
                    out_.println("[" + displayIndex + "] " + qname);
                }
                else
                {
                    out_.println("[" + displayIndex + "] " + localName);
                }
            }
        }
        catch (Throwable e) {
            String msg = sLoc_.t("0029: {0}",e.getMessage());
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

}
