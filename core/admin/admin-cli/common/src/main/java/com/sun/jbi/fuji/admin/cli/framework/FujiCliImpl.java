/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliImpl.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.framework;


import java.io.PrintStream;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
import org.osgi.framework.BundleContext;
import com.sun.jbi.fuji.admin.cli.commands.FujiCliCommand;
import com.sun.jbi.fuji.admin.cli.commands.FujiCliHelp;
import com.sun.jbi.fuji.admin.cli.Localizer;


public class FujiCliImpl
{
    // The OSGi framework context
    private BundleContext context_ = null;

    // Localizer instance
    private Localizer sLoc_ = Localizer.get();

    // Properties object to hold the set options values.
    public static Properties setOptionValue_ = null;

    // The name of the cli program
    public static String cliShellName_ = "";

    public static TreeMap EipMap = new TreeMap();
    public static int     EipMapIndex_ = 0;



    public FujiCliImpl() {
    }

    public FujiCliImpl(BundleContext context)
    {
        context_ = context;
        if (setOptionValue_ == null)
        {
            setOptionValue_ = new Properties();
        }
    }

    public List getCommandList (String command)
    {
        ArrayList commandParts = new ArrayList();
        StringBuffer currPart = new StringBuffer();
        boolean inQuote = false;
        boolean addEscape = false;
        char lastCh = ' ';
        char ch = ' ';

        for (int x = 0; x < command.length(); x++) {
            if (!(Character.isWhitespace(ch))) {
                lastCh = ch;
            }
            ch = command.charAt(x);
            if (ch == '\'' || ch == '"') 
            { 
                inQuote = !inQuote; 
                if (inQuote) {
                    if (lastCh == '=') {
                        addEscape = true;
                    }
                }
            } 
            else if ((Character.isWhitespace(ch) || 
                      (ch == '=') ||
                      (ch == ',')) && !inQuote) { 
                commandParts.add(currPart.toString()); 
                currPart = new StringBuffer(); 
                addEscape = false;
            } 
            else { 

                if (ch == ',') {
                    if (addEscape) {
                        currPart.append('\\');
                    }
                }
                currPart.append(ch); 
            }

            // get the last part of the command 
            if (x == command.length() - 1) { 
                commandParts.add(currPart.toString().trim()); 
            } 
        }

        String[] coms = new String[commandParts.size()]; 
        commandParts.toArray(coms);
        List parameterList = new ArrayList();
        for (int i=0; i<coms.length; i++) {
            if (coms[i].length() > 0) {
                if (coms[i].endsWith(",")) {
                    coms[i] = coms[i].substring(0,coms[i].length() - 1);
                }
                parameterList.add(coms[i]);
            }
        }
        return parameterList; 
    } 


    public void execute (String name,
                         String s, 
                         PrintStream out, 
                         PrintStream err)
    {
        // Assign the name to the static variable.
        cliShellName_ = name;

        List tokens = getCommandList(s);
        String cliName = (String)tokens.get(0);

        if (tokens.size() > 1)
        {
            String cmdName = (String)tokens.get(1);
            cmdName = FujiCliUtilities.CheckForEnvVariable(cmdName);

            try {
                // Check to see if we are asking for general help or help on a specific command
                if ((cmdName.equals("--help")) || (cmdName.equals("-h")))
                {
                    if (tokens.size() > 2)
                    {
                        if (tokens.size() > 3)
                        {
                            String msg = sLoc_.t("0010: More then one command was specified.");
                            err.println (msg);
                        }

                        // Will display help for a specific command. (i.e. fuji --help list-configuration)
                        else
                        {
                            cmdName = (String)tokens.get(2);
                            FujiCliCommand cmd = new FujiCliHelp(out,err);
                            cmd.execute(cmdName);
                        }
                    }

                    // Will display the general help (i.e. fuji --help)
                    else
                    {
                        FujiCliCommand cmd = new FujiCliHelp(out,err);
                        cmd.execute();
                    }
                }

                else
                {
                    Properties properties = new Properties();
                    try {
                        String bundleName = "com.sun.jbi.fuji.admin.cli.resources.FujiCommands";
                        ResourceBundle fujiCommandClasses = ResourceBundle.getBundle(bundleName);
                        String classPath = fujiCommandClasses.getString(cmdName);
                        Class theClass  = Class.forName(classPath);
                        FujiCliCommand cmd = (FujiCliCommand)theClass.newInstance();
                        cmd.init(out,err,tokens,context_);
                        Properties prop = cmd.getOptions();

                        if (prop.containsKey("--help"))
                        {
                            FujiCliCommand helpCmd = new FujiCliHelp(out,err);
                            helpCmd.execute(cmdName);
                        }
                        else
                        {
                            cmd.setOptionProperties(prop);
                            cmd.execute(cmdName);
                        }

                    } 
                    catch (MissingResourceException e) {
                        String msg = sLoc_.t("0008: Invalid {0} Command: {1}.", cliShellName_, cmdName);
                        Exception fujiException = new Exception (msg);
                        throw (fujiException);
                    } 
                    catch (ClassNotFoundException ex) {
                        String msg = sLoc_.t("0007: ClassNotFoundException: Make sure the class specified for the key {0} is correct in the resource bundle.", cmdName);
                        Exception fujiException = new Exception (msg);
                        throw (fujiException);
                    }
                }
                if (FujiCliCommand.successMsgFlag_)
                {
                    String msg = sLoc_.tNoID("1008: Command {0} executed successfully.", cmdName);
                    out.println ("\n"+msg);
                }
            }
            catch (Throwable ex) {
                if (ex.getMessage() != "")
                {
                    err.println (ex.getMessage());
                    if (ex.getCause() != null)
                    {
                        err.println (ex.getCause());
                    }
                }
                if (FujiCliCommand.failureMsgFlag_)
                {
                    String msg = sLoc_.tNoID("1009: Command {0} failed.", cmdName);
                    err.println ("\n"+msg);
                }
            }
        }
        else
        {
            String msg = sLoc_.t("0000: No command specified, enter --help or -h for a list of available commands.");
            err.println (msg);
        }
    }
}
