/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import org.osgi.framework.ServiceReference;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.lang.Object;
import java.io.File;
import java.io.IOException;
import java.io.EOFException;
import java.io.FileInputStream;
import java.lang.NullPointerException;
import java.io.FileNotFoundException;
import java.util.Map;
import com.sun.jbi.clientservices.eip.EIPManager;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;

import javax.xml.namespace.QName;


public class FujiCliListAspects extends FujiCliCommand
{

    public FujiCliListAspects () 
    {
    }


    public void execute () throws Exception
    {
        validateOptions();

        try 
        {
        }
        catch (Throwable err) {
            err.printStackTrace();
        }
    }

}
