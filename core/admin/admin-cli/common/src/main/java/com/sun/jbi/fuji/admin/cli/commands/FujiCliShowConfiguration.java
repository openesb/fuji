/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliGetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Enumeration;
import java.util.Properties;

public class FujiCliShowConfiguration extends FujiCliCommand
{

    public FujiCliShowConfiguration () 
    {
        // Initialize the options for this command
        FujiCliOption name = new FujiCliOption ("--name",true);
        name.setOptionalOperand(true);
        name.setHelpName("configuration-name");
        addOption(name);
    }

    public void execute () throws Exception
    {
        validateOptions();
        String name = getOptionalOperand("--name");
        FujiCliConfiguration config = getConfiguration(name);
        if (config == null)
        {
            String msg = sLoc_.t("0009: Configuration Service not found for {0}.",name);
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        else 
        {
            String msg = sLoc_.tNoID("1006: Configuration Values For {0}",name);
            String underline = createFillString('-',msg.length());
            out_.println ("\n"+msg);
            out_.println (underline);
            Properties outputProperties = restrictIO(config.getConfigProperties());
            Enumeration e = outputProperties.keys();
            while (e.hasMoreElements())
            {
                String key   = (String)e.nextElement();
                String value = (String)outputProperties.get(key);
                out_.println (key + " = " + value);
            }
        }
    }

    public String getName () {
        return ("show-configuration");
    }

}
