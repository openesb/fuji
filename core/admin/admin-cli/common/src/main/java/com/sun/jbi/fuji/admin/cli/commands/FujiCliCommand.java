/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliCommand.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.PrintStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.lang.Exception;
import java.lang.IllegalArgumentException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Properties;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;  
import org.osgi.service.cm.ConfigurationAdmin; 
import com.sun.jbi.fuji.admin.cli.Localizer;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;
import com.sun.jbi.clientservices.eip.EIPManager;
import com.sun.jbi.configuration.ServicePID;
import com.sun.jbi.configuration.Constants;

import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.StandardMBean;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;
import javax.management.remote.JMXConnectorFactory;
import javax.xml.namespace.QName;

import java.lang.management.ManagementFactory;


public abstract class FujiCliCommand
{
    protected Logger log_ = Logger.getLogger(FujiCliCommand.class.getPackage().getName());

    protected PrintStream   out_     = null;
    protected PrintStream   err_     = null;
    protected List          tokens_  = null;
    protected BundleContext context_ = null;
    protected Localizer     sLoc_    = Localizer.get();
    protected Hashtable     options_ = new Hashtable();

    protected TreeMap       EIPMap   = new TreeMap();
    protected int           EIPMapIndex_ = 0;

    protected Properties optionProperties_; 
    protected String helpOperand_ = "";
    protected String commandName_ = "";

    public static boolean successMsgFlag_ = true;
    public static boolean partialMsgFlag_ = true;
    public static boolean failureMsgFlag_ = true;

    // Static strings that are used to construct PID's
    public static final String FUJI_PID_SET_LOGGER_TYPE           = "runtime-logger";
    public static final String FUJI_PID_GET_LOGGER_TYPE           = "runtime-logger-display";
    public static final String FUJI_PID_SET_COMPONENT_LOGGER_TYPE = "component-logger";
    public static final String FUJI_PID_GET_COMPONENT_LOGGER_TYPE = "component-logger-display";
    public static final String FUJI_PID_LOGGERS_KEY               = "loggers";

    public static final String FUJI_ADMIN_SERVICE_MBEAN_NAME   = "com.sun.jbi:ControlType=AdminService,ServiceName=AdminService";

    // The types that can be display when listing configurations.  This list restrict the
    // type of fuji configurations that can be displey in the list-configurations command 
    List<String> configListTypes_ = Arrays.asList("interceptor","aspect");

    // A list containing all the EIP types.
    List<String> EIPTypes = Arrays.asList("aggregate","broadcast","select","filter","split","tee");


    public FujiCliCommand () 
    {
    }

    /**
     * Initialize the command object.  Called from the framework to initialize the command.
     * This is a utility parses the command string and places them into a property object,
     * that has all values of null.  I know, why don't we create a List, probably because
     * the calling routine needs the returning object to a of type Properties.
     * @param out The standard output
     * @param err The standard error
     * @param tokens The complete list of tokens read from the command line
     * @param context The bundle context
     */
    public void init (PrintStream out, 
                      PrintStream err, 
                      List tokens,
                      BundleContext context)
    {
        out_     = out;
        err_     = err;
        tokens_  = tokens;
        context_ = context;
    }


    /**
     * Add an option object to the global options Hashtable.
     * @param option The option object
     */
    public void addOption (FujiCliOption option)
    {
        String name = option.getName();
        options_.put(name,option);
    }


    /**
     * Retrieve the specified option object.
     * @param name of option
     */
    public FujiCliOption getOption(String name)
    {
        return (FujiCliOption)options_.get(name);
    }


    /**
     * Validate the options that have been placed in the Hashtable.
     * @throws Exception if an error occured during validation.
     */
    public void validateOptions() throws Exception
    {
        // First lets make sure all options speccified are valid for this command
        Enumeration oPe = optionProperties_.keys();
        while (oPe.hasMoreElements())
        {
            String key = (String)oPe.nextElement();
            if (!(options_.containsKey(key)))
            {
                if (key.startsWith("--"))
                {
                    String msg = sLoc_.t("0022: The option {0} is not a valid option for the command {1}.", key,getName());
                    Exception fujiException = new Exception (msg);
                    throw (fujiException);
                }
            }
        }

        Enumeration e = options_.keys();
        while (e.hasMoreElements())
        {
            String key = (String)e.nextElement();
            FujiCliOption cliOption = (FujiCliOption)options_.get(key);

            // Is a required option not present 
            if (!(optionProperties_.containsKey(cliOption.getName())))
            {
                // If the option is NOT a optional operand (i.e. name in get-configuration)
                if (!(cliOption.isOptionalOperand()))
                {
                    // If the option has a value stored in the setOptionValue map
                    if (FujiCliImpl.setOptionValue_.containsKey(cliOption.getName()))
                    {
                        optionProperties_.put(cliOption.getName(),
                                              FujiCliImpl.setOptionValue_.get(cliOption.getName()));
                    }

                    // Is there a assigned default value associated with this option
                    else if (cliOption.getMissingValueDefault() != "")
                    //else if (cliOption.getDefault() != "")
                    {
                        optionProperties_.put(cliOption.getName(),cliOption.getMissingValueDefault());
                        //optionProperties_.put(cliOption.getName(),cliOption.getDefault());
                    }

                    // If none of the above are true, then display the error message.
                    else if (cliOption.isRequired())
                    {
                        String msg = sLoc_.t("0003: The required option {0} is missing.", key);
                        Exception fujiException = new Exception (msg);
                        throw (fujiException);
                    }
                }
            }
            else
            {
                String optionValue = (String)optionProperties_.get(cliOption.getName());
                if (!(cliOption.isLegal(optionValue)))
                {
                    String msg = sLoc_.t("0005: The value specified {0} is not valid for the option {1}.",optionValue,key);
                    Exception fujiException = new Exception (msg);
                    throw (fujiException);
                }
            }
        }
    }


    /**
     * Return the name of the command.  This method should be overridden.
     * @return name of the command
     */
    public String getName() 
    {
        return commandName_;
    }


    /**
     * Will initialize the name of the command.
     * @param cmdName The name of the command
     */
    public void setName (String cmdName)
    {
        commandName_ = cmdName;
    }


    public void setOptionProperties (Properties options)
    {
        optionProperties_ = options;
    }

    public void execute (String cmdName) throws Exception
    {
        setName(cmdName);
        execute();
    }

    public void execute() throws Exception
    {
    }

    public void execute (FujiCliCommand cmd) throws Exception
    {
    }

    public Properties getOptions () 
    {
        Properties properties = new Properties();

        // If the --help option is specified, then we can ignore any other option
        if (tokens_.contains("--help") || tokens_.contains("-h"))
        {
            properties.put("--help","--help");
        }
        else
        {
            int size = tokens_.size();
            int i = 0;
            String key = (String)tokens_.get(i++);
            String value = (String)tokens_.get(i++);
            key = FujiCliUtilities.CheckForEnvVariable(key);
            value = FujiCliUtilities.CheckForEnvVariable(value);

            properties.put (key,value);

            // Place the options in the properties object
            String comma = "";
            String operandString = "";
            while (i < size)
            {
                key = (String)tokens_.get(i++);
                if (i < size) {
                    value = (String)tokens_.get(i++);
                }
                else {
                    value = "";
                }

                // If no value found, check to see if there is a default value for that option
                if ((value.startsWith("--")) || (value == ""))
                {
                    if (value.startsWith("--")) 
                        i--;
                    FujiCliOption option = getOption(key);
                    if (option != null)
                    {
                        value = option.getDefault();
                    }
                }

                key = FujiCliUtilities.CheckForEnvVariable(key);
                value = FujiCliUtilities.CheckForEnvVariable(value);
                if (key.startsWith("--"))
                {
                    properties.put (key,value);
                }
                else
                {
                    operandString += comma + key;
                    comma = ",";
                    //if (key.length() > 0)
                    if (value.length() > 0)
                    {
                        operandString += comma + value;
                        //operandString += comma + key;
                    }
                }
            }
            if (operandString.length() > 0)
            {
                properties.put ("=",operandString);
            }

        }
        return properties;
    }

    /**
     * Will set the logger levels for the specified nane (component). Note, it the name
     * is not specified (blank) then the logger levels for the runtiem will be set.
     * @param name The component name
     * @param properties the log names and values.
     */
    public void setLogger (String componentName,
                           Properties properties, 
                           String override) throws Exception
    {
        ConfigurationAdmin configAdmin = getConfigurationAdminService();
        Configuration config = null;
        FujiCliConfiguration fujiConfig = null;
        String pid     = "";
        String pidType = "";
        String pidName = "";

        // Set the pid to retieve either the runtime or component loggers
        if (componentName == null) {
            pidType = FUJI_PID_SET_LOGGER_TYPE;
            pidName = FUJI_PID_SET_LOGGER_TYPE;
        }
        else {
            pidType = FUJI_PID_SET_COMPONENT_LOGGER_TYPE;
            pidName = componentName;
        }
        pid = ServicePID.getServicePID(pidType,pidName);


        try {
            config = configAdmin.getConfiguration(pid,null);  
        }
        catch (Exception ex) {
            String msg = sLoc_.t("0002: Unable to retrieve information from the Configuration Management Service.");
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }

        if (config != null)
        {
            fujiConfig = new FujiCliConfiguration(pid);
            Dictionary configProperties = config.getProperties();
            if (configProperties != null)
            {
                Vector loggers = (Vector)configProperties.get(FUJI_PID_LOGGERS_KEY);
                fujiConfig.setConfigProperties(loggers,null,null);
            }

            properties = validateLogLevels(properties);

            //*** Commented out due to Issue #1143:  I'm  going to force the specified 
            //*** logger to work, (i.e. no validation which was automatially being done
            //*** in the updateConfigProperties method.) by setting override to "on"
            // override = "on";

            if (override.equals("on"))
            {
                fujiConfig.setConfigProperties(properties);
            }
            else
            {
                fujiConfig.updateConfigProperties(properties);
            }

            Vector loggers = fujiConfig.getConfigPropertyVector();
            configProperties.put(FUJI_PID_LOGGERS_KEY,loggers);
            config.update(configProperties); 
        }
        else
        {
            String msg = sLoc_.t("0001: The Configuration Admin. Service is not available.");
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

    /**
     * Will validate the levels in a properties object.
     * @param properties the properties object containing the logger name and level
     * @throws Exception if an illegal level is found.
     */
    private Properties validateLogLevels (Properties properties) throws Exception
    {
        Properties validLevels = new Properties();
        Enumeration e = properties.keys();
        while (e.hasMoreElements())
        {
            String key   = (String)e.nextElement();
            String value = ((String)properties.get(key)).toUpperCase();
            try {
                if (!(value.equalsIgnoreCase("DEFAULT")))
                {
                    Level level = Level.parse(value);
                }
                validLevels.put(key,value);
            }
            catch (IllegalArgumentException ex)
            {
                String msg = sLoc_.t("0012: The level specified {0} is invalid.",value);
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
        }
        return validLevels;
    }


    /**
     * Will return a FujiCliConfiguration object contain properties for
     * the specified name.  The properties are the loggers and thrie levels
     * @param name the name of the logger.
     * @return a FujiCliConfiguration object
     */
    public FujiCliConfiguration getLoggers (String componentName, 
                                            String filter) throws Exception
    {
        List engineList = new ArrayList();
        List bindingList = new ArrayList();
        ConfigurationAdmin configAdmin = getConfigurationAdminService();
        FujiCliConfiguration fujiConfig = null;
        String pid     = "";
        String pidType = "";
        String pidName = "";

        // Set the pid to retieve either the runtime or component loggers
        if (componentName == null) {
            pidType = FUJI_PID_GET_LOGGER_TYPE;
            pidName = FUJI_PID_GET_LOGGER_TYPE;
        }
        else {
            engineList = getJBIComponentNames("EngineComponents");
            bindingList = getJBIComponentNames("BindingComponents");
            if ((!(engineList.contains(componentName))) && (!(bindingList.contains(componentName))))
            {
                String msg = sLoc_.t("0021: The component name {0} is invalid.",componentName);
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
            pidType = FUJI_PID_GET_COMPONENT_LOGGER_TYPE;
            pidName = componentName;
        }
        pid = ServicePID.getServicePID(pidType,pidName);

        try {
            Configuration config = configAdmin.getConfiguration(pid,null); 
            if (config != null)
            {
                fujiConfig = new FujiCliConfiguration(pid);
                Dictionary properties = config.getProperties();
                if (properties != null)
                {
                    Vector loggers = (Vector)properties.get(FUJI_PID_LOGGERS_KEY);
                    fujiConfig.setConfigProperties(loggers,filter,null);
                }
            }
            else
            {
                throw new Exception();
            }
        } catch (Exception ex)
        {
            String msg = sLoc_.t("0002: Unable to retrieve information from the Configuration Management Service.");
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        return fujiConfig;
    }


    /**
     * Will return a list of FujiCliConfiguration objects for the given type.
     * @param tyoe the type of configuration to retireve
     * @return a List of FujiCliConfiguration object
     */
    public List listConfigurations (String type, String filter) throws Exception
    {
        return configurationAdministration ("list", type, null, filter, null);
    }


    /**
     * Will delete the specified properties from the specifed configuration (name).
     * @param name the name of the configuration.
     * @param deleteProperties the properties to delete.
     */
    public void deleteConfiguration (String name, Properties deleteProperties) throws Exception
    {
        configurationAdministration ("delete", null, name, deleteProperties);
    }


    /**
     * Will return a FujiCliConfiguration object contain properties for
     * the specified name.
     * @param name the name of the logger.
     * @return a FujiCliConfiguration object
     */
    public FujiCliConfiguration getConfiguration (String name) throws Exception
    {
        List configurations = configurationAdministration ("get", null, name, null);
        FujiCliConfiguration config = null;
        if (configurations.size() == 1)
        {
            config = (FujiCliConfiguration)configurations.get(0);
        }
        return config;
    }


    /**
     * Will add or update the given properties in the specified configuration (name).
     * @param name the name of the configuration.
     * @param newProperties the properties to add or update.
     */
    public void setConfiguration (String name, Properties newProperties) throws Exception
    {
        configurationAdministration ("set", null, name, newProperties);
    }


    private List configurationAdministration (String action,
                                              String type, 
                                              String name, 
                                              Properties properties) throws Exception
    {
        return configurationAdministration (action,type,name,null,properties);
    }

    /**
     * The method that will perform the administration of the configuration.  This method
     * is call for add, set, delete and list.  It will pass back a list of FujiCliConfiguration
     * objects.  For the get, set and delete the list will only contain one entry.
     * @param action what action to perform (set, get, list or delete)
     * @param type the type of object which is being administrated
     * @param name the name of the configuration.
     * @param filter the filter used when calling the Configuration Service list command
     * @param properties the properties to add or update or delete
     */
    private List configurationAdministration (String action,
                                              String type, 
                                              String name, 
                                              String filter,
                                              Properties properties) throws Exception
    {
        String commandStatus = "init";
        List configList = new ArrayList();
        ConfigurationAdmin configAdmin = getConfigurationAdminService();
        Configuration[] configs = null;
        try
        {
            if (filter != null)
            {
                filter = "(service.pid=*.*." + filter + ")";
            }
            configs = configAdmin.listConfigurations(filter);
        } catch (Exception ex)
        {
            String msg = sLoc_.t("0002: Unable to retrieve information from the Configuration Management Service.");
            Exception fujiException = new Exception (msg,ex);
            throw (fujiException);
        }

        if (configs != null)
        {
            for (int i=0; i<configs.length; i++)
            {
                String pid = configs[i].getPid();
                pid = FujiCliConfiguration.cleanUpPid(pid);
                ServicePID servicePid = new ServicePID(pid);
                String pidDomain = servicePid.getDomain();
                String pidType   = servicePid.getType();
                String pidName   = servicePid.getName();

                // We only want to muck with fuji configurations
                if (pidDomain.equalsIgnoreCase(Constants.CONFIG_DOMAIN))
                {
                    FujiCliConfiguration config = new FujiCliConfiguration(pid);
                    config.setPidProperties();

                    // Get Configuration
                    if (action.equals("get"))
                    {
                        if (config.getName().equals(name))
                        {
                            commandStatus = "name-found";
                            config.setConfigProperties(configs[i].getProperties());
                            configList.add(config);
                            break;
                        }
                    }

                    // Set Configuration.  Since we don't want the set to remove any existing
                    // properties, we will first read in any existing properties.
                    else if (action.equals("set"))
                    {
                        if (config.getName().equals(name))
                        {
                            commandStatus = "name-found";
                            config.setConfigProperties(configs[i].getProperties());
                            config.setConfigProperties(properties);
                            Dictionary allProperties = config.getConfigProperties();
                            configs[i].update(allProperties); 
                            configList.add(config);
                            break;
                        }
                    }

                    // Delete Configuration.  This requires us to read in the existing
                    // properties and then remove the ones we no longer want.
                    else if (action.equals("delete"))
                    {
                        if (config.getName().equals(name))
                        {
                            commandStatus = "name-found";
                            config.setConfigProperties(configs[i].getProperties());
                            config.deleteConfigProperties(properties);
                            Dictionary allProperties = config.getConfigProperties();
                            configs[i].update(allProperties); 
                            configList.add(config);
                            break;
                        }
                    }

                    // List Configurations
                    else if (type != null)
                    {
                        commandStatus = "list-empty";
                        if (configListTypes_.contains(config.getType())) {
                            if ((config.getType().equals(type)) ||
                                (type.equals("all")) || 
                                (type.equals("*"))) {
                                   configList.add(config);
                                   commandStatus = "list-full";
                            }
                        }
                    }
                }
            }
            if (commandStatus.equals("init"))
            {
                String msg = sLoc_.t("0014: The Configuration named \"{0}\" does not exist.",name);
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
        }

        return configList;
    }


    /**
     * Retrieve the Configuration Administratin Service reference. Not and exceptin
     * will be thrown if the Config Admin is not found.
     * @return Configuration Admin reference
     */
    protected ConfigurationAdmin getConfigurationAdminService() throws Exception
    {
        ConfigurationAdmin configAdmin= null;
        ServiceReference  configAdminRef = context_.getServiceReference(ConfigurationAdmin.class.getName());
        if (configAdminRef != null) 
        {
            configAdmin = (ConfigurationAdmin) context_.getService(configAdminRef);  
        }
        else
        {
            String msg = sLoc_.t("0001: The Configuration Admin. Service is not available.");
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        return configAdmin;
    }


    /**
     * Retrieve the component names using the AdminService MBean.  The list of 
     * components (Either Bindings or Engines depending on the input componentType)
     * will be returned from this method.
     * @param componentType The type of component ("BindingComponents" or "EngineComponents")
     * @return List of Component (Bindings or Engines) names.
     */
    public List getJBIComponentNames(String componentType) throws Exception
    {
        ArrayList nameList = new ArrayList();
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName queueObjName = new ObjectName(FUJI_ADMIN_SERVICE_MBEAN_NAME);
        ObjectName[] objectNames =  (ObjectName[])mbs.getAttribute(queueObjName, componentType);
        for (int i=0; i<objectNames.length;i++)
        {
            String value = objectNames[i].getKeyProperty("componentName");
            nameList.add(value);
        }
        return nameList;
    }


    public String getHelp (String cmdName) {
        setName(cmdName);
        return getHelp();
    }


    public String getHelp () {
        String helpString = sLoc_.tNoID("1000: Command Syntax:");
        helpString += " fuji " + getName();
        Enumeration e = options_.keys();
        while(e.hasMoreElements()) {
            String key = (String)e.nextElement();
            FujiCliOption option = (FujiCliOption)options_.get(key);
            if (option.getShowHelp())
            {
                String optionLeftBracket = "[";
                String optionRightBracket = "]";
                if (option.isRequired())
                {
                    optionLeftBracket = "";
                    optionRightBracket = "";
                }
                helpString += " " + optionLeftBracket;
             
                if (option.isOptionalOperand())
                    helpString += "["+option.getName()+"=]";
                else
                    helpString += option.getName() + "=";
            
                List<String> legalValues = option.getLegalValues();
                if (legalValues == null)
                {
                    helpString += "<" + option.getHelpName() + ">";
                }
                else
                {
                    helpString += "(";
                    String comma = "";
                    for (String value : legalValues)
                    {
                        helpString += comma;
                        if (value.equals(option.getDefault()))
                            helpString += "<"+value+">";
                        else
                            helpString += value;
                        comma = ",";
                    }
                    helpString += ")";
                }
                helpString += optionRightBracket;
            }
        }
        helpString += " " + helpOperand_;
        return helpString;
    }


    protected void setHelpOperand (String helpOperand)
    {
        helpOperand_ = helpOperand;
    }


    protected String getOptionalOperand (String optionName) throws Exception
    {
         String option = (String)optionProperties_.get(optionName);
         if (option == null)
         {
             option = (String)optionProperties_.get("=");
         }
         if (option == null)
         {
             FujiCliOption cliOption = (FujiCliOption)options_.get(optionName);

             if (FujiCliImpl.setOptionValue_.containsKey(optionName))
             {
                 option = (String)FujiCliImpl.setOptionValue_.get(optionName);
             }
             else if (cliOption.getDefault() != "")
             {
                 option = (String)cliOption.getDefault();
             }
             else if (cliOption.isRequired())
             {
                 String msg = sLoc_.t("0003: The required option {0} is missing.",optionName);
                 Exception fujiException = new Exception (msg);
                 throw (fujiException);
             }
         }
         return option;
    }


    /**
     * Will create a string of the size specified filled with the fillChar.
     * @param fillChar the character to create the string with
     * @param the size of the string
     */
    protected String createFillString (char fillChar, int size)
    {
        String fillString = "";
        for (int i=0; i<size; i++)
        {
            fillString += fillChar;
        }
        return fillString;
    }


    /**
     * Will display a string and underline it.
     * @param header - the string to display
     */
    protected void displayHeader (String header)
    {
        String underline = createFillString('-',header.length());
        out_.println ("\n"+header);
        out_.println (underline);
    }


    /**
     * Will remove any restrictive variables from the given properties object
     * @param the properties object
     * @return the updated properties object
     */
    protected Properties restrictIO (Properties properties)
    {
        String bundleName = "com.sun.jbi.fuji.admin.cli.resources.FujiRestrictiveProperties";
        ResourceBundle restrictiveBundle = ResourceBundle.getBundle(bundleName);
        String restrictValues = restrictiveBundle.getString(getName());
        String[] restrictElements = restrictValues.split(",");
        for (int i=0; i<restrictElements.length; i++)
        {
            properties.remove(restrictElements[i].trim());
        }
        return properties;
    }

    /**
     * Will check the given properties object for any restrictive values and
     * will return a string (comma seperated) containg any values found.
     * @param the properties object
     * @return the comma seperated string
     */
    protected String getRestrictedValues (Properties properties)
    {
        String restrictedValues = "";
        String bundleName = "com.sun.jbi.fuji.admin.cli.resources.FujiRestrictiveProperties";
        ResourceBundle restrictiveBundle = ResourceBundle.getBundle(bundleName);
        String restrictValues = restrictiveBundle.getString(getName());
        String[] restrictElements = restrictValues.split(",");
        List restrictedList = new ArrayList();
        String comma = "";
        for (int i=0; i<restrictElements.length; i++)
        {
            String restrictedKey = restrictElements[i].trim();
            if (properties.containsKey(restrictedKey))
            {
                restrictedValues += (comma + "\"" + restrictedKey + "\"");
                comma = ", ";
            }
        }
        return restrictedValues;
    }


    protected void constructEIPList (String type) throws Exception
    {
        ServiceReference svcRef = context_.getServiceReference(EIPManager.class.getName());
        Object svc = context_.getService(svcRef);

        if (svcRef != null)
        {
            EIPManager mgr = (EIPManager)svc;
            java.util.List<QName> flows = mgr.list(type);
            for (QName eip: flows)
            {
                String fullname = eip.toString();
                addEipEntry (fullname);
            }
        }
    }


    protected void addEipEntry (String qName)
    {
        if (!(FujiCliImpl.EipMap.containsValue(qName)))
        {
            Integer index = new Integer(FujiCliImpl.EipMapIndex_);
            FujiCliImpl.EipMap.put (index,qName);
            FujiCliImpl.EipMapIndex_++;
        }
    }


    protected void removeEipEntry (String qName)
    {
        log_.fine ("DEBUG: In removeEipEntry: " + qName);
        Set set = FujiCliImpl.EipMap.entrySet (  ) ; 
        Iterator iterator = set.iterator (  ) ; 
        boolean done = false;
        while (iterator.hasNext())
        {  
            Map.Entry entry =  (Map.Entry)iterator.next();
            String value = (String)entry.getValue();
            log_.fine ("DEBUG: value: " + value);
            if (value.equalsIgnoreCase(qName))
            {
                log_.fine ("DEBUG: Remove: " + qName);
                FujiCliImpl.EipMap.remove(entry.getKey());
                break;
            }
        }
    }

    
    /**
     * If the eipId is a number, then we can assume it is an index into the EipMap.  
     * If it is not a number, then we'll assume it is the eipId.
     * @param the eipId
     * @return the eipId
     */
    protected String getEIPListNumber (String eipId) throws Exception
    {
        try {
            String id = eipId;
            int pos = id.indexOf("--");  // Just in case it is the first option not an operand
            if (pos > 0)
            {
                id = (id.substring(0,pos-1)).trim();
            }
            Integer index = new Integer(id);
            eipId = (String)FujiCliImpl.EipMap.get(index);
            if (eipId == null)
            {
                constructEIPList(null);
                eipId = (String)FujiCliImpl.EipMap.get(index);
                if (eipId == null)
                {
                    String msg = sLoc_.t("0028: The index number specified {0} is invalid.",index);
                    Exception fujiException = new Exception (msg);
                    throw (fujiException);
                }
            }
        }
        catch (NumberFormatException e) {
        }
        return eipId;
    }


    protected EIPManager getEIPManagerService () throws Exception
    {
        ServiceReference svcRef = context_.getServiceReference(EIPManager.class.getName());
        if (svcRef == null) {
            String msg = sLoc_.t("0024: Unable to retrieve the EIPManager Service Reference.");
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        Object svc = context_.getService(svcRef);
        if (svc == null) {
            String msg = sLoc_.t("0025: Unable to retrieve the EIPManager Service.");
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        EIPManager mgr = (EIPManager)svc;
        return mgr;
     }


    /**
     * Will return a FileInputStream for the specified file.  It the file does not
     * exist, an error message will be thrown.
     * @param the filename
     * @return the File Input Stream
     */
    protected FileInputStream getFileInputStream (String filename) throws Exception
    {
        FileInputStream fis = null;
        try {
            File file = new File(filename);
            fis = new FileInputStream(file);
        }
        catch (FileNotFoundException e) {
            String msg = sLoc_.t("0026: The file specified {0} does not exist",filename);
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        return fis;
    }
}
