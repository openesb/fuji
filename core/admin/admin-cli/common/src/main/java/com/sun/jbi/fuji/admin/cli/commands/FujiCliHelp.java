/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliCliHelp.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.PrintStream;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;

public class FujiCliHelp extends FujiCliCommand
{

    public FujiCliHelp (PrintStream out, PrintStream err)
    {
        out_ = out;
        err_ = err;
    }


    public void execute () throws Exception
    {
        String cmdName = getName();

        // We don't want the success or failure message to be display at the end of the help.
        successMsgFlag_ = false;
        failureMsgFlag_ = false;

        String bundleName = "com.sun.jbi.fuji.admin.cli.resources.FujiCommands";
        ResourceBundle fujiCommandClasses = ResourceBundle.getBundle(bundleName);

        if (cmdName == "")
        {
            String msg = sLoc_.tNoID("1002: Available {0} Commands",FujiCliImpl.cliShellName_);
            out_.println ("\n"+msg);
            String underline = createFillString('-',msg.length());
            out_.println (underline);

            Enumeration e = fujiCommandClasses.getKeys();
            ArrayList list = new ArrayList();
            while (e.hasMoreElements())
            {
                String key = (String)e.nextElement();
                String value = (String)fujiCommandClasses.getString(key);
                if (value.length() == 0)
                {
                    key += " (Not Available)";
                    //list.add(key);  // For now if command class is not specified, we will not show it in the help list
                }
                else
                {
                    list.add(key);
                }

            }
            Collections.sort(list);
            for (Iterator it=list.iterator(); it.hasNext();) 
            {
                String key = (String)it.next ();
                out_.println(key);
            }

        }

        else {
            try {
                String classPath = fujiCommandClasses.getString(cmdName);
                Class theClass  = Class.forName(classPath);
                FujiCliCommand cmd = (FujiCliCommand)theClass.newInstance();
                String cmdHelp = cmd.getHelp(cmdName);
                out_.println(cmdHelp);
            }
            catch (MissingResourceException e) {
                String msg = sLoc_.t("0008: Invalid {0} Command: {1}.", FujiCliImpl.cliShellName_, cmdName);
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            } 
            catch (ClassNotFoundException ex) {
                String msg = sLoc_.t("0007: ClassNotFoundException: Make sure the class specified for the key {0} is correct in the resource bundle.",cmdName);
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
        }
    }

}
