/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import org.osgi.framework.ServiceReference;
import javax.xml.namespace.QName;
import java.util.HashMap;
import com.sun.jbi.clientservices.eip.EIPManager;
import org.ho.yaml.YamlDecoder;
import org.ho.yaml.Yaml;
import java.util.List;


public class FujiCliListConfigsEIPs extends FujiCliCommand
{

    public FujiCliListConfigsEIPs () 
    {
        FujiCliOption eipId = new FujiCliOption ("--eip-id","",null,true);
        eipId.setOptionalOperand(true);        
        addOption(eipId);
    }


    public void execute () throws Exception
    {
        validateOptions();
        String eipId    = getOptionalOperand("--eip-id");

        // If the eipId is a number, then we can assume it is an index into the EipMap.  If it is not
        // a number, then we'll assume it is the eipId.
        eipId = getEIPListNumber(eipId);

        log_.fine ("DEBUG: eipId: " + eipId);

        EIPManager mgr = getEIPManagerService();

        try {
            QName qname = QName.valueOf(eipId);
            Yaml yaml = new Yaml();
            HashMap map = (HashMap)mgr.listConfigIdsInfo(qname);
            String configString = (String)map.get("config-ids");
            String name = (String)map.get("name");
            String[] configs = null;
            if (configString != null || !configString.equalsIgnoreCase(""))
            {
                String header = sLoc_.tNoID("1015: Configurations For EIP {0}",name);
                displayHeader(header);
                configs = configString.split("\\,");
                for (int i=0; i<configs.length; i++)
                {
                    out_.println (configs[i].trim());
                }
            }
        }
        catch (Throwable e) {
            String msg = sLoc_.t("0029: {0}",e.getMessage());
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

}
