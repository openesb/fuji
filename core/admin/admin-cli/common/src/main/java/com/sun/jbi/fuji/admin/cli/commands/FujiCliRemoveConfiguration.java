/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliDeleteConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.IOException;
import java.io.FileInputStream;
import java.util.Properties;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;

public class FujiCliRemoveConfiguration extends FujiCliCommand
{

    public FujiCliRemoveConfiguration () 
    {
        // Initialize the options for this command
        FujiCliOption name = new FujiCliOption ("--name",true);
        addOption(name);
        setHelpOperand("param1[,param2,*]");
    }

    public void execute () throws Exception
    {
        validateOptions();
        String name    = (String)optionProperties_.get("--name");
        String operand = (String)optionProperties_.get("=");
        Properties deleteProperties = new Properties();
        try
        {
            if (operand == null) {
                String msg = sLoc_.t("0004: The command is missing a required operand.");
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
            deleteProperties.load(new FileInputStream(operand));
        }
        catch (IOException e)
        {
            // Replace all equal signs with a comma if they are not escaped
            operand = operand.replaceAll("(?<![\\\\])=",",");
            deleteProperties = FujiCliUtilities.stringToPropertiesList(operand);
        }

        String restrictedValues = getRestrictedValues(deleteProperties);
        if (restrictedValues != "")
        {
            String[] buffer = restrictedValues.split(",");
            String msg = sLoc_.t("0017: The variable {0} is a internal read only variable that can not be deleted.",restrictedValues);
            if (buffer.length > 1)
            {
                msg = sLoc_.t("0020: The variables {0} are internal read only variables that can not be deleted.",restrictedValues);
            }
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        //deleteProperties = restrictIO(deleteProperties);
        deleteConfiguration (name, deleteProperties);
    }

}
