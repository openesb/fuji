package com.sun.jbi.fuji.admin.cli;

import java.util.regex.Pattern;

public class Localizer extends net.java.hulp.i18n.LocalizationSupport {
    public Localizer() {
        super(Pattern.compile("(\\d\\d\\d\\d)(: )(.*)", Pattern.DOTALL),
              "ESBCLI-",
              null);
    }

    private static final Localizer s = new Localizer();

    public static Localizer get() {
        return s;
    }
}
