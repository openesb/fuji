/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import com.sun.jbi.clientservices.interceptor.AspectManager;
import org.osgi.framework.ServiceReference;


public class FujiCliDeleteAspect extends FujiCliCommand
{

    public FujiCliDeleteAspect () 
    {
        FujiCliOption name = new FujiCliOption ("--name",true);
        name.setOptionalOperand(true);
        addOption(name);
    }

    public void execute () throws Exception
    {
        validateOptions();
        String name = getOptionalOperand("--name");

        ServiceReference svcRef = context_.getServiceReference(AspectManager.class.getName());
        Object svc = context_.getService(svcRef);
        AspectManager mgr = (AspectManager)svc;
        log_.fine ("DEBUG: name: " + name);
        mgr.delete(name);
    }

}
