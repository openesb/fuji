/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliListComponents.java - Last published on 03/20/2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FujiCliListComponents extends FujiCliCommand
{

    public FujiCliListComponents () 
    {
        List<String> typeOptions = Arrays.asList("all","binding","bindings","engine","engines","*");
        FujiCliOption type = new FujiCliOption ("--type","*",typeOptions,false);

        addOption(type);
    }

    public void execute () throws Exception
    {
        validateOptions();
        List<String> bindingList = new ArrayList<String>();
        List<String> engineList = new ArrayList<String>();
        String componentType = (String)optionProperties_.get("--type");

        boolean prefix = false;
        String header = sLoc_.tNoID("1010: Binding & Engine Components");
        if (componentType.equalsIgnoreCase("all") ||
            componentType.equalsIgnoreCase("*"))
        {
            bindingList = getJBIComponentNames("BindingComponents");
            engineList = getJBIComponentNames("EngineComponents");
            prefix = true;
        }
        else if ((componentType.equalsIgnoreCase("binding")) ||
                 (componentType.equalsIgnoreCase("bindings")))
        {
            bindingList = getJBIComponentNames("BindingComponents");
            header = sLoc_.tNoID("1011: Binding Components");
        }
        else if ((componentType.equalsIgnoreCase("engine")) ||
                 (componentType.equalsIgnoreCase("engines")))
        {
            engineList = getJBIComponentNames("EngineComponents");
            header = sLoc_.tNoID("1012: Engine Components");
        }

        // Lets sort each list.
        Collections.sort(bindingList);
        Collections.sort(engineList);

        displayHeader(header);
        String compType = "";
        for (String name: bindingList) {
            if (prefix)
                compType = sLoc_.tNoID("1013: Binding") + ": ";
            out_.println (compType + name);
        }
        for (String name: engineList) {
            if (prefix)
                compType = sLoc_.tNoID("1014: Engine") + " : ";
            out_.println (compType + name);
        }
    }

}
