/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Properties;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Vector;
import com.sun.jbi.fuji.admin.cli.commands.FujiCliCommand;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;
import com.sun.jbi.fuji.admin.cli.Localizer;
import com.sun.jbi.configuration.ServicePID;

public class FujiCliConfiguration
{
    private Properties pidProperties_     = new Properties();
    private Properties configProperties_  = new Properties();
    private Localizer sLoc_ = Localizer.get();


    public FujiCliConfiguration (String pid)
    {
        pidProperties_.put ("pid",pid);
    }


    public FujiCliConfiguration (String type, String name)
    {
        String pid = ServicePID.getServicePID(type,name);
        pidProperties_.put ("pid",pid);
    }


    public void setConfigProperties (Dictionary configProp)
    {
        Enumeration e = configProp.keys();
        while (e.hasMoreElements())
        {
            String key = (String)e.nextElement();
            String value = (String)configProp.get(key);
            configProperties_.put(key,value);
        }
    }


    public void setConfigProperties (Dictionary configProp, 
                                     String keyFilter, 
                                     String valueFilter)
    {
        boolean addIt = true;
        Enumeration e = configProp.keys();
        while (e.hasMoreElements())
        {
            String key = (String)e.nextElement();
            String value = (String)configProp.get(key);
            if (keyFilter != null) {
                addIt = filterString(keyFilter,key);
            }

            if (valueFilter != null) {
                if (addIt) {
                    addIt = filterString(valueFilter,value);
                }
            }
            if (addIt) {
                configProperties_.put(key,value);
            }
        }
    }


    public void setConfigProperties (Vector configProp, 
                                     String keyFilter, 
                                     String valueFilter)
    {
        boolean addIt = true;
        Enumeration e = configProp.elements();
        while (e.hasMoreElements())
        {
            String element = (String)e.nextElement();
            String[] pidTokens = element.split(",");
            for (int i=0; i<pidTokens.length; i++)
            {
                String key   = "";
                String value = "";
                String[] pidElement = pidTokens[i].split("=");

                if (pidElement.length == 1)
                {
                    key = pidElement[0];
                }
                else
                {
                    key   = pidElement[0];
                    value = pidElement[1];
                }
                if (keyFilter != null) {
                    addIt = filterString(keyFilter,key);
                }

                if (valueFilter != null) {
                    if (addIt) {
                        addIt = filterString(valueFilter,value);
                    }
                }
                if (addIt) {
                    configProperties_.put(key,value);
                }
            }
        }
    }


    public void updateConfigProperties (Dictionary configProp) throws Exception
    {
        Enumeration e = configProp.keys();
        while (e.hasMoreElements())
        {
            String key = (String)e.nextElement();
            String value = (String)configProp.get(key);
            if (!(configProperties_.containsKey(key)))
            {
                String msg = sLoc_.t("0013: The logger specified {0} does not exist.",key);
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
            else
            {
                configProperties_.put(key,value);
            }
        }
    }


    public void deleteConfigProperties (Properties configProp) throws Exception
    {
        int errorCount = 0;
        String errorString = "";
        Enumeration e = configProp.keys();
        if ((configProp.size() == 1) && (configProp.containsKey("*")))
        {
            configProperties_ = new Properties();
        }
        else
        {
            String comma = "";
            while (e.hasMoreElements())
            {
                String key = (String)e.nextElement();
                if (!(configProperties_.containsKey(key)))
                {
                    errorCount++;
                    errorString += (comma + "\"" + key + "\"");
                    comma = ", ";
                }
                else
                {
                   configProperties_.remove(key);
                }
            }
            if (errorCount > 0)
            {
                String msg = sLoc_.t("0018: Delete failed, the following variable {0} does not exist.",errorString);
                if (errorCount > 1)
                {
                    msg = sLoc_.t("0019: Delete failed, the following variables {0} do not exist.",errorString);
                }
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
        }
    }


    public Properties getConfigProperties ()
    {
        return configProperties_;
    }


    public Vector getConfigPropertyVector ()
    {
        Vector returnVector = new Vector();
        Enumeration e = configProperties_.keys();
        while (e.hasMoreElements())
        {
            String key   = (String)e.nextElement();
            String value = (String)configProperties_.get(key);
            String propertyString = key + "=" + value;
            returnVector.add(propertyString);
        }
        return returnVector;
    }


    public String getName()
    {
        String name = getPidPropertyValue("name");
        if (name == null)
        {
            name = getPidPropertyValue("pid");
        }
        return name;
    }


    public String getType()
    {
        return getPidPropertyValue("type");
    }


    public String getPid()
    {
        return getPidPropertyValue("pid");
    }


    public String getPidPropertyValue (String key)
    {
        String propertyValue = (String)pidProperties_.get(key);
        return propertyValue;
    }


    public void setPidProperties()
    {
        setPidProperties((String)pidProperties_.get("pid"));
    }


    public void setPidProperties(String thePid)
    {
        ServicePID servicePid = new ServicePID(thePid);
        String pidDomain = servicePid.getDomain();
        String pidType   = servicePid.getType();
        String pidName   = servicePid.getName();
        setPidProperty("domain",pidDomain);
        setPidProperty("type",pidType);
        setPidProperty("name",pidName);
    }


    public void setPidProperty (String key, String value)
    {
        key = cleanUpPid(key).toLowerCase();
        value = cleanUpPid(value);
        pidProperties_.put (key,value);
    }


    public static String cleanUpPid (String pid)
    {
        // Remove any quotes that may be wrapped around the pid
        if (pid.substring(0,1).equals("\"")) {
            pid = pid.substring(1);
        }
        int len = pid.length();
        if (pid.substring(len-1).equals("\"")) {
            pid = pid.substring(0,len-1);
        }
        return pid;
    }


    private boolean filterString (String filter, String value)
    {
        String filterText = "";
        int filterLen = filter.length(); 
        if ((filter.startsWith("*")) && (filter.endsWith("*")))
        {
            filterText = filter.substring(1,filterLen-1);
            if (value.indexOf(filterText) != -1)
            {
                return true;
            }
        }
        else if (filter.startsWith("*"))
        {
            filterText = filter.substring(1);
            if (value.endsWith(filterText))
            {
                return true;
            }
        }
        else if (filter.endsWith("*"))
        {
            filterText = filter.substring(0,filterLen-1);
            if (value.startsWith(filterText))
            {
                return true;
            }
        }
        return false;
    }


    public String toString() {
        String theString = null;
        theString += "pidProperties: \n";
        theString += pidProperties_.toString() + "\n";
        theString += "configProperties: \n";
        theString += configProperties_.toString() + "\n";
        return theString;
    }

}
