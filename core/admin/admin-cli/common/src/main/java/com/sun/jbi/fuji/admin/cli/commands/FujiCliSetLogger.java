/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetLogger.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.Properties;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;


public class FujiCliSetLogger extends FujiCliCommand
{

    public FujiCliSetLogger () 
    {
        FujiCliOption componentName = new FujiCliOption ("--component",false);
        componentName.setHelpName("component-name");

       // String defaultName = " ";  
       // FujiCliOption componentName = new FujiCliOption ("--component",defaultName,null,false);
       // componentName.setHelpName("component-name");

        String overrideDefault = "off";
        List<String> overrideOptions = Arrays.asList("on","off");
        FujiCliOption override = new FujiCliOption ("--override",overrideDefault,overrideOptions,false);
        override.setShowHelp(false);

        addOption(componentName);
        addOption(override);

        setHelpOperand("<logLevelFile> | <loggerName=loggerLevel[,loggerName=loggerLevel,*]>");


    }

    public void execute () throws Exception
    {
        validateOptions();
        String componentName = (String)optionProperties_.get("--component");
        String override      = (String)optionProperties_.get("--override");
        String operand       = (String)optionProperties_.get("=");

        Properties properties = new Properties();
        try
        {
            if (operand == null) {
                String msg = sLoc_.t("0004: The command is missing a required operand.");
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
            properties.load(new FileInputStream(operand));
        }
        catch (IOException e)
        {
            // Replace all equal signs with a comma if they are not escaped
            operand = operand.replaceAll("(?<![\\\\])=",",");
            properties = FujiCliUtilities.stringToProperties(operand);
        }
        setLogger (componentName, properties, override);
    }

}
