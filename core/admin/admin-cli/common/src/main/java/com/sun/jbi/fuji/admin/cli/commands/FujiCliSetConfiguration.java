/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.IOException;
import java.io.FileInputStream;
import java.util.Properties;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;


public class FujiCliSetConfiguration extends FujiCliCommand
{

    public FujiCliSetConfiguration () 
    {
        // Initialize the options for this command
        FujiCliOption name = new FujiCliOption ("--name",true);
        addOption(name);
        setHelpOperand("<configFile> | <key=value[,key=value,*]>");
    }

    public void execute () throws Exception
    {
        validateOptions();
        String name    = (String)optionProperties_.get("--name");
        String operand = (String)optionProperties_.get("=");

        Properties newProperties = new Properties();
        try
        {
            if (operand == null) {
                String msg = sLoc_.t("0004: The command is missing a required operand.");
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
            newProperties.load(new FileInputStream(operand));
        }
        catch (IOException e)
        {
            // Replace all equal signs with a comma if they are not escaped
            operand = operand.replaceAll("(?<![\\\\])=",",");
            newProperties = FujiCliUtilities.stringToProperties(operand);
        }
        String restrictedValues = getRestrictedValues(newProperties);
        if (restrictedValues != "")
        {
            String[] buffer = restrictedValues.split(",");
            String msg = sLoc_.t("0015: The configuration variable {0} is a fuji read only internal variable that can not be altered.",restrictedValues);
            if (buffer.length > 1)
            {
                msg = sLoc_.t("0016: The configuration variables {0} are fuji read only internal variables that can not be altered.",restrictedValues);
            }
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }

        // Not needed anymore, since we are displaying an error message if any restricted values were being set.
        //newProperties = restrictIO(newProperties);  // Remove any restricted variables


        setConfiguration (name, newProperties);
    }

}
