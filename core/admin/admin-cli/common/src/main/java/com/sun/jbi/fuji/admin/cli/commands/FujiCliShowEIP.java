/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import org.osgi.framework.ServiceReference;
import javax.xml.namespace.QName;
import java.util.HashMap;
import com.sun.jbi.clientservices.eip.EIPManager;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;
import org.ho.yaml.YamlDecoder;
import org.ho.yaml.Yaml;


public class FujiCliShowEIP extends FujiCliCommand
{

    public FujiCliShowEIP () 
    {
        FujiCliOption eipId = new FujiCliOption ("--eip-id","",null,true);
        eipId.setOptionalOperand(true);        

        FujiCliOption configId = new FujiCliOption ("--config-id","",null,true);
        
        addOption(eipId);
        addOption(configId);
    }

    public void execute () throws Exception
    {
        validateOptions();
        String eipId    = getOptionalOperand("--eip-id");
        String configId = (String)optionProperties_.get("--config-id");

        // If the eipId is a number, then we can assume it is an index into the EipMap.  If it is not
        // a number, then we'll assume it is the eipId.
        eipId = getEIPListNumber(eipId);

        log_.fine ("DEBUG: eipId: " + eipId);
        log_.fine ("DEBUG: configId: " + configId);

        EIPManager mgr = getEIPManagerService();

        try {
            QName qname = QName.valueOf(eipId);
            Yaml yaml = new Yaml();
            HashMap map = (HashMap)mgr.get(qname,configId);
            String dump = yaml.dump(map);
            dump = FujiCliUtilities.cleanUpYamlDump(dump);
            out_.println(dump);
        }
        catch (Throwable e) {
            String msg = sLoc_.t("0029: {0}",e.getMessage());
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

}
