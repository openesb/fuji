/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.FileInputStream;
import java.util.HashMap;
import org.ho.yaml.exception.YamlException;
import org.ho.yaml.YamlDecoder;
import org.ho.yaml.Yaml;
import com.sun.jbi.clientservices.eip.EIPManager;


public class FujiCliAddEIP extends FujiCliCommand
{

    public FujiCliAddEIP () 
    {
        FujiCliOption filename = new FujiCliOption ("--file",true);
        filename.setOptionalOperand(true);
        addOption(filename);
    }

    public void execute () throws Exception
    {
        String msg = "";
        validateOptions();
        String filename = getOptionalOperand("--file");

        FileInputStream fis = getFileInputStream(filename);
        EIPManager mgr = getEIPManagerService();

        try {
            HashMap<String, Object> obj1 = (HashMap<String, Object>) Yaml.load(fis);
            fis.close();
            String dump = Yaml.dump(obj1);
            log_.fine ("DEBUG: " + dump);
            mgr.add(obj1);
        }
        catch (YamlException e) {
            msg = sLoc_.t("0027: Yaml format error in file: {0} : {1}",filename,e.getMessage());
            fis.close();
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        catch (Exception e) {
            msg = sLoc_.t("0029: {0}",e.getMessage());
            fis.close();
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

}
