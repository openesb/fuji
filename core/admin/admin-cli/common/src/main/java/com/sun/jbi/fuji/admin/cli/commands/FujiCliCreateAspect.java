/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetConfiguration.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.IOException;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.HashMap;
import com.sun.jbi.clientservices.interceptor.AspectManager;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;
import org.osgi.framework.ServiceReference;


public class FujiCliCreateAspect extends FujiCliCommand
{

    public FujiCliCreateAspect () 
    {
        FujiCliOption config = new FujiCliOption ("--config","",null,true);
        config.setOptionalOperand(true);  

        addOption(config);

        setHelpOperand("| <key=value[,key=value,*]>");
    }

    public void execute () throws Exception
    {
        String msg = null;
        validateOptions();
        String operand = getOptionalOperand("--config");

        Properties newProperties = new Properties();

        // First lets assume that the operand is a file.  If that doesn't work out, we will
        // assume it is a string of name value pairs.
        try
        {
            if (operand == null) {
                msg = sLoc_.t("0004: The command is missing a required operand.");
                Exception fujiException = new Exception (msg);
                throw (fujiException);
            }
            newProperties.load(new FileInputStream(operand));
        }
        catch (IOException e)
        {
            // Replace all equal signs with a comma if they are not escaped
            operand = operand.replaceAll("(?<![\\\\])=",",");
            newProperties = FujiCliUtilities.stringToProperties(operand);
        }

        ServiceReference svcRef = context_.getServiceReference(AspectManager.class.getName());
        Object svc = context_.getService(svcRef);
        AspectManager mgr = (AspectManager)svc;
        HashMap newMap = FujiCliUtilities.propertiesToHashMap(newProperties);
        log_.fine ("DEBUG: newMap: " + newMap);
        try {
            String name = mgr.create(newMap);
            msg = sLoc_.tNoID("1017: Created Aspect: {0}",name);
            out_.println (msg);
        }
        catch (Throwable e) {
            msg = sLoc_.t("0029: {0}",e.getMessage());
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
    }

}
