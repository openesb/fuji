/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliGetLogger.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import com.sun.jbi.fuji.admin.cli.framework.FujiCliUtilities;
import java.util.Enumeration;
import java.util.Properties;

public class FujiCliListLoggers extends FujiCliCommand
{

    public FujiCliListLoggers () 
    {
        FujiCliOption componentName = new FujiCliOption ("--component",false);
        componentName.setHelpName("component-name");

        FujiCliOption filter = new FujiCliOption ("--filter",false);

        addOption(componentName);
        addOption(filter);
    }

    public void execute () throws Exception
    {
        validateOptions();
        String filter        = (String)optionProperties_.get("--filter");
        String componentName = (String)optionProperties_.get("--component");
        if (componentName == null)
        {
            componentName = (String)optionProperties_.get("=");
        }
        FujiCliConfiguration config = getLoggers(componentName,filter);
        if (config == null)
        {
            String msg = sLoc_.t("0011: No loggers was found for {0}.",componentName);
            Exception fujiException = new Exception (msg);
            throw (fujiException);
        }
        else
        {
            String msg = sLoc_.tNoID("1004: Fuji Runtime Loggers");
            if (componentName != null) {
                msg = sLoc_.tNoID("1005: Loggers For Component {0}",componentName);
            }
            String underline = createFillString('-',msg.length());
            out_.println ("\n"+msg);
            out_.println (underline);
            Properties outputProperties = restrictIO(config.getConfigProperties());
           // Enumeration e = outputProperties.keys();
            Enumeration e = FujiCliUtilities.getSortedPropertiesKeys(outputProperties);
            while (e.hasMoreElements()) {
                String logger = (String)e.nextElement();
                String level  = (String)config.getConfigProperties().get(logger);
                out_.println (logger + " = " + level);
            }
        }
    }

}
