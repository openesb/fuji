/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliOption.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class FujiCliOption
{
    private String name_ = "";
    private String helpName_ = "";
    private String defaultValue_ = "";
    private String missingDefaultValue_ = "";
    private List legalValues_;
    private boolean required_ = true;
    private boolean optionalOperand_ = false;
    private boolean showHelp_ = true;

    public FujiCliOption (String name, 
                          String defaultValue, 
                          List legalValues, 
                          boolean required)
    {
        name_ = name;
        defaultValue_ = defaultValue;
        missingDefaultValue_ = defaultValue;
        legalValues_ = legalValues;
        required_ = required;
    }

    public FujiCliOption (String name, 
                          boolean required)
    {
        this(name,"",null,required);
    }

    public boolean isRequired ()
    {
        return required_;
    }

    public boolean isLegal (String value)
    {
        if (legalValues_ != null)
        {
            return legalValues_.contains(value);
        }
        return true;
    }

    public boolean isOptionalOperand ()
    {
        return optionalOperand_;
    }

    public String getDefault ()
    {
        return defaultValue_;
    }

    public String getMissingValueDefault ()
    {
        return missingDefaultValue_;
    }

    public String getName ()
    {
        return name_;
    }

    public String getNameOnly ()
    {
        int index = 0;
        if (name_.substring(index,index+1).equals("-"))
            index++;
        if (name_.substring(index,index+1).equals("-"))
            index++;
        String name = name_.substring(index);
        return name;
    }

    public String getHelpName() {
        if (helpName_.equals(""))
            return getNameOnly();
        else
            return helpName_;
    }

    public void setHelpName (String helpName) {
        helpName_ = helpName;
    }

    public boolean getRequired ()
    {
        return required_;
    }

    public List<String> getLegalValues ()
    {
        return legalValues_;
    }

    public void setDefault (String defaultValue)
    {
        defaultValue_ = defaultValue;
    }

    public void setMissingValueDefault (String value)
    {
        missingDefaultValue_ = value;
    }

    public void setName (String name)
    {
        name_ = name;
    }

    public void setRequired (boolean required)
    {
        required_ = required;
    }

    public void setLegalValues (List<String> values)
    {
        legalValues_ = values;
    }

    public void setOptionalOperand (boolean optionalOperand)
    {
        optionalOperand_ = optionalOperand;
    }

    public String toString ()
    {
        return name_;
    }

    public String dump ()
    {
        String d = "Name: " + name_ + "\n" +
            "Required: " + required_ + "\n" +
            "Legal Values: " + legalValues_ + "\n";
        return d;
    }

    public void setShowHelp (boolean flag) {
        showHelp_ = flag;
    }

    public boolean getShowHelp () {
        return showHelp_;
    }


}
