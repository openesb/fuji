/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliSetOptions.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.commands;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Dictionary;
import org.osgi.framework.BundleContext;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;


public class FujiCliSetOption extends FujiCliCommand
{

    public FujiCliSetOption () 
    {
    }

    public void execute () throws Exception
    {
        validateOptions();
        if (optionProperties_.size() > 1)
        {
            Enumeration e = optionProperties_.keys();
            while (e.hasMoreElements())
            {
                String key = ((String)e.nextElement()).trim();
                String value = ((String)optionProperties_.getProperty(key)).trim();
                if (!(key.equals(FujiCliImpl.cliShellName_)))
                {
                    // if key is "=" then it thinks it is an operand and the value
                    // will be the key that needs to be removed. (i.e. set-option key =)
                    if (key == "=") {
                        FujiCliImpl.setOptionValue_.remove(value);
                    }
                    // else we are adding a new entry into the default option 
                    else {
                        FujiCliImpl.setOptionValue_.put(key,value);
                    }
                }
            }
        }
        else
        {
            Enumeration e2 = FujiCliImpl.setOptionValue_.keys();
            while (e2.hasMoreElements())
            {
                String key = (String)e2.nextElement();
                String value = (String)FujiCliImpl.setOptionValue_.getProperty(key);
                out_.println (key + " = " + value);
            }
        }
    }

}
