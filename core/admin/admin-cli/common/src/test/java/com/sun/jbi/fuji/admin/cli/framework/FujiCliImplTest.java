/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliImplTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.framework;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;
import java.util.Properties;
import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;

/**
 * Unit tests for Fuji Cli Impl class.
 * @author markrs
 */
public class FujiCliImplTest extends TestCase {

    public FujiCliImplTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
       
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test the getCommand List function.
     */
    public void testTest1() throws Exception {
        FujiCliImpl cli = new FujiCliImpl();
        String cmdString = "fuji list-configurations --filter=*name* --full=true";
        List cmdList = cli.getCommandList(cmdString);
        String cmdListStr = cmdList.toString();
        assertEquals(cmdList.toString(),"[fuji, list-configurations, --filter, *name*, --full, true]");
    }

}

