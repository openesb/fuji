/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliUtilitiesTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.framework;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Properties;


/**
 * Unit tests for Fuji Cli Utilities class.
 * @author markrs
 */
public class FujiCliUtilitiesTest extends TestCase {

    public FujiCliUtilitiesTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
       
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testStringToProperties() throws Exception {
        String testStr = "one,two,three,   four, five,six, seven, ei ght";
        Properties testProperties = FujiCliUtilities.stringToProperties(testStr);
        assertEquals(testProperties.get("one"),"two");
        assertEquals(testProperties.get("three"),"four");
        assertEquals(testProperties.get("five"),"six");
        assertEquals(testProperties.get("seven"),"ei ght");
    }
    
    public void testStringToPropertiesList() throws Exception {
        String testStr = "one,two,three,   four, five,six, seven, ei ght";
        Properties testProperties = FujiCliUtilities.stringToPropertiesList(testStr);
        assertEquals(testProperties.get("one"),"");
        assertEquals(testProperties.get("two"),"");
        assertEquals(testProperties.get("three"),"");
        assertEquals(testProperties.get("four"),"");
        assertEquals(testProperties.get("five"),"");
        assertEquals(testProperties.get("six"),"");
        assertEquals(testProperties.get("seven"),"");
        assertEquals(testProperties.get("ei ght"),"");
    }

    public void testRemoveBackslashCharacters() throws Exception {
        String testStr = "one\\=two\\=three\\=four";
        String outStr = FujiCliUtilities.removeBackslashCharacters(testStr);
        assertEquals(outStr,"one=two=three=four");
    }

    
}
