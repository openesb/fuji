/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliActivator.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.osgi;

import com.sun.jbi.fuji.admin.cli.osgi.Localizer;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class FujiCliEquinoxActivator implements BundleActivator
{
    private transient BundleContext context_ = null;
    private Localizer sLoc_ = Localizer.get();

    public void start(BundleContext context) throws Exception
    {
        context_ = context;
        try {
            context.registerService(org.eclipse.osgi.framework.console.CommandProvider.class.getName(), 
                                    new FujiCliEquinox(context_), 
                                    null);
        }
        catch (Exception ex) {
            String msg = sLoc_.t("0001: Failed to register the Fuji CLI bundle into the Equinox Runtime.");
            System.out.println (msg);
        }

 //       context_ = context;

        //String msg = sLoc_.t("0001: The very first test message");
        //System.out.println (msg);

        //String msg2 = sLoc_.tNoID("0002: The very second test message");
        //System.out.println (msg2);

	//	boolean registered = false;


      //  try {
//			Class cmdClass = Class.forName("org.eclipse.osgi.framework.console.CommandProvider");
//			context.registerService(cmdClass.getName(), new FujiCliEquinox(context), null);
	  //  	registered = true;
	//	}
	//	catch (ClassNotFoundException eclipseError) {
     //       System.err.println ("***** Unable to register with eclipse");
	//	}

	  //  try {
		//	Class cmdClass = Class.forName("org.apache.felix.shell.Command");
		//	context.registerService(cmdClass.getName(), new FelixCmCommand(context), null);
		//	registered = true;
		//}
		//catch (ClassNotFoundException felixError) {
        //    System.err.println ("***** Unable to register with felix");
		//}

	
     //   if (! registered) {
//			System.err.println("Could not register command (command class not found). "
//					+ "Note that this bundle only works with the Felix or Equinox command shell.");
//		}

        // Register the command shell service.
     //   context.registerService(org.apache.felix.shell.Command.class.getName(),
     //       new FujiCliImpl(context_), null);
    }  

    public void stop(BundleContext context)
    {
        // Services are automatically unregistered so
        // we don't have to unregister the factory here.
    }
}
