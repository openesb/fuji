/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FujiCliActivator.java - Last published on 11/20/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.cli.osgi;

import java.util.ArrayList;
import java.util.List;

import com.sun.jbi.fuji.admin.cli.framework.FujiCliImpl;
import org.eclipse.osgi.framework.console.CommandInterpreter;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.osgi.framework.BundleContext;

public class FujiCliEquinox implements CommandProvider
{
	protected BundleContext context;

    // This is the name that is uses to run the fuji cli program"
    public static final String CLI_SHELL_NAME = "fuji";

	
	public FujiCliEquinox(BundleContext context) {
		this.context = context;
	}

	public String getHelp() {
		return "basic help";
	}


/*
    public String createTokenString (String tokenStr, String cmdArgument)
    {
        arg = ci.nextArgument();
        if (arg != null)
        {
            tokenStr += arg
        }

		while ((arg = ci.nextArgument()) != null) {	  
            String arg = arg.trim();

            int i = indexOf("=");
            if (i == 0)
            {
                if (arg.length() == 1)
                {
                    arg = ci.nextArgument();
                    if (arg == null)
                    {
                        return tokenString;
                    }
                }
            }


            elde if 
            )
            {
            }
                if (arg != null)
                {
                    tokenStr += "=" + arg.trim();
                }
            }

            else {

            }
            return tokenStr;
        }
        else 
            int i = indexOf("=");
            if (i > -1)
            {
                tokenStr += arg.substring(0,i);
                if (i < arg.length())
                {
                    tokenStr += "\"" + argsubstring(i) + "\"";
                }
            }
        }
        System.out.println ("arg:" + arg);
        if ()
        {
        }
    }
*/

	public void _fuji(CommandInterpreter ci)
	{
        List<String> tokens = new ArrayList<String>();
		String arg = null;
        String cmdString = CLI_SHELL_NAME;
		while ((arg = ci.nextArgument()) != null) {	  
            cmdString += ", " + arg;
		}
		
		FujiCliImpl impl = new FujiCliImpl(context);
        impl.execute(CLI_SHELL_NAME,
                     cmdString, 
                     System.out, 
                     System.err);

	}

}
