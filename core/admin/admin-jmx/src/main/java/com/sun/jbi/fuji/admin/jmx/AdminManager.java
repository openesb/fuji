/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminManager.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import java.lang.management.ManagementFactory;
import javax.management.MBeanServer;
import org.osgi.framework.BundleContext;

/**
 * AdminManager where you manage creation of the required mbean implemenatations and registering and unregistering 
 * in mbean server and other initialization.
 * 
 * @author chikkala
 */
public class AdminManager {

    public static final String DEPRECATED_FUJI_ADMIN_OBJECT_NAME = "con.sun.jbi.fuji:runtime=OSGi,type=admin,service=admin";
    public static final String FUJI_JMX_DOMAIN = "com.sun.jbi.fuji";
    public static final String FUJI_ADMIN_OBJECT_NAME = FUJI_JMX_DOMAIN + ":runtime=OSGi,type=admin,service=admin";

    private AdminMBeanImpl mAdminImpl;

    public AdminManager() {
        mAdminImpl = new AdminMBeanImpl();
    }

    public AdminMBeanImpl getAdminMBeanImpl() {
        return mAdminImpl;
    }

    public MBeanServer getMBeanServer() {
        return ManagementFactory.getPlatformMBeanServer();
    }

    public void registerMBeans() {
        MBeanServer mbs = getMBeanServer();
        mAdminImpl.registerMBean(mbs);
    }

    public void unregisterMBeans() {
        MBeanServer mbs = getMBeanServer();
        mAdminImpl.unregisterMBean(mbs);
    }

    public void start(BundleContext ctx) {
        mAdminImpl.setContext(ctx);
        registerMBeans();
    }

    public void stop(BundleContext ctx) {
        unregisterMBeans();
    }
}
