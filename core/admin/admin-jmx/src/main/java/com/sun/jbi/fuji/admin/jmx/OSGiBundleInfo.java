/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OSGiBundleInfo.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

/**
 * Bean that represents the osgi bundle info and has a serialization/deserialization 
 * code to/from jmx open types that represent the data.
 * 
 * @author chikkala
 */
public class OSGiBundleInfo {

    // bundle properties
    public static final String PROP_BUNDLE_ID = "bundleID";
    public static final String PROP_SYMBOLIC_NAME = "symbolicName";
    public static final String PROP_STATE = "state";
    public static final String PROP_JBI_ARTIFACT_TYPE = "jbiArtifactType";
    public static final String PROP_LAST_MODIFIED = "lastModified";
    public static final String PROP_LOCATION = "location";
    public static final String PROP_HEADERS = "headers";
    public static final String PROP_JBI_DESCRIPTOR = "jbiDescriptor";
    // OSGi States
    public static final String STATE_ACTIVE = "Active";
    public static final String STATE_RESOLVED = "Resolved";
    public static final String STATE_INSTALLED = "Installed";
    public static final String STATE_STARTING = "Starting";
    public static final String STATE_STOPPING = "Stopping";
    public static final String STATE_UNINSTALLED = "Uninstalled";
    public static final String STATE_UNKNOWN = "Unknown";  // Not a OSGi state.
    // JBI Types

    public static final String JBI_BC = "binding-component";
    public static final String JBI_SE = "service-engine";
    public static final String JBI_SA = "service-assembly";
    public static final String JBI_SL = "shared-library";
    public static final String JBI_UNKNOWN = "Unknown";
    // jmx tabular data names
    public static final String[] ITEM_NAMES = {
        PROP_BUNDLE_ID,
        PROP_SYMBOLIC_NAME,
        PROP_STATE,
        PROP_JBI_ARTIFACT_TYPE,
        PROP_LAST_MODIFIED,
        PROP_LOCATION,
        PROP_HEADERS,
        PROP_JBI_DESCRIPTOR
    };

    // properties
    private String bundleId;
    private String symbolicName;
    private String state;
    private String jbiArtifactType;
    private String location;
    private String lastModified;
    private OSGiPropertyInfoMap headers;
    private String jbiDescriptor;

    /**
     * constructor
     */
    public OSGiBundleInfo() {
        headers = new OSGiPropertyInfoMap();
    }

    /**
     * Get the value of lastModified
     * @return
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     *
     * @param lastModified
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     *
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Get the value of jbiArtifactType
     *
     * @return the value of jbiArtifactType
     */
    public String getJbiArtifactType() {
        return jbiArtifactType;
    }

    /**
     * Set the value of jbiArtifactType
     *
     * @param jbiArtifactType new value of jbiArtifactType
     */
    public void setJbiArtifactType(String jbiArtifactType) {
        this.jbiArtifactType = jbiArtifactType;
    }

    public String getJbiDescriptor() {
        return jbiDescriptor;
    }

    public void setJbiDescriptor(String jbiDescriptor) {
        this.jbiDescriptor = jbiDescriptor;
    }

    /**
     * Get the value of symbolicName
     *
     * @return the value of symbolicName
     */
    public String getSymbolicName() {
        return symbolicName;
    }

    /**
     * Set the value of symbolicName
     *
     * @param symbolicName new value of symbolicName
     */
    public void setSymbolicName(String symbolicName) {
        this.symbolicName = symbolicName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public List<OSGiPropertyInfo> getHeaders() {
        return headers.getProperties();
    }

    public void setHeaders(List<OSGiPropertyInfo> headers) {
        this.headers.setProperties(headers);
    }

    public String getHeader(String name) {
        return this.headers.getPropertyValue(name);
    }

    public static OpenType[] getItemTypes() throws OpenDataException {
        TabularType headersType = OSGiPropertyInfo.getTabularType();
        OpenType[] itemTypes = {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            headersType,
            SimpleType.STRING
        };
        return itemTypes;
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiBundleInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemNames = ITEM_NAMES;
        String[] itemDescs = ITEM_NAMES;
        OpenType[] itemTypes = getItemTypes();
        type = new CompositeType(typeName, typeDesc, itemNames, itemDescs, itemTypes);
        return type;
    }

    public CompositeData toCompositeData() throws OpenDataException {
        CompositeData data = null;
        CompositeType type = getCompositeType();
        String[] itemNames = ITEM_NAMES;
        TabularData headerData = OSGiPropertyInfo.toTabularData(this.getHeaders());
        Object[] itemValues = {
            this.getBundleId(),
            this.getSymbolicName(),
            this.getState(),
            this.getJbiArtifactType(),
            this.getLastModified(),
            this.getLocation(),
            headerData,
            this.getJbiDescriptor()
        };
        data = new CompositeDataSupport(type, itemNames, itemValues);
        return data;
    }

    public static OSGiBundleInfo toOSGiBundleInfo(CompositeData data) throws OpenDataException {
        OSGiBundleInfo info = new OSGiBundleInfo();
//        if (!data.getCompositeType().equals(getCompositeType())) {
//            throw new OpenDataException("Can not convert ");
//        }
        info.setBundleId((String) data.get(PROP_BUNDLE_ID));
        info.setSymbolicName((String) data.get(PROP_SYMBOLIC_NAME));
        info.setState((String) data.get(PROP_STATE));
        info.setJbiArtifactType((String) data.get(PROP_JBI_ARTIFACT_TYPE));
        info.setLastModified((String) data.get(PROP_LAST_MODIFIED));
        info.setLocation((String) data.get(PROP_LOCATION));
        TabularData headersData = (TabularData) data.get(PROP_HEADERS);
        List<OSGiPropertyInfo> headersList = new ArrayList<OSGiPropertyInfo>();
        if (headersData != null) {
            headersList = OSGiPropertyInfo.toOSGiPropertyInfoList(headersData);
        }
        info.setHeaders(headersList);
        info.setJbiDescriptor((String) data.get(PROP_JBI_DESCRIPTOR));
        return info;
    }

    public static List<OSGiBundleInfo> toOSGiBundleInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>) tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiBundleInfo info = OSGiBundleInfo.toOSGiBundleInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiBundleInfo> list) throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiBundleInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiBundleInfo.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiBundleInfo.ITEM_NAMES);
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiBundleInfo info : list) {
            tdata.put(info.toCompositeData());
        }
        return tdata;
    }
}
