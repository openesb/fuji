/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMXConnector.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.admin.jmx;

import java.lang.management.ManagementFactory;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import org.osgi.framework.BundleContext;

/**
 * This class provides the facility to start and stop a JMX connector server
 * so that tools such as jconsole can be used to examine the runtime.
 * @author mwhite (with some code cribbed from kcbabo's JBI Java SE work)
 * @author chikkala
 */
public class JMXConnector  {
    /** Logger for all messages. */
    private Logger logger_;
    private Localizer sLoc_;

    /** JMX connector server to allow JMX RMI connections. */
    private JMXConnectorServer jmxServer_;

    /** RMI registry. */
    private Registry rmiRegistry_;

    /** Port number used for JMX RMI connections. */
    private int connectorPort_;

    /** Connector port property used to override the default port number. */
    public static final String CONNECTOR_PORT = "com.sun.jbi.fuji.admin.jmx.connectorPort";

    /** 
     * Create a new instance of the JMX connector helper class.
     * @param context the OSGi bundle context.
     */
    public JMXConnector(BundleContext context) {
        logger_ = Logger.getLogger(this.getClass().getPackage().getName());
        sLoc_ = Localizer.get();
        
        // Get connector port value; if not specified no connector is started
        connectorPort_ = 0;
        String port = context.getProperty(CONNECTOR_PORT);
//        if ( port == null || port.trim().length() == 0 ) {
//            port = "8699";
//        } 
        if (port != null) {
            try {
                connectorPort_ = Integer.parseInt(port);
            }
            catch (NumberFormatException nfEx) {
                logger_.log(Level.WARNING,
                            sLoc_.t("0001: Invalid connector server port: {0} . Remote JMX connector will not be created.",
                                    connectorPort_), nfEx);
            }
        }
    }
    
    /** 
     * Start the JMX connector server.
     */
    public void start() {
        // Setup the remote JMX connector server
        if (connectorPort_ != 0) {
            HashMap<String, String> map = new HashMap<String, String>();
            // "java.naming.factory.initial"
            map.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.rmi.registry.RegistryContextFactory");

            try {
                // Create the service URL
                JMXServiceURL serviceURL = new JMXServiceURL(
                    "service:jmx:rmi:///jndi/rmi://localhost:" +
                    connectorPort_ + "/jmxrmi");

                // Create an RMI registry instance
                rmiRegistry_ = LocateRegistry.createRegistry(connectorPort_);

                // Create and start the connector server
                jmxServer_ = JMXConnectorServerFactory.newJMXConnectorServer(
                    serviceURL, map, ManagementFactory.getPlatformMBeanServer());
                jmxServer_.start();

                logger_.info(sLoc_.t("0002: Remote JMX connector available at {0}",
                    jmxServer_.getAddress()));
            }
            catch (Exception ex) {
                connectorPort_ = 0;
                logger_.log(Level.WARNING,
                            sLoc_.t("0003: Failed to create remote JMX connector"), ex);
            }
        }
    }

    /** 
     * Stop the JMX connector server.
     */
    public void stop() {
        if (connectorPort_ != 0) {
            try {
                jmxServer_.stop();
                UnicastRemoteObject.unexportObject(rmiRegistry_, true);
            }
            catch (Exception ex) {
                logger_.log(Level.WARNING,
                            sLoc_.t("0004: Error stopping JMX connector server"), ex);
            }
        }
    }
}
