/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OSGiServiceInfo.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

/**
 * Bean that represents the osgi configuraiton info and has a serialization/deserialization 
 * code to/from jmx open types that represent the data.
 * 
 * @author chikkala
 */
public class OSGiConfigInfo {
    // property names

    public static final String PROP_PID = "pid";
    public static final String PROP_FACTORY_PID = "factoryPid";
    public static final String PROP_BUNDLE_LOCATION = "bundleLocation";
    public static final String PROP_PROPERTIES = "properties";
    // types
    public static final String[] ITEM_NAMES = {
        PROP_PID,
        PROP_FACTORY_PID,
        PROP_BUNDLE_LOCATION,
        PROP_PROPERTIES
    };
    // property values
    private String bundleLocation;
    private String pid;
    private String factoryPid;
    private OSGiPropertyInfoMap properties;

    public OSGiConfigInfo() {
        this.properties = new OSGiPropertyInfoMap();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getFactoryPid() {
        return factoryPid;
    }

    public void setFactoryPid(String factoryPid) {
        this.factoryPid = factoryPid;
    }

    public String getBundleLocation() {
        return bundleLocation;
    }

    public void setBundleLocation(String bundleLocation) {
        this.bundleLocation = bundleLocation;
    }

    public List<OSGiPropertyInfo> getProperties() {
        return this.properties.getProperties();
    }

    public void setProperties(List<OSGiPropertyInfo> properties) {
        this.properties.setProperties(properties);
    }

    public Map<String, OSGiPropertyInfo> getPropertiesMap() {
        return this.properties.getPropertiesMap();
    }

    public OSGiPropertyInfo getProperty(String name) {
        return this.properties.getProperty(name);
    }

    public String getPropertyValue(String name) {
        return this.properties.getPropertyValue(name);
    }

    public String print() {
        StringWriter writer = new StringWriter();
        PrintWriter out = new PrintWriter(writer);
        out.println("--- OSGI Config Info ----");
        out.println("pid: " + getPid());
        out.println("factoryPid: " + getFactoryPid());
        out.println("BundleLocation: " + getBundleLocation());
        out.println("-------");
        out.close();
        return writer.getBuffer().toString();
    }

    @Override
    public String toString() {
        return print();
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiConfigInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemDescs = ITEM_NAMES;
        TabularType propertiesType = OSGiPropertyInfo.getTabularType();

        OpenType[] itemTypes = {
            SimpleType.STRING,
            SimpleType.STRING,
            SimpleType.STRING,
            propertiesType
        };

        type = new CompositeType(typeName, typeDesc, ITEM_NAMES, itemDescs, itemTypes);
        return type;
    }

    public CompositeData toCompositeData() throws OpenDataException {
        CompositeData data = null;
        CompositeType type = getCompositeType();
        TabularData propertiesData = OSGiPropertyInfo.toTabularData(this.getProperties());
        Object[] itemValues = {
            this.getPid(),
            this.getFactoryPid(),
            this.getBundleLocation(),
            propertiesData
        };
        data = new CompositeDataSupport(type, ITEM_NAMES, itemValues);
        return data;
    }

    public static OSGiConfigInfo toOSGiConfigInfo(CompositeData data) throws OpenDataException {
        OSGiConfigInfo info = new OSGiConfigInfo();

        info.setPid((String) data.get(PROP_PID));
        info.setFactoryPid((String) data.get(PROP_FACTORY_PID));
        info.setBundleLocation((String) data.get(PROP_BUNDLE_LOCATION));

        TabularData propData = (TabularData) data.get(PROP_PROPERTIES);
        List<OSGiPropertyInfo> propList = new ArrayList<OSGiPropertyInfo>();
        if (propData != null) {
            propList = OSGiPropertyInfo.toOSGiPropertyInfoList(propData);
        }
        info.setProperties(propList);

        return info;
    }

    public static List<OSGiConfigInfo> toOSGiConfigInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiConfigInfo> list = new ArrayList<OSGiConfigInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>) tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiConfigInfo info = OSGiConfigInfo.toOSGiConfigInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiConfigInfo> list) throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiConfigInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiConfigInfo.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiConfigInfo.ITEM_NAMES);
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiConfigInfo info : list) {
            tdata.put(info.toCompositeData());
        }
        return tdata;
    }
}
