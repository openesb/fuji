/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Activator.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * 
 * @author chikkala
 */
public class Activator implements BundleActivator {

    private BundleContext mContext = null;
    private AdminManager mAdmimMgr = null;
    /** JMX Connector Server which allows remote JMX clients to connect to us. */
    private JMXConnector mJMXConnector;

    public void start(BundleContext context) {
        mContext = context;
        // init admim manager
        this.mAdmimMgr = new AdminManager();
        this.mAdmimMgr.start(mContext);
        // start jmx connector
        mJMXConnector = new JMXConnector(context);
        mJMXConnector.start();
    }

    public void stop(BundleContext context) {
        this.mJMXConnector.stop();
        this.mAdmimMgr.stop(context);

    }
}