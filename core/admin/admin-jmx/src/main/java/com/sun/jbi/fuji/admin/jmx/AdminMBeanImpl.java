/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminMBeanImpl.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import com.sun.jbi.framework.descriptor.Component;
import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.descriptor.ServiceAssembly;
import com.sun.jbi.framework.descriptor.SharedLibrary;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.management.openmbean.TabularData;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

/**
 * AdminMBean implemenation.
 * @author chikkala
 */
public class AdminMBeanImpl implements AdminMBean {

    private static final Logger LOG = Logger.getLogger(AdminMBeanImpl.class.getName());
    private BundleContext mContext = null;

    public void shutdown() throws Exception {
        LOG.info("Shutting down Fuji instance...");
        System.out.println("Shutting down Fuji instance...");
        try {
            Bundle bundle = this.mContext.getBundle(0);
            bundle.stop();
        //TODO: equinox require to call system.exit also along with stoping the osgi framework bundle.
        } catch (Exception ex) {
            System.err.println(ex.toString());
            LOG.log(Level.WARNING, ex.getMessage(), ex);
        }
    }
    
    public TabularData listConfigurations() throws Exception {
        List<OSGiConfigInfo> list = getConfigInfoList();
        TabularData tdata = OSGiConfigInfo.toTabularData(list);
        return tdata;
    }
    
    public TabularData listServiceRefs() throws Exception {
        List<OSGiServiceInfo> list = getServiceInfoList();
        TabularData tdata = OSGiServiceInfo.toTabularData(list);
        return tdata;
    }

    public TabularData listBundles() throws Exception {
        List<OSGiBundleInfo> list = getBundleInfoList();
        TabularData tdata = OSGiBundleInfo.toTabularData(list);
        return tdata;
    }

    public String installBundle(String bundlePath) throws Exception {
        return installBundle(bundlePath, false, false);
    }

    public String installBundle(String bundlePath, boolean start, boolean reInstall) throws Exception {
        String bundleURL = (new File(bundlePath)).toURL().toString();
        if ( reInstall ) {
            // check for already installed bundle and uninstall it
            Bundle installedBundle = null;
            installedBundle = this.findBundle(bundleURL);
            if ( installedBundle == null ) {
                LOG.finest("ingoring reInstall: Bundle not found with location " + bundleURL);  
            } else {
                LOG.fine("uninstalling existing bundle before reinstall");
                try {
                    installedBundle.stop();
                } catch (Exception ex) {
                    LOG.log(Level.FINEST, ex.getMessage(), ex);
                }
                try {
                    installedBundle.uninstall(); 
                } catch (Exception ex) {
                    throw new Exception("Unable to uninstall bundle for reinstalling", ex);
                }
            }
        }
        
        LOG.fine(" Installing bundle= " + bundleURL);
        System.out.println(" Installing bundle= " + bundleURL);
        
        Bundle bundle = this.mContext.installBundle(bundleURL);
        String result = "";
        if (bundle != null) {
            result = Long.toString(bundle.getBundleId());
            if ( start ) {
                try {
                bundle.start();
                } catch (Exception ex) {
                    // log the exception and print the warning to console.
                    String warningMsg = "Unable to start bundle " + result + ". Exception: " + ex.getMessage();
                    System.err.println(warningMsg);
                    LOG.warning(warningMsg);
                    LOG.log(Level.FINE, warningMsg, ex);
                }
            }
        }
        return result;
    }

    public String startBundle(long bundleId) throws Exception {
        Bundle bundle = this.mContext.getBundle(bundleId);
        String result = "";
        if (bundle != null) {
            bundle.start();
            result = Long.toString(bundle.getBundleId());
        } else {
            throw new Exception("Bundle with BundleId " + bundleId + " not found");
        }
        return result;
    }

    public String stopBundle(long bundleId) throws Exception {

        Bundle bundle = this.mContext.getBundle(bundleId);
        String result = "";
        if (bundle != null) {
            bundle.stop();
            result = Long.toString(bundle.getBundleId());
        } else {
            throw new Exception("Bundle with BundleId " + bundleId + " not found");
        }
        return result;
    }

    public String uninstallBundle(long bundleId) throws Exception {

        Bundle bundle = this.mContext.getBundle(bundleId);
        String result = "";
        if (bundle != null) {
            try {
                bundle.stop();
            } catch (Exception ex) {
                LOG.log(Level.FINEST, ex.getMessage(), ex);
            }
            bundle.uninstall();
            result = Long.toString(bundle.getBundleId());
        } else {
            throw new Exception("Bundle with BundleId " + bundleId + " not found");
        }
        return result;
    }

    private String getBundleInfoState(int state) {
        String bundleInfoState = OSGiBundleInfo.STATE_UNKNOWN;
        if (state == Bundle.ACTIVE) {
            bundleInfoState = OSGiBundleInfo.STATE_ACTIVE;
        } else if (state == Bundle.INSTALLED) {
            bundleInfoState = OSGiBundleInfo.STATE_INSTALLED;
        } else if (state == Bundle.RESOLVED) {
            bundleInfoState = OSGiBundleInfo.STATE_RESOLVED;
        } else if (state == Bundle.STARTING) {
            bundleInfoState = OSGiBundleInfo.STATE_STARTING;
        } else if (state == Bundle.STOPPING) {
            bundleInfoState = OSGiBundleInfo.STATE_STOPPING;
        }
        return bundleInfoState;
    }

    private String getJbiDescriptor(Bundle bundle) {
        String jbiDescriptor = null;
        URL jbiXmlURL = null;
        InputStream inS = null;
        jbiXmlURL = bundle.getEntry("/META-INF/jbi.xml");
        if (jbiXmlURL != null) {
            try {
                StringWriter writer = new StringWriter();
                PrintWriter out = new PrintWriter(writer);
                inS = jbiXmlURL.openConnection().getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(inS));
                for (String line = null; (line = in.readLine()) != null;) {
                    out.println(line);
                }
                out.close();
                jbiDescriptor = writer.getBuffer().toString();
            } catch (Exception ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            } finally {
                if (inS != null) {
                    try {
                        inS.close();
                    } catch (IOException ex) {
                        LOG.log(Level.FINER, null, ex);
                    }
                }
            }
        }
        return jbiDescriptor;
    }
    
    private String getJbiArtifactType(String jbiDescriptor) {
        String jbiArtifactType = OSGiBundleInfo.JBI_UNKNOWN;
        if ( jbiDescriptor == null ) {
            return jbiArtifactType; // unknown
        }
        
        InputStream inS = null;

        try {
            inS = new ByteArrayInputStream(jbiDescriptor.getBytes());
            Jbi jbi = Jbi.newJbi(inS);
            Component jbiComp = jbi.getComponent();
            if (jbiComp != null) {
                jbiArtifactType = jbiComp.getType();
            } else {
                ServiceAssembly jbiSA = jbi.getServiceAssembly();
                if (jbiSA != null) {
                    jbiArtifactType = OSGiBundleInfo.JBI_SA;
                } else {
                    SharedLibrary jbiSL = jbi.getSharedLibrary();
                    if (jbiSL != null) {
                        jbiArtifactType = OSGiBundleInfo.JBI_SL;
                    }
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.FINE, null, ex);
        } finally {
            if (inS != null) {
                try {
                    inS.close();
                } catch (IOException ex) {
                    LOG.log(Level.FINER, null, ex);
                }
            }
        }

        return jbiArtifactType;
    }
    
    private String getJbiArtifactType(Bundle bundle) {
        String jbiArtifactType = OSGiBundleInfo.JBI_UNKNOWN;

        URL jbiXmlURL = null;
        InputStream inS = null;
        jbiXmlURL = bundle.getEntry("/META-INF/jbi.xml");

        if (jbiXmlURL != null) {
            try {
                inS = jbiXmlURL.openConnection().getInputStream();
                Jbi jbi = Jbi.newJbi(inS);
                Component jbiComp = jbi.getComponent();
                if (jbiComp != null) {
                    jbiArtifactType = jbiComp.getType();
                } else {
                    ServiceAssembly jbiSA = jbi.getServiceAssembly();
                    if (jbiSA != null) {
                        jbiArtifactType = OSGiBundleInfo.JBI_SA;
                    } else {
                        SharedLibrary jbiSL = jbi.getSharedLibrary();
                        if (jbiSL != null) {
                            jbiArtifactType = OSGiBundleInfo.JBI_SL;
                        }
                    }
                }
            } catch (Exception ex) {
                LOG.log(Level.FINE, null, ex);
            } finally {
                if (inS != null) {
                    try {
                        inS.close();
                    } catch (IOException ex) {
                        LOG.log(Level.FINER, null, ex);
                    }
                }
            }
        }

        return jbiArtifactType;
    }

    private OSGiBundleInfo createOSGiBundleInfo(Bundle bundle) {
        OSGiBundleInfo info = new OSGiBundleInfo();
        info.setBundleId(Long.toString(bundle.getBundleId()));
        info.setSymbolicName(bundle.getSymbolicName());
        info.setState(getBundleInfoState(bundle.getState()));
        String jbiDescriptor = this.getJbiDescriptor(bundle);
        info.setJbiArtifactType(getJbiArtifactType(jbiDescriptor));
        info.setJbiDescriptor(jbiDescriptor);
        info.setLastModified(Long.toString(bundle.getLastModified()));
        info.setLocation(bundle.getLocation());
        
        List<OSGiPropertyInfo> headerList = new ArrayList<OSGiPropertyInfo>();
        Dictionary headers = bundle.getHeaders();
        Enumeration keys = headers.keys();
        for ( ;keys.hasMoreElements();)  {
            Object key = keys.nextElement();
            Object value = headers.get(key);
            headerList.add(new OSGiPropertyInfo(key.toString(), value));
        }
        info.setHeaders(headerList);
        return info;
    }

    private List<OSGiBundleInfo> getBundleInfoList() {
        List<OSGiBundleInfo> list = new ArrayList<OSGiBundleInfo>();
        BundleContext ctx = getContext();
        if (ctx != null) {
            Bundle[] bundles = ctx.getBundles();
            for (Bundle bundle : bundles) {
                OSGiBundleInfo info = createOSGiBundleInfo(bundle);
                list.add(info);
            }
        }
        return list;
    }

    private OSGiServiceInfo createOSGiServiceInfo(ServiceReference ref) {

        OSGiServiceInfo info = new OSGiServiceInfo();

        String bundleName = ref.getBundle().getSymbolicName();
        Object serviceId = ref.getProperty(Constants.SERVICE_ID);
        Object serviceDesc = ref.getProperty(Constants.SERVICE_DESCRIPTION);
        info.setBundle(bundleName);
        if (serviceId != null) {
            info.setId(serviceId.toString());
        }
        if (serviceDesc != null) {
            info.setDescription(serviceDesc.toString());
        }

        Object[] clazzes = (Object[]) ref.getProperty(Constants.OBJECTCLASS);
        if (clazzes == null) {
            clazzes = new Object[0];
        }
        List<String> clazzList = new ArrayList<String>();
        for (Object clazz : clazzes) {
            clazzList.add(clazz.toString());
        }
        info.setObjectClasses(clazzList.toArray(new String[0]));

        Bundle[] using = ref.getUsingBundles();
        if (using == null) {
            using = new Bundle[0];
        }
        List<String> usingList = new ArrayList<String>();
        for (Bundle uBundle : using) {
            usingList.add(uBundle.getSymbolicName());
        }
        info.setUsingBundles(usingList.toArray(new String[0]));

        // add  properties
        List<OSGiPropertyInfo> propList = new ArrayList<OSGiPropertyInfo>();
        String[] keys = ref.getPropertyKeys();
        for ( String key : keys ) {
           Object value = ref.getProperty(key);
           propList.add(new OSGiPropertyInfo(key, value));
        }
        info.setProperties(propList);

        return info;
    }

    private List<OSGiServiceInfo> getServiceInfoList() {
        List<OSGiServiceInfo> list = new ArrayList<OSGiServiceInfo>();
        BundleContext ctx = getContext();
        if (ctx == null) {
            return list;
        }
        Bundle[] bundles = ctx.getBundles();
        for (Bundle bundle : bundles) {
            ServiceReference[] refs = bundle.getRegisteredServices();
            if (refs == null) {
                continue;
            }
            for (ServiceReference ref : refs) {
                OSGiServiceInfo info = createOSGiServiceInfo(ref);
                // System.out.println(info);    
                list.add(info);
            }
        }
        return list;
    }

    /**
     * Retrieve the Configuration Administratin Service from context.
     * @return Configuration Admin object
     */
    private ConfigurationAdmin getConfigurationAdminService(BundleContext ctx)
    {
        ConfigurationAdmin configAdmin= null;
        ServiceReference  configAdminRef = ctx.getServiceReference(ConfigurationAdmin.class.getName());
        if (configAdminRef != null) {
            configAdmin = (ConfigurationAdmin) ctx.getService(configAdminRef);  
        } else {
            // LOG WARNING.
            LOG.warning("The Configuration Admin Service is not available for JMX Admin.");
        }
        return configAdmin;
    }
    
    private OSGiConfigInfo createOSGiConfigInfo(Configuration config) {
        OSGiConfigInfo info = new OSGiConfigInfo();
        info.setPid(config.getPid());
        info.setFactoryPid(config.getFactoryPid());
        info.setBundleLocation(config.getBundleLocation());
        // add  properties
        List<OSGiPropertyInfo> propList = new ArrayList<OSGiPropertyInfo>();
        Dictionary props = config.getProperties();
        Enumeration keys = props.keys();
        for ( ;keys.hasMoreElements();)  {
            Object key = keys.nextElement();
            Object value = props.get(key);
            propList.add(new OSGiPropertyInfo(key.toString(), value));
        }
        info.setProperties(propList);      
        return info;
    }
    
    private List<OSGiConfigInfo> getConfigInfoList() {
        List<OSGiConfigInfo> list = new ArrayList<OSGiConfigInfo>();
        BundleContext ctx = getContext();
        ConfigurationAdmin configAdmin = null;
        if (ctx != null) {
            configAdmin = getConfigurationAdminService(ctx);
        }
        Configuration[] configs = null;
        if (configAdmin != null) {
            try {

                configs = configAdmin.listConfigurations(null); // get all configurations

            } catch (Exception ex) {
                LOG.warning(ex.getMessage());
                LOG.log(Level.FINER, ex.getMessage(), ex);
            }
        }
        if (configs == null) {
            return list;
        }
        for (Configuration config : configs) {
            OSGiConfigInfo info = createOSGiConfigInfo(config);
            list.add(info);
        }
        return list;
    }
    
    public BundleContext getContext() {
        return mContext;
    }

    public void setContext(BundleContext context) {
        this.mContext = context;
    }

    /**
     * registration with old domain with typo
     * //TODO: remove this in M8
     * @deprecated 
     * @param mbs
     */
    public void registerMBeanOld(MBeanServer mbs) {
        try {
            ObjectName jmxObjectName = new ObjectName(AdminManager.DEPRECATED_FUJI_ADMIN_OBJECT_NAME);
            StandardMBean mbean = new StandardMBean(this, AdminMBean.class);
            mbs.registerMBean(mbean, jmxObjectName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /**
     * unregistration with old domain with typo
     * //TODO: remove this in M8
     * @deprecated
     * @param mbs
     */
    public void unregisterMBeanOld(MBeanServer mbs) {
        try {
            ObjectName jmxObjectName = new ObjectName(AdminManager.DEPRECATED_FUJI_ADMIN_OBJECT_NAME);
            mbs.unregisterMBean(jmxObjectName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void registerMBean(MBeanServer mbs) {
        try {
            ObjectName jmxObjectName = new ObjectName(AdminManager.FUJI_ADMIN_OBJECT_NAME);
            StandardMBean mbean = new StandardMBean(this, AdminMBean.class);
            mbs.registerMBean(mbean, jmxObjectName);
            registerMBeanOld(mbs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void unregisterMBean(MBeanServer mbs) {
        try {
            ObjectName jmxObjectName = new ObjectName(AdminManager.FUJI_ADMIN_OBJECT_NAME);
            mbs.unregisterMBean(jmxObjectName);
            unregisterMBeanOld(mbs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * returns the bundle installed from the location 
     * //TODO: use a better searching algo than a simple linear search if possible.
     * @param location location ( url or a path to the file)
     * @return Bundle
     * @throws Exception if not found.
     */
    protected Bundle findBundle(String location) {
        Bundle myBundle = null;
        BundleContext ctx = getContext();
        if (ctx != null) {
            Bundle[] bundles = ctx.getBundles();
            for (Bundle bundle : bundles) {
                if (location.equals(bundle.getLocation()) ) {
                    myBundle = bundle;
                    break;
                }
            }
        }
        return myBundle;
//        if ( myBundle != null ) {
//            return myBundle;
//        } else {
//            throw new Exception("Bundle not found with location " + location);
//        }
    }

    public String startBundle(String bundlePath) throws Exception {
        String bundleURL = (new File(bundlePath)).toURL().toString();
        Bundle bundle = findBundle(bundleURL);
        if ( bundle == null ) {
          throw new Exception("Bundle not found with location " + bundleURL);  
        }
        String result = Long.toString(bundle.getBundleId());
        bundle.start();
        return result;
    }

    public String stopBundle(String bundlePath) throws Exception {
        String bundleURL = (new File(bundlePath)).toURL().toString();
        Bundle bundle = findBundle(bundleURL);
        if ( bundle == null ) {
          throw new Exception("Bundle not found with location " + bundleURL);  
        }        
        String result = Long.toString(bundle.getBundleId());
        bundle.stop();
        return result;
    }

    public String uninstallBundle(String bundlePath) throws Exception {
        String bundleURL = (new File(bundlePath)).toURL().toString();
        Bundle bundle = findBundle(bundleURL);
        if ( bundle == null ) {
          throw new Exception("Bundle not found with location " + bundleURL);  
        }        
        String result = Long.toString(bundle.getBundleId());
        try {
            bundle.stop();
        } catch(Exception ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex);
        }
        bundle.uninstall();
        return result;
    }
    
    private Dictionary getUpdatableConfiguration(TabularData properties) throws Exception {
        // remove pid, factory id, location properties from the list
        Hashtable<String,Object> props = new Hashtable<String,Object>();
        List<OSGiPropertyInfo> propList = OSGiPropertyInfo.toOSGiPropertyInfoList(properties);
        LOG.fine("getting Updatable Configuration");
        for ( OSGiPropertyInfo info : propList ) {
            String name = info.getName();
            String value = info.getValue();
            LOG.fine("Name=" + name + " Value=" + value);
            if ( Constants.SERVICE_PID.equals(name) || 
                    "service.factoryPid".equals(name) ||
                    "service.bundleLocation".equals(name) ) {
                continue;
            } else {
                props.put(name, value);
            }
        }
        return props;
    }
    
    public String updateConfiguration(String pid, TabularData properties) throws Exception {
        BundleContext ctx = getContext();
        ConfigurationAdmin configAdmin = null;
        if (ctx != null) {
            configAdmin = getConfigurationAdminService(ctx);
        }
        if ( configAdmin == null ) {
            LOG.warning("No Configuraiton admin service found");
            return pid;            
        }
        Configuration config = configAdmin.getConfiguration(pid);
        if (config == null) {
            throw new Exception("No coniguration found for PID " + pid);
        }
        Dictionary props = getUpdatableConfiguration(properties);
        
        Dictionary configProps = config.getProperties();
        
        for ( Enumeration keys = props.keys(); keys.hasMoreElements() ;)  {
            Object key = keys.nextElement();
            Object value = props.get(key);
            if ( key != null && value != null) {
                configProps.put(key, value);
            } else {
                LOG.fine("Update Configuration Null Key or Value " + key + "=" + value);
            }
        }
        config.update(configProps);
        return pid;
    }
}
