/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminMBean.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import javax.management.openmbean.TabularData;

/**
 * Main JMX MBean interface for osgi/fuji admin via JMX
 * @author chikkala
 */
public interface AdminMBean {

    /**
     * 
     * @return TabularData representing the list of bundle infos
     * @throws java.lang.Exception
     */
    TabularData listBundles() throws Exception;
    /**
     * 
     * @return TabularData representing the list of service infos
     * @throws java.lang.Exception
     */
    TabularData listServiceRefs() throws Exception;
    /**
     * 
     * @return TabularData representing the list of config infos
     * @throws java.lang.Exception
     */
    TabularData listConfigurations() throws Exception;    
    /**
     * 
     * @param pid
     * @param properties
     * @throws java.lang.Exception
     */
    String updateConfiguration(String pid, TabularData properties) throws Exception;
    /**
     * 
     * @param bundlePath file path
     * @return
     * @throws java.lang.Exception
     */
    String installBundle(String bundlePath) throws Exception;
    /**
     *
     * @param bundlePath file path
     * @param start true to start after install.
     * @param reInstall true to uninstall and install if the bundle already exists.
     * @return
     * @throws java.lang.Exception
     */
    String installBundle(String bundlePath, boolean start, boolean reInstall) throws Exception;
    /**
     * 
     * @param bundleId
     * @return
     * @throws java.lang.Exception
     */
    String startBundle(long bundleId) throws Exception;
    /**
     * 
     * @param bundlePath bundle file path using which it was installed.
     * @return
     * @throws java.lang.Exception
     */
    String startBundle(String bundlePath) throws Exception;
    /**
     * 
     * @param bundleId
     * @return
     * @throws java.lang.Exception
     */
    String stopBundle(long bundleId) throws Exception;
    /**
     *
     * @param bundlePath bundle file path using which it was installed.
     * @return
     * @throws java.lang.Exception
     */
    String stopBundle(String bundlePath) throws Exception;
    /**
     * 
     * @param bundleId
     * @return
     * @throws java.lang.Exception
     */
    String uninstallBundle(long bundleId) throws Exception;
    /**
     *
     * @param bundlePath bundle file path using which it was installed.
     * @return
     * @throws java.lang.Exception
     */
    String uninstallBundle(String bundlePath) throws Exception;
    /**
     * shutdown the fuji/osgi runtime
     * @throws java.lang.Exception
     */
    void shutdown() throws Exception;
}
