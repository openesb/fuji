/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OSGiPropertyInfo.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

/**
 *
 * @author chikkala
 */
public class OSGiPropertyInfo {
    //  properties

    public static final String PROP_NAME = "name";
    public static final String PROP_VALUE = "value";
    public static final String PROP_TYPE = "type";
    // jmx tabular data names
    public static final String[] ITEM_NAMES = {
        PROP_NAME,
        PROP_VALUE,
        PROP_TYPE
    };
    // jmx tabular data types
    public static final OpenType[] ITEM_TYPES = {
        SimpleType.STRING,
        SimpleType.STRING,
        SimpleType.STRING
    };
    // properties
    private String name;
    private String value;
    private String type;

    /**
     * constructor
     */
    protected OSGiPropertyInfo() {
    }

    public OSGiPropertyInfo(String name, Object value) {
        this.name = name;
        if (value != null) {
            Class clazz = value.getClass();
            this.type = clazz.getName();
            if (clazz.isArray()) {
                this.value = Arrays.toString((Object[]) value);
            } else {
                this.value = value.toString();
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static CompositeType getCompositeType() throws OpenDataException {
        CompositeType type = null;
        String typeName = "com.sun.jbi.fuji.admin.jmx.OSGiPropertyInfo" + ":CompositeType";
        String typeDesc = "Represents " + typeName;
        String[] itemNames = ITEM_NAMES;
        String[] itemDescs = ITEM_NAMES;
        OpenType[] itemTypes = ITEM_TYPES;
        type = new CompositeType(typeName, typeDesc, itemNames, itemDescs, itemTypes);
        return type;
    }

    public static TabularType getTabularType() throws OpenDataException {
        String ttypeName = "com.sun.jbi.fuji.admin.jmx.OSGiPropertyInfo" + ":TabularType";
        String ttypeDesc = "Represents " + ttypeName;
        CompositeType rowType = OSGiPropertyInfo.getCompositeType();
        TabularType ttype = new TabularType(ttypeName, ttypeDesc, rowType, OSGiPropertyInfo.ITEM_NAMES);
        return ttype;
    }

    public CompositeData toCompositeData() throws OpenDataException {
        CompositeData data = null;
        CompositeType compositeType = getCompositeType();
        String[] itemNames = ITEM_NAMES;
        Object[] itemValues = {
            this.getName(),
            this.getValue(),
            this.getType()
        };
        data = new CompositeDataSupport(compositeType, itemNames, itemValues);
        return data;
    }

    public static OSGiPropertyInfo toOSGiPropertyInfo(CompositeData data) throws OpenDataException {
        OSGiPropertyInfo info = new OSGiPropertyInfo();
//        if (!data.getCompositeType().equals(getCompositeType())) {
//            throw new OpenDataException("Can not convert ");
//        }
        info.setName((String) data.get(PROP_NAME));
        info.setValue((String) data.get(PROP_VALUE));
        info.setType((String) data.get(PROP_TYPE));
        return info;
    }

    public static List<OSGiPropertyInfo> toOSGiPropertyInfoList(TabularData tdata) throws OpenDataException {
        List<OSGiPropertyInfo> list = new ArrayList<OSGiPropertyInfo>();
        Collection<CompositeData> cdataList = (Collection<CompositeData>) tdata.values();
        for (CompositeData cdata : cdataList) {
            OSGiPropertyInfo info = OSGiPropertyInfo.toOSGiPropertyInfo(cdata);
            list.add(info);
        }
        return list;
    }

    public static TabularData toTabularData(List<OSGiPropertyInfo> list) throws OpenDataException {
        TabularType ttype = getTabularType();
        TabularData tdata = new TabularDataSupport(ttype);
        for (OSGiPropertyInfo info : list) {
            tdata.put(info.toCompositeData());
        }
        return tdata;
    }
}
