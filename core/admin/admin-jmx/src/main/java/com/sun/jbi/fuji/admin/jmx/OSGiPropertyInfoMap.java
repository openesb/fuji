/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OSGiPropertyInfoMap.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.admin.jmx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chikkala
 */
public class OSGiPropertyInfoMap {

    private Map<String, OSGiPropertyInfo> propMap;

    public OSGiPropertyInfoMap() {
        this.propMap = new HashMap<String, OSGiPropertyInfo>();
    }

    public OSGiPropertyInfoMap(List<OSGiPropertyInfo> properties) {
        super();
        if (properties != null) {
            setProperties(properties);
        }
    }

    public Map<String, OSGiPropertyInfo> getPropertiesMap() {
        return Collections.unmodifiableMap(this.propMap);
    }

    public List<OSGiPropertyInfo> getProperties() {
        List<OSGiPropertyInfo> list = new ArrayList<OSGiPropertyInfo>(this.propMap.values());
        return list;
    }

    public void setProperties(List<OSGiPropertyInfo> properties) {
        this.propMap.clear();
        if (properties != null) {
            for (OSGiPropertyInfo info : properties) {
                this.propMap.put(info.getName(), info);
            }
        }
    }

    public OSGiPropertyInfo getProperty(String name) {
        return this.propMap.get(name);

    }

    public String getPropertyValue(String name) {
        String value = null;
        OSGiPropertyInfo info = this.propMap.get(name);
        if (info != null) {
            value = info.getValue();
        }
        return value;
    }
}
