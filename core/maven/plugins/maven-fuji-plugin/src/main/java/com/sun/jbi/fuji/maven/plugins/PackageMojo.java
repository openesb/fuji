/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PackageMojo.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;


import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.maven.util.ApplicationUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.handler.manager.ArtifactHandlerManager;
import java.io.File;

/**
 * Goal which packages an integration app into a JBI service assembly.
 *
 * @goal package-app
 */
public class PackageMojo
        extends BaseMojo {

    /**
     * we use the project artifact id for application id
     * @parameter expression="${project.artifactId}"
     * @required
     */
    private String applicationId_;

        
    /**
     * The project verison is needed to create the correct output file
     * @parameter expression="${project.version}"
     */
    private String applicationVersion_;
    
    
    /**
     * ArtifactHandlerManager
     * @component
     */
    private ArtifactHandlerManager artifactHandlerManager_;

    public void execute()
            throws MojoExecutionException {

        initServiceToolsLocator();

        try {
             MavenServiceToolsLocator locator = this.getToolsLocator();
             File iflFile = new File(baseDirectory_, "src/main/ifl");
            
            IFLModel ifl = null;
            if ( iflFile.exists())
            {       
                // Parse all IFL definitions in the app
                
                ifl = new IFLReader().read(new File(baseDirectory_, "src/main/ifl"));

            }
            String applicationJar = ApplicationUtils.packageApplication(
                    locator, ifl, applicationId_, applicationVersion_, baseDirectory_, outputDirectory_); 

            Artifact artifact = project_.getArtifact();
 
            /* ugly hack - see open-esb issue 1396. 
               Caused by netbeans issue 135043 - Badly formed maven project */
            //artifact.setFile(new File(applicationJar));
            applicationJar = new File(outputDirectory_, applicationId_ + "-" + applicationVersion_ + ".jar").getAbsolutePath();
            
            artifact.setFile(new File(applicationJar));
           
            artifact.setArtifactHandler( artifactHandlerManager_.getArtifactHandler( "jar" ) );
            
        } catch (Exception ex) {
            throw new MojoExecutionException(ex.getMessage(), ex);
        }
    }

}
