/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.maven.util.AppBuildManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * This class is a mojo that adds the "generate-config" goal to the fuji maven plugin.
 * <p>
 * The "generate-config" goal generates service configuration for a given service under
 * an integration application. A successful execution of the goal results in creating the
 * service.properties file under /src/main/config/<service-type>/<service-name>/service.properties
 * for a given service.
 *
 * <p>
 * Note that to generate artifacts (using generate-service) for a service/group of services,
 * correpsonding service configuration must exist in the project sources ( under
 * /src/main/config/<service-type>/<service-name>/service.properties ). So, this goal can be used to
 * generate specific service configuration before execute the other goals.
 *
 * @goal generate-config
 *
 * @author chikkala
 */
public class GenerateConfigMojo extends BaseMojo {

    private static final Logger LOG = Logger.getLogger(GenerateConfigMojo.class.getName());
    /**
     * service type for which artifacts will be generated.
     *
     * @parameter property="serviceType" alias="serviceType" expression="${service.type}" default-value=" "
     */
    private String mServiceType;
    /**
     * service name for which artifacts will be generated.
     *
     * @parameter property="serviceName" alias="serviceName" expression="${service.name}" default-value=" "
     */
    private String mServiceName;
    /**
     * input file that contains the properties which override the default property values. The properties should include
     * "service.configuration.mode" property set to appropriate value. If that property is not present, it is assumed
     * that the properties are for a default configuration.
     *
     * @parameter property="inputFile" alias="inputFile" expression="${input.file}" default-value="${project.build.directory}/null"
     *
     */
    private File mInputFile;
    /**
     * tempate (service configuration mode) to use to generate specific template. This property is present along with the input file,
     * the service configuration mode in the input file takes precedence. If this property is present without the input file, the
     * configuration representing this tempalte will be generated with the default values for all the properties in that configuration
     * specified in the service descriptor.
     *
     * @parameter property="configTemplate" alias="configTemplate" expression="${service.config.template}" default-value=" "
     *
     */
    private String mConfigTemplate;

    /**
     * flag to generate the default service configuration if user did not include the input file.
     * @parameter property="generateDefault" alias="generateDefault" expression="${generate.default}" default-value="false"
     */
    private boolean mGenerateDefault;

    private ToolsLookup mLookup;

    public void setServiceType(String serviceType) {
        this.mServiceType = serviceType;
    }

    protected String getServiceType() {
        return this.mServiceType;
    }

    public void setServiceName(String serviceName) {
        this.mServiceName = serviceName;
    }

    protected String getServiceName() {
        return this.mServiceName;
    }

    public void setInputFile(File inputFile) {
        this.mInputFile = inputFile;
    }

    protected File getInputFile() {
        return this.mInputFile;
    }

    public void setConfigTemplate(String configTemplate) {
        this.mConfigTemplate = configTemplate;
    }

    protected String getConfigTemplate() {
        return this.mConfigTemplate;
    }

    public void setGenerateDefault(boolean generateDefault) {
        this.mGenerateDefault = generateDefault;
    }

    protected boolean isGenerateDefault() {
        return this.mGenerateDefault;
    }

    protected File getOutputDirectory() {
        return this.outputDirectory_;
    }

    private ToolsLookup getToolsLookup() {
        if (this.mLookup == null) {
            this.mLookup = new MavenToolsLookup(this);
        }
        return this.mLookup;
    }

    private IFLModel getIFLModel() throws Exception {

        IFLModel prjIFLModel = null;
        File iflDir = new File(baseDirectory_, "src/main/ifl");
        if (iflDir.exists()) {
            try {
                prjIFLModel = new IFLReader().read(iflDir);
            } catch (Exception ex) {
                this.getLog().debug(ex);
            }
        }
        if (prjIFLModel == null) {
            // create empty.
            StringReader iflReader = new StringReader("# empty IFL model");
            prjIFLModel = new IFLReader().read(iflReader);
        }
        return prjIFLModel;
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        initServiceToolsLocator();

        logOptions();

        this.getLog().info("Executing Generation Configuration....");
        String serviceName = this.getServiceName().trim();

        String usage = "Usage: " +
                "mvn fuji:generate-config " +
                " -Dservice.name=<ServiceName> -Dservice.config.template=<mode>";

        if (serviceName.length() == 0 ) {
            throw new MojoExecutionException("Service Name is required." +
                    "\n Use -Dservice.name option to set the service name " +
                    "\n" + usage);
        }

        if (serviceName.length() > 0) {
            try {
                generateServiceConfiguration(serviceName);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to generate service artifacts", ex);
            }
            return;
        }

        // invalid options. print the usage.
        throw new MojoExecutionException("Service Name is required." +
                "\n Use -Dservice.name option to set the service name " +
                "\n" + usage);

    }
    
    private void logOptions() {
        try {
            if (this.getLog().isDebugEnabled()) {
                this.getLog().debug("Service Type: " + this.getServiceType());
                this.getLog().debug("Service Name: " + this.getServiceName());
                this.getLog().debug("Config Template: " + this.getConfigTemplate());
                this.getLog().debug("Generate Default: " + this.isGenerateDefault());
                this.getLog().debug("Input File: " + this.getInputFile().getAbsolutePath());
            }
        } catch (Exception ex) {
            this.getLog().debug(ex);
        }
    }

    private File generateServiceConfiguration(String serviceName) throws Exception {

        AppBuildManager appBuildMgr = createAppBuildManager();
        IFLModel iflModel = getIFLModel();

        String serviceType = this.getServiceTypeForServiceName(iflModel, serviceName);
        if (serviceType == null || serviceType.length() == 0 ) {
            throw new Exception("Can not determine service type for service name " + serviceName +
                    ". Please sepecify service type using command line option -Dservice.type or fix it in IFL");
        }
        ServiceProjectModel prjModel = appBuildMgr.getServiceProjectModel(serviceType);

        Properties inputProps = getServiceConfigurationInput();
        if (this.getLog().isDebugEnabled()) {
            this.getLog().debug("Service Configuration Input ----");
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            inputProps.list(out);
            this.getLog().debug(writer.getBuffer().toString());
        }

        ServiceDescriptor sd = this.getToolsLookup().getServiceDescriptor(serviceType);
        ServiceConfigurationHelper configHelper = new ServiceConfigurationHelper(sd);

        return configHelper.generateServiceConfiguration(prjModel, iflModel, serviceName, inputProps);
    }

    private String getServiceTypeForServiceName(IFLModel iflModel, String serviceName) {

        String serviceType = this.getServiceType().trim();
        if (serviceType.length() == 0 ) {
            // find the service type from IFL.
            Set<String> serviceTypes = iflModel.getServiceTypes();
            for (String type : serviceTypes) {
                if(iflModel.getServicesForType(type).contains(serviceName)) {
                    serviceType = type;
                    break;
                }
            }
        }
        return serviceType;
    }
    
    private AppBuildManager createAppBuildManager() {

        String applicationId = this.project_.getArtifactId();
        if (applicationId == null) {
            applicationId = ApplicationProjectModel.DEF_APPLICATION_ID;
        }
        String name = this.project_.getName();
        if (name == null) {
            name = applicationId;
        }
        String displayName = name;
        String description = this.project_.getDescription();
        if (description == null) {
            description = name;
        }

        Properties prjProps = new Properties();
        prjProps.setProperty(ApplicationProjectModel.PROP_APPLICATION_ID, applicationId);
        prjProps.setProperty(ApplicationProjectModel.PROP_NAME, name);
        prjProps.setProperty(ApplicationProjectModel.PROP_DISPLAY_NAME, displayName);
        prjProps.setProperty(ApplicationProjectModel.PROP_DESCRIPTION, description);

        AppBuildManager appBuildMgr = new AppBuildManager(this.baseDirectory_, prjProps);
        return appBuildMgr;        
    }

    private Properties getServiceConfigurationInput() throws Exception {

        Properties inputProps = new Properties();
        File configInputFile = this.getInputFile();
        File nullFile = new File(this.getOutputDirectory(), "null");
        if (configInputFile != null && nullFile.getAbsolutePath().endsWith(configInputFile.getAbsolutePath())) {
            configInputFile = null;
        }
        if (configInputFile != null ) {
            if (!configInputFile.exists()) {
                throw new MojoExecutionException("Service Configuration Input file not found " + configInputFile.getAbsolutePath());
            } else {
                inputProps = readProperties(configInputFile);
            }
        }
        String configTemplate = this.getConfigTemplate();

        // get the config template value from input file or from configTemplate option.
        configTemplate = inputProps.getProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, configTemplate);
        configTemplate = configTemplate.trim();

        // no config template specified and no default generation option, exist with error.
        if (configTemplate.length() == 0 && !this.isGenerateDefault()) {
                throw new MojoExecutionException("Can not determine the service configuration template(mode). " +
                        "\n Please specify Service Configuration Template(mode) in input file or service.config.template option or set the generate default to true ");
        }
        if(configTemplate.length() > 0 ) {
            inputProps.setProperty(ServiceConfigurationHelper.PROP_SERVICE_CONFIG_MODE, configTemplate);
        }
        
        return inputProps;
    }


    private Properties readProperties(File propFile) throws IOException {
        Properties props = new Properties();
        FileInputStream inS = null;
        try {
            inS = new FileInputStream(propFile);
            props.load(inS);
        } finally {
            if (inS != null) {
                try {
                    inS.close();
                } catch (IOException ex) {
                    //ignore
                }
            }
        }
        return props;
    }

}
