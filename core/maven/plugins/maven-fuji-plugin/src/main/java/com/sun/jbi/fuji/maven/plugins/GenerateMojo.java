/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)GenerateMojo.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.maven.util.ApplicationUtils;
import org.apache.maven.plugin.MojoExecutionException;
import java.io.File;
import java.util.ArrayList;
import java.util.Properties;
/**
 * Goal which generates service artifacts for an integration application 
 * based on IFL.
 *
 * @goal generate-artifacts
 * 
 */
public class GenerateMojo extends BaseMojo {

    /**
     * we use the project artifact id for application id
     * @parameter expression="${project.artifactId}"
     * @required
     */
    private String applicationId_;
    
    /**
     * deletes the unused java files and their directories that were  
     * generated in the previous execution of the fuji:generate-artifacts goal. 
     * The default value is "true" which will delete the unused java file artifacts.
     * Set it to "false" if you need to keep the unused java files.
     *
     * @parameter property="deleteUnusedJavaFiles" alias="deleteUnusedJavaFiles"  default-value="true"
     */
    private boolean deleteUnusedJavaFiles_;

    /**
     * deletes the unused service artifacts(files and directories) that were 
     * generated in the previous execution of the fuji:generate-artifacts goal. 
     * The default value is "true" which will delete the unused service artifacts.
     * Set it to "false" if you need to keep the unused service artifacts.
     *
     * @parameter property="deleteUnusedArtifacts" alias="deleteUnusedArtifacts"  default-value="true"
     */
    private boolean deleteUnusedArtifacts_;


    /**
     * returns the value of the flag set for deleting the unused java files and directories
     * @return true to delete and false to not delete
     */
    public boolean isDeleteUnusedJavaFiles() {
        return deleteUnusedJavaFiles_;
    }

    /**
     * setter for the setDeleteUnusedJavaFiles option
     * @param deleteUnsedArtifacts
     */
    public void setDeleteUnusedJavaFiles(boolean deleteUnsedJavaFiles) {
        deleteUnusedJavaFiles_ = deleteUnsedJavaFiles;
    }

    /**
     * returns the value of the flag set for deleting the unused service artifacts
     * @return true to delete and false to not delete
     */
    public boolean isDeleteUnusedArtifacts() {
        return deleteUnusedArtifacts_;
    }

    /**
     * setter for the deleteUnusedArtifacts option
     * @param deleteUnsedArtifacts
     */
    public void setDeleteUnusedArtifacts(boolean deleteUnsedArtifacts) {
        this.deleteUnusedArtifacts_ = deleteUnsedArtifacts;
    }

    /**
     * Do our stuff.
     * @throws org.apache.maven.plugin.MojoExecutionException
     */
    public void execute() throws MojoExecutionException {
        initServiceToolsLocator();

        try {
            MavenServiceToolsLocator locator = this.getToolsLocator();
            
            final File iflFile = new File(baseDirectory_, "src/main/ifl");

            final ArrayList usedFiles = new ArrayList();

            
            if (iflFile.exists())
            {       
                final IFLModel ifl = new IFLReader().read(iflFile);

                final Properties props = new Properties();
                props.put("applicationId", applicationId_); //NOI18N
                props.put("artifactId", applicationId_); //NOI18N

                ApplicationUtils.generateApplication(
                    locator, ifl, baseDirectory_, props, usedFiles);
            }
            
            ApplicationUtils.generateInterceptorConfiguration(locator, baseDirectory_, usedFiles);
            ApplicationUtils.generateAspectsProeprtiesFromAspectsDirectory(baseDirectory_, usedFiles);

            if (deleteUnusedArtifacts_)
            {
                ApplicationUtils.moveUnusedArtifacts (usedFiles, baseDirectory_);
            }

        } catch (Exception ex) {
            throw new MojoExecutionException(ex.toString(), ex);
        }
    }
}
