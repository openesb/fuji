/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DistributionMojo.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.fuji.maven.util.MavenUtils;
import org.apache.maven.plugin.MojoExecutionException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.codehaus.plexus.util.FileUtils;

/**
 * Goal which captures all of the required bundles to create an application
 * distribution.
 * @execute phase="install"
 * @goal dist
 * 
 */
public class DistributionMojo
        extends BaseMojo {

    /**
     * we use the project artifact id for application id
     * @parameter expression="${project.artifactId}"
     * @required
     */
    private String applicationId_;

        
    /**
     * The project verison is needed to create the correct output file
     * @parameter expression="${project.version}"
     */
    private String applicationVersion_;
    
    // Some consts we rely on for artifact resolution
    private static final String FELIX_CONFIG = "felix/conf/config.properties";
    private static final String FELIX_BUNDLE_DIR = "felix/bundle";
    /** Packaging directory for service units **/
    private static final String SERVICE_UNIT_DIR = "service-units";
    /** SU header that tells us which component is targeted **/
    private static final String COMPONENT_NAME = "Component-Name";
    private static final String FELIX_AUTO_INSTALL_KEY =
            "autoinstall.bundles";
    private static final String FELIX_AUTO_INSTALL_PREFIX =
            "file:felix/bundle/";
    /** List of artifacts that we're adding to the distribution. */
    private List<File> artifactList_ = new ArrayList<File>();

    private File getComponentInstaller(String componentName) {
        File installer = null;
        try {
            URL installerURL = this.getToolsLocator().locateServiceInstaller(componentName);
            installer = new File(installerURL.toURI());
        } catch (Exception ex) {
            this.getLog().debug(ex);
        }
        return installer;
    }
    private File getSharedLibraryInstaller(String slibName) {
        File installer = null;
        try {
            URL installerURL = this.getToolsLocator().locateServiceInstaller(slibName);
            installer = new File(installerURL.toURI());
        } catch (Exception ex) {
            this.getLog().debug(ex);
        }
        return installer;
    }
    /**
     * Do our stuff.
     * @throws org.apache.maven.plugin.MojoExecutionException
     */
    public void execute()
            throws MojoExecutionException {

        initServiceToolsLocator();

        try {
            MavenServiceToolsLocator locator = this.getToolsLocator();

            // Figure out which components and libraries we need to include
            File serviceUnitDir = new File(outputDirectory_, SERVICE_UNIT_DIR);
            for (File f : serviceUnitDir.listFiles()) {
                // Pull out the Component-Name header from the SU manifest
                ZipFile su = new ZipFile(f);
                ZipEntry manifest = su.getEntry("META-INF/MANIFEST.MF");
                Properties mfEntries = new Properties();
                mfEntries.load(su.getInputStream(manifest));

                // We need the Component-Name header to do our job
                if (mfEntries.containsKey(COMPONENT_NAME)) {
                    // Grab the component
                    File componentArchive = getComponentInstaller(
                            (String) mfEntries.getProperty(COMPONENT_NAME));
                    artifactList_.add(componentArchive);
                    // Grab any library dependencies
                    for (String libName : getSharedLibrariesForComponent(componentArchive)) {
                        File slArchive = getSharedLibraryInstaller(libName);
                        addArchiveToFelix(slArchive);
                        artifactList_.add(slArchive);
                    }
                }
                su.close();
            }

            // Don't forget our app!
            artifactList_.add(new File(outputDirectory_, applicationId_ + "-" + applicationVersion_ + ".jar"));

            // Copy over the artifacts
            for (File archive : artifactList_) {
                addArchiveToFelix(archive);
            }

            // Get our config on
            initFelixConfig();
        } catch (Exception ex) {
            throw new MojoExecutionException("Failed to create distribution!", ex);
        }
    }

    private void initFelixConfig() throws Exception {
        // open a stream to the felix config file
        URL url = this.getClass().getClassLoader().getResource(FELIX_CONFIG);
        InputStream in = url.openStream();

        File configFile = new File(baseDirectory_, "target/" + FELIX_CONFIG);
        if (!configFile.getParentFile().exists()) {
            configFile.getParentFile().mkdir();
        }

        // write it out to the felix dist dir
        FileOutputStream fos = new FileOutputStream(configFile);
        byte[] buf = new byte[4 * 1028];
        int count;
        while ((count = in.read(buf)) != -1) {
            fos.write(buf, 0, count);
        }

        // clean up
        in.close();
        fos.close();

        Properties props = new Properties();
        props.setProperty(FELIX_AUTO_INSTALL_KEY, createAutoInstallList());
        MavenUtils.replaceTokensInFile(configFile, props);
    }

    private void addArchiveToFelix(File archiveFile) throws java.io.IOException {
        File bundleDir = new File(outputDirectory_, FELIX_BUNDLE_DIR);
        FileUtils.copyFileToDirectory(archiveFile, bundleDir);
    }

    private List<String> getSharedLibrariesForComponent(File componentArchive)
            throws Exception {
        ZipFile archive = new ZipFile(componentArchive);
        ZipEntry jbiXml = archive.getEntry("META-INF/jbi.xml");
        Jbi jbi = Jbi.newJbi(archive.getInputStream(jbiXml));
        return jbi.getComponent().getSharedLibraries();
    }

    private String createAutoInstallList() {
        StringBuilder listString = new StringBuilder();
        Iterator<File> archives = artifactList_.iterator();
        while (archives.hasNext()) {
            File archive = archives.next();
            listString.append(FELIX_AUTO_INSTALL_PREFIX + archive.getName());
            // Only add a line continuation if we have more entries
            if (archives.hasNext()) {
                listString.append(" \\");
            }
            listString.append(System.getProperty("line.separator"));
        }
        return listString.toString();
    }
}
