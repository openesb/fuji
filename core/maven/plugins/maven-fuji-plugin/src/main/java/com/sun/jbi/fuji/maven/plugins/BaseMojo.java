/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)GenerateMojo.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;

import java.io.IOException;
import java.net.URL;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import java.io.File;
import java.util.List;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Plugin;
import org.glassfish.openesb.tools.common.DefaultToolsLookup;
import org.glassfish.openesb.tools.common.ToolsLookup;

/**
 * Base Mojo implementation for Fuji plugins.
 * 
 */
public abstract class BaseMojo extends AbstractMojo {

    /**
     * target dir
     * @parameter expression="${project.build.directory}"
     * @required
     */
    protected File outputDirectory_;
    /**
     * base dir
     * @parameter expression="${project.basedir}"
     * @required
     */
    protected File baseDirectory_;
    /** 
     * @component
     */
    protected ArtifactFactory artifactFactory_;
    /** 
     * @component
     */
    protected ArtifactResolver resolver_;
    /**
     * @parameter expression="${localRepository}"
     */
    protected ArtifactRepository localRepository_;
    /** 
     * @parameter expression="${project.remoteArtifactRepositories}"
     */
    protected List remoteRepositories_;    

    /**
     *  The project model.
     *  @parameter expression="${project}"
     */
    protected MavenProject project_;

    private MavenServiceToolsLocator mToolsLocator;
    /**
     * This method is used to retrieve the version of maven-fuji-plugin used  
     * by the application. This version string would be used to fetch the components
     * needed by the application
     * @return String the version of maven-fuji-plugin
     * @throws MojoExecutionException if the vesion could not be determined
     */
    public String getFujiVersion() throws MojoExecutionException {
    
        List plugins = project_.getBuildPlugins();
        for (Object p : plugins) {
            Plugin plugin = (Plugin)p;
            if (plugin.getArtifactId().equals("maven-fuji-plugin")) {
                    return plugin.getVersion();
             }
        }
        throw new MojoExecutionException("Could not retrieve maven-fuji-plugin version");
    }

    protected void initServiceToolsLocator() throws MojoExecutionException {
        MavenToolsLookup lookup = null;
        try {
            lookup = new MavenToolsLookup(this);
        } catch (Exception ex) {
            this.getLog().info(ex);
        }
        this.mToolsLocator = new MavenServiceToolsLocator(
                    lookup,
                    artifactFactory_,
                    resolver_,
                    localRepository_,
                    remoteRepositories_,
                    getFujiVersion());
        //TODO: set the tools locator statically so that others can get it.
        // MavenUtils.setServiceToolsLocator(this.mToolsLocator);
    }

    protected MavenServiceToolsLocator getToolsLocator() {
        return this.mToolsLocator;
    }

    public static class MavenToolsLookup extends DefaultToolsLookup {

        private static final String FUJI_GROUP_ID = "open-esb.fuji"; // NOI18N
        // deprecated location. TODO: remove when all the descriptor bundles moved to <type>-service-plugin syntax
        private static final String DESCRIPTOR_GROUP_ID = FUJI_GROUP_ID + ".components.descriptors"; //NOI18N
        private static final String DESCRIPTOR_SUFFIX = "-descriptor";
        // final location for the service plugins (a.k.a service descriptors) with the
        // bundle name as <type>-service-plugin
        private static final String SERVICE_PLUGIN_GROUP_ID = FUJI_GROUP_ID + ".components.plugins"; //NOI18N
        private static final String SERVICE_PLUGIN_SUFFIX = "-service-plugin";

        private BaseMojo mBaseMojo;
        
        public MavenToolsLookup(BaseMojo baseMojo) {
            this.mBaseMojo = baseMojo;
        }

        protected Artifact resolveArtifact(
                String groupId,
                String artifactId,
                String version) throws Exception {

            Artifact artifact = this.mBaseMojo.artifactFactory_.createArtifactWithClassifier(
                    groupId, artifactId, version, "jar", null);
            this.mBaseMojo.resolver_.resolve(artifact,
                    this.mBaseMojo.remoteRepositories_, this.mBaseMojo.localRepository_);
            return artifact;
        }

        private File locateServiceDescriptorPreferred(String serviceType) throws IOException {
            String artifactId = serviceType.trim().toLowerCase() + SERVICE_PLUGIN_SUFFIX;
            String version = null;
            try {
                version = this.mBaseMojo.getFujiVersion();
                Artifact artifact =
                        resolveArtifact(SERVICE_PLUGIN_GROUP_ID, artifactId, version);
                return artifact.getFile();
            } catch (Exception ex) {
                this.mBaseMojo.getLog().debug(ex);
                throw new IOException("Unable to locate service descriptor archive for service type: " + serviceType);
            }
        }
        /**
         * @deprecated
         */
        private File locateServiceDescriptorDeprecated(String serviceType) throws IOException {

            String artifactId = serviceType.trim().toLowerCase() + DESCRIPTOR_SUFFIX;
            String version = null;
            try {
                version = this.mBaseMojo.getFujiVersion();
                Artifact artifact =
                        resolveArtifact(DESCRIPTOR_GROUP_ID, artifactId, version);
                return artifact.getFile();
            } catch (Exception ex) {
                this.mBaseMojo.getLog().debug(ex);
                throw new IOException("Unable to locate service descriptor archive for service type: " + serviceType);
            }
        }

        public File locateServiceDescriptor(String serviceType) throws IOException {
            File file = null;
            try {
                file = locateServiceDescriptorPreferred(serviceType);
            } catch (Exception ex) {
                this.mBaseMojo.getLog().debug(ex);
            }
            if ( file == null ) {
                file = locateServiceDescriptorDeprecated(serviceType);
            }
            return file;
        }

        @Override
        protected URL[] findClassLoaderURLs(String serviceType) {
            try {
                File sdJar = this.locateServiceDescriptor(serviceType);
                return new URL[]{sdJar.toURI().toURL()};
            } catch (Exception ex) {
                return null;
            }
        }
    }
}
