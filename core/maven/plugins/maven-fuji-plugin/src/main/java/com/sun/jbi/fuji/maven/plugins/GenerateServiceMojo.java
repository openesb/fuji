/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.fuji.maven.util.AppBuildManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;

/**
 * Goal which generates service artifacts in an integration applicaton. unlike
 * generate-artifacts (<code>GenerateMojo</code>) goal, this goal only generates
 * the service artifacts for a given service type.
 * <p>
 * This goal can be executed whenever user want to generate or regenerate a
 * particular service's artifacts in the IFL without genreating artifacts for
 * all the services in the IFL. For example, user may want to change a database
 * service artifacts from query to insert without re-generating all the IFL
 * services artifacts.
 * <p>
 * This goal can be used to generate artifacts for a single service or a group of
 * services as set of providers and consumers as defined in the service group syntax
 * in IFL.
 * <p>
 * a service or grouped services need not be declared in the IFL in the project to
 * generate artifacts. All the required input for services for which the artifacts
 * are being generated can be specified using the goal's configuration properties.
 * Whenever any input is not passed through the configuration properties, the goal
 * try to find the required input from the application's IFL model derived from the
 * IFL files in the project.  For example, if user supplies only the group name, then
 * the type and the services in the group will be lookup from the IFL.
 * <p>
 * Note that to generate artifacts for a service/group of services, correpsonding
 * service configuration must exist in the project sources ( under
 * /src/main/config/<service-type>/<service-name>/service.properties ). If not present
 * by setting the generate.service.configuration to true can generate the default service
 * configuration for a service before generating the artifacts. This flag is false by
 * default so that the goal fails to execute if the service configuration file for any
 * service or group of services for which the artifacts are being generated does not exist.
 *
 * @goal generate-service
 *
 * @author chikkala
 */
public class GenerateServiceMojo extends BaseMojo {

    private static final Logger LOG = Logger.getLogger(GenerateServiceMojo.class.getName());

    /**
     * service type for which artifacts will be generated.
     *
     * @parameter property="serviceType" alias="serviceType" expression="${service.type}" default-value=" "
     */
    private String mServiceType;
    /**
     * service name for which artifacts will be generated.
     *
     * @parameter property="serviceName" alias="serviceName" expression="${service.name}" default-value=" "
     */
    private String mServiceName;

    // group attrbutes
    /**
     * service name for which artifacts will be generated.
     *
     * @parameter property="serviceGroup" alias="serviceGroup" expression="${service.group}" default-value=" "
     */
    private String mServiceGroup;
    /**
     * comma separated list of service providers in the services group for which artifacts will be generated.
     * e.g. "p1, p2, p2"
     *
     * @parameter property="serviceProviders" alias="serviceProviders" expression="${service.group.providers}" default-value=" "
     */
    private String mServiceProviders;
    /**
     * comma separated list of service consumers in the services group for which artifacts will be generated.
     * e.g "c1, c2, c3"
     *
     * @parameter property="serviceConsumers" alias="serviceConsumers" expression="${service.group.consumers}" default-value=" "
     */
    private String mServiceConsumers;
    /**
     * flag to generate the default service configuration if it is not found for the service/group for which the service
     * artifacts are being generated. It is required to have a service configuration file (service.properties) file for a
     * service to generate the artifacts.
     * @parameter property="generateServiceConfig" alias="generateServiceConfig" expression="${generate.service.config}" default-value="false"
     */
    private boolean mGenerateServiceConfig;

    private ToolsLookup mLookup;

    public void setServiceType(String serviceType) {
        this.mServiceType = serviceType;
    }

    protected String getServiceType() {
        return this.mServiceType;
    }

    public void setServiceName(String serviceName) {
        this.mServiceName = serviceName;
    }

    protected String getServiceName() {
        return this.mServiceName;
    }

    public void setServiceGroup(String serviceGroup) {
        this.mServiceGroup = serviceGroup;
    }

    protected String getServiceGroup() {
        return this.mServiceGroup;
    }

    public void setServiceProviders(String serviceProviders) {
        this.mServiceProviders = serviceProviders;
    }

    protected String getServiceProviders() {
        return this.mServiceProviders;
    }

    public void setServiceConsumers(String serviceConsumers) {
        this.mServiceConsumers = serviceConsumers;
    }

    protected String getServiceConsumers() {
        return this.mServiceConsumers;
    }

    public void setGenerateServiceConfig(boolean generateServiceConfig) {
        this.mGenerateServiceConfig = generateServiceConfig;
    }

    protected boolean isGenerateServiceConfig() {
        return this.mGenerateServiceConfig;
    }

    protected File getOutputDirectory() {
        return this.outputDirectory_;
    }

    private ToolsLookup getToolsLookup() {
        if (this.mLookup == null) {
            this.mLookup = new MavenToolsLookup(this);
        }
        return this.mLookup;
    }

    private IFLModel getIFLModel() throws Exception {

        IFLModel prjIFLModel = null;
        File iflDir = new File(baseDirectory_, "src/main/ifl");
        if (iflDir.exists()) {
            try {
                prjIFLModel = new IFLReader().read(iflDir);
            } catch (Exception ex) {
                this.getLog().debug(ex);
            }
        }
        if (prjIFLModel == null) {
            // create empty.
            StringReader iflReader = new StringReader("# empty IFL model");
            prjIFLModel = new IFLReader().read(iflReader);
        }
        return prjIFLModel;
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        initServiceToolsLocator();

        logOptions();

        this.getLog().info("Executing Service Generation ....");
        String serviceName = this.getServiceName().trim();
        String serviceGroup = this.getServiceGroup().trim();

        String usage = "Usage: " +
                "mvn fuji:generate-service " +
                " -Dservice.name=<ServiceName> or " +
                " -Dservice.group=<ServiceGroup>";

        if (serviceName.length() == 0 && serviceGroup.length() == 0) {
            throw new MojoExecutionException("Service Name or Service Group is required." +
                    "\n Use -Dservice.name option to set the service name " +
                    "\n Use -Dservice.group option to set the service group " +
                    "\n" + usage);
        }

        if (serviceGroup.length() > 0) {
            String providers = this.getServiceProviders().trim();
            String consumers = this.getServiceConsumers().trim();
            try {
                generateGroupedServiceArtifacts(serviceGroup, providers, consumers);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to generate service artifacts", ex);
            }
            return;
        }
        if (serviceName.length() > 0) {
            try {
                generateServiceArtifacts(serviceName);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to generate service artifacts", ex);
            }
            return;
        }

        // invalid options. print the usage.
        throw new MojoExecutionException("Service Name or Service Group is required." +
                "\n Use -Dservice.name option to set the service name " +
                "\n Use -Dservice.group option to set the service group " +
                "\n" + usage);

    }
    
    private void logOptions() {
        try {
            if (this.getLog().isDebugEnabled()) {
                this.getLog().debug("Service Type: " + this.getServiceType());
                this.getLog().debug("Service Name: " + this.getServiceName());
                this.getLog().debug("Service Group: " + this.getServiceGroup());
                this.getLog().debug("Service Providers: " + this.getServiceProviders());
                this.getLog().debug("Service Consumers: " + this.getServiceConsumers());
                this.getLog().debug("Generate Service Configuration: " + this.isGenerateServiceConfig());
            }
        } catch (Exception ex) {
            this.getLog().debug(ex);
        }
    }

    private void generateServiceArtifacts(String serviceName) throws Exception {

        MavenServiceToolsLocator locator = this.getToolsLocator();
        AppBuildManager appBuildMgr = createAppBuildManager();
        IFLModel iflModel = getIFLModel();

        String serviceType = this.getServiceTypeForServiceName(iflModel, serviceName);
        if (serviceType == null || serviceType.length() == 0 ) {
            throw new Exception("Can not determine service type for service name " + serviceName +
                    ". Please sepecify service type using command line option -Dservice.type or fix it in IFL");
        }
        ServiceProjectModel prjModel = appBuildMgr.getServiceProjectModel(serviceType);

        Properties config = getServiceConfiguration(prjModel, iflModel, serviceType, serviceName, this.isGenerateServiceConfig());

        if (this.getLog().isDebugEnabled()) {
            this.getLog().debug("Service Configuration Input ----");
            StringWriter writer = new StringWriter();
            PrintWriter out = new PrintWriter(writer);
            config.list(out);
            this.getLog().debug(writer.getBuffer().toString());
        }
        ServiceGenerator serviceGen = locator.getServiceGenerator(serviceType);
        serviceGen.generateServiceArtifacts(prjModel, iflModel, serviceName, config);
    }

    private void generateGroupedServiceArtifacts(String groupName, String providers, String consumers) throws Exception {
        MavenServiceToolsLocator locator = this.getToolsLocator();
        AppBuildManager appBuildMgr = createAppBuildManager();
        IFLModel iflModel = getIFLModel();

        String serviceType = this.getServiceTypeForServiceGroup(iflModel, groupName);
        if (serviceType == null || serviceType.length() == 0) {
            throw new Exception("Can not determine service type for service group " + groupName +
                    ". Please sepecify service type using command line option -Dservice.type or fix it in IFL");
        }

        ServiceProjectModel prjModel = appBuildMgr.getServiceProjectModel(serviceType);
        boolean groupInIFL = false;
        List<String> groups = iflModel.getServiceGroupNames();
        if (groups.contains(groupName)) {
            groupInIFL = true;
        }
        Set<String> groupedServices = new TreeSet<String>();
        Set<String> serviceRefs = new TreeSet<String>();
        findServicesInServiceGroup(iflModel, groupName, groupedServices, serviceRefs);

        List<String> providerList = createServiceList(providers);
        if (providerList.size() == 0 && groupInIFL) {
            // get the providers from IFL.
            providerList.addAll(groupedServices);
        }

        List<String> consumerList = createServiceList(consumers);
        if (consumerList.size() == 0 && groupInIFL) {
            // get the consumers from IFL.
            consumerList.addAll(serviceRefs);
        }

        // load configurations from disk.
        boolean createServiceConfig = isGenerateServiceConfig();
        Map<String, Properties> providersConfigMap = createServiceConfigMap(prjModel, iflModel, serviceType, providerList, createServiceConfig);
        Map<String, Properties> consumersConfigMap = createServiceConfigMap(prjModel, iflModel, serviceType, consumerList, createServiceConfig);
        // invoke service artifacts generation.
        ServiceGenerator serviceGen = locator.getServiceGenerator(serviceType);
        serviceGen.generateServiceArtifacts(prjModel, iflModel, groupName, providersConfigMap, consumersConfigMap);

    }

    private String getServiceTypeForServiceGroup(IFLModel iflModel, String serviceGroup) {

        String serviceType = this.getServiceType().trim();
        if (serviceType.length() == 0 ) {
            // find the service type from IFL.
            List<String> serviceGroups = iflModel.getServiceGroupNames();
            if ( serviceGroups.contains(serviceGroup)) {
                Set<String> groupedServices = new TreeSet<String>();
                Set<String> serviceRefs = new TreeSet<String>();
                findServicesInServiceGroup(iflModel, serviceGroup, groupedServices, serviceRefs);
               String serviceName = null;
               if (groupedServices.size() > 0 ) {
                   serviceName = groupedServices.iterator().next();
               } else if ( serviceRefs.size() > 0 ) {
                   serviceName = serviceRefs.iterator().next();
               }
               if (serviceName != null) {
                   serviceType = getServiceTypeForServiceName(iflModel, serviceName);
               }
            }
        }
        return serviceType;
    }

    private String getServiceTypeForServiceName(IFLModel iflModel, String serviceName) {

        String serviceType = this.getServiceType().trim();
        if (serviceType.length() == 0 ) {
            // find the service type from IFL.
            Set<String> serviceTypes = iflModel.getServiceTypes();
            for (String type : serviceTypes) {
                if(iflModel.getServicesForType(type).contains(serviceName)) {
                    serviceType = type;
                    break;
                }
            }
        }
        return serviceType;
    }
    
    private List<String> createServiceList(String services) {
        List<String> serviceList = new ArrayList<String>();
        String[] tokens = services.split(",");
        for (String token : tokens) {
            if (token.trim().length() > 0 ) {
                serviceList.add(token.trim());
            }
        }
        return serviceList;
    }

    private Map<String, Properties> createServiceConfigMap(ServiceProjectModel prjModel, IFLModel iflModel, String serviceType, List<String> services, boolean createServiceConfig) throws IOException {
        Map<String, Properties> serviceConfigMap = new HashMap<String, Properties>();
        for (String service : services) {
            Properties configProps = getServiceConfiguration(prjModel, iflModel, serviceType, service, createServiceConfig);
            serviceConfigMap.put(service, configProps);
        }
        return serviceConfigMap;
    }
    private void findServicesInServiceGroup(IFLModel ifl, String group, Set<String> groupedServices, Set<String> serviceRefs) {
        groupedServices.clear();
        serviceRefs.clear();

        // build grouped services and services references sets and then their config maps.
        serviceRefs.addAll(ifl.getServiceReferencesInServiceGroup(group));
        // remove the service references (services after "calls") from the list of all services (including service refs)
        // to get the grouped services for which artifacts need to be generated.
        groupedServices.addAll(ifl.getServicesInServiceGroup(group));
        groupedServices.removeAll(serviceRefs);
    }
    private AppBuildManager createAppBuildManager() {

        String applicationId = this.project_.getArtifactId();
        if (applicationId == null) {
            applicationId = ApplicationProjectModel.DEF_APPLICATION_ID;
        }
        String name = this.project_.getName();
        if (name == null) {
            name = applicationId;
        }
        String displayName = name;
        String description = this.project_.getDescription();
        if (description == null) {
            description = name;
        }

        Properties prjProps = new Properties();
        prjProps.setProperty(ApplicationProjectModel.PROP_APPLICATION_ID, applicationId);
        prjProps.setProperty(ApplicationProjectModel.PROP_NAME, name);
        prjProps.setProperty(ApplicationProjectModel.PROP_DISPLAY_NAME, displayName);
        prjProps.setProperty(ApplicationProjectModel.PROP_DESCRIPTION, description);

        AppBuildManager appBuildMgr = new AppBuildManager(this.baseDirectory_, prjProps);
        return appBuildMgr;        
    }

    private Properties getServiceConfiguration(ServiceProjectModel prjModel, IFLModel iflModel, String serviceType, String serviceName, boolean createServiceConfig) throws IOException {
        File configFile = new File(prjModel.getConfigDir(serviceName), ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
        if (!configFile.exists() && createServiceConfig) {
            // create it using default configuration.
            configFile = createServiceConfiguration(prjModel, iflModel, serviceType, serviceName, new Properties());
        }
        if (configFile == null) {
            throw new IOException("Service Configuration file not found for service " + serviceName);
        }
        if (!configFile.exists()) {
            throw new IOException("Service Configuration file not found for service " + serviceName + " at location " + configFile.getAbsolutePath());
        }
        Properties configProps = readProperties(configFile);
        return configProps;
    }

    private File createServiceConfiguration(ServiceProjectModel prjModel, IFLModel iflModel, String serviceType, String serviceName, Properties userInput) throws IOException {
        ServiceDescriptor sd;
        try {
            sd = this.getToolsLookup().getServiceDescriptor(serviceType);
        } catch (Exception ex) {
            this.getLog().debug(ex);
            throw new IOException(ex.getMessage());
        }
        ServiceConfigurationHelper configHelper = new ServiceConfigurationHelper(sd);
        return configHelper.generateServiceConfiguration(prjModel, iflModel, serviceName, userInput);
    }

    private Properties readProperties(File propFile) throws IOException {
        Properties props = new Properties();
        FileInputStream inS = null;
        try {
            inS = new FileInputStream(propFile);
            props.load(inS);
        } finally {
            if (inS != null) {
                try {
                    inS.close();
                } catch (IOException ex) {
                    //ignore
                }
            }
        }
        return props;
    }

}
