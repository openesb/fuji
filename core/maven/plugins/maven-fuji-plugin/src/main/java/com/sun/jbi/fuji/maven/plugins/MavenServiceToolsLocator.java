/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.plugins;

import com.sun.jbi.fuji.maven.util.AbstractServiceToolsLocator;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.glassfish.openesb.tools.common.ToolsLookup;

/**
 * @author Kirill Sorokin
 * @author chikkala
 */
public class MavenServiceToolsLocator extends AbstractServiceToolsLocator {

    private static final Logger LOG = Logger.getLogger(MavenServiceToolsLocator.class.getName());

    private ArtifactFactory artifactFactory;
    private ArtifactResolver artifactResolver;
    private ArtifactRepository localRepository;
    private List remoteRepositories;
    
    /** fuji current version */
    private String fujiVersion_;

    public MavenServiceToolsLocator(
            ToolsLookup toolsLookup,
            ArtifactFactory artifactFactory,
            ArtifactResolver artifactResolver,
            ArtifactRepository localRepository,
            List remoteRepositories,
            String fujiVersion) {
        
        super(toolsLookup);
        this.artifactFactory = artifactFactory;
        this.artifactResolver = artifactResolver;

        this.localRepository = localRepository;
        this.remoteRepositories = remoteRepositories;
        this.fujiVersion_ = fujiVersion;
    }

    protected URL locateServiceArchetype(String serviceType) throws IOException {
        
        final String artifactId = serviceType.toLowerCase() + ARCHETYPE_SUFFIX;

        try {
            final Artifact artifact =
                    resolveArtifact(COMP_ARCHETYPE_GROUP_ID, artifactId, fujiVersion_);

            return artifact.getFile().toURI().toURL();
        } catch (Exception e) {
            LOG.log(Level.FINE, e.getMessage(), e);
            throw new IOException("Unable to locate archetype for service: " + serviceType);
        }
    }

    protected URL locateServicePlugin(String serviceType) throws IOException {

        final String artifactId = MAVEN_PREFIX + serviceType.toLowerCase() + PLUGIN_SUFFIX;

        try {
            final Artifact artifact =
                    resolveArtifact(COMP_PLUGIN_GROUP_ID, artifactId, fujiVersion_);

            return artifact.getFile().toURI().toURL();
        } catch (Exception e) {
            LOG.log(Level.FINE, e.getMessage(), e);
            throw new IOException("Unable to locate maven plugin for service: " + serviceType);
        }
    }

    protected URL locateServiceInstaller(String componentName) throws IOException {

        try {
            final Artifact artifact =
                    resolveArtifact(COMP_INSTALLER_GROUP_ID, componentName, fujiVersion_);
            return artifact.getFile().toURI().toURL();
        } catch (Exception e) {
            LOG.log(Level.FINE, e.getMessage(), e);
            throw new IOException("Unable to locate jbi component installer for: " + componentName);
        }
    }



    protected Artifact resolveArtifact(
            final String groupId,
            final String artifactId,
            final String version) throws Exception {

        final Artifact artifact = artifactFactory.createArtifactWithClassifier(
                groupId, artifactId, version, "jar", null);
        artifactResolver.resolve(artifact, remoteRepositories, localRepository);

        return artifact;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String BASE_GROUP_ID =
            "open-esb.fuji"; // NOI18N
    private static final String COMP_INSTALLER_GROUP_ID =
            BASE_GROUP_ID + ".components.installers"; // NOI18N
    private static final String COMP_ARCHETYPE_GROUP_ID =
            BASE_GROUP_ID + ".components.archetypes"; // NOI18N
    private static final String COMP_PLUGIN_GROUP_ID =
            BASE_GROUP_ID + ".components.plugins"; // NOI18N
    private static final String MAVEN_PREFIX = 
            "maven-"; // NOI18N
    private static final String ARCHETYPE_SUFFIX = 
            "-archetype"; // NOI18N
    private static final String PLUGIN_SUFFIX = 
            "-plugin"; // NOI18N
}
