/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RunMojo.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.plugins;

import java.util.logging.LogRecord;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.felix.main.AutoActivator;
import org.codehaus.plexus.util.FileUtils;

/**
 * Goal which captures all of the required bundles to create an application
 * distribution.
 * @execute goal="dist"
 * @goal run
 * 
 */
public class RunMojo
        extends AbstractMojo {
    // List of felix config properties that have relative path values
    private static final String[] FELIX_PATH_PROPERTIES = new String [] {
        "felix.cache.profiledir", "felix.auto.install.1", "felix.auto.start.1"
    };
    // Felix cache config key
    private static final String FELIX_CACHE = "felix.cache.profiledir";
    // Default cache dir name
    private static final String FELIX_CACHE_DIR_NAME = "cache";
    // Default cache dir name
    private static final String FELIX_BUNDLE_DIR_NAME = "bundle";
    // Relative path to felix config file
    private static final String FELIX_CONFIG = "conf/config.properties";
    
    /**
     * target dir
     * @parameter expression="${project.build.directory}"
     * @required
     */
    private File outputDirectory;
    /**
     * base dir
     * @parameter expression="${project.basedir}"
     * @required
     */
    private File baseDirectory;
    
    /**
     * List of loggers that we allow to be output to the felix console.
     */
    List<String> allowedLoggers_ = new ArrayList<String>();
    
    /**
     * Do our stuff.
     * @throws org.apache.maven.plugin.MojoExecutionException
     */
    public void execute()
            throws MojoExecutionException {
        
        File felixDir = new File(baseDirectory, "target/felix");
        File cacheDir = new File(felixDir, FELIX_CACHE_DIR_NAME);
        File bundleDir = new File(felixDir, FELIX_BUNDLE_DIR_NAME);
        File configFile = new File(felixDir, FELIX_CONFIG);
        
        // initialize allowed loggers
        allowedLoggers_.add("com.sun.jbi.framework");
        
        try {
            
            // Clean up felix install with every run
            if (cacheDir.exists()) {
                FileUtils.deleteDirectory(cacheDir);
                cacheDir.mkdir();
            }
            
            Properties configProps = new Properties();
            configProps.load(new FileInputStream(configFile));
            //configProps.setProperty(FELIX_CACHE, cacheDir.getAbsolutePath());
            
            // Replace relative file URLs in config with the Maven directory
            // path
            for (String prop : FELIX_PATH_PROPERTIES) {
                configProps.setProperty(prop, configProps.getProperty(prop).
                        replace("felix/", "target/felix/"));
            }
            
            //unset URLStreamHandlerFactory, which causes big problems in NB
            URL url = new URL("file://tmp");
            Field handlerFactory = url.getClass().getDeclaredField("factory");
            handlerFactory.setAccessible(true);
            handlerFactory.set(null, null);
            // ditto for URLConnection
            handlerFactory = URLConnection.class.getDeclaredField("factory");
            handlerFactory.setAccessible(true);
            handlerFactory.set(null, null);
            
            final FileHandler fh = new FileHandler(
                    new File(felixDir, "felix.log").getAbsolutePath());
            fh.setFormatter(new SimpleFormatter());
            
            Logger comLogger = Logger.getLogger("");
            LogManager.getLogManager().addLogger(comLogger);
            for (Handler handler : comLogger.getHandlers()) {
                handler.setFilter(new Filter () {

                    public boolean isLoggable(LogRecord logEntry) {
                        boolean isLoggable = false;
                        
                        for (String loggerName : allowedLoggers_) {
                            if (logEntry.getLoggerName().startsWith(loggerName)) {
                                isLoggable = true;
                                break;
                            }
                        }
                        
                        if (!isLoggable && 
                            logEntry.getLevel().intValue() >= Level.INFO.intValue()) {
                            fh.publish(logEntry);
                        }
                        
                        return isLoggable;
                    }
                    
                });
            }
            
            // Create a list for custom framework activators and
            // add an instance of the auto-activator it for processing
            // auto-install and auto-start properties. Add this list
            // to the configuration properties.
            List list = new ArrayList();
            list.add(new AutoActivator(configProps));
            configProps.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, list);
        
            /**
            List list = new ArrayList();
            list.add(new AutoActivator(configProps));
            // (8) Create a case-insensitive property map.
            Map configMap = new StringMap(configProps, false);
             */
            // (9) Create an instance of the framework.
            Felix felix = new Felix(configProps);
            felix.start();
                
            Object x = new Object();
            synchronized(x) {
                x.wait();
            }
            
        }
        catch (Exception ex) {
            throw new MojoExecutionException(ex.toString(), ex);
        }
    }

}
