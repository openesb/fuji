/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.fuji.maven.plugins;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * Fuji Admin interface through maven plugin to deploy, undeploy, start, stop the
 * application bundle on the fuji runtime running on a external vm. Requires
 * jmx interface. 
 * 
 * @author chikkala
 */
public abstract class AdminMojo extends BaseMojo {

    /**
     *  The project model.
     *
     *  @parameter expression="${project}"
     */
    private MavenProject mProject;
    /**
     * jmx admin port on which jmx remote connector of the the fuji runtime is listening.
     *
     * @parameter property="adminPort" alias="adminPort" expression="${fuji.admin.port}" default-value="8699"
     */
    private String mAdminPort;
    /**
     * jmx admin user - username for the jmx remote connector of the fuji runtime.
     *
     * @parameter property="adminUser" alias="adminUser" expression="${fuji.admin.user}" default-value=" "
     */
    private String mAdminUser;
    /**
     * jmx admin password - password for the jmx remote connector of the fuji runtime.
     *
     * @parameter property="adminPassword" alias="adminPassword" expression="${fuji.admin.password}" default-value=" "
     */
    private String mAdminPassword;
    /**
     * start on deploy - true for start the bundle after deploy, false for no action. defaults to true.
     *
     * @parameter property="startOnDeploy" alias="startOnDeploy" expression="${fuji.admin.start.ondeploy}" default-value="true"
     */
    private boolean mStartOnDeploy;
    
    /**
     * true to redeploy (uninstall and install) the bundle if it already exists,
     * false for no redeployment which will atempt to deploy directly without check.
     * defaults to true.
     * 
     * @parameter property="reDeploy" alias="reDeploy" expression="${fuji.admin.redeploy}"  default-value="true"
     */
    private boolean mReDeploy;

    public boolean isReDeploy() {
        return this.mReDeploy;
    }

    public void setReDeploy(boolean reDeploy) {
        this.mReDeploy = reDeploy;
    }
    
    public boolean isStartOnDeploy() {
        return mStartOnDeploy;
    }

    public void setStartOnDeploy(boolean startOnDeploy) {
        this.mStartOnDeploy = startOnDeploy;
    }

    public String getAdminPort() {
        return mAdminPort;
    }

    public void setAdminPort(String adminPort) {
        this.mAdminPort = adminPort;
    }

    public String getAdminUser() {
        return mAdminUser;
    }

    public void setAdminUser(String adminUser) {
        this.mAdminUser = adminUser;
    }

    public String getAdminPassword() {
        return mAdminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.mAdminPassword = adminPassword;
    }

    protected String getApplicationJarPath() {
        String appJarPath = null;
        String outputDirectory = this.mProject.getBuild().getDirectory();
        String applicationId = this.mProject.getArtifactId();
        String applicationVersion = this.mProject.getVersion();
        String versionSuffix = "-" + applicationVersion;
        if (applicationVersion == null || applicationVersion.trim().equals("")) {
            versionSuffix = "";
        }
        File applicationJar = new File(outputDirectory, applicationId + versionSuffix + ".jar");
        appJarPath = applicationJar.getAbsolutePath();
        this.getLog().info("Bundle Jar Path: " + appJarPath);

        return appJarPath;
    }

    /**
     *  deployed the built fuji app jar file to the standalone fuji runtime using fuji jmx
     *  admin interface.
     *
     *  @goal deploy-app
     */
    public static class DeployAppMojo extends AdminMojo {

        public void execute() throws MojoExecutionException, MojoFailureException {
            String appJarPath = getApplicationJarPath();
            this.getLog().info("Deploying the Bundle = " + appJarPath);

            FujiAdmin admin = new FujiAdmin("localhost", this.getAdminPort(), this.getAdminUser(), this.getAdminPassword());
            String bundlePath = appJarPath;
            try {
                String result = admin.deploy(bundlePath, isStartOnDeploy(), isReDeploy());
                this.getLog().info("Bundle Deployed : " + result);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to Deploy Bundle", ex);
            }
        }
    }

    /**
     *  undeploys the deployed fuji app correspoding to this project from the standalone
     *  fuji runtime using jmx admin interface. Note that this goal looks for the app in
     *  the runtime using  jar file path built in this project as a location of the bundle
     *  in the runtime. If the path or the app name is changed, it can not find the previously
     *  deployed app with the old name.
     *
     *  @goal undeploy-app
     */
    public static class UnDeployAppMojo extends AdminMojo {

        public void execute() throws MojoExecutionException, MojoFailureException {
            String appJarPath = getApplicationJarPath();
            this.getLog().info("Undeploying the Bundle = " + appJarPath);

            FujiAdmin admin = new FujiAdmin("localhost", this.getAdminPort(), this.getAdminUser(), this.getAdminPassword());
            String bundlePath = appJarPath;
            try {
                String result = admin.undeploy(bundlePath);
                this.getLog().info("Bundle Undeployed : " + result);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to Undeploy Bundle", ex);
            }
        }
    }

    /**
     *  starts the deployed app corresponds to this project.
     *
     *  @goal start-app
     */
    public static class StartAppMojo extends AdminMojo {

        public void execute() throws MojoExecutionException, MojoFailureException {

            String appJarPath = getApplicationJarPath();
            this.getLog().info("Starting the Bundle = " + appJarPath);

            FujiAdmin admin = new FujiAdmin("localhost", this.getAdminPort(), this.getAdminUser(), this.getAdminPassword());
            String bundlePath = appJarPath;
            try {
                String result = admin.start(bundlePath);
                this.getLog().info("Bundle Started : " + result);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to Start Bundle", ex);
            }
        }
    }

    /**
     *  stops the deployed app corresponding to this project
     * 
     *  @goal stop-app
     */
    public static class StopAppMojo extends AdminMojo {

        public void execute() throws MojoExecutionException, MojoFailureException {
            String appJarPath = getApplicationJarPath();
            this.getLog().info("Stopping the Bundle = " + appJarPath);

            FujiAdmin admin = new FujiAdmin("localhost", this.getAdminPort(), this.getAdminUser(), this.getAdminPassword());
            String bundlePath = appJarPath;
            try {
                String result = admin.stop(bundlePath);
                this.getLog().info("Bundle Stopped : " + result);
            } catch (Exception ex) {
                throw new MojoExecutionException("Failed to Stop Bundle", ex);
            }
        }
    }
    /**
     * Admin interface which will be used in this mojo goal execution to
     * talk to the fuji runtime.
     */
    public static class FujiAdmin {

        private static Logger LOG = Logger.getLogger(FujiAdmin.class.getName());
        private JMXManager mJmxMgr;

        public FujiAdmin(JMXManager jmxMgr) {
            this.mJmxMgr = jmxMgr;
        }

        public FujiAdmin(String host, String port, String username, String password) {
            this.mJmxMgr = new JMXManager(host, port, username, password);
        }

        public Logger getLogger() {
            return LOG;
        }

        public String deploy(String bundlePath, boolean startOnDeploy, boolean reDeploy) throws Exception {
            getLogger().info("Deploying Bundle " + bundlePath);
            Object result = null;
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "installBundle", bundlePath, startOnDeploy, reDeploy);
            return (String) result;
        }

        public String start(String bundlePath) throws Exception {
            //TODO: this should be based on bundle path.
            getLogger().info("Starting Bundle " + bundlePath);
            Object result = null;
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "startBundle", bundlePath);
            return (String) result;
        }

        public String stop(String bundlePath) throws Exception {
            //TODO: this should be based on bundle path.
            getLogger().info("Stopping Bundle " + bundlePath);
            Object result = null;
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "stopBundle", bundlePath);
            return (String) result;
        }

        public String undeploy(String bundlePath) throws Exception {
            getLogger().info("Undeploying Bundle " + bundlePath);
            Object result = null;
            ObjectName mbeanOName = JMXManager.getAdminMBeanOjectName();
            result = this.mJmxMgr.invokeMBeanOperation(mbeanOName, "uninstallBundle", bundlePath);
            return (String) result;
        }
    }
    /**
     * JMX interface to connect to the fuji admin mbeans to perfrom the admin tasks
     * correpsonding to the admin goals in tis mojo.
     */
    public static class JMXManager {

        private static Logger LOG = Logger.getLogger(JMXManager.class.getName());
        public static final String FUJI_JMX_DOMAIN = "com.sun.jbi.fuji";
        public static final String FUJI_ADMIN_OBJECT_NAME = FUJI_JMX_DOMAIN + ":runtime=OSGi,type=admin,service=admin";

        private static ObjectName sAdminMBeanOName;
        private MBeanServerConnection mMBS;
        private JMXConnector mConnector;
        private String mHost;
        private String mPort;
        private String mUsername;
        private String mPassword;

        public JMXManager() {
            this("localhost", "8699", "", "");
        }

        public JMXManager(String port) {
            this("localhost", port, "", "");
        }

        public JMXManager(String port, String username, String password) {
            this("localhost", port, username, password);
        }

        public JMXManager(String host, String port, String username, String password) {
            this.mHost = host;
            this.mPort = port;
            this.mUsername = username;
            this.mPassword = password;
        }

        public String getHost() {
            return mHost;
        }

        public void setHost(String host) {
            this.mHost = host;
        }

        public String getPassword() {
            return mPassword;
        }

        public void setPassword(String password) {
            this.mPassword = password;
        }

        public String getPort() {
            return mPort;
        }

        public void setPort(String port) {
            this.mPort = port;
        }

        public String getUsername() {
            return mUsername;
        }

        public void setUsername(String username) {
            this.mUsername = username;
        }

        public MBeanServerConnection getMBeanServerConnection() {
            return mMBS;
        }

        protected String getURL() {

            String host = getHost();
            String port = getPort();


            if (host == null) {
                host = "localhost";
            }
            if (port == null || port.trim().length() == 0 ) {
                port = "8699";
            }

            String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
            return url;
        }

        protected Map<String, Object> getEnvironment() {
            Map<String, Object> env = new HashMap<String, Object>();
            String username = getUsername();
            String password = getPassword();
            if (username == null || username.trim().length() == 0 ) {
                username = "";
            }
            if (password == null || password.trim().length() == 0 ) {
                password = "";
            }
            // LOG.info("Connection Credentials user:'" + username + "' password:" + password.length());
            env.put(JMXConnector.CREDENTIALS, new String[]{username, password});
            return env;
        }

        /**
         * JMX Agent connection
         * 
         */
        protected void connect() throws Exception {
            // Create JMX Agent URL
            String url = getURL();
            LOG.info("Conneting to FUJI Server:  JMX Connetion URL " + url);
            JMXServiceURL jmxServiceURL = new JMXServiceURL(url);
            // Connect the JMXConnector
            mConnector = JMXConnectorFactory.connect(jmxServiceURL, getEnvironment());
            // Get the MBeanServerConnection
            mMBS = mConnector.getMBeanServerConnection();
        }

        protected void close() throws Exception {

            //Close the connection
            try {
                if ( mConnector != null ) {
                    mConnector.close();
                }
            } finally {
                mConnector = null;
                mMBS = null;
            }

        }

        public static ObjectName getAdminMBeanOjectName() {

            try {
                if (sAdminMBeanOName == null) {
                    sAdminMBeanOName = new ObjectName(FUJI_ADMIN_OBJECT_NAME);
                }
            } catch (Exception ex) {
                LOG.log(Level.FINE, ex.getMessage(), ex);
            }
            return sAdminMBeanOName;
        }

        public boolean isJMXAdminAvailable() {
            boolean available = false;
            try {
                this.connect();
            } catch (Exception ex) {
                return false; // can not connect.

            }
            try {
                available = this.getMBeanServerConnection().isRegistered(JMXManager.getAdminMBeanOjectName());
            } catch (Exception ex) {
                // ignore
            } finally {
                try {
                    this.close();
                } catch (Exception ex) {
                    // ingore.
                }
            }
            return available;
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName,
                Object[] params, String[] signature) throws Exception {
            Object resultObject = null;
            this.connect();
            try {
                resultObject = getMBeanServerConnection().invoke(objName, operationName, params, signature);
            } catch (MBeanException ex) {
                Exception targetEx = ex.getTargetException();
                if (targetEx == null) {
                    targetEx = ex;
                }
                throw targetEx;
            } finally {
                try {
                    this.close();
                } catch (Exception ex) {
                    // ingore.
                }
            }
            return resultObject;
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName) throws Exception {
            Object[] params = new Object[0];
            String[] signature = new String[0];
            return invokeMBeanOperation(objName, operationName, params, signature);
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName, String param) throws Exception {
            Object[] params = {param};
            String[] signature = {"java.lang.String"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }

        public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2) throws Exception {
            Object[] params = {param1, Boolean.valueOf(param2)};
            String[] signature = {"java.lang.String", "boolean"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }
        public Object invokeMBeanOperation(ObjectName objName, String operationName, String param1, boolean param2, boolean param3) throws Exception {
            Object[] params = {param1, Boolean.valueOf(param2), Boolean.valueOf(param2)};
            String[] signature = {"java.lang.String", "boolean", "boolean"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }
        public Object invokeMBeanOperation(ObjectName objName, String operationName, long param) throws Exception {
            Object[] params = {Long.valueOf(param)};
            String[] signature = {"long"};
            return invokeMBeanOperation(objName, operationName, params, signature);
        }
    }
}
