/*
 * This is a generated file, the source can be modfied as per the
 * required aggregation strategy.
 * 
 * Aggregator.java
 */

package ${packagename};

import org.glassfish.openesb.api.eip.split.BaseSplitter;
import org.glassfish.openesb.api.service.ServiceMessage;

import java.util.ArrayList;


/**
 * Split EIP POJO.
 * 
 * @author 
 */
public class ${classname} extends BaseSplitter{

    
    public ${classname}(){
       System.out.println("Java Splitter"); 
    }
    
    
    /**
     * Split the current active message in the exchange and return the split documents ina 
     * ArrayList
     * 
     * @param svcMsg - svcMsg to split
     */
    public ArrayList split(ServiceMessage svcMsg)
        throws Exception{
        ArrayList list = new ArrayList();
        list.add(svcMsg.getPayload());
        return list;
    }
    
}