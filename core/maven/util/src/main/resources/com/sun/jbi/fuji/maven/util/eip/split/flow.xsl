<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : flow.xsl
    Created on : 
    Author     : 
    Description:
        Split a message in to multiple messages.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
