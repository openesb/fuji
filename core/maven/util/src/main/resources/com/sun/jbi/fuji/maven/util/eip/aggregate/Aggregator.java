/*
 * This is a generated file, the source can be modfied as per the
 * required aggregation strategy.
 * 
 * Aggregator.java
 */

package ${packagename};

import org.glassfish.openesb.api.service.ServiceMessage;


import org.glassfish.openesb.api.eip.aggregate.Aggregate;
import org.glassfish.openesb.api.eip.aggregate.BaseAggregator;
import org.glassfish.openesb.api.eip.aggregate.AggregateHelper;

/**
 * A POJO (Simple Class) with Interceptor annotations to add Fuji 
 * message exchange interceptor.
 * 
 * @author 
 */
public class ${classname} extends BaseAggregator{

    
    /**
     * Get the correlation id. Default implementation, that returns a constant id 
     * @return the Correlation ID from the message
     */
    public Object getCorrelationID(ServiceMessage message){
        return AggregateHelper.inferID(message, config_);
    }
    
    /**
     * Default implementation which returns true if one or messages aggregated.
     *  @return true if all the conditions required to aggregate the message have
     *          been satisfied.
     */
    public boolean isAggregateComplete(Object correlationId, ServiceMessage[] serviceMessages){
        return AggregateHelper.areAllMessagesReceived(correlationId, serviceMessages, config_);
    }
    
    /**
     * Aggregate the messages being passed in, into one service message. This is
     * a default implementation, that selects the first message payload.
     * 
     * @param corrId  - correlation ID 
     * @param serviceMessages - the messages to aggregate
     */
    public Object aggregateMessages(Object corrId, ServiceMessage[] serviceMessages)
        throws Exception {

       return AggregateHelper.aggregateMessages(corrId, serviceMessages, config_);
    }
}