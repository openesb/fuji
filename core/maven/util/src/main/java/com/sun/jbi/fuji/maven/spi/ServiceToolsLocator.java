/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.spi;

import java.io.IOException;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 * This interface should be implemented by entities wishing to utilize the service
 * packaging routines in Fuji maven utilities. It provides them with means to locate
 * and use the service archetype data and (optionally) service-specific packaging
 * logic.
 *
 * Note:  Since com.sun.jbi.fuji.maven.spi.ServicePacakger and its default implemenation
 * is deprecated in favour of new Service descriptor, the methods in this interface
 * were changed significantly to provide the descriptor, generator, build, packageing
 * interfaces.
 *
 * @see org.glassfish.openesb.tools.common.service.ServiceDescriptor
 * @see org.glassfish.openesb.tools.common.service.spi.ServiceGenerator
 * @see org.glassfish.openesb.tools.common.service.spi.ServiceBuilder
 * @see org.glassfish.openesb.tools.common.service.spi.ServicePackager
 *
 * @author Kirill Sorokin
 * @author chikkala
 */
public interface ServiceToolsLocator {
    /**
     * return the service descriptor interface from the service descriptor bundle. If the
     * service descriptor bundle not present for a service type, but has deprecated archetype,
     * and maven plugin model, it should return a wrapper service descriptor inteface
     * around the deprecated interfaces based on archetype and plugins.
     *
     * @param serviceType
     * @return
     * @throws java.io.IOException
     */
    public ServiceDescriptor getServiceDescriptor(String serviceType) throws IOException;
    /**
     * provides the ServiceGenerator for a specific service type. If the service type
     * does not have a specific ServiceGenerator implementation, it should return a
     * default implementation or a wrapper around deprecated com.sun.jbi.fuji.maven.spi.ServicePacakger
     * interface implemented by the maven plugin model for the service type.
     *
     * @param serviceType
     * @return ServiceGenerator
     * @throws java.io.IOException
     */
    public ServiceGenerator getServiceGenerator(String serviceType) throws IOException;
    /**
     * provides the ServiceBuilder for a specific service type. If the service type
     * does not have a specific ServiceBuilder implementation, it should return a
     * default implementation or a wrapper around deprecated com.sun.jbi.fuji.maven.spi.ServicePacakger
     * interface implemented by the maven plugin model for the service type.
     * @param serviceType
     * @return ServiceBuilder
     * @throws java.io.IOException
     */
    public ServiceBuilder getServiceBuilder(String serviceType) throws IOException;
    /**
     * provides the ServicePackager for a specific service type. If the service type
     * does not have a specific ServicePackager implementation, it should return a
     * default implementation or a wrapper around deprecated com.sun.jbi.fuji.maven.spi.ServicePacakger
     * interface implemented by the maven plugin model for the service type.
     * @param serviceType
     * @return ServicePackager
     * @throws java.io.IOException
     */
    public ServicePackager getServicePackager(String serviceType) throws IOException;
}
