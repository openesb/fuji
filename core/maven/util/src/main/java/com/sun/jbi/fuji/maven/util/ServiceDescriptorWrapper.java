/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.util;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.maven.spi.ServiceToolsLocator;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.Utils;
import org.glassfish.openesb.tools.common.descriptor.Configuration;
import org.glassfish.openesb.tools.common.descriptor.Configuration.UsageHint;
import org.glassfish.openesb.tools.common.descriptor.FeatureInfo;
import org.glassfish.openesb.tools.common.descriptor.HelpContext;
import org.glassfish.openesb.tools.common.descriptor.Property;
import org.glassfish.openesb.tools.common.descriptor.PropertyGroup;
import org.glassfish.openesb.tools.common.descriptor.Template;
import org.glassfish.openesb.tools.common.service.DefaultServiceBuilder;
import org.glassfish.openesb.tools.common.service.DefaultServiceGenerator;
import org.glassfish.openesb.tools.common.service.DefaultServicePackager;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 * This class is a ServiceDescriptor Wrapper for the deprecated archetype and plugin model.
 * Should be removed when all the service types completely support descriptor bundle.
 * 
 * @author chikkala
 */
public class ServiceDescriptorWrapper implements ServiceDescriptor {

    private static final Logger LOG = Logger.getLogger(ServiceDescriptorWrapper.class.getName());
    protected static final String APP_SERVICE_PACKAGER = "App-Service-Packager"; //NOI18N
    private String mServiceType;
    private String mComponentName;
    private boolean mBinding;
    private URL mArchetypeURL;
    private URL mPluginURL;
    private ServiceGenerator mSG;
    private ServiceBuilder mSB;
    private ServicePackager mSP;
    private ServiceToolsLocator mLocator;
    private ServiceSPIWrapper mSpiWrapper;

    public ServiceDescriptorWrapper(String serviceType, URL archetypeURL, URL pluginURL, ServiceToolsLocator locator) {
        this.mServiceType = serviceType;
        this.mArchetypeURL = archetypeURL;
        this.mPluginURL = pluginURL;
        this.mLocator = locator;
        LOG.info("### Creating ServiceDescriptor wrapper for service type " + serviceType +
                " ArchetypeURL: " + this.mArchetypeURL +
                " pluginURL " + this.mPluginURL +
                " service tools locator " + locator);
        init();
    }

    private void init() {
        String compName = "INVALID";
        Utils.UnJar archetypeUnJar = new Utils.UnJar(this.mArchetypeURL);
        String suMfPath = "archetype-resources/src/main/resources/META-INF/MANIFEST.MF";
        ByteArrayOutputStream outBuff = new ByteArrayOutputStream();
        try {
            archetypeUnJar.copy(suMfPath, outBuff);
            Properties props = new Properties();
            props.load(new ByteArrayInputStream(outBuff.toByteArray()));
            compName = props.getProperty("Component-Name");
        } catch (Exception ex) {
            LOG.log(Level.INFO, ex.getMessage(), ex);
        }
        this.mComponentName = compName;
    }

    public String getServiceType() {
        return this.mServiceType;
    }

    public String getComponentName() {
        return this.mComponentName;
    }

    public boolean isBindingComponent() {
        return this.mBinding;
    }

    public String getDisplayName() {
        return this.getServiceType() + " Service";
    }

    public String getShortDescription() {
        return getDisplayName();
    }

    public String getLongDescription() {
        return getDisplayName();
    }

    public HelpContext getHelpContext() {
        return null;
    }

    public URL getIcon(IconType type) {
        return null;
    }

    public List<URL> getIcons() {
        return new ArrayList<URL>();
    }

    public List<Configuration> getConfigurations() {
        return new ArrayList<Configuration>();
    }

    public Configuration getDefaultConfiguration() {
        final String name="default";
        final FeatureInfo info = new FeatureInfo() {

            public String getDisplayName() {
                return name;
            }

            public String getShortDescription() {
                return name;
            }

            public String getLongDescription() {
                return name;
            }

            public String getHelpID() {
                return name;
            }

            public URL getHelpURL() {
                return null;
            }

            public HelpContext getHelpContext() {
                return null;
            }

        };

        Configuration defConfig = new Configuration () {

            public FeatureInfo getInfo() {
                return info;
            }
            public UsageHint getUsageHint() {
                return null;
            }
            public String getName() {
                return name;
            }

            public Property getProperty(String name) {
                return null;
            }

            public List<Property> getPropertyList() {
                return new ArrayList<Property>();
            }

            public Properties getProperties() {
                return new Properties();
            }

            public List<PropertyGroup> getPropertyGroupList() {
                return new ArrayList<PropertyGroup>();
            }
        };
        return defConfig;
    }

    public List<Template> getTemplates() {
        return new ArrayList<Template>();
    }

    public Map<String, URL> getWebResources() {
        // These are temp resources until we move everything to decriptor bundle.
        Map<String, URL> resMap = new HashMap<String, URL>();
        String archetypeKey = "web/" + getServiceType() + "/archetype.jar";
        resMap.put(archetypeKey, this.mArchetypeURL);
        String pluginKey = "web/" + getServiceType() + "/plugin.jar";
        resMap.put(pluginKey, this.mPluginURL);
        return resMap;
    }

    public ServiceGenerator getServiceGenerator() {
        if (this.mSG == null) {
            this.mSG = getServiceSPIWrapper();
            if (this.mSG == null) {
                this.mSG = new DefaultServiceGenerator(this);
            }
        }
        return this.mSG;
    }

    public ServiceBuilder getServiceBuilder() {
        if (this.mSB == null) {
            this.mSB = getServiceSPIWrapper();
            if (this.mSB == null) {
                this.mSB = new DefaultServiceBuilder(this);
            }
        }
        return this.mSB;
    }

    public ServicePackager getServicePackager() {
        if (this.mSP == null) {
            this.mSP = getServiceSPIWrapper();
            if (this.mSP == null) {
                this.mSP = new DefaultServicePackager(this);
            }
        }
        return this.mSP;
    }

    private ServiceSPIWrapper getServiceSPIWrapper() {
        if (this.mSpiWrapper == null) {
            com.sun.jbi.fuji.maven.spi.ServicePackager packager = locateServicePackager(this.mServiceType, this.mPluginURL);
            if (packager != null) {
                LOG.info("ServiceSPI Wrapper is created for service type " + this.mServiceType);
                this.mSpiWrapper = new ServiceSPIWrapper(this.mServiceType, packager, this.mLocator);
            }
        }
        return this.mSpiWrapper;
    }

    protected com.sun.jbi.fuji.maven.spi.ServicePackager locateServicePackager(String serviceType, URL pluginURL) {

        InputStream in = null;
        JarInputStream jarInS = null;
        try {
            in = pluginURL.openStream();
            jarInS = new JarInputStream(in);
            Manifest jarManifest = jarInS.getManifest();
            String classname =
                    jarManifest.getMainAttributes().getValue(APP_SERVICE_PACKAGER);
            URLClassLoader loader = new URLClassLoader(
                    new URL[]{pluginURL},
                    getClass().getClassLoader());
            return (com.sun.jbi.fuji.maven.spi.ServicePackager) loader.loadClass(classname).newInstance();
        } catch (Exception e) {
            return null; // if a plugin not present,  Don't use the deprecated. default pacakger imple.
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    LOG.log(Level.FINEST, null, ex);
                }
            }
            if (jarInS != null) {
                try {
                    jarInS.close();
                } catch (IOException ex) {
                    LOG.log(Level.FINEST, null, ex);
                }
            }
        }
    }

    /**
     * wrapper for the deprecated service packager model based on the maven plugin.
     */
    public static class ServiceSPIWrapper implements ServiceGenerator, ServiceBuilder, ServicePackager {

        private com.sun.jbi.fuji.maven.spi.ServicePackager mDeprecatedPackager;
        private ServiceToolsLocator mLocator;
        private String mServiceType;

        protected ServiceSPIWrapper(String serviceType,
                com.sun.jbi.fuji.maven.spi.ServicePackager packager, ServiceToolsLocator locator) {
            this.mServiceType = serviceType;
            this.mDeprecatedPackager = packager;
            this.mLocator = locator;
        }

        private Properties mapToProperties(Map<Object, Object> map) {
            Properties props = new Properties();
            for (Object key : map.keySet()) {
                props.setProperty((String) key, (String) map.get(key));
            }
            return props;
        }

        public void generateServiceUnit(ServiceProjectModel prjModel, IFLModel ifl) throws IOException {
            List usedFiles = new ArrayList();
            Properties props = mapToProperties(prjModel.getProperties());
            this.mDeprecatedPackager.generateServiceUnit(this.mLocator, ifl, prjModel.getBaseDir(), props, usedFiles);
        }

        public void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String serviceName, Properties serviceConfig) throws IOException {
            LOG.info("GenerateServiceArtifacts is not supported in deprecated generator model");
        }
        
        public void generateServiceArtifacts(ServiceProjectModel prjModel, IFLModel ifl, String groupName, Map<String, Properties> providersServiceConfigMap, Map<String, Properties> consumersServiceConfigMap) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void deleteUnusedArtifacts(ServiceProjectModel prjModel, IFLModel ifl) throws IOException {
            LOG.info("deleteUnusedArtifacts is not supported in deprecated generator model");
        }

        public void deleteServiceArtifacts(ServiceProjectModel prjModel, String serviceName, Properties serviceConfig) throws IOException {
            LOG.info("deleteServiceArtifacts is not supported in deprecated generator model");
        }

        public void processResources(ServiceProjectModel prjModel, List<String> services, IFLModel ifl) throws IOException {
            String applicationId = prjModel.getProperty(ServiceProjectModel.PROP_APPLICATION_ID, "newapp");
            File baseDir = prjModel.getBaseDir();
            File outputDir = new File(prjModel.getBaseDir(), "target");
            this.mDeprecatedPackager.prepareServiceUnitResources(ifl, services, applicationId, baseDir, outputDir);
        }

        public void compile(ServiceProjectModel prjModel, List<String> services, IFLModel ifl) throws IOException {
            LOG.info("compile is not supported in deprecated generator model");
        }

        public File packageServiceUnit(ServiceProjectModel prjModel, List<String> services, IFLModel ifl) throws IOException {
            String applicationId = prjModel.getProperty(ServiceProjectModel.PROP_APPLICATION_ID, "newapp");
            File outputDir = new File(prjModel.getBaseDir(), "target");
            return this.mDeprecatedPackager.packageServiceUnit(ifl, this.mServiceType, services, applicationId, outputDir);
        }

        public void beforeServicePackaging(ServiceProjectModel prjModel, List<String> services) throws IOException {
            File outputDir = new File(prjModel.getBaseDir(), "target");
            File resourceDir = new File(prjModel.getBaseDir(), "target/classes/" + this.mServiceType);
            this.mDeprecatedPackager.beforeServicePackaging(outputDir, resourceDir, services);
        }

        public void beforeApplicationPackaging(ServiceProjectModel prjModel, List<String> services, ApplicationProjectModel appPrjModel) throws IOException {
            File appOutputDir = appPrjModel.getOutputDir();
            this.mDeprecatedPackager.beforeApplicationPackaging(appOutputDir, services);
        }
    }
}
