/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AnnotationHandler.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.net.URL;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.io.FileOutputStream;


/**
 * This class handles the processing of the annotation types. It will generate the 
 * correponding configuration artifacts during generate and package phases.
 * 
 */
public class AnnotationHandler {

    // Some constamts used by the Handlers
    public static final String CONFIG_FILE_TYPE         = ".properties";
    public static final String DEFAULT_CONFIG_FILE_NAME = "default";
    public static final String EXCLUDE_CONFIG_FILE_NAME = "exclude";

    public void AnnotationHandler() 
    {
    }

    public void setAnnotationConfigFilename (String fname)
    {
    }
        
    /**
     * Will construct the properties file name based on the given type.
     * @param type The flow type.
     */
    protected String getResourceFileName (String type) {
        String resourceFileName = type + "/" + DEFAULT_CONFIG_FILE_NAME + CONFIG_FILE_TYPE;
        return resourceFileName;
    }

    /**
     * Will construct the properties file name based on the given type.
     * @param type The flow type.
     */
    protected String getExcludeFileName (String type) {
        String resourceFileName = type + "/" + EXCLUDE_CONFIG_FILE_NAME + CONFIG_FILE_TYPE;
        return resourceFileName;
    }

    /**
     * Generates the named configuration in a destination directory.
     * This method is called in GenerateMojo to generate named configuration
     * in the project source directory
     * @param packageList Collection containing the PackageNameInfo for each file
     * @param startDir The starting directory
     * @param annotationDir Used for destination dir ("interceptor",...)
     * @throws java.lang.Exception
     */
    public void generateNamedConfiguration (Collection<PackageNameInfo> interceptList,
                                            String startDir,
                                            String annotationDirName,
                                            List usedFiles) throws IOException
    {
        ArrayList<String> invalidNames = new ArrayList<String>();
        ArrayList<String> nameList = new ArrayList<String>();
        ArrayList<String> duplicateNameList = new ArrayList<String>();

        // Process all packages and classes
        for (PackageNameInfo pni : interceptList)
        {
            Set annotationSet = pni.getAnnotationNames();
            for (Iterator iter = annotationSet.iterator(); iter.hasNext();) {

                String methodName = (String)iter.next();  
                AnnotationInfo annotationInfo = pni.getAnnotationInfo(methodName);
                String annotationName = annotationInfo.getElement("name");
                
                boolean newFile = false;

                if (nameList.contains(annotationName))
                {
                    duplicateNameList.add(annotationName);
                }

                else {
                    nameList.add(annotationName);
                    if (annotationName.indexOf(" ") == -1)
                    {
                        // Create the annotation directory if it does not already exist
                        String dirPath = startDir + "/" + annotationDirName + "/" + annotationName;
                        File annotationDir = new File(dirPath);
                        if (!annotationDir.exists()) 
                        {
                            annotationDir.mkdirs();
                        }

                        // If the file does not exist then we need to create a new one.
                        String configFilename = annotationDirName + CONFIG_FILE_TYPE;
                        File annotationFile = new File(dirPath, configFilename);
                        usedFiles.add(annotationFile);
                        if (!annotationFile.exists()) 
                        {
                            newFile = true;
                        }

                        // For now we will only update the values in the config file it
                        // it is new file.  We may add more smarts here later.
                        if (newFile)
                        {
                            // Load any default properties into the properites file.
                            Properties propertiesFile = new Properties();
                            URL sourceURL = this.getClass().getResource(getResourceFileName(annotationDirName));
                            if (sourceURL != null)
                            {
                                propertiesFile.load(sourceURL.openStream());
                            }

                            // Load the excluded properties into the excludePropertiesFile.
                            Properties excludePropertiesFile = new Properties();
                            URL excludeURL = this.getClass().getResource(getExcludeFileName(annotationDirName));
                            if (excludeURL != null)
                            {
                                excludePropertiesFile.load(excludeURL.openStream());
                            }

                            // Place the element values from the annotation into the properties file
                            // Note, we are currently excluding the "name" property value.
                            HashMap<String,String> elements = annotationInfo.getElements();
                            Iterator itr = elements.keySet().iterator();
                            while (itr.hasNext()) {
                                String key = (String)itr.next();
                                String value = elements.get(key);
                                if (!(excludePropertiesFile.containsKey(key)))
                                {
                                    propertiesFile.put(key,value);
                                }
                            }

                            // Write out the properties file
                            FileOutputStream outputStream = new FileOutputStream(annotationFile);
                            propertiesFile.store(outputStream,null);
                        }
                    }
                    else 
                    {
                        invalidNames.add(annotationName);
                    }
                }
            }
        }

        // Process and report any errors that may have been incountered.
        String errorMsg = "";
        String newLine = "";
        if (duplicateNameList.size() > 0)
        {
            errorMsg += "The following Interceptor names are not unique: " + duplicateNameList.toString();
            newLine = "\n";
        }
        if (invalidNames.size() > 0)
        {
            errorMsg += newLine + "The following Interceptor names are invalid: " + invalidNames.toString();
        }
        if (errorMsg != "")
        {
            throw new IOException(errorMsg);
        }
    }


    /**
     * Generates the annotation configuration for packaging.
     * @param srcDir build directory ( e.g. target/classes ) where the named
     * configuration is available
     * @param metaInfDir target metainf directory which will be packaged in the 
     * application jar file.
     * @param annotationDir Used for destination dir ("interceptor",...)
     * @throws java.lang.Exception
     */
    protected void generateConfigurationForPacakging (File srcDir, 
                                                      File metaInfDir,
                                                      String annotationDir) throws IOException 
    {
        // 1. create annotation directory
        //
        // This code is not used any more, since the annotation directory, which
        // is only "interceptor" at the moment, is place directly under the
        // META-INF directory (i.e. there is not annotation group),  For now, I'm
        // keeping the commented code here in case this is revised in the near future.

        //File metaAnnotationDir = new File(metaInfDir, annotationDir);
        //if (!metaAnnotationDir.exists()) {
        //    metaAnnotationDir.mkdirs();
        //}
        File metaAnnotationDir = metaInfDir; // remove if code above is used

        // 2. copy the configuration vales from the target/classes
        File artifactSrcDir  = new File(srcDir, annotationDir);
        File artifactDestDir = new File(metaAnnotationDir, annotationDir);
        if (artifactSrcDir.exists()) {
            MavenUtils.copy(artifactSrcDir, artifactDestDir);
        }
    }


}
