/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.util;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ApplicationProjectModel;
import org.glassfish.openesb.tools.common.FujiProjectModel;
import org.glassfish.openesb.tools.common.jbi.descriptor.Jbi;
import org.glassfish.openesb.tools.common.jbi.descriptor.ServiceUnit;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;

/**
 * This class should be the starting point for managing the integration app's
 * build lifecycle. 
 *
 * @author chikkala
 */
public class AppBuildManager {

    private static final Logger LOG = Logger.getLogger(AppBuildManager.class.getName());
    private ApplicationProjectModel mPrj;
    //TODO: this should probably go into the application project model.
    /** list of service unit projects per type*/
    private Map<String, ServiceProjectModel> mSuPrjMap;

    public AppBuildManager(File baseDir, Properties prjProps) {
        init(baseDir, prjProps);
    }

    public AppBuildManager(File baseDir, String appId, String version) {
        Properties prjProps = new Properties();
        prjProps.setProperty(FujiProjectModel.PROP_APPLICATION_ID, appId);
        init(baseDir, prjProps);
    }

    private void init(File baseDir, Properties prjProps) {
        this.mSuPrjMap = new HashMap<String, ServiceProjectModel>();
        this.mPrj = new ApplicationProjectModel(baseDir.getAbsolutePath(), prjProps);
    }

    public ApplicationProjectModel getAppProjectModel() {
        return this.mPrj;
    }

    public ServiceProjectModel getServiceProjectModel(String serviceType) {
        ServiceProjectModel suPrj = this.mSuPrjMap.get(serviceType);
        if (suPrj == null) {
            suPrj = ServiceProjectModel.newInstance(serviceType, this.mPrj);
            this.mSuPrjMap.put(serviceType, suPrj);
        }
        return suPrj;
    }

    public void build() {
        processResources();
        compile();
        packageApp();
    }

    public void clean() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void deleteUnusedArtifacts() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void generateSources() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void processResources() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void compile() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void packageApp() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    protected List<File> findFiles(File file, boolean recurse) {
        if (recurse) {
            return MavenUtils.findFiles(file, new FileFilter() {
                public boolean accept(final File file) {
                    return true;
                }
            });
        }
        List<File> fileList = new ArrayList<File>();
        FileFilter filter = new FileFilter() {
            public boolean accept(final File file) {
                return file.isFile();
            }
        };
        File[] list = null;
        if ( file.isFile()) {
            list = new File[1];
            list[0] = file;
        } else {
            list = file.listFiles(filter);
        }
        if ( list == null ) {
            list = new File[0];
        }
        fileList.addAll(Arrays.asList(list));
        return fileList;
    }

    public List<File> findUsedServiceArtifacts(IFLModel ifl) {
        List<File> usedFiles = new ArrayList<File>();

        for (String type : ifl.getServiceTypes()) {
            ServiceProjectModel suPrj = this.getServiceProjectModel(type);
            File srcDir = suPrj.getSourceDir();
            List<File> usedSrcFiles = findFiles(srcDir, true);
            usedFiles.addAll(usedSrcFiles);

            File configDir = suPrj.getConfigDir();
            if (!configDir.getAbsolutePath().replace("\\", "/").endsWith(FujiProjectModel.DEF_CONFIG_DIR) ){
                usedFiles.addAll(findFiles(configDir,false));
            }
            
            File resDir = suPrj.getResourcesDir();
            if (!resDir.getAbsolutePath().replace("\\", "/").endsWith(FujiProjectModel.DEF_RESOURCES_DIR) ){
                usedFiles.addAll(findFiles(resDir, false));
                File metainfDir = new File(resDir, "META-INF");
                usedFiles.addAll(findFiles(metainfDir, false));
            }
            
            for (String service : ifl.getServicesForType(type)) {
                configDir = suPrj.getConfigDir(service);
                resDir = suPrj.getResourcesDir(service);
                usedFiles.addAll(findFiles(configDir,false));
                usedFiles.addAll(findFiles(resDir, false));
            }
        }
//        LOG.info("Used files in services");
//        for ( File file : usedFiles) {
//            LOG.info(file.getAbsolutePath());
//        }
        return usedFiles;
    }

    public List<File> findUsedServiceArtifacts(ServiceProjectModel suPrj, String serviceName) {
        List<File> usedFiles = new ArrayList<File>();

        File srcDir = suPrj.getSourceDir();
        List<File> usedSrcFiles = findFiles(srcDir, true);
        usedFiles.addAll(usedSrcFiles);

        File configDir = suPrj.getConfigDir();
        if (!configDir.getAbsolutePath().replace("\\", "/").endsWith(FujiProjectModel.DEF_CONFIG_DIR)) {
            usedFiles.addAll(findFiles(configDir, false));
        }

        File resDir = suPrj.getResourcesDir();
        if (!resDir.getAbsolutePath().replace("\\", "/").endsWith(FujiProjectModel.DEF_RESOURCES_DIR)) {
            usedFiles.addAll(findFiles(resDir, false));
            File metainfDir = new File(resDir, "META-INF");
            usedFiles.addAll(findFiles(metainfDir, false));
        }
        configDir = suPrj.getConfigDir(serviceName);
        resDir = suPrj.getResourcesDir(serviceName);
        usedFiles.addAll(findFiles(configDir, false));
        usedFiles.addAll(findFiles(resDir, false));

//        LOG.info("Used files in services");
//        for (File file : usedFiles) {
//            LOG.info(file.getAbsolutePath());
//        }
        return usedFiles;
    }

    public void addServiceUnitToAssemblyDescriptor(
            Jbi assemblyDescriptor,
            ServiceDescriptor serviceDescriptor,
            File serviceUnitJar,
            ServiceProjectModel suPrjModel,
            ApplicationProjectModel appPrjModel
            ) throws IOException {

        // update the service assembly jbi.xml
        ServiceUnit su = assemblyDescriptor.getServiceAssembly().addServiceUnit();
        su.setArtifactsZip(serviceUnitJar.getName());  //TODO. find out the service unit paths from app Prj model.
        String targetComponent = serviceDescriptor.getComponentName();
        su.setComponentName(targetComponent);
        String applicationId = appPrjModel.getProperty(ApplicationProjectModel.PROP_APPLICATION_ID);
        su.getIdentification().setName(applicationId + "-" + targetComponent);
    }

}
