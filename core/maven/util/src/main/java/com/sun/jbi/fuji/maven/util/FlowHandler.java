/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FlowHandler.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import com.sun.jbi.fuji.ifl.IFLModel;


/**
 * This class handles the processing of the flow data in ifl to generate the 
 * correponding configuration artifacts during generate and package phases.
 * 
 */
public class FlowHandler extends BaseHandler {


    public FlowHandler() {
        setArtifactFileName(FLOW_ARTIFACT_NAME);
        setXslFileName(FLOW_XSL_NAME);
    }
        

    /**
     * Generates the named configuration in a destination directory.
     * This method is called in GenerateMojo to generate named configuration
     * in the project source directory
     * @param destDir directory in which to create the config. it is
     * "$prj/src/main/"
     * @throws java.lang.Exception
     */
    public void generateNamedConfiguration (File destDir,
                                            IFLModel ifl,
                                            String flowType,
                                            List usedFiles) throws IOException 
    {
        setIFLModel(ifl);
        generateXslStyleSheet (destDir,flowType,usedFiles);
        generateNamedConfiguration (destDir,flowType,usedFiles);
    }   


    /**
     *
     * @param mainDir
     * @param destDir
     * @param ifl
     * @param flowType
     * @throws java.io.IOException
     */
    public void generateFlowJavaExtensions(File mainDir,
                                           File destDir,
                                           IFLModel ifl,
                                           String flowType,
                                           List usedFiles) throws IOException 
    {
        setIFLModel(ifl);
        generateFlowJavaExtensions(mainDir, destDir, flowType, usedFiles);
    }   
    
    /**
     * Generates the configuration content inlcuding the inline configuration
     * into the META-INF/flow/xxx/** for packaging
     * @param srcDir build directory ( e.g. target/classes ) where the named
     * configuration is available
     * @param metaInfDir target metainf directory which will be packaged in the 
     * application jar file.
     */
    public void generateConfigurationForPackaging (File srcDir,
                                                   File metaInfDir,
                                                   IFLModel ifl,
                                                   String flowType) throws IOException 
    {
        setIFLModel(ifl);
        generateConfigurationForPackaging (srcDir, metaInfDir, flowType);
    }

}
