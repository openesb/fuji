/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BaseHandler.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * This class handles the processing of the data in ifl to generate the 
 * correponding configuration artifacts during generate and package phases.
 * 
 * @author chikkala
 */
public abstract class BaseHandler {

    // Some constamts used by the Handlers
    protected static final String FLOW_ARTIFACT_NAME  = "flow.properties";
    protected static final String FLOW_DIR_NAME       = "flow";
    protected static final String FLOW_XSL_NAME       = "flow.xsl";

    // Define the Class variables
    protected List<String> artifactList_ = null;
    protected IFLModel IFL_              = null;
    protected String artifactFileName_   = "";
    protected String xslFileName_        = "";


    public BaseHandler() {
    }

    public void setIFLModel(IFLModel ifl) {
        this.IFL_ = ifl;
    }

    /**
     * Will set the artifact file name.  This is the destination file name.
     * @param fileName The file name of the artifact file
     */
    protected void setArtifactFileName (String fileName) {
        this.artifactFileName_ = fileName;
    }
    
    /**
     * Will set the XSL file name.  This is the destination file name.
     * @param fileName The file name of the artifact file
     */
    protected void setXslFileName (String fileName) {
        this.xslFileName_ = fileName;
    }

    /**
     * *** NOTE Will need to be updated once the constance file is defined ***
     * Return the configuration list based on the specified type
     * @param type The flow type (split, aggregate, filter, select)
     */
    protected Collection getConfigList (String flowType) {
        Collection configList = null;
        if (flowType.equals("split")) {
            configList = this.IFL_.getSplits().values();
        }
        else if (flowType.equals("aggregate")) {
            configList = this.IFL_.getAggregates().values();
        }
        else if (flowType.equals("filter")) {
            configList = this.IFL_.getFilters().values();
        }
        else if (flowType.equals("select")) {
            configList = this.IFL_.getSelects().values();
        }
        return configList;
    }


    /**
     * Will construct the properties file name based on the given type.
     * @param type The flow type (split, aggregate, filter, select)
     */
    protected List<String> getResourceFileNames (String flowType, String flowTypeType) {
        ArrayList resourceFileList = new ArrayList();
        resourceFileList.add ("eip/default.properties");
        resourceFileList.add ("eip/default-" + flowTypeType + ".properties");
        resourceFileList.add ("eip/" + flowType + "/default.properties");
        resourceFileList.add ("eip/" + flowType + "/default-" + flowTypeType + ".properties");
        return resourceFileList;
    }
    
    /**
     * Will construct the java extension file name based on the given type.
     * @param type The flow type (split, aggregate, filter, select)
     */
    protected String getJavaExtFileName (String flowType) {
        String javaExtFileName = "eip/" + flowType + "/" + Flow.fromString(flowType).getExtensionClassName() + ".java";
        return javaExtFileName;
    }
    
    /**
     * Will construct the java extension file name based on the given type.
     * @param type The flow type (split, aggregate, filter, select)
     */
    protected String getXSLFileName (String flowType) {
        String javaXslFileName = "eip/" + flowType + "/" + "flow.xsl";
        return javaXslFileName;
    }
    

    /**
     * Generates the named configuration in a destination directory.
     * This method is called in GenerateMojo to generate named configuration
     * in the project source directory
     * @param destDir directory in which to create the config. it is
     * @param flowType Used for destination dir ("split", "aggregate", filter or select"
     * "$prj/src/main/"
     * @throws java.lang.Exception
     */
    protected void generateNamedConfiguration (File destDir, String flowType, List usedFiles) throws IOException
    {
        Collection<Map<String, Object>> configList = getConfigList(flowType);

        for (Map<String, Object> configMap : configList) {

            final String nameValue    = (String)configMap.get(Flow.Keys.NAME.toString());
            final String flowTypeType = (String)configMap.get(Flow.Keys.TYPE.toString());
            final Map configValue     = (Map)configMap.get(Flow.Keys.CONFIG.toString());

            // For inline, we do not want to create a properties file.
            // Distrubute mojo will copy inline to a xxx$file.
            if (configValue != null) {
                continue;
            }

            // Create the configuration type directory (i.e. split, aggregrate)
            final File configTypeDir = new File(destDir, flowType);
            if (!configTypeDir.exists()) {
                configTypeDir.mkdirs();
            }

            // Create the configuration name directory
            final File configDir = new File(configTypeDir, nameValue);
            if (!configDir.exists()) {
                configDir.mkdirs();
            }

            // Create the file containing the properties and also add it to the used file list.
            final File destinationFile = new File(configDir, artifactFileName_);
            usedFiles.add(destinationFile);

            // Initialize the file content if it did not already exist
            if (!destinationFile.exists()) {

                // Create the flow.properties file by merging the default property file for
                // the flow type (aggregate, split, ...) with the default property file for
                // flow type type (java, set, ...).
                List defaultPropertiesList = getResourceFileNames(flowType,flowTypeType);
                ListIterator<String> listIt = defaultPropertiesList.listIterator();
                Properties sourceProperties = new Properties();
                while (listIt.hasNext())
                {
                    URL sourceURL = this.getClass().getResource(listIt.next());
                    if (sourceURL != null)
                    {
                        sourceProperties.load(sourceURL.openStream());
                    }
                }

                // Take the source properties and add in any original property values.
                try {
                    FileInputStream fis = new FileInputStream(destinationFile);
                    sourceProperties.load(fis);
                    fis.close();
                }
                catch (java.io.FileNotFoundException e) {
                }

                // -- Special Case Handling --

                // For java, we need to set the classname and packagename properties
                if ("java".equals(flowTypeType))
                {
                    String defClassName   = Flow.fromString(flowType).getExtensionClassName();
                    String defPackageName = flowType + "." + nameValue; //.toLowerCase();
                    sourceProperties.setProperty(Flow.JavaConfigKeys.CLASSNAME.toString(), defClassName);
                    sourceProperties.setProperty(Flow.JavaConfigKeys.PACKAGENAME.toString(), defPackageName);
                }
                
                if ("xsl".equals(flowTypeType)){
                String stylesheet = sourceProperties.getProperty("stylesheet");
                if (stylesheet == null ){
                    File genXslFile = new File(configDir, xslFileName_);
                    if (genXslFile.exists()){
                        sourceProperties.setProperty("stylesheet", xslFileName_);
                    }
                }
            }

                // Write out the properties file
                MavenUtils.writeFile (sourceProperties, destinationFile);
            }
        }
    }
    
    /**
     * 
     * @param destDir directory in which to create the config. it is
     * @param flowType Used for destination dir ("split", "aggregate", filter or select"
     * "$prj/src/main/"
     * @throws java.lang.Exception
     */
    protected void generateXslStyleSheet (File destDir, String flowType, List usedFiles) throws IOException
    {
        Collection<Map<String, Object>> configList = getConfigList(flowType);

        for (Map<String, Object> configMap : configList) {

            final String nameValue    = (String)configMap.get(Flow.Keys.NAME.toString());
            final String flowTypeType = (String)configMap.get(Flow.Keys.TYPE.toString());
            final Map configValue     = (Map)configMap.get(Flow.Keys.CONFIG.toString());
            boolean inline = false;
            String stylesheet = null;
            
            // For inline, we do not want to create a properties file.
            // Distrubute mojo will copy inline to a xxx$file.
            if (configValue != null) {
                inline = true;
                stylesheet = (String)configValue.get("stylesheet");
            }
            
            if ( (!inline || stylesheet == null) &&  "xsl".equals(flowTypeType) ) {

                // Create the configuration type directory (i.e. split, aggregrate)
                final File configTypeDir = new File(destDir, flowType);
                if (!configTypeDir.exists()) {
                    configTypeDir.mkdirs();
                }

                // Create the configuration name directory
                final File configDir = new File(configTypeDir, nameValue);
                if (!configDir.exists()) {
                    configDir.mkdirs();
                }

                // Create the file containing the properties and also add it to the used file list.
                final File destinationFile = new File(configDir, xslFileName_);
                usedFiles.add(destinationFile);



                // Initialize the file content if it did not already exist
                if (!destinationFile.exists()) {

                    URL sourceURL = this.getClass().getResource(getXSLFileName(flowType));
                    MavenUtils.writeFile (sourceURL, destinationFile);
                }
            }
        }
    }

    
    /**
     * 
     * 
     * @param destDir
     * @param flowType
     * @throws java.io.IOException
     */
    protected void generateFlowJavaExtensions(File mainDir, File destDir, String flowType, List usedFiles) throws IOException
    {
 
        if (!"".equals(Flow.fromString(flowType).getExtensionClassName()) ) {

            Collection<Map<String, Object>> configList = getConfigList(flowType);

            for (Map<String, Object> configMap : configList) {
                
                if ( "java".equals(configMap.get(Flow.Keys.TYPE.toString())) ){
                    
                    String nameValue   = (String)configMap.get(Flow.Keys.NAME.toString());
                    Map configValue = (Map)configMap.get(Flow.Keys.CONFIG.toString());
                    
                    // The default classname, packagename
                    String className   = Flow.fromString(flowType).getExtensionClassName();
                    String packageName = flowType + "." + nameValue; // .toLowerCase();


                    /** 
                     * If the configuration is external, then defaults are
                     *   classname = Aggregator, package name = aggregate.[configName]
                     * If configuration is inline, then 
                     *    if a classname and package exists in the config use that, 
                     *           create only if it doesn't exist
                     *    if not then the the classname = Aggregator, package name = aggregate.[configName]
                     */
                    if ( configValue != null )
                    {
                        // Inline
                        java.util.Properties config = updateFlowConfiguration(flowType, 
                                nameValue, (String)configMap.get(Flow.Keys.TYPE.toString()), configValue);
                        className   = config.getProperty(Flow.JavaConfigKeys.CLASSNAME.toString());
                        packageName = config.getProperty(Flow.JavaConfigKeys.PACKAGENAME.toString());
                    } else {
                        // Load the className and packageName from the external
                        // configuration.
                        final File eConfigFile = new File(
                                mainDir,
                                flowType + "/" + nameValue + "/" + artifactFileName_);

                        // Add the file to the used file list
                        usedFiles.add(eConfigFile);

                        final Properties eProperties = new Properties();
                        FileInputStream fis = null;
                        try {
                            fis = new FileInputStream(eConfigFile);
                            eProperties.load(fis);

                            if (eProperties.containsKey(
                                    Flow.JavaConfigKeys.CLASSNAME.toString())) {
                                
                                className = eProperties.getProperty(
                                        Flow.JavaConfigKeys.CLASSNAME.toString());
                            }

                            if (eProperties.containsKey(
                                    Flow.JavaConfigKeys.PACKAGENAME.toString())) {

                                packageName = eProperties.getProperty(
                                        Flow.JavaConfigKeys.PACKAGENAME.toString());
                            }
                        } catch (IOException e) {
                            Logger.getLogger(getClass().getName()).severe(
                                    e.getMessage());
                        } finally {
                            if (fis != null) {
                                try {
                                    fis.close();
                                } catch (IOException ex) {
                                    Logger.getLogger(getClass().getName()).warning(
                                            ex.getMessage());
                                }
                            }
                        }
                    }

                    // Create the type extension directory based on package name, 
                    //    replace all non word !(a-zA-Z0-9) characters with a _ except the .
                    packageName = packageName.replaceAll("[\\W&&[^\\x2E]]", "_");
                    String packageDirs = packageName.replaceAll("\\x2E", "/");


                    File configTypeDir = new File(destDir, packageDirs);
                    if (!configTypeDir.exists()) {
                        configTypeDir.mkdirs();
                    }

                    // Create the extension file, only if one does not exist and also add it to the usedFiles list
                    File destinationFile = new File(configTypeDir, className + ".java");
                    usedFiles.add(destinationFile);

                    if (!destinationFile.exists()) {
                        // Create the POJO
                        URL sourceURL = this.getClass().getResource(getJavaExtFileName(flowType));
                        MavenUtils.writeFile (sourceURL, destinationFile);
                        
                        // Replace the package and classname in the generated configuration file
                        java.util.Properties props = new java.util.Properties();
                        props.setProperty(Flow.JavaConfigKeys.PACKAGENAME.toString(),packageName );
                        props.setProperty(Flow.JavaConfigKeys.CLASSNAME.toString() ,className );
                        MavenUtils.replaceTokensInFile(destinationFile, props);
                    }
                }
            }  
        }
    }

    /**
     * Generates the configuration content inlcuding the inline configuration
     * into the META-INF/flow/xxx/** for packaging
     * @param srcDir build directory ( e.g. target/classes ) where the named
     * configuration is available
     * @param metaInfDir target metainf directory which will be packaged in the 
     * application jar file.
     * @param flowType Used for destination dir ("split", "aggregate", filter or select"
     */
    protected void generateConfigurationForPackaging (File srcDir,
                                                      File metaInfDir,
                                                      String flowType) throws IOException 
    {
        Collection<Map<String, Object>> configList = getConfigList(flowType);

        if (configList == null || configList.size() == 0) {
            return; // no flow configurations.
        }

        // 1. create flow directory
        File flowDir = new File(metaInfDir, FLOW_DIR_NAME);
        if (!flowDir.exists()) {
            flowDir.mkdirs();
        }

        // 2. copy the configuration vales from the target/classes
        File artifactSrcDir  = new File(srcDir, flowType);
        File artifactDestDir = new File(flowDir, flowType);
        if (artifactSrcDir.exists()) {
            // Skip compiled sources
            java.io.FileFilter filter = new java.io.FileFilter() {

                public boolean accept(final File file) {
                    if ( file.getName().endsWith(".class") || 
                         file.getName().endsWith(".java")){
                        return false;
                    }
                    return true;
                }
            };
            MavenUtils.copy(artifactSrcDir, artifactDestDir, filter);             
        }

        // 3. generate inline configurations   
        for (Map<String, Object> configMap : configList) {
            Map inlineData = (Map)configMap.get(Flow.Keys.CONFIG.toString());
            String nameValue  = (String)configMap.get(Flow.Keys.NAME.toString());

            if (inlineData == null) {
                continue;
            }

            // Update the configuration if needed
            java.util.Properties config = updateFlowConfiguration(flowType, 
                    nameValue, (String)configMap.get(Flow.Keys.TYPE.toString()), inlineData);
            
            File configDir = new File(artifactDestDir, nameValue);
            if (!configDir.exists()) {
                configDir.mkdirs();
            }
            
            if ("xsl".equals((String)configMap.get(Flow.Keys.TYPE.toString()))){
                String stylesheet = config.getProperty("stylesheet");
                if (stylesheet == null ){
                    File genXslFile = new File(configDir, xslFileName_);
                    if (genXslFile.exists()){
                        config.setProperty("stylesheet", xslFileName_);
                    }
                }
            }
            
            // write inline data to file
            File configFile = new File(configDir, artifactFileName_);
            MavenUtils.writeFile(config, configFile);
        } 
        
        // 4. Cleanup: delete any empty folders under the flow type folder
        MavenUtils.deleteEmptyDirectory(artifactDestDir);
    }
    
    /**
     * Update the configuration with the right package/class name 
     * and return Properties
     */
     private static java.util.Properties updateFlowConfiguration(String flowType, 
             String flowName, String type, Map configValue){
        Properties p = new Properties();
        p.putAll(configValue);
        return updateFlowConfiguration(flowType, flowName, type, p);
     }
     
     /**
      * Update the configuration with the right package/class name 
      * and return Properties.
      */
      private static java.util.Properties updateFlowConfiguration(String flowType, 
             String flowName, String type, java.util.Properties config){
          
        if ( "java".equals(type) ){
            String defClassName   = Flow.fromString(flowType).getExtensionClassName();
            String defPackageName = flowType + "." + flowName; //.toLowerCase();

            if ( !config.containsKey(Flow.JavaConfigKeys.CLASSNAME.toString())){
                config.setProperty(Flow.JavaConfigKeys.CLASSNAME.toString(), defClassName);
            }

            if ( !config.containsKey(Flow.JavaConfigKeys.PACKAGENAME.toString())){
                defPackageName = defPackageName.replaceAll("[\\W&&[^\\x2E]]", "_");
                config.setProperty(Flow.JavaConfigKeys.PACKAGENAME.toString(), defPackageName);
            }
        }
        return config;
     }

}
