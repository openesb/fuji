/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.util;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.maven.spi.ServiceToolsLocator;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.xml.namespace.QName;
import org.glassfish.openesb.tools.common.jbi.descriptor.Connection;
import org.glassfish.openesb.tools.common.jbi.descriptor.Identification;
import org.glassfish.openesb.tools.common.jbi.descriptor.Jbi;
import org.glassfish.openesb.tools.common.service.ServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.ServiceProjectModel;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 * @author Kirill Sorokin
 */
public final class ApplicationUtils {

    private static Logger logger = Logger.getLogger(ApplicationUtils.class.getName());

    private static final String INTERCEPTOR_DIR_NAME         = "interceptor";
    private static final String SPLIT_DIR_NAME               = "split";
    private static final String SELECT_DIR_NAME              = "select";
    private static final String AGGREGATE_DIR_NAME           = "aggregate";
    private static final String FILTER_DIR_NAME              = "filter";
    private static final String INTERCEPTOR_ANNOTATION_NAME  = "Intercept";


    // Generation ////////////////////////////////////////////////////////////////////////
    public static void generateProject(
            final File baseDirectory,
            final File projectArchetype,
            final String groupId,
            final String artifactId,
            final String version,
            final String name) throws IOException {

        FileInputStream input = null;
        try {
            input = new FileInputStream(projectArchetype);
            generateProject(baseDirectory, input, groupId, artifactId, version, name);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public static void generateProject(
            final File baseDirectory,
            final InputStream projectArchetypeStream,
            final String groupId,
            final String artifactId,
            final String version,
            final String name) throws IOException {

        ZipInputStream zipInput = null;

        try {
            zipInput = new ZipInputStream(projectArchetypeStream);

            ZipEntry entry = null;
            while ((entry = zipInput.getNextEntry()) != null) {
                // We can't extract the archetype as is because it has a funky
                // structure.  This code massages the directory naming to match
                // the current app layout.
                if (entry.getName().startsWith("archetype-resources/") &&
                        !entry.isDirectory()) {

                    // need to mangle the name a bit - this is so hacky
                    String entryName = entry.getName();
                    entryName = entryName.replace("archetype-resources/", "");

                    final File file = new File(baseDirectory, entryName);

                    // Do not overwrite existing artifacts
                    if (file.exists()) {
                        continue;
                    }

                    // New generation, need to create parent dir
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }

                    MavenUtils.writeFile(zipInput, file, entry.getSize());
                }
            }

            final Properties props = new Properties();
            props.setProperty("groupId", groupId);
            props.setProperty("artifactId", artifactId);
            props.setProperty("version", version);
            props.setProperty("name", name);

            MavenUtils.replaceTokensInDirectory(baseDirectory, props, MavenUtils.DIRS_TO_SKIP);
        } finally {
            try {
                if (zipInput != null) {
                    zipInput.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }


    public static void generateApplication(
            final ServiceToolsLocator locator,
            final IFLModel ifl,
            final File baseDirectory,
            final Properties props,
            final List usedFiles) throws IOException {

        generateServiceUnits (locator, ifl, baseDirectory, props, usedFiles);
        generateFlowConstructs (ifl, baseDirectory, usedFiles);
        generateFlowTypeExtensions (ifl, baseDirectory, usedFiles);
    }


    /** 
     * Private method the will return all the existing files that exist under the
     * given (f) file path excluding any files existing under any path specified
     * in the ignoreFileSet.
     * 
     * @param f - File or diretory path
     * @param currentFileSet - The set that will hold the files and directory (input & output)
     * @param ignoreFileSet - The set of directories to ignore
     * @param add - The flag the controls if we should save the file in the currentFileSet.
     */
    private static void constructCurrentFileSet(File f, 
                                                Set currentFileSet, 
                                                Set ignoreFileSet, 
                                                Set doNotTouchDirs,
                                                boolean add) {
        if (!(ignoreFileSet.contains(f))) {
            if (add) {
                currentFileSet.add(f);
            }
            if (f.isDirectory()) {
                String[] children = f.list();
                for (int i=0; i<children.length; i++) {
                    File childFile = new File(f, children[i]);
                    constructCurrentFileSet (childFile,
                                             currentFileSet,
                                             ignoreFileSet,
                                             doNotTouchDirs,
                                             true);
                }
            }
        }
    }


    private static void constructCurrentFileMap (File f, 
                                                 File b, 
                                                 TreeMap currentFileMap, 
                                                 Set ignoreFileSet, 
                                                 Set doNotTouchDirs,
                                                 boolean add) {
        if (!(doNotTouchDirs.contains(f.getName())))
        {
            if (!(ignoreFileSet.contains(f))) {
                if (add) {
                    currentFileMap.put(f,b);
                }
                if (f.isDirectory()) {
                    String[] children = f.list();
                    for (int i=0; i<children.length; i++) {
                        File childFile = new File(f, children[i]);
                        File backFile = new File(b, children[i]);
                        constructCurrentFileMap (childFile,
                                                 backFile,
                                                 currentFileMap,
                                                 ignoreFileSet,
                                                 doNotTouchDirs,
                                                 true);
                    }
                }
            }
        }
    }


    /**
     * Will create the Interceptor Configuration.  This method will scan through
     * the source code, looking for the Intercept annotations.
     * @param ifl the IFL model
     * @param baseDiretory The base directory (i.e. target)
     * @param usedFiles contains the files that are being used (input/output)
     * @throws IOException
     */
    public static void generateInterceptorConfiguration(
            final ServiceToolsLocator locator,
            final File baseDirectory,
            final List usedFiles) throws IOException {
        
        generateInterceptorConfigurationFromSource (baseDirectory,usedFiles);
    }

    
    /**
     * Will create the Service Configurations.
     * @param ifl the IFL model
     * @param baseDiretory The base directory (i.e. target)
     * @param usedFiles contains the files that are being used (input/output)
     * @throws IOException
     */
    public static void generateServiceUnits(
            final ServiceToolsLocator locator,
            final IFLModel ifl,
            final File baseDirectory,
            final Properties props,
            final List usedFiles) throws IOException {

        AppBuildManager appBuilder = new AppBuildManager(baseDirectory, props);

        for (String type : ifl.getServiceTypes()) {
            logger.fine("Generating service artifacts for " + type);
            ServiceProjectModel suPrj = appBuilder.getServiceProjectModel(type);
            ServiceGenerator sg = locator.getServiceGenerator(type);
            sg.generateServiceUnit(suPrj, ifl);
            // sg.deleteUnusedArtifacts(suPrj, ifl);
        }
        usedFiles.addAll(appBuilder.findUsedServiceArtifacts(ifl));
    }
    /**
     * generate a service artifacts for a service by first reading the
     * already saved configuration parameters (.../service.properties) as
     * input properties for generating remaining artifacts
     */
    public static void generateServiceArtifacts(
            ServiceToolsLocator locator,
            AppBuildManager appBuilder,
            IFLModel ifl,
            List usedFiles,
            String serviceType,
            String serviceName) throws IOException {

        // read already generated service configuration
        ServiceProjectModel prjModel = appBuilder.getServiceProjectModel(serviceType);
        File configFile = new File(prjModel.getConfigDir(serviceName), ServiceConfigurationHelper.DEF_SERVICE_CONFIG_FILE);
        Properties configProps = new Properties();
        if (configFile.exists()) {
            logger.fine("Configuration existing...Reading config file... " + configFile.getAbsolutePath());
            configProps = org.glassfish.openesb.tools.common.Utils.readProperties(configFile);
        }
        // generate the artifacts
        ServiceProjectModel svcPrjModel = appBuilder.getServiceProjectModel(serviceType);
        ServiceGenerator serviceGen = locator.getServiceGenerator(serviceType);
        logger.fine("#### Generating service artifacts with generator " + serviceGen.getClass().getName());
        serviceGen.generateServiceArtifacts(svcPrjModel, ifl, serviceName, configProps);
        usedFiles.addAll(appBuilder.findUsedServiceArtifacts(svcPrjModel, serviceName));
    }
    
    /** 
     * Generate the flow constructs
     * 
     * @param ifl
     * @param baseDirectory
     * @param usedFiles contains the files that are being used (input/output)
     * @throws java.io.IOException
     */
    public static void generateFlowConstructs(
            final IFLModel ifl,
            final File baseDirectory,
            final List usedFiles) throws IOException {

        final File destDir = new File(baseDirectory, "src/main");
        final FlowHandler flowHandler = new FlowHandler();

        logger.fine("Creating configuration file for split(s).");
        flowHandler.generateNamedConfiguration(destDir,ifl, SPLIT_DIR_NAME, usedFiles);

        logger.fine("Creating configuration file for aggregate(s).");
        flowHandler.generateNamedConfiguration(destDir,ifl, AGGREGATE_DIR_NAME, usedFiles);

        logger.fine("Creating configuration file for filter(s).");
        flowHandler.generateNamedConfiguration(destDir,ifl, FILTER_DIR_NAME, usedFiles);

        logger.fine("Creating configuration file for SELECT(s).");
        flowHandler.generateNamedConfiguration(destDir,ifl, SELECT_DIR_NAME, usedFiles);

    }
    

    /** 
     * Generate type extensions for java types
     * 
     * @param ifl
     * @param baseDirectory
     * @param usedFiles contains the files that are being used (input/output)
     * @throws java.io.IOException
     */
    public static void generateFlowTypeExtensions(
            final IFLModel ifl,
            final File baseDirectory,
            final List usedFiles) throws IOException {

        final File mainDir = new File(baseDirectory, "src/main");
        final File destDir = new File(baseDirectory, "src/main/java");
        final FlowHandler flowHandler = new FlowHandler();

        flowHandler.generateFlowJavaExtensions(mainDir, destDir, ifl, AGGREGATE_DIR_NAME, usedFiles);
        flowHandler.generateFlowJavaExtensions(mainDir, destDir, ifl, SPLIT_DIR_NAME, usedFiles);
    }


    /** 
     * Will remove the unsed artifacts (java files, properties files ....)
     * 
     * @param usedFiles contains the files that are being used (input/output)
     * @param baseDirectory
     */
    public static void moveUnusedArtifacts (final List usedFiles, final File baseDirectory)
    {
        final Set usedFileSet = new HashSet();
        final Set currentFileSet = new HashSet();
        final TreeMap currentFileMap = new TreeMap();
        final List unusedList = new ArrayList();
        File mainDir   = new File(baseDirectory, "src/main");
        File backupDir = new File(baseDirectory, "bak/main");

        // Construct the "usedFileSet" that will hold all the files and their parents
        // directories that are currently being used in the application.
        Iterator it = usedFiles.iterator();     
        while (it.hasNext()) {
            File f = (File)it.next();
            while (!(f.equals(mainDir))) {
                usedFileSet.add(f);
                f = f.getParentFile();
            }
        }

        // Create the "Do Not Touch Directory" Set
        Set doNotTouchDirs = new HashSet();
        doNotTouchDirs.add(".svn");
        doNotTouchDirs.add("CVS");

        // Create the currentFileSet which will contain all the files and directories 
        // under the mainDir excluding any that exist in the ignoreFileSet.
        Set ignoreFileSet = new HashSet();
        ignoreFileSet.add (new File(baseDirectory, "src/main/resources/java"));
        ignoreFileSet.add (new File(baseDirectory, "src/main/resources/META-INF"));
        ignoreFileSet.add (new File(baseDirectory, "src/main/resources/wsdl"));
        ignoreFileSet.add (new File(baseDirectory, "src/main/ifl"));
        ignoreFileSet.add (new File(baseDirectory, "src/main/java"));
        constructCurrentFileMap (mainDir,
                                 backupDir,
                                 currentFileMap,
                                 ignoreFileSet,
                                 doNotTouchDirs,
                                 false);

        // Add any special directoies (Directories that we want to include that reside
        // under a directory that we ignored).
        mainDir   = new File(baseDirectory, "src/main/java/aggregate");
        backupDir = new File(baseDirectory, "bak/main/java/aggregate");
        ignoreFileSet = new HashSet();
        constructCurrentFileMap (mainDir,
                                 backupDir,
                                 currentFileMap,
                                 ignoreFileSet,
                                 doNotTouchDirs,
                                 true);


        // Create the list of files and possible directories that are no longer being used.
        Set keySet = currentFileMap.keySet();
        it = keySet.iterator();     
        while (it.hasNext()) {
            File f = (File)it.next();
            if (!(usedFileSet.contains(f))) {
                unusedList.add(f);
            }
        }

        // First we need to move the unused files to their backup directories.  We want to sort
        // the list first so we will create any directories before moving the file to it.  Note,
        // if the destination directory already contains a file with the destination file name,
        // a version number will be appended to the end of the file name.
        Collections.sort(unusedList);
        it = unusedList.iterator(); 
        while (it.hasNext()) {
            File unusedFile = (File)it.next();
            File destinationFile = (File)currentFileMap.get(unusedFile);

            // If it's a file we're dealing with, make sure the parent directories are created
            if (unusedFile.isFile())
            {
                File destinationDir = destinationFile.getParentFile();
                if (destinationDir != null)
                {
                    boolean success = destinationDir.mkdirs();
                }
                String initialFileName = destinationFile.getAbsolutePath();
                int version = 1;
                while (destinationFile.exists()) {
                    String filename = initialFileName + "." + version;
                    destinationFile = new File(filename);
                    version++;
                }
                boolean success = unusedFile.renameTo(destinationFile);
                if (!success)
                {
                    logger.fine("Was not able to rename the file: " + unusedFile + " to: " + destinationFile);
                }
            }

            // We are dealing with a directory, so create the destination directories
            else
            {
                if (!destinationFile.exists())
                {
                    boolean success = destinationFile.mkdirs();
                    if (!success)
                    {
                        logger.fine("Was not able to make the directory: " + destinationFile);
                    }
                }
            }
        }

        // Now remove the unsed files and directories.  
        deleteUnusedFilesAndDirs (unusedList, doNotTouchDirs);
    }




    /** 
     * Will remove the unsed artifacts (java files, properties files ....)
     * 
     * @param usedFiles contains the files that are being used (input/output)
     * @param baseDirectory
     */
    private static void deleteUnusedFilesAndDirs (final List unusedList, final Set doNotTouchDirs)
    {

        // To remove the unsed files and directories, we first want the reverse the list,
        // so we will first delete the files before deleting any unused directories.
        Collections.reverse(unusedList);
        Iterator it = unusedList.iterator();     
        while (it.hasNext()) {
            File unusedFile = (File)it.next();
            if (unusedFile.exists())
            {
                if (unusedFile.delete()) {
                    logger.fine("Deleted the unused file or directory: " + unusedFile);
                }

                // OK, the deletion failed, so we need to check and see if it was a directory
                // If it is, we will check an see if the ONLY file/dir that is in the directory
                // is in the doNotTouchDir set.  If so, then we can go ahead and delete the dir.
                else {
                    if (unusedFile.isDirectory()) {
                        File[] contents = unusedFile.listFiles();
                        boolean deleteDir = true;
                        if (contents != null) {
                            for (File file : contents) {
                                if (file.isDirectory()) {
                                    if (!(doNotTouchDirs.contains(file.getName()))) {
                                        deleteDir = false;
                                    }
                                }
                                else {
                                    deleteDir = false;
                                }
                            }
                        }
                        if (deleteDir) {
                            if (deleteDir(unusedFile)) {
                                logger.fine("Deleted the empty unused directory: " + unusedFile);
                            }

                        }
                    }
                }
            }
        }
    }


    /** 
     * Will delete all the files and subdirectories under "dir"
     * 
     * @param dir the directory to delete
     * @return true if all deletions were successful
     */
    private static boolean deleteDir (File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }


    /** 
     * Will remove the unsed artifacts (java files, properties files ....)
     * 
     * @param usedFiles contains the files that are being used (input/output)
     * @param baseDirectory
     */
    public static void removeTheUnusedArtifacts (final List usedFiles,
                                                 final File baseDirectory)
    {
        final Set ignoreFileSet = new HashSet();
        final Set usedFileSet = new HashSet();
        final Set currentFileSet = new HashSet();
        final File mainDir = new File(baseDirectory, "src/main");
        final List unusedList = new ArrayList();

        // Construct the "usedFileSet" that will hold all the files and their parents
        // directories that are currently being used in the application.
        Iterator it = usedFiles.iterator();     
        while (it.hasNext()) {
            File f = (File)it.next();
            while (!(f.equals(mainDir))) {
                usedFileSet.add(f);
                f = f.getParentFile();
            }
        }

        // Create the "Do Not Touch Directory" Set
        Set doNotTouchDirs = new HashSet();
        doNotTouchDirs.add(".svn");
        doNotTouchDirs.add("CVS");

        // Create the currentFileSet which will contain all the files and directories 
        // under the mainDir excluding any that exist in the ignoreFileSet.
        ignoreFileSet.add (new File(baseDirectory, "src/main/resources"));
        ignoreFileSet.add (new File(baseDirectory, "src/main/ifl"));
        constructCurrentFileSet (mainDir,currentFileSet,ignoreFileSet,doNotTouchDirs,false);

        // Create the list of files and possible directories that are no longer being used
        // Note, the list will be ordered so the files will be deleted before the directory.
        it = currentFileSet.iterator();     
        while (it.hasNext()) {
            File f = (File)it.next();
            if (!(usedFileSet.contains(f))) {
                unusedList.add(f);
            }
        }
        Collections.sort(unusedList);

        // Now remove the unsed files and directories.  
        deleteUnusedFilesAndDirs (unusedList, doNotTouchDirs);
    }


    /**
     * Will create the Interceptor Configuration.  This method will scan through
     * the source code, looking for the Intercept annotations.
     * @param ifl the IFL model
     * @param baseDiretory The base directory (i.e. target)
     * @param usedFiles contains the files that are being used (input/output)
     * @throws IOException
     */
    public static void generateInterceptorConfigurationFromSource (
        final File baseDirectory,
        final List usedFiles) throws IOException
    {
        final String startDir = baseDirectory + "/src/main";
        GeneratePackageNames packageNames = new GeneratePackageNames();
        Collection<PackageNameInfo> interceptList = 
            packageNames.getAnnotationList(startDir,INTERCEPTOR_ANNOTATION_NAME);

        // Initialize the usedFiles list with the interceptor files.
        Iterator it = interceptList.iterator();     
        while (it.hasNext()) {
            PackageNameInfo info = (PackageNameInfo)it.next();
            String fullPathName = info.getFullPathName();
            usedFiles.add(new File(fullPathName));
        }

        logger.fine("Creating configuration file for interceptor(s).");
        final AnnotationHandler annotationHandler = new AnnotationHandler();
        annotationHandler.generateNamedConfiguration(interceptList,
                                                     startDir,
                                                     INTERCEPTOR_DIR_NAME,
                                                     usedFiles);
    }    

    /**
     * Will tag aspects properties files are as used files.
     * Makes sure that aspects artifacts will always be packaged.
     * @param ifl the IFL model
     * @param baseDiretory The base directory (i.e. target)
     * @param usedFiles contains the files that are being used (input/output)
     * @throws IOException
     */
    public static void generateAspectsProeprtiesFromAspectsDirectory(
            final File baseDirectory,
            final List usedFiles) throws IOException {
        final String startDir = baseDirectory + "/src/main/aspects";


        final List<File> aspectPropertyFiles = MavenUtils.findFiles(
                new File(startDir),
                new FileFilter() {

                    public boolean accept(final File file) {
                        return file.getName().endsWith(".properties");
                    }
                });

        usedFiles.add(new File(startDir));  //Retain aspects folder also.
        for (File aspectPropertyFile : aspectPropertyFiles) {
            usedFiles.add(aspectPropertyFile);
        }
        logger.fine("Aspects properties files are tagged as required files to be packaged");
    }
    
    // Packaging /////////////////////////////////////////////////////////////////////////
    /**
     * This method is used to package an integration application.
     * The application name is constructed as <applicationId>-<applicationVersion>.jar.
     * If the version is null, the application jar name will be <applicationId>.jar
     * @param locator
     * @param ifl
     * @param applicationId
     * @param applicationVersion
     * @param baseDirectory
     * @param outputDirectory
     * @return the path to the packaged application
     * @throws java.io.IOException
     */
    public static String packageApplication(
            final ServiceToolsLocator locator,
            final IFLModel ifl,
            final String applicationId,
            final String applicationVersion,
            final File baseDirectory,
            final File outputDirectory) throws IOException {
        Jbi jbi = null;
        Map<String, File> suMap = new HashMap();
        
        if ( ifl != null )
        {
            jbi = createJbi(applicationId);
            suMap = packageServiceUnits(
                    locator, jbi, ifl, applicationId, baseDirectory, outputDirectory);
            packageFlowConstructs(ifl, outputDirectory);
        }
        packageInterceptorConfiguration(outputDirectory);
        return 
        packageAssembly(locator, jbi, ifl, applicationId, applicationVersion, outputDirectory, suMap);
    }

    public static Jbi createJbi(
            final String applicationId) {

        // Create the SA jbi.xml - we need to update this as we process SUs
        final Jbi jbi = Jbi.newServiceAssemblyDescriptor();

        final Identification id = jbi.getServiceAssembly().getIdentification();
        id.setName(applicationId);

        return jbi;
    }

    public static Map<String, File> packageServiceUnits(
            final ServiceToolsLocator locator,
            final Jbi jbi,
            final IFLModel ifl,
            final String applicationId,
            final File baseDirectory,
            final File outputDirectory) throws IOException {

//        final Map<String, File> suMap = new HashMap<String, File>();

//        // Each service type becomes its own service unit
//        for (String serviceType : ifl.getServiceTypes()) {
//            final List<String> services = ifl.getServicesForType(serviceType);
//            final ServicePackager packager = locator.locateServicePackager(serviceType);
//
//            packager.prepareServiceUnitResources(
//                    ifl, services, applicationId, baseDirectory, outputDirectory);
//
//            final File suArchive = packager.packageServiceUnit(
//                    ifl, serviceType, services, applicationId, outputDirectory);
//
//            suMap.put(serviceType, suArchive);
//
//            packager.addServiceUnitToApplication(
//                    jbi, applicationId, outputDirectory, suMap);
//        }

        Map<String, File> suMap = new HashMap<String, File>();

        String version = "";
        AppBuildManager appBuilder = new AppBuildManager(baseDirectory, applicationId, version);

        // Each service type becomes its own service unit
        for (String serviceType : ifl.getServiceTypes()) {
            List<String> services = ifl.getServicesForType(serviceType);
            ServiceDescriptor sd = locator.getServiceDescriptor(serviceType);
            ServiceBuilder builder = locator.getServiceBuilder(serviceType);
            ServicePackager packager = locator.getServicePackager(serviceType);
            ServiceProjectModel suPrj = appBuilder.getServiceProjectModel(serviceType);

            builder.processResources(suPrj,services,ifl);

            File suArchive = packager.packageServiceUnit(suPrj,services, ifl);
            logger.fine("Service unit archive created " + suArchive.getAbsolutePath());
            suMap.put(serviceType, suArchive);

            appBuilder.addServiceUnitToAssemblyDescriptor(
                    jbi, sd, suArchive,
                    appBuilder.getServiceProjectModel(serviceType), appBuilder.getAppProjectModel());
        }
        
        return suMap;
    }

    /**
     * Will package up the Flow configurations 
     * @param ifl the IFL model
     * @param outputDiretory The output directory (i.e. target)
     * @throws IOException
     */
    public static void packageFlowConstructs(
            final IFLModel ifl,
            final File outputDirectory) throws IOException {

        final File classesDir = new File(outputDirectory, "classes");
        final File metaInfDir = new File(classesDir, "META-INF");

        final FlowHandler flowHandler = new FlowHandler();

        logger.fine("Generating the split(s) configurations for Packaging...");
        flowHandler.generateConfigurationForPackaging(classesDir,metaInfDir,ifl, SPLIT_DIR_NAME);

        logger.fine("Generating the aggregate(s) configurations for Packaging...");
        flowHandler.generateConfigurationForPackaging(classesDir,metaInfDir,ifl, AGGREGATE_DIR_NAME);

        logger.fine("Generating the filter(s) configurations for Packaging...");
        flowHandler.generateConfigurationForPackaging(classesDir,metaInfDir,ifl, FILTER_DIR_NAME);

        logger.fine("Generating the SELECT(s) configurations for Packaging...");
        flowHandler.generateConfigurationForPackaging(classesDir,metaInfDir,ifl, SELECT_DIR_NAME);

    }

    /**
     * This method is used to package an integration application.
     * The application name is constructed as <applicationId>-<applicationVersion>.jar.
     * If the version is null, the application jar name will be <applicationId>.jar
     * @return the path to the packaged application
     * @throws java.io.IOException
     */
    public static String packageAssembly(
            final ServiceToolsLocator locator,
            final Jbi jbi,
            final IFLModel ifl,
            final String applicationId,
            final String applicationVersion,
            final File outputDirectory,
            final Map<String, File> suMap) throws IOException {

        File baseDir = outputDirectory.getParentFile();
        AppBuildManager appBuilder = new AppBuildManager(baseDir, applicationId, applicationVersion);

        final File appMetaInf = new File(outputDirectory, "classes/META-INF/");
        if ( ifl != null )
        {
            // We need a list of consumed services which do not have connections
            final List<String> unconnectedServices = new ArrayList<String>();
            for (String service : ifl.getConsumedServices()) {
                unconnectedServices.add(service);
            }

            // add consumer->provider connections
            for (Map.Entry<String, Set<String>> entry : ifl.getConnections()) {
                final String consumerService = entry.getKey();
                final String consumerType = ifl.getTypeForService(consumerService);
                for (String providerService : entry.getValue()) {
                    final Connection connection = jbi.getServiceAssembly().addConnection();
                    final String providerType = ifl.getTypeForService(providerService);

                    connection.getConsumer().setServiceName(
                            getServiceQName(ifl.getNamespace(consumerService),
                               (consumerService.equals(providerService) ?
                                    (consumerType + "_" + consumerService) :
                                    (consumerService + "_" + providerService))));
                     connection.getConsumer().setEndpointName(
                           getEndpointName(providerService));
                    connection.getProvider().setServiceName(
                            getServiceQName(ifl.getNamespace(providerService), providerService));
                    connection.getProvider().setEndpointName(
                            getEndpointName(providerService));

                    // we have a connection for this consumer service, so remove it
                    // from the list of unconnected services
                    unconnectedServices.remove(consumerService);
                }
            }

            /* check for broadcast routes and unconnected consumer services
            for (String service : unconnectedServices) {
                final Connection connection = jbi.getServiceAssembly().addConnection();

                connection.getConsumer().setServiceName(
                        getServiceQName(applicationId, service));
                connection.getConsumer().setEndpointName(
                        getEndpointName(service));

                // check to see if this is a broadcast route
                if (ifl.getBroadcastsForService(service).size() > 0) {
                    connection.getProvider().setServiceName(
                            getServiceQName(applicationId, service));
                    connection.getProvider().setEndpointName(
                            getBroadcastEndpointName(service));
                } else {
                    // not much we can do here right now
                }
            }
             */

            // Persist jbi.xml
            
            FileOutputStream output = null;

            try {
                output = new FileOutputStream(new File(appMetaInf, "jbi.xml"));
                jbi.writeTo(output);
            } catch (IOException e) {
                throw e;
            } catch (Exception e) {
                throw (IOException) new IOException().initCause(e);
            } finally {
                try {
                    if (output != null) {
                        output.close();
                    }
                } catch (IOException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }

////            // Call each service type to see if pre-processing of the app package
////            // is required
////            for (String serviceType : ifl.getServiceTypes()) {
////                // App Packaging Pre-Process:
////                // Give each service a chance to process the application bits before
////                // they are packaged into a service assembly
////                final ServicePackager packager = locator.locateServicePackager(serviceType);
////
////                if (packager != null) {
////                    packager.beforeApplicationPackaging(
////                            new File(outputDirectory, "classes"),
////                            ifl.getServicesForType(serviceType));
////                }
////            }

            // Call each service type to see if pre-processing of the app package
            // is required
            for (String serviceType : ifl.getServiceTypes()) {
                // App Packaging Pre-Process:
                // Give each service a chance to process the application bits before
                // they are packaged into a service assembly
                ServicePackager packager = locator.getServicePackager(serviceType);
                ServiceProjectModel suPrj = appBuilder.getServiceProjectModel(serviceType);
                if (packager != null) {
                    packager.beforeApplicationPackaging(suPrj,
                            ifl.getServicesForType(serviceType),
                            appBuilder.getAppProjectModel());
                }
            }

        }

        // package the app
        ZipOutputStream zipOutput = null;
        
        String versionSuffix = "-" + applicationVersion;
        if (applicationVersion == null || applicationVersion.trim().equals("")) {
            versionSuffix = "";
        }
        
        /*ugly hack - see open-esb issue 1396. 
         Caused by netbeans issue 135043 - Badly formed maven project */
         
        //File applicationJar = new File(outputDirectory, applicationId + versionSuffix + ".jar");
        File applicationJar = new File(outputDirectory, "app.jar");
                
        try {
                        
            zipOutput = new ZipOutputStream(
                    new FileOutputStream(applicationJar));

            MavenUtils.addToArchive(appMetaInf, "META-INF/", zipOutput);            
            
            for (File archive : suMap.values()) {
                MavenUtils.addToArchive(archive, archive.getName(), zipOutput);
            }

            final List<File> classFiles = MavenUtils.findFiles(
                    new File(outputDirectory, "classes"),
                    new FileFilter() {

                        public boolean accept(final File file) {
                            return file.getName().endsWith(".class");
                        }
                    });

            for (File classFile : classFiles) {
                final String name =
                        classFile.toURI().toString().split("target/classes/")[1];

                MavenUtils.addToArchive(classFile, name, zipOutput);
            }
        } finally {
            try {
                if (zipOutput != null) {
                    zipOutput.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return applicationJar.getAbsolutePath();
    }

    /**
     * Will package the interceptor configuration
     * @param outputDir The outpue directory (i.e. target)
     * @throws IOException
     */
    public static void packageInterceptorConfiguration (final File outputDir) throws IOException 
    {
        final File classesDir = new File(outputDir, "classes");
        final File metaInfDir = new File(classesDir, "META-INF");
        final AnnotationHandler annotationHandler = new AnnotationHandler();
        logger.fine("Generating the interceptor(s) configurations for Packaging...");
        annotationHandler.generateConfigurationForPacakging(classesDir,
                                                            metaInfDir,
                                                            "interceptor");
    }
    
    // Miscellanea ///////////////////////////////////////////////////////////////////////
    public static QName getServiceQName(
            final String namespace,
            final String serviceName) {

        return new QName(namespace, serviceName);
    }

    public static QName getInterfaceQName(
            final String applicationId) {

        return new QName(
                FUJI_APPLICATION_URI + applicationId,
                applicationId + "_interface");
    }

    public static String getEndpointName(
            final String serviceName) {

        return serviceName + "_endpoint";
    }

    public static String getBroadcastEndpointName(
            final String serviceName) {

        return serviceName + "_broadcast";
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    /**
     * Finds the list of services for which there exists service artifacts 
     * in the project.
     * @return a map of type <ServiceType, <List of ServiceNames>>
     */
    private static Map<String, List<String>> findServicesInProject (final File baseDirectory) 
    {
        Map<String, List<String>> serviceMap = new HashMap<String, List<String>>();
        // list the service types and services from the configuration directory
        File configDir = new File(baseDirectory, APP_CONFIG_PATH);
        if (configDir.exists() && configDir.isDirectory()) {
            // find list of service type dirs
            File[] serviceTypeDirs = findServiceTypeDirectories(configDir);
            // get the service dirs for each service type dirs
            for (File serviceTypeDir : serviceTypeDirs) {
                String serviceType = serviceTypeDir.getName();
                // find list of service type dirs
                File[] serviceDirs = findServiceDirectories(serviceTypeDir);
                List<String> serviceList = new ArrayList<String>();
                for (File serviceDir : serviceDirs) {
                    String serviceName = serviceDir.getName();
                    serviceList.add(serviceName);
                }
                serviceMap.put(serviceType, serviceList);
            }
        } else {
            logger.fine("Configuration directory path \"" + configDir + "\" not exists or not a directory");
        }

        // list service types and services at known source level
        for (String serviceType : KNOWN_SRC_TYPES) {
            File serviceTypeDir = new File(baseDirectory, APP_SRC_PATH + serviceType);
            if (serviceTypeDir.exists() && serviceTypeDir.isDirectory()) {
                File[] serviceDirs = findServiceDirectories(serviceTypeDir);
                List<String> serviceList = new ArrayList<String>();
                for (File serviceDir : serviceDirs) {
                    serviceList.add(serviceDir.getName());
                }
                serviceMap.put(serviceType, serviceList);
            }
        }
        return serviceMap;
    }

    /**
     * finds the directories that represent the service type artifacts holders
     * in the project sources under "src/main/config/" directory
     * @param configDir full path to "src/main/config/"
     * @return list of directory file objects of the service types.
     * 
     */
    private static File[] findServiceTypeDirectories(
            final File configDir) {

        // list all directories that have names that are valid as service type names
        File[] serviceTypeDirs = configDir.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                String dirName = pathname.getName();
                return (isValidServiceType(dirName) &&
                        pathname.isDirectory());
            }
        });
        return (serviceTypeDirs == null) ? new File[0] : serviceTypeDirs;
    }

    /**
     * checks the serviceType value is valid or not. used in deleting service artifacts.
     * @param serviceType
     * @return true or false
     */
    private static boolean isValidServiceType(
            final String serviceType) {

        //TODO: what other validations we need have here?
        // like should not contain ? # @ etc.
        return !(serviceType.startsWith(".") || serviceType.endsWith("."));
    }

    /**
     * finds the directories that represents services in the service type 
     * directory folder. 
     * @param serviceTypeDir full path to service type directory 
     * basedir + "/src/main/config/" + servicetype 
     * @return list of directory file objects which hold the service artifacts
     */
    private static File[] findServiceDirectories(
            final File serviceTypeDir) {

        // list all directories that have names that are valid as service names
        File[] serviceDirs = serviceTypeDir.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                String dirName = pathname.getName();
                return (isValidServiceName(dirName) &&
                        pathname.isDirectory());
            }
        });

        return (serviceDirs == null) ? new File[0] : serviceDirs;
    }

    /**
     * checks the serviceName is valid or not. used in deleting service artifacts
     * @param serviceName
     * @return
     */
    private static boolean isValidServiceName(
            final String serviceName) {

        //TODO: what other validations we need to have here?
        // like should not contain ? # @ etc.
        return !(serviceName.startsWith(".") || serviceName.endsWith("."));
    }

    /**
     * removes the sevices being used in the IFL from the list of services 
     * exists in the project.
     * @param serviceMap sevice map that contains all the services that exists in
     * the project.
     */
    private static void filterUsedServices(
            final IFLModel ifl,
            final Map<String, List<String>> serviceMap) {

        for (String serviceType : ifl.getServiceTypes()) {
            final List<String> serviceList = serviceMap.get(serviceType);

            // TODO: This happens for XSLT (why? no configuration?)
            if (serviceList == null) {
                continue;
            }

            final List<String> currentList = ifl.getServicesForType(serviceType);
            for (String currentService : currentList) {
                serviceList.remove(currentService);
            }

            if (serviceList.size() == 0) {
                // remove the service type
                serviceMap.remove(serviceType);
            }
        }
    }

    /**
     * checks whether a given  direcotry that holds the service artifacts of a 
     * specific service type is considered empty. 
     * 
     * @param serviceTypeDir
     * @return
     */
    private static boolean isEmptyServiceTypeDir(
            final File serviceTypeDir) {

        String[] files = serviceTypeDir.list();
        if ((files == null) || (files.length == 0)) {
            return true;
        } else if ((files.length == 1) && files[0].equals("META-INF")) {
            return true;  // dir with only META-INF is considered empty
        } else {
            return false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private ApplicationUtils() {
        // Does nothing -- a placeholder
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    private static final String FUJI_APPLICATION_URI =
            "http://fuji.dev.java.net/application/"; // NOI18N
    private static final String APP_CONFIG_PATH =
            "src/main/config/"; // NOI18N
    private static final String APP_RESOURCE_PATH =
            "src/main/resources/"; // NOI18N
    private static final String APP_SRC_PATH =
            "src/main/"; // NOI18N
    /** 
     * These are the directory names under src/main that we treat as application
     * source directories.  This really should be dynamic (perhaps through
     * service plugins), or at the very least configurable in the application
     * project.
     */
    private static final String[] KNOWN_SRC_TYPES = new String[]{
        "xslt", // NOI18N
        "jruby", // NOI18N
    };
}
