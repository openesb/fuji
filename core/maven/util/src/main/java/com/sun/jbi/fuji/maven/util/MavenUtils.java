/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MojoUtils.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.glassfish.openesb.tools.common.templates.TemplateEngine;

/**
 * Grab bag of utility methods.
 *
 * @author kcbabo
 * @author ksorokin
 */
public final class MavenUtils {

    private static Logger logger = Logger.getLogger(MavenUtils.class.getName());
    private static Pattern TOKEN_PATTERN = Pattern.compile("\\$\\!\\{[\\w\\.]+\\}");
    public static final List DIRS_TO_SKIP = Arrays.asList(new String[]{
                ".svn"
            });

    /**
     * Replace all tokens in the specified file with the values specified 
     * in props.  The tokens in the file should be in the form '${'token'}'.
     * @param file replace tokens in this file
     * @param props with values in this file
     * @throws java.lang.Exception failed during R/W of file
     */
    public static void replaceTokensInFile(
            final File file,
            final Properties props) throws IOException {

        // total hack to read, replace, and write back

//        String content = readFile(file).toString();
//
//        for (Object name : props.keySet()) {
//            content = content.replace("${" + name + "}",
//                    props.getProperty((String) name));
//            content = content.replace("$!{" + name + "}",
//                    props.getProperty((String) name));
//        }
//        // further check if there are still
//        // unresolved 'quite reference' tokens - if yes, replace them with empty string
//        if ( file.getName().endsWith(".wsdl")) {
//            // restrict the scope - don't want to touch things like jruby code templates etc
//            content = TOKEN_PATTERN.matcher(content).replaceAll("");
//        }
//        writeFile(content, file);
        replaceTokensInFile(file, props, false);
    }

    public static void replaceTokensInFileOld(
            final File file,
            final Properties props,
            final boolean prepareResources) throws IOException {
        String content = readFile(file).toString();

        for (Object name : props.keySet()) {
            content = content.replace("${" + name + "}",
                    props.getProperty((String) name));
        }
        if (prepareResources && file.getName().endsWith(".wsdl")) {
            // restrict the scope - don't want to touch things like jruby code templates etc for now
            //
            // resolve tokens of quite references as in velocity template,
            // unresolved 'quite reference' tokens - if yes, replace them with empty string
            for (Object name : props.keySet()) {
                content = content.replace("$!{" + name + "}",
                        props.getProperty((String) name));
            }
            // enforce the "quite reference" semantics on those tokens
            content = TOKEN_PATTERN.matcher(content).replaceAll("");
        }
        writeFile(content, file);
    }

    public static void replaceTokensInFile(
            final File file,
            final Properties props,
            final boolean prepareResources) throws IOException {

        String content = readFile(file);

        if ((prepareResources && file.getName().endsWith(".wsdl")) || file.getName().endsWith(".properties")) {
            // restricting velocity template processing only for the .wsdl and .properties files

            // TODO: the comments in the old code inidicates that some issue
            // with jruby code templates to process as velocity templates. investigate
            // what is wrong with them.
            // See replaceTokensInFileOld() for the details of the comments.

            // restrict the scope - don't want to touch things like jruby code templates etc for now
            //
            logger.info("######################### USING VELOCITY ENGINE ######### ");
            try {
                StringReader template = new StringReader(content);
                StringWriter out = new StringWriter();
                TemplateEngine.getDefault().processTemplate(template, out, props);
                content = out.getBuffer().toString();
            } catch (Exception ex) {
                logger.log(Level.FINE, ex.getMessage(), ex);
                throw new IOException(ex.getMessage());
            }

        } else {
            //TODO: we really don't want to do this and use the velocity for all template processing.
            // use the old template processing
            for (Object name : props.keySet()) {
                content = content.replace("${" + name + "}",
                        props.getProperty((String) name));
            }
        }

        writeFile(content, file);
    }

    public static void replaceTokensInStream(
            final InputStream input,
            final File file,
            final Properties props) throws IOException {
        try {
            StringWriter out = new StringWriter();
            TemplateEngine.getDefault().processTemplate(input, out, props);
            writeFile(out.getBuffer().toString(), file);
        } catch (Exception ex) {
            logger.log(Level.FINE, ex.getMessage(), ex);
            throw new IOException(ex.getMessage());
        }
    }

    /**
     * Equivalent to replaceTokensInFile for an entire directory.
     * @param file replace tokens in this file
     * @param props with values in this file
     * @throws java.lang.Exception failed during R/W of file
     */
    public static void replaceTokensInDirectory(
            final File directory,
            final Properties props) throws IOException {

        if (!directory.exists() || !directory.isDirectory()) {
            return;
        }

        final File[] children = directory.listFiles();

        if ((children != null) && (children.length > 0)) {
            for (File child : children) {
                if (child.isDirectory()) {
                    replaceTokensInDirectory(child, props);
                } else {
                    replaceTokensInFile(child, props);
                }
            }
        }
    }

    public static void replaceTokensInDirectory(
            final File directory,
            final Properties props,
            final List skipDirs) throws IOException {

        replaceTokensInDirectory(directory, props, null, false);
//        if (!directory.exists() || !directory.isDirectory()) {
//            return;
//        }
//
//        final File[] children = directory.listFiles();
//
//        if ((children != null) && (children.length > 0)) {
//            for (File child : children) {
//                if (child.isDirectory()) {
//                    replaceTokensInDirectory(child, props);
//                } else {
//                    replaceTokensInFile(child, props);
//                }
//            }
//        }
    }

    /**
     * Equivalent to replaceTokensInFile for an entire directory.
     * @param file replace tokens in this file
     * @param props with values in this file
     * @param skipDirs list of dir names that should not recursive into - e.g. .svn
     * @throws java.lang.Exception failed during R/W of file
     */
    public static void replaceTokensInDirectory(
            final File directory,
            final Properties props,
            final List skipDirs,
            boolean prepareResources) throws IOException {

        if (skipDirs != null && skipDirs.contains(directory.getName())) {
            return;
        }

        if (!directory.exists() || !directory.isDirectory()) {
            return;
        }

        final File[] children = directory.listFiles();

        if ((children != null) && (children.length > 0)) {
            for (File child : children) {
                if (child.isDirectory()) {
                    replaceTokensInDirectory(child, props, skipDirs, prepareResources);
                } else {
                    replaceTokensInFile(child, props, prepareResources);
                }
            }
        }
    }

    public static void writeFile(
            final InputStream input,
            final File file) throws IOException {

        writeFile(input, file, -1);
    }

    public static void writeFile(
            final InputStream input,
            final File file,
            final long limit) throws IOException {

        FileOutputStream output = null;

        try {
            output = new FileOutputStream(file);

            if (limit != -1) {
                transferStream(input, output, limit);
            } else {
                transferStream(input, output);
            }
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public static void writeFile(
            final String contents,
            final File target) throws IOException {

        FileWriter writer = null;

        try {
            writer = new FileWriter(target);
            writer.write(contents);
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public static void writeFile(
            final java.util.Properties contents,
            final File file) throws IOException {

        FileOutputStream output = null;

        try {
            output = new FileOutputStream(file);
            contents.store(output, null);

        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public static void writeFile(
            final URL url,
            final File target) throws IOException {

        InputStream input = null;
        try {
            input = url.openStream();

            writeFile(input, target);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Transfers the contents of a resource identified by the given URL to the specified
     * file on the file system.
     *
     * @param file file where the contents of the URL resource should be stored.
     * @param url URL of the resource, which should be transferred to the given file.
     */
    public static void writeURLToFile(
            final File file,
            final URL url) throws IOException {

        InputStream input = null;

        try {
            input = url.openConnection().getInputStream();

            writeFile(input, file);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                // We don't want to be throwing an exception for such situation, so just
                // log and proceed normally.
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     *  reads entire file to a StringBuffer
     * @param file file to read.
     * @return StringBuffer with the data in the file.
     * @throws java.io.IOException
     */
    public static String readFile(
            final File file) throws IOException {

        FileReader reader = null;
        StringWriter writer = new StringWriter();
        try {
            reader = new FileReader(file);
            char[] buff = new char[7 * 1024];
            int count = 0;
            while ((count = reader.read(buff)) != -1) {
                writer.write(buff, 0, count);
            }
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    Logger.getLogger(MavenUtils.class.getName()).log(Level.FINEST, null, ex);
                }
            }
        }
        return writer.getBuffer().toString();
    }

    /**
     *  loads entire url content into Properties
     * @param URL URL to read.
     * @return String with the data in the file.
     * @throws java.io.IOException
     */
    public static Properties readURLContent(
            final URL url) throws IOException {

        InputStream is = url.openStream();
        Properties props = new Properties();
        try {
            props.load(is);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(MavenUtils.class.getName()).log(Level.FINEST, null, ex);
                }
            }
        }
        return props;

    }

    /**
     * Recursively deletes all empty directories.
     * 
     * @param dir directory to be deleted. If the directory has even a sin
     */
    public static void deleteEmptyDirectory(
            final File directory) throws IOException {

        if (!directory.exists()) {
            logger.fine("Trying to delete direcotry that does not exists : " + directory.getPath());
            return;
        }
        File[] children = directory.listFiles();

        if ((children != null) && (children.length != 0)) {
            for (File child : children) {
                if (child.isDirectory()) {
                    deleteEmptyDirectory(child);
                }
            }
        }

        if ((children != null) && (children.length == 0)) {
            logger.fine("Deleting empty directory " + directory.getAbsolutePath());
            if (!directory.delete()) {
                throw new IOException(
                        "Failed to delete directory: " + directory.getAbsolutePath());
            }
        }
    }

    /**
     * Deletes the directory and its children.
     * 
     * @param dir directory to be deleted.
     */
    public static void deleteDirectory(
            final File directory) throws IOException {

        if (!directory.exists()) {
            logger.fine("Trying to delete direcotry that does not exists : " + directory.getPath());
            return;
        }
        logger.fine("Deleting directory " + directory.getAbsolutePath());

        File[] children = directory.listFiles();

        if ((children != null) && (children.length != 0)) {
            for (File child : children) {
                if (child.isDirectory()) {
                    deleteDirectory(child);
                } else {
                    if (!child.delete()) {
                        throw new IOException(
                                "Failed to delete file: " + child.getAbsolutePath());
                    }
                }
            }
        }

        if (!directory.delete()) {
            throw new IOException(
                    "Failed to delete directory: " + directory.getAbsolutePath());
        }
    }

    public static void transferStream(
            final InputStream input,
            final OutputStream output) throws IOException {

        byte[] buffer = new byte[8 * 1024];
        int length = -1;

        while ((length = input.read(buffer)) != -1) {
            output.write(buffer, 0, length);
        }
    }

    public static void transferStream(
            final InputStream input,
            final OutputStream output,
            final long limit) throws IOException {

        long remaining = limit;

        int bufferSize = 8 * 1024;
        byte[] buffer = new byte[bufferSize];
        int length = -1;

        do {
            if (remaining > bufferSize) {
                length = input.read(buffer);
            } else {
                length = input.read(buffer, 0, (int) remaining);
            }

            if (length == -1) {
                logger.info("Input stream finished prematurely: read " +
                        (limit - remaining) + " bytes out of " + limit + ".");
                return;
            }

            remaining -= length;
            output.write(buffer, 0, length);
        } while (remaining > 0);
    }

    /**
     * Copies a single file.
     * 
     * @param source
     * @param target
     * @throws java.io.IOException
     */
    public static void copyFile(
            final File source,
            final File target) throws IOException {

        assert source.isFile();
        assert !target.exists();

        if (!target.getParentFile().exists() && !target.mkdirs()) {
            throw new IOException(
                    "Cannot create directory structure " + // NOI18N
                    "for " + target.getParent() + "."); // NOI18N
        }

        FileInputStream input = null;
        FileOutputStream output = null;

        try {
            input = new FileInputStream(source);
            output = new FileOutputStream(target);

            transferStream(input, output);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }

            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public static void moveFile(
            final File source,
            final File target) throws IOException {

        // First check whether the target file exists. If it does not, we check whether
        // its parent exists, if not we create it, if it is a file we throw an exception.
        if (!target.exists()) {
            if (!target.getParentFile().exists()) {
                if (!target.getParentFile().mkdirs()) {
                    throw new IOException("Failed to create directory: " +
                            target.getParentFile().getAbsolutePath());
                }
            } else if (!target.getParentFile().isDirectory()) {
                throw new IOException("The parent of the given target file, " +
                        target.getParentFile().getAbsolutePath() + " is not a " +
                        "directory.");
            }
        }

        // Then check whether the source exists and is a file. Otherwise we throw an
        // exception.
        if (!source.exists()) {
            throw new IOException("The source file, " + source.getAbsolutePath() +
                    ", does not exist.");
        }

        if (!source.isFile()) {
            throw new IOException("The source file, " + source.getAbsolutePath() +
                    ", is not a real file (might be a directory.)");
        }

        InputStream input = null;
        OutputStream output = null;

        try {
            input = new FileInputStream(source);
            output = new FileOutputStream(target);

            transferStream(input, output);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }

            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }

        // Now delete the source file. We won't be throwing an exception for this one, as
        // it does not matter that much. Just log something.
        if (!source.delete()) {
            logger.log(Level.SEVERE, "Failed to delete the source file, " +
                    source.getAbsolutePath() + ", after moving.");
        }
    }

    /**
     * Copies a file or a directory structure completely.
     * 
     * @param source
     * @param target
     * @throws java.io.IOException
     */
    public static void copy(
            final File source,
            final File target) throws IOException {

        // Copy with a default filter that accepts all
        FileFilter filter = new FileFilter() {

            public boolean accept(final File file) {
                return true;
            }
        };

        if (!target.exists()){
            target.mkdirs();
        }
        
        copy(source, target, filter);
    }

    public static void copy(
            final File source,
            final File target,
            final FileFilter filter) throws IOException {

        if (source.isDirectory()) {
            if (target.exists() && target.isFile()) {
                throw new IOException(
                        "An attempt was made to copy a directory into a file.");
            }

            if (!target.exists() && !target.mkdirs()) {
                throw new IOException(
                        "Failed to create directory structure " +
                        "for " + target + ".");
            }

            final File[] children = source.listFiles();

            if ((children != null) && (children.length > 0)) {
                for (File child : children) {
                    final File childTarget = new File(target, child.getName());

                    if (child.isFile()) {

                        if (filter.accept(child)) {
                            copyFile(child, childTarget);
                        }
                    } else {
                        copy(child, childTarget, filter);
                    }
                }
            }
        } else {
            // source is a file
            if (filter.accept(source)) {
                if (!target.exists() || target.isFile()) {
                    copyFile(source, target);
                } else {
                    copyFile(source, new File(target, source.getName()));
                }
            }
        }
    }

    public static void archive(
            final File input,
            final File archive) throws IOException {


        archive(input, archive, true);
    }

    public static void archive(
            final File input,
            final File archive,
            final boolean includeRoot) throws IOException {

        if (archive.exists() && archive.isDirectory()) {
            throw new IOException("Trying to create an archive in " + // NOI18N
                    "place of an existing directory."); // NOI18N
        }

        final File archiveParent = archive.getParentFile();
        if (!archiveParent.exists() && !archiveParent.mkdirs()) {
            throw new IOException("Cannot create directory structure " + // NOI18N
                    "for " + archiveParent); // NOI18N
        }

        if (!input.exists()) {
            throw new IOException("Trying to create an archive out " + // NOI18N
                    "of a non-existing file."); // NOI18N
        }

        ZipOutputStream zipOutput = null;

        try {
            zipOutput = new ZipOutputStream(new FileOutputStream(archive));

            final FileFilter filter = new FileFilter() {

                public boolean accept(final File file) {
                    return true;
                }
            };

            if (input.isDirectory() && !includeRoot) {
                final File[] children = input.listFiles();

                if ((children != null) && (children.length > 0)) {
                    for (File child : children) {
                        addToArchive(child, child.getName(), zipOutput, filter);
                    }
                }
            } else {
                addToArchive(input, input.getName(), zipOutput, filter);
            }
        } finally {
            try {
                if (zipOutput != null) {
                    zipOutput.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public static void addToArchive(
            final File input,
            final String name,
            final ZipOutputStream zipOutput) throws IOException {

        addToArchive(input, name, zipOutput, new FileFilter() {

            public boolean accept(final File file) {
                return true;
            }
        });
    }

    public static void addToArchive(
            final File input,
            final String name,
            final ZipOutputStream zipOutput,
            final FileFilter filter) throws IOException {

        if (!input.exists()) {
            throw new IOException("Trying to add a non-existing file to " +
                    "the archive."); // NOI18N
        }

        zipOutput.putNextEntry(createZipEntry(input, name));

        if (input.isDirectory()) {
            final File[] children = input.listFiles(filter);

            if ((children != null) && (children.length > 0)) {
                for (File child : children) {
                    final String childName =
                            (name + "/" + child.getName()).replace("//", "/");

                    addToArchive(child, childName, zipOutput, filter);
                }
            }
        } else {
            FileInputStream fileInput = null;

            try {
                fileInput = new FileInputStream(input);

                transferStream(fileInput, zipOutput);
            } finally {
                try {
                    if (fileInput != null) {
                        fileInput.close();
                    }
                } catch (IOException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }

    public static ZipEntry createZipEntry(
            final File input,
            final String name) {

        assert input.exists();

        final String realName;
        if (input.isDirectory() && !name.endsWith("/")) {
            realName = name + "/";
        } else {
            realName = name;
        }

        final ZipEntry entry = new ZipEntry(realName);
        entry.setSize(input.length());
        entry.setTime(input.lastModified());

        return entry;
    }

    public static List<File> findFiles(
            final File file,
            final FileFilter filter) {

        final List<File> result = new LinkedList<File>();

        if (file.exists()) {
            if (file.isFile() && filter.accept(file)) {
                result.add(file);
            } else {
                final File[] children = file.listFiles();

                if ((children != null) && (children.length > 0)) {
                    for (File child : children) {
                        result.addAll(findFiles(child, filter));
                    }
                }
            }
        }

        return result;
    }

    /**
     * Parse the flow configuration and read it into a HashMap. 
     * 
     * @param config - configuration properties in a string. This
     *                  string is always going to be of the form "name=value, *"
     * @return the configuration in a HashMap
     */
    public static java.util.Properties parseConfig(String config) {
        java.util.Properties configMap = new java.util.Properties();

        if (config != null) {
            String[] nvPairs = config.split(",");

            // parse all name/value pairs and store in a Map
            for (String nvPair : nvPairs) {
                String[] nv = nvPair.split("=", 2);
                String name = nv[0].trim();
                String value = "";
                if (nv.length == 2) {
                    value = nv[1].trim();
                    // remove quotes, if any
                    String v1 = value;
                    if (v1.startsWith("\"")) {
                        String v2 = v1.substring(1, v1.length());
                        if (v2.endsWith("\"")) {
                            value = v2.substring(0, v2.length() - 1);
                        }
                    }
                }

                configMap.setProperty(name, value.trim());
            }
        }
        return configMap;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private MavenUtils() {
        // Does nothing -- a placeholder
    }
}
