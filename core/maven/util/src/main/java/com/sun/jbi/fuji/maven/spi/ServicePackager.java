/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServicePackager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.spi;

import com.sun.jbi.fuji.ifl.IFLModel;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.glassfish.openesb.tools.common.jbi.descriptor.Jbi;

/**
 * A provider of given service type can implement this interface to perform
 * special processing related to packaging service artifacts in an application
 * archive.  Components and other providers should only implement this interface
 * if they have specific requirements which are not met by the default service
 * packaging implementation in the application packaging plugin.
 *
 * @author kcbabo
 *
 * @deprecated this interface is split into generate, build and package interfaces
 * specific to service type.
 *
 * @see org.glassfish.openesb.tools.common.service.spi.ServiceGenerator
 * @see org.glassfish.openesb.tools.common.service.spi.ServiceBuilder
 * @see org.glassfish.openesb.tools.common.service.spi.ServicePackager
 */
public interface ServicePackager {
    
    public void generateServiceUnit(
            final ServiceToolsLocator locator,
            final IFLModel ifl,
            final File baseDirectory,
            final Properties props,
            final List usedFiles) throws IOException;
    
    public void prepareServiceUnitResources(
            final IFLModel ifl,
            final List<String> services,
            final String applicationId,
            final File baseDirectory,
            final File outputDirectory) throws IOException;

    public File packageServiceUnit(
            final IFLModel ifl,
            final String serviceType,
            final List<String> serviceNames,
            final String applicationId,
            final File outputDirectory) throws IOException;

    public void addServiceUnitToApplication(
            final Jbi jbi,
            final String applicationId,
            final File outputDirectory,
            final Map<String, File> suMap) throws IOException;

    /**
     * This method allows you to play around with service resources before 
     * the application packager creates the application archive.
     * @param outputDir the output (build) directory for the entire application
     * @param resourceDir the output directory where all service resources
     * specific to a given service type are stored.
     * @param services list of service names contained in the archive
     * @throws java.lang.Exception throwing an exception will abrubtly end
     * the application packaging goal, resulting in a failed build.
     */
    public void beforeServicePackaging(
            File outputDir,
            File resourceDir,
            List<String> services) throws IOException;

    /**
     * This method allows you to play with the application bits before they 
     * are packaged into a service assembly.
     * @param appRoot root directory of application
     * @param services list of service names contained in the archive
     * @throws java.lang.Exception throwing an exception will abrubtly end
     * the application packaging goal, resulting in a failed build.
     */
    public void beforeApplicationPackaging(
            File appRoot,
            List<String> services) throws IOException;
}
