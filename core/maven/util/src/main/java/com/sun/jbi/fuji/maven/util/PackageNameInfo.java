/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PackageNameinfo.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.io.FileFilter;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.ArrayList;
import java.lang.StringBuffer;
                             
public class PackageNameInfo
{
    private String fullPathName = "";
    private String fileSeperatorChar = ".";
    private String packageName = "";
    private String className   = "";
    private HashMap<String,AnnotationInfo> annotations = new HashMap<String,AnnotationInfo>();


    public void setPackageName (String name) {
        this.packageName = name;
    }

    public void setClassName (String name) {
        this.className = name;
    }

    public void setFullPathName (String name) {
        fullPathName = name;
    }

    public boolean containsName(String name) {
        Set annotationSet = getAnnotationNames();
        for (Iterator iter = annotationSet.iterator(); iter.hasNext();) {
            String methodName = (String) iter.next();  
            AnnotationInfo annotationInfo = getAnnotationInfo(methodName);
            if (annotationInfo.containsKeyValue("name",name))
            {
                return true;
            }
        }
        return false;
    }

    public void addAnnotationInfo (String methodName,
                                   AnnotationInfo annotationInfo)
    {
        String name = annotationInfo.getElement("name");
        if (name == null) {
            name = getPathName(".",methodName);
        }
        // We don't want any "." in the name, so lets replace any "." with a "-".
        name = name.replaceAll("\\.","\\-");
        annotationInfo.setElement("name",name);
        annotations.put(methodName,annotationInfo);
    }

    public String getPackageName() {
        return this.packageName;
    }

    public String getFullPathName() {
        return fullPathName;
    }

    public String getPackagePathName(String seperator) {
        String pathName = this.packageName;
        if (!(seperator.equals(".")))
            pathName = this.packageName.replaceAll("\\.","\\/");
        return pathName;
    }

    public String getClassName() {
        return this.className;
    }

    public Set<String> getAnnotationNames () {
        return annotations.keySet();
    }

    public AnnotationInfo getAnnotationInfo (String methodName) {
        return annotations.get(methodName);
    }

    public HashMap<String,AnnotationInfo> getAnnotations() {
        return annotations;
    }

    public List<String> getMethodPathNames(String seperator) {
        List<String> methodPathNames = new ArrayList<String>();
        Set annotationSet = getAnnotationNames();
        for (Iterator iter = annotationSet.iterator(); iter.hasNext();) {
            String methodName = (String) iter.next();      
            String pathName = getPathName(seperator,methodName);
            methodPathNames.add(pathName);
        }
        return methodPathNames;
    }

    public List<String> getMethodPathNames() {
        return getMethodPathNames("/");
    }

    public String getPathName (String seperator, String methodName) {
        String packageName = getPackagePathName(seperator);
        String className   = getClassName();
        String pathName = packageName + seperator + className + seperator + methodName;
        return pathName;
    }

    // Good to use when debugging
    public String toString () {
        StringBuffer buffer = new StringBuffer();
        buffer.append ("Package: " + getPackageName() + "\n");
        buffer.append ("Class  : " + getClassName() + "\n");
        Set annotationSet = getAnnotationNames();
        for (Iterator iter = annotationSet.iterator(); iter.hasNext();) {
            String methodName = (String) iter.next();  
            buffer.append("  Method: " + methodName + "\n");
            AnnotationInfo annotationInfo = getAnnotationInfo(methodName);
            String elementsString = annotationInfo.toString("    ");
            buffer.append (elementsString);
        }
        return buffer.toString();
    }

}
