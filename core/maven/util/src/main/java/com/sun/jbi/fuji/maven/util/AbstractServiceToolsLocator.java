/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.util;

import com.sun.jbi.fuji.maven.spi.ServiceToolsLocator;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
import org.glassfish.openesb.tools.common.service.spi.ServiceBuilder;
import org.glassfish.openesb.tools.common.service.spi.ServiceGenerator;
import org.glassfish.openesb.tools.common.service.spi.ServicePackager;

/**
 * This class implements the common code for the ServiceToolsLocator which the
 * Maven, IDE and WebUi based tools will extend.
 * 
 * @author chikkala
 */
public abstract class AbstractServiceToolsLocator implements ServiceToolsLocator {

    private static final Logger LOG = Logger.getLogger(AbstractServiceToolsLocator.class.getName());
    private ToolsLookup mToolsLooiup;

    /**
     * constructor that takes the tools lookup implementation. Different environments
     * will have different ways to initialize the
     * @param toolsLookup
     */
    public AbstractServiceToolsLocator(ToolsLookup toolsLookup) {
        this.mToolsLooiup = toolsLookup;
    }

    protected ToolsLookup getToolsLookup() {
        return this.mToolsLooiup;
    }

    /**
     * returns a URL for maven archetype for a specified service type.
     * @deprecated  Only required for supporting backward compatibility until all
     * component archetypes are moved to service descriptor bundle.
     */
    protected abstract URL locateServiceArchetype(String serviceType) throws IOException;

    /**
     * returns a maven plugin for a specified service type.
     * @deprecated  Only required for supporting backward compatibility until all
     * component archetypes are moved to service descriptor bundle.
     */
    protected abstract URL locateServicePlugin(String serviceType) throws IOException;

    /**
     * return a URL for the jbi component installer jar which supports the specified
     * service type for deployment.
     */
    protected abstract URL locateServiceInstaller(String serviceType) throws IOException;

    public ServiceDescriptor getServiceDescriptor(String serviceType) throws IOException {
        // first try to load it from the service descriptor archive.
        // if not there, construct the service descriptor from maven archetype and plugin
        ServiceDescriptor sd = null;
        try {
            sd = this.getToolsLookup().getServiceDescriptor(serviceType);
        } catch (Exception ex) {
            throw new IOException(ex.getMessage());
        }
        //TODO: fix it when every thing is moved to service descriptor model.
        // if service descriptor not found, it creates a wrapper service descriptor
        // around the deprecated service archetype and plugin model.
        if (sd == null) {
            // contruction it from maven archetype and plugin
            URL archetypeURL = null;
            URL pluginURL = null;
            archetypeURL = this.locateServiceArchetype(serviceType);
            try {
                pluginURL = this.locateServicePlugin(serviceType);
            } catch (Exception ex) {
                //ignore. plugin is optional.
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.log(Level.FINEST, ex.getMessage(), ex);
                }
            }
            if (archetypeURL != null) {
                sd = new ServiceDescriptorWrapper(serviceType,
                        archetypeURL, pluginURL, this);
                //TODO: cache the descriptor for performance.
            } else {
                LOG.warning("No Service Descritpro found for service type " + serviceType);
            }
        }
        return sd;
    }

    public ServiceGenerator getServiceGenerator(String serviceType) throws IOException {
        ServiceDescriptor sd = getServiceDescriptor(serviceType);
        ServiceGenerator sg = null;
        if (sd != null) {
            sg = sd.getServiceGenerator();
        }
        return sg;
    }

    public ServiceBuilder getServiceBuilder(String serviceType) throws IOException {
        ServiceDescriptor sd = getServiceDescriptor(serviceType);
        ServiceBuilder sb = null;
        if (sd != null) {
            sb = sd.getServiceBuilder();
        }
        return sb;
    }

    public ServicePackager getServicePackager(String serviceType) throws IOException {
        ServiceDescriptor sd = getServiceDescriptor(serviceType);
        ServicePackager sp = null;
        if (sd != null) {
            sp = sd.getServicePackager();
        }
        return sp;
    }
}
