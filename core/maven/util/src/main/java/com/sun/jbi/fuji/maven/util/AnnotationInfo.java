/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PackageNameinfo.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.util.HashMap;
import java.util.Iterator;
                             
public class AnnotationInfo
{
    private String name;
    private HashMap<String,String> elements = new HashMap<String,String>();

    public AnnotationInfo () {
    }

    public AnnotationInfo (String name) {
        setName(name);
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return this.name;
    }

    public void setElement (String key, String value) {
        elements.put(key,value);
    }

    public String getElement (String key) {
        return elements.get(key);
    }

    public HashMap getElements () {
        return elements;
    }

    public boolean containsKeyValue (String key, String value) {
        String keyValue = elements.get(key);
        if (keyValue.equals(value))
        {
            return true;
        }
        return false;
    }

    public String toString (String indent) {
        StringBuffer buffer = new StringBuffer();
        Iterator itr = elements.keySet().iterator();
        while (itr.hasNext()) {
            String key = (String)itr.next();
            String value = elements.get(key);
            buffer.append (indent + key + "=" + value + "\n");
        }
        return buffer.toString();
    }

    public String toString () {
        return toString ("");
    }

}
