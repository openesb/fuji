/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 */
package com.sun.jbi.fuji.maven.util;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.maven.spi.ServicePackager;
import com.sun.jbi.fuji.maven.spi.ServiceToolsLocator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.glassfish.openesb.tools.common.jbi.descriptor.Jbi;
import org.glassfish.openesb.tools.common.jbi.descriptor.Provides;
import org.glassfish.openesb.tools.common.jbi.descriptor.ServiceEntry;
import org.glassfish.openesb.tools.common.jbi.descriptor.ServiceUnit;
import org.glassfish.openesb.tools.common.service.FileServiceConfigurationHelper;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;

/**
 *
 * @deprecated This implementation is deprecated.
 */
public class DefaultServicePackager implements ServicePackager {
    //////////////////////////////////////////////////////////////////////////////////////
    // Static

    private static Logger logger =
            Logger.getLogger(DefaultServicePackager.class.getName());

    //////////////////////////////////////////////////////////////////////////////////////
    // Instance
    private String serviceType;

    public DefaultServicePackager(
            final String serviceType) {
        this.serviceType = serviceType;
    }

    protected URL getServiceArchetypeURL(ServiceToolsLocator locator, String serviceType) {
        URL archetypeURL = null;
        try {
            ServiceDescriptor sd = locator.getServiceDescriptor(serviceType);
            Map<String, URL> resMap = sd.getWebResources();
            for (String key : resMap.keySet()) {
                if (key.endsWith("archetype.jar")) {
                    archetypeURL = resMap.get(key);
                    break;
                }
            }
        } catch (Exception ex) {
            logger.log(Level.INFO, ex.getMessage(), ex);
        }
        return archetypeURL;
    }

    public void generateServiceUnit(
            final ServiceToolsLocator locator,
            final IFLModel ifl,
            final File baseDirectory,
            final Properties props,
            final List usedFiles) throws IOException {

        FileServiceConfigurationHelper fileConfigHelper = new FileServiceConfigurationHelper();

        for (String serviceName: ifl.getServicesForType(serviceType)) {
            ZipInputStream zipInput = null;
            InputStream archetypeIS = null;
            try {
                URL archetypeURL = getServiceArchetypeURL(locator, serviceType);
                archetypeIS = archetypeURL.openStream();
                zipInput = new ZipInputStream(archetypeIS);

                ZipEntry entry = null;
                while ((entry = zipInput.getNextEntry()) != null) {
                    // We can't extract the archetype as is because it has a funky
                    // structure.  This code massages the directory naming to match
                    // the current app layout.
                    if (entry.getName().startsWith("archetype-resources/src/main") &&
                            !entry.isDirectory()) {

                        // need to mangle the name a bit - this is so hacky
                        String name = entry.getName();
                        name = name.replace("archetype-resources/", "");

                        // take care of the static Fuji directories
                        if (name.startsWith("src/main/config")) {
                            name = name.replace("src/main/config",
                                    "src/main/config/" + serviceType + "/" + serviceName);
                        } else if (name.startsWith("src/main/resources/META-INF")) {
                            name = name.replace("src/main/resources",
                                    "src/main/resources/" + serviceType);
                        } else if (name.startsWith("src/main/resources")) {
                            name = name.replace("src/main/resources",
                                    "src/main/resources/" + serviceType + "/" + serviceName);
                        } else if (name.startsWith("src/main/" + serviceType)) {
                            name = name.replace("src/main/" + serviceType,
                                    "src/main/" + serviceType + "/" + serviceName);
                        }

                        final File file = new File(baseDirectory, name);
                        usedFiles.add(file);

                        // Do not overwrite existing artifacts
                        if (file.exists()) {
                            // for generate service unit request from webui
                            // we need to merge any changes made - so don't skip
                            if (fileConfigHelper.isTempConfigExists(file.getParentFile())) {
                                // process further.
                            } else {
                                // process next
                                continue;
                            }
                        }
                        // New generation, need to create parent dir
                        if (!file.getParentFile().exists()) {
                            file.getParentFile().mkdirs();
                        }
                        // for file service prototyping only
                        // now also added ftp service
                        if ( (serviceType.equals("file") || serviceType.equals("ftp")) && name.endsWith("service.properties")) {
                            boolean fileConfigGenerated =
                                    fileConfigHelper.generateServiceConfigFile(
                                    serviceType, serviceName, ifl, file, name, zipInput, entry.getSize());
                        } else {
                            MavenUtils.writeFile(zipInput, file, entry.getSize());
                        }
                    }
                }
            } finally {
                try {
                    if ( archetypeIS != null) {
                        try {
                        archetypeIS.close();
                        } catch(Exception ex) {}
                    }
                    if (zipInput != null) {
                        zipInput.close();
                    }
                } catch (IOException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }

    public void prepareServiceUnitResources(
            final IFLModel ifl,
            final List<String> services,
            final String applicationId,
            final File baseDirectory,
            final File outputDirectory) throws IOException {

        for (String serviceName: services) {
            final File resourceDir =
                    getServiceResourceDir(serviceName, outputDirectory);

            final Properties props = new Properties();
            props.put("applicationId", applicationId);
            props.put("artifactId", applicationId);
            props.put("serviceName", serviceName);
            props.put("serviceType", serviceType);

            if (ifl.getConsumedServices().contains(serviceName)) {
                props.put("consumedService", serviceType + "_" + serviceName);
                props.put("providedService", "");
            } else {
                 props.put("consumedService", "");
                 props.put("providedService", serviceName);
            }

            // check for a service-specific properties file
            final String propsFilePath = "src/main/config/" +
                    serviceType + "/" + serviceName + "/service.properties";
            final File propsFile = new File(baseDirectory, propsFilePath);

            if (propsFile.exists()) {
                FileInputStream input = null;

                try {
                    input = new FileInputStream(propsFile);
                    props.load(input);
                } finally {
                    try {
                        if (input != null) {
                            input.close();
                        }
                    } catch (IOException e) {
                        logger.log(Level.SEVERE, e.getMessage(), e);
                    }
                }

            }

            MavenUtils.replaceTokensInDirectory(resourceDir, props, MavenUtils.DIRS_TO_SKIP, true);

            // make sure all the WSDL bits are in place
            final File defaultWSDLDir = new File(outputDirectory, "classes/wsdl");
            if (!(new File(resourceDir, WSDL_BINDING).exists())) {
                MavenUtils.copy(new File(defaultWSDLDir, WSDL_BINDING), resourceDir);
            }
            if (!(new File(resourceDir, WSDL_INTERFACE).exists())) {
                MavenUtils.copy(new File(defaultWSDLDir, WSDL_INTERFACE), resourceDir);
            }
            if (!(new File(resourceDir, WSDL_MESSAGE).exists())) {
                MavenUtils.copy(new File(defaultWSDLDir, WSDL_MESSAGE), resourceDir);
            }
        }
    }

    public File packageServiceUnit(
            final IFLModel ifl,
            final String serviceType,
            final List<String> serviceNames,
            final String applicationId,
            final File outputDirectory) throws IOException {

        File resourceDir = getServiceTypeResourceDir(outputDirectory);

        // create service unit jbi.xml
        final Jbi jbi = Jbi.newServiceUnitDescriptor();

        for (String serviceName: serviceNames) {
            if (ifl.isConsumed(serviceName)) {
                final ServiceEntry consumes = jbi.getServices().addConsumes();

                consumes.setServiceName(
                        ApplicationUtils.getServiceQName(ifl.getNamespace(serviceName), serviceType + "_" + serviceName));
                consumes.setInterfaceName(
                        ApplicationUtils.getInterfaceQName(applicationId));
                consumes.setEndpointName(
                        ApplicationUtils.getEndpointName(serviceName));
            } else if (ifl.isProvided(serviceName)) {
                final Provides provides = jbi.getServices().addProvides();

                provides.setServiceName(
                        ApplicationUtils.getServiceQName(ifl.getNamespace(serviceName), serviceName));
                provides.setInterfaceName(
                        ApplicationUtils.getInterfaceQName(applicationId));
                provides.setEndpointName(
                        ApplicationUtils.getEndpointName(serviceName));
            }
        }

        final File metaInf = new File(resourceDir, "META-INF");
        if (!metaInf.exists()) {
            metaInf.mkdirs();
        }

        FileOutputStream output = null;

        try {
            output = new FileOutputStream(new File(metaInf, "jbi.xml"));
            jbi.writeTo(output);
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw (IOException) new IOException().initCause(e);
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }

        // callback for subclasses to override
        beforeServicePackaging(outputDirectory, resourceDir, serviceNames);

        final File archive = new File(
                outputDirectory,
                SU_PACKAGE_DIR + "/" + serviceType + ".jar");

        MavenUtils.archive(resourceDir, archive, false);

        return archive;
    }

    public void addServiceUnitToApplication(
            final Jbi jbi,
            final String applicationId,
            final File outputDirectory,
            final Map<String, File> suMap) throws IOException {

        final File resourceDir = getServiceTypeResourceDir(outputDirectory);

        // update the service assembly jbi.xml
        final ServiceUnit su = jbi.getServiceAssembly().addServiceUnit();
        su.setArtifactsZip(((File) suMap.get(serviceType)).getName());

        final Properties suManifest = new Properties();
        FileInputStream input = null;
        try {
            input = new FileInputStream(new File(resourceDir, "META-INF/MANIFEST.MF"));
            suManifest.load(input);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }

        final String targetComponent = suManifest.getProperty(COMPONENT_NAME);
        
        su.setComponentName(targetComponent);
        su.getIdentification().setName(applicationId + "-" + targetComponent);
    }

    public void beforeServicePackaging(
            final File outputDir,
            final File resourceDir,
            final List<String> serviceNames) throws IOException {

        // Does nothing
    }

    public void beforeApplicationPackaging(
            final File appRoot,
            final List<String> serviceNames) throws IOException {

        // Does nothing
    }

    // Private ///////////////////////////////////////////////////////////////////////////
    private File getServiceResourceDir(
            final String serviceName,
            final File outputDirectory) {

        return new File(
                outputDirectory,
                "classes/" + serviceType + "/" + serviceName);
    }


    public File getServiceTypeResourceDir(
            final File outputDirectory) {
        return new File(outputDirectory, "classes/" + serviceType);
    }
    //////////////////////////////////////////////////////////////////////////////////////
    // Constants
    /** 
     * WSDL resources that need to be deployed in each service unit. 
     */
    private static final String WSDL_BINDING =
            "binding.wsdl"; // NOI18N
    private static final String WSDL_INTERFACE =
            "interface.wsdl"; // NOI18N
    private static final String WSDL_MESSAGE =
            "message.xsd"; // NOI18N
    /** 
     * Packaging directory for service units 
     */
    private static final String SU_PACKAGE_DIR =
            "service-units"; // NOI18N
    /** 
     * Consts used in SU manifest header 
     */
    private static final String COMPONENT_NAME =
            "Component-Name"; // NOI18N


}
