/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)GeneratePackageNames.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StreamTokenizer;
import java.io.IOException;
import java.io.File;
import java.io.FileFilter;
import java.util.Properties;
import java.util.Collection;
import java.util.ArrayList;
import com.sun.jbi.fuji.maven.util.FileWalker;
import com.sun.jbi.fuji.maven.util.PackageNameInfo;

public class GeneratePackageNames
{
    Collection<PackageNameInfo> fileList_ = new ArrayList<PackageNameInfo>();

    public void GeneratePackageNames () 
    {
    }

    public Collection<PackageNameInfo> getAnnotationList (String startingDir,
                                                          String keyAnnotationName) throws IOException
    {
        // Create a collection of all the .java files living under the starting directory
        JavaFileFilter filter = new JavaFileFilter();
        FileWalker fileWalker = new FileWalker();
        Collection<File> results = fileWalker.walk (startingDir,filter);

        // Process all the file in the Collection list that was created above
        for (File f : results)
        {
            PackageNameInfo packageName = new PackageNameInfo();
            packageName.setFullPathName(f.toString());

            // Initialize some variables
            boolean annotationFound          = false;
            boolean waitForMethodLine        = false;
            boolean methodLine               = false;
            boolean keyAnnotationFound       = false;
            boolean insideKeyAnnotation      = false;
            boolean annotationNameFound      = false;

            AnnotationInfo annotationInfo = new AnnotationInfo();

            int packageFlag = 0;
            int classFlag   = 0;

            String interceptName = "";
            String word = "";
            String key = "";
            String value = "";

            // Create the tokenizer to read from a file
            FileReader rd = new FileReader(f.getPath());
            BufferedReader br = new BufferedReader(rd);
            br.mark(255);

            // I know this looks a little funny, but it is needed, since the
            // Streamtokenizer has problems picking up the first word in a file.
            String line1 = br.readLine();
            String firstWord = line1;
            int i = line1.indexOf(' ');
            if (i != -1)
                firstWord = line1.substring(0,i);
            if (firstWord.equals("package"))
                packageFlag = 1;
            br.reset();

            StreamTokenizer st = new StreamTokenizer(br);

            // Prepare the tokenizer for Java-style tokenizing rules
            st.parseNumbers();
            st.wordChars('_', '_');
            st.eolIsSignificant(true);

            // If whitespace is not to be discarded, make this call
            st.ordinaryChars(0, ' ');

            // These calls caused comments to be discarded
            st.slashSlashComments(true);
            st.slashStarComments(true);

            // Parse the file
            int token = st.nextToken();
            while (token != StreamTokenizer.TT_EOF)
            {
                token = st.nextToken();
                switch (token)
                {

                case StreamTokenizer.TT_WORD:
                    word = st.sval;
                    if (packageFlag == 1)
                    {
                        packageName.setPackageName(word);
                        packageFlag = 2;
                    }
                    else if (classFlag == 1)
                    {
                        packageName.setClassName(word);
                        classFlag = 2;
                    }
                    if (annotationFound)
                    {
                        if (word.equals(keyAnnotationName)) 
                        {
                            insideKeyAnnotation = true;
                            keyAnnotationFound  = true;
                            waitForMethodLine   = true;
                        }
                        annotationFound = false;
                        break;
                    }
                    if (word.equals("package"))
                    {
                        if (packageFlag == 0)
                            packageFlag = 1;
                    }
                    else if (word.equals("class"))
                    {
                        if (classFlag == 0)
                            classFlag = 1;
                    }
                    if (insideKeyAnnotation)
                    {
                        if (key.equals("")) {
                            key = word;
                        }
                        else {
                            value = word;
                            annotationInfo.setElement(key,value);
                            key = "";
                        }
                        if (annotationNameFound) {
                            interceptName = word;
                            annotationNameFound = false;
                        }
                        else if (word.equalsIgnoreCase("name"))
                        {
                            annotationNameFound = true;
                        }
                    }
                    break;

                case StreamTokenizer.TT_NUMBER:
                    if (insideKeyAnnotation)
                    {
                        if (!(key.equals(""))) {
                            value = st.nval + "";
                            if (value.endsWith(".0"))
                            {
                                value = value.substring(0,value.length()-2);
                            }
                            annotationInfo.setElement(key,value);
                            key = "";
                        }
                    }
                    break;

                case StreamTokenizer.TT_EOL:
                    insideKeyAnnotation = false;
                    if (waitForMethodLine)
                        methodLine = true;
                    break;

                case StreamTokenizer.TT_EOF:
                    if (keyAnnotationFound) {
                        fileList_.add(packageName);
                        keyAnnotationFound = false;
                    }
                    break;

                case '"':
                    if (insideKeyAnnotation)
                    {
                        if (!(key.equals(""))) {
                            value = st.sval + "";
                            annotationInfo.setElement(key,value);
                            key = "";
                        }
                    }

                    if (annotationNameFound) {
                        interceptName = st.sval;
                        annotationNameFound = false;
                    }
                    break;

                case '=':
                    break;

                case '@':
                    annotationFound = true;
                    break;

                case '(':
                    if (methodLine == true)
                    {
                        packageName.addAnnotationInfo (word, annotationInfo);
                        methodLine = false;
                        waitForMethodLine = false;
                        interceptName = "";
                        annotationInfo = new AnnotationInfo();
                    }
                    break;

                case ')':
                    break;

                case '{':
                    insideKeyAnnotation = false;
                    annotationInfo = new AnnotationInfo();
                    break;
                }
            }
            rd.close();
        }
        return fileList_;
    }


    public boolean containsName (String name)
    {
        // Process all the file in the Collection list that was created above
        for (PackageNameInfo packageName : fileList_)
        {
            if (packageName.containsName(name))
                return true;
        }
        return false;
    }


    // Private inner class defining the filter used to find the .java files
    private class JavaFileFilter implements FileFilter 
    {
        public boolean accept (File f) {
            if (f.isDirectory())return true;
            String name = f.getName().toLowerCase();
            return name.endsWith("java");
        }
    }

}
