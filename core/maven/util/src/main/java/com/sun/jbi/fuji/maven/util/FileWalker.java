/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileWalker.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.maven.util;

import java.io.File;
import java.io.FileFilter;
import java.util.*;

/**
 * Utility class that will walk through the files under a specified directory path and 
 * generate a collection of all files that meet the filter criteria.
 * 
 * @author markrs
 */
public class FileWalker
{
    public Collection<File> walk (String startingDirectory, FileFilter filter)
    {
        Collection<File> results = new ArrayList<File>();
        File startingDir = new File(startingDirectory);
        walkDirectory (startingDir, filter, results);
        return results;
    }

    private void walkDirectory (File dir, FileFilter filter, Collection<File> results)
    {
        final List<File> pendingDirectories = new LinkedList<File>();
        final File[] files = dir.listFiles();
        for (final File currentFile : files)
        {
            if (currentFile.isFile() && filter != null && filter.accept(currentFile))
            {
                results.add(currentFile);
            } else if (currentFile.isDirectory())
            {
                pendingDirectories.add(currentFile);
            }
        }
        while (!pendingDirectories.isEmpty())
        {
            File childDir = pendingDirectories.get(0);
            pendingDirectories.remove(0);
            walkDirectory(childDir, filter, results);
        }
    }
}



