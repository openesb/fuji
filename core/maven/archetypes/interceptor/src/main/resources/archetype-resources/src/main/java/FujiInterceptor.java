/*
 * FujiInterceptor.java
 */

package $package;

import java.util.Properties;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.MessageExchange;

import com.sun.jbi.interceptors.Intercept;

/**
 * A POJO (Simple Class) with Interceptor annotations to add Fuji 
 * message exchange interceptor.
 * 
 * @author 
 */
public class FujiInterceptor {

    public FujiInterceptor() {
    }

    /**
     * A  message exchange interceptor
     * 
     * @param exchange - MessageExchange
     */
    @Intercept(priority = 201, status = "Active")
    public boolean messageExchangeInterceptor(MessageExchange exchange) {
        //TODO: add message exchange interceptor processing code here.
        NormalizedMessage msg = exchange.getMessage("in");
        // msg.setProperty("Fuji1", "Value1");
        
        return true;

    }
    
    /**
     * This method is invoked by the runtime when the interceptors
     * configuration is created/updated.
     * 
     * @param interceptorName - name of the interceptor that this configuration applies to
     * @param properties - updated configuration properties
     */
     public void setConfiguration(String interceptorName, Properties properties)
     {
         // TODO: handle the configuration change notification
         // System.out.println("Configuration for interceptor " 
         //   + interceptorName + " is  " + properties.toString());

     }
}