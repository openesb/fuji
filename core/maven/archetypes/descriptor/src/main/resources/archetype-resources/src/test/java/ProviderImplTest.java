#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */
package ${package};

import java.net.URL;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.glassfish.openesb.tools.common.DefaultToolsLookup;
import org.glassfish.openesb.tools.common.ToolsLookup;
import org.glassfish.openesb.tools.common.service.ServiceDescriptor;
/**
 *
 * @author chikkala
 */
public class ProviderImplTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ProviderImplTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ProviderImplTest.class);
    }
    /**
     * Rigourous Test :-)
     */
    public void testProviderImpl() throws Exception {
        // just instatiating it is the test :-)
        ProviderImpl provider = new ProviderImpl();
        URL url = provider.getServiceDescriptorURL();
        System.out.println("ServiceDescriptor URL " + url);
        assertNotNull(url);
        ToolsLookup lookup = new DefaultToolsLookup();
        ServiceDescriptor sd = lookup.getServiceDescriptor("${serviceType}");
        System.out.println("Service Descriptor found for " + sd.getServiceType());
        assertEquals("${serviceType}", sd.getServiceType());        
    }

}

