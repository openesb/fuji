#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $serviceTypeUpperCase = $serviceType.toUpperCase() )
#set( $serviceTypeCamelCase = $serviceType.substring(0,1).toUpperCase() + $serviceType.substring(1))
/*
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 */

//----------------------------------------------------------------------------------------
var ${serviceTypeUpperCase}_ADAPTER = "${serviceType}"; // NOI18N

//----------------------------------------------------------------------------------------
var ${serviceTypeCamelCase}AdapterNode = Class.create(AdapterNode, {
    initialize: function($super, data, ui_options, skip_ui_init) {
        $super(data, ui_options, skip_ui_init);

        if (!skip_ui_init) {
            this.ui = Ws.Graph.NodeUiFactory.new_node_ui(
                    ${serviceTypeUpperCase}_ADAPTER, this);
        }

        Ws.Utils.apply_defaults(this.properties, {
            ${serviceType}_param1: "value1",
            ${serviceType}_param2: "value2",
            ${serviceType}_param3: "value3"
        });
    },

    pc_get_descriptors: function($super) {
        var container = $super();

        container.sections[0].properties.push({
            name: "properties_${serviceType}_param1",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Param1",
            description: "Should get it from the descriptor",
            value: this.properties.${serviceType}_param1
        });

        container.sections[0].properties.push({
            name: "properties_${serviceType}_param2",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Param2",
            description: "Should get it from the descriptor",
            value: this.properties.${serviceType}_param2
        });

        container.sections[0].properties.push({
            name: "properties_${serviceType}_param3",
            type: Ws.PropertiesSheet.STRING,

            display_name: "Param3",
            description: "Should get it from the descriptor",
            value: this.properties.${serviceType}_param3
        });

        return container;
    }
});

//----------------------------------------------------------------------------------------
var ${serviceTypeCamelCase}AdapterNodeUi = Class.create(AdapterNodeUi, {
    initialize: function($super, node, options) {
        $super(node, options);
    }
});

//----------------------------------------------------------------------------------------
Ws.Graph.NodeFactory.register_type(${serviceTypeUpperCase}_ADAPTER, ${serviceTypeCamelCase}AdapterNode);
Ws.Graph.NodeUiFactory.register_type(${serviceTypeUpperCase}_ADAPTER, ${serviceTypeCamelCase}AdapterNodeUi);
