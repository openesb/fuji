#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $serviceTypeUpperCase = $serviceType.toUpperCase() )
#set( $serviceTypeCamelCase = $serviceType.substring(0,1).toUpperCase() + $serviceType.substring(1))
{
    id: "${serviceType}-template",

    display_name: "${serviceTypeCamelCase} Adapter",
    description: "Basic template for ${serviceTypeCamelCase} adapters.",
    icon: null,
    tags: ["template", "adapter", "${serviceType}"],

    is_drag_source: true,
    drag_shadow: null,

    offset: 10200,

    section: "templates",

    item_data: {
        node_type: "${serviceType}",
        node_data: {
            properties: {
                code_name: "${serviceType}-",
                is_shared: false,

                ${serviceType}_param1: null,
                ${serviceType}_param2: null,
                ${serviceType}_param3: null,

                noop: null
            },

            ui_properties: {
                display_name: "${serviceTypeCamelCase} ",
                description: "${serviceTypeCamelCase} description",
                icon: null,

                noop: null
            }
        }
    }
}