/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceMessageImplTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.service.ServiceMessageImpl;
import org.glassfish.openesb.api.test.MessageImpl;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.namespace.QName;
import junit.framework.TestCase;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author kcbabo
 */
public class ServiceMessageImplTest extends TestCase {
    
    private static final String XML_STR = "<parent><child/></parent>";
    
    private static final String WRAPPED_MSG = 
            "ServiceMessageImplTest/wrapped-msg.xml";
    
    private ServiceMessageImpl serviceMessage_;
    private ServiceMessageImpl serviceMessage2_;
    private ServiceMessageImpl serviceMessage3_;

    public ServiceMessageImplTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        serviceMessage_ = new ServiceMessageImpl(new MessageImpl());
        serviceMessage2_ = new ServiceMessageImpl(new MessageImpl(), new QName("ns", "a"));
        serviceMessage3_ = new ServiceMessageImpl(new MessageImpl(), serviceMessage2_);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getPayload method, of class ServiceMessageImpl.
     */
    public void testGetPayload() throws Exception {
        String payload = "hello!";
        
        // Should be empty at first
        assertNull(serviceMessage_.getPayload());
        serviceMessage_.setPayload(payload);
        // Should get back what I put on there
        assertEquals(payload, serviceMessage_.getPayload());
    }

    /**
     * Test of setPayload method, of class ServiceMessageImpl.
     */
    public void testSetPayload() throws Exception {
        // test the different types of service message payload
        serviceMessage_.setPayload("hello");
        assertTrue(serviceMessage_.getPayload() instanceof String);
    }
 
    /**
     * Test of getPayload method, of class ServiceMessageImpl.
     * This will be enabled once the related issue is fixed.
     *
    public void testGetPayloadTwice() throws Exception {
        // test the different types of service message payload
        serviceMessage_.setPayload("hello");
        assertTrue(serviceMessage_.getPayload() instanceof String);
        assertTrue(serviceMessage_.getPayload() instanceof String);
    }*/
    
    /**
     * Test of setPayload method with alternating object and XML payloads.
     */
    public void testMixedPayloads() throws Exception {
        serviceMessage_.setPayload("hello");
        assertTrue(serviceMessage_.getPayload() instanceof String);
        
        serviceMessage_.setPayload(new StreamSource(
                new ByteArrayInputStream(XML_STR.getBytes())));
        assertTrue(serviceMessage_.getPayload() instanceof Document);
        
        serviceMessage_.setPayload("goodbye");
        assertTrue(serviceMessage_.getPayload() instanceof String);
    }
    
    public void testGetWrappedPayload() throws Exception {
        MessageImpl msg = new MessageImpl();
        InputStream is = getClass().getClassLoader().getResourceAsStream(WRAPPED_MSG);
        msg.setContent(new StreamSource(is));
        ServiceMessage smsg = new ServiceMessageImpl(msg);
        smsg.getPayload();
    }
    
    /**
     * Test of getNormalizedMessage method, of class ServiceMessageImpl.
     */
    public void testGetNormalizedMessage() {
        assertTrue(serviceMessage_.getNormalizedMessage() != null);
    }

    /**
     * Test of ServiceMessageImpl constructor with ServiceMessage type.
      */
    public void testServiceMessageHint() throws Exception {
        assertEquals(nodeToString(((DOMSource)(serviceMessage2_.getContent())).getNode()),
                nodeToString(((DOMSource)(serviceMessage3_.getContent())).getNode()));
    }

    private String nodeToString(Node node)
        throws Exception
    {
        String nodeAsString = null;
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();

        if (node.getNodeType() == Node.DOCUMENT_NODE) {
            node = ((Document)node).getDocumentElement();
        }
        if ( node.getNodeType() == Node.ELEMENT_NODE)
        {
            // xsd:anyType
            StringWriter sw = new StringWriter();
            transformer.transform(new DOMSource(node), new StreamResult(sw));
            nodeAsString = sw.toString();
        }
        else if (node.getNodeType() == Node.DOCUMENT_NODE) {

        }
        else if ( node.getNodeType() == Node.TEXT_NODE )
        {
            // xsd:string
            nodeAsString = node.getNodeValue();
        }

        return nodeAsString;
    }

}
