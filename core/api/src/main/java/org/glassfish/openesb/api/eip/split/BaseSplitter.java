/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitType.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.eip.split;

import java.util.ArrayList;
import java.util.HashMap;

import org.glassfish.openesb.api.eip.Resource;
import org.glassfish.openesb.api.service.ServiceMessage;
import org.osgi.framework.Bundle;
import javax.xml.namespace.NamespaceContext;

/**
 * Interface for a splitter
 * 
 * @author Sun Microsystems, Inc.
 */
abstract public class BaseSplitter implements Splitter { 
    
    @Resource 
    public  HashMap config_;
    
    @Resource  
    public NamespaceContext namespaceCtx_;
    
    @Resource
    public Bundle bundle_;
    
    /**
     * Split the current active message in the exchange and return the split documents ina 
     * ArrayList
     * 
     * @param svcMsg - message to split
     */
    abstract public ArrayList split(ServiceMessage svcMsg)
        throws Exception;
    
}
