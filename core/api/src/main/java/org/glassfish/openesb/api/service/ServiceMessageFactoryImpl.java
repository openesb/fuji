/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceMessageFactoryImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.xml.namespace.QName;

/**
 * 
 * @author kcbabo
 */
public class ServiceMessageFactoryImpl implements ServiceMessageFactory {
    
    /** Hook to create message instances */
    private MessageExchange messageFactory_;
    /**
     * Creates a new instance of ServiceMessageFactory
     * @param mef exchange factory used to create messages
     */
    public ServiceMessageFactoryImpl(MessageExchangeFactory mef) {
        // This is a bit of a hack, but message exchange is the only public
        // interface to create a normalized message instance
        try {
            messageFactory_ = mef.createInOnlyExchange();
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }
    
    /**
     * Creates a new ServiceMessage.
     * @return new ServiceMessage.
     */
    public ServiceMessage createMessage() {
        try {
            return new ServiceMessageImpl(messageFactory_.createMessage());
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }
    
    /**
     * Creates a new ServiceMessage with the specified type.  This method is
     * provided for WSDL 1.1-based services which must be wrapped with part/type
     * information in order to travel over the NMR.
     * @param type QName identifying the message type
     * @return service message with the appropriate wrapper
     */
    public ServiceMessage createMessage(QName type) {
        try {
            return new ServiceMessageImpl(messageFactory_.createMessage(), type);
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }
    
     /**
     * Creates a new ServiceMessage with the type taken from the given ServiceMessage.
     * This method is provided for WSDL 1.1-based services which must be wrapped with part/type
     * information in order to travel over the NMR.
     * @param type QName identifying the message type
     * @return service message with the appropriate wrapper
     */
    public ServiceMessage createMessage(ServiceMessage type) {
        try {
            return new ServiceMessageImpl(messageFactory_.createMessage(), type);
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }

    /**
     * Creates a new ServiceMessage representing a fault.  The returned object
     * will return true when isFault() is called.
     * @return new ServiceMessage.
     */
    public ServiceMessage createFault() {
        try {
            return new ServiceMessageImpl(messageFactory_.createFault());
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }
    
    /**
     * Creates a new ServiceMessage with the specified type.  This method is
     * provided for WSDL 1.1-based services which must be wrapped with part/type
     * information in order to travel over the NMR. The returned object
     * will return true when isFault() is called.
     * @param type QName identifying the message type
     * @return service message with the appropriate wrapper
     */
    public ServiceMessage createFault(QName type) {
        try {
            return new ServiceMessageImpl(messageFactory_.createFault(), type);
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }

    /**
     * Creates a new ServiceMessage with the specified type.  This method is
     * provided for WSDL 1.1-based services which must be wrapped with part/type
     * information in order to travel over the NMR. The returned object
     * will return true when isFault() is called.
     * @param type QName identifying the message type
     * @return service message with the appropriate wrapper
     */
    public ServiceMessage createFault(ServiceMessage type) {
        try {
            return new ServiceMessageImpl(messageFactory_.createFault(), type);
        }
        catch (javax.jbi.messaging.MessagingException jbiEx) {
            // Highly unlikely and most definitely fatal, so throw a runtime ex
            throw new RuntimeException(jbiEx);
        }
    }

}
