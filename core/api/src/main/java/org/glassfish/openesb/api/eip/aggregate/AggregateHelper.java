/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AggregateHelper.java - Last published on May 21, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.eip.aggregate;

import java.util.HashMap;
import java.io.StringWriter;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import org.glassfish.openesb.api.message.MessageProps;
import org.glassfish.openesb.api.service.ServiceMessage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Aggregate Helper
 * 
 * @author Sun Microsystems
 */
public class AggregateHelper {
    
    private static String DEFAULT_CORR_ID = "aggregate-id";
    private static String COUNT           = "count";
    private static Transformer transformer_;        
    private static DocumentBuilder     builder_;
    private static String XML_HEADER = "<aggregated_messages>";
    private static String XML_TRAILER = "</aggregated_messages>";
    private static Logger log_ = Logger.getLogger("com.sun.jbi");
    

    /**
     * Determine the correlation id from the message and/or configuration. 
     * This returns the value of the Normalized Message property 
     * "org.glassfish.openesb.messaging.groupid"
     * 
     * @param message
     * @param config
     * @return
     */
    public static Object inferID(ServiceMessage message, HashMap config){
      String  corrId = (String)message.getProperty(MessageProps.GROUP_ID.toString());
      if ( corrId == null ) {
          corrId = DEFAULT_CORR_ID;
      }
      
      return corrId;
    }
    
    /**
     * This checks if the aggregate condition is satisfied, in this case
     * the "count" number or messages have been received.
     * 
     * @param correlationId - correlation id
     * @param serviceMessages - messages aggregated so far
     * @param config - aggregate configuration
     * @return true if the aggregate condition is met and all messages are received
     */
    public static boolean areAllMessagesReceived(Object correlationId, 
            ServiceMessage[] serviceMessages, HashMap config){
        
        int count = Integer.parseInt((String)config.get(COUNT));
        
        if ( serviceMessages.length == count ){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Aggregate the messages into one message.
     * 
     * @param corrId - correlation id
     * @param serviceMessages - messages to be aggregated
     * @param config - aggregate configuration
     * @return aggregated service message
     * @throws java.lang.Exception
     */
    public static Object aggregateMessages(Object corrId, ServiceMessage[] serviceMessages, HashMap config)
        throws Exception{
        Object document = null;
        StringWriter writer = null;
        boolean isXmlPayload = false;
        
        for ( ServiceMessage msg : serviceMessages){  
           
            Object obj = msg.getPayload();
            if ( writer == null ){
                writer = new StringWriter();
                if ( obj instanceof Node ){
                    writer.append(XML_HEADER);
                    isXmlPayload = true;
                }
            }
            
            if ( obj instanceof Node ){
                Node doc = (Node) obj;
                
                // Now append the node content as string
                writer.append(NodeToString(doc));
                isXmlPayload = true;
            }
            else if ( obj instanceof String ){
                writer.append((String)obj);
            }
        }
        
        // Finally add the trailer if one exists
        if ( isXmlPayload ){
            writer.append(XML_TRAILER);
        }

        if (log_.isLoggable(Level.FINER)) {
            log_.finer("The aggregated document being sent is : " + writer.toString());
        }
        
        try{
            document = createDocument(writer, isXmlPayload); 
        }catch(org.xml.sax.SAXParseException ex){
            String errMsg = "Aggregate failed to send message \n" 
                    + writer.toString() 
                    + "\n The  message is not a valid XML document";
            log_.severe(errMsg );
            throw new Exception(errMsg);
        }
        
        return document;
        
    }
        
    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private static String NodeToString(Node node)
        throws Exception{
        String nodeAsString = null;
        if ( node.getNodeType() == Node.ELEMENT_NODE){
            // xsd:anyType
            StringWriter sw = new StringWriter();
            getTransformer().transform(new DOMSource(node), new StreamResult(sw));
            nodeAsString = sw.toString();
        }
        else if ( node.getNodeType() == Node.TEXT_NODE ){
            // xsd:string
            nodeAsString = node.getNodeValue();
        }
        
        return nodeAsString;
    }
    
    /**
     * Create a message object from the combined message. If the isXML flag is 
     * true, the message content is XML
     * 
     * @param writer - StringWriter which has the message string 
     * @param isXml - flag if set to true it indicates that the message is an XML document
     */
    private static Object createDocument(StringWriter writer, boolean isXml)
        throws Exception {
        String stringPayload = writer.toString();
        
        if ( isXml ){
            return getBuilder().parse(new InputSource(new StringReader(stringPayload)));
        }else{
            return stringPayload;
        }
    }  
    
    /**
     * 
     * @return a DocumentBuilder instance
     * @throws java.lang.Exception
     */
    private static DocumentBuilder getBuilder() throws Exception{
        if (builder_ == null ){
            builder_ = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }
        return builder_;
    }
    
    /**
     * 
     * @return a Transformer instance
     * @throws java.lang.Exception
     */
    private static Transformer getTransformer() throws Exception{
        if ( transformer_ == null){
            transformer_ = TransformerFactory.newInstance().newTransformer();
            transformer_.setOutputProperty (
                    javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");
        }
        return transformer_;
    }
}
