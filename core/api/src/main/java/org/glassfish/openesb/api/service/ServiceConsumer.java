/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceChannel.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

/**
 * Friendly client interface to service endpoints exposed on the NMR.  The 
 * <code>call</code> method provides a synchronous call-and-return contract, 
 * while the <code>send</code> method provides an asynchronous contract.
 * @author kcbabo
 */
public interface ServiceConsumer {
    
    /**
     * Simple synchronous service invocation contract that maps to InOut.  
     * This method blocks until the service provider has returned.
     * @param operation operation to invoke
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public ServiceMessage invoke(ServiceMessage request, String operation) 
            throws Exception;
    
    
    /**
     * Simple synchronous service invocation contract that maps to InOut.  
     * This method blocks until the service provider has returned.
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public ServiceMessage invoke(ServiceMessage request) 
            throws Exception;
    
    
    /**
     * Simple synchronous service invocation contract that maps to InOnly.  
     * This method blocks until the service provider has returned.
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public void invokeOneWay(ServiceMessage request)
            throws Exception;
    
    /**
     * Simple synchronous service invocation contract that maps to InOnly.  
     * This method blocks until the service provider has returned.
     * @param operation operation to invoke
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public void invokeOneWay(ServiceMessage request, String operation)
            throws Exception;
    
    
    /**
     * Returns a ServiceMessageFactory instance which can be used to create
     * ServiceMessages.
     * @return ServiceMessageFactory
     */
    public ServiceMessageFactory getServiceMessageFactory();
}
