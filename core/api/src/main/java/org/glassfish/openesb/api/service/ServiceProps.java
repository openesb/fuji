/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceProps.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

/**
 *
 * @author kcbabo
 */
public enum ServiceProps {
    SERVICE_NAME("org.glassfish.openesb.serviceName"), 
    ENDPOINT_NAME("org.glassfish.openesb.endpointName"), 
    OPERATION_NAME("org.glassfish.openesb.operationName");

    /** The String value */
    private String val_;

    ServiceProps(String strValue)
    {
	val_ = strValue;
    }
    
    /**
     * @return the String value for the state
     */
    public String toString()
    {
        return val_;
    }
    
    /**
     * @return a ServiceProps based on the String value. 
     * @param valStr - the string whose equivalent ServiceProps
     *                 instance is required.
     */
    public static ServiceProps valueOfString(String valStr)
    {
        return ServiceProps.valueOf(valStr.toUpperCase());
    }
};
