/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceMessageImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.util.Set;
import javax.activation.DataHandler;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.security.auth.Subject;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Implementation of ServiceMessage interface that adapts to underlying 
 * NormalizedMessage reference.
 * @author kcbabo
 */
public class ServiceMessageImpl implements ServiceMessage {
    
    private static final String WSDL_WRAPPER_URI = 
            "http://java.sun.com/xml/ns/jbi/wsdl-11-wrapper";
    
    private static final String OBJECT_PAYLOAD_FLAG =
            "com.sun.jbi.fuji.objectPayload";

    public static  QName   ANYTYPE = new QName("http://www.w3.org/2001/XMLSchema", "anyType");
    public static  QName   STRINGTYPE = new QName("http://www.w3.org/2001/XMLSchema", "string");

 
    private NormalizedMessage   normalMsg_;
    private QName               messageType_;
    private Document            document_;
    private Element             wrapper_;
    private Element             part_;
    private boolean             wsdl_11_mode_;

    private static  DocumentBuilder        docBuilder_;
    private static  TransformerFactory     tFactory_;

    static {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            docBuilder_ = dbf.newDocumentBuilder();
            tFactory_ = TransformerFactory.newInstance();
         } catch (Exception e) {
            throw new RuntimeException("Can't initialize ServiceMessageImpl static state", e);
        }
    }

    /** 
     * Create a new ServiceMessage around the specified NormalizedMessage.
     * @param normalMsg service message wraps this message
     */
    public ServiceMessageImpl(NormalizedMessage normalMsg)
            throws javax.jbi.messaging.MessagingException {
        normalMsg_ = normalMsg;
        messageType_ = ANYTYPE;
        if (normalMsg_.getContent() != null) {
            setContent(normalMsg_.getContent());
        }
    }
    
    /**
     * Create a new ServiceMessage for the specified message type.  The 
     * resulting content will contain a WSDL 1.1 message wrapper consistent 
     * with the JSR 208 specification requirements for WSDL 1.1 messages.
     * @param normalMsg service message wraps this message
     * @param msgType type of the message
     */
    public ServiceMessageImpl(NormalizedMessage normalMsg, QName msgType)
            throws javax.jbi.messaging.MessagingException {
        this(normalMsg);
        createWrapper(msgType);
    }
    
    /**
     * Create a new ServiceMessage for the specified message type.  The
     * resulting content will contain a WSDL 1.1 message wrapper consistent
     * with the JSR 208 specification requirements for WSDL 1.1 messages.
     * @param normalMsg service message wraps this message
     * @param msgType type of the message
     */
    public ServiceMessageImpl(NormalizedMessage normalMsg, ServiceMessage msgType)
            throws javax.jbi.messaging.MessagingException {
        this(normalMsg);

        //
        //  See if we have a wrapper.
        //
        ServiceMessageImpl  msg = (ServiceMessageImpl)msgType;
        if (msg.wrapper_ != null) {
            //
            // Create a similar wrapper on the new SimpleMessage.
            //
            document_ = toDOM(null);

            document_.appendChild(wrapper_ = (Element)document_.importNode(msg.wrapper_, false));
            wrapper_.appendChild(part_ = (Element)document_.importNode(msg.part_, false));
            normalMsg_.setContent(new DOMSource(document_.getDocumentElement()));

        }
    }


    /** ServiceMessage methods **/
    
    /** 
     * Returns an object representation of the payload, provided that an 
     * object was specified as the content.  If the content is not an object,
     * then we just return NormalizedMessage.getContent().
     * @return message payload
     * @throws java.lang.Exception failed to deserialize payload object
     */
    public Object getPayload() throws Exception {
        Object payload = null;
        
        // Are we dealing with XML content here?
        if (!isObjectPayload()) {
            if (wrapper_ != null) {
                if (part_ != null) {
                    payload = part_.getFirstChild();
                    if (messageType_ == STRINGTYPE) {
                        payload = ((Node)payload).getTextContent();
                    }
                }
            } else {
                Source source = normalMsg_.getContent();

                if (source instanceof DOMSource) {
                    payload = ((DOMSource)source).getNode();
                } else if (source instanceof StreamSource) {
                    payload = ((StreamSource)source).getInputStream();
                }
            }
            return (payload);
        }
        // Get an object stream to yank out our payload
        InputStream is = ((StreamSource)normalMsg_.getContent()).getInputStream();
        ObjectInputStream ois = new ObjectInputStream(is);
        return ois.readObject();
    }

    /**
     * Specifies the message payload.  If the payload reference is a stream or
     * a TrAX Source object, we just set that directly on the underlying 
     * NormalizedMessage.  If the payload is an object, we create an object
     * stream and set that on the NormalizedMessage.
     * @param payload
     * @throws java.lang.Exception
     */
    public void setPayload(Object payload) throws Exception {
        
        if (payload instanceof InputStream) {
            normalMsg_.setContent(new StreamSource((InputStream)payload));
            normalMsg_.setProperty(OBJECT_PAYLOAD_FLAG, 
                    payload.getClass().getName());
            wrapper_ = null;
            part_ = null;
        } else if (payload instanceof Source) {
            setContent((Source)payload);
        } else if (payload instanceof Node) {
            setXmlPayload((Node)payload);
        } else if (payload instanceof String) {
            setStringPayload((String)payload);
        } else {
            // Serialize to a byte array
            StreamBuffer sh = new StreamBuffer();
            ObjectOutputStream oos = new ObjectOutputStream(sh);
            oos.writeObject(payload);
            oos.flush();

            // Create an object stream pointer
            InputStream    is = sh.asInputStream();
            normalMsg_.setContent(new StreamSource(is));
            normalMsg_.setProperty(OBJECT_PAYLOAD_FLAG, 
                    payload.getClass().getName());
        }
    }
    
    public boolean isFault() {
        return (normalMsg_ instanceof Fault);
    }

    public QName getMessageType() {
        return (messageType_);
    }

    public void setMessageType(QName messageType) {
        messageType_ = messageType;
        if (wrapper_ != null) {
            wrapper_.setAttribute("type", "msgns:" + messageType.getLocalPart());
            wrapper_.setAttribute("xmlns:msgns", messageType.getNamespaceURI());
        }
    }

    public NodeList getParts() {
        NodeList    parts = null;

        if (wrapper_ != null) {
            parts = wrapper_.getElementsByTagNameNS(WSDL_WRAPPER_URI, "part");
        }
        return (parts);
    }

    public void setWSDL11WrapperMode() {
        wsdl_11_mode_ = true;
    }

    public void setWSDL20Mode() {
        wsdl_11_mode_ = false;
    }
    /** NormalizedMessage methods **/

    public void addAttachment(String arg0, DataHandler arg1) throws MessagingException {
        normalMsg_.addAttachment(arg0, arg1);
    }

    public DataHandler getAttachment(String arg0) {
        return normalMsg_.getAttachment(arg0);
    }

    public Set getAttachmentNames() {
        return normalMsg_.getAttachmentNames();
    }

    public Source getContent() {
        return normalMsg_.getContent();
    }

    public Object getProperty(String arg0) {
        return normalMsg_.getProperty(arg0);
    }

    public Set getPropertyNames() {
        return normalMsg_.getPropertyNames();
    }

    public Subject getSecuritySubject() {
        return normalMsg_.getSecuritySubject();
    }

    public void removeAttachment(String arg0) throws MessagingException {
        normalMsg_.removeAttachment(arg0);
    }

    public void setProperty(String arg0, Object arg1) {
        normalMsg_.setProperty(arg0, arg1);
    }

    public void setSecuritySubject(Subject arg0) {
        normalMsg_.setSecuritySubject(arg0);
    }
    
    /**
     * Returns the underlying NM representation of this ServiceMessage.
     * @return normalized message reference
     */
    public NormalizedMessage getNormalizedMessage() {
        return normalMsg_;
    }
        
    public void setContent(Source content) throws MessagingException {
        if (content instanceof StreamSource) {
            content = new DOMSource(toDOM(content));
        }
        if (content instanceof DOMSource) {
            Node        payload = ((DOMSource)content).getNode();
            Element     message = null;
            Document    document = null;

            if (payload.getNodeType() == Node.DOCUMENT_NODE) {
                message = ((Document)payload).getDocumentElement();
                document = (Document)payload;
            } else if (payload.getNodeType() == Node.ELEMENT_NODE) {
                message = (Element)payload;
                document = message.getOwnerDocument();
            }
            String  nsURI = message.getNamespaceURI();

            if (nsURI != null && nsURI.equals(WSDL_WRAPPER_URI) &&
                    message.getLocalName().equals("message")) {
                NodeList parts = message.getElementsByTagNameNS(WSDL_WRAPPER_URI, "part");
                if (parts.getLength() > 0) {
                    document_ = document;
                    wrapper_ = message;
                    part_ = (Element)parts.item(0);
                    normalMsg_.setContent(content);
                    wsdl_11_mode_ = true;

                    String type = "anyType";
                    String ns = "http://www.w3.org/2001/XMLSchema";
                    String name = wrapper_.getAttribute("type");
                    if (name != null) {
                        String[] pieces = name.split(":");

                        if (pieces.length == 2) {
                            type = pieces[1];
                            ns = wrapper_.getAttribute("xmlns:" + pieces[0]);
                        }
                    }
                    messageType_ = new QName(ns, type);
                    return;
                }
            }
        }
        removeObjectPayload();
        document_ = null;
        wrapper_ = null;
        part_ = null;
        normalMsg_.setContent(content);
        wsdl_11_mode_ = false;
    }

    private void setXmlPayload(Source source) throws Exception {
        //
        //  See if we have any existing content.
        //
        if (wrapper_ != null && part_ != null) {
            Node payload;

            if (source instanceof DOMSource) {
                    payload = ((DOMSource)source).getNode();
            }
            else { // looks like we have to massage the source a bit
                payload = toDOM(source).getDocumentElement();
            }
            Node child = part_.getFirstChild();
            if (child != null) {
                part_.removeChild(child);
            }
            part_.appendChild(document_.importNode(payload, true));
//            setMessageType(ANYTYPE);
            return;
        } else {
            removeObjectPayload();
        }
        setContent(source);
   }

    private void setXmlPayload(Node payload) throws Exception {
        //
        // If we already have some content, see if it's wrapped.
        //
        if (wrapper_ != null && part_ != null) {
            if (payload.getNodeType() == Node.DOCUMENT_NODE) {
                payload = ((Document)payload).getDocumentElement();
            }
            Node child = part_.getFirstChild();
            if (child != null) {
                part_.removeChild(child);
            }
            part_.appendChild(document_.importNode(payload, true));
//            setMessageType(ANYTYPE);
            return;

        } else {
            removeObjectPayload();
        }
        if (payload.getNodeType() != Node.DOCUMENT_NODE) {
            Document doc = toDOM(null);
            doc.appendChild(doc.importNode(payload, true));
            payload = doc;
        }
        setContent(new DOMSource(payload));
   }

    private void setStringPayload(String payload) throws Exception {
        //
        // If we already have some content, see if it's wrapped.
        //
        if (document_ == null) {
            createWrapper(STRINGTYPE);
        } else {
            wrapper_.setAttribute("xmlns:msgns", STRINGTYPE.getNamespaceURI());
            wrapper_.setAttribute("type", "msgns:" + STRINGTYPE.getLocalPart());
        }
        part_.appendChild(document_.createTextNode(payload));
        messageType_ = STRINGTYPE;
        return;
    }

//    private Node    getWrappedPart(Document doc) {
//        Element message = doc.getDocumentElement();
//        String  nsURI = message.getNamespaceURI();
//        String  prefix = message.getPrefix();
//        Node    part = null;
//
//        //
//        //  See if we have a wrapper
//        //
//        if (nsURI != null && nsURI.equals(WSDL_WRAPPER_URI) &&
//            message.getLocalName().equals("message")) {
//            //
//            // Yep, it's wrapped.  Dig out the part element
//            //
//            NodeList parts = message.getElementsByTagNameNS(WSDL_WRAPPER_URI, "part");
//            if (parts.getLength() > 0) {
//                part = parts.item(0);
//                Node child = part.getFirstChild();
//                if (child != null) {
//                    part.removeChild(child);
//                }
//            } else {
//                if (prefix == null) {
//                    part = doc.createElementNS(WSDL_WRAPPER_URI, "part");
//                } else {
//                    part = doc.createElementNS(WSDL_WRAPPER_URI, prefix+":part");
//                }
//                message.appendChild(part);
//            }
//        }
//        return (part);
//    }

    /**
     * Create a new ServiceMessage based on the prototype.
     * The contents of the properties and the WSDL 1.1 wrapper information
     * is transfered. The payload is left empty.
     * @param sm
     */
    public ServiceMessageImpl getPrototype(MessageExchange me) {
        ServiceMessageImpl  sm = null;

        try
        {
            if (messageType_ == null)
            {
                sm = new ServiceMessageImpl(me.createMessage(), this);
            } else {
                sm = new ServiceMessageImpl(me.createMessage(), messageType_);
            }

            //
            //  Add attachments and properties.
            //
            for (Object a : getAttachmentNames()) {
                sm.addAttachment((String)a, getAttachment((String)a));
            }
            for (Object a : getPropertyNames()) {
                sm.setProperty((String)a, getProperty((String)a));
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx) {

        }
        return (sm);
    }

    /**
     * Used to determine if this message carries an Object payload.
     * @return true if the message carries an Object payload, false otherwise
     */
    private boolean isObjectPayload() {
        return normalMsg_.getProperty(OBJECT_PAYLOAD_FLAG) != null;
    }
    
    /**
     * If the message carries an object payload, this method removes it and
     * clears the object payload header.
     */
    private void removeObjectPayload() throws MessagingException {
        if (isObjectPayload()) {
            normalMsg_.setProperty(OBJECT_PAYLOAD_FLAG, null);
            normalMsg_.setContent(null);
        }
    }

    private void createWrapper(QName msgType) throws javax.jbi.messaging.MessagingException {
        document_ = toDOM(null);
        Element msgElement = document_.createElementNS(WSDL_WRAPPER_URI, "jbi:message");
        Element msgPart = document_.createElementNS(WSDL_WRAPPER_URI, "jbi:part");

        //
        //  Create WSDL 1.1 Wrapper and Part Elements.
        //
        msgElement.setAttribute("version", "1.0");
        msgElement.setAttribute("xmlns:msgns", msgType.getNamespaceURI());
        msgElement.setAttribute("xmlns:jbi", WSDL_WRAPPER_URI);
        msgElement.setAttribute("type", "msgns:" + msgType.getLocalPart());
        document_.appendChild(msgElement);
        msgElement.appendChild(msgPart);
        messageType_ = msgType;
        wrapper_ = msgElement;
        part_ = msgPart;
        normalMsg_.setContent(new DOMSource(document_));
    }

//    private String nodeToString(Node node)
//        throws Exception
//    {
//        String nodeAsString = null;
//        TransformerFactory tf = TransformerFactory.newInstance();
//        Transformer transformer = tf.newTransformer();
//
//        if ( node.getNodeType() == Node.ELEMENT_NODE)
//        {
//            // xsd:anyType
//            StringWriter sw = new StringWriter();
//            transformer.transform(new DOMSource(node), new StreamResult(sw));
//            nodeAsString = sw.toString();
//        }
//        else if ( node.getNodeType() == Node.TEXT_NODE )
//        {
//            // xsd:string
//            nodeAsString = node.getNodeValue();
//        }
//
//        return nodeAsString;
//    }

    private Document toDOM(Source source)
            throws javax.jbi.messaging.MessagingException {
        Document        doc = null;

        try {
            if (source != null)
            {
                DOMResult result = new DOMResult();

                tFactory_.newTransformer().transform(source, result);
                doc = (Document)result.getNode();
            } else {
                doc = docBuilder_.newDocument();
            }
        }
        catch (Exception ex) {
            throw new javax.jbi.messaging.MessagingException (ex);
        }

        return doc;

    }

    public String toString() {
        StringBuffer    sb = new StringBuffer();
        sb.append("            ServiceMessage Type: " + ((messageType_ == null) ? "Null" : messageType_.toString()) + "\n");
        if (normalMsg_ != null)
            sb.append(normalMsg_.toString());
        return (sb.toString());
    }

}
