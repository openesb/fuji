/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceProvider.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

/**
 * This interface represents a simple service provisioning contract with the
 * JBI runtime environment.  The invoke method is called when a message 
 * exchange is addressed to this service instance.  Classes implementing this
 * interface must be registered in the OSGi service registry in order to be
 * available to consumers in the JBI runtime environment.  Further, the 
 * service registration must contain a property called "jbi.service.name" which
 * contains the name of the service being offered.  Optionally, a property
 * called "jbi.endpoint.name" can also be specified.
 * @author kcbabo
 */
public interface ServiceProvider {
    
    
    /**
     * This method is called every time a consumer initiates a message exchange 
     * with the service provider.
     * @param message request message
     * @return response message, or null if there is no response
     * @throws java.lang.Exception if an exception is thrown, the message
     * exchange is marked with an ERROR status and returned to the service
     * consumer.
     */
    ServiceMessage invoke(ServiceMessage message) throws Exception;
    
    /**
     * Setter for factory used to create ServiceMessages.  This setter is called
     * once when the framework detects the service provider in the OSGi 
     * registry.  It is guaranteed that this method will be called before the
     * first call to invoke().  ServiceProvider instances can simply NOP this 
     * method if they do not need to create response messages. 
     * @param factory factory used to create ServiceMessages
     */
    void setMessageFactory(ServiceMessageFactory factory);
    
    
}
