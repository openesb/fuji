/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceChannelImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

import java.util.Dictionary;
import java.util.Properties;
import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;

/**
 * Prototype implementation of ServiceChannel.  At this point, only InOut MEPs
 * are supported.
 * 
 * @author kcbabo
 */
public class ServiceConsumerImpl 
        implements org.glassfish.openesb.api.service.ServiceConsumer {
    
    /** Client's component context that sits behind this service channel */
    private ComponentContext context_;
    /** NMR delivery channel */
    private DeliveryChannel deliveryChannel_;
    /** Message exchange factory */
    private MessageExchangeFactory exchangeFactory_;
    /** Service endpoint that this channel points to. */
    private ServiceEndpoint endpoint_;
    /** Service properties */
    private Dictionary serviceProperties_;
    /** Used to create ServiceMessage instances */
    private ServiceMessageFactory msgFactory_;
    
    /**
     * Create a new service channel for a service endpoint.
     * @param deliveryChannel NMR delivery channel
     * @param endpoint NMR service endpoint
     */
    public ServiceConsumerImpl(DeliveryChannel channel, ServiceEndpoint endpoint)
            throws Exception {
        deliveryChannel_    = channel;
        endpoint_           = endpoint;
        exchangeFactory_    = deliveryChannel_.createExchangeFactory();
        msgFactory_         = new ServiceMessageFactoryImpl(exchangeFactory_);
        
        // Create the service properties
        serviceProperties_ = new Properties();
        serviceProperties_.put(ServiceProps.SERVICE_NAME.toString(),
                endpoint.getServiceName().toString());
        serviceProperties_.put(ServiceProps.ENDPOINT_NAME.toString(), 
                endpoint.getEndpointName());
    }
    
    /**
     * Simple synchronous service invocation contract that maps to InOut.  
     * This method blocks until the service provider has returned.
     * @param operation operation to invoke
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public ServiceMessage invoke(ServiceMessage request, String operation) 
            throws Exception {
        
        // Create an exchange and set the message + properties
        InOut exchange = exchangeFactory_.createInOutExchange();
        exchange.setEndpoint(endpoint_);
        if (operation != null) {
            exchange.setOperation(new QName(operation));
        }
        exchange.setInMessage(
                ((ServiceMessageImpl)request).getNormalizedMessage());
        
        // Handle the NMR communication
        deliveryChannel_.sendSync(exchange);
        
        // Return the out message
        NormalizedMessage   nm = exchange.getOutMessage();
        ServiceMessage      sm = null;

        if (nm != null) {
            sm = new ServiceMessageImpl(nm);
        }
        if (exchange.getStatus().equals(ExchangeStatus.ACTIVE)) {
            exchange.setStatus(ExchangeStatus.DONE);
            deliveryChannel_.send(exchange);
        }
        return (sm);
    }
    
    /**
     * Simple synchronous service invocation contract that maps to InOut.  
     * This method blocks until the service provider has returned.
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public ServiceMessage invoke(ServiceMessage request) 
            throws Exception {
        
        return invoke(request, null);
    }
    
    /**
     * Simple synchronous service invocation contract that maps to InOnly.  
     * This method blocks until the service provider has returned.
     * @param operation operation to invoke
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public void invokeOneWay(ServiceMessage request, String operation) throws Exception {
        // Create an exchange and set the message + properties
        InOnly exchange = exchangeFactory_.createInOnlyExchange();
        exchange.setEndpoint(endpoint_);
        if (operation != null) {
            exchange.setOperation(new QName(operation));
        }
        exchange.setInMessage(
                ((ServiceMessageImpl)request).getNormalizedMessage());
        
        // Handle the NMR communication
        deliveryChannel_.sendSync(exchange); 
    }
    
    /**
     * Simple synchronous service invocation contract that maps to InOnly.  
     * This method blocks until the service provider has returned.
     * @param operation operation to invoke
     * @param request the request message
     * @return the response fault/message, or null if (a) the pattern does not 
     *  support it or the provider did not return a response.
     * @throws java.lang.Exception something bad happened
     */
    public void invokeOneWay(ServiceMessage request) throws Exception {
        invokeOneWay(request, null);
    }
    
    /**
     * Returns a ServiceMessageFactory instance which can be used to create
     * ServiceMessages.
     * @return ServiceMessageFactory
     */
    public ServiceMessageFactory getServiceMessageFactory() {
        return msgFactory_;
    }
    
    /**
     * Return the property set for this service.  Properties are bound in the
     * OSGi service registry along with the service interface name.
     * @return service properties
     */
    public Dictionary getProperties() {
        return serviceProperties_;
    }
    
}
