/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceMessage.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.service;

import javax.jbi.messaging.NormalizedMessage;

/**
 * Extension of JBI NormalizedMessage construct which allows for a friendlier
 * application client interface.  For now, ServiceMessage is simply a way to
 * pass Objects around on the NMR without serializing them into XML.
 * <br><br>
 * Note that ServiceMessage represents normal message as well as faults.  From
 * a JBI and WSDL perspective there is no signifcant difference between the 
 * two other than the fact that they play different roles semantically.  Use
 * the isFault() method to check if a ServiceMessage is an instance of a fault.
 * 
 * @author kcbabo
 */
public interface ServiceMessage extends NormalizedMessage {
    
    /** 
     * Sets the content of the message.
     * @param payload message content
     * @throws Exception problem serializing payload
     */
    void setPayload(Object payload) throws Exception;
    
    /**
     * Returns the content of the message.
     * @return message content; null if no object content has been specified.
     * @throws Exception problem deserializing payload
     */
    Object getPayload() throws Exception;
    
    /**
     * Returns true if this message represents a fault.
     * @return true is this is a fault, false otherwise
     */
    boolean isFault();
}
