/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Aggregate.java - Last published on Mar 31, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.eip.aggregate;

import org.glassfish.openesb.api.service.ServiceMessage;

import java.util.Properties;

/**
 * This interface defines the SPI for an Aggregate type
 *
 * @author Sun Microsystems
 */
public interface Aggregate {
        
    /**
     * Set the configuration for the type
     * 
     * @param config
     */
    void setConfiguration(Properties config)
        throws Exception;
    
    /**
     * 
     * @return the Correlation ID from the message
     */
    public Object getCorrelationID(ServiceMessage message);

    /**
     *  @return true if all the conditions required to aggregate the message have
     *          been satisfied.
     */
    boolean isAggregateComplete(Object correlationId, ServiceMessage[] serviceMessages);
    
    /**
     * Aggregate the messages being passed in, into one service message
     * 
     * @param corrId  - correlation ID 
     * @param serviceMessages - the messages to aggregate
     */
    Object aggregateMessages(Object corrId, ServiceMessage[] serviceMessages)
        throws Exception;
}
