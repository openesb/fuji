/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageProps.java - Last published on 3/4/2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.api.message;

/**
 *
 * @author Sun Microsystems
 */
public enum MessageProps {
    
    GROUP_ID("org.glassfish.openesb.messaging.groupid"),
    MESSAGE_ID("org.glassfish.openesb.messaging.messageid"),
    MESSAGE_CT("org.glassfish.openesb.messaging.finalmessageid"),
    MESSAGE_TYPE("org.glassfish.openesb.messaging.messageidtype"),
    MESSAGE_TS("org.glassfish.openesb.messaging.messagetimestamp"),
    PARENT("org.glassfish.openesb.messaging.parentid");

    /** The String value */
    private String val_;

    MessageProps(String strValue)
    {
	val_ = strValue;
    }
    
    /**
     * @return the String value for the property
     */
    public String toString()
    {
        return val_;
    }
    
    /**
     * @return a MessageProps based on the String value. 
     * @param valStr - the string whose equivalent MessageProps
     *                 instance is required.
     */
    public static MessageProps valueOfString(String valStr)
    {
        return MessageProps.valueOf(valStr.toUpperCase());
    }
};
