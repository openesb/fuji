/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FlowTest.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for Flow enum.
 */
public class FlowTest
    extends TestCase {
    
    private static final String AGGREGATE   = "aggregate";
    private static final String SPLIT       = "split";
    private static final String SELECT      = "select";
    private static final String BROADCAST   = "broadcast";
    private static final String FILTER      = "filter";
    private static final String TEE         = "tee";

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FlowTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FlowTest.class);
    }

    public void testAggregateValueOf() throws Exception {
        assertEquals(Flow.fromString(AGGREGATE), Flow.AGGREGATE);
        assertEquals(Flow.fromString(Flow.AGGREGATE.toString()), Flow.AGGREGATE);
    }
    
    public void testSplitValueOf() throws Exception {
        assertEquals(Flow.fromString(SPLIT), Flow.SPLIT);
        assertEquals(Flow.fromString(Flow.SPLIT.toString()), Flow.SPLIT);
    }

    public void testSelectValueOf() throws Exception {
        assertEquals(Flow.fromString(SELECT), Flow.SELECT);
        assertEquals(Flow.fromString(Flow.SELECT.toString()), Flow.SELECT);
    }
    
    public void testBroadcastValueOf() throws Exception {
        assertEquals(Flow.fromString(BROADCAST), Flow.BROADCAST);
        assertEquals(Flow.fromString(Flow.BROADCAST.toString()), Flow.BROADCAST);
    }
    
    public void testFilterValueOf() throws Exception {
        assertEquals(Flow.fromString(FILTER), Flow.FILTER);
        assertEquals(Flow.fromString(Flow.FILTER.toString()), Flow.FILTER);
    }
    
    public void testTeeValueOf() throws Exception {
        assertEquals(Flow.fromString(TEE), Flow.TEE);
        assertEquals(Flow.fromString(Flow.TEE.toString()), Flow.TEE);
    }
}
