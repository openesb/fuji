/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying informatiFon: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLModelTest.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.parser.IFLParsedModel;
import com.sun.jbi.fuji.ifl.parser.IFLParsedObjectModel;
import com.sun.jbi.fuji.ifl.parser.IFLParser;

import com.sun.jbi.fuji.ifl.parser.IFLRoutes;
import com.sun.jbi.fuji.ifl.parser.Route;

import com.sun.jbi.fuji.ifl.parser.IFLServices;
import com.sun.jbi.fuji.ifl.parser.Service;

import com.sun.jbi.fuji.ifl.parser.IFLServiceGroups;
import com.sun.jbi.fuji.ifl.parser.ServiceGroup;

import com.sun.jbi.fuji.ifl.parser.IFLConnections;
import com.sun.jbi.fuji.ifl.parser.Connection;

import com.sun.jbi.fuji.ifl.parser.IFLBroadcasts;
import com.sun.jbi.fuji.ifl.parser.Broadcast;

import com.sun.jbi.fuji.ifl.parser.IFLTees;
import com.sun.jbi.fuji.ifl.parser.Tee;

import com.sun.jbi.fuji.ifl.parser.IFLSelects;
import com.sun.jbi.fuji.ifl.parser.Select;

import com.sun.jbi.fuji.ifl.parser.IFLSplits;
import com.sun.jbi.fuji.ifl.parser.Split;

import com.sun.jbi.fuji.ifl.parser.IFLFilters;
import com.sun.jbi.fuji.ifl.parser.Filter;

import com.sun.jbi.fuji.ifl.parser.IFLAggregates;
import com.sun.jbi.fuji.ifl.parser.Aggregate;

import com.sun.jbi.fuji.ifl.parser.IFLNamespaces;
import com.sun.jbi.fuji.ifl.parser.Namespace;

/**
 * Unit test for IFLModel.
 */
public class IFLObjectModelTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public IFLObjectModelTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(IFLObjectModelTest.class);
    }


    /**
     * return File object with test data dir path appended.
     * @param relFilePath relative path w.r.t. src/test/data
     * @return
     */
    private File getTestIFLFile(String relFilePath) {

        String basedir = System.getProperty("basedir");
        String iflFilePath = "src/test/data/" + relFilePath; // relative to basedir;
        File iflFile = new File(basedir, iflFilePath);
        return iflFile;
    }

    
    public void testInLineRoutes() throws Exception
    {
        File iflFile = getTestIFLFile("line-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        int totalRoutes = routes.getCount();
        assertEquals("totalRoutes", 3, totalRoutes);

        Route route1 = routes.getRoute("route$1");
        String route1Name            = route1.getName();
        int route1TotalEntries       = route1.getCount();
        boolean route1IsProvided     = route1.isProvided();
        String route1FromServiceName = route1.getFromServiceName();

        IFLServices services = model.getIFLServices();
        //System.out.println (services);  // Keep in for debugging 

        Service route1FromService = services.getService(route1FromServiceName);
        boolean fromServiceIsConsumed = route1FromService.isConsumed();
        Service bar = services.getService("bar");
        boolean barServiceIsProvided = bar.isProvided();
        Service xyz = services.getService("xyz");
        boolean xyzServiceIsConsumed = xyz.isConsumed();
        Service abc = services.getService("abc");
        boolean abcServiceIsProvided = abc.isProvided();

        assertEquals("route1Name", "foo", route1Name);
        assertEquals("route1TotalEntries", 1, route1TotalEntries);
        assertTrue("route1IsProvided", route1IsProvided);
        assertTrue("fromServiceIsConsumed", fromServiceIsConsumed);
        assertTrue("barServiceIsProvided", barServiceIsProvided);
        assertTrue("xyzServiceIsConsumed", xyzServiceIsConsumed);
        assertTrue("abcServiceIsProvided", abcServiceIsProvided);

        IFLConnections connections = model.getIFLConnections();
        //System.out.println (connections);  // Keep in for debugging

        int totalConnections = connections.getCount();
        assertEquals("route1TotalEntries", 1, route1TotalEntries);

        Connection route1Connection = connections.getConnection("route$1");
        List route1ConnectionEntries = route1Connection.getEntries();
        int totalRoute1ConnectionEntries = route1ConnectionEntries.size();

        Connection route3Connection = connections.getConnection("route$3");
        List route3ConnectionEntries = route3Connection.getEntries();
        int totalRoute3ConnectionEntries = route3ConnectionEntries.size();

        assertEquals("totalRoute1ConnectionEntries", 2, totalRoute1ConnectionEntries);
        assertEquals("totalRoute3ConnectionEntries", 1, totalRoute3ConnectionEntries);
    }
    

    public void testMultiRoutes() throws Exception 
    {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        int totalRoutes = routes.getCount();
        assertEquals("totalRoutes", 5, totalRoutes);

        List names = routes.getNames();
        String strExpectedValue = "[finbound, hinbound1, notknown, hinbound2, hinbound3]";
        assertEquals("routes.getNames()", strExpectedValue, names.toString().trim());
        
        Map<String,Route> routeMap = routes.getMap();
        Route route4_1 = routeMap.get("route$4");

        int totalEntries1 = route4_1.getCount();
        List<Route.RouteEntry> entries1 = route4_1.getRouteEntries();
        
        Route route4_2 = routes.getRoute("route$4");
        int totalEntries2 = route4_2.getCount();
        List<Route.RouteEntry> entries2 = route4_2.getRouteEntries(); 

        Route.RouteEntry entry1 = entries1.get(1);
        Route.RouteEntry entry2 = entries2.get(1);

        String routePath1 = entry1.getRoutePath();
        String routePath2 = entry2.getRoutePath();

        assertEquals("routePath1", "bprocess", routePath1);
        assertEquals("totalEntries1", 2, totalEntries1);
        assertEquals("totalEntries2", totalEntries1, totalEntries2);
        assertEquals("routePath2", routePath1, routePath2);

        Route route5 = routes.getRoute("route$5");
        String route5Name = route5.getName();
        assertEquals("route5Name", "hinbound3", route5Name);

        String filename = route5.getFilename();
        int index = filename.indexOf("multi-routes.ifl");
        assertTrue("route5.getFilename() failed",(index > -1));

    }


    public void testNamespaceRoute() throws Exception {
        File file1 = getTestIFLFile("namespace-test2-positive.ifl");
        File file2 = getTestIFLFile("shared-namespaces.ifl");
        File[] fileList = new File[] { file1, file2 };
        IFLModel model = (new IFLReader()).read(fileList);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        int totalRoutes = routes.getCount();
        assertEquals("totalRoutes", 2, totalRoutes);

        List names = routes.getNames();
        Collections.sort(names);
        String expectedValue = "[infile, myInfile]";
        assertEquals("routes.getNames()", expectedValue, names.toString().trim());

        Route route1 = routes.getRoute("route$1");
        String route1Name            = route1.getName();
        int route1TotalEntries       = route1.getCount();
        boolean route1IsProvided     = route1.isProvided();
        String route1FromServiceName = route1.getFromServiceName();

        assertEquals("route1Name", "infile", route1Name);        
        assertEquals("route1TotalEntries", 6, route1TotalEntries);  
        assertTrue("route1IsProvided", route1IsProvided);
        assertEquals("route1FromServiceName", "infile", route1FromServiceName);  

        List<Route.RouteEntry> routeEntries = route1.getRouteEntries();
        Route.RouteEntry entry1 = routeEntries.get(1);
        String routePath = entry1.getRoutePath();
        String namespace = entry1.getNamespace();
        String respovedNamespace = entry1.getResolvedNamespace();

        assertEquals("routePath", "convert-order2", routePath);        
        assertEquals("namespace", "app4", namespace);        
        assertEquals("respovedNamespace", "http://fuji.dev.java.net/application/my-app/private/app4", respovedNamespace);        
        
        Route.RouteEntry entry2 = routeEntries.get(2);
        routePath = entry2.getRoutePath();
        namespace = entry2.getNamespace();
        respovedNamespace = entry2.getResolvedNamespace();

        assertEquals("routePath", "convert-order3", routePath);        
        assertEquals("namespace", "http://fuji.dev.java.net/application/Application_3", namespace);        
        assertEquals("respovedNamespace", "http://fuji.dev.java.net/application/Application_3", respovedNamespace);        

        Route.RouteEntry entry3 = routeEntries.get(3);
        routePath = entry3.getRoutePath();
        namespace = entry3.getNamespace();
        respovedNamespace = entry3.getResolvedNamespace();

        assertEquals("routePath", "convert-order4", routePath);        
        assertEquals("namespace", "Application_3", namespace);        
        assertEquals("respovedNamespace", "http://fuji.dev.java.net/application/my-app/private/Application_3", respovedNamespace);        
    }

    public void testGroupServices() throws Exception {

        File iflFile = getTestIFLFile("group-services-positive.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLServiceGroups serviceGroups = model.getIFLServiceGroups();
        //System.out.println (serviceGroups);  // Keep in for debugging

        List<String> serviceGroupNames = serviceGroups.getNames();
        String strExpectedValue = "[myInfile, myInfile2, myOutfile, myBPEL, myBPEL2, myBPEL3]";
        assertEquals("serviceGroups.getNames()", strExpectedValue, serviceGroupNames.toString().trim());        

        ServiceGroup serviceGroup = serviceGroups.getServiceGroup("myBPEL2");
        List<String> consumedServiceNames = serviceGroup.getConsumedNames();
        strExpectedValue = "[svc10, svc20]";
        assertEquals("serviceGroup.getConsumedNames()", strExpectedValue, consumedServiceNames.toString().trim());        

        List<String> providedServiceNames = serviceGroup.getProvidedNames();
        strExpectedValue = "[bpel10, bpel20]";
        assertEquals("serviceGroup.getProvidedNames()", strExpectedValue, providedServiceNames.toString().trim());        

        IFLServices services = model.getIFLServices();
        Service service = services.getService("svc20");
        String serviceGroupName = service.getGroupName();
        strExpectedValue = "myBPEL2";
        assertEquals("serviceGroupName", strExpectedValue, serviceGroupName);   

        serviceGroup = serviceGroups.getServiceGroup("myInfile");
        consumedServiceNames = serviceGroup.getConsumedNames();
        strExpectedValue = "[myInfile]";
        assertEquals("consumedServiceNames", strExpectedValue, consumedServiceNames.toString().trim());        

        providedServiceNames = serviceGroup.getProvidedNames();
        strExpectedValue = "[]";
        assertEquals("providedServiceNames", strExpectedValue, providedServiceNames.toString().trim());        
    }

    
    public void testCallServices() throws Exception {

        File iflFile = getTestIFLFile("call-services-positive.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLServices services = model.getIFLServices();
        //System.out.println (services);

        IFLServiceGroups serviceGroups = model.getIFLServiceGroups();
        //System.out.println (serviceGroups);  // Keep in for debugging

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        List<String> serviceNames = services.getNames();
        String expectedValue = "[buy-something, convert-format, transform1, clean-data, transform2, bpel10, bpel20, svc10, svc20]";
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());        

        List<String> serviceGroupNames = serviceGroups.getNames();
        expectedValue = "[buy-something, transform1, transform2, myBPEL2]";
        assertEquals("serviceGroupNames", expectedValue, serviceGroupNames.toString().trim());        

        List<String> routeNames = routes.getNames();
        expectedValue = "[my-soap-service, another-soap-service, convert-format, clean-data]";
        assertEquals("routeNames", expectedValue, routeNames.toString().trim());        

        ServiceGroup serviceGroup = serviceGroups.getServiceGroup("transform2");
        List<String> consumedNames = serviceGroup.getConsumedNames();
        expectedValue = "[convert-format, clean-data]";
        assertEquals("consumedNames", expectedValue, consumedNames.toString().trim());        

        List<String> providedNames = serviceGroup.getProvidedNames();
        expectedValue = "[transform2]";
        assertEquals("providedNames", expectedValue, providedNames.toString().trim());   

        List<String> callNames = serviceGroup.getCallServiceNames();
        expectedValue = "[convert-format, clean-data]";
        assertEquals("callNames", expectedValue, callNames.toString().trim());   
    }


    public void testServiceGroupAndCall() throws Exception {
        File iflFile = getTestIFLFile("group-and-call-services.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLServices services = model.getIFLServices();
        //System.out.println (services);

        IFLServiceGroups serviceGroups = model.getIFLServiceGroups();
        //System.out.println (serviceGroups);  // Keep in for debugging

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        List<String> javaServiceNames = services.getServiceNamesByType("java");
        Collections.sort(javaServiceNames);
        String expectedValue = "[buy-something, clean-data, convert-format1, convert-format2, transform1, transform2]";
        assertEquals("javaServiceNames", expectedValue, javaServiceNames.toString().trim());        

        List<String> bpelServiceNames = services.getServiceNamesByType("bpel");
        Collections.sort(bpelServiceNames);
        expectedValue = "[bpel11, bpel12, bpel21, bpel22, c21, c22]";
        assertEquals("bpelServiceNames", expectedValue, bpelServiceNames.toString().trim());        

        List<String> serviceGroupNames = serviceGroups.getNames();
        Collections.sort(serviceGroupNames);
        expectedValue = "[buy-something, myBPEL1, myBPEL2, transform1, transform2]";
        assertEquals("serviceGroupNames", expectedValue, serviceGroupNames.toString().trim());    

        ServiceGroup serviceGroup = serviceGroups.getServiceGroup("buy-something");
        List<String> serviceNames = serviceGroup.getServiceNames();
        expectedValue = "[buy-something]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("myBPEL1");
        serviceNames = serviceGroup.getServiceNames();
        expectedValue = "[bpel11, bpel12]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("myBPEL2");
        serviceNames = serviceGroup.getServiceNames();
        expectedValue = "[bpel21, bpel22, c21, c22]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("transform1");
        serviceNames = serviceGroup.getServiceNames();
        expectedValue = "[convert-format1, transform1]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("transform2");
        serviceNames = serviceGroup.getServiceNames();
        expectedValue = "[clean-data, convert-format2, transform2]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("transform2");
        serviceNames = serviceGroup.getConsumedNames();
        expectedValue = "[clean-data, convert-format2]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("transform2");
        serviceNames = serviceGroup.getCallServiceNames();
        expectedValue = "[clean-data, convert-format2]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("buy-something");
        serviceNames = serviceGroup.getConsumedNames();
        expectedValue = "[buy-something]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());

        serviceGroup = serviceGroups.getServiceGroup("buy-something");
        serviceNames = serviceGroup.getCallServiceNames();
        expectedValue = "[]";
        Collections.sort(serviceNames);
        assertEquals("serviceNames", expectedValue, serviceNames.toString().trim());
    }



    public void testServicesTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("group-service-test.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLServices services = model.getIFLServices();
        //System.out.println ("services: " + services);  // Keep for debugging

        int totalServices = services.getCount();
        assertEquals("totalServices", 14, totalServices);        

        List consumedServices = services.getConsumed();
        int totalConsumedServices = consumedServices.size();
        assertEquals("totalConsumedServices", 6, totalConsumedServices);        

        List providedServices = services.getProvided();
        int totalProvidedServices = providedServices.size();
        assertEquals("totalProvidedServices", 8, totalProvidedServices);        

        List unknownServices = services.getUnknnown();
        int totalUnknnownServices = unknownServices.size();
        assertEquals("totalUnknnownServices", 0, totalUnknnownServices);        
    }

    
    public void testConnectionsTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println ("routes: " + routes);  // Keep for debugging

        int totalRoutes = routes.getCount();
        assertEquals("totalRoutes", 5, totalRoutes);        

        IFLConnections connections = model.getIFLConnections();
        //System.out.println ("connections: " + connections);  // Keep for debugging

        int[] counts = {2,2,0,3,4};
        int index = 0;
        List<String> routeIds = routes.getIds();
        for (String routeId : routeIds) {
            Connection connection = connections.getConnection(routeId);
            int count = counts[index++];
            if (connection != null) {
                int totalEntries = connection.totalEntries();
                assertEquals("totalEntries", count, totalEntries);        
            }
        }

        // Pick Connection "route4" and check its values
        Connection route4 = connections.getConnection("route$4");
        String[] names = {"hinbound2","maninthemiddle","bprocess"};
        String[] types = {"from","to","to"};
        index = 0;
        List<Connection.ConnectionEntry> connectionEntries = route4.getEntries();
        for (Connection.ConnectionEntry entry : connectionEntries) {
            String expectedName = names[index];
            String expectedType = types[index++];
            String name = entry.getEntryName();
            String type = entry.getEntryType();
            assertEquals("name", expectedName, name);        
            assertEquals("type", expectedType, type);        
        }
    }


    public void testBroadcastTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("broadcast.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLBroadcasts broadcasts = model.getIFLBroadcasts();
        //System.out.println ("broadcasts: " + broadcasts);  // Keep for debugging

        Broadcast broadcast = broadcasts.getBroadcast("broadcast$1");
        String name         = broadcast.getName();
        String fromService  = broadcast.getFromServiceName();
        String type         = broadcast.getType();
        String routeId      = broadcast.getRouteId();
        Properties config   = broadcast.getConfig();

        assertEquals("name", "broadcast$1", name);        
        assertEquals("fromService", "myFeed", fromService);        
        assertEquals("routeId", "route$3", routeId);        
        
        List<String> toServices = broadcast.getServiceNames();
        String strExpectedValue = "[transform, myIM]";
        assertEquals("toServices", strExpectedValue, toServices.toString().trim());  

    }


    public void testBroadcastTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("broadcast2.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        String[] routeIds     = {"route$1", "route$2", "route$3", "route$4", "route$5"};
        String[] routeNames   = {"route$1", "route$2", "route$3", "route$4", "myFeed"};
        String[] fromServices = {"myFeed",  "myFeed",  "myFeed",  "myFeed",  null};
        int index = 0;
        List<Route> routeList = routes.getRoutes();
        for (Route route: routeList) {
            String routeId   = route.getId();
            String routeName = route.getName();
            assertEquals("routeId", routeIds[index], routeId);        
            assertEquals("routeName", routeNames[index], routeName); 

            String broadcastId = route.getBroadcastId();
            if (broadcastId != null) {
                IFLBroadcasts broadcasts = model.getIFLBroadcasts();
                Broadcast broadcast = broadcasts.getBroadcast(broadcastId);
                String fromService = broadcast.getFromServiceName();
                if (fromServices[index] != null) {
                    assertEquals("fromService", fromServices[index], fromService);        
                }
            }
            index++;
        }
    }

    
    public void testTeeTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("tee-to-external-route.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLTees tees = model.getIFLTees();
        //System.out.println (tees);  // Keep in for debugging

        int totalTees = tees.getCount();
        assertEquals("totalTees", 1, totalTees);        

        Tee tee = tees.getTee("tee$1");

        String fromService = tee.getFromServiceName();
        String endPoint    = tee.getEndPoint();
        String routeId     = tee.getRouteId();
        String name        = tee.getName();

        assertEquals("fromService", "inbound", fromService);        
        assertEquals("endPoint", "log", endPoint);        
        assertEquals("routeId", "route$1", routeId);        
        assertEquals("name", "tee$1", name);        
    }


    public void testTeeTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("tee-to-external-duplicate-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLTees tees = model.getIFLTees();
        //System.out.println (tees);  // Keep in for debugging

        int totalTees = tees.getCount();
        assertEquals("totalTees", 1, totalTees);        

        Tee tee = tees.getTee("tee$1");

        String fromService = tee.getFromServiceName();
        String endPoint    = tee.getEndPoint();
        String routeId     = tee.getRouteId();
        String name        = tee.getName();

        assertEquals("fromService", "inbound", fromService);        
        assertEquals("endPoint", "log", endPoint);        
        assertEquals("routeId", "route$1", routeId);        
        assertEquals("name", "tee$1", name);   

        IFLServices services = model.getIFLServices();
        boolean isService = services.containsName(endPoint);
        assertFalse("Endpoint is NOT a service.",isService);

        IFLRoutes routes = model.getIFLRoutes();
        boolean isRoute = routes.containsName(endPoint);
        assertTrue("Endpoint is a route.",isRoute);

        List<Route> routeList = routes.getRoutesByName(endPoint);
        String[] routeIds = {"route$2", "route$3"};
        int index = 0;
        for (Route route: routeList) {
            routeId = route.getId();
            assertEquals("routeId", routeIds[index], routeId); 
            index++;
        }
    }

    
public void testTeeTest3() throws Exception 
    {
        File iflFile = getTestIFLFile("tee-to-service.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLTees tees = model.getIFLTees();
        //System.out.println (tees);  // Keep in for debugging

        int totalTees = tees.getCount();
        assertEquals("totalTees", 1, totalTees);        

        Tee tee = tees.getTee("tee$1");

        String fromService = tee.getFromServiceName();
        String endPoint    = tee.getEndPoint();
        String routeId     = tee.getRouteId();
        String name        = tee.getName();

        assertEquals("fromService", "inbound", fromService);        
        assertEquals("endPoint", "another-log", endPoint);        
        assertEquals("routeId", "route$1", routeId);        
        assertEquals("name", "tee$1", name);        
    }


    public void testTeeTest4() throws Exception 
    {
        File iflFile = getTestIFLFile("tee-to-external-and-service.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLTees tees = model.getIFLTees();
        //System.out.println (tees);  // Keep in for debugging

        int totalTees = tees.getCount();
        assertEquals("totalTees", 2, totalTees);        

        Tee tee = tees.getTee("tee$1");

        String fromService = tee.getFromServiceName();
        String endPoint    = tee.getEndPoint();
        String routeId     = tee.getRouteId();
        String name        = tee.getName();

        assertEquals("fromService", "inbound", fromService);        
        assertEquals("endPoint", "log", endPoint);        
        assertEquals("routeId", "route$1", routeId);        
        assertEquals("name", "tee$1", name);   

        tee = tees.getTee("tee$2");

        fromService = tee.getFromServiceName();
        endPoint    = tee.getEndPoint();
        routeId     = tee.getRouteId();
        name        = tee.getName();

        assertEquals("fromService", "inbound", fromService);        
        assertEquals("endPoint", "another-log", endPoint);        
        assertEquals("routeId", "route$1", routeId);        
        assertEquals("name", "tee$2", name);        
    }


    public void testTeeTest5() throws Exception 
    {
        File iflFile = getTestIFLFile("tee-to-service-with-config.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLTees tees = model.getIFLTees();
        //System.out.println (tees);  // Keep in for debugging

        int totalTees = tees.getCount();
        assertEquals("totalTees", 1, totalTees);        

        Tee tee = tees.getTee("tee$1");

        String fromService = tee.getFromServiceName();
        String endPoint    = tee.getEndPoint();
        String routeId     = tee.getRouteId();
        String name        = tee.getName();
        Properties config  = tee.getConfig();

        assertEquals("fromService", "inbound", fromService);        
        assertEquals("endPoint", "another-log", endPoint);        
        assertEquals("routeId", "route$1", routeId);        
        assertEquals("name", "tee$1", name);   
        assertEquals("config", "{name=fred}", config.toString().trim());   
    }


    public void testSelectTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("select-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSelects selects = model.getIFLSelects();
        //System.out.println (selects);  // Keep in for debugging

        int totalSelects = selects.getCount();
        assertEquals("totalSelects", 6, totalSelects);  

        Select select = selects.getSelect("select$1");
        String name          = select.getName();
        String fromService   = select.getFromServiceName();
        String selectType    = select.getType();
        String routeId       = select.getRouteId();
        String conditionType = select.getConditionType();
        Properties config    = select.getConfig();

        assertEquals("name", "myXpath", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("selectType", "xpath", selectType);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("conditionType", "value", conditionType);  
        assertTrue("config", config.isEmpty());  

        List<Select.SelectCondition> conditions = select.getConditions();
        int totalConditions = conditions.size();
        assertEquals("totalConditions", 3, totalConditions);  

        String[] conditionClauses   = {"when", "when", "else"};
        String[] conditionTypes     = {null, null, null};
        String[] conditionValues    = {"abc", "xyz", ""};
        String[] conditionToService = {"route-1", "route-2", "route-3"};
        int index = 0;
        for (Select.SelectCondition condition: conditions) {
            assertEquals("conditionClause", conditionClauses[index], condition.getConditionClause());  
            assertEquals("conditionType", conditionTypes[index], condition.getConditionType());  
            assertEquals("conditionValue", conditionValues[index], condition.getConditionValue());  
            assertEquals("conditionToService", conditionToService[index], condition.getConditionToService());  
            index++;
        }

        select = selects.getSelect("select$4");
        name          = select.getName();
        fromService   = select.getFromServiceName();
        selectType    = select.getType();
        routeId       = select.getRouteId();
        conditionType = select.getConditionType();
        config        = select.getConfig();

        assertEquals("name", "select$4", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("selectType", "xpath", selectType);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("conditionType", "condition", conditionType);  
        assertEquals("config", "{exp=count(//Container/EUID) > 0}", config.toString().trim());  
        
        conditions = select.getConditions();
        totalConditions = conditions.size();
        assertEquals("totalConditions", 3, totalConditions);  

        String[] conditionClauses2   = {"when", "when", "else"};
        String[] conditionTypes2     = {"xpath", "xpath", null};
        String[] conditionValues2    = {"/abc", "/xyz", ""};
        String[] conditionToService2 = {"route-1", "route-2", "route-3"};
        index = 0;
        for (Select.SelectCondition condition: conditions) {
            assertEquals("conditionClause", conditionClauses2[index], condition.getConditionClause());  
            assertEquals("conditionType", conditionTypes2[index], condition.getConditionType());  
            assertEquals("conditionValue", conditionValues2[index], condition.getConditionValue());  
            assertEquals("conditionToService", conditionToService2[index], condition.getConditionToService());  
            index++;
        }

        select = selects.getSelect("select$6");
        name          = select.getName();
        fromService   = select.getFromServiceName();
        selectType    = select.getType();
        routeId       = select.getRouteId();
        conditionType = select.getConditionType();
        config        = select.getConfig();

        assertEquals("name", "select$6", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("selectType", "any", selectType);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("conditionType", "condition", conditionType);  
        assertTrue("config", config.isEmpty());  

        conditions = select.getConditions();
        totalConditions = conditions.size();
        assertEquals("totalConditions", 1, totalConditions);  

        Select.SelectCondition condition = conditions.get(0);

        assertEquals("conditionClause", "else", condition.getConditionClause());  
        assertEquals("conditionType", null, condition.getConditionType());  
        assertEquals("conditionValue", "", condition.getConditionValue());  
        assertEquals("conditionToService", "route-3", condition.getConditionToService());  

    }


    public void testSelectTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("select-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSelects selects = model.getIFLSelects();
        //System.out.println (selects);  // Keep in for debugging

        Select select = selects.getSelect("select$1");
        String name         = select.getName();
        String fromService  = select.getFromServiceName();
        String selectType   = select.getType();
        String routeId      = select.getRouteId();
        Properties config   = select.getConfig();

        assertEquals("name", "myRouteRule", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("selectType", "drools", selectType);  
        assertEquals("routeId", "route$1", routeId);  
        assertTrue("config", config.isEmpty());  
    }

    public void testSelectDynamicRulesIFLModel() throws Exception {
        File iflFile = getTestIFLFile("select-dynamic-rules.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSelects selects = model.getIFLSelects();
        //System.out.println (selects);  // Keep in for debugging

        Select select = selects.getSelect("select$1");
        String name          = select.getName();
        String fromService   = select.getFromServiceName();
        String selectType    = select.getType();
        String routeId       = select.getRouteId();
        String conditionType = select.getConditionType();
        Properties config    = select.getConfig();
        List<Select.SelectCondition> conditions = select.getConditions();
        int totalConditions = conditions.size();

        assertEquals("name", "dynamic-type-selection1", name);
        assertEquals("fromService", null, fromService);
        assertEquals("selectType", "any", selectType);
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("conditionType", "condition", conditionType);  
        assertTrue("config", config.isEmpty());  
        assertEquals("totalConditions", 0, totalConditions);  

        select = selects.getSelect("select$2");
        name          = select.getName();
        fromService   = select.getFromServiceName();
        selectType    = select.getType();
        routeId       = select.getRouteId();
        conditionType = select.getConditionType();
        config        = select.getConfig();
        conditions    = select.getConditions();
        totalConditions = conditions.size();

        assertEquals("name", "dynamic-type-selection2", name);
        assertEquals("fromService", "inbound1", fromService);
        assertEquals("selectType", "any", selectType);
        assertEquals("routeId", "route$2", routeId);  
        assertEquals("conditionType", "condition", conditionType);  
        assertEquals("totalConditions", 1, totalConditions);  

        String[] conditionClauses1   = {"else"};
        String[] conditionTypes1     = {null};
        String[] conditionValues1    = {""};
        String[] conditionToService1 = {"other-transaction"};
        int index = 0;
        for (Select.SelectCondition condition: conditions) {
            assertEquals("conditionClause", conditionClauses1[index], condition.getConditionClause());  
            assertEquals("conditionType", conditionTypes1[index], condition.getConditionType());  
            assertEquals("conditionValue", conditionValues1[index], condition.getConditionValue());  
            assertEquals("conditionToService", conditionToService1[index], condition.getConditionToService());  
            index++;
        }

        select = selects.getSelect("select$3");
        name          = select.getName();
        fromService   = select.getFromServiceName();
        selectType    = select.getType();
        routeId       = select.getRouteId();
        conditionType = select.getConditionType();
        config        = select.getConfig();
        conditions    = select.getConditions();
        totalConditions = conditions.size();

        assertEquals("name", "dynamic-type-selection3", name);
        assertEquals("fromService", "inbound2", fromService);
        assertEquals("selectType", "any", selectType);
        assertEquals("routeId", "route$3", routeId);  
        assertEquals("conditionType", "condition", conditionType);  
        assertEquals("totalConditions", 2, totalConditions);  

        String[] conditionClauses2   = {"when", "else"};
        String[] conditionTypes2     = {"regex", null};
        String[] conditionValues2    = {"Text-", ""};
        String[] conditionToService2 = {"non-xml-handling", "other-transaction"};
        index = 0;
        for (Select.SelectCondition condition: conditions) {
            assertEquals("conditionClause", conditionClauses2[index], condition.getConditionClause());  
            assertEquals("conditionType", conditionTypes2[index], condition.getConditionType());  
            assertEquals("conditionValue", conditionValues2[index], condition.getConditionValue());  
            assertEquals("conditionToService", conditionToService2[index], condition.getConditionToService());  
            index++;
        }
    }


    public void testSplitTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("split-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSplits splits = model.getIFLSplits();
        //System.out.println (splits);  // Keep in for debugging

        Split split = splits.getSplit("split$1");
        String name         = split.getName();
        String fromService  = split.getFromServiceName();
        String splitType    = split.getType();
        String routeId      = split.getRouteId();
        Properties config   = split.getConfig();

        assertEquals("name", "split$1", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("selectType", "xpath", splitType);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("config", "{exp=/PurchaseOrder}", config.toString().trim());  
    }


    public void testSplitTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("split-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSplits splits = model.getIFLSplits();
        //System.out.println (splits);  // Keep in for debugging

        Split split = splits.getSplit("split$1");
        String name         = split.getName();
        String fromService  = split.getFromServiceName();
        String splitType    = split.getType();
        String routeId      = split.getRouteId();
        Properties config   = split.getConfig();

        assertEquals("name", "po-split", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("selectType", "xpath", splitType);  
        assertEquals("routeId", "route$1", routeId);  
        assertTrue("config", config.isEmpty());  
    }


    public void testSplitTest3() throws Exception 
    {
        File iflFile = getTestIFLFile("split-inline-exp.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSplits splits = model.getIFLSplits();
        //System.out.println (splits);  // Keep in for debugging

        Split split = splits.getSplit("split$1");
        String name         = split.getName();
        String fromService  = split.getFromServiceName();
        String splitType    = split.getType();
        String routeId      = split.getRouteId();
        Properties config   = split.getConfig();

        assertEquals("name", "split$1", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("selectType", "xpath", splitType);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("config", "{other=5, exp=/PurchaseOrder}", config.toString().trim());  
    }


    public void testSplitandBroadcastTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("split-all.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        IFLSplits splits = model.getIFLSplits();
        //System.out.println (splits);  // Keep in for debugging

        IFLBroadcasts broadcasts = model.getIFLBroadcasts();
        //System.out.println ("broadcasts: " + broadcasts);  // Keep for debugging

        int totalRoutes     = routes.getCount();
        int totalSplits     = splits.getCount();
        int totalBroadcasts = broadcasts.getCount();

        assertEquals("totalRoutes", 24, totalRoutes);
        assertEquals("totalSplits", 6, totalSplits);
        assertEquals("totalBroadcasts", 5, totalBroadcasts);
    }


    public void testSplitandAggregateTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("split-aggregate-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLSplits splits = model.getIFLSplits();
        //System.out.println (splits);  // Keep in for debugging

        IFLAggregates aggregates = model.getIFLAggregates();
        //System.out.println (aggregates);  // Keep in for debugging

        Split split = splits.getSplit("split$1");
        String name         = split.getName();
        String fromService  = split.getFromServiceName();
        String type         = split.getType();
        String routeId      = split.getRouteId();
        Properties config   = split.getConfig();

        assertEquals("name", "foo", name);  
        assertEquals("fromService", "a", fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertTrue("config", config.isEmpty());  

        Aggregate aggregate = aggregates.getAggregate("aggregate$1");
        name          = aggregate.getName();
        fromService   = aggregate.getFromServiceName();
        type          = aggregate.getType();
        routeId       = aggregate.getRouteId();
        config        = aggregate.getConfig();

        assertEquals("name", "bar", name);  
        assertEquals("fromService", "a", fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertTrue("config", config.isEmpty());  
    }


    public void testFilterTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("filter-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLFilters filters = model.getIFLFilters();
        //System.out.println (filters);  // Keep in for debugging

        Filter filter = filters.getFilter("filter$1");
        String name         = filter.getName();
        String fromService  = filter.getFromServiceName();
        String type         = filter.getType();
        String routeId      = filter.getRouteId();
        Properties config   = filter.getConfig();

        assertEquals("name", "filter$1", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("config", "{exp=/PurchaseOrder}", config.toString().trim());  
    }


    public void testFilterTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("filter-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLFilters filters = model.getIFLFilters();
        //System.out.println (filters);  // Keep in for debugging

        Filter filter = filters.getFilter("filter$1");
        String name         = filter.getName();
        String fromService  = filter.getFromServiceName();
        String type         = filter.getType();
        String routeId      = filter.getRouteId();
        Properties config   = filter.getConfig();

        assertEquals("name", "po-filter", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertTrue("config", config.isEmpty());  
    }


    public void testSplitAggregateFilterBroadcastTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("split-aggregate-filter.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        System.out.println (routes);

        IFLSplits splits = model.getIFLSplits();
        //System.out.println (splits);  // Keep in for debugging

        IFLAggregates aggregates = model.getIFLAggregates();
        //System.out.println (aggregates);  // Keep in for debugging

        IFLFilters filters = model.getIFLFilters();
        //System.out.println (filters);  // Keep in for debugging

        IFLBroadcasts broadcasts = model.getIFLBroadcasts();
        //System.out.println (broadcasts);  // Keep for debugging

        Split split = splits.getSplit("split$1");
        String name         = split.getName();
        String fromService  = split.getFromServiceName();
        String type         = split.getType();
        String routeId      = split.getRouteId();
        Properties config   = split.getConfig();

        assertEquals("name", "split$1", name);  
        assertEquals("fromService", "foo", fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("config", "{exp=//orders/order}", config.toString().trim());  

        Aggregate aggregate = aggregates.getAggregate("aggregate$1");
        name          = aggregate.getName();
        fromService   = aggregate.getFromServiceName();
        type          = aggregate.getType();
        routeId       = aggregate.getRouteId();
        config        = aggregate.getConfig();

        assertEquals("name", "aggregate$1", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("type", "set", type);  
        assertEquals("routeId", "route$2", routeId);  
        assertEquals("config", "{count=2, trailer=</report>, header=<report>}", config.toString().trim());  

        Filter filter = filters.getFilter("filter$1");
        name          = filter.getName();
        fromService   = filter.getFromServiceName();
        type          = filter.getType();
        routeId       = filter.getRouteId();
        config        = filter.getConfig();

        assertEquals("name", "filter$1", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("config", "{exp=count(//order/euid) > 0}", config.toString().trim());  

        filter = filters.getFilter("filter$2");
        name          = filter.getName();
        fromService   = filter.getFromServiceName();
        type          = filter.getType();
        routeId       = filter.getRouteId();
        config        = filter.getConfig();

        assertEquals("name", "filter$2", name);  
        assertEquals("fromService", null, fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$2", routeId);  
        assertEquals("config", "{exp=count(//order/euid) = 0}", config.toString().trim());  

        Broadcast broadcast = broadcasts.getBroadcast("broadcast$1");
        name          = broadcast.getName();
        fromService   = broadcast.getFromServiceName();
        type          = broadcast.getType();
        routeId       = broadcast.getRouteId();
        config        = broadcast.getConfig();

        List<String> serviceNames = broadcast.getServiceNames();

   //     System.out.println ("name: " + name);
   //     System.out.println ("fromService: " + fromService);
   //     System.out.println ("type: " + type);
   //     System.out.println ("routeId: " + routeId);
   //     System.out.println ("config: " + config);
   //     System.out.println ("serviceNames.get(0): " + serviceNames.get(0));
   //     System.out.println ("serviceNames.get(1): " + serviceNames.get(1));

        assertEquals("name", "broadcast$1", name);  
        assertEquals("fromService", "foo", fromService);  
        assertEquals("type", null, type);  
        assertEquals("routeId", "route$3", routeId);  
        assertTrue("config", config.isEmpty());  
        assertEquals("serviceNames(0)", "route$1", serviceNames.get(0));  
        assertEquals("serviceNames(1)", "route$2", serviceNames.get(1));  
    }


    public void testAggregateTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("aggregate-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLAggregates aggregates = model.getIFLAggregates();
        //System.out.println (aggregates);  // Keep in for debugging

        Aggregate aggregate = aggregates.getAggregate("aggregate$1");
        String name          = aggregate.getName();
        String fromService   = aggregate.getFromServiceName();
        String type          = aggregate.getType();
        String routeId       = aggregate.getRouteId();
        Properties config    = aggregate.getConfig();

        assertEquals("name", "aggregate$1", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("type", "set", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertEquals("config", "{count=5}", config.toString().trim());  
    }


    public void testAggregateTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("aggregate-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLAggregates aggregates = model.getIFLAggregates();
        //System.out.println (aggregates);  // Keep in for debugging

        Aggregate aggregate = aggregates.getAggregate("aggregate$1");
        String name          = aggregate.getName();
        String fromService   = aggregate.getFromServiceName();
        String type          = aggregate.getType();
        String routeId       = aggregate.getRouteId();
        Properties config    = aggregate.getConfig();

        assertEquals("name", "po-aggregate", name);  
        assertEquals("fromService", "batch-inbound", fromService);  
        assertEquals("type", "xpath", type);  
        assertEquals("routeId", "route$1", routeId);  
        assertTrue("config", config.isEmpty());  
    }


    public void testVisibilityTest1() throws Exception 
    {
        File iflFile = getTestIFLFile("visibility-public.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        Route route1 = routes.getRoute("route$1");
        String name        = route1.getName();
        String visibility  = route1.getVisibility();
        String fromService = route1.getFromServiceName();
        String broadcastId = route1.getBroadcastId();
        boolean isProvided = route1.isProvided();
        boolean isConsumed = route1.isConsumed();

        assertEquals("name", "infile", name);  
        assertEquals("visibility", "public", visibility);  
        assertEquals("fromService", null, fromService);  
        assertEquals("broadcastId", null, broadcastId);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        List<Route.RouteEntry> routeEntries = route1.getRouteEntries();
        String[] routePath    = {"convert-order", "convert-order2", "purchase-order"};
        String[] eipId        = {null, null, null};
        String[] serviceName  = {"convert-order", "convert-order2", "purchase-order"};
        String[] namespace    = {"app3", "app4", ""};
        String[] operation    = {"", "", ""};
        int index = 0;
        for (Route.RouteEntry entry: routeEntries) {
            assertEquals("routePath", routePath[index], entry.getRoutePath());  
            assertEquals("eipId", eipId[index], entry.getEipId());  
            assertEquals("serviceName", serviceName[index], entry.getServiceName());  
            assertEquals("namespace", namespace[index], entry.getNamespace());  
            assertEquals("operation", operation[index], entry.getOperation()); 
            index++;
        }

        IFLServices services = model.getIFLServices();
        //System.out.println (services);  // Keep in for debugging 

        Service service = services.getService("infile");
        String type = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "file", type);  
        assertEquals("visibility", "private", visibility);  
        assertFalse("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        service = services.getService("convert-order");
        type        = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "bpel", type);  
        assertEquals("visibility", "public", visibility);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        service = services.getService("convert-order2");
        type        = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "bpel", type);  
        assertEquals("visibility", "private", visibility);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        service = services.getService("purchase-order");
        type        = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "bpel", type);  
        assertEquals("visibility", "private", visibility);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);
    }


    public void testVisibilityTest2() throws Exception 
    {
        File iflFile = getTestIFLFile("visibility-private.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        Route route1 = routes.getRoute("route$1");
        String name        = route1.getName();
        String visibility  = route1.getVisibility();
        String fromService = route1.getFromServiceName();
        String broadcastId = route1.getBroadcastId();
        boolean isProvided = route1.isProvided();
        boolean isConsumed = route1.isConsumed();

        assertEquals("name", "infile", name);  
        assertEquals("visibility", "private", visibility);  
        assertEquals("fromService", null, fromService);  
        assertEquals("broadcastId", null, broadcastId);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);
    }


    public void testVisibilityTest3() throws Exception 
    {
        File iflFile = getTestIFLFile("visibility-default.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        IFLRoutes routes = model.getIFLRoutes();
        //System.out.println (routes);  // Keep in for debugging

        Route route1 = routes.getRoute("route$1");
        String name        = route1.getName();
        String visibility  = route1.getVisibility();
        String fromService = route1.getFromServiceName();
        String broadcastId = route1.getBroadcastId();
        boolean isProvided = route1.isProvided();
        boolean isConsumed = route1.isConsumed();

        assertEquals("name", "infile", name);  
        assertEquals("visibility", "private", visibility);  
        assertEquals("fromService", "infile", fromService);  
        assertEquals("broadcastId", null, broadcastId);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        IFLServices services = model.getIFLServices();
        //System.out.println (services);  // Keep in for debugging 

        Service service = services.getService("infile");
        String type = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "file", type);  
        assertEquals("visibility", "private", visibility);  
        assertFalse("isProvided", isProvided);
        assertTrue("isConsumed", isConsumed);

        service = services.getService("convert-order");
        type        = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "bpel", type);  
        assertEquals("visibility", "public", visibility);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        service = services.getService("convert-order2");
        type        = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "bpel", type);  
        assertEquals("visibility", "private", visibility);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);

        service = services.getService("purchase-order");
        type        = service.getType();
        visibility  = service.getVisibility();
        isProvided  = service.isProvided();
        isConsumed  = service.isConsumed();

        assertEquals("type", "bpel", type);  
        assertEquals("visibility", "private", visibility);  
        assertTrue("isProvided", isProvided);
        assertFalse("isConsumed", isConsumed);
    }


    public void testVisibilityTest4() throws Exception 
    {
        File iflFile = getTestIFLFile("visibility-negitive.ifl");
        try {
            IFLModel model = (new IFLReader()).read(iflFile);
        } catch (IFLParseException ex) {
            int index = ex.getMessage().indexOf("Invalid service visibility type specified");
            boolean condition = (index == -1) ? false : true;
            assertTrue("Error should of occured due to a duplicate service name.",condition);
        }
    }


    public void testNamespacePositiveTest1() throws Exception 
    {
        File file1 = getTestIFLFile("namespace-test1-positive.ifl");
        File file2 = getTestIFLFile("namespace-second-file.ifl");
        File file3 = getTestIFLFile("shared-namespaces.ifl");
        File[] fileList = new File[] { file1, file2, file3 };
        IFLModel model = (new IFLReader()).read(fileList);

        String filepath = file1.getAbsolutePath();

        IFLNamespaces namespaces = model.getIFLNamespaces();
        //System.out.println (namespaces);  // Keep in for debugging 

        int totalNamespaces = namespaces.getCount();
        assertEquals("totalNamespaces", 10, totalNamespaces);        

        Namespace namespace1 = namespaces.getNamespace(filepath,"app3");
        String namespace1Name   = namespace1.getName();
        String namespace1Key    = namespace1.getKey();
        String namespace1Value  = namespace1.getValue();
        assertEquals("namespace1Name", "namespace-test1-positive.ifl", namespace1Name);  
        assertEquals("namespace1Key", "app3", namespace1Key);  
        assertEquals("namespace1Value", "/app3/local", namespace1Value);  

        Namespace namespace2 = namespaces.getNamespace(filepath,"app4");
        String namespace2Name   = namespace2.getName();
        String namespace2Key    = namespace2.getKey();
        String namespace2Value  = namespace2.getValue();
        assertEquals("namespace2Name", "shared-namespaces.ifl", namespace2Name);  
        assertEquals("namespace2Key", "app4", namespace2Key);  
        assertEquals("namespace2Value", "/app4", namespace2Value);  

        Namespace namespace3 = namespaces.getNamespace(filepath,"fred");
        assertNull("namespace3 should be null", namespace3);
    }



}
