/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLUtilTest.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import java.util.Enumeration;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.Set;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;


/**
 * Unit test for IFLModel.
 */
public class IFLUtilTest
    extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public IFLUtilTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(IFLUtilTest.class);
    }

    private void checkMaps(Properties ref, Properties map) {
        Enumeration e = ref.propertyNames();
        while (e.hasMoreElements()) {
            String name = (String)e.nextElement();
            assertEquals("Should contain NV pair with Key: " + name, ref.getProperty(name), map.getProperty(name));
            map.remove(name);
        }
        assertTrue("Extra entries present", map.isEmpty());
    }

    /**
     * Test variations of NVpairs parsing
     */
    public void testNVpairs() throws Exception {
        Properties	map;
        Properties  reference = new Properties();

    //
    // Check white-space around name.
    //
    reference.setProperty("a","b");
	map = IFLUtil.parseNVpairs("a:b", false);
    checkMaps(reference, map);
	map = IFLUtil.parseNVpairs(" a:b", false);
    checkMaps(reference, map);
	map = IFLUtil.parseNVpairs("a :b", false);
    checkMaps(reference, map);
	map = IFLUtil.parseNVpairs(" a :b", false);
    checkMaps(reference, map);
	map = IFLUtil.parseNVpairs("  a  =b", false);
    checkMaps(reference, map);

    //
    //  Check escaped = or : in name.
    //
    reference.clear();
    reference.setProperty("a:","b");
	map = IFLUtil.parseNVpairs("a\\:=b", false);
    checkMaps(reference, map);
    reference.clear();
    reference.setProperty("a=","b");
	map = IFLUtil.parseNVpairs("a\\=:b", false);
    checkMaps(reference, map);

    //
    // Check escaped ,.
    //
    reference.clear();
    reference.setProperty("a","b,");
	map = IFLUtil.parseNVpairs("a=b\\,", false);
    checkMaps(reference, map);

    //
    // Check with default simple expression
    //
    reference.clear();
    reference.setProperty("exp","a=b,");
	map = IFLUtil.parseNVpairs("a=b,", true);
    checkMaps(reference, map);
	map = IFLUtil.parseNVpairs("exp=a\\=b\\,", true);
    checkMaps(reference, map);

    }
}
