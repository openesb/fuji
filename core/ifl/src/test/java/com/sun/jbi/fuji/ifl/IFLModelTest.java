/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying informatiFon: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLModelTest.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.parser.IFLParsedModel;
import com.sun.jbi.fuji.ifl.parser.IFLParser;


/**
 * Unit test for IFLModel.
 */
public class IFLModelTest
    extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public IFLModelTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(IFLModelTest.class);
    }

    public static void printIFLParsedModel(IFLParsedModel parser) {
        System.out.println("######## IFL PARSED MODEL ########");
        System.out.println("------- Services declaration ---------");
        Map<String, String> servicesMap = parser.getServicesMap();
        Collection<Map.Entry<String, List<String>>> routeList = parser.getRouteList().values();
        for (String service : servicesMap.keySet()) {
            System.out.println("Service Name= " + service + " Type= " + servicesMap.get(service));
        }
        System.out.println("------- Rounting info ----------");
        for (Map.Entry<String, List<String>> route : routeList) {
            System.out.println("Route Do");
            System.out.println(" From= " + route.getKey());
            List<String> toList = route.getValue();
            for (String to : toList) {
                System.out.println(" To= " + to);
            }
            System.out.println("End");
        }
        System.out.println("################################");
    }

    public static void printIFLModel(IFLModel model) {
        System.out.println("============ IFL PROCESSING MODEL ============");
        System.out.println("------  Services per Service Type --------");
        for (String type : model.getServiceTypes()) {
            System.out.println("Service Type= " + type);
            for (String service : model.getServicesForType(type)) {
                boolean consumed = model.isConsumed(service);
                boolean provided = model.isProvided(service);
                System.out.print(" Service= " + service);
                if (consumed) {
                    System.out.print(" (consumed)");
                }
                if (provided) {
                    System.out.print(" (provided)");
                }
                System.out.println();
            }
        }

        System.out.println("--------- Consumed Services --------");
        for (String consumedService : model.getConsumedServices()) {
            System.out.println("Consumed service " + consumedService);
        }
        System.out.println("--------- Provided Services --------");
        for (String providedService : model.getProvidedServices()) {
            System.out.println("Provided service " + providedService);
        }
        System.out.println("-------- Service Connections ---------");
        for (Map.Entry<String, Set<String>> conn : model.getConnections()) {
            System.out.println("Connection [consumed=" + conn.getKey() + " provided=" + conn.getValue() + "]");
            
        }
        System.out.println("====================================");
    }

    /**
     * return File object with test data dir path appended.
     * @param relFilePath relative path w.r.t. src/test/data
     * @return
     */
    private File getTestIFLFile(String relFilePath) {

        String basedir = System.getProperty("basedir");
        // String iflFilePath = "C:/home/chikkala/dev/fuji/test/iflparser/src/test/data/test.ifl";
        String iflFilePath = "src/test/data/" + relFilePath; // relative to basedir;

        File iflFile = new File(basedir, iflFilePath);
        return iflFile;
    }

    /**
     * Rigourous Test :-)
     */
    public void testIFLParsedModel() throws Exception {

        File iflFile = getTestIFLFile("multi-routes.ifl");
        FileReader reader = new FileReader(iflFile);
        IFLParser parser = new IFLParser(reader);
        parser.parse();
        printIFLParsedModel(parser);

    }

    /**
     * Rigourous Test :-)
     */
    public void testIFLModel() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        printIFLModel(model);
        assertEquals("multi-routes.ifl should have 3 connections", 4, model.getConnections().size());
        List<String> services = model.getServicesForType("File");
        assertTrue("Should have File Service: finbound", services.contains("finbound"));
        assertTrue("Should have File Service: foutbound", services.contains("foutbound"));
        assertTrue("fibbound should be consumed service", model.isConsumed("finbound"));
        assertTrue("foutbound should be provided service", model.isProvided("foutbound"));
        Set<String> serviceTypes = model.getServiceTypes();
        assertTrue("Should have File Service type name as lower case file", serviceTypes.contains("file"));
    }

    public void testIFLModelWithInputStream() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(new FileInputStream(iflFile));
        assertEquals("multi-routes.ifl should have 3 connections", 4, model.getConnections().size());
        List<String> services = model.getServicesForType("File");
        assertTrue("Should have File Service: finbound", services.contains("finbound"));
        assertTrue("Should have File Service: foutbound", services.contains("foutbound"));
        assertTrue("fibbound should be consumed service", model.isConsumed("finbound"));
        assertTrue("foutbound should be provided service", model.isProvided("foutbound"));

    }

    public void testIFLModelWithReader() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(new FileReader(iflFile));
        assertEquals("multi-routes.ifl should have 3 connections", 4, model.getConnections().size());
        List<String> services = model.getServicesForType("File");
        assertTrue("Should have File Service: finbound", services.contains("finbound"));
        assertTrue("Should have File Service: foutbound", services.contains("foutbound"));
        assertTrue("fibbound should be consumed service", model.isConsumed("finbound"));
        assertTrue("foutbound should be provided service", model.isProvided("foutbound"));
    }

    /**
     * Rigourous Test :-)
     */
    public void testEmptyIFLModel() throws Exception {
        File iflFile = getTestIFLFile("empty.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        assertEquals("Empty ifl file should return zero services ", model.getProvidedServices().size(), 0);
    }

    /**
     * Rigourous Test :-)
     */
    public void testCommentsOnlyIFLModel() throws Exception {
        File iflFile = getTestIFLFile("comments-only.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        assertEquals("Comments only ifl file should return zero services ", model.getProvidedServices().size(), 0);
    }

    /**
     * Rigourous Test :-)
     */
    public void testServiesOnlyIFLModel() throws Exception {
        File iflFile = getTestIFLFile("services-only.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        assertEquals("Services only ifl file should return zero connections", model.getConnections().size(), 0);
        List<String> services = model.getServicesForType("File");
        assertTrue("Should have File Service: finbound", services.contains("finbound"));
    }

    /**
     * Rigourous Test :-)
     */
    public void testSimpleIFLModel() throws Exception {
        File iflFile = getTestIFLFile("simple.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        assertEquals("simple ifl file should return one connections", model.getConnections().size(), 1);
        List<String> services = model.getServicesForType("File");

        assertTrue("Should have File Service: inbound", services.contains("inbound"));
        assertTrue("Should have File Service: outbound", services.contains("outbound"));
        assertTrue("ibbound should be consumed service", model.isConsumed("inbound"));
        assertTrue("outbound should be provided service", model.isProvided("outbound"));
    }

    /**
     * Rigourous Test :-)
     */
    public void testFilterIFLModel() throws Exception {
        File iflFile = getTestIFLFile("filter.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        // printIFLModel(model);
        assertEquals("Filter ifl file should return one connections", model.getConnections().size(), 1);
        List<String> services = model.getServicesForType("File");
        assertTrue("Should have File Service: inbound", services.contains("inbound"));
        assertTrue("Should have File Service: outbound", services.contains("outbound"));
        assertTrue("ibbound should be consumed service", model.isConsumed("inbound"));
        assertTrue("outbound should be provided service", model.isProvided("outbound"));
        assertTrue("transform should be provided service", model.isProvided("transform"));
    }

    /**
     * Rigourous Test :-)
     */
    public void testMultiRoutesIFLModel() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        // printIFLModel(model);
        assertEquals("Filter ifl file should return 4 connections", model.getConnections().size(), 4);
        // test no filter for file based route
        List<String> services = model.getServicesForType("File");
        assertTrue("Should have File Service: finbound", services.contains("finbound"));
        assertTrue("Should have File Service: foutbound", services.contains("foutbound"));
        assertTrue("fibbound should be consumed service", model.isConsumed("finbound"));
        assertTrue("foutbound should be provided service", model.isProvided("foutbound"));
    }

    /**
     * Rigourous Test :-)
     */
    public void testIFLModelAllServices() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(new FileReader(iflFile));

        // Create the Map containing the expected data
        HashMap services = new HashMap() {{
            put("bpel", Arrays.asList("bporocess"));
            put("file", Arrays.asList("finbound","foutbound"));
            put("http", Arrays.asList("hinbound1","hinbound2","hinbound3","notknown","transform2"));
            put("xslt", Arrays.asList("maninthemiddle","transform1"));
        }};

        // Validate the services
        validateServices (model,services);
    }

    /**
     * Rigourous Test :-)
     */
    public void testError1IFLFile() throws Exception {
        File iflFile = getTestIFLFile("error/error1.ifl");
        try {
            IFLModel model = (new IFLReader()).read(iflFile);
            fail("Did not throw Exception:  should print error : string/service expected");
        } catch (Exception ex) {
            System.out.println("----Expected ifl error1 (string/service expected) test output-----");
            System.out.println(ex.getMessage());
            System.out.println("----");
        }
    }

    public void testError2IFLFile() throws Exception {
        File iflFile = getTestIFLFile("error/error2.ifl");
        try {
            IFLModel model = (new IFLReader()).read(iflFile);
            fail("Did not throw Exception:  should print error : from expected");
        } catch (Exception ex) {
            System.out.println("----Expected ifl error2(from expected) test output-----");
            System.out.println(ex.getMessage());
            System.out.println("----");
        }
    }

    public void testError3IFLFile() throws Exception {
        File iflFile = getTestIFLFile("error/error3.ifl");
        try {
            IFLModel model = (new IFLReader()).read(iflFile);
            fail("Did not throw Exception:  should print error : to or end expected");
        } catch (Exception ex) {
            System.out.println("----Expected ifl error3(to, broadcast or end expected) test output-----");
            System.out.println(ex.getMessage());
            System.out.println("----");
        }
    }

     public void testError4IFLFile() throws Exception {
        File iflFile = getTestIFLFile("error/error4.ifl");
        try {
            IFLModel model = (new IFLReader()).read(iflFile);
            fail("Did not throw Exception:  should print error : Select when clauses can only contain values or conditions, not both.");
        } catch (Exception ex) {
            System.out.println("----Expected ifl error4(Select when clauses can only contain values or conditions, not both.) test output-----");
            System.out.println(ex.getMessage());
            System.out.println("----");
        }
    }

    public void testConsumerOnlyRoute() throws Exception {
        File iflFile = getTestIFLFile("consumer-only.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        assertTrue("myIM should be consumer service", model.isConsumed("myIM"));
        assertEquals("Should have one consumer", 1, model.getConsumedServices().size());
        assertEquals("Should have zero provided", 1, model.getProvidedServices().size());
    }

    public void testProvidedOnlyRoute() throws Exception {
        File iflFile = getTestIFLFile("provider-only.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        assertTrue("myIM should be provided service", model.isProvided("myIM"));
        assertEquals("Should have one provided", 2, model.getProvidedServices().size());
        assertEquals("Should have zero consumed", 0, model.getConsumedServices().size());
    }

    public void testLineSyntaxRoutes() throws Exception {
        File iflFile = getTestIFLFile("line-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        assertTrue("foo should be consumer service", model.isConsumed("foo"));
        assertTrue("bar should be provided service", model.isProvided("bar"));

        assertTrue("xyz should be consumer service", model.isConsumed("xyz"));
        assertTrue("abc should be provided service", model.isProvided("abc"));

        assertEquals("Should have 5 provided", 5, model.getProvidedServices().size());
        assertEquals("Should have 2 consumed", 2, model.getConsumedServices().size());

        assertEquals("should have one connections", 2, model.getConnections().size());
        Map.Entry<String, Set<String>> connection = model.getConnections().iterator().next();
        assertEquals("Connection: consumed should be foo", "foo", connection.getKey());
        assertEquals("Connection: provided should be foo", "[bar, foo]", connection.getValue().toString());
    }

    public void testMixedSyntaxRoutes() throws Exception {
        File iflFile = getTestIFLFile("mix-syntax-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        assertTrue("footestRouteWithBroadcast should be consumer service", model.isConsumed("foo"));
        assertTrue("bar should be provided service", model.isProvided("bar"));

        assertTrue("xyz should be consumer service", model.isConsumed("xyz"));
        assertTrue("abc should be provided service", model.isProvided("abc"));

        assertEquals("Should have 2 provided", 5, model.getProvidedServices().size());
        assertEquals("Should have 2 consumed", 2, model.getConsumedServices().size());

        assertEquals("should have 2 connections", 2, model.getConnections().size());

        Set<Entry<String, Set<String>>> connectionSet = model.getConnections();
        String connectionSetString = connectionSet.toString();
        assertEquals("The connection set should be [route$2=[abc], foo=[bar, foo]]", "[route$2=[abc], foo=[bar, foo]]", connectionSetString);
    }

    public void testBroadcastTest1() throws Exception {
        File iflFile = getTestIFLFile("broadcast.ifl");
        IFLModel model = (new IFLReader()).read(new FileReader(iflFile));
        //model.close();
        Collection broadcastList = model.getBroadcasts().values();

        HashMap broadcast = new HashMap() { {
                put("CONFIG",   null);
                put("TYPE",     null);
                put("TO",       Arrays.asList("transform","myIM"));
                put("NAME",     "broadcast$1");
                put("FROM",     "myFeed"); } };
        List broadcastCollection = Arrays.asList(broadcast);
        validateData(broadcastList,broadcastCollection);
    }

    public void testBroadcastTest2() throws Exception {
        File iflFile = getTestIFLFile("split-aggregate-filter.ifl");
        IFLModel model = (new IFLReader()).read(new FileReader(iflFile));
        //model.close();
        Collection broadcastList = model.getBroadcasts().values();

        HashMap broadcast = new HashMap() { {
                put("CONFIG",   null);
                put("TYPE",     null);
                put("TO",       Arrays.asList("route$1","route$2"));
                put("NAME",     "broadcast$1");
                put("FROM",     "foo"); } };
        List broadcastCollection = Arrays.asList(broadcast);
        validateData(broadcastList,broadcastCollection);
    }

    public void testRouteWithBroadcast() throws Exception {
        File iflFile = getTestIFLFile("broadcast.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        //model.close();
        assertEquals("Broadcast ifl file should return 1 connection", 3, model.getConnections().size());

        assertTrue("myFeed should be consumed service", model.isConsumed("myFeed"));
        assertTrue("rubyFilter should be provided service", model.isProvided("rubyFilter"));
        assertTrue("rubyFilter should be provided service", model.isProvided("transform"));
        assertTrue("rubyFilter should be provided service", model.isProvided("myIM"));

        assertEquals("Should have 3 provided", 6, model.getProvidedServices().size());
        assertEquals("Should have 1 consumed", 1, model.getConsumedServices().size());
    }

    protected IFLParsedModel parseIFL(File iflFile) throws Exception {
        Reader reader = new FileReader(iflFile);
        IFLParser parser = new IFLParser(reader);
        parser.parse();
        IFLParsedModel parsedModel = (IFLParsedModel) parser;
        return parsedModel;
    }

    protected void printSplitData(Map<String, Object> splitData) {
        System.out.println("-------- Split Data ------------");
        for (String key : splitData.keySet()) {
            System.out.println(key + "=" + splitData.get(key));
        }
        System.out.println("--------------------");
    }

    protected void printSplitMap(Map<String, Map<String, Object>> splitMap) throws Exception {
        for (String service : splitMap.keySet()) {
            Map<String, Object> splitData = splitMap.get(service);
            printSplitData(splitData);
        }
    }

    protected void printAggregateData(Map<String, Object> aggregateData) {
        System.out.println("-------- Aggregate Data ------------");
        for (String key : aggregateData.keySet()) {
            System.out.println(key + "=" + aggregateData.get(key));
        }
        System.out.println("--------------------");
    }

    protected void printAggregateMap(Map<String, Map<String, Object>> aggregateMap) throws Exception {
        for (String service : aggregateMap.keySet()) {
            Map<String, Object> aggregateData = aggregateMap.get(service);
            printAggregateData(aggregateData);
        }
    }

    protected void printFilterData(Map<String, Object> filterData) {
        System.out.println("-------- Filter Data ------------");
        for (String key : filterData.keySet()) {
            System.out.println(key + "=" + filterData.get(key));
        }
        System.out.println("--------------------");
    }

    protected void printFilterMap(Map<String, Map<String, Object>> filterMap) throws Exception {
        for (String service : filterMap.keySet()) {
            Map<String, Object> filterData = filterMap.get(service);
            printFilterData(filterData);
        }
    }

    protected void validateServices (IFLModel model, HashMap testData)
    {
        Iterator it = testData.keySet().iterator();
        while(it.hasNext()) {
            String key = (String)it.next();
            List serviceList = (List)testData.get(key);
            List<String> services = model.getServicesForType(key);
            Iterator listIt = serviceList.iterator();
            while (listIt.hasNext()) {
                String serviceName = (String)listIt.next();
                if (!(services.contains(serviceName)))
                {
                    String errorStr = "The service type '" + key + "' is missing service '" + serviceName + "'.";
                    System.out.println (errorStr);
                    assertTrue (errorStr,false);
                }
            }

        }
    }

    protected void validateData(Collection<Map<String, Object>> iflCollection,
                                List testCollection)
    {
        int iflSize = iflCollection.size();
        int testSize = testCollection.size();
        if (iflSize != testSize)
        {
            String errorStr = "The test and ifl collection size is not equal";
            System.out.println (errorStr);
            assertTrue(errorStr,false);
        }

        Iterator iflIterator = iflCollection.iterator();
        Iterator testIterator = testCollection.iterator();

        while (testIterator.hasNext()) {
            HashMap iflMap  = (HashMap)iflIterator.next();
            HashMap testMap = (HashMap)testIterator.next();
            Set set = iflMap.keySet() ; 
            Iterator iter = set.iterator(); 
            while (iter.hasNext())  {  
                String key = (String)iter.next();
                Object iflObject  = (Object)iflMap.get(key);
                Object testObject = (Object)testMap.get(key);

                // If null was expected, then we're cool
                if ((iflObject == null) && (testObject == null))
                {
                    continue;
                }

                // If only one of the values is null, then we're NOT cool
                if ((iflObject == null) || (testObject == null))
                {
                    String errorStr = "Unexpected data found for key '" + key + "'.";
                    System.out.println (errorStr);
                    assertTrue(errorStr,false);
                }

                // Else lets evaluate the data
                else
                {
                    // Check to see if we are dealing with an ArrayList
                    String oName = iflObject.getClass().getName();
                    if (oName.equals("java.util.ArrayList"))
                    {
                        Iterator listIt = ((ArrayList)iflObject).iterator();
                        while (listIt.hasNext()) {
                            String toName = (String)listIt.next();
                            List testValue = (List)testMap.get(key);
                            if (!(testValue.contains(toName))) {
                                String errorStr = "The key '" + key + "' is missing the entry '" + toName +"'";
                                System.out.println (errorStr);
                                assertTrue (errorStr,false);
                            }
                        }
                    }

                    // Else validate String (default) entries.
                    else
                    {
                        if (iflMap.get(key) instanceof String) {
                            String iflValue  = (String)iflMap.get(key);
                            String testValue = (String)testMap.get(key);
                            if (!(iflValue.equals(testValue)))
                            {
                                String errorStr = "Expected key '" + key + "' to be '" + testValue + "' found '" + iflValue + "'.";
                                System.out.println (errorStr);
                                assertTrue (errorStr,false);
                            }
                        } else {
                            Map iflValue  = (Map)iflMap.get(key);
                            Map testValue = (Map)testMap.get(key);
                            Set<Map.Entry> me = testValue.entrySet();
                            for (Map.Entry e : me) {
                                assertEquals("Should have config map key " + (String)e.getKey(), (String)e.getValue(), (String)testValue.get(e.getKey()));
                                iflValue.remove(e.getKey());
                            }
                            assertTrue("Extra values found", iflValue.isEmpty());
                        }
                    }
                }
            }  
        }
    }

    protected void validateFlowData(Map data, String fromService, String type, Object config, String name) throws Exception {
        
        String value = (String)data.get("FROM");
        assertEquals("Should have from service as " + fromService, fromService, value);
        
        //value = data.get("TO");
        //assertEquals("Should have to service as " + toService, toService, value);        
        
        value = (String)data.get("TYPE");
        assertEquals("Should have type as " + type, type, value);       
        
        value = (String)data.get("NAME");
        assertEquals("Should have name as " + name, name, value);       
        
        Map configData = (Map)data.get("CONFIG");
        if (config instanceof Map) {
            Map       configMap = (Map)config;
            Set<Map.Entry> me = configMap.entrySet();
            for (Map.Entry e : me) {
                assertEquals("Should have config map as " + (String)e.getKey(), (String)e.getValue(), (String)configData.get(e.getKey()));
            }
        } else {
            if (configData != null)
                value = (String)configData.get("exp");
            else
                value = null;
            assertEquals("Should have config as " + (String)config, (String)config, value);
        }
        
    }
    
    public void testSplitInlineIFLParsedModel() throws Exception {
        File iflFile = getTestIFLFile("split-inline.ifl");
        //IFLModel model = (new IFLReader()).read(iflFile);
        IFLParsedModel parsedModel = parseIFL(iflFile);
        Map<String, Map<String, Object>> splitMap = parsedModel.getSplitMap();
        printSplitMap(splitMap);
        assertEquals("Should have 1 splitdata ", 1, splitMap.size()); 
        String fromService = "batch-inbound";
        String toService ="po-process";
        Map<String, Object> splitData = splitMap.get("split$1");
        assertNotNull("Should have splitData for from service " + fromService, splitData);
        validateFlowData(splitData,fromService, "xpath", "/PurchaseOrder", "split$1");
    }

    public void testSplitExternalIFLParsedModel() throws Exception {
        File iflFile = getTestIFLFile("split-external.ifl");
        //IFLModel model = (new IFLReader()).read(iflFile);
        IFLParsedModel parsedModel = parseIFL(iflFile);
        Map<String, Map<String, Object>> splitMap = parsedModel.getSplitMap();
        printSplitMap(splitMap);
        assertEquals("Should have 1 splitdata ", 1, splitMap.size()); 
        String fromService = "batch-inbound";
        String toService ="po-process";
        Map<String, Object> splitData = splitMap.get("po-split");
        assertNotNull("Should have splitData for from service " + fromService, splitData);
        validateFlowData(splitData,fromService, "xpath", null, "po-split");     
    }

    public void testSplitInlineIFLModel() throws Exception {
        File iflFile = getTestIFLFile("split-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> splitList = model.getSplits().values();
        assertEquals("Should have 1 splitdata ", 1, splitList.size());
        String fromService = "batch-inbound";
        String toService ="po-process";
        Map<String, Object> splitData = splitList.iterator().next();
        assertNotNull("Should have splitData for from service " + fromService, splitData);
        validateFlowData(splitData,fromService, "xpath", "/PurchaseOrder", "split$1");
    }
    
    public void testSplitInlineWithExpIFLModel() throws Exception {
        File iflFile = getTestIFLFile("split-inline-exp.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> splitList = model.getSplits().values();
        assertEquals("Should have 1 splitdata ", 1, splitList.size()); 
        String fromService = "batch-inbound";
        String toService ="po-process";
        Map<String, Object> splitData = splitList.iterator().next();
        assertNotNull("Should have splitData for from service " + fromService, splitData);
        Map<String,String>  config = new HashMap();
        config.put("exp", "/PurchaseOrder");
        config.put("other", "5");
        validateFlowData(splitData,fromService, "xpath", config, "split$1");
    }

    public void testSplitExternalIFLModel() throws Exception {
        File iflFile = getTestIFLFile("split-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> splitList = model.getSplits().values();
        assertEquals("Should have 1 splitdata ", 1, splitList.size()); 
        String fromService = "batch-inbound";
        String toService ="po-process";
        Map<String, Object> splitData = splitList.iterator().next();
        assertNotNull("Should have splitData for from service " + fromService, splitData);
        validateFlowData(splitData,fromService, "xpath", null, "po-split");       
    } 

    public void testSplitAggregateExternalIFLModel() throws Exception {
        File iflFile = getTestIFLFile("split-aggregate-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> splitList = model.getSplits().values();
        Collection<Map<String, Object>> aggregateList = model.getAggregates().values();

        assertEquals("Should have 1 splitdata ", 1, splitList.size()); 
        assertEquals("Should have 1 aggregatedata ", 1, aggregateList.size()); 

        String fromService        = "a";
        String toServiceSplit     = "b";
        String toServiceAggregate = "c";
        String configSplit        = null;
        String configAggregate    = null;
        String typeSplit          = "xpath";
        String typeAggregate      = "xpath";
        String nameSplit          = "foo";
        String nameAggregate      = "bar";

        Map<String, Object> splitData = splitList.iterator().next();
        Map<String, Object> aggregateData = aggregateList.iterator().next();
        assertNotNull("Should have splitData for from service " + fromService, splitData);
        assertNotNull("Should have aggregateData for from service " + fromService, aggregateData);

        validateFlowData (splitData, 
                          fromService,
                          typeSplit, 
                          configSplit, 
                          nameSplit);    

        validateFlowData (aggregateData, 
                          fromService, 
                          typeAggregate, 
                          configAggregate, 
                          nameAggregate);       
    }


    // -----------------------
    // *** Select Tests Start
    // -----------------------

    private void selectInlineTest (Map<String, Map<String,Object>> selectMap) throws Exception {
        assertEquals("Should have 6 Select entries ", 6, selectMap.size());
        String fromService = null;
        String type        = "xpath";
        String config      = null;
        String name        = "myXpath";
        Map<String, Object> selectData = selectMap.get(name);
        validateFlowData (selectData, 
                          fromService,
                          type, 
                          config, 
                          name); 
        List<List<String>> routeMap = (List)selectData.get("WHEN");
        List<String>       entry = routeMap.get(0);
        assertEquals("1) Should have 2 route to strings ", 2, routeMap.size());
        String id = entry.get(0);
        String value = entry.get(1);
        String route = entry.get(2);
        assertEquals("   Type should be empty", "when", id);
        assertEquals("   Value should be abc", "abc", value);
        assertEquals("   Route should be route-1", "route-1", route);
        entry = routeMap.get(1);
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be empty", "when", id);
        assertEquals("   Value should be xyz", "xyz", value);
        assertEquals("   Route should be route-2", "route-2", route);

        name        = "myXpath2";
        selectData = selectMap.get(name);
        validateFlowData (selectData,
                          fromService,
                          type,
                          config,
                          name);
        routeMap = (List)selectData.get("WHEN");
        entry = routeMap.get(0);
        assertEquals("1) Should have 2 route to strings ", 2, routeMap.size());
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "xpath", id);
        assertEquals("   Value should be /abc", "/abc", value);
        assertEquals("   Route should be route-1", "route-1", route);
        entry = routeMap.get(1);
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "xpath", id);
        assertEquals("   Value should be /xyz", "/xyz", value);
        assertEquals("   Route should be route-2", "route-2", route);

        name        = "select$3";
        selectData = selectMap.get(name);
        config = "count(//Container/EUID) > 0";
        validateFlowData (selectData,
                          fromService,
                          type,
                          config,
                          name);
        routeMap = (List)selectData.get("WHEN");
        entry = routeMap.get(0);
        assertEquals("1) Should have 2 route to strings ", 2, routeMap.size());
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "when", id);
        assertEquals("   Value should be abc", "abc", value);
        assertEquals("   Route should be route-1", "route-1", route);
        entry = routeMap.get(1);
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "when", id);
        assertEquals("   Value should be xyz", "xyz", value);
        assertEquals("   Route should be route-2", "route-2", route);

        name        = "select$4";
        selectData = selectMap.get(name);
        config = "count(//Container/EUID) > 0";
        validateFlowData (selectData,
                          fromService,
                          type,
                          config,
                          name);
        routeMap = (List)selectData.get("WHEN");
        entry = routeMap.get(0);
        assertEquals("1) Should have 2 route to strings ", 2, routeMap.size());
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "xpath", id);
        assertEquals("   Value should be /abc", "/abc", value);
        assertEquals("   Route should be route-1", "route-1", route);
        entry = routeMap.get(1);
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "xpath", id);
        assertEquals("   Value should be /xyz", "/xyz", value);
        assertEquals("   Route should be route-2", "route-2", route);

        name        = "select$5";
        selectData = selectMap.get(name);
        config = null;
        type = "any";
        validateFlowData (selectData,
                          fromService,
                          type,
                          config,
                          name);
        routeMap = (List)selectData.get("WHEN");
        entry = routeMap.get(0);
        assertEquals("1) Should have 2 route to strings ", 2, routeMap.size());
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "xpath", id);
        assertEquals("   Value should be /abc", "/abc", value);
        assertEquals("   Route should be route-1", "route-1", route);
        entry = routeMap.get(1);
        id = entry.get(0);
        value = entry.get(1);
        route = entry.get(2);
        assertEquals("   Type should be xpath", "xpath", id);
        assertEquals("   Value should be /xyz", "/xyz", value);
        assertEquals("   Route should be route-2", "route-2", route);

        name        = "select$6";
        selectData = selectMap.get(name);
        config = null;
        validateFlowData (selectData,
                          fromService,
                          type,
                          config,
                          name);
        routeMap = (List)selectData.get("WHEN");
        assertEquals("1) Should have 0 route to strings ", 0, routeMap.size());

//        when = "";
//
//        routeToValue = (String)routeMap.get(when);
//        config  = "count(//Container/EUID) > 0";
//        name    = "select$2";
//        selectData = selectMap.get(name);
//        validateFlowData (selectData,
//                          fromService,
//                          type,
//                          config,
//                          name);
//        routeMap = (Map)selectData.get("WHEN");
//        when = "abc";
//        routeTo = "route-1";
//        routeToValue = (String)routeMap.get(when);
//        assertEquals("2) Should have 2 route to strings ", 2, routeMap.size());
//        assertEquals("The route to value for " + when + " should be " + routeTo + ".", routeTo, routeToValue);
//
//        routeToValue = (String)routeMap.get(when);
//        config  = null;
//        name    = "xpath2";
//        selectData = selectMap.get(name);
//        validateFlowData (selectData,
//                          fromService,
//                          type,
//                          config,
//                          name);
//        routeMap = (Map)selectData.get("WHEN");
//        when = "invoice";
//        routeTo = "log-invoices";
//        routeToValue = (String)routeMap.get(when);
//        String elseValue = "log-defaults";
//        assertEquals("3) Should have 2 route to strings ", 2, routeMap.size());
//        assertEquals("The route to value for " + when + " should be " + routeTo + ".", routeTo, routeToValue);
//
//        String elseFromMap = (String)selectData.get("ELSE");
//        assertEquals("The else value for " + when + " should be " + elseValue + ".", elseValue, elseFromMap);
    }


    private void selectExternalTest (Map<String, Map<String,Object>> selectMap) throws Exception {
        assertEquals("Should have 1 Select entries ", 1, selectMap.size()); 
        String fromService = null;
        String type        = "drools";
        String config      = null;
        String name        = "myRouteRule";
        Map<String, Object> selectData = selectMap.get(name);
        validateFlowData (selectData, 
                          fromService,
                          type, 
                          config, 
                          name);    
    }


    public void testSelectInlineIFLParsedModel() throws Exception {
        File iflFile = getTestIFLFile("select-inline.ifl");
        IFLParsedModel parsedModel = parseIFL(iflFile);
        Map<String, Map<String, Object>> selectMap = parsedModel.getSelectMap();
        displaySelectInfo(selectMap);
        selectInlineTest (selectMap);
    }

    
    public void testSelectExternalIFLParsedModel() throws Exception {
        File iflFile = getTestIFLFile("select-external.ifl");
        IFLParsedModel parsedModel = parseIFL(iflFile);
        Map<String, Map<String, Object>> selectMap = parsedModel.getSelectMap();
        displaySelectInfo(selectMap);
        selectExternalTest (selectMap);
    }
    

    public void testSelectInlineIFLModel() throws Exception {
        File iflFile = getTestIFLFile("select-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> selectMap = model.getSelects();
        displaySelectInfo(selectMap);
        selectInlineTest (selectMap);
    }

    public void testSelectExternalIFLModel() throws Exception {
        File iflFile = getTestIFLFile("select-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> selectMap = model.getSelects();
        displaySelectInfo(selectMap);
        selectExternalTest(selectMap);
    } 
    
    public void testSelectSyntaxIFLModel() throws Exception {
        File iflFile = getTestIFLFile("select-all-syntax.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> selectMap = model.getSelects();
        displaySelectInfo(selectMap);

        Map<String, Object> selectData = selectMap.get("xpath2");
        String elseValue = (String)selectData.get("ELSE");

//>             String elseValue = (String)select.get("ELSE");
//>             if (elseValue != null) {
//>                 select.put("ELSE", resolveServiceName(elseValue));
//>             }



        List<List<String>> whenMap = (List)selectData.get("WHEN");
        String invoice0 = whenMap.get(0).get(2);
        String invoice1 = whenMap.get(1).get(2);
        String invoice2 = whenMap.get(2).get(2);
        String invoice3 = whenMap.get(3).get(2);
        String invoice4 = whenMap.get(4).get(2);

        assertEquals("else should be log-defaults ",     "log-defaults", elseValue); 
        assertEquals("invoice0 should be create-delivery ",      "create-delivery",      invoice0); 
        assertEquals("invoice1 should be log-orders",   "log-orders",   invoice1); 
        assertEquals("invoice2 should be create-delivery ",      "create-delivery",      invoice2); 
        assertEquals("invoice3 should be log-defaults", "log-defaults", invoice3); 
        assertEquals("invoice4 should be route-1 ",      "route-1",      invoice4); 
    }


    public void testSelectDynamicRulesIFLModel() throws Exception {
        File iflFile = getTestIFLFile("select-dynamic-rules.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        Map<String, Map<String,Object>> selectMap = model.getSelects();

        Map<String,Object> select1 = selectMap.get("dynamic-type-selection1");
        String type   = (String)select1.get("TYPE");
        String config = (String)select1.get("CONFIG");
        String name   = (String)select1.get("NAME");
        String select = (String)select1.get("SELECT");
        String from   = (String)select1.get("FROM");
        List when     = (List)select1.get("WHEN");
        assertEquals("type dynamic-type-selection1", "any", type);
        assertEquals("config dynamic-type-selection1", null, config);
        assertEquals("name dynamic-type-selection1", "dynamic-type-selection1", name);
        assertEquals("select dynamic-type-selection1", "condition", select);
        assertEquals("from dynamic-type-selection1", null, from);
        assertTrue("when dynamic-type-selection1", when.isEmpty());  

        Map<String,Object> select2 = selectMap.get("dynamic-type-selection2");
        type   = (String)select2.get("TYPE");
        config = (String)select2.get("CONFIG");
        when   = (List)select2.get("WHEN");
        name   = (String)select2.get("NAME");
        select = (String)select2.get("SELECT");
        from   = (String)select2.get("FROM");
        assertEquals("type dynamic-type-selection2", "any", type);
        assertEquals("config dynamic-type-selection2", null, config);
        assertEquals("name dynamic-type-selection2", "dynamic-type-selection2", name);
        assertEquals("select dynamic-type-selection2", "condition", select);
        assertEquals("from dynamic-type-selection2", "inbound1", from);
        assertTrue("when dynamic-type-selection2", when.isEmpty());  

        Map<String,Object> select3 = selectMap.get("dynamic-type-selection3");
        type   = (String)select3.get("TYPE");
        config = (String)select3.get("CONFIG");
        when   = (List)select3.get("WHEN");
        name   = (String)select3.get("NAME");
        select = (String)select3.get("SELECT");
        from   = (String)select3.get("FROM");
        assertEquals("type dynamic-type-selection3", "any", type);
        assertEquals("config dynamic-type-selection3", null, config);
        assertEquals("name dynamic-type-selection3", "dynamic-type-selection3", name);
        assertEquals("select dynamic-type-selection3", "condition", select);
        assertEquals("from dynamic-type-selection3", "inbound2", from);
        String expectedValue = "[[regex, Text-, non-xml-handling]]";
        assertEquals("when dynamic-type-selection3", expectedValue, when.toString().trim());
    }


    // -----------------------
    // *** Aggregate Tests End
    // -----------------------

    public void testAggregateInlineIFLModel() throws Exception {
        File iflFile = getTestIFLFile("aggregate-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> list = model.getAggregates().values();
        assertEquals("Should have 1 aggregatedata ", 1, list.size()); 

        String fromService = "batch-inbound";
        String toService   = "po-process";
        String type        = "set";
        Map config      = new HashMap();
        config.put("count", "5");
        String name        = "aggregate$1";

        Map<String, Object> dataMap = list.iterator().next();
        assertNotNull("Should have aggregateData for from service " + fromService, dataMap);

        validateFlowData(dataMap,
                         fromService,
                         type,
                         config,
                         name);       
    } 


    public void testAggregateExternalIFLModel() throws Exception {
        File iflFile = getTestIFLFile("aggregate-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> list = model.getAggregates().values();
        assertEquals("Should have 1 aggregatedata ", 1, list.size()); 

        String fromService = "batch-inbound";
        String toService   = "po-process";
        String type        = "xpath";
        String config      = null;
        String name        = "po-aggregate";

        Map<String, Object> dataMap = list.iterator().next();
        assertNotNull("Should have aggregateData for from service " + fromService, dataMap);

        validateFlowData(dataMap,
                         fromService,
                         type,
                         config,
                         name);       
    } 

    
    public void testAggregateExternalIFLParsedModel() throws Exception {
        File iflFile = getTestIFLFile("aggregate-external.ifl");
        //IFLModel model = (new IFLReader()).read(iflFile);

        IFLParsedModel parsedModel = parseIFL(iflFile);
        Map<String, Map<String, Object>> parserModelMap = parsedModel.getAggregateMap();
        printAggregateMap(parserModelMap);
        assertEquals("Should have 1 aggregatedata ", 1, parserModelMap.size()); 

        String fromService = "batch-inbound";
        String toService   = "po-process";
        String type        = "xpath";
        String config      = null;
        String name        = "po-aggregate";

        Map<String, Object> dataMap = parserModelMap.get(name);
        assertNotNull("Should have aggregateData for from service " + fromService, dataMap);

        validateFlowData(dataMap,
                         fromService,
                         type,
                         config,
                         name);       
    } 
    

    public void testFilterInlineIFLModel() throws Exception {
        File iflFile = getTestIFLFile("filter-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> list = model.getFilters().values();
        assertEquals("Should have 1 filterdata ", 1, list.size()); 

        String fromService = "batch-inbound";
        String toService   = "po-process";
        String type        = "xpath";
        String config      = "/PurchaseOrder";
        String name        = "filter$1";

        Map<String, Object> dataMap = list.iterator().next();
        assertNotNull("Should have filterData for from service " + fromService, dataMap);
        validateFlowData(dataMap,
                         fromService,
                         type,
                         config,
                         name);       
    } 

    
    public void testFilterExternalIFLModel() throws Exception {
        File iflFile = getTestIFLFile("filter-external.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Collection<Map<String, Object>> list = model.getFilters().values();
        assertEquals("Should have 1 filterdata ", 1, list.size()); 

        String fromService = "batch-inbound";
        String toService   = "po-process";
        String type        = "xpath";
        String config      = null;
        String name        = "po-filter";

        Map<String, Object> dataMap = list.iterator().next();
        assertNotNull("Should have aggregateData for from service " + fromService, dataMap);

        validateFlowData(dataMap,
                         fromService,
                         type,
                         config,
                         name);       
    } 



    public void testFilterExternalIFLParsedModel() throws Exception {
        File iflFile = getTestIFLFile("filter-external.ifl");
        //IFLModel model = (new IFLReader()).read(iflFile);
        IFLParsedModel parsedModel = parseIFL(iflFile);
        Map<String, Map<String, Object>> parserModelMap = parsedModel.getFilterMap();
        printFilterMap(parserModelMap);
        assertEquals("Should have 1 filterdata ", 1, parserModelMap.size()); 

        String fromService = "batch-inbound";
        String toService   = "po-process";
        String type        = "xpath";
        String config      = null;
        String name        = "po-filter";

        Map<String, Object> dataMap = parserModelMap.get(name);
        assertNotNull("Should have filterData for from service " + fromService, dataMap);

        validateFlowData(dataMap,
                         fromService,
                         type,
                         config,
                         name);       
    } 


    public void testM2Demo() throws Exception {
        File iflFile = getTestIFLFile("m2.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        Map<String, Map.Entry<String, List<String>>> routes = model.getRoutes();
        assertTrue(routes.size() == 3);
        assertNotNull(model.getRoute("route$1"));
        assertNotNull(model.getRoute("route$2"));
        assertNotNull(model.getRoute("receive-manifest"));
    } 


    public void testGetRoute() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        
        Map<String, Map.Entry<String, List<String>>> routes = model.getRoutes();
        assertTrue(routes.size() == 5);
        
        assertNotNull(model.getRoute("hinbound2"));
    }
    

    public void testRoutePathOrder() throws Exception {
        File iflFile = getTestIFLFile("multi-routes.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        model.close();

        List<String> route1 = model.getRoute("hinbound1").getValue();
        assertTrue(route1.size() == 1);
        assertEquals(route1.get(0), "bprocess");
        
        List<String> route2 = model.getRoute("hinbound2").getValue();
        assertTrue(route2.size() == 2);

        assertEquals(route2.get(0), "{http://fuji.dev.java.net/application/ifl/private}maninthemiddle");
        assertEquals(route2.get(1), "bprocess");
    }
    

    public void testRoutePathWithFlow() throws Exception {
        File iflFile = getTestIFLFile("split-inline.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        model.close();
        List<String> route = model.getRoute("batch-inbound").getValue();
        assertTrue(route.size() == 2);

        assertEquals(route.get(0), "split$1");
        assertEquals(route.get(1), "{http://fuji.dev.java.net/application/ifl/private}po-process");
    }


    public void testSplitAggregateFilterIFLModel() throws Exception {
        File iflFile = getTestIFLFile("split-aggregate-filter.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        
        // Collect the data from the model
        Collection splitList     = model.getSplits().values();
        Collection aggregateList = model.getAggregates().values();
        Collection filterList    = model.getFilters().values();

        // Create the Map containing the expected data
        HashMap services = new HashMap() {{
            put("ftp",  Arrays.asList("foo", "extra-foo"));
            put("jruby",Arrays.asList("verify-address"));
            put("file", Arrays.asList("create-delivery"));
            put("email", Arrays.asList("email-operations"));
        }};
        final HashMap splitConfig = new HashMap() { { put("exp", "//orders/order"); }} ;
        HashMap split1 = new HashMap() { {
            put("CONFIG",    splitConfig);
            put("TYPE",     "xpath");
            put("NAME",     "split$1");
            put("EIP_ID",   "split$1");
            put("FROM",     "foo"); } }; 
        List<HashMap> splitCollection = Arrays.asList(split1);

        final HashMap aggregate1Config = new HashMap() { {
            put("count", "2");
            put("header","<report>");
            put("trailer","</rrepot>");
        }} ;
        HashMap aggregate1 = new HashMap() { {
            put("CONFIG",   aggregate1Config);
            put("TYPE",     "set");
            put("NAME",     "aggregate$1");
            put("EIP_ID",   "aggregate$1");
            put("FROM",     null); } }; 
        List<HashMap> aggregateCollection = Arrays.asList(aggregate1);

        final HashMap filter1Config = new HashMap() { { put("exp", "count(//order/euid) > 0"); }} ;
        final HashMap filter2Config = new HashMap() { { put("exp", "count(//order/euid) = 0"); }} ;
        HashMap filter1 = new HashMap() { {
                put("CONFIG",    filter1Config);
                put("TYPE",     "xpath");
                put("NAME",     "filter$1");
                put("EIP_ID",   "filter$1");
                put("FROM",     null); } };
        HashMap filter2 = new HashMap() { {
                put("CONFIG",   filter2Config);
                put("TYPE",     "xpath");
                put("NAME",     "filter$2");
                put("EIP_ID",   "filter$2");
                put("FROM",     null); } }; 
        List<HashMap> filterCollection = Arrays.asList(filter1,filter2);

        // Validate the services
        validateServices (model,services);

        // Validate the ifl data by comparing it to the test data created above
        validateData(splitList,splitCollection);
        validateData(aggregateList,aggregateCollection);
        validateData(filterList,filterCollection);
    }

    public void testNamedRoute() throws Exception {
        File iflFile = getTestIFLFile("named-route.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Entry<String, List<String>> route = model.getRoute("log");
        assertNotNull("Expecting Non Null route for route name log", route);
        //System.out.println("Route[Log] from " + route.getKey());
        List<String> toServices = route.getValue();
        //for ( String toService : toServices ) {
        //    System.out.println("  to " + toService);
        //}
    }

    protected void displayTeeInfo(Map<String, Map<String,Object>> teeMap) {
        for ( String tee : teeMap.keySet() ) {
            System.out.println("----- TEE DATA --------");
            System.out.println("Tee : " + tee);
            Map<String,Object> teeData = teeMap.get(tee);
            for ( String key : teeData.keySet() ) {
                System.out.println(" " + key + " = " + teeData.get(key));
            }
            System.out.println("---------------------");
        }
    }


    protected void displaySelectInfo(Map<String, Map<String,Object>> selectMap) {
        for ( String select : selectMap.keySet() ) {
            System.out.println("----- SELECT DATA --------");
            System.out.println("Select : " + select);
            Map<String,Object> selectData = selectMap.get(select);
            for ( String key : selectData.keySet() ) {
                System.out.println(" " + key + " = " + selectData.get(key));
            }
            System.out.println("---------------------");
        }
    }

    
    public void testTeeToExternalRoute() throws Exception {
        File iflFile = getTestIFLFile("tee-to-external-route.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> teeMap = model.getTees();
        displayTeeInfo(teeMap);
        assertEquals("Should have 1 tee entry", 1, teeMap.size());
        Map<String,Object> teeData1 = teeMap.get("tee$1");
        String route = (String) teeData1.get(Flow.Keys.TO.toString());
        assertEquals("Should have \"another-log\" for tee$1 route", "log", route);
    }

    public void testTeeToService() throws Exception {
        File iflFile = getTestIFLFile("tee-to-service.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> teeMap = model.getTees();
        displayTeeInfo(teeMap);
        assertEquals("Should have 1 tee entry", 1, teeMap.size());
        Map<String,Object> teeData1 = teeMap.get("tee$1");
        String service = (String) teeData1.get(Flow.Keys.TO.toString());
        assertEquals("Should have \"another-log\" for tee$1 service", "another-log", service);
    }

    public void testTeeToExternalAndService() throws Exception {
        File iflFile = getTestIFLFile("tee-to-external-and-service.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> teeMap = model.getTees();
        displayTeeInfo(teeMap);
        assertEquals("Should have 2 tee entry", 2, teeMap.size());
        Map<String,Object> teeData1 = teeMap.get("tee$1");
        String route = (String) teeData1.get(Flow.Keys.TO.toString());
        Map<String,Object> teeData2 = teeMap.get("tee$2");
        String service = (String) teeData2.get(Flow.Keys.TO.toString());
        assertEquals("Should have \"log\" for tee$1 external route", "log", route);
        assertEquals("Should have \"another-log\" for tee$2 service", "another-log", service);
    }

    public void testTeeToServiceWithConfig() throws Exception {
        File iflFile = getTestIFLFile("tee-to-service-with-config.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, Map<String,Object>> teeMap = model.getTees();
        displayTeeInfo(teeMap);
        assertEquals("Should have 1 tee entry", 1, teeMap.size());
        Map<String,Object> teeData1 = teeMap.get("tee$1");
        String service = (String) teeData1.get(Flow.Keys.TO.toString());
        assertEquals("Should have \"another-log\" for tee$1 service", "another-log", service);
        Object configData = teeData1.get(Flow.Keys.CONFIG.toString());
        String configDataString = configData.toString();
        assertEquals("The Config data should be {name=fred}.", "{name=fred}", configDataString);
    }


    public void testGroupModelPositive1() throws Exception {
        File iflFile = getTestIFLFile("group-services-positive.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        String expectedValue = "";

        List<String> serviceGroups = model.getServiceGroupNames();
        expectedValue =  "[myBPEL, myBPEL2, myBPEL3, myInfile, myInfile2, myOutfile]";
        String serviceString = serviceGroups.toString();
        assertEquals("The serviceGroups should be: .", expectedValue, serviceString);

        String serviceGroup = model.getGroupForConsumerService("svc2");
        expectedValue = "myBPEL";
        assertEquals("The serviceGroup value for svc2 should be: .", expectedValue, serviceGroup);

        serviceGroup = model.getGroupForProviderService("bpel1");
        expectedValue = "myBPEL";
        assertEquals("The serviceGroup value bpel1 should be: .", expectedValue, serviceGroup);

        serviceGroup = model.getGroupForConsumerService("bpel1");
        expectedValue = "";
        assertEquals("The serviceGroup value should be: .", expectedValue, serviceGroup);

        serviceGroup = model.getGroupForProviderService("svc2");
        expectedValue = "";
        assertEquals("The serviceGroup value should be: .", expectedValue, serviceGroup);

        serviceGroup = model.getGroupForService("svc20");
        expectedValue = "myBPEL2";
        assertEquals("The serviceGroup value for scv20 should be: .", expectedValue, serviceGroup);

        String groupName = model.getGroupForProviderService("bpel1");
        expectedValue = "myBPEL";
        assertEquals("The group name for bpel1 should be: .", expectedValue, groupName);

        List<String> providerList = model.getProvidersInServiceGroup("myBPEL");
        Collections.sort(providerList);
        expectedValue = "[bpel1, bpel2]";
        String providerString = providerList.toString();
        assertEquals("The provider list should contain [bpel1, bpel2].", expectedValue, providerString);

        List<String> consumerList = model.getConsumersInServiceGroup("myBPEL");
        Collections.sort(consumerList);
        expectedValue = "[svc1, svc2]";
        String consumerString = consumerList.toString();
        assertEquals("The consumers service in myBPEL should be: .", expectedValue, consumerString);

        List<String> providedServices = model.getProvidedServices();
        expectedValue = "[bpel1, bpel10, bpel2, bpel20, myInfile, myInfile2, myOutfile, route$2, route$3, route$4, svc10]";
        String providedString = providedServices.toString();
        assertEquals("The provider services should be: .", expectedValue, providedString);

        List<String> consumedServices = model.getConsumedServices();
        expectedValue = "[myInfile, myInfile2, svc1, svc10, svc2, svc20, svc30]";
        String consumedString = consumedServices.toString();
        assertEquals("The consumer services should be: .", expectedValue, consumedString);
        
        boolean consumedCheck = model.isConsumed("svc1");
        assertTrue("isConsumed for svc1 should be true.",consumedCheck);

        consumedCheck = model.isConsumed("bpel10");
        assertFalse("isConsumed for bpel10 should be false.",consumedCheck);

        boolean providedCheck = model.isProvided("svc1");
        assertFalse("isProvided for svc1 should be false.",providedCheck);

        providedCheck = model.isProvided("bpel10");
        assertTrue("isProvided for bpel10 should be true.",providedCheck);

    }

    
    public void testGroupServiceDefinitionPositive() throws Exception {
        File iflFile = getTestIFLFile("group-services-positive.ifl");
        IFLParsedModel parsedModel = parseIFL(iflFile);
        List providers = parsedModel.getProvidersInServiceGroup("myBPEL");
        Collections.sort(providers);
        String providerString = providers.toString();
        assertEquals("The provider list should contain [bpel1, bpel2].", "[bpel1, bpel2]", providerString);
        List consumers = parsedModel.getConsumersInServiceGroup("myBPEL");
        Collections.sort(consumers);
        String consumerString = consumers.toString();
        assertEquals("The consumer list should contain [svc1, svc2].", "[svc1, svc2]", consumerString);
        String groupName = parsedModel.getGroupNameForProviderService("bpel1");
        assertEquals("The group name should be myBPEL.", "myBPEL", groupName);
        groupName = parsedModel.getGroupNameForConsumerService("svc1");
        assertEquals("The group name should be myBPEL.", "myBPEL", groupName);
    }

    
    public void testGroupServiceDefinitionNegitiveTest1() throws Exception {
        File iflFile = getTestIFLFile("group-services-negitive1.ifl");
        try {
            IFLParsedModel parsedModel = parseIFL(iflFile);
        } catch (IFLParseException ex) {
            int index = ex.getMessage().indexOf("Duplicate group name found");
            boolean condition = (index == -1) ? false : true;
            assertTrue("Error should of occured due to a duplicate group name.",condition);
        }
    }
    

    public void testCallServices() throws Exception {

        File iflFile = getTestIFLFile("call-services-positive.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        List<String> serviceGroups = model.getServiceGroupNames();
        String expectedValue = "[buy-something, myBPEL2, transform1, transform2]";
        assertEquals("serviceGroups", expectedValue, serviceGroups.toString().trim());        

        String serviceGroup = "";
        serviceGroup = model.getGroupForService("clean-data");
        expectedValue = "transform2";
        assertEquals("serviceGroup", expectedValue, serviceGroup);        

        List<String> providerList = model.getProvidersInServiceGroup("transform2");
        Collections.sort(providerList);
        expectedValue = "[transform2]";
        assertEquals("providerList", expectedValue, providerList.toString().trim());        

        List<String> consumerList = model.getConsumersInServiceGroup("transform2");
        Collections.sort(consumerList);
        expectedValue = "[clean-data, convert-format]";
        assertEquals("consumerList", expectedValue, consumerList.toString().trim());        
    }


    private void namespaceModelTest (IFLModel model, String scope) {
        Map<String, String> namespacesForScope = model.getNamespacesForScope(scope);
        String value = namespacesForScope.toString();
        String correctValue = "{base-ns=http://fuji.dev.java.net/application/local, private-ns=/my-app/private/local, app3=/app3/local}";
        assertEquals("The namespacesForScope method should return: " + correctValue, correctValue, value);

        String namespaceValue = model.getNamespaceValue (scope,"app3");
        correctValue = "/app3/local";
        assertEquals("The getNamespaceValue method for app3 should return: " + correctValue, correctValue, namespaceValue);

        namespaceValue = model.getNamespaceValue (scope,"app4");
        correctValue = "/app4";
        assertEquals("The getNamespaceValue method for app4 should return: " + correctValue, correctValue, namespaceValue);

        namespaceValue = model.getNamespaceValue (scope,"private-ns");
        correctValue = "/my-app/private/local";
        assertEquals("The getNamespaceValue method for private-ns should return: " + correctValue, correctValue, namespaceValue);

        namespaceValue = model.getNamespaceValue (scope,"public-ns");
        correctValue = "/my-app";
        assertEquals("The getNamespaceValue method for public-ns should return: " + correctValue, correctValue, namespaceValue);

        Map<String, String> routeVisibilities = model.getRouteVisibilities ();
        value = routeVisibilities.toString();
        correctValue = "{infile=private}";
        assertEquals("The getRouteVisibilities method should return: " + correctValue, correctValue, value);

        String routeVisibility = model.getRouteVisibility ("infile");
        correctValue = "private";
        assertEquals("The getRouteVisibility method should return: " + correctValue, correctValue, routeVisibility);
    }


    public void testNamespacePositiveTest1() throws Exception {
        File iflFile = getTestIFLFile("namespace-test1-positive.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        String scope = iflFile.getAbsolutePath();
        String namespaceValue = model.getNamespaceValue (scope,"app3");
        String correctValue = "/app3/local";
        assertEquals("The getNamespaceValue method for app3 should return: " + correctValue, correctValue, namespaceValue);
    }


    public void testNamespacePositiveTest2() throws Exception {
        File file1 = getTestIFLFile("namespace-test1-positive.ifl");
        File file2 = getTestIFLFile("shared-namespaces.ifl");
        File[] fileList = new File[] { file1, file2 };
        IFLModel model = (new IFLReader()).read(fileList);

        String scope1 = file1.getAbsolutePath();
        String scope2 = model.getServiceScope("convert-order2");
        assertEquals("The scope values should be the same: ", scope1, scope2);
        namespaceModelTest (model, scope1);
    }


    /**
     * Same test as above, except this one will use the shared-namespaces in a different directory
     */
    public void testNamespacePositiveTest3() throws Exception {
        File file1 = getTestIFLFile("namespace-test1-positive.ifl");
        File file2 = getTestIFLFile("/multi/shared-namespaces.ifl");
        File[] fileList = new File[] { file1, file2 };
        IFLModel model = (new IFLReader()).read(fileList);

        String scope = file1.getAbsolutePath();
        namespaceModelTest (model, scope);
    }


    public void testResolvedNamespaceTest() throws Exception {
        File file1 = getTestIFLFile("namespace-test2-positive.ifl");
        File file2 = getTestIFLFile("shared-namespaces.ifl");
        File[] fileList = new File[] { file1, file2 };
        IFLModel model = (new IFLReader()).read(fileList);
        model.close();
        
        Map<String, Map.Entry<String, List<String>>> routes = model.getRoutes();
        List<String> route1 = model.getRoute("infile").getValue();

        assertTrue(route1.contains("{http://fuji.dev.java.net/application/my-app/private/app3/local}convert-order.myoperation"));
        assertTrue(route1.contains("{http://fuji.dev.java.net/application/my-app/private/app4}convert-order2"));
        assertTrue(route1.contains("{http://fuji.dev.java.net/application/Application_3}convert-order3"));
        assertTrue(route1.contains("{http://fuji.dev.java.net/application/my-app/private/Application_3}convert-order4"));
        assertTrue(route1.contains("{http://fuji.dev.java.net/application/my-app/private}purchase-order1"));
        assertTrue(route1.contains("{http://fuji.dev.java.net/application/my-app}purchase-order2"));
    }
   

    /**
     * This test should throw an exception since multiple copies of the shared-namespaces.ifl file 
     * are being processed.
     */
 //   public void testNamespacePositiveTest4() throws Exception {
 //       File file1 = getTestIFLFile("namespace-test1-positive.ifl");
 //       File file2 = getTestIFLFile("shared-namespaces.ifl");
 //       File file3 = getTestIFLFile("/multi/shared-namespaces.ifl");
 //       File[] fileList = new File[] { file1, file2, file3 };

 //       // Error condition should happen, since a second shared-namespaces.ifl file is being processed
 //       try {
 //           IFLModel model = (new IFLReader()).read(fileList);
 //       } catch (IFLParseException ex) {
 //           int index = ex.getMessage().indexOf("Multiple copies of the shared-namespaces");
 //           boolean condition = (index == -1) ? false : true;
 //           assertTrue("Error should of occured due to a duplicate service name.",condition);
 //       }
 //   }

    public void testVisibilityTest1() throws Exception {
        File iflFile = getTestIFLFile("visibility-public.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, String> routeVisibilities  = model.getRouteVisibilities();

        String value = routeVisibilities.toString();
        String correctValue = "{infile=public}";
        assertEquals("The getRouteVisibilities method should return: " + correctValue, correctValue, value);

        Map<String, String> serviceVisibilities = model.getServiceVisibilities();
        value = serviceVisibilities.toString();
        correctValue = "{infile=private, convert-order=public, convert-order2=private, purchase-order=private}";
        assertEquals("The getServiceVisibilities method should return: " + correctValue, correctValue, value);

        String routeVisibility = model.getRouteVisibility("infile");
        correctValue = "public";
        assertEquals("The getRouteVisibility method for infile should return: " + correctValue, correctValue, routeVisibility);

        String serviceVisibility = model.getServiceVisibility("convert-order");
        correctValue = "public";
        assertEquals("The getServiceVisibility method for convert-order should return: " + correctValue, correctValue, serviceVisibility);
    }

    public void testVisibilityTest2() throws Exception {
        File iflFile = getTestIFLFile("visibility-private.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, String> routeVisibilities  = model.getRouteVisibilities();
        String value = routeVisibilities.toString();
        String correctValue = "{infile=private}";
        assertEquals("The getRouteVisibilities method should return: " + correctValue, correctValue, value);

        Map<String, String> serviceVisibilities = model.getServiceVisibilities();
        value = serviceVisibilities.toString();
        correctValue = "{infile=private, convert-order=public, convert-order2=private, purchase-order=private}";
        assertEquals("The getServiceVisibilities method should return: " + correctValue, correctValue, value);

        String routeVisibility = model.getRouteVisibility("infile");
        correctValue = "private";
        assertEquals("The getRouteVisibility method for infile should return: " + correctValue, correctValue, routeVisibility);

        String serviceVisibility = model.getServiceVisibility("convert-order2");
        correctValue = "private";
        assertEquals("The getServiceVisibility method for convert-order should return: " + correctValue, correctValue, serviceVisibility);

        boolean privateServiceCheck = model.isServicePrivate("convert-order2");
        assertTrue("isServicePrivate for service convert-order2 should be true.",privateServiceCheck);

        boolean publicServiceCheck = model.isServicePublic("convert-order2");
        assertFalse("isServicePublic for service convert-order2 should be false.",publicServiceCheck);

        boolean privateRouteCheck = model.isRoutePrivate("infile");
        assertTrue("isRoutePrivate for route infile should be true.",privateServiceCheck);

        boolean publicRouteCheck = model.isRoutePublic("infile");
        assertFalse("isRoutePrivate for route infile should be false.",publicRouteCheck);
    }

    public void testVisibilityTest3() throws Exception {
        File iflFile = getTestIFLFile("visibility-default.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);
        Map<String, String> routeVisibilities  = model.getRouteVisibilities();
        String value = routeVisibilities.toString();
        String correctValue = "{infile=private}";
        assertEquals("The getRouteVisibilities method should return: " + correctValue, correctValue, value);

        Map<String, String> serviceVisibilities = model.getServiceVisibilities();
        value = serviceVisibilities.toString();
        correctValue = "{infile=private, convert-order=public, convert-order2=private, purchase-order=private}";
        assertEquals("The getServiceVisibilities method should return: " + correctValue, correctValue, value);

        String routeVisibility = model.getRouteVisibility("infile");
        correctValue = "private";
        assertEquals("The getRouteVisibility method for infile should return: " + correctValue, correctValue, routeVisibility);

        String serviceVisibility = model.getServiceVisibility("purchase-order");
        correctValue = "private";
        assertEquals("The getServiceVisibility method for convert-order should return: " + correctValue, correctValue, serviceVisibility);
    }

    public void testVisibilityTest4() throws Exception {
        File iflFile = getTestIFLFile("visibility-negitive.ifl");
        try {
            IFLParsedModel parsedModel = parseIFL(iflFile);
        } catch (IFLParseException ex) {
            int index = ex.getMessage().indexOf("Invalid service visibility type specified");
            boolean condition = (index == -1) ? false : true;
            assertTrue("Error should of occured due to a duplicate service name.",condition);
        }

    }

    public void testServiceGroupAndCall() throws Exception {
        File iflFile = getTestIFLFile("group-and-call-services.ifl");
        IFLModel model = (new IFLReader()).read(iflFile);

        List<String> javaServices = model.getServicesForType("java");
        Collections.sort(javaServices);
        String expectedValue = "[buy-something, clean-data, convert-format1, convert-format2, transform1, transform2]";
        assertEquals("javaServices", expectedValue, javaServices.toString().trim());        

        List<String> bpelServices = model.getServicesForType("bpel");
        Collections.sort(bpelServices);
        expectedValue = "[bpel11, bpel12, bpel21, bpel22, c21, c22]";
        assertEquals("bpelServices", expectedValue, bpelServices.toString().trim());        

        Map serviceGroups = model.getServiceGroups();
        Set keySet = serviceGroups.keySet();
        List serviceGroupList = new ArrayList(keySet); 
        Collections.sort(serviceGroupList);
        expectedValue = "[buy-something, myBPEL1, myBPEL2, transform1, transform2]";
        assertEquals("serviceGroupList", expectedValue, serviceGroupList.toString().trim());    

        List<String> serviceList;
        serviceList = model.getServicesInServiceGroup("buy-something");
        expectedValue = "[buy-something]";
        Collections.sort(serviceList);
        assertEquals("serviceList for buy-something", expectedValue, serviceList.toString().trim());

        serviceList = model.getServicesInServiceGroup("myBPEL1");
        expectedValue = "[bpel11, bpel12]";
        Collections.sort(serviceList);
        assertEquals("serviceList for myBPEL1", expectedValue, serviceList.toString().trim());

        serviceList = model.getServicesInServiceGroup("myBPEL2");
        expectedValue = "[bpel21, bpel22, c21, c22]";
        Collections.sort(serviceList);
        assertEquals("serviceList for myBPEL2", expectedValue, serviceList.toString().trim());

        serviceList = model.getServicesInServiceGroup("transform1");
        expectedValue = "[convert-format1, transform1]";
        Collections.sort(serviceList);
        assertEquals("serviceList for transform1", expectedValue, serviceList.toString().trim());

        serviceList = model.getServicesInServiceGroup("transform2");
        expectedValue = "[clean-data, convert-format2, transform2]";
        Collections.sort(serviceList);
        assertEquals("serviceList for transform2", expectedValue, serviceList.toString().trim());

        List<String> consumersServices = model.getConsumersInServiceGroup("transform2");
        Collections.sort(consumersServices);
        expectedValue = "[clean-data, convert-format2]";
        assertEquals("consumersServices for transform2", expectedValue, consumersServices.toString().trim());

        List<String> callServices = model.getServiceReferencesInServiceGroup("transform2");
        Collections.sort(callServices);
        expectedValue = "[clean-data, convert-format2]";
        assertEquals("callServices for transform2", expectedValue, callServices.toString().trim());
        
        consumersServices = model.getConsumersInServiceGroup("buy-something");
        Collections.sort(consumersServices);
        expectedValue = "[buy-something]";
        assertEquals("consumersServices for buy-something", expectedValue, consumersServices.toString().trim());

        callServices = model.getServiceReferencesInServiceGroup("buy-something");
        Collections.sort(callServices);
        expectedValue = "[]";
        assertEquals("callServices for buy-something", expectedValue, callServices.toString().trim());

        assertEquals("Expected group name", "buy-something", model.getGroupForService("buy-something"));
        assertEquals("Expected group name", "transform1", model.getGroupForService("transform1"));
        assertEquals("Expected group name", "transform1", model.getGroupForService("convert-format1"));

        assertEquals("Expected group name", "transform2", model.getGroupForService("transform2"));
        assertEquals("Expected group name", "transform2", model.getGroupForService("convert-format2"));
        assertEquals("Expected group name", "transform2", model.getGroupForService("clean-data"));

        assertEquals("Expected group name", "myBPEL1", model.getGroupForService("bpel11"));
        assertEquals("Expected group name", "myBPEL1", model.getGroupForService("bpel12"));

        assertEquals("Expected group name", "myBPEL2", model.getGroupForService("bpel21"));
        assertEquals("Expected group name", "myBPEL2", model.getGroupForService("bpel22"));
        assertEquals("Expected group name", "myBPEL2", model.getGroupForService("c21"));
        assertEquals("Expected group name", "myBPEL2", model.getGroupForService("c22"));

        String group = "buy-something";
        List<String> providers = model.getProvidersInServiceGroup(group);
        List<String> consumers = model.getConsumersInServiceGroup(group);
        assertEquals("Expected 1 provider", 0, providers.size());
        assertEquals("Expected 0 consumer", 1, consumers.size());

        group = "transform1";
        providers = model.getProvidersInServiceGroup(group);
        consumers = model.getConsumersInServiceGroup(group);
        assertEquals("Expected 1 provider", 0, providers.size());
        assertEquals("Expected 1 consumer", 1, consumers.size());

        group = "transform2";
        providers = model.getProvidersInServiceGroup(group);
        consumers = model.getConsumersInServiceGroup(group);
        assertEquals("Expected 1 provider", 1, providers.size());
        assertEquals("Expected 2 consumer", 2, consumers.size());

        group = "myBPEL1";
        providers = model.getProvidersInServiceGroup(group);
        consumers = model.getConsumersInServiceGroup(group);
        assertEquals("Expected 2 provider", 2, providers.size());
        assertEquals("Expected 0 consumer", 0, consumers.size());

        group = "myBPEL2";
        providers = model.getProvidersInServiceGroup(group);
        consumers = model.getConsumersInServiceGroup(group);
        assertEquals("Expected 2 provider", 2, providers.size());
        assertEquals("Expected 2 consumer", 2, consumers.size());
   }

}
