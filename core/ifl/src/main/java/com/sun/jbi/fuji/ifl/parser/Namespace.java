/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.io.File;

/**
 * @author markrs
 */

public class Namespace {

    private String  mNamespaceId     = null;
    private File    mNamespaceFile   = null;
    private String  mNamespaceKey    = null;
    private String  mNamespaceValue  = null;

    public Namespace () {
    }

    public Namespace (String filename, String key, String value)
    {
        filename = IFLUtil.trimQuotes(filename);
        key      = IFLUtil.trimQuotes(key);
        value    = IFLUtil.trimQuotes(value);

        setId (IFLUtil.getIdFromMap("namespace"));
        setFilename (filename);
        setKey (key);
        setValue (value);
        
    }

    public void setId (String id) {
        mNamespaceId = id;
    }

    public void setFilename (String filename) {
        File newFile = new File(filename);
        mNamespaceFile = newFile;
    }

    public void setKey (String key) {
        mNamespaceKey = key;
    }

    public void setValue (String value) {
        mNamespaceValue = value;
    }


    public String getId () {
        return mNamespaceId;
    }

    public File getFile () {
        return mNamespaceFile;
    }

    public String getPath () {
        String filename = mNamespaceFile.getPath();
        return filename;
    }

    public String getName () {
        String name = mNamespaceFile.getName();
        return name;
    }

    public String getKey () {
        return mNamespaceKey;
    }

    public String getValue () {
        return mNamespaceValue;
    }

    public String toString () {
        String outString = "";
        outString += "Namespace Id       : " + mNamespaceId + "\n";
        outString += "Namespace Filename : " + getName() + "\n";
        outString += "Namespace Path     : " + getPath() + "\n";
        outString += "Namespace Key      : " + mNamespaceKey + "\n";
        outString += "Namespace Value    : " + mNamespaceValue + "\n";
        return outString;
    }

}
