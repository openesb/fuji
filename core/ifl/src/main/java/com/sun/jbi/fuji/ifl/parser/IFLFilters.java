/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Filter;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLFilters {

    private Map<String, Filter> mFiltersMap = new LinkedHashMap<String, Filter>();


    public IFLFilters() {
    }

    public void add (Filter filter) {
        String id = filter.getId();
        mFiltersMap.put(id,filter);
        IFLUtil.incrementIdInMap("filter");
    }

    public boolean containsKey (String key) {
        return mFiltersMap.containsKey(key);
    }

    public boolean containsName (String key) {
        List<String> nameList = getNames();
        return nameList.contains(key);
    }

    public int getCount () {
        int totalFilters = mFiltersMap.size();
        return totalFilters;
    }

    public void putAll (Map<String,Filter> filters) {
        mFiltersMap.putAll(filters);
    }

    public void putAll (IFLFilters filters) {
        mFiltersMap.putAll(filters.getMap());
    }

    public Map<String,Filter> getMap() {
        return mFiltersMap;
    }

    public List getFilters() {
        List list = new ArrayList();
        for (Map.Entry<String, Filter> entry : mFiltersMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Filter getFilter (String filterId) {
        Filter filter = mFiltersMap.get(filterId);
        return filter;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        Collection c = mFiltersMap.values();
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            Filter filter = (Filter)itr.next();
            String name = filter.getName();
            nameList.add(name);
        }
        return nameList;
    }

    public List<String> getIds() {
        List idList = new ArrayList();
        for (Map.Entry<String, Filter> entry : mFiltersMap.entrySet()) {
            idList.add(entry.getKey());
        }
        return idList;
    }

    public String toString() {
        String outString = "";
        Set<String> set = mFiltersMap.keySet();
        for (String key : set)
        {
            Filter filter = mFiltersMap.get(key);
            outString += "\n" + filter.toString();
        }
        return outString;
    }

}
