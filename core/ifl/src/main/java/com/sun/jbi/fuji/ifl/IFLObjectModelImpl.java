/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLObjectModelImpl.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import com.sun.jbi.fuji.ifl.parser.IFLServices;
import com.sun.jbi.fuji.ifl.parser.IFLRoutes;
import com.sun.jbi.fuji.ifl.parser.IFLBroadcasts;
import com.sun.jbi.fuji.ifl.parser.IFLTees;
import com.sun.jbi.fuji.ifl.parser.IFLSelects;
import com.sun.jbi.fuji.ifl.parser.IFLSplits;
import com.sun.jbi.fuji.ifl.parser.IFLAggregates;
import com.sun.jbi.fuji.ifl.parser.IFLFilters;
import com.sun.jbi.fuji.ifl.parser.IFLServiceGroups;
import com.sun.jbi.fuji.ifl.parser.IFLConnections;
import com.sun.jbi.fuji.ifl.parser.IFLNamespaces;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.parser.IFLParsedObjectModel;
import com.sun.jbi.fuji.ifl.Localizer;
import java.lang.Integer;
import java.util.logging.Logger;


/**
 * This class implements the IFLObjectModel interface and builds the processing model
 * from the IFLParsedObjectModel.
 * 
 * @author markrs
 */
public class IFLObjectModelImpl implements IFLObjectModel {

    private IFLServices      mServices      = new IFLServices();
    private IFLRoutes        mRoutes        = new IFLRoutes();
    private IFLBroadcasts    mBroadcasts    = new IFLBroadcasts();
    private IFLTees          mTees          = new IFLTees();
    private IFLSelects       mSelects       = new IFLSelects();
    private IFLSplits        mSplits        = new IFLSplits();
    private IFLFilters       mFilters       = new IFLFilters();
    private IFLAggregates    mAggregates    = new IFLAggregates();
    private IFLServiceGroups mServiceGroups = new IFLServiceGroups();
    private IFLConnections   mConnections   = new IFLConnections();
    private IFLNamespaces    mNamespaces    = new IFLNamespaces();
    
    /** logger */
    private Logger mLogger = Logger.getLogger(IFLObjectModelImpl.class.getPackage().getName());
    private static final Localizer sLoc_ = Localizer.get();

    protected String mApplicationId = "";

    public IFLObjectModelImpl() {
        IFLUtil.initializeIdMap();
    }

    public IFLRoutes getIFLRoutes () throws IFLParseException {
        try {
            mRoutes.resolveNamespaces (mNamespaces, mApplicationId, mServices);
        }
        catch (IFLParseException ex) {
            String message = ex.getMessage();
            mLogger.fine(message);
            throw new IFLParseException(message); 
        }
        return mRoutes;
    }

    public IFLServices getIFLServices () {
        return mServices;
    }

    public IFLBroadcasts getIFLBroadcasts () {
        return mBroadcasts;
    }

    public IFLTees getIFLTees () {
        return mTees;
    }

    public IFLSelects getIFLSelects () {
        return mSelects;
    }

    public IFLSplits getIFLSplits () {
        return mSplits;
    }

    public IFLFilters getIFLFilters () {
        return mFilters;
    }

    public IFLAggregates getIFLAggregates () {
        return mAggregates;
    }

    public IFLServiceGroups getIFLServiceGroups () {
        return mServiceGroups;
    }

    public IFLConnections getIFLConnections () {
        return mConnections;
    }

    public IFLNamespaces getIFLNamespaces () {
        return mNamespaces;
    }

    public String getApplicationId() {
        return mApplicationId;
    }

    public void setApplicationId(String appId) {
        mApplicationId = appId;
    }

    /**
     * Add the IFL parsed object model data to the object model
     * @param parsedObjectModel
     */
    public void addIFLParsedModel (IFLParsedObjectModel parsedObjectModel) throws IFLParseException {

        mApplicationId = parsedObjectModel.getApplicationId();

        mRoutes.putAll (parsedObjectModel.getRoutes());
        mServices.putAll (parsedObjectModel.getServices());
        mServiceGroups.putAll (parsedObjectModel.getServiceGroups());
        mConnections.putAll (parsedObjectModel.getConnections());
        mBroadcasts.putAll (parsedObjectModel.getBroadcasts());
        mTees.putAll (parsedObjectModel.getTees());
        mSelects.putAll (parsedObjectModel.getSelects());
        mSplits.putAll (parsedObjectModel.getSplits());
        mFilters.putAll (parsedObjectModel.getFilters());
        mAggregates.putAll (parsedObjectModel.getAggregates());
        mNamespaces.putAll (parsedObjectModel.getNamespaces());
    }
    
}
