/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author markrs
 */

public class Connection {

    private String mConnectionName = null;
    private List<ConnectionEntry> mConnectionEntries = new ArrayList<ConnectionEntry>();


    public Connection () {
    }

    public Connection (String connectionName)
    {
        setName (connectionName);
    }


    public String toString () {
        String outString = "";
        outString += "Connection Name   : " + mConnectionName + "\n";
        int entryNum = 0;
        for (ConnectionEntry entry: mConnectionEntries) {
            outString += "  ------(" + entryNum++ + ")------\n";
            outString += entry.toString();
        }
        return outString;
    }

    public void setName (String connectionName) {
        mConnectionName = connectionName;
    }

    public void addEntry (String name, String type) {
        ConnectionEntry entry = new ConnectionEntry (name, type);
        mConnectionEntries.add(entry);
    }

    public String getName () {
        return mConnectionName;
    }

    public List<ConnectionEntry> getEntries () {
        return mConnectionEntries;
    }

    public int totalEntries () {
        return mConnectionEntries.size();
    }


    // Inner class to hold the route entries
    public class ConnectionEntry {
        private String  mEnteyName      = null;
        private String  mEntryType      = null;

        public ConnectionEntry (String entryName,
                                String entryType) {
            setEntryName (entryName);
            setEntryType (entryType);
        }

        public String toString () {
            String outString = "";
            outString += "  Entry Name : " + mEnteyName + "\n";
            outString += "  Entry Type : " + mEntryType + "\n";
            return outString;
        }

        public void setEntryName (String entryName) {
            mEnteyName = entryName;
        }

        public void setEntryType (String entryType) {
            mEntryType = entryType;
        }

        public String getEntryName () {
            return mEnteyName;
        }

        public String getEntryType () {
            return mEntryType;
        }

    }



}
