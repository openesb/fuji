/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Select;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLSelects {

    private Map<String, Select> mSelectsMap = new LinkedHashMap<String, Select>();


    public IFLSelects() {
    }

    public void add (Select select) {
        String id = select.getId();
        mSelectsMap.put(id,select);
        IFLUtil.incrementIdInMap("select");
    }

    public boolean containsKey (String key) {
        return mSelectsMap.containsKey(key);
    }

    public boolean containsName (String name) {
        List<String> nameList = getNames();
        return nameList.contains(name);
    }

    public int getCount () {
        int totalSelects = mSelectsMap.size();
        return totalSelects;
    }

    public Select get (String name) {
        return mSelectsMap.get(name);
    }

    public void putAll (Map<String,Select> selects) {
        mSelectsMap.putAll(selects);
    }

    public void putAll (IFLSelects selects) {
        mSelectsMap.putAll(selects.getMap());
    }

    public Map<String,Select> getMap() {
        return mSelectsMap;
    }

    public List getSelects() {
        List list = new ArrayList();
        for (Map.Entry<String, Select> entry : mSelectsMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Select getSelect (String selectId) {
        Select select = mSelectsMap.get(selectId);
        return select;
    }

    public List<Select> getSelectsByName (String name) {
        List list = new ArrayList();
        for (Map.Entry<String, Select> entry : mSelectsMap.entrySet()) {
            Select selectEntry = entry.getValue();
            if (selectEntry.getName().equals(name)) {
                list.add(entry.getValue());
            }
        }
        return list;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        Collection c = mSelectsMap.values();
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            Select select = (Select)itr.next();
            String name = select.getName();
            nameList.add(name);
        }
        return nameList;
    }

    public List<String> getIds() {
        List idList = new ArrayList();
        for (Map.Entry<String, Select> entry : mSelectsMap.entrySet()) {
            idList.add(entry.getKey());
        }
        return idList;
    }

    public String toString() {
        String outString = "";
        Set<String> set = mSelectsMap.keySet();
        for (String key : set)
        {
            Select select = mSelectsMap.get(key);
            outString += "\n" + select.toString();
        }
        return outString;
    }

}
