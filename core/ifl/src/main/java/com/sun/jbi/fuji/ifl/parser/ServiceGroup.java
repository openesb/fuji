/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Service;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.List;
import java.util.ArrayList;


/**
 * @author markrs
 */

public class ServiceGroup {

    private String mGroupName = null;
    private List<Service> mServices = new ArrayList();
    private List<String> mCallServiceNames = new ArrayList();

    public ServiceGroup(String groupName) {
        groupName = IFLUtil.trimQuotes(groupName);
        setName(groupName);
    }

    public String toString () {
        String outString = "";
        outString += "Service Group Name : " + mGroupName + "\n";
        outString += "  Consumed Services: \n";;
        for (Service service: mServices) {
            if (service.isConsumed()) {
                outString += "    " + service.getName() + "\n";
            }
        }
        outString += "  Provided Services: \n";;
        for (Service service: mServices) {
            if (service.isProvided()) {
                outString += "    " + service.getName() + "\n";
            }
        }
        outString += "  Call Services: \n";;
        for (String name: mCallServiceNames) {
                outString += "    " + name + "\n";
        }
        return outString;
    }

    public void addService (Service service) {
        mServices.add(service);
    }

    public void addCallService (String serviceName) {
        mCallServiceNames.add(serviceName);
    }

    public void setName (String groupName) {
        mGroupName = groupName;
    }


    public String getName () {
        return mGroupName;
    }

    public List<Service> getConsumed () {
        List consumedList = new ArrayList();
        for (Service service: mServices) {
            if (service.isConsumed()) {
                consumedList.add(service);
            }
        }
        return consumedList;
    }

    public List<String> getConsumedNames () {
        List consumedList = new ArrayList();
        for (Service service: mServices) {
            if (service.isConsumed()) {
                consumedList.add(service.getName());
            }
        }
        return consumedList;
    }

    public List<Service> getProvided () {
        List providedList = new ArrayList();
        for (Service service: mServices) {
            if (service.isProvided()) {
                providedList.add(service);
            }
        }
        return providedList;
    }

    public List<String> getProvidedNames () {
        List providedList = new ArrayList();
        for (Service service: mServices) {
            if (service.isProvided()) {
                providedList.add(service.getName());
            }
        }
        return providedList;
    }

    public List<Service> getServices () {
        return mServices;
    }

    public List<String> getServiceNames () {
        List list = new ArrayList();
        for (Service service: mServices) {
            list.add(service.getName());
        }
        return list;
    }

    public List<String> getCallServiceNames () {
        return mCallServiceNames;
    }



}
