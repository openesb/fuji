/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * @author markrs
 */

public class Aggregate {

    private String     mAggregateId        = null;
    private String     mAggregateName      = null;
    private String     mFromService        = null;
    private String     mAggregateType      = null;
    private String     mAggregateRouteId   = null;
    private Properties mAggregateConfig    = new Properties();

    public Aggregate () {
    }

    public Aggregate (Map<String, Object> aggregateData)
    {
        setName ((String)aggregateData.get(Flow.Keys.NAME.toString()));
        setFromServiceName ((String)aggregateData.get(Flow.Keys.FROM.toString()));
        setConfig ((Properties)aggregateData.get(Flow.Keys.CONFIG.toString()));
        setType ((String)aggregateData.get(Flow.Keys.TYPE.toString()));
        setId (IFLUtil.getIdFromMap("aggregate"));
        setRouteId (IFLUtil.getIdFromMap("route"));

        // If the aggregate does not have a name, then use the id as the name
        if (mAggregateName == null) {
            setName (getId());
        }

    }

    
    public void setName (String aggregateName) {
        mAggregateName = aggregateName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setConfig (Properties aggregateConfig) {
        if (aggregateConfig != null)
            mAggregateConfig.putAll(aggregateConfig);
    }

    public void setType (String aggregateType) {
        mAggregateType = aggregateType;
    }

    public void setId (String id) {
        mAggregateId = id;
    }

    public void setRouteId (String routeId) {
        mAggregateRouteId = routeId;
    }


    public String getName() {
        return mAggregateName;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public Properties getConfig () {
        return mAggregateConfig;
    }

    public String getType () {
        return mAggregateType;
    }

    public String getId () {
        return mAggregateId;
    }

    public String getRouteId () {
        return mAggregateRouteId;
    }

    public String toString () {
        String outString = "";
        outString += "Aggregate Id     : " + mAggregateId + "\n";
        outString += "Aggregate Name   : " + mAggregateName + "\n";
        outString += "From Service     : " + mFromService + "\n";
        outString += "Aggregate Type   : " + mAggregateType + "\n";
        outString += "Route Id         : " + mAggregateRouteId + "\n";
        outString += "Aggregate Config : " + mAggregateConfig + "\n";
        return outString;
    }

}
