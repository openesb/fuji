/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Service;
import com.sun.jbi.fuji.ifl.Localizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;


/**
 * @author markrs
 */

public class IFLServices {

    private Map<String, Service> mServicesMap = new LinkedHashMap<String, Service>();

    public IFLServices() {
    }

    public void add (Service service) {
        String name = service.getName();
        mServicesMap.put(name,service);
    }

    public boolean containsKey (String key) {
        return mServicesMap.containsKey(key);
    }

    public boolean containsName (String name) {
        List<String> nameList = getNames();
        return nameList.contains(name);
    }

    public Service getService (String name) {
        Service service = mServicesMap.get(name);
        return service;
    }

    public int getCount () {
        int totalServices = mServicesMap.size();
        return totalServices;
    }

    public void putAll (Map<String,Service> services) {
        mServicesMap.putAll(services);
    }

    public void putAll (IFLServices services) {
        mServicesMap.putAll(services.getMap());
    }

    public Map<String,Service> getMap() {
        return mServicesMap;
    }

    public List getServices() {
        List list = new ArrayList();
        for (Map.Entry<String, Service> entry : mServicesMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        for (Map.Entry<String, Service> entry : mServicesMap.entrySet()) {
            nameList.add(entry.getKey());
        }
        return nameList;
    }

    public List<Service> getServicesByName (String name) {
        List list = new ArrayList();
        for (Map.Entry<String, Service> entry : mServicesMap.entrySet()) {
            Service serviceEntry = entry.getValue();
            if (serviceEntry.getName().equals(name)) {
                list.add(entry.getValue());
            }
        }
        return list;
    }

    public List<Service> getServicesByType (String type) {
        List list = new ArrayList();
        for (Map.Entry<String, Service> entry : mServicesMap.entrySet()) {
            Service serviceEntry = entry.getValue();
            if (serviceEntry.getType().equals(type)) {
                list.add(entry.getValue());
            }
        }
        return list;
    }

    public List<String> getServiceNamesByType (String type) {
        List list = new ArrayList();
        for (Map.Entry<String, Service> entry : mServicesMap.entrySet()) {
            Service serviceEntry = entry.getValue();
            if (serviceEntry.getType().equals(type)) {
                list.add(serviceEntry.getName());
            }
        }
        return list;
    }

    public List<Service> getConsumed() {
        List<Service> consumedList = new ArrayList();
        for (Service service: mServicesMap.values()) {
            if (service.isConsumed()) {
                consumedList.add(service);
            }
        }
        return consumedList;
    }

    public List<Service> getProvided() {
        List<Service> providedList = new ArrayList();
        for (Service service: mServicesMap.values()) {
            if (service.isProvided()) {
                providedList.add(service);
            }
        }
        return providedList;
    }

    public List<Service> getUnknnown() {
        List<Service> unknownList = new ArrayList();
        for (Service service: mServicesMap.values()) {
            if (!(service.isProvided()) && !(service.isConsumed())) {
                unknownList.add(service);
            }
        }
        return unknownList;
    }


    public String toString() {
        String outString = "";
        Set<String> set = mServicesMap.keySet();
        for (String key : set)
        {
            Service service = mServicesMap.get(key);
            outString += "\n" + service.toString();
        }
        return outString;
    }


}
