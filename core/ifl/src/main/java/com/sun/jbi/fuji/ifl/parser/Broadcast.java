/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author markrs
 */

public class Broadcast {

    private String       mBroadcastId        = null;
    private String       mBroadcastName      = null;
    private String       mFromService        = null;
    private String       mBroadcastType      = null;
    private String       mBroadcastRouteId   = null;
    private Properties   mBroadcastConfig    = new Properties();
    private List<String> mServiceNames       = new ArrayList();

    public Broadcast () {
    }

    public Broadcast (Map<String, Object> broadcastData, List<String> serviceNames)
    {
        setName ((String)broadcastData.get(Flow.Keys.NAME.toString()));
        setFromServiceName ((String)broadcastData.get(Flow.Keys.FROM.toString()));
        setConfig ((Properties)broadcastData.get(Flow.Keys.CONFIG.toString()));
        setType ((String)broadcastData.get(Flow.Keys.TYPE.toString()));
        setServiceNames (serviceNames);
        setId (IFLUtil.getIdFromMap("broadcast"));
        setRouteId (IFLUtil.getIdFromMap("route"));

        // If the broadcast does not have a name, then use the id as the name
        if (mBroadcastName == null) {
            setName (getId());
        }

    }

    
    public void setName (String broadcastName) {
        mBroadcastName = broadcastName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setServiceNames (List<String> serviceNames) {
        mServiceNames.addAll(IFLUtil.trimQuotes(serviceNames));
    }

    public void setConfig (Properties broadcastConfig) {
        if (broadcastConfig != null)
            mBroadcastConfig.putAll(broadcastConfig);
    }

    public void setType (String broadcastType) {
        mBroadcastType = broadcastType;
    }

    public void setId (String id) {
        mBroadcastId = id;
    }

    public void setRouteId (String routeId) {
        mBroadcastRouteId = routeId;
    }


    public String getName() {
        return mBroadcastName;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public List<String> getServiceNames() {
        return mServiceNames;
    }

    public Properties getConfig () {
        return mBroadcastConfig;
    }

    public String getType () {
        return mBroadcastType;
    }

    public String getId () {
        return mBroadcastId;
    }

    public String getRouteId () {
        return mBroadcastRouteId;
    }

    public String toString () {
        String outString = "";
        outString += "Broadcast Id     : " + mBroadcastId + "\n";
        outString += "Broadcast Name   : " + mBroadcastName + "\n";
        outString += "From Service     : " + mFromService + "\n";
        outString += "Broadcast Type   : " + mBroadcastType + "\n";
        outString += "Route Id         : " + mBroadcastRouteId + "\n";
        outString += "Broadcast Config : " + mBroadcastType + "\n";
        outString += "To Services      :\n";
        int index = 0;
        for (String service: mServiceNames) {
            outString += "    " + index + ") " + service + "\n";
            index++;
        }
        return outString;
    }

}
