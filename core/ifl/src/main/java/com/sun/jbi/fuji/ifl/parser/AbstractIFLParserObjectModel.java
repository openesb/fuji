/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractIFLParser2.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.IFLServices;
import com.sun.jbi.fuji.ifl.parser.IFLRoutes;
import com.sun.jbi.fuji.ifl.parser.IFLBroadcasts;
import com.sun.jbi.fuji.ifl.parser.IFLTees;
import com.sun.jbi.fuji.ifl.parser.IFLSelects;
import com.sun.jbi.fuji.ifl.parser.IFLSplits;
import com.sun.jbi.fuji.ifl.parser.IFLAggregates;
import com.sun.jbi.fuji.ifl.parser.IFLFilters;
import com.sun.jbi.fuji.ifl.parser.IFLServiceGroups;
import com.sun.jbi.fuji.ifl.parser.IFLConnections;
import com.sun.jbi.fuji.ifl.parser.IFLNamespaces;

import com.sun.jbi.fuji.ifl.parser.Service;
import com.sun.jbi.fuji.ifl.parser.Route;
import com.sun.jbi.fuji.ifl.parser.Broadcast;
import com.sun.jbi.fuji.ifl.parser.Tee;
import com.sun.jbi.fuji.ifl.parser.Select;
import com.sun.jbi.fuji.ifl.parser.Split;
import com.sun.jbi.fuji.ifl.parser.Aggregate;
import com.sun.jbi.fuji.ifl.parser.Filter;
import com.sun.jbi.fuji.ifl.parser.ServiceGroup;
import com.sun.jbi.fuji.ifl.parser.Connection;
import com.sun.jbi.fuji.ifl.parser.Namespace;

import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.lang.Integer;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import com.sun.jbi.fuji.ifl.Localizer;
import java.io.File;


/**
 * This class is the base class which will be extended by the JavaCC generated IFLParser
 * class to populate the parsed model for the IFL from the parser.
 * 
 * @author markrs
 */
public abstract class AbstractIFLParserObjectModel implements IFLParsedObjectModel 
{
    private IFLServices      mServices      = new IFLServices();
    private IFLRoutes        mRoutes        = new IFLRoutes();
    private IFLBroadcasts    mBroadcasts    = new IFLBroadcasts();
    private IFLTees          mTees          = new IFLTees();
    private IFLSelects       mSelects       = new IFLSelects();
    private IFLSplits        mSplits        = new IFLSplits();
    private IFLFilters       mFilters       = new IFLFilters();
    private IFLAggregates    mAggregates    = new IFLAggregates();
    private IFLServiceGroups mServiceGroups = new IFLServiceGroups();
    private IFLConnections   mConnections   = new IFLConnections();
    private IFLNamespaces    mNamespaces    = new IFLNamespaces();

    protected static final String[] mVisibilityTypes = {IFLUtil.VISIBILITY_PUBLIC, IFLUtil.VISIBILITY_PRIVATE};        
    protected static final String[] mNamespaceTypes  = {IFLUtil.PUBLIC_NS, IFLUtil.PRIVATE_NS, IFLUtil.BASE_NS};

    protected static final Logger LOG = Logger.getLogger(AbstractIFLParserObjectModel.class.getName());
    protected static final Localizer sLoc_ = Localizer.get();
    
    /** The name of the file is used as the scope value */
    protected String mScopeName = null;
    protected String mApplicationId = null;
    
    /**
     * this method is the starting point for parsing the ifl file. Calling this will reconstrcut the
     * ifl parsed model by invoking the javacc parser implementation which populates the parsed model.
     * 
     * @throws com.sun.jbi.fuji.ifl.parser.IFLParseException
     */
    public abstract void parse() throws IFLParseException;


    public String getScopeName() {
        return this.mScopeName;
    }

    public IFLServices getServices () {
        return mServices;
    }

    public IFLRoutes getRoutes () {
        return mRoutes;
    }

    public IFLBroadcasts getBroadcasts () {
        return mBroadcasts;
    }

    public IFLTees getTees () {
        return mTees;
    }

    public IFLSelects getSelects () {
        return mSelects;
    }

    public IFLSplits getSplits () {
        return mSplits;
    }

    public IFLFilters getFilters () {
        return mFilters;
    }

    public IFLAggregates getAggregates () {
        return mAggregates;
    }

    public IFLServiceGroups getServiceGroups () {
        return mServiceGroups;
    }

    public IFLConnections getConnections () {
        return mConnections;
    }

    public IFLNamespaces getNamespaces () {
        return mNamespaces;
    }

    public String getApplicationId() {
        return mApplicationId;
    }


    public void setScopeName (String filename) {
       // System.out.println ("*** filename: " + filename);
        mScopeName = filename;

        // Traverse up to retrieve and set the application name.
        String applicationId = "";
        File applicationFile = new File(filename);
        File parentFile = applicationFile.getParentFile();
        boolean done = false;
        while ((parentFile != null) && (!(done)))
        {
            if (parentFile.getName().equalsIgnoreCase("src")) {
                File applicationDir = parentFile.getParentFile();
                applicationId = applicationDir.getName();
                done = true;
            }
            else {
                parentFile = parentFile.getParentFile();
            }
        }
        mApplicationId = applicationId;
    }


    protected void addNamespace (String namespaceKey, 
                                 String namespaceValue) throws Exception
    {
        // Create the new namespace and add it to the namespace list
        String filename = getScopeName();
        Namespace newNamespace = new Namespace (filename,
                                                namespaceKey,
                                                namespaceValue);
        mNamespaces.add(newNamespace);

    }

    /**
     * Add a service definition
     * @param serviceName
     * @param serviceType
     * @param serviceType
     */
    protected void addService (String serviceName, 
                               String serviceType,
                               String serviceVisibility) throws Exception
    {
        // Validate that the visibility type is either "public" or "private"
        if (!((Arrays.asList(mVisibilityTypes)).contains(serviceVisibility))) {
            String errorMsg = sLoc_.t("0008: Invalid service visibility type specified: {0}.",serviceVisibility);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }

        // Create the new service and add it to the service list
        String serviceIFLFile = getScopeName();
        Service newService = new Service(serviceName,
                                         serviceType,
                                         serviceVisibility,
                                         serviceIFLFile);
        mServices.add(newService);
    }


    protected void addServicesToGroup (String groupName,
                                       List<String> providesServiceList,
                                       List<String> consumesServiceList,
                                       List<String> unknownServiceList) throws Exception
    {

        /* First create the new Service Group */
        ServiceGroup newGroup = new ServiceGroup(groupName);

        /* Next fill it up with the already created services */
        if (providesServiceList != null)
        {
            for (String serviceName: providesServiceList) {
                serviceName = IFLUtil.trimQuotes(serviceName);
                Service service = mServices.getService(serviceName);
                if (service != null) {
                    service.setProvided(true);
                    newGroup.addService(service);
                    service.setGroupName(groupName);
                }
            }
        }

        /* Now do the same for the consumes services */
        if (consumesServiceList != null)
        {
            for (String serviceName: consumesServiceList) {
                serviceName = IFLUtil.trimQuotes(serviceName);
                Service service = mServices.getService(serviceName);
                if (service != null) {
                    service.setConsumed(true);
                    newGroup.addService(service);
                    newGroup.addCallService(service.getName());
                    service.setGroupName(groupName);
                }
            }
        }

        /* And finally the unknown services */
        if (unknownServiceList != null)
        {
            for (String serviceName: unknownServiceList) {
                serviceName = IFLUtil.trimQuotes(serviceName);
                Service service = mServices.getService(serviceName);
                if (service != null) {
                    newGroup.addService(service);
                    service.setGroupName(groupName);
                }
            }
        }

        /* Now we need to add the newGroup into the serviceGroups obuect */
        mServiceGroups.add(newGroup);
    }


    /**
     * Add route information
     * @param fromService
     * @param toServiceList
     * @param name route name in the ifl
     * @return route name . can be null if a default route name should be generated
     */
    protected String addRoute (boolean inBroadcast,
                               String fromService, 
                               List<String> toServices, 
                               List<String> routePaths, 
                               String routeName,
                               String visibility,
                               List<String> eipIds) throws Exception 
    {
        // Validate the visibility identifier
        if (!((Arrays.asList(mVisibilityTypes)).contains(visibility))) {
            String errorMsg = sLoc_.t("0007: Invalid route type specified: {0}.",visibility);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }

        // Create the new route and add it to the route list
        Route newRoute = new Route (inBroadcast,
                                    routeName,
                                    fromService,
                                    visibility,
                                    toServices,
                                    routePaths,
                                    eipIds);

        // Add the filename to the route object.
        newRoute.setFilename (getScopeName());

        // Everything is cool, so go ahead and add the route 
        mRoutes.add(newRoute);

        // Retriece the route name and the from service name
        String newRouteId         = newRoute.getId();
        String newFromServiceName = newRoute.getFromServiceName();

        // Set the "to" services to be a "Provided" service and also add the collection information
        int totalRoutes = newRoute.getCount();
        if (totalRoutes > 0) {
            Connection newConnection = new Connection (newRouteId);
            if (newFromServiceName != null) {
                newConnection.addEntry(newFromServiceName,"from");
            }
            for (Route.RouteEntry entry: newRoute.getRouteEntries()) {
                String serviceName = entry.getServiceName();
                if (serviceName != null)
                {
                    Service service = mServices.getService(serviceName);
                    if (service != null) {
                        service.setProvided(true);
                    }
                    newConnection.addEntry(serviceName,"to");
                }
            }
            mConnections.add(newConnection);
        }

        // If the "from" service is not null, then set it as a "Consumed" service
        if (newFromServiceName != null) {
            Service service = mServices.getService(newFromServiceName);
            if (service != null) {
                service.setConsumed(true);
            }
        }

        return newRouteId;
    }


    /**
     * Add Broadcast data
     * @param broadcastData
     * @param toServices
     */
    protected String addBroadcast (Map<String, Object> broadcastData, 
                                   List<String> toServices) throws IFLParseException
    {
        // Create the new Broadcast object
        Broadcast newBroadcast = new Broadcast (broadcastData, toServices);

        // Everything is cool, so add the Broadcast object to the mBroadcasts map.
        mBroadcasts.add(newBroadcast);

        return newBroadcast.getId();
    }


    /**
     * Add Tee data
     * @param broadcastData
     * @param endPointRef
     */
    protected String addTee (Map<String, Object> teeData, 
                             String endPointRef) throws IFLParseException
    {
        // Create the new Tee object
        Tee newTee = new Tee (teeData, endPointRef);

        // Everything is cool, so add the Tee object to the mTees map.
        mTees.add(newTee);

        return newTee.getId();
    }


    /**
     * Add Select data
     * @param selectData
     * @param toServices
     */
    protected String addSelect (Map<String, Object> selectData, 
                                List<String> selectConditions) throws IFLParseException
    {
        Select newSelect = null;
        try {
            newSelect = new Select (selectData, selectConditions);
        }
        catch (IFLParseException ex) {
            String message = ex.getMessage();
            String logMessage = sLoc_.t(message);
            LOG.fine(logMessage);
            throw new IFLParseException(message); 
        }

        // Everything is cool, so add the Select object to the mSelects map.
        mSelects.add(newSelect);

        return newSelect.getId();
    }


    /**
     * Add Split data
     * @param splitData
     */
    protected String addSplit (Map<String, Object> splitData) throws IFLParseException
    {
        // Create the new Split object
        Split newSplit = new Split (splitData);

        // Everything is cool, so add the Split object to the mSplits map.
        mSplits.add(newSplit);

        return newSplit.getId();
    }


    /**
     * Add Filter data
     * @param filterData
     */
    protected String addFilter (Map<String, Object> filterData) throws IFLParseException
    {
        // Create the new Filter object
        Filter newFilter = new Filter (filterData);

        // Everything is cool, so add the Filter object to the mFilters map.
        mFilters.add(newFilter);

        return newFilter.getId();
    }


    /**
     * Add Aggregate data
     * @param aggregateData
     */
    protected String addAggregate (Map<String, Object> aggregateData)  throws IFLParseException
    {
        // Create the new Aggregate object
        Aggregate newAggregate = new Aggregate (aggregateData);

        // Everything is cool, so add the Aggregate object to the mAggregates map.
        mAggregates.add(newAggregate);

        return newAggregate.getId();
    }


}
