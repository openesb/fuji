/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLParseException.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.ifl.parser;

/**
 * This exception class provides the nested exception interface from the 
 * javacc parser exception and other exceptions while parsing the ifl file.
 * //TODO: add more error formating and query interface here.
 * @author chikkala
 */
public class IFLParseException extends Exception {
    /** ifl file path/name for which this exception belongs */
    private String mIFLFile = null;
    
    public IFLParseException(Throwable cause) {
        super(cause);
    }

    public IFLParseException (String cause) {
        super(cause);
    }

    /**
     * getter for the ifl file name/path for which this exception is created.
     * @return 
     */
    public String getIflFile() {
        return mIFLFile;
    }
    /**
     * set the iflFile path for which this exception is created. default is null.
     * @param iflFile
     */
    public void setIflFile(String iflFile) {
        this.mIFLFile = iflFile;
    }

}
