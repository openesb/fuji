/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author markrs
 */

public class Select {

    private String     mSelectId            = null;
    private String     mSelectName          = null;
    private String     mFromService         = null;
    private String     mSelectType          = null;
    private String     mSelectConditionType = null;
    private String     mSelectElse          = null;
    private String     mSelectRouteId       = null;
    private Properties mSelectConfig        = new Properties();
    private List<SelectCondition> mSelectConditions = new ArrayList<SelectCondition>();

    public Select () {
    }

    public Select (Map<String, Object> selectData, List<String> selectConditions) throws IFLParseException 
    {
        setName ((String)selectData.get(Flow.Keys.NAME.toString()));
        setFromServiceName ((String)selectData.get(Flow.Keys.FROM.toString()));
        setConfig ((Properties)selectData.get(Flow.Keys.CONFIG.toString()));
        setType ((String)selectData.get(Flow.Keys.TYPE.toString()));
        //setConditionType ((String)selectData.get(Flow.Keys.SELECT.toString()));
        setConditions (selectConditions);
        setId (IFLUtil.getIdFromMap("select"));
        setRouteId (IFLUtil.getIdFromMap("route"));

        // If the select does not have a name, then use the id as the name
        if (mSelectName == null) {
            setName (getId());
        }

    }

    public String toString () {
        String outString = "";
        outString += "Select Id     : " + mSelectId + "\n";
        outString += "Select Name   : " + mSelectName + "\n";
        outString += "From Service  : " + mFromService + "\n";
        outString += "Select Type   : " + mSelectType + "\n";
        outString += "Condition Type: " + mSelectConditionType + "\n";
        outString += "Route Id      : " + mSelectRouteId + "\n";
        outString += "Select Config : " + mSelectConfig + "\n";
        int entryNum = 0;
        for (SelectCondition entry: mSelectConditions) {
            outString += "  ------(" + entryNum++ + ")------\n";
            outString += entry.toString();
        }
        return outString;
    }

    public void setName (String selectName) {
        mSelectName = selectName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setConditions (List<String> selectConditions) throws IFLParseException {

        boolean  valueBased = false;
        boolean  conditionBased = false;

        // Iterate through the selectCondition list to create the "SelectCondition" objects
        for (Iterator it = selectConditions.iterator(); it.hasNext(); ) {
            String conditionClause  = IFLUtil.trimQuotes((String)it.next());
            String conditionType    = null;
            String conditionValue   = null;
            String conditionService = null;
            if (!(conditionClause.equalsIgnoreCase(Flow.Keys.WHEN.toString()))) {
                if (!(conditionClause.equalsIgnoreCase(Flow.Keys.ELSE.toString()))) {
                    conditionType   = conditionClause;
                    conditionClause = Flow.Keys.WHEN.toString().toLowerCase();
                    conditionBased  = true;
                }
            }
            else {
                valueBased = true;
            }

            if (valueBased && conditionBased) {
                String errorMsg = "Select when clauses can only contain values or conditions, not both.";
                throw new IFLParseException(errorMsg);
            } else if (valueBased == false && conditionBased == false) {
                conditionBased = true;
            }

            if (it.hasNext()) {
                conditionValue = IFLUtil.trimQuotes((String)it.next());
            }
            if (it.hasNext()) {
                conditionService = IFLUtil.trimQuotes((String)it.next());
            }
            if (conditionValue == null || conditionService == null) {
                String errorMsg = "Select clause is missing value and/or route service";
                throw new IFLParseException(errorMsg);
            }
            SelectCondition newSelectionCondition = new SelectCondition(conditionClause,
                                                                        conditionType,
                                                                        conditionValue,
                                                                        conditionService);
            mSelectConditions.add(newSelectionCondition);
        }

        if (valueBased) {
            setConditionType("value");
        }
        else {
            setConditionType("condition");
        }
    }

    public void setConfig (Properties selectConfig) {
        if (selectConfig != null)
            mSelectConfig.putAll(selectConfig);
    }

    public void setType (String selectType) {
        mSelectType = selectType;
    }

    public void setConditionType (String selectConditionType) {
        mSelectConditionType = selectConditionType;
    }

    public void setId (String id) {
        mSelectId = id;
    }

    public void setRouteId (String routeId) {
        mSelectRouteId = routeId;
    }

    public String getName() {
        return mSelectName;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public List<SelectCondition> getConditions() {
        return mSelectConditions;
    }

    public Properties getConfig () {
        return mSelectConfig;
    }

    public String getType () {
        return mSelectType;
    }

    public String getConditionType () {
        return mSelectConditionType;
    }

    public String getId () {
        return mSelectId;
    }

    public String getRouteId () {
        return mSelectRouteId;
    }

    // Inner class to hold the select condition information
    public class SelectCondition {
        private String  mConditionClause    = null;
        private String  mConditionType      = null;
        private String  mConditionValue     = null;
        private String  mConditionToService = null;

        public SelectCondition (String conditionClause,
                                String conditionType,
                                String conditionValue,
                                String conditionToService) {
            setConditionClause (conditionClause);
            setConditionType (conditionType);
            setConditionValue (conditionValue);
            setConditionToService (conditionToService);
        }

        public String toString () {
            String outString = "";
            outString += "  Condition Clause     : " + mConditionClause + "\n";
            outString += "  Condition Type       : " + mConditionType + "\n";
            outString += "  Condition Value      : " + mConditionValue + "\n";
            outString += "  Condition To Service : " + mConditionToService + "\n";
            return outString;
        }

        public void setConditionClause (String conditionClause) {
            mConditionClause = conditionClause;
        }

        public void setConditionType (String conditionType) {
            mConditionType = conditionType;
        }

        public void setConditionValue (String conditionValue) {
            mConditionValue = conditionValue;
        }

        public void setConditionToService (String toService) {
            mConditionToService = toService;
        }

        public String getConditionClause () {
            return mConditionClause;
        }

        public String getConditionType () {
            return mConditionType;
        }

        public String getConditionValue () {
            return mConditionValue;
        }

        public String getConditionToService () {
            return mConditionToService;
        }


    }
}
