/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * @author markrs
 */

public class Split {

    private String     mSplitId       = null;
    private String     mSplitName     = null;
    private String     mFromService   = null;
    private String     mSplitType     = null;
    private Properties mSplitConfig   = new Properties();
    private String     mSplitRouteId  = null;


    public Split () {
    }

    public Split (Map<String, Object> splitData)
    {
        setName ((String)splitData.get(Flow.Keys.NAME.toString()));
        setFromServiceName ((String)splitData.get(Flow.Keys.FROM.toString()));
        setConfig ((Properties)splitData.get(Flow.Keys.CONFIG.toString()));
        setType ((String)splitData.get(Flow.Keys.TYPE.toString()));
        setId (IFLUtil.getIdFromMap("split"));
        setRouteId (IFLUtil.getIdFromMap("route"));

        // If the split does not have a name, then use the id as the name
        if (mSplitName == null) {
            setName (getId());
        }

    }

    public void setName (String splitName) {
        mSplitName = splitName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setConfig (Properties splitConfig) {
        if (splitConfig != null)
            mSplitConfig.putAll(splitConfig);
    }

    public void setType (String splitType) {
        mSplitType = splitType;
    }

    public void setId (String id) {
        mSplitId = id;
    }

    public void setRouteId (String routeId) {
        mSplitRouteId = routeId;
    }


    public String getName() {
        return mSplitName;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public Properties getConfig () {
        return mSplitConfig;
    }

    public String getType () {
        return mSplitType;
    }

    public String getId () {
        return mSplitId;
    }

    public String getRouteId () {
        return mSplitRouteId;
    }

    public String toString () {
        String outString = "";
        outString += "Split Id     : " + mSplitId + "\n";
        outString += "Split Name   : " + mSplitName + "\n";
        outString += "From Service : " + mFromService + "\n";
        outString += "Split Type   : " + mSplitType + "\n";
        outString += "Route Id     : " + mSplitRouteId + "\n";
        outString += "Split Config : " + mSplitConfig + "\n";
        return outString;
    }


}
