/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLModel.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.ifl;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This interface represents the model that will be derived from the ifl files.
 * All the required ifl files will be parsed, validated and conbined together 
 * to construct this model which will be used in various points like in the
 * service artifacts generation (in fuji:dist goal) or in i-app build validation
 * phase etc.
 * <p>
 * <pre>
 * IFLReader iflReader = new IFLReader();
 * IFLModel model = iflReader.read(new File("/ifldir"));
 * model = iflReader.read(new File("/dir/myapp.ifl"));
 * model = iflReader.read(new File("/dir/1.ifl"), new File("/dir/2.ifl"));
 * </pre>
 * 
 * @see IFLReader
 * 
 * @author chikkala
 */
public interface IFLModel extends IFLObjectModel {
    /**
     * returns all the service types used in the ifl.
     * @return Set of service type names.
     */
    Set<String> getServiceTypes();

    /**
     * given a servicetype, returns the list of all the services of this type
     * defined in the ifl.
     * @param serviceType service type name
     * @return List of services or empty list.
     */
    List<String> getServicesForType(String serviceType);

    /**
     * Given a service return its type.
     * @param service name
     * @return serviceType
     */
    String getTypeForService(String service);

    /**
     * return all the consumer service to provider service connections 
     * defined in the ifl. 
     * @return Set of Map.Entry object whose key is a consumed service name and the
     * value is a Provider service name. ( from and last to service name in the route
     * block in ifl)
     */
    Set<Map.Entry<String, Set<String>>> getConnections();
    
    /**
     * returns all the consumed services defined in the ifl
     * @return List of service names
     */
    List<String> getConsumedServices();

    /**
     * returns all the provided services defined in the ifl
     * @return List of service names
     */
    List<String> getProvidedServices();

    /**
     * checks whether the given service is a consumed service or not.
     * @param serviceName
     * @return true if it is consumed service else false
     */
    boolean isConsumed(String serviceName);

    /**
     * checks whether the given service is a provided service or not.
     * @param serviceName
     * @return true if it is provided service else false
     */    
    boolean isProvided(String serviceName);   

    /**
     * returns a map of all split constructs in the ifl. The map is keyed by
     * split name and the values represent the data for a split contruct 
     * in the ifl which consists of name/value pairs for the following 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, 
     *  CONFIG=<inline text>, NAME="<split name>")
     * @return a list of split data as a Map with name value pairs that provide
     * the split data.
     */
    Map<String, Map<String,Object>> getSplits();

    /**
     * returns a map of all aggregate constructs in the ifl. The map is keyed by
     * aggregate name and the values represent the data for a aggregate contruct 
     * in the ifl which consists of name/value pairs for the following 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, 
     *  CONFIG=<inline text>, NAME="<aggregate name>")
     * @return a list of aggregate data as a Map with name value pairs that provide
     * the aggregate data.
     */
    Map<String, Map<String,Object>> getAggregates();
    
    /**
     * returns a map of all filter constructs in the ifl. The map is keyed by
     * filter name and the values represent the data for a filter contruct 
     * in the ifl which consists of name/value pairs for the following 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, 
     *  CONFIG=<inline text>, NAME="<filter name>")
     * @return a list of filter data as a Map with name value pairs that provide
     * the filter data.
     */
    Map<String, Map<String,Object>> getFilters();

    /**
     * Returns a list of all broadcasts defined in the IFL definition.
     * @return list of broadcasts keyed by broadcast name
     */
    Map<String, Map<String,Object>> getBroadcasts();

    /**
     * Returns a list of all tees defined in the IFL definition.
     * @return list of tees keyed by tee name
     */
    Map<String, Map<String,Object>> getTees();

    /**
     * returns a map of all select (CBR) constructs in the ifl. The map is keyed by
     * select name and the values represent the data for a select contruct 
     * in the ifl which consists of name/value pairs for the following 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, 
     *  CONFIG=<inline text>, NAME="<select name>")
     * @return a list of select data as a Map with name value pairs that provide
     * the select data.
     */
    Map<String, Map<String,Object>> getSelects();

    /**
     * Returns split data for a specific split definition.
     * @return split data as a Map with name value pairs that provide
     * the split data.
     * @see IFLModel#getSplits()
     */
    Map<String, Object> getSplit(String name);
    
    /**
     * Returns select data for a specific select definition.
     * @return select data as a Map with name value pairs that provide
     * the select data.
     * @see IFLModel#getSelects()
     */
    Map<String, Object> getSelect(String name);

    /**
     * Returns aggregate data for a specific aggregate definition.
     * @return aggregate data as a Map with name value pairs that provide
     * the aggregate data.
     * @see IFLModel#getAggregates()
     */
    Map<String, Object> getAggregate(String name);
    
    /**
     * Returns filter data for a specific filter definition.
     * @return filter data as a Map with name value pairs that provide
     * the filter data.
     * @see IFLModel#getAggregates()
     */
    Map<String, Object> getFilter(String name);

    /**
     * Returns route paths for all route definitions in the parsed IFL.  The 
     * route path is a list of names, corresponding to the services and flow 
     * constructs in a given route.  The list returned retains the order of 
     * the route definition.
     * @return a map containing route paths, keyed by route name
     */
    Map<String, Map.Entry<String, List<String>>> getRoutes();
    Map<String, Map.Entry<String, List<String>>> getEipIds();
    
    /**
     * Returns route path for a specific route definition.  The 
     * route path is a list of names, corresponding to the services and flow 
     * constructs in a given route.  The list returned retains the order of 
     * the route definition.
     * @param name route name
     * @return route path
     */
    Map.Entry<String, List<String>> getRoute(String name);
    Map.Entry<String, List<String>> getEipId(String name);
    
    /**
     * Returns a list containing all the Service Group Names.
     * @return list of service group names
     */
    List<String> getServiceGroupNames();

    /**
     * Returns a list of all the providers in a service group.
     * @param name of service group
     * @return list of service group names
     */
    List<String> getProvidersInServiceGroup (String groupName);

    /**
     * Returns a list of all the consumers in a service group.
     * @param name of service group
     * @return list of service group names
     */
    List<String> getConsumersInServiceGroup (String groupName);

    /**
     * Returns a list of all the unknown services in a service group.
     * An unknown service is a service that has been defined, but not used.
     * @param name of service group
     * @return list of service group names
     */    
    List<String> getUnknownInServiceGroup (String groupName);

    /**
     * Returns a list of the services defined after the "call" statement for the specified group
     * @param name of service group
     * @return list of service group names
     */
    List<String> getServiceReferencesInServiceGroup (String groupName);

    /**
     * Returns a list of all services in the specified group
     * @param name of service group
     * @return list of service group names
     */
    List<String> getServicesInServiceGroup (String groupName);

    /**
     * Returns the group name for a specified provider service.
     * @param name of service group
     * @return group name
     */
    String getGroupForProviderService (String serviceName);

    /**
     * Returns the group name for a specified consumer service.
     * @param name of service group
     * @return group name
     */
    String getGroupForConsumerService (String serviceName);

    /**
     * Returns the group name for a specified service.
     * @param name of service group
     * @return group name
     */
    String getGroupForService (String serviceName);

    /**
     * Returns the complete Service Group Map.
     * @return service group map data
     */
    Map<String, Map<String, List<String>>> getServiceGroups();

    /**
     * Will return the scope name (filename) for a specified service.
     * @param serviceName the service name
     * @return the scope name (filename)
     */
    String getServiceScope (String serviceName);

    /**
     * Returns the complete Route Visibility Map.
     * @return map of service visibilities
     */
    Map<String, String> getRouteVisibilities();

    /**
     * Returns the route visibility for the given route name
     * @return service visibility value
     */
    String getRouteVisibility (String routeName);

    /**
     * Returns the complete Service Visibility Map.
     * @return map of service visibilities
     */
    Map<String, String> getServiceVisibilities();

    /**
     * Returns the service visibility for the given service name
     * @return service visibility value
     */
    String getServiceVisibility (String serviceName);

    /**
     * Returns true if the given route is public.
     * @param name
     * @return true if the visibility is public
     */
    boolean isRoutePublic(String name);

    /**
     * Returns true if the given route is private.
     * @param name
     * @return true if the visibility is private
     */
    boolean isRoutePrivate(String name);

    /**
     * Returns true if the given service is public.
     * @param name
     * @return true if the visibility is public
     */
    boolean isServicePublic(String name);

    /**
     * Returns true if the given service is private.
     * @param name
     * @return true if the visibility is private
     */
    boolean isServicePrivate(String name);

    /**
     * Returns a map of maps containing all namespaces
     * @return Map of Map <<scope>, <namespace key, namespace value>>
     */
    Map<String, Map<String, String>> getNamespaces ();

    /**
     * Returns the namespaces that were defined in a given scope (filename)
     * @return Map of namespaces <key> <value>
     */
    Map<String, String> getNamespacesForScope (String scopeName);

    /**
     * Returns the namespace value, given the scope and namespace key.
     * @return namespace value
     */
    String getNamespaceValue (String scopeName, String namespaceKey);

    String getNamespace(String serviceName);
    void close();
}
