/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Namespace;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLNamespaces {

    private Map<String, Namespace> mNamespacesMap = new LinkedHashMap<String, Namespace>();


    public IFLNamespaces() {
    }

    public void add (Namespace namespace) {
        String id = namespace.getId();
        mNamespacesMap.put(id,namespace);
        IFLUtil.incrementIdInMap("namespace");
    }

    public boolean containsKey (String key) {
        return mNamespacesMap.containsKey(key);
    }

    public boolean containsNamespace (String filename, String key) {
        Namespace namespace = getNamespace(filename, key);
        if (namespace == null) {
            return false;
        }
        else {
            return true;
        }
    }

    public int getCount () {
        int total = mNamespacesMap.size();
        return total;
    }

    public void putAll (Map<String,Namespace> namespaces) {
        mNamespacesMap.putAll(namespaces);
    }

    public void putAll (IFLNamespaces namespaces) {
        mNamespacesMap.putAll(namespaces.getMap());
    }

    public Map<String,Namespace> getMap() {
        return mNamespacesMap;
    }

    public List<Namespace> getNamespaces() {
        List list = new ArrayList();
        for (Map.Entry<String, Namespace> entry : mNamespacesMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Namespace getNamespace (String id) {
        Namespace namespace = mNamespacesMap.get(id);
        return namespace;
    }

    public Namespace getNamespace (String aFilename, String aKey) {
        Namespace returnNamespace = null;
        Collection c = mNamespacesMap.values();
        Iterator itr = c.iterator();
        Map<String,Namespace> tempMap = new LinkedHashMap();
        while (itr.hasNext()) {
            Namespace namespace = (Namespace)itr.next();
            String key = namespace.getKey();
            if (key.equalsIgnoreCase(aKey)) {
                String filePath = namespace.getPath();
                if (namespace.getName().equalsIgnoreCase(IFLUtil.NAMESPACE_SHARED_FILE)) {
                    tempMap.put(IFLUtil.NAMESPACE_SHARED_FILE,namespace);
                }
                else if (namespace.getPath().equalsIgnoreCase(aFilename)) {
                    tempMap.put(aFilename,namespace);
                }
            }
        }

      //  System.out.println ("tempMap: " + tempMap);

        if (tempMap != null) {
            returnNamespace = tempMap.get(aFilename);
            if (returnNamespace == null) {
                returnNamespace = tempMap.get(IFLUtil.NAMESPACE_SHARED_FILE);
            }
        }
        return returnNamespace;
    }

    public String getNamespaceValue (String aFilename, String aKey) {
        String namespaceValue = "";
        Namespace namespace = getNamespace (aFilename,aKey);
        if (namespace == null) {
            namespace = getNamespace (IFLUtil.NAMESPACE_SHARED_FILE,aKey);
        }
        if (namespace != null) {
            namespaceValue = namespace.getValue();
        }
        return namespaceValue;
    }

    public String getNamespaceValue (String aFilename, String aKey, String defaultValue) {
        String namespaceValue = getNamespaceValue (aFilename,aKey);
        if (namespaceValue.equals("")) {
            namespaceValue = defaultValue;
        }
        return namespaceValue;
    }
    
    public String toString() {
        String outString = "";
        Set<String> set = mNamespacesMap.keySet();
        for (String key : set)
        {
            Namespace namespace = mNamespacesMap.get(key);
            outString += "\n" + namespace.toString();
        }
        return outString;
    }


}
