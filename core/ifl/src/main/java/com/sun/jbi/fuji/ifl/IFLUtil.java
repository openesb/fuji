/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLUtil.java - Last published on 4/28/2008
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.ifl;

import java.lang.Integer;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


/**
 *
 * @author derek
 */
public class IFLUtil {

    /* Public Constants used in parse routines */
    public static final String GROUP_SERVICE_PROVIDERS   = "SERVICE_PROVIDERS";
    public static final String GROUP_SERVICE_CONSUMERS   = "SERVICE_CONSUMERS";
    public static final String GROUP_SERVICE_UNKNOWN     = "SERVICE_UNKNOWN";
    public static final String GROUP_SERVICE_CALLS       = "SERVICE_CALLS";
    public static final String SERVICE_NAME_NAMESPACE    = "namespace";
    public static final String SERVICE_NAME_SERVICE      = "service";
    public static final String SERVICE_NAME_OPERATION    = "operation";
    public static final String VISIBILITY_PUBLIC         = "public";
    public static final String VISIBILITY_PRIVATE        = "private";
    public static final String NAMESPACE_SHARED_FILE     = "shared-namespaces.ifl";
    public static final String PUBLIC_NS                 = "public-ns";
    public static final String PRIVATE_NS                = "private-ns";
    public static final String BASE_NS                   = "base-ns";
    public static final String SERVICE_CATAGORY_UNKNOWN  = "unknown";
    public static final String SERVICE_CATAGORY_PROVIDER = "provider";
    public static final String SERVICE_CATAGORY_CONSUMER = "consumer";

    public static final String DEFAULT_BASE_NS    = "http://fuji.dev.java.net/application";
    public static final String DEFAULT_PUBLIC_NS  = "";
    public static final String DEFAULT_PRIVATE_NS = "/private";

    private static Map<String,Integer> sIdMap = new TreeMap<String,Integer>();

    /* Private Constants only used in this class */
    static private Pattern expPattern = Pattern.compile("^\\s*[eE][xX][pP]\\s*[:=]");


    /**
     * Parse the name value pairs into property values.
     * @param test the text string to parse
     * @param expDefault if true, the text value is the "exp" default value.
     * @return config properties value
     */
    public static Properties parseNVpairs (String text, boolean expDefault) {
        Properties config = new java.util.Properties();

        if (text == null) {
            return (null);
        }
        if (expDefault) {
            Matcher     match = expPattern.matcher(text);
            if (!match.lookingAt()) {
                config.put("exp", text);
                return (config);
            }
        }
        char[] buffer = text.toCharArray();
        int limit = buffer.length;
        int index = 0;
        int keyLen;
        int keyStart;
        int valueStart;
        int valueLen;
        char c;
        boolean hasEquals;
        boolean precedingBackslash;
        boolean skipLeading;
        boolean skipTrailing;

        for (;index < limit;) {
            c = 0;
            keyLen = 0;
            keyStart = index;
            valueStart = 0;
            valueLen = 0;
            hasEquals = false;
            precedingBackslash = false;
            skipTrailing = false;
            skipLeading = true;
            for (; index < limit; index++) {
                c = buffer[index];
                if (!precedingBackslash) {
                    if (c == ' ') {
                        if (skipLeading) {
                            continue;
                        }
                        skipTrailing = true;
                        continue;
                    }  else if (c == '=' || c== ':') {
                        valueStart = ++index;
                        hasEquals = true;
                        break;
                    } else {
                        if (skipTrailing) {
                            keyLen = index - keyStart;
                            skipTrailing = false;
                        }
                    }
                }
                if (c == '\\') {
                    precedingBackslash = !precedingBackslash;
                } else {
                    precedingBackslash = false;
                }
                if (skipLeading) {
                    skipLeading = false;
                    keyStart = index;
                }
                keyLen++;
            }
            precedingBackslash = false;
            while (index < limit) {
                c = buffer[index];
                if (c == ','  && !precedingBackslash) {
                    index++;
                    break;
                } if (c == '\\') {
                    precedingBackslash = !precedingBackslash;
                } else {
                    precedingBackslash = false;
                }
                valueLen++;
                index++;
            }
            if (!hasEquals) {
                if (keyLen > 0) {
                    config.setProperty("exp", convert(buffer, keyStart, keyLen));
                }
            } else {
                config.setProperty(convert(buffer, keyStart, keyLen), convert(buffer, valueStart, valueLen));
            }
        }
        return (config);
    }


    /**
     * Trims the quotes around the string literal.
     * @param name
     * @return trimed string
     */
    public static String trimQuotes (String name) {
        String trimedName = name;
        if (name != null && name.startsWith("\"") && name.endsWith("\"")) {
            if (name.length() > 2) {
                trimedName = name.substring(1, name.length() - 1);
            } else {
                trimedName = "";
            }
            trimedName.trim();
        }
        return trimedName;
    }


    /**
     * Trims quotes from a list of string literals.
     * @param list of quoted strings
     * @return list of unquoted strings
     */
    public static List<String> trimQuotes (List<String> names) {
        for (int i = 0; i < names.size(); i++) {
            names.set(i, trimQuotes(names.get(i)));
        }
        return names;
    }


    public static void initializeIdMap() {
        sIdMap.put("route",new Integer(1));
        sIdMap.put("split",new Integer(1));
        sIdMap.put("broadcast",new Integer(1));
        sIdMap.put("tee",new Integer(1));
        sIdMap.put("select",new Integer(1));
        sIdMap.put("filter",new Integer(1));
        sIdMap.put("aggregate",new Integer(1));
        sIdMap.put("namespace",new Integer(1));
    }


    public static String getIdFromMap (String key) {
        String idString = key + "$" + ((Integer)sIdMap.get(key).intValue());
        return idString;
    }


    public static void incrementIdInMap (String key) {
        int intValue = ((Integer)sIdMap.get(key).intValue()) + 1;
        sIdMap.put(key,new Integer(intValue));
    }

    /**
     * Create the rotue id given the current route number
     * @param routeNumber
     * @return routeId
     */
    public static String createRouteId (int routeNumber) {
        String routeId = "route$" + (routeNumber + 1);
        return routeId;
    }


    /**
     * Parses the a service name and returns a map containing the "Namespace", "Service Name" and
     * the "Operation".
     * @param completeServiceName - the full service name string extracted from the ifl
     * @return map containing the "Namespace", "Service Name" and "Operation"
     */
    public static Map<String, String> parseServiceName (String completeServiceName) {
        Map serviceNameMap = new HashMap<String,String>();
        String namespace = "";
        String service = "";
        String operation = "";
        int index2 = 0;

        // Parse out the namespace
        int index1 = completeServiceName.indexOf("{");
        if (index1 >= 0) {
            index1++;
            index2 = completeServiceName.indexOf("}");
            if (index2 >= 0) {
                namespace = completeServiceName.substring(index1,index2);
                index2++;
            }
        }
        else {
            index2 = completeServiceName.indexOf(":");
            if (index2 >= 0) {
                namespace = completeServiceName.substring(0,index2);
            }
            index2++;
        }

        // Only continue if an error has not occured
        if (index2 >= 0) {
        
            // Parse out the name of the service
            index1 = index2;
            index2 = completeServiceName.indexOf(".",index1);

            // Service name only
            if (index2 == -1) {
                service = completeServiceName.substring(index1);
            }

            // Service name and operation name
            else {
                service = completeServiceName.substring(index1,index2);
                index1 = index2 + 1;
                operation = completeServiceName.substring(index1);
            }
        }
        serviceNameMap.put(SERVICE_NAME_NAMESPACE,namespace);
        serviceNameMap.put(SERVICE_NAME_SERVICE,service);
        serviceNameMap.put(SERVICE_NAME_OPERATION,operation);
        return serviceNameMap;
    }


    public static String buildResolvedString (String stringOne, String stringTwo) {
        if ((stringOne.length() == 0) || (stringTwo.length() == 0)) {
            return (stringOne + stringTwo);
        }
        if (stringOne.endsWith("/")) {
            stringOne = stringOne.substring(0, stringOne.length()-1);
        }
        if (stringTwo.startsWith("/")) {
            stringTwo = stringTwo.substring(1);
        }
        String newString = stringOne + "/" + stringTwo;
        return newString;
    }

    public static String buildServiceReference(String namespace, String service, String operation) {
        StringBuilder   sb = new StringBuilder();

        if (namespace != null && !namespace.equals("")) {
            sb.append("{");
            sb.append(namespace);
            sb.append("}");
        }
        if (service != null && !service.equals("")) {
            sb.append(service);
        }
        if (operation != null && !operation.equals("")) {
            sb.append(".");
            sb.append(operation);
        }
        return (sb.toString());
    }

    private static String convert (char[] in, int off, int len) {
        char[]  out = new char[len];

        char aChar;
        int outLen = 0;
        int end = off + len;

        while (off < end) {
            aChar = in[off++];
            if (aChar == '\\') {
                aChar = in[off++];
                if(aChar == 'u') {
                    // Read the xxxx
                    int value=0;
            for (int i=0; i<4; i++) {
                aChar = in[off++];
                switch (aChar) {
                  case '0': case '1': case '2': case '3': case '4':
                  case '5': case '6': case '7': case '8': case '9':
                     value = (value << 4) + aChar - '0';
                 break;
              case 'a': case 'b': case 'c':
                          case 'd': case 'e': case 'f':
                 value = (value << 4) + 10 + aChar - 'a';
                 break;
              case 'A': case 'B': case 'C':
                          case 'D': case 'E': case 'F':
                 value = (value << 4) + 10 + aChar - 'A';
                 break;
              default:
                              throw new IllegalArgumentException(
                                           "Malformed \\uxxxx encoding.");
                        }
                     }
                    out[outLen++] = (char)value;
                } else {
                    out[outLen++] = (char)aChar;
                }
            } else {
                out[outLen++] = (char)aChar;
            }
        }
        return new String (out, 0, outLen);
    }

}
