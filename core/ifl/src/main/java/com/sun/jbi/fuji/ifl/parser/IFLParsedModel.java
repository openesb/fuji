/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLParsedModel.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import java.util.List;
import java.util.Map;

/**
 * This interface represents the model parsed from the ifl file. The ifl parser
 * implements this interface to convert the parsed file to the memory model. This
 * interface will be used in the IFL object model <code>IFLModel</code> to constrcut 
 * the IFL Object model from the parsed model.
 * 
 * @see com.sun.jbi.fuji.ifl.IFLModel
 * @see com.sun.jbi.fuji.ifl.IFLReader
 * @see com.sun.jbi.fuji.ifl.IFLModelImpl
 * @see AbstractIFLParser
 * 
 * @author chikkala
 */
public interface IFLParsedModel extends IFLParsedObjectModel {

    /**
     * returns the list of services declarations in the ifl file 
     * @return Map of entries with key=<servicename> and value=<servicetype>
     */
    Map<String, String> getServicesMap();

    /**
     * returns the list of routing blocks defined in the ifl file. each routing
     * block defines one "from" service and zero or more "to" services.
     * 
     * @return List of Map Entries and each entry with "from" service as key
     * and List of "to" services as a value.
     */
    Map<String, Map.Entry<String, List<String>>> getRouteList();

    /**
     * returns the list of broadcasts defined in the IFL routes
     * 
     * @return List of Map Entries and each entry with broadcast name as key and a
     * Map of values representing a aggregate data 
     * (FROM=<servicename>,TO=List<String> broadcast services)
     */
    Map<String, Map<String, Object>> getBroadcastMap();

    /**
     * returns the list of tees defined in the IFL routes
     *
     * @return List of Map Entries and each entry with tee name as key and a
     * Map of values representing a aggregate data
     * (FROM=<servicename>,TO=List<String> of services, ROUTE=<route name>)
     */
    Map<String, Map<String, Object>> getTeeMap();

    /**
     * returns the list of splits defined in the IFL routes
     * 
     * @return List of Map Entries and each entry with service name as key and a 
     * Map of values representing a aggregate data 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, CONFIG=<name/inline text>, INLINE="true"/"false")
     */
    Map<String, Map<String, Object>> getSplitMap();

    /**
     * returns the list of selects defined in the IFL routes
     * 
     * @return List of Map Entries and each entry with service name as key and a 
     * Map of values representing a aggregate data 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, CONFIG=<name/inline text>, INLINE="true"/"false")
     */
    Map<String, Map<String, Object>> getSelectMap();

    /**
     * returns the list of aggregate defined in the IFL routes
     * 
     * @return List of Map Entries and each entry with service name as key and a 
     * Map of values representing a aggregate data 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, CONFIG=<name/inline text>, INLINE="true"/"false")
     */
    Map<String, Map<String, Object>> getAggregateMap();

    /**
     * returns the list of filters defined in the IFL routes
     * 
     * @return List of Map Entries and each entry with service name as key and a 
     * Map of values representing a filter data 
     * (FROM=<servicename>,TO=<servicename> TYPE=<userdefine>, CONFIG=<name/inline text>, INLINE="true"/"false")
     */
    Map<String, Map<String, Object>> getFilterMap();

    /**
     * returns a list of all route paths in the IFL.  The returned map is keyed
     * by route name.  Unless specified explicitly in the IFL, the route name
     * is equal to the "from" service in the route.  The values in the map are
     * an in-order list of services and flows in the route definition.
     * @return map of route paths keyed by route name
     */
    Map<String, Map.Entry<String, List<String>>> getRoutePaths();
    Map<String, Map.Entry<String, List<String>>> getEipIds();

    /**
     * returns a map of group names that were defined in the IFL.  The returned map is keyed
     * by the groupName.  Each groupName map keyed by either "PROVIDERS" or "CONSUMERS" and
     * contains a list of services.
     * @return map of form MAP<groupName,MAP<"PROVIDERS"/"CONSUMERS",List<services>>>
     */
    Map<String, Map<String, List<String>>> getServiceGroupMap();

    /**
     * returns true if the group name exist in the group table
     * @param groupName the service group name
     * @return true if group name exist
     */
    boolean containsGroup (String groupName);

    /**
     * Add the route name to the group map
     * @param routeName
     */
//    void addRouteToGroup (String routeName);

    /**
     * Given a groupName, this method will return a list of providers in the group
     * @param groupName the service group name
     * @return list of provider services in the group
     */
    List<String> getProvidersInServiceGroup(String groupName);

    /**
     * Given a groupName, this method will return a list of consumers in the group
     * @param groupName the service group name
     * @return list of consumer services in the group
     */
    List<String> getConsumersInServiceGroup(String groupName);
    
    /**
     * Will return the group name where a providing service has been defined
     * @param serviceName the provider service name
     * @return the group name of the provider service
     */
    String getGroupNameForProviderService(String serviceName);

    /**
     * Will return the group name where a consumer service has been defined
     * @param serviceName the consumer service name
     * @return the group name of the consumer service
     */
    String getGroupNameForConsumerService(String serviceName);

    /**
     * Will return all the "Consumed" services
     * @return the list of consumed service
     */
    List<String> getConsumedServices ();

    /**
     * Will return all the "Provided" services
     * @return the list of provided service
     */
    List<String> getProvidedServices ();

    /**
     * Will set the specified service to "Consumed"
     */
    void setConsumerType (String serviceName);

    /**
     * Will set the specified service to "Provided"
     */
    void setProviderType (String serviceName);

    /**
     * Will set the specified route to "Provided"
     */
 //   void setRouteProviderType (String routeName);

    /**
     * Will return the Namespace Map
     * @return namespace map
     */
    Map<String, String> getNamespaceMap();

    /**
     * Will return the Route Visiblilty Map
     * @return service visibility map
     */
    Map<String, String> getRouteVisibilityMap();

    /**
     * Will return the Service Visiblilty Map
     * @return service visibility map
     */
    Map<String, String> getServiceVisibilityMap();

    /**
     * Will retrieve services scope map
     */
    Map<String, String> getServicesScopeMap();


}
