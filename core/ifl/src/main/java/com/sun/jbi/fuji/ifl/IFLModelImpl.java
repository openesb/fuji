/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLModelImpl.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.parser.IFLParsedModel;
import com.sun.jbi.fuji.ifl.parser.AbstractIFLParser;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.LinkedHashMap;
import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.logging.Logger;

/**
 * This class implements the IFLModel interface and builds the processing model
 * from the IFLParsedModel.
 * 
 * @author chikkala
 */
public class IFLModelImpl extends IFLObjectModelImpl implements IFLModel {

    /** map with entries <servicename, servicetype> */
    private Map<String, String> mServices = new LinkedHashMap<String, String>();


    /** map with entries <consumed-service, provided-service> */
    private Map<String, Set<String>> mConnections = new LinkedHashMap<String, Set<String>>();
    
    /** map of split data */
    Map<String, Map<String,Object>> mSplits;
    /** map of select data */
    Map<String, Map<String,Object>> mSelects;
    /** map of aggregate data */
    Map<String, Map<String,Object>> mAggregates;
    /** map of filter data */
    Map<String, Map<String,Object>> mFilters;
    /** map of broadcast data */
    Map<String, Map<String,Object>> mBroadcasts;
    /** map of tee data */
    Map<String, Map<String,Object>> mTees;
    /** map of route paths */
    Map<String, Map.Entry<String, List<String>>> mRoutePaths;
    /** map of eip types */
    Map<String, Map.Entry<String, List<String>>> mEipIds;
    /** map of group service data */
    Map<String, Map<String, List<String>>> mServiceGroups;

    /** Namespace Map */
    Map<String, Map <String, String>> mNamespaceScopeMap;

    /** Route Visibility Map */
    Map<String, String> mRouteVisibilities;

    /** Service Visibility Map */
    Map<String, String> mServiceVisibilities;

    /** The Map the will hold the scope info for each service */
    Map<String, String> mServiceScopeMap;

    /** A list containing all the route names **/
    Set<String> mRoutes;

    /** Variable that will hold the complete AbsolutePath name of the shared namespace file */
    String sharedNamespaceFile = "";

    /** logger */
    private Logger mLogger = Logger.getLogger(IFLModelImpl.class.getPackage().getName());
    private static final Localizer sLoc_ = Localizer.get();


    public IFLModelImpl() {
        super();
        this.mServices             = new LinkedHashMap<String, String>();
        this.mConnections          = new LinkedHashMap<String, Set<String>>();
        this.mBroadcasts           = new LinkedHashMap<String, Map<String,Object>>();
        this.mTees                 = new LinkedHashMap<String, Map<String,Object>>();
        this.mSelects              = new LinkedHashMap<String, Map<String,Object>>();
        this.mSplits               = new LinkedHashMap<String, Map<String,Object>>();
        this.mAggregates           = new LinkedHashMap<String, Map<String,Object>>();
        this.mFilters              = new LinkedHashMap<String, Map<String,Object>>();
        this.mRoutePaths           = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
        this.mEipIds               = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
        this.mServiceGroups        = new LinkedHashMap<String, Map<String, List<String>>>();
        this.mRouteVisibilities    = new LinkedHashMap<String, String>();
        this.mServiceVisibilities  = new LinkedHashMap<String, String>();
        this.mServiceScopeMap      = new LinkedHashMap<String, String>();
        this.mNamespaceScopeMap    = new LinkedHashMap<String, Map<String,String>>();
        this.mRoutes               = new TreeSet();

        this.sharedNamespaceFile   = "";
    }

    public void close() {
        resolveRoutes();
        resolveTees();
        resolveSelects();
        resolveBroadcasts();
    }

    public Set<String> getServiceTypes() {
        return new LinkedHashSet(this.mServices.values());
    }

    public List<String> getServicesForType(String serviceType) {
        serviceType = serviceType.toLowerCase(); // service type should always be lower case.
        List<String> services = new ArrayList<String>();
        for (String serviceName : this.mServices.keySet()) {
            if (this.mServices.get(serviceName).equals(serviceType)) {
                services.add(serviceName);
            }
        }
        return services;
    }

   public String getTypeForService(String service) {
       String   type = this.mServices.get(service);
       if (type == null) {
           type = "route";
       }
       return (type);
    }

    public Set<Entry<String, Set<String>>> getConnections() {
        return Collections.unmodifiableSet(mConnections.entrySet());
    }

    public Map<String, Map<String,Object>> getSplits() {
        return Collections.unmodifiableMap(this.mSplits);
    }

    public Map<String, Map<String,Object>> getSelects() {
        return Collections.unmodifiableMap(this.mSelects);
    }

    public Map<String, Map<String,Object>> getAggregates() {
        return Collections.unmodifiableMap(this.mAggregates);
    }

    public Map<String, Map<String,Object>> getFilters() {
        return Collections.unmodifiableMap(this.mFilters);
    }
    
    public Map<String, Map.Entry<String, List<String>>> getRoutes() {
        return Collections.unmodifiableMap(this.mRoutePaths);
    }

    public Map<String, Map.Entry<String, List<String>>> getEipIds() {
        return Collections.unmodifiableMap(this.mEipIds);
    }

    public Map<String, Map<String,Object>> getBroadcasts() {
        return Collections.unmodifiableMap(this.mBroadcasts);
    }

    public Map<String, Map<String,Object>> getTees() {
        return Collections.unmodifiableMap(this.mTees);
    }
    
    /** Get the named aggregate from the parsed IFL model */
    public Map<String, Object> getAggregate(String name) {
        return mAggregates.get(name);
    }

    /** Get the named filter from the parsed IFL model */
    public Map<String, Object> getFilter(String name) {
        return mFilters.get(name);
    }

    /**
     * Returns the complete Route Visibility Map.
     * @return map of service visibilities
     */
    public Map<String, String> getRouteVisibilities() {
        return Collections.unmodifiableMap(this.mRouteVisibilities);
    }

    /**
     * Returns the route visibility for the given route name
     * @return service visibility value
     */
    public String getRouteVisibility(String routeName) {
        return this.mRouteVisibilities.get(routeName);
    }

    /**
     * Returns the complete Service Visibility Map.
     * @return map of service visibilities
     */
    public Map<String, String> getServiceVisibilities() {
        return Collections.unmodifiableMap(this.mServiceVisibilities);
    }

    /**
     * Returns the service visibility for the given service name
     * @return service visibility value
     */
    public String getServiceVisibility(String serviceName) {
        return this.mServiceVisibilities.get(serviceName);
    }

    /**
     * Returns true if the given service is public.
     * @param serviceName
     * @return true if the visibility is public
     */
    public boolean isServicePublic (String name) {
        return checkServiceVisibility ("public", name);
    }

    /**
     * Returns true if the given service is private.
     * @param serviceName
     * @return true if the visibility is private
     */
    public boolean isServicePrivate (String name) {
        return checkServiceVisibility ("private", name);
    }

    /**
     * Returns true if the given route is public.
     * @param serviceName
     * @return true if the visibility is public
     */
    public boolean isRoutePublic (String name) {
        return checkRouteVisibility ("public", name);
    }

    private void resolveRoutes()
    {
        Map<String, Map.Entry<String, List<String>>> resolvedRoutePath = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
        Map.Entry<String, List<String>> routeEntry = null;
        Set<String> routeNames = mRoutePaths.keySet();
        for (String routeName : routeNames)
        {
            routeEntry = mRoutePaths.get(routeName);
            String routeEntryKey = routeEntry.getKey();
            List<String> entryList = routeEntry.getValue();
            List<String> newEntryList = new ArrayList();

            for (String serviceName : entryList)
            {
                String resolvedServiceName = resolveServiceName(serviceName);
                newEntryList.add(resolvedServiceName);
            }

            Map<String, List<String>> map = new HashMap<String, List<String>>();
            map.put(routeEntryKey, newEntryList);

            resolvedRoutePath.put(routeName,map.entrySet().iterator().next());
        }
        mRoutePaths = resolvedRoutePath;
    }

    private void resolveTees() {
        Set<String>    teeNames = mTees.keySet();
        for (String teeName : teeNames) {
           Map<String,Object>   tee = mTees.get(teeName);
           String               toClause = (String)tee.get("TO");
           String               rsn = resolveServiceName(toClause);
           tee.put("TO", rsn);
        }
    }

    private void resolveSelects() {
        Set<String> selectNames = mSelects.keySet();
        for (String selectName : selectNames) {
            Map<String,Object>   select = mSelects.get(selectName);
            LinkedList<LinkedList> whenList = (LinkedList)select.get("WHEN");
            for (LinkedList whenEntry : whenList) {
                String routeOrServiceName = (String)whenEntry.get(2);
                String resolvedName = resolveServiceName(routeOrServiceName);
                whenEntry.set(2,resolvedName);
            }
            String elseValue = (String)select.get("ELSE");
            if (elseValue != null) {
                select.put("ELSE", resolveServiceName(elseValue));
            }
        }
    }

    private void resolveBroadcasts() {
        Set<String> broadcastNames = mBroadcasts.keySet();
        for (String broadcastName : broadcastNames) {
            Map<String,Object>   broadcast = mBroadcasts.get(broadcastName);
            List<String> toList = (List)broadcast.get("TO");
            List<String>  newToList = new ArrayList();

            for (String toEntry : toList) {
                newToList.add(resolveServiceName(toEntry));
            }
            broadcast.put("TO", newToList);
        }
    }

    private String resolveServiceName(String serviceName) {
        String resolvedServiceName = "";
        Map<String,String> serviceNameMap = IFLUtil.parseServiceName (serviceName);

        String service_namespace = serviceNameMap.get(IFLUtil.SERVICE_NAME_NAMESPACE);
        String service_name      = serviceNameMap.get(IFLUtil.SERVICE_NAME_SERVICE);
        String service_operation = serviceNameMap.get(IFLUtil.SERVICE_NAME_OPERATION);

        // If the namespace is already resolved (absolute URI)
        if (service_namespace.toLowerCase().startsWith("http:")) {
            resolvedServiceName = service_namespace;
        }

        // Else we'll need to resolve the service name
        else {

            // True services will have a visibility of either "public" or "private"
            String visibility = getServiceVisibility (service_name);
            if (visibility == null) {
                visibility = getRouteVisibility(service_name);
            }
            if (visibility != null) {

                String filename = getServiceScope(service_name);
                String namespaceValue = getNamespaceValue (filename, service_namespace, service_namespace);

                // Prefix the default public and private locations with the application name.
                String publicDefaultNS  = IFLUtil.buildResolvedString (mApplicationId, IFLUtil.DEFAULT_PUBLIC_NS);
                String privateDefaultNS = IFLUtil.buildResolvedString (mApplicationId, IFLUtil.DEFAULT_PRIVATE_NS);

                // Retrieve (or assign if not found in ifl file) the values for  base, public-ns and private-ns
                String base_ns    = getNamespaceValue (filename, "base-ns", IFLUtil.DEFAULT_BASE_NS);
                String public_ns  = getNamespaceValue (filename, "public-ns", publicDefaultNS);
                String private_ns = getNamespaceValue (filename, "private-ns", privateDefaultNS);

                // Determine if we should use the public or private namespace
                String publicPrivateValue = private_ns;
                if (visibility.equalsIgnoreCase("public")) {
                    publicPrivateValue = public_ns;
                }

                // Build the resolved namespace.
                resolvedServiceName = IFLUtil.buildResolvedString (base_ns, publicPrivateValue);
                resolvedServiceName = IFLUtil.buildResolvedString (resolvedServiceName, namespaceValue);
            }
        }

        // Glue the service name to the end of the derived namespace
        resolvedServiceName = IFLUtil.buildServiceReference (resolvedServiceName, service_name, service_operation);
        return (resolvedServiceName);
    }

    public String getNamespace(String serviceName) {
        String visibility = getServiceVisibility (serviceName);
        String resolvedServiceName = null;
        if (visibility == null) {
            visibility = getRouteVisibility(serviceName);
        }
        if (visibility != null) {

            String filename = getServiceScope(serviceName);

            // Prefix the default public and private locations with the application name.
            String publicDefaultNS  = IFLUtil.buildResolvedString (mApplicationId, IFLUtil.DEFAULT_PUBLIC_NS);
            String privateDefaultNS = IFLUtil.buildResolvedString (mApplicationId, IFLUtil.DEFAULT_PRIVATE_NS);

            // Retrieve (or assign if not found in ifl file) the values for  base, public-ns and private-ns
            String base_ns    = getNamespaceValue (filename, "base-ns", IFLUtil.DEFAULT_BASE_NS);
            String public_ns  = getNamespaceValue (filename, "public-ns", publicDefaultNS);
            String private_ns = getNamespaceValue (filename, "private-ns", privateDefaultNS);

            // Determine if we should use the public or private namespace
            String publicPrivateValue = private_ns;
            if (visibility.equalsIgnoreCase("public")) {
                publicPrivateValue = public_ns;
            }

            // Build the resolved namespace.
            resolvedServiceName = IFLUtil.buildResolvedString (base_ns, publicPrivateValue);
        }

        return (resolvedServiceName);
    }

    /**
     * Returns true if the given route is private.
     * @param serviceName
     * @return true if the visibility is private
     */
    public boolean isRoutePrivate (String name) {
        return checkRouteVisibility ("private", name);
    }

    public Map.Entry<String, List<String>> getRoute(String name) {
        return mRoutePaths.get(name);
    }

    public Map.Entry<String, List<String>> getEipId(String id) {
        return mEipIds.get(id);
    }

    public Map<String, Object> getSplit(String name) {
        return mSplits.get(name);
    }

    public Map<String, Object> getSelect(String name) {
        return mSelects.get(name);
    }

    public Map<String, Map<String, List<String>>> getServiceGroups() {
        return mServiceGroups;
    }

    public List<String> getServiceGroupNames() {
        Set s = mServiceGroups.keySet();
        List list = new ArrayList(s); 
        Collections.sort(list);
        return Collections.unmodifiableList(list);
    }

    public List<String> getProvidersInServiceGroup (String groupName) {
        List list = getServiceGroupList(IFLUtil.GROUP_SERVICE_PROVIDERS,groupName);
        return list;
    }

    public List<String> getConsumersInServiceGroup (String groupName) {
        List list = getServiceGroupList(IFLUtil.GROUP_SERVICE_CONSUMERS,groupName);
        return list;
    }

    public List<String> getUnknownInServiceGroup (String groupName) {
        List list = getServiceGroupList(IFLUtil.GROUP_SERVICE_UNKNOWN,groupName);
        return list;
    }

    public List<String> getServiceReferencesInServiceGroup (String groupName) {
        List list = getServiceGroupList(IFLUtil.GROUP_SERVICE_CALLS,groupName);
        return list;
    }

    public List<String> getServicesInServiceGroup (String groupName) {
        List services = new ArrayList();
        List providerList = getProvidersInServiceGroup(groupName);
        List consumerList = getConsumersInServiceGroup(groupName);
        List unknownList  = getUnknownInServiceGroup(groupName);
        services.addAll(providerList);
        services.addAll(consumerList);
        services.addAll(unknownList);
        return services;
    }

    public String getGroupForProviderService (String serviceName) {
        return getGroupForService (serviceName, IFLUtil.GROUP_SERVICE_PROVIDERS);
    }

    public String getGroupForConsumerService (String serviceName) {
        return getGroupForService (serviceName, IFLUtil.GROUP_SERVICE_CONSUMERS);
    }

    public String getGroupForUnknownService (String serviceName) {
        return getGroupForService (serviceName, IFLUtil.GROUP_SERVICE_UNKNOWN);
    }

    public String getGroupForService (String serviceName) {
        String groupName = getGroupForProviderService(serviceName);
        if (groupName.equals("")) {
            groupName = getGroupForConsumerService(serviceName);
        }
        if (groupName.equals("")) {
            groupName = getGroupForUnknownService(serviceName);
        }
        return groupName;
    }

    public List<String> getConsumedServices () {
        List list = getServices (IFLUtil.GROUP_SERVICE_CONSUMERS);
        Collections.sort(list);
        return Collections.unmodifiableList(list);
    }

    public List<String> getProvidedServices () {
        List list = getServices (IFLUtil.GROUP_SERVICE_PROVIDERS);
        Set set = new TreeSet();
        set.addAll(mRoutes);
        set.addAll(list);  // Use set to make sure there are not duplicates
        List newList = new ArrayList(set);
        Collections.sort(newList);   // Sort for consistent output
        return Collections.unmodifiableList(newList);
    }

    public boolean isConsumed (String serviceName) {
        List list = getConsumedServices();
        return list.contains(serviceName);
    }

    public boolean isProvided (String serviceName) {
        List list = getProvidedServices();
        return list.contains(serviceName);
    }

    /**
     * Returns a map of maps containing all namespaces
     * @return Map of Map <<scope>, <namespace key, namespace value>>
     */
    public Map<String, Map<String, String>> getNamespaces () {
        return mNamespaceScopeMap;
    }


    /**
     * Will return a map of namespaces for the given scope.
     * @param scopeName the scope (filename) where to check for the namespace
     * @return the namespace map
     */
    public Map<String,String> getNamespacesForScope (String scopeName) {
       return mNamespaceScopeMap.get(scopeName);
    }


    public String getNamespaceValue (String scopeName, String namespaceKey, String defaultValue) {
        String namespaceValue = getNamespaceValue(scopeName,namespaceKey);
        if (namespaceValue == null) {
            namespaceValue = defaultValue;
        }
        return namespaceValue;
    }

    
    /**
     * Will return the namespace value.  It will first check the for the value in the given scope.
     * If it is not found, it will check for the namespace value in the namespace shared file.
     * @param scopeName the scope (filename) where to check for the namespace
     * @param namespaceKey the name space key
     * @return the namespace value
     */
    public String getNamespaceValue (String scopeName, String namespaceKey) {
        String namespaceValue = null;
        Map<String,String>namespaceMap = getNamespacesForScope(scopeName);
        if (namespaceMap != null) {
            namespaceValue = namespaceMap.get(namespaceKey);
        }
        if (namespaceValue == null)
        {
            namespaceMap = getNamespacesForScope(this.sharedNamespaceFile);
            if (namespaceMap != null)
            {
                namespaceValue = namespaceMap.get(namespaceKey);
            }
        }
        return namespaceValue;
    }


    /**
     * Will return the scope name (filename) for a specified service.
     * @param serviceName the service name
     * @return the scope name (filename)
     */
    public String getServiceScope (String serviceName) {
        return this.mServiceScopeMap.get(serviceName);
    }

    /**
     * Retrieve the provider or consumer group list from the GroupNames map
     * @param groupType the group type "PROVIDER" or "CONSUMER"
     * @param groupName the key into the GroupNames map
     * @return list of services
     */
    private List<String> getServiceGroupList (String groupType, String groupName) {
        Map<String,List<String>> map = new HashMap<String,List<String>>();
        List<String> serviceList = new ArrayList();
        map = this.mServiceGroups.get(groupName);
        if (map != null)
        {
            serviceList = map.get(groupType);
        }
        return serviceList;
    }

    private void putAll (Map sourceMap, Map destinationMap, String eipType) throws IFLParseException {
        Set sourceKeySet = sourceMap.keySet();
        Set destinationKeySet = destinationMap.keySet();
        Iterator iter = sourceKeySet.iterator();
        while (iter.hasNext()) {
            String key = (String)iter.next();
            if (destinationKeySet.contains(key)) {
                String errorMsg = sLoc_.t("0010: Duplicate name \"{0}\" was specified for \"{1}\".",key,eipType);
                mLogger.fine(errorMsg);
                throw new IFLParseException(errorMsg); 
            }
        }
        destinationMap.putAll(sourceMap);
    }



    /**
     * Will check the visibility type of the rout name against the given
     * visibilityType.  If the match, true will be returned else false.
     * @param visibilityType ("public" or "private"
     * @param name the service or route name
     * @return true if visibility or route or service matches given type
     */
    private boolean checkRouteVisibility (String visibilityType, String name) {
        boolean returnValue = false;
        String visibilityValue = getRouteVisibility(name);
        if (visibilityValue != null) {
            returnValue = visibilityValue.equals(visibilityType);
        }
        return returnValue;
    }


    /**
     * Will check the visibility type of the service name against the given
     * visibilityType.  If the match, true will be returned else false.
     * @param visibilityType ("public" or "private"
     * @param name the service or route name
     * @return true if visibility or route or service matches given type
     */
    private boolean checkServiceVisibility (String visibilityType, String name) {
        boolean returnValue = false;
        String visibilityValue = getServiceVisibility(name);
        if (visibilityValue != null) {
            returnValue = visibilityValue.equals(visibilityType);
        }
        return returnValue;
    }


    /**
     * Retrieve the group name for the given service
     * @param serviceName the name of the service
     * @param serviceGroupType the type of service "Provider" or "Consumer"
     * @return the group name or a empty string if the service name is not found in the group map
     */
    private String getGroupForService (String serviceName, String serviceGroupType) {
        Set s = mServiceGroups.keySet();
        Iterator itr = s.iterator();
        while (itr.hasNext())
        {
            String groupName = (String)itr.next();
            Map gNameMap = (Map)mServiceGroups.get(groupName);
            if (gNameMap != null)
            {
                List list = (List)gNameMap.get(serviceGroupType);
                if (list.contains(serviceName))
                {
                    return groupName;
                }
            }
        }
        return "";
    }


    /**
     * Will return a List containing all the services for a given Group Service Type
     * which will be a "Provider" or "Consumer"
     * Retrieve the group name for the given service
     * @param serviceGroupType the type of service "Provider" or "Consumer"
     * @return the list of services
     */
    private List<String> getServices (String serviceGroupType) {
        HashSet serviceSet = new HashSet();
        Set s = mServiceGroups.keySet();
        Iterator itr = s.iterator();
        while (itr.hasNext()) {
            String groupName = (String)itr.next();
            Map gNameMap = (Map)mServiceGroups.get(groupName);
            if (gNameMap != null) {
                List list = (List)gNameMap.get(serviceGroupType);
                serviceSet.addAll(list);
            }
        }
        List serviceList = new ArrayList();
        serviceList.addAll(serviceSet);
        return serviceList;
    }

    
    /**
     * maps the parsed route data to the processing model
     * @param fromService service name or null
     * @param toServices list of services or empty list
     * @param broadcastServices list of services or empty list
     */
    private void processRouteData (String routeName, 
                                   String fromService, 
                                   List<String> toServices, 
                                   List<String> path,
                                   IFLParsedModel parsedModel) {
        //
        // 0. Save the route names.  Since routes are also provided services, this list will be used
        // when the user wants a list of provided services or wants to check if a services is provided.
        this.mRoutes.add(routeName);

        //
        // 1. add from service as consumer.
        // (fromService could be null in route to "xyz" or route "name" do to "xyx" ... end cases
        //
        if (fromService != null) {
            parsedModel.setConsumerType (fromService);
        }

        //
        // 2. add toServices and broadcastServices as providers
        // 2.1 add to services to providers list
        for (String toService : toServices) {
            Set s = mConnections.get(routeName);
            if (s == null) {
                this.mConnections.put(routeName, s = new LinkedHashSet<String>());
            }
            s.add(toService);
            parsedModel.setProviderType (toService);
        }

        //
        // 3. add service connection if needed
        //
        if (fromService != null && path.size() > 0) {
            Set s = mConnections.get(fromService);
            if (s == null) {
                this.mConnections.put(fromService, s = new LinkedHashSet<String>());
            }
            s.add(routeName);
        }

//        if (toServices.size() > 0 && path.size() > 0) {
//            String lastTo = toServices.get(toServices.size() - 1);
//            // If the route ends in a "to" service, then a connection is needed
//            if ( fromService != null && lastTo.equals(path.get(path.size() - 1))) {
//                // System.out.println("Connection[consumed= " + fromService + " provided= " + providerService + "]");
//                this.mConnections.put(fromService, lastTo);
//            }
//        }

    }


    /**
     * Checks to see if any key in the new map is also present in the originalMap.
     * @param originalMap the original map to check against
     * @param newMap the new map containing the keys to validate
     * @return a list of duplicate keys found, or an empty list if no duplicates were found.
     */
    private List mapContainsKeys (Map originalMap, Map newMap) {
        List containList = new ArrayList();
        Set<String> keys = newMap.keySet();
        for (String key : keys) {
            if (originalMap.containsKey(key))
            {
                containList.add(key);
            }
        }
        return containList;
    }


    /**
     * add the parsedModel data got from a ifl parser to the IFLModel
     * @param parsedModel
     */
    public void addIFLParsedModel (IFLParsedModel parsedModel) throws IFLParseException {

        super.addIFLParsedModel(parsedModel);
        
        // add split data from parsed model to ifl model
        putAll (parsedModel.getSplitMap(),mSplits,"split");

        // add select data from parsed model to ifl model
       // this.mSelects.putAll(parsedModel.getSelectMap());
        putAll (parsedModel.getSelectMap(),mSelects,"select");

        // add aggregate data from parsed model to ifl model
        //this.mAggregates.putAll(parsedModel.getAggregateMap());
        putAll (parsedModel.getAggregateMap(),mAggregates,"aggregate");

        // add aggregate data from parsed model to ifl model
        //this.mFilters.putAll(parsedModel.getFilterMap());
        putAll (parsedModel.getFilterMap(),mFilters,"filter");
        
        // add broadcast data from parsed model to ifl model
        //this.mBroadcasts.putAll(parsedModel.getBroadcastMap());
        putAll (parsedModel.getBroadcastMap(),mBroadcasts,"broadcast");

        // add tee data from parsed model to ifl model
        //this.mTees.putAll(parsedModel.getTeeMap());
        putAll (parsedModel.getTeeMap(),mTees,"tee");

        // get the route paths
        //this.mRoutePaths.putAll(parsedModel.getRoutePaths());
        putAll (parsedModel.getRoutePaths(),mRoutePaths,"route");

        // get the eip types
        this.mEipIds.putAll(parsedModel.getEipIds());

        // Retrieve the scope key from the parser model
        String scopeKey = parsedModel.getScopeName();

        if (scopeKey.endsWith(IFLUtil.NAMESPACE_SHARED_FILE)) {
            if (this.sharedNamespaceFile.equals("")) {
                this.sharedNamespaceFile = scopeKey;
            }
            else {
                String errorMsg = sLoc_.t("0005: Multiple copies of the " + IFLUtil.NAMESPACE_SHARED_FILE  + " file were encountered when only one can exist.  Please consolidate them into a single file.");
                mLogger.fine(errorMsg);
                throw new IFLParseException(errorMsg); 
            }
        }

        // Get the Route Visibilities for this file
        this.mRouteVisibilities.putAll(parsedModel.getRouteVisibilityMap());

        // Get the Service Visibilities for this file
        this.mServiceVisibilities.putAll(parsedModel.getServiceVisibilityMap());

        // Add the services to the Services Scope map
        this.mServiceScopeMap.putAll(parsedModel.getServicesScopeMap());

        // Add the namespace to the Namespace Scope map
        Map<String, String> scopeNamespaceMap = parsedModel.getNamespaceMap();
        this.mNamespaceScopeMap.put(scopeKey,scopeNamespaceMap);

         // add the list of services defined 
        Map<String, String> servicesMap = parsedModel.getServicesMap();
        for (Map.Entry<String, String> entry : servicesMap.entrySet()) {
            //TODO: report error on a duplicate service name
            String service = entry.getKey();
            String type = entry.getValue().toLowerCase(); // service type should be store at lowercase.

            if (this.mServices.containsKey(service)) {
                // TODO: print error
            }
            this.mServices.put(service, type);
        }
        
        Map<String, Map.Entry<String, List<String>>> routeList = parsedModel.getRouteList();
        for (String routeName : routeList.keySet()) {
            Map.Entry<String, List<String>> route = routeList.get(routeName);

            String fromService = route.getKey();
            List<String> toServices = route.getValue();
            if ( toServices == null ) {
                toServices = new ArrayList<String>();
            }
            processRouteData(routeName, 
                             fromService, 
                             toServices, 
                             getRoute(routeName).getValue(), 
                             parsedModel);
        }


        Map<String, Map<String, Object>> teeList = parsedModel.getTeeMap();
        for (String teeName : teeList.keySet()) {
            Map<String, Object> tee = teeList.get(teeName);
            String teeEndpoint = (String)tee.get("TO");
            if (teeEndpoint != null) {
                parsedModel.setProviderType (teeEndpoint);
            }
        }

        // Add the services inside of the select block to the provider list.
        Map<String, Map<String, Object>> selectList = parsedModel.getSelectMap();
        for (String selectName : selectList.keySet()) {
            Map<String, Object> select = selectList.get(selectName);
            String toRoute = (String)select.get(Flow.Keys.ELSE.toString());
            if (toRoute != null) {
                parsedModel.setProviderType (toRoute);
            }
            List <List> selectWhen = (List)select.get(Flow.Keys.WHEN.toString());
            if (selectWhen != null) {  // Just making sure
                if (selectWhen.size() > 0) {
                    for (List whenList: selectWhen)
                    {
                        if (whenList.size() >= 2) {  // Again just to make sure
                            toRoute = (String)whenList.get(2);
                            parsedModel.setProviderType (toRoute);
                        }
                    }
                }
            }
        }

        // add add the service group data from the parsed model to the ifl model
        this.mServiceGroups.putAll(parsedModel.getServiceGroupMap());

        //resolveNamespaces(mRoutePaths);
    }
    
}
