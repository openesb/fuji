/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLReader.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import com.sun.jbi.fuji.ifl.IFLObjectModelImpl;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.parser.IFLParsedModel;
import com.sun.jbi.fuji.ifl.parser.IFLParsedObjectModel;
import com.sun.jbi.fuji.ifl.parser.IFLParser;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * This class creates IFLModel from the list of ifl files or 
 * directories containing ifl files.
 * usage 
 * <p>
 * <pre>
 * IFLReader iflReader = new IFLReader();
 * IFLModel model = iflReader.read(new File("/ifldir"));
 * model = iflReader.read(new File("/dir/myapp.ifl"));
 * model = iflReader.read(new File("/dir/1.ifl"), new File("/dir/2.ifl"));
 * </pre>
 * 
 * @see IFLModel 
 * @author chikkala
 */
public class IFLReader {

    public IFLReader() {
    }

    public void processIFLReader (IFLModelImpl impl,
                                  File file) throws IOException, IFLParseException {
        Reader reader = new FileReader(file);
        IFLParser parser = new IFLParser (reader);
        String filename = file.getAbsolutePath();
        //parser.setIdMap(impl.getIdMap());
        parser.parse(filename);
        impl.addIFLParsedModel((IFLParsedModel) parser);
    }

    protected void processIFLReader(IFLModelImpl impl, 
                                    Reader reader) throws IOException, IFLParseException {
        IFLParser parser = new IFLParser(reader);
        //parser.setIdMap(impl.getIdMap());
        parser.parse();
        impl.addIFLParsedModel((IFLParsedModel) parser);
    }

    protected void processIFLURLReader(IFLModelImpl impl, 
                                       Reader reader,
                                       String filename) throws IOException, IFLParseException {
        IFLParser parser = new IFLParser(reader);
        //parser.setIdMap(impl.getIdMap());
        File f = new File(filename);
        String name = f.getName();
        parser.parse(name);
        impl.addIFLParsedModel((IFLParsedModel) parser);
    }

    protected void processIFLFile(IFLModelImpl impl, File file) throws IOException, IFLParseException {
        FileReader reader = null;
        try {
            processIFLReader (impl,file);
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
                // Logger.getLogger(IFLModelFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * returns the list of files with .ifl or .IFL extensions from a directory
     * 
     * @param dir directory in which to locate the .ifl files
     * @return array of .ifl files
     */
    protected File[] listIFLFiles(File dir) {
        File[] iflFiles = dir.listFiles(
                new FilenameFilter() {

                    public boolean accept(File dir, String name) {
                        return (name.endsWith(".ifl") || name.endsWith(".IFL"));
                    }
                });
        if (iflFiles == null) {
            //TODO: show list dir error
            iflFiles = new File[0]; // return empty array.

        }
        return iflFiles;
    }

    /**
     * reads set of IFL files and constrcut the combined IFL Model
     * @param files list of files with .ifl extension or the directories
     * if the list contains directory, the directory will be searched for the 
     * .ifl files for processing.
     * @return combined IFLModel from all the list of files.
     * @throws IOException  on any IO errors
     * @throws IFLParseException on any ifl syntax errors
     * //TODO: provide error handler interface so that
     * the errors are better propagated to the required clients. 
     */
    public IFLModel read(File... files) throws IOException, IFLParseException {
        IFLModelImpl model = null;
        model = new IFLModelImpl();
        for (File file : files) {
            if (file == null) {
                throw new IOException("Can not process Null IFL File path ");
            }
            if (!file.exists()) {
                throw new IOException("IFL File Not Found : " + file.getAbsolutePath());
            }
            if (file.isDirectory()) {
                // list .ifl files 
                File[] iflFiles = listIFLFiles(file);
                for (File iflFile : iflFiles) {
                    processIFLFile(model, iflFile);
                }
            }
            String name = file.getName();
            if (name.endsWith(".ifl") || name.endsWith(".IFL")) {
                // process file.
                processIFLFile(model, file);
            }
        }
        return model;
    }

    /**
     * reads IFL from the set of URLs and constrcut the combined IFL Model
     * @param urls list of urls pointing to the ifl files for processing.
     * @return combined IFLModel from all the list of urls.
     * @throws IOException  on any IO errors
     * @throws IFLParseException on any ifl syntax errors
     * //TODO: provide error handler interface so that
     * the errors are better propagated to the required clients. 
     */
    public IFLModel read(URL... urls) throws IOException, IFLParseException {
        IFLModelImpl model = null;
        model = new IFLModelImpl();
        for (URL url : urls) {
            InputStream in = null;
            InputStreamReader reader = null;
            try {
                in = url.openStream();
                String filename = url.getFile();
                reader = new InputStreamReader(in);
                //this.processIFLReader(model, (Reader)reader);
                this.processIFLURLReader(model, (Reader)reader, filename);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception ex) {
                        // ignore.
                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (Exception ex) {
                        //ignore
                    }
                }
            }
        }
        return model;
    }

    /**
     * reads IFL from the set of Readers and constrcut the combined IFL Model
     * @param readers list of readers to read ifl text for processing.
     * @return combined IFLModel from all the list of urls.
     * @throws IOException  on any IO errors
     * @throws IFLParseException on any ifl syntax errors
     * //TODO: provide error handler interface so that
     * the errors are better propagated to the required clients. 
     */
    public IFLModel read(Reader... readers) throws IOException, IFLParseException {
        IFLModelImpl model = null;
        model = new IFLModelImpl();
        for (Reader reader : readers) {
            this.processIFLReader(model, reader);
        }
        return model;
    }

    /**
     * reads the IFL text from the Reader object and contructs the IFLModel
     * from it.
     * @param reader Reader 
     * @return IFLModel 
     * @throws java.io.IOException
     * @throws com.sun.jbi.fuji.ifl.parser.IFLParseException
     */
    public IFLModel read(Reader reader) throws IOException, IFLParseException {
        IFLModelImpl model = null;
        model = new IFLModelImpl();
        this.processIFLReader(model, reader);
        return model;
    }

    /**
     * reads the ifl text from the input stream.
     * @param in input stream to read ifl text.
     * @return IFLModel 
     * @throws java.io.IOException
     * @throws com.sun.jbi.fuji.ifl.parser.IFLParseException
     */
    public IFLModel read(InputStream in) throws IOException, IFLParseException {
        return read(new InputStreamReader(in));
    }
}
