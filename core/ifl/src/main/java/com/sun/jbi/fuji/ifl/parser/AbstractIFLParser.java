/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractIFLParser.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.AbstractIFLParserObjectModel;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.fuji.ifl.Localizer;
import java.io.File;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.LinkedHashMap;
import java.util.logging.Logger;


/**
 * This class is the base class which will be extended by the JavaCC generated IFLParser
 * class to populate the parsed model for the IFL from the parser.
 * 
 * @author chikkala, markrs
 */
public abstract class AbstractIFLParser extends AbstractIFLParserObjectModel implements IFLParsedModel {    

    /** map with entry <servicename, servicetype> */
    private Map<String, String> mServicesMap = new LinkedHashMap<String, String>();

    /** map the will contain the scopename (i.e. filename) for each service */
    private Map<String, String> mServicesScopeMap = new LinkedHashMap<String, String>();

    /** map the will contain the visibility information for each service */
    private Map<String, String> mServicesVisibilityMap = new LinkedHashMap<String, String>();

    /** collection of route paths */
    private Map<String, Map.Entry<String, List<String>>> mRoutePaths = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
    private Map<String, Map.Entry<String, List<String>>> mEipIds     = new LinkedHashMap<String, Map.Entry<String, List<String>>>();

    /** list with entry object <fromservice>, list<toservices> from route */
    private Map<String, Map.Entry<String, List<String>>> mRouteList = new LinkedHashMap<String, Map.Entry<String, List<String>>>();

    /** map that will contain the service visibility information */
    private Map<String, String> mRouteVisibility = new LinkedHashMap<String, String>();

    /** list with entry object <fromservice> in parent, list<toservices> from broadcast routes */
    private Map<String, Map<String, Object>> mBroadcastMap = new LinkedHashMap<String, Map<String, Object>>();
    private Map<String, Map<String, Object>> mTeeMap       = new LinkedHashMap<String, Map<String, Object>>();
    private Map<String, Map<String, Object>> mSelectMap    = new LinkedHashMap<String, Map<String, Object>>();
    private Map<String, Map<String, Object>> mSplitMap     = new LinkedHashMap<String, Map<String, Object>>();
    private Map<String, Map<String, Object>> mAggregateMap = new LinkedHashMap<String, Map<String, Object>>();
    private Map<String, Map<String, Object>> mFilterMap    = new LinkedHashMap<String, Map<String, Object>>();

    /** namespace maps */
  //  private Map<String, String> mNamespaceDefaultMap = new LinkedHashMap<String, String>();
    private Map<String, String> mNamespaceMap        = new LinkedHashMap<String, String>();

    /** map holding the group information */
    private Map<String, Map<String, List<String>>> mServiceGroups = new LinkedHashMap<String, Map<String, List<String>>>();


    /**  Public Methods **/

    public Map<String, String> getServicesMap() {
        return this.mServicesMap;
    }

    public Map<String, String> getServicesScopeMap() {
        return this.mServicesScopeMap;
    }

    public Map<String, Map.Entry<String, List<String>>> getRouteList() {
        return this.mRouteList;
    }

    public Map<String, Map<String, Object>> getBroadcastMap() {
        return this.mBroadcastMap;
    }

    public Map<String, Map<String, Object>> getTeeMap() {
        return this.mTeeMap;
    }

    public Map<String, Map<String, Object>> getSplitMap() {
        return this.mSplitMap;
    }

    public Map<String, Map<String, Object>> getSelectMap() {
        return this.mSelectMap;
    }

    public Map<String, Map<String, Object>> getAggregateMap() {
        return this.mAggregateMap;
    }

    public Map<String, Map<String, Object>> getFilterMap() {
        return this.mFilterMap;
    }

    public Map<String, Map.Entry<String, List<String>>> getRoutePaths() {
        return this.mRoutePaths; 
    }

    public Map<String, Map.Entry<String, List<String>>> getEipIds() {
        return this.mEipIds; 
    }

    public Map<String, String> getRouteVisibilityMap() {
        return this.mRouteVisibility;
    }

    public Map<String, String> getServiceVisibilityMap() {
        return this.mServicesVisibilityMap;
    }
    
    public Map<String, Map<String, List<String>>> getServiceGroupMap() {
        return this.mServiceGroups;
    }

    public boolean containsGroup (String groupName) {
        return this.mServiceGroups.containsKey(groupName);
    }

    public List<String> getProvidersInServiceGroup (String groupName) {
        return getServiceGroupList(IFLUtil.GROUP_SERVICE_PROVIDERS,groupName);
    }

    public List<String> getConsumersInServiceGroup (String groupName) {
        return getServiceGroupList(IFLUtil.GROUP_SERVICE_CONSUMERS,groupName);
    }

    public List<String> getServiceGroupNames() {
        Set s = mServiceGroups.keySet();
        List list = new ArrayList(s); 
        return list;
    }

    public String getRouteVisibility (String routeName) {
        String visibility = (String)this.mRouteVisibility.get(routeName);
        return visibility;
    }

    public String getServiceVisibility (String serviceName) {
        String visibility = (String)this.mServicesVisibilityMap.get(serviceName);
        return visibility;
    }

    public String getGroupNameForProviderService (String serviceName) {
        return getGroupNameForService (serviceName, IFLUtil.GROUP_SERVICE_PROVIDERS);
    }

    public String getGroupNameForConsumerService (String serviceName) {
        return getGroupNameForService (serviceName, IFLUtil.GROUP_SERVICE_CONSUMERS);
    }

    public String getGroupNameForService (String serviceName) {
        String groupName = getGroupNameForProviderService(serviceName);
        if (groupName.equals("")) {
            groupName = getGroupNameForConsumerService(serviceName);
        }
        return groupName;
    }

    public List<String> getConsumedServices () {
        return getServices (IFLUtil.GROUP_SERVICE_CONSUMERS);
    }

    public List<String> getProvidedServices () {
        return getServices (IFLUtil.GROUP_SERVICE_PROVIDERS);
    }

//    public void setRouteProviderType (String routeName) {
//        setServiceProviderConsumerType (routeName,IFLUtil.GROUP_ROUTE_PROVIDERS);
//    }

    public void setConsumerType (String serviceName) {
        setServiceProviderConsumerType (serviceName,IFLUtil.GROUP_SERVICE_CONSUMERS);
    }

    public void setProviderType (String serviceName) {
        setServiceProviderConsumerType (serviceName,IFLUtil.GROUP_SERVICE_PROVIDERS);
    }

    public Map<String, String> getNamespaceMap() {
        return this.mNamespaceMap;
    }

    public Map<String, String> parseServiceName (String completeServiceName) {
        Map serviceNameMap = new HashMap<String,String>();
        String namespace = "";
        String service = "";
        String operation = "";
        int index2 = 0;

        // Parse out the namespace
        int index1 = completeServiceName.indexOf("{");
        if (index1 >= 0) {
            index1++;
            index2 = completeServiceName.indexOf("}");
            if (index2 >= 0) {
                namespace = completeServiceName.substring(index1,index2);
                index2++;
            }
        }
        else {
            index2 = completeServiceName.indexOf(":");
            if (index2 >= 0) {
                namespace = completeServiceName.substring(0,index2);
            }
            index2++;
        }

        // Only continue if an error has not occured
        if (index2 >= 0) {
        
            // Parse out the name of the service
            index1 = index2;
            index2 = completeServiceName.indexOf(".",index1);

            // Service name only
            if (index2 == -1) {
                service = completeServiceName.substring(index1);
            }

            // Service name and operation name
            else {
                service = completeServiceName.substring(index1,index2);
                index1 = index2 + 1;
                operation = completeServiceName.substring(index1);
            }
        }
        serviceNameMap.put(IFLUtil.SERVICE_NAME_NAMESPACE,namespace);
        serviceNameMap.put(IFLUtil.SERVICE_NAME_SERVICE,service);
        serviceNameMap.put(IFLUtil.SERVICE_NAME_OPERATION,operation);
        return serviceNameMap;
    }


    /**  Private and Protected Methods **/


    /**
     * Will set the group service type to either "Provider" or "Consumer"
     * Retrieve the group name for the given service
     * @param serviceName the name of the service
     * @param serviceGroupType the type of service "Provider" or "Consumer"
     */
    private void setServiceProviderConsumerType (String serviceName, String serviceGroupType) {
        Map gNameMap = (Map)mServiceGroups.get(serviceName);
        if (gNameMap != null) {
            List list1 = (List)gNameMap.get(serviceGroupType);
            if (!(list1.contains(serviceName))) {
                list1.add(serviceName);
                gNameMap.put(serviceGroupType,list1);
            }
            List list2 = (List)gNameMap.get(IFLUtil.GROUP_SERVICE_UNKNOWN);
            if (list2 != null) {
                if (list2.contains(serviceName)) {
                    list2.remove(serviceName);
                }
            }
        }
    }

    /**
     * Will return a List containing all the services for a given Group Service Type
     * which will be a "Provider" or "Consumer"
     * Retrieve the group name for the given service
     * @param serviceGroupType the type of service "Provider" or "Consumer"
     * @return the list of services
     */
    private List<String> getServices (String serviceGroupType) {
        HashSet serviceSet = new HashSet();
        Set s = mServiceGroups.keySet();
        Iterator itr = s.iterator();
        while (itr.hasNext())
        {
            String groupName = (String)itr.next();
            Map gNameMap = (Map)mServiceGroups.get(groupName);
            if (gNameMap != null)
            {
                List list = (List)gNameMap.get(serviceGroupType);
                serviceSet.addAll(list);
            }
        }
        List serviceList = new ArrayList();
        serviceList.addAll(serviceSet);
        return serviceList;
    }

    /**
     * Retrieve the group name for the given service
     * @param serviceName the name of the service
     * @param serviceGroupType the type of service "Provider" or "Consumer"
     * @return the group name or a empty string if the service name is not found in the group map
     */
    private String getGroupNameForService (String serviceName, String serviceGroupType) {
        Set s = mServiceGroups.keySet();
        Iterator itr = s.iterator();
        while (itr.hasNext())
        {
            String groupName = (String)itr.next();
            Map gNameMap = (Map)mServiceGroups.get(groupName);
            if (gNameMap != null)
            {
                List list = (List)gNameMap.get(serviceGroupType);
                if (list.contains(serviceName))
                {
                    return groupName;
                }
            }
        }
        return "";
    }

    /**
     * Retrieve the provider or consumer group list from the GroupNames map
     * @param groupType the group type "PROVIDER" or "CONSUMER"
     * @param groupName the key into the GroupNames map
     * @return list of services
     */
    private List<String> getServiceGroupList (String groupType, String groupName) {
        Map<String,List<String>> map = new HashMap<String,List<String>>();
        List<String> serviceList = new ArrayList();
        map = this.mServiceGroups.get(groupName);
        if (map != null)
        {
            serviceList = map.get(groupType);
        }
        return serviceList;
    }

    /**
     * initializes the parsed model
     */
    protected void initParsedModel() {
        this.mServicesMap           = new LinkedHashMap<String, String>();
        this.mServicesScopeMap      = new LinkedHashMap<String, String>();
        this.mRouteList             = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
        this.mBroadcastMap          = new LinkedHashMap<String, Map<String, Object>>();
        this.mTeeMap                = new LinkedHashMap<String, Map<String, Object>>();
        this.mSelectMap             = new LinkedHashMap<String, Map<String, Object>>();
        this.mSplitMap              = new LinkedHashMap<String, Map<String, Object>>();
        this.mAggregateMap          = new LinkedHashMap<String, Map<String, Object>>();
        this.mFilterMap             = new LinkedHashMap<String, Map<String, Object>>();
        this.mRoutePaths            = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
        this.mEipIds                = new LinkedHashMap<String, Map.Entry<String, List<String>>>();
        this.mServiceGroups         = new LinkedHashMap<String, Map<String, List<String>>>();
        this.mServicesVisibilityMap = new LinkedHashMap<String, String>();
        this.mNamespaceMap          = new LinkedHashMap<String, String>();
    }

    /**
     * adds the namespace info to the parsed model
     * @param serviceName
     * @param serviceType
     */
    protected void addNamespace (String namespaceKey, String namespaceValue) throws Exception
    {
        super.addNamespace (namespaceKey, namespaceValue);

        String key = IFLUtil.trimQuotes(namespaceKey);
        String value = IFLUtil.trimQuotes(namespaceValue);

        // Add the namespace to the map if one with the current key value does not already exist.
        String mapType = this.mNamespaceMap.get(key);
        if (mapType == null) {
            this.mNamespaceMap.put(key, value);
        }
        else {
            String errorMsg = sLoc_.t("0006: Unable to define the namespace {0} to {1}, since it was previously defined to {2}.",key,value,mapType);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }
    }


    /**
     * adds the service definition info to the parsed model
     * @param serviceName
     * @param serviceType
     */
    protected void addService (String serviceName, 
                               String serviceType,
                               String visibility) throws Exception 
    {
        super.addService (serviceName,
                          serviceType,
                          visibility);

        // Validate that the visibility type is either "public" or "private"
        if (!((Arrays.asList(mVisibilityTypes)).contains(visibility))) {
            String errorMsg = sLoc_.t("0008: Invalid service visibility type specified: {0}.",visibility);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }

        String sName = IFLUtil.trimQuotes(serviceName);
        String mapType = this.mServicesMap.get(sName);

        if (mapType == null) {
            this.mServicesMap.put(sName, serviceType);
            this.mServicesScopeMap.put(sName,this.mScopeName);
            this.mServicesVisibilityMap.put(sName,visibility);
        }
    }

    
    /**
     * Add the services to the Group Map
     * @param groupName
     * @param providesServiceList - list of provider services
     * @param consumesServiceList - list of consumer services
     */
    protected void addServicesToGroup (String groupName,
                                       List<String> providesServiceList,
                                       List<String> consumesServiceList,
                                       List<String> unknownServiceList) throws Exception
    {
        super.addServicesToGroup (groupName,
                                  providesServiceList,
                                  consumesServiceList,
                                  unknownServiceList);

        // Lets validate that this group does not already exist
        groupName = IFLUtil.trimQuotes(groupName);
        String mapType = this.mServicesMap.get(groupName);
        if (this.mServiceGroups.containsKey(groupName)) {
            String errorMsg = sLoc_.t("0004: Duplicate group name found: {0}",groupName);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }

        // Add the providers and consumers to the group map
        Map<String,List<String>> map = new HashMap<String,List<String>>();
        map.put(IFLUtil.GROUP_SERVICE_PROVIDERS, IFLUtil.trimQuotes(providesServiceList));
        map.put(IFLUtil.GROUP_SERVICE_CONSUMERS, IFLUtil.trimQuotes(consumesServiceList));
        if (unknownServiceList != null) {
            map.put(IFLUtil.GROUP_SERVICE_UNKNOWN, IFLUtil.trimQuotes(unknownServiceList));
        }

        // Save the consumed services that were defined after the "call" statement.
        List callsList = new ArrayList();
        callsList.addAll(IFLUtil.trimQuotes(consumesServiceList));
        map.put(IFLUtil.GROUP_SERVICE_CALLS, IFLUtil.trimQuotes(callsList));
        
        this.mServiceGroups.put(groupName, map);
    }


    /**
     * Add the service to the group map
     * @param serviceName
     */
    protected void addServiceToGroup (String serviceName) throws Exception
    {
        List providerList = new ArrayList();
        List consumerList = new ArrayList();
        List unknownList  = new ArrayList();
        unknownList.add(serviceName);
        addServicesToGroup (serviceName,
                            providerList,
                            consumerList,
                            unknownList);
    }


    /**
     * Add the service definition info to the parsed model
     * @param serviceName
     * @param serviceType
     */
    protected void addServices (String groupType, 
                                List<String> providesServiceList,
                                List<String> consumesServiceList,
                                List<String> unknownServiceList,
                                String visibility) throws Exception
    {
        for (int i = 0; i < providesServiceList.size(); i++) {
            String sName = providesServiceList.get(i);
            addService (sName, groupType, visibility);
        }
        for (int i = 0; i < consumesServiceList.size(); i++) {
            String sName = consumesServiceList.get(i);
            addService (sName, groupType, visibility);
        }
        for (int i = 0; i < unknownServiceList.size(); i++) {
            String sName = unknownServiceList.get(i);
            addService (sName, groupType, visibility);
        }
    }


    /**
     * adds the route definition info to the parsed model
     * @param fromService
     * @param toServiceList
     * @param name route name in the ifl
     * @return route name . can be null if a default route name should be generated
     */
    protected String addRoute (boolean inBroadcast,
                               String fromService, 
                               List<String> toServiceList, 
                               List<String> path, 
                               String name,
                               String visibility,
                               List<String> eipIds) throws Exception {

        String routeId = super.addRoute (inBroadcast,
                                         fromService,
                                         toServiceList,
                                         path,
                                         name,
                                         visibility,
                                         eipIds);

        String trimmedFromService = null;
        if (fromService != null) {
            trimmedFromService = IFLUtil.trimQuotes(fromService);
        }
        
        String routeName = null;
        if  (name != null) {
            routeName = IFLUtil.trimQuotes(name).trim();
        }

        // Validate the visibility identifier
        if (!((Arrays.asList(mVisibilityTypes)).contains(visibility))) {
            String errorMsg = sLoc_.t("0007: Invalid route type specified: {0}.",visibility);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }

        if (routeName == null) {
            routeName = trimmedFromService != null ? trimmedFromService : routeId;
        }

        // Validate that the name being used is unique.
        if (mRoutePaths.containsKey(routeName)) {
            String errorMsg = sLoc_.t("0010: Duplicate name \"{0}\" was specified for \"{1}\".",routeName,"route");
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }

        this.mRouteList.put(routeName, createRouteEntry(trimmedFromService, serviceNameOnly(IFLUtil.trimQuotes(toServiceList))));
        this.mRoutePaths.put(routeName, createRouteEntry(trimmedFromService, IFLUtil.trimQuotes(path)));
        this.mEipIds.put(routeId, createRouteEntry(trimmedFromService, IFLUtil.trimQuotes(eipIds)));
        this.mRouteVisibility.put(routeName, visibility);
        
//System.out.println ("");
//System.out.println ("*** routeName  : " + routeName);
//System.out.println ("*** routeId    : " + routeId);
//System.out.println ("*** mRouteList : " + mRouteList);
//System.out.println ("*** mRoutePaths: " + mRoutePaths);
//System.out.println ("*** mEipIds    : " + mEipIds);
//System.out.println ("*** visibility : " + visibility);

        return routeName;
    }

    /**
     * adds the broadcast data to the broadcast data map.
     * @param broadcastData
     * @param toService List
     */
    protected String addBroadcast (Map<String, Object> data, 
                                 List<String> toServiceList) throws IFLParseException {
        
        String eipId = super.addBroadcast (data, toServiceList);

        // broadcasts without a name should be assigned one based on the order 
        // they are added, e.g. broadcast${n}.
        if (data.get(Flow.Keys.NAME.toString()) == null) {
            data.put(Flow.Keys.NAME.toString(), eipId);
        }
        
        Map<String, Object> broadcastData = new HashMap<String, Object>();
        broadcastData.putAll(data);
        broadcastData.put(Flow.Keys.TO.toString(), IFLUtil.trimQuotes(toServiceList));

        // Place the EIP information into the global Broadcast Map
        putEipMap (broadcastData,mBroadcastMap,"broadcast");

        return eipId;
    }

    /**
     * add the tee data and a sevicelist in the embedded route or a route ref.
     * @param data
     * @param endpointRef name of the endpoint reference (declared  service name or endpoint name)
     */
    protected String addTee (Map<String, Object> data, 
                           String endpointRef) throws IFLParseException {

        String eipId = super.addTee (data, endpointRef);

        // tees without a name should be assigned one based on the order
        // they are added, e.g. tee${n}.
        if (data.get(Flow.Keys.NAME.toString()) == null) {
            data.put(Flow.Keys.NAME.toString(), eipId);
        }

        Map<String, Object> teeData = new HashMap<String, Object>();
        teeData.putAll(data);

        if (endpointRef != null) {
            teeData.put(Flow.Keys.TO.toString(), IFLUtil.trimQuotes(endpointRef));
        }

        // Place the EIP information into the global Tee Map
        putEipMap (teeData,mTeeMap,"tee");

        return eipId;
    }

    /**
     * adds the select data to the select map.
     * @param selectData
     * @param toService
     */
    protected String addSelect (Map<String, Object> data, 
                                List<String> toServiceList) throws IFLParseException
    {
        boolean     valueBased = false;
        boolean     conditionBased = false;

        String eipId = super.addSelect (data, toServiceList);

        if (data.get(Flow.Keys.NAME.toString()) == null ) {
            data.put(Flow.Keys.NAME.toString(), eipId);
        }
        Map<String, Object> selectData = new HashMap<String, Object>();
        selectData.putAll(data);
        List<List<String>>  whenMap = new LinkedList();
        for (Iterator it = toServiceList.iterator(); it.hasNext(); ) {
            String type = IFLUtil.trimQuotes((String)it.next());
            String value = "";
            String route = "";
            if (it.hasNext())
            {
                value = IFLUtil.trimQuotes((String)it.next());
            }
            if (it.hasNext())
            {
                route = IFLUtil.trimQuotes((String)it.next());
            }
            if (value == null || route == null) {
                String errorMsg = sLoc_.t("Select clause is missing value and/or route");
                LOG.fine(errorMsg);
                throw new IFLParseException(errorMsg);
            }
            if (type.equals("else")) {
                selectData.put(Flow.Keys.ELSE.toString(), IFLUtil.trimQuotes(route));
                continue;
            }
            if (type.equals("when")) {
                valueBased = true;
            } else {
                conditionBased = true;
            }
            List    l = new LinkedList();
            l.add(type); l.add(value); l.add(route);
            whenMap.add(l);
        }
        if (valueBased && conditionBased) {
            String errorMsg = sLoc_.t("Select when clauses can only contain values or conditions, not both.");
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg);
        } else if (valueBased == false && conditionBased == false) {
            conditionBased = true;
        }
        selectData.put(Flow.Keys.WHEN.toString(), whenMap);
        selectData.put(Flow.Keys.SELECT.toString(), valueBased ? "value" : "condition");

        // Place the EIP information into the global Select Map
        putEipMap (selectData,mSelectMap,"select");

        return eipId;
    }

    /**
     * create the Map.Entry that represents the route defintion.
     * @param fromService "from" sevicename. could be null.
     * @param toServiceList list of to services or a empty list.
     * @return
     */
    protected Map.Entry<String, List<String>> createRouteEntry(String fromService, List<String> toServiceList) {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        map.put(fromService, toServiceList);
        return map.entrySet().iterator().next();
    }

    /**
     * Creates a new string literal command map and populates the data.
     * String literal commands are: Split, Aggregate, Select & Filter
     * @param fromService
     * @param type
     * @param name
     * @param config
     * @return
     */
    protected static Map<String, Object> createStringLiteralMap (String fromService, 
                                                                 String type, 
                                                                 String name, 
                                                                 String config,
                                                                 String eipType) {
        String trimedFromService = IFLUtil.trimQuotes(fromService);
        Map<String, Object> commandData = new HashMap<String, Object>();
        boolean             expDefault = false;

        if (type != null && (type.equals("regex") || type.equals("xpath"))) {
            expDefault = true;
        }
        commandData.put(Flow.Keys.FROM.toString(), trimedFromService);
        commandData.put(Flow.Keys.TYPE.toString(), type);
        commandData.put(Flow.Keys.CONFIG.toString(), IFLUtil.parseNVpairs(IFLUtil.trimQuotes(config), expDefault));
        commandData.put(Flow.Keys.NAME.toString(), IFLUtil.trimQuotes(name));
        //commandData.put(Flow.Keys.EIP_TYPE.toString(), eipType);
        return commandData;
    }

    /**
     * Will store the data in the source EIP data map into the destination global map.
     * Note, validation will be performed to make sure the name is unique, since the
     * name is used as the key in the destination map.
     */
    private void putEipMap (Map sourceMap, Map destinationMap, String eipType) throws IFLParseException {
        String name = (String)sourceMap.get(Flow.Keys.NAME.toString());
        if (destinationMap.containsKey(name)) {
            String errorMsg = sLoc_.t("0010: Duplicate name \"{0}\" was specified for \"{1}\".",name,eipType);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }
        destinationMap.put(name, sourceMap);
    }


    /**
     * adds the split data to the split map.
     * @param splitData
     * @param toService
     */
    protected String addSplit (Map<String, Object> splitData) throws IFLParseException {

        String splitId = super.addSplit (splitData);

        if (this.mSplitMap == null) {
            this.mSplitMap = new LinkedHashMap<String, Map<String, Object>>();
        }

        if (splitData == null) {
            LOG.warning(sLoc_.t("0001: Trying to add null (0} data to IFL Parsed Model", "split"));
            return null;
        }
        
        // splits without a name should be assigned one based on the order 
        // they are added, e.g. split${n}.
        if (splitData.get(Flow.Keys.NAME.toString()) == null) {
            splitData.put(Flow.Keys.NAME.toString(), splitId);
        }
        
        // Place the EIP information into the global Split Map
        putEipMap (splitData,mSplitMap,"split");

        return splitId;
    }

    /**
     * adds the aggregate data to the aggregate map.
     * @param aggregateData
     * @param toService
     */
    protected String addAggregate (Map<String, Object> aggregateData) throws IFLParseException {

        String eipId = super.addAggregate (aggregateData);

        if (this.mAggregateMap == null) {
            this.mAggregateMap = new LinkedHashMap<String, Map<String, Object>>();
        }

        if (aggregateData == null) {
            LOG.warning(sLoc_.t("0001: Trying to add null (0} data to IFL Parsed Model", "aggregate"));
            return null;
        }
        
        // aggregrates without a name should be assigned one based on the order 
        // they are added, e.g. aggregate${n}.
        if (aggregateData.get(Flow.Keys.NAME.toString()) == null) {
            aggregateData.put(Flow.Keys.NAME.toString(), eipId);
        }

        // Place the EIP information into the global Aggregate Map
        putEipMap (aggregateData,mAggregateMap,"aggregate");

        return eipId;
    }

    /**
     * adds the filter data to the filter map.
     * @param filterData
     * @param toService
     */
    protected String addFilter (Map<String, Object> filterData) throws IFLParseException {

        String eipId = super.addFilter (filterData);

        if (this.mFilterMap == null) {
            this.mFilterMap = new LinkedHashMap<String, Map<String, Object>>();
        }

        if (filterData == null) {
            LOG.warning(sLoc_.t("0001: Trying to add null (0} data to IFL Parsed Model", "filter"));
            return null;
        }
        
        // filters without a name should be assigned one based on the order 
        // they are added, e.g. filter${n}.
        if (filterData.get(Flow.Keys.NAME.toString()) == null) {
            filterData.put(Flow.Keys.NAME.toString(), eipId);
        }
        
        // Place the EIP information into the global Filter Map
        putEipMap (filterData,mFilterMap,"filter");

        return eipId;
    }

    /**
     * Return only the name of a service (i.e. remove any namespace or operation info)
     * @param complete service name
     * @return service name only
     */
    protected String serviceNameOnly (String name) {
        Map<String,String> serviceMap = parseServiceName (name);
        name = serviceMap.get(IFLUtil.SERVICE_NAME_SERVICE);
        return name;

    }

    /**
     * Return a list of only the service names for(i.e. remove any namespace or operation info)
     * @param list complete service name
     * @return list service name only
     */
    protected List<String> serviceNameOnly(List<String> names) {
        for (int i = 0; i < names.size(); i++) {
            names.set(i, serviceNameOnly(names.get(i)));
        }
        return names;
    }

    /**
     * Validate the visibility modifier is public.
     * @param visibility value
     */
    protected void isVisibilityPublic (String visibility)  throws Exception {
        if (!(visibility.equals(IFLUtil.VISIBILITY_PUBLIC))) {
            String errorMsg = sLoc_.t("0009: Invalid visibility modifier specified: {0}.",visibility);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }
    }

    /**
     * Validate the visibility modifier is private.
     * @param visibility value
     */
    protected void isVisibilityPrivate (String visibility)  throws Exception {
        if (!(visibility.equals(IFLUtil.VISIBILITY_PRIVATE))) {
            String errorMsg = sLoc_.t("0009: Invalid visibility modifier specified: {0}.",visibility);
            LOG.fine(errorMsg);
            throw new IFLParseException(errorMsg); 
        }
    }

}
