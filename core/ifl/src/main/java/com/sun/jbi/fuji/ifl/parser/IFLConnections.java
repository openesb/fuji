/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Route;
import com.sun.jbi.fuji.ifl.Localizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLConnections {

    private Map<String, Connection> mConnectionMap = new LinkedHashMap<String, Connection>();

    public IFLConnections() {
    }

    public void add (Connection connection) {
        String name = connection.getName();
        mConnectionMap.put(name,connection);
    }

    public int getCount () {
        int totalRoutes = mConnectionMap.size();
        return totalRoutes;
    }

    public void putAll (Map<String,Connection> connection) {
        mConnectionMap.putAll(connection);
    }

    public void putAll (IFLConnections connections) {
        mConnectionMap.putAll(connections.getMap());
    }

    public Map<String,Connection> getMap() {
        return mConnectionMap;
    }

    public List getConnections() {
        List list = new ArrayList();
        for (Map.Entry<String, Connection> entry : mConnectionMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Connection getConnection (String connectionName) {
        Connection connection = mConnectionMap.get(connectionName);
        return connection;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        for (Map.Entry<String, Connection> entry : mConnectionMap.entrySet()) {
            nameList.add(entry.getKey());
        }
        return nameList;
    }

    public String toString() {
        String outString = "";
        Set<String> set = mConnectionMap.keySet();
        for (String key : set)
        {
            Connection connection = mConnectionMap.get(key);
            outString += "\n" + connection.toString();
        }
        return outString;
    }

}
