/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.fuji.ifl.parser.IFLNamespaces;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author markrs
 */

public class Route {

    private String  mRouteId      = null;
    private String  mRouteName    = null;
    private String  mFromService  = null;
    private String  mVisibility   = null;
    private String  mBroadcastId  = null;
    private List    mServiceNames = new ArrayList();
    private List    mRoutePaths   = new ArrayList();
    private List    mEipIds       = new ArrayList();
    private List<RouteEntry> mRouteEntries = new ArrayList<RouteEntry>();
    private boolean mProvided     = false;
    private boolean mConsumed     = false;
    private String  mFilename     = null;


    public Route () {
    }

    public Route (boolean inBroadcast,
                  String routeName,
                  String fromService,
                  String visibility,
                  List<String> serviceNames,
                  List<String> routePaths,
                  List<String> eipIds) 
    {

        // Trim off any quotes that may be around the from service and route name
        fromService  = IFLUtil.trimQuotes(fromService);
        routeName    = IFLUtil.trimQuotes(routeName);
        serviceNames = IFLUtil.trimQuotes(serviceNames);
        routePaths   = IFLUtil.trimQuotes(routePaths);
        eipIds       = IFLUtil.trimQuotes(eipIds);

        // If the route name was not specified, then use the name of the from service, and
        // if the from service name does not exist, use the route id as the namee.
        if (routeName == null) {
            routeName = fromService != null ? fromService : IFLUtil.getIdFromMap("route");
        }

        setId (IFLUtil.getIdFromMap("route"));
        setName (routeName);
        setFromServiceName (fromService);
        setVisibility (visibility);
        setServiceNames (serviceNames);
        setPaths (routePaths);
        setEipIds (eipIds);
        setRouteEntry (serviceNames, routePaths, eipIds);

        // We need to set the broadcastId if the route was defined inside of a broadcast statement
        if (inBroadcast) {
            setBroadcastId (IFLUtil.getIdFromMap("broadcast"));
        }

        // The Route itself is a provided service 
        setProvided(true);
    }

    public String toString () {
        String outString = "";
        outString += "Route Id     : " + mRouteId + "\n";
        outString += "Route Name   : " + mRouteName + "\n";
        outString += "Visibility   : " + mVisibility + "\n";
        outString += "From Service : " + mFromService + "\n";
        outString += "Broadcast Id : " + mBroadcastId + "\n";
        outString += "Provided     : " + mProvided + "\n";
        outString += "Consumed     : " + mConsumed + "\n";

        //outString += "Service Names: " + mServiceNames + "\n";
        //outString += "Route Paths: " + mRoutePaths + "\n";
        int entryNum = 0;
        for (RouteEntry entry: mRouteEntries) {
            outString += "  ------(" + entryNum++ + ")------\n";
            outString += entry.toString();
        }
        return outString;
    }

    public void setId (String routeId) {
        mRouteId = routeId;
    }

    public void setName (String routeName) {
        mRouteName = routeName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setVisibility (String visibility) {
        mVisibility = visibility;
    }

    public void setServiceNames (List<String> serviceNames) {
        mServiceNames.addAll(IFLUtil.trimQuotes(serviceNames));
    }

    public void setPaths (List<String> routePaths) {
        mRoutePaths.addAll(routePaths);
    }

    public void setEipIds (List<String> eipIds) {
        mEipIds.addAll(eipIds);
    }

    public void setBroadcastId (String broadcastId) {
        mBroadcastId = broadcastId;
    }


    public void setRouteEntry (List<String> serviceNames, 
                               List<String> routePaths,
                               List<String> eipIds) {

        // The route Size and eip size will be equal, If not there is a coding error in the ifl parser
        int size = routePaths.size();

        for (int i=0; i<size; i++)
        {
            String routePath   = (String)routePaths.get(i);
            String eipId       = (String)eipIds.get(i);
            String serviceName = null;
            if (serviceNames.contains(routePath)) {
                serviceName = routePath;
            }
            RouteEntry entry = new RouteEntry (routePath,
                                               serviceName,
                                               eipId);
            mRouteEntries.add(entry);
        }
    }

    public void setProvided (boolean bValue) {
        mProvided = bValue;
    }

    public void setConsumed (boolean bValue) {
        mConsumed = bValue;
    }

    public void setFilename (String filename) {
        mFilename = filename;
    }

    public boolean isConsumed () {
        return mConsumed;
    }

    public boolean isProvided () {
        return mProvided;
    }

    public String getName() {
        return mRouteName;
    }

    public String getId() {
        return mRouteId;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public String getVisibility() {
        return mVisibility;
    }

    public int getCount() {
        return mRouteEntries.size();
    }

    public List<String> getServiceNames() {
        return mServiceNames;
    }

    public List<String> getPaths() {
        return mRoutePaths;
    }

    public List<RouteEntry> getRouteEntries() {
        return mRouteEntries;
    }

    public String getFilename() {
        return mFilename;
    }

    public String getBroadcastId () {
        return mBroadcastId;
    }

    public Route.RouteEntry getRouteEntry (String name, String type) {
        Route.RouteEntry returnEntry = null;
        for (int i=0; i<mRouteEntries.size(); i++) {
            RouteEntry entry = mRouteEntries.get(i);
            if (entry.getEipId().equals(type)) {
                if (entry.getRoutePath().equals(name)) {
                    returnEntry = entry;
                    break;
                }
            }
        }
        return returnEntry;
    }

    public void resolveNamespaces (IFLNamespaces iflNamespaces, 
                                   String applicationName,
                                   IFLServices iflServices) throws IFLParseException {
        for (int i=0; i<mRouteEntries.size(); i++) {
            RouteEntry entry = mRouteEntries.get(i);
            String serviceName = entry.getServiceName();
            String resolvedNamespace = "";
            if (serviceName != null) {
                Service service = iflServices.getService(serviceName);

                // Since the service was not defined, we will default to "public"  Later we may want
                // to throw an exception.
                String visibility = "public";
                if (service == null) {
                    if (!(visibility.equals("public"))) {  // REMOVE if statement to throw and exception.
                        String errorMsg = "The service " + serviceName + " was not defined.";
                        throw new IFLParseException(errorMsg);
                    }
                }
                else {
                    visibility = service.getVisibility();
                }

                String filename = getFilename();
                String namespacePrefix = entry.getNamespace();

                if (namespacePrefix.toLowerCase().startsWith("http:")) {
                    resolvedNamespace = namespacePrefix;
                }
                else {
                    String publicDefaultNS  = IFLUtil.buildResolvedString (applicationName, IFLUtil.DEFAULT_PUBLIC_NS);
                    String privateDefaultNS = IFLUtil.buildResolvedString (applicationName, IFLUtil.DEFAULT_PRIVATE_NS);
                    String base_ns    = iflNamespaces.getNamespaceValue(filename,"base-ns", IFLUtil.DEFAULT_BASE_NS);
                    String public_ns  = iflNamespaces.getNamespaceValue(filename,"public-ns", publicDefaultNS);
                    String private_ns = iflNamespaces.getNamespaceValue(filename,"private-ns", privateDefaultNS);

                    String publicPrivateValue = private_ns;
                    if (visibility.equalsIgnoreCase("public")) {
                        publicPrivateValue = public_ns;
                    }
                    resolvedNamespace = IFLUtil.buildResolvedString (base_ns, publicPrivateValue);

                    if (namespacePrefix.length() > 0) {
                        String namespaceValue = iflNamespaces.getNamespaceValue(filename,namespacePrefix);
                        if (namespaceValue.equals("")) {
                            namespaceValue = namespacePrefix;
                        }
                        resolvedNamespace = IFLUtil.buildResolvedString (resolvedNamespace, namespaceValue);
                    }
                }
                // Since the serviceName is already a field in the route object, we will not append
                // it to the resolved namespace.
                //resolvedNamespace = IFLUtil.buildResolvedString (resolvedNamespace, serviceName);
            }
            entry.setResolvedNamespace(resolvedNamespace);
        }
    }

    // Inner class to hold the route entries
    public class RouteEntry {
        private String  mRoutePath         = null;
        private String  mServiceName       = null;
        private String  mEipId             = null;
        private String  mOperation         = null;
        private String  mNamespace         = null;
        private String  mResolvedNamespace = null;

        public RouteEntry (String routePath,
                           String serviceName,
                           String eipId) 
        {
            String service = null;
            Map<String,String> routeMap = IFLUtil.parseServiceName(routePath);
            String namespace = routeMap.get(IFLUtil.SERVICE_NAME_NAMESPACE);
            String route     = routeMap.get(IFLUtil.SERVICE_NAME_SERVICE);
            String operation = routeMap.get(IFLUtil.SERVICE_NAME_OPERATION);
            if (serviceName != null) {
                Map<String,String> serviceMap = IFLUtil.parseServiceName(serviceName);
                service = serviceMap.get(IFLUtil.SERVICE_NAME_SERVICE);
            }
            setRoutePath (route);
            setServiceName (service);
            setOperation (operation);
            setNamespace (namespace);
            setEipId (eipId);
        }

        public String toString () {
            String outString = "";
            outString += "  Route Path         : " + mRoutePath + "\n";
            outString += "  Eip Id             : " + mEipId + "\n";
            outString += "  Service Name       : " + mServiceName + "\n";
            outString += "  Namespace          : " + mNamespace + "\n";
            outString += "  Resolved Namespace : " + mResolvedNamespace + "\n";
            outString += "  Operation          : " + mOperation + "\n";
            return outString;
        }

        public void setRoutePath (String routePath) {
            mRoutePath = routePath;
        }

        public void setServiceName (String serviceName) {
            mServiceName = serviceName;
        }

        public void setOperation (String operation) {
            mOperation = operation;
        }

        public void setNamespace (String namespace) {
            mNamespace = namespace;
        }

        public void setEipId (String eipId) {
            mEipId = eipId;
        }

        public void setResolvedNamespace (String resolvedNamespace) {
            mResolvedNamespace = resolvedNamespace;
        }

        public String getRoutePath () {
            return mRoutePath;
        }

        public String getServiceName () {
            return mServiceName;
        }

        public String getOperation () {
            return mOperation;
        }

        public String getNamespace () {
            return mNamespace;
        }

        public String getResolvedNamespace () {
            return mResolvedNamespace;
        }

        public String getEipId () {
            return mEipId;
        }

    }



}
