/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * @author markrs
 */

public class Tee {

    private String     mTeeId       = null;
    private String     mTeeName      = null;
    private String     mFromService  = null;
    private String     mTeeType      = null;
    private String     mTeeEndPoint  = null;
    private String     mTeeRouteId  = null;
    private Properties mTeeConfig    = new Properties();

    public Tee () {
    }

    public Tee (Map<String, Object> teeData, String endPoint)
    {
        endPoint  = IFLUtil.trimQuotes(endPoint);

        setName ((String)teeData.get(Flow.Keys.NAME.toString()));
        setFromServiceName ((String)teeData.get(Flow.Keys.FROM.toString()));
        setConfig ((Properties)teeData.get(Flow.Keys.CONFIG.toString()));
        setType ((String)teeData.get(Flow.Keys.TYPE.toString()));
        setEndPoint (endPoint);
        setId (IFLUtil.getIdFromMap("tee"));
        setRouteId (IFLUtil.getIdFromMap("route"));

        // If the tee does not have a name, then use the id as the name
        if (mTeeName == null) {
            setName (getId());
        }

    }

    
    public void setName (String teeName) {
        mTeeName = teeName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setEndPoint (String endPoint) {
        mTeeEndPoint = endPoint;
    }

    public void setConfig (Properties teeConfig) {
        if (teeConfig != null)
            mTeeConfig.putAll(teeConfig);
    }

    public void setType (String teeType) {
        mTeeType = teeType;
    }

    public void setId (String id) {
        mTeeId = id;
    }

    public void setRouteId (String routeId) {
        mTeeRouteId = routeId;
    }


    public String getName() {
        return mTeeName;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public String getEndPoint () {
        return mTeeEndPoint;
    }

    public Properties getConfig () {
        return mTeeConfig;
    }

    public String getType () {
        return mTeeType;
    }

    public String getId () {
        return mTeeId;
    }

    public String getRouteId () {
        return mTeeRouteId;
    }

    public String toString () {
        String outString = "";
        outString += "Tee Id       : " + mTeeId + "\n";
        outString += "Tee Name     : " + mTeeName + "\n";
        outString += "From Service : " + mFromService + "\n";
        outString += "Tee Type     : " + mTeeType + "\n";
        outString += "Tee Endpoint : " + mTeeEndPoint + "\n";
        outString += "Route Id     : " + mTeeRouteId + "\n";
        outString += "Tee Config   : " + mTeeConfig + "\n";
        return outString;
    }

}
