/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Broadcast;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLBroadcasts {

    private Map<String, Broadcast> mBroadcastsMap = new LinkedHashMap<String, Broadcast>();


    public IFLBroadcasts() {
    }

    public void add (Broadcast broadcast) {
        String id = broadcast.getId();
        mBroadcastsMap.put(id,broadcast);
        IFLUtil.incrementIdInMap("broadcast");
    }

    public boolean containsKey (String key) {
        return mBroadcastsMap.containsKey(key);
    }

    public boolean containsName (String name) {
        List<String> nameList = getNames();
        return nameList.contains(name);
    }

    public int getCount () {
        int totalBroadcasts = mBroadcastsMap.size();
        return totalBroadcasts;
    }

    public void putAll (Map<String,Broadcast> broadcasts) {
        mBroadcastsMap.putAll(broadcasts);
    }

    public void putAll (IFLBroadcasts broadcasts) {
        mBroadcastsMap.putAll(broadcasts.getMap());
    }

    public Map<String,Broadcast> getMap() {
        return mBroadcastsMap;
    }

    public List getBroadcasts() {
        List list = new ArrayList();
        for (Map.Entry<String, Broadcast> entry : mBroadcastsMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Broadcast getBroadcast (String broadcastName) {
        Broadcast broadcast = mBroadcastsMap.get(broadcastName);
        return broadcast;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        Collection c = mBroadcastsMap.values();
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            Broadcast broadcast = (Broadcast)itr.next();
            String name = broadcast.getName();
            nameList.add(name);
        }
        return nameList;
    }

    public List<String> getIds() {
        List idList = new ArrayList();
        for (Map.Entry<String, Broadcast> entry : mBroadcastsMap.entrySet()) {
            idList.add(entry.getKey());
        }
        return idList;
    }

    public String toString() {
        String outString = "";
        Set<String> set = mBroadcastsMap.keySet();
        for (String key : set)
        {
            Broadcast broadcast = mBroadcastsMap.get(key);
            outString += "\n" + broadcast.toString();
        }
        return outString;
    }

}
