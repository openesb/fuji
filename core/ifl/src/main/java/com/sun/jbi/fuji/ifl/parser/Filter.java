/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * @author markrs
 */

public class Filter {

    private String     mFilterId        = null;
    private String     mFilterName      = null;
    private String     mFromService     = null;
    private String     mFilterType      = null;
    private String     mFilterRouteId   = null;
    private Properties mFilterConfig    = new Properties();

    public Filter () {
    }

    public Filter (Map<String, Object> filterData)
    {
        setName ((String)filterData.get(Flow.Keys.NAME.toString()));
        setFromServiceName ((String)filterData.get(Flow.Keys.FROM.toString()));
        setConfig ((Properties)filterData.get(Flow.Keys.CONFIG.toString()));
        setType ((String)filterData.get(Flow.Keys.TYPE.toString()));
        setId (IFLUtil.getIdFromMap("filter"));
        setRouteId (IFLUtil.getIdFromMap("route"));

        // If the filter does not have a name, then use the id as the name
        if (mFilterName == null) {
            setName (getId());
        }

    }

    
    public void setName (String filterName) {
        mFilterName = filterName;
    }

    public void setFromServiceName (String fromServiceName) {
        mFromService = fromServiceName;
    }

    public void setConfig (Properties filterConfig) {
        if (filterConfig != null)
            mFilterConfig.putAll(filterConfig);
    }

    public void setType (String filterType) {
        mFilterType = filterType;
    }

    public void setId (String id) {
        mFilterId = id;
    }

    public void setRouteId (String routeId) {
        mFilterRouteId = routeId;
    }


    public String getName() {
        return mFilterName;
    }

    public String getFromServiceName() {
        return mFromService;
    }

    public Properties getConfig () {
        return mFilterConfig;
    }

    public String getType () {
        return mFilterType;
    }

    public String getId () {
        return mFilterId;
    }

    public String getRouteId () {
        return mFilterRouteId;
    }

    public String toString () {
        String outString = "";
        outString += "Filter Id     : " + mFilterId + "\n";
        outString += "Filter Name   : " + mFilterName + "\n";
        outString += "From Service  : " + mFromService + "\n";
        outString += "Filter Type   : " + mFilterType + "\n";
        outString += "Route Id      : " + mFilterRouteId + "\n";
        outString += "Filter Config : " + mFilterConfig + "\n";
        return outString;
    }

}
