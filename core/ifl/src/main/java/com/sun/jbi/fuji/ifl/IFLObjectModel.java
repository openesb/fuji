/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLModel.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.ifl;

import com.sun.jbi.fuji.ifl.parser.IFLServices;
import com.sun.jbi.fuji.ifl.parser.IFLRoutes;
import com.sun.jbi.fuji.ifl.parser.IFLBroadcasts;
import com.sun.jbi.fuji.ifl.parser.IFLTees;
import com.sun.jbi.fuji.ifl.parser.IFLSelects;
import com.sun.jbi.fuji.ifl.parser.IFLSplits;
import com.sun.jbi.fuji.ifl.parser.IFLAggregates;
import com.sun.jbi.fuji.ifl.parser.IFLFilters;
import com.sun.jbi.fuji.ifl.parser.IFLServiceGroups;
import com.sun.jbi.fuji.ifl.parser.IFLConnections;
import com.sun.jbi.fuji.ifl.parser.IFLNamespaces;
import com.sun.jbi.fuji.ifl.parser.IFLParseException;

import java.lang.Integer;
import java.util.Map;


/**
 * This interface represents the model that will be derived from the ifl files.
 * All the required ifl files will be parsed, validated and conbined together 
 * to construct this model which will be used in various points like in the
 * service artifacts generation (in fuji:dist goal) or in i-app build validation
 * phase etc.
 * <p>
 * <pre>
 * IFLReader iflReader = new IFLReader();
 * IFLObjectModel model = iflReader.read(new File("/ifldir"));
 * model = iflReader.read(new File("/dir/myapp.ifl"));
 * model = iflReader.read(new File("/dir/1.ifl"), new File("/dir/2.ifl"));
 * </pre>
 * 
 * @see IFLReader
 * 
 * @author markrs
 */


public interface IFLObjectModel
{
    IFLRoutes getIFLRoutes () throws IFLParseException;
    IFLServices getIFLServices ();
    IFLBroadcasts getIFLBroadcasts ();
    IFLTees getIFLTees ();
    IFLSelects getIFLSelects ();
    IFLSplits getIFLSplits ();
    IFLFilters getIFLFilters ();
    IFLAggregates getIFLAggregates ();
    IFLServiceGroups getIFLServiceGroups ();
    IFLConnections getIFLConnections ();
    IFLNamespaces getIFLNamespaces ();

    String getApplicationId();
    void setApplicationId(String appId);


}
