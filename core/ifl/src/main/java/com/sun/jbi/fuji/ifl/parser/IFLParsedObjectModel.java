/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLParsedModel.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import java.util.List;
import java.util.Map;
import java.lang.Integer;


/**
 * This interface represents the model parsed from the ifl file. The ifl parser
 * implements this interface to convert the parsed file to the memory model. This
 * interface will be used in the IFL object model <code>IFLModel</code> to constrcut 
 * the IFL Object model from the parsed model.
 * 
 * @see com.sun.jbi.fuji.ifl.IFLObjectModel
 * @see com.sun.jbi.fuji.ifl.IFLReader
 * @see com.sun.jbi.fuji.ifl.IFLObjectModelImpl
 * @see AbstractIFLParser
 * 
 * @author markrs
 */
public interface IFLParsedObjectModel
{

    /**
     * Will retrieve the scope name
     */
    String getScopeName();
    String getApplicationId();

    IFLRoutes getRoutes ();
    IFLServices getServices ();
    IFLBroadcasts getBroadcasts ();
    IFLTees getTees ();
    IFLSelects getSelects ();
    IFLSplits getSplits ();
    IFLFilters getFilters ();
    IFLAggregates getAggregates ();
    IFLServiceGroups getServiceGroups ();
    IFLConnections getConnections ();
    IFLNamespaces getNamespaces ();
}
