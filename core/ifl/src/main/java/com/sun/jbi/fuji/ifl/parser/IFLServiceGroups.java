/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.ServiceGroup;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;


/**
 * @author markrs
 */

public class IFLServiceGroups {

    private Map<String, ServiceGroup> mServiceGroupsMap = new LinkedHashMap<String, ServiceGroup>();

    public IFLServiceGroups() {
    }

    public void add (ServiceGroup serviceGroup) {
        String name = serviceGroup.getName();
        String groupName = IFLUtil.trimQuotes(name);
        mServiceGroupsMap.put(groupName,serviceGroup);
    }

    public int getCount () {
        int totalServices = mServiceGroupsMap.size();
        return totalServices;
    }

    public void putAll (Map<String,ServiceGroup> serviceGroups) {
        mServiceGroupsMap.putAll(serviceGroups);
    }

    public void putAll (IFLServiceGroups serviceGroups) {
        mServiceGroupsMap.putAll(serviceGroups.getMap());
    }

    public Map<String,ServiceGroup> getMap() {
        return mServiceGroupsMap;
    }

    public List<ServiceGroup> getServiceGroups() {
        List list = new ArrayList();
        for (Map.Entry<String, ServiceGroup> entry : mServiceGroupsMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public ServiceGroup getServiceGroup (String groupName) {
        ServiceGroup group = mServiceGroupsMap.get(groupName);
        return group;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        for (Map.Entry<String, ServiceGroup> entry : mServiceGroupsMap.entrySet()) {
            nameList.add(entry.getKey());
        }
        return nameList;
    }

    public String toString() {
        String outString = "";
        Set<String> set = mServiceGroupsMap.keySet();
        for (String key : set)
        {
            ServiceGroup serviceGroup = mServiceGroupsMap.get(key);
            outString += "\n" + serviceGroup.toString();
        }
        return outString;
    }

}
