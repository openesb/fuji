/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.Localizer;
//import javax.xml.namespace.QName;
//import javax.xml.XMLConstants;
import com.sun.jbi.fuji.ifl.IFLUtil;


/**
 * @author markrs
 */

public class Service {

    //private String mServiceType  = null;
    //private String mLocalPart    = null;
    //private String mPrefix       = XMLConstants.DEFAULT_NS_PREFIX;
    //private String mNamespaceURI = XMLConstants.NULL_NS_URI;

    private String mServiceName         = null;
    private String mServiceType         = null;
    private String mServiceVisibility   = null;
    private String mServiceIFLFile      = null;
    private String mServiceGroupName    = null;
    private boolean mProvided           = false;
    private boolean mConsumed           = false;

    public Service() {
    }

    public Service (String serviceName,
                    String serviceType,
                    String serviceVisibility,
                    String serviceIFLFile)
    {
        serviceName  = IFLUtil.trimQuotes(serviceName);
        setType (serviceType);
        setName (serviceName);
        setVisibility (serviceVisibility);
        setIFLFilename (serviceIFLFile);
    }

    public String toString () {
        String outString = "";
        outString += "  Service Name     : " + mServiceName + "\n";
        outString += "  Service Type     : " + mServiceType + "\n";
        outString += "  Visibility       : " + mServiceVisibility + "\n";
        outString += "  IFL Filename     : " + mServiceIFLFile + "\n";
        outString += "  Provided         : " + mProvided + "\n";
        outString += "  Consumed         : " + mConsumed + "\n";
        /** May also want to add QName here (when it works) as well. **/
        return outString;
    }

    public void setType (String serviceType) {
        mServiceType = serviceType;
    }

    public void setName (String serviceName) {
        mServiceName = serviceName;
    }

    public void setVisibility (String serviceVisibility) {
        mServiceVisibility = serviceVisibility;
    }

    public void setIFLFilename (String serviceIFLFile) {
        mServiceIFLFile = serviceIFLFile;
    }

    public void setProvided (boolean bValue) {
        mProvided = bValue;
    }

    public void setGroupName (String serviceGroupName) {
        mServiceGroupName = IFLUtil.trimQuotes(serviceGroupName);
    }

    public void setConsumed (boolean bValue) {
        mConsumed = bValue;
    }

    public boolean isConsumed () {
        return mConsumed;
    }

    public boolean isProvided () {
        return mProvided;
    }

    /*** This still needs work.  Need to construct the QName and return it **/
    //public QName getQName() {
    //    String namespaceURI = XMLConstants.NULL_NS_URI;
    //    String prefix = XMLConstants.DEFAULT_NS_PREFIX;
    //    String localPart = getName();
    //    QName myQName = new QName(namespaceURI,localPart,prefix);
    //    return myQName;
    //}

    public String getType() {
        return mServiceType;
    }

    public String getName() {
        return mServiceName;
    }

    public String getVisibility() {
        return mServiceVisibility;
    }

    public String getIFLFilename() {
        return mServiceIFLFile;
    }

    public String getGroupName () {
        return mServiceGroupName;
    }

    //public String getCategory() {
    //    return mServiceCategory;
    //}

}
