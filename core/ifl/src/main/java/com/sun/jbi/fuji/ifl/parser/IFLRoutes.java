/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Route;
import com.sun.jbi.fuji.ifl.parser.IFLNamespaces;
import com.sun.jbi.fuji.ifl.parser.IFLServices;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLRoutes {

    private Map<String, Route> mRoutesMap = new LinkedHashMap<String, Route>();


    public IFLRoutes() {
    }

    public void add (Route route) {
        String routeId = route.getId();
        mRoutesMap.put(routeId,route);
        IFLUtil.incrementIdInMap("route");
    }

    public boolean containsKey (String key) {
        return mRoutesMap.containsKey(key);
    }

    public boolean containsName (String name) {
        List<String> nameList = getNames();
        return nameList.contains(name);
    }

    public int getCount () {
        int totalRoutes = mRoutesMap.size();
        return totalRoutes;
    }

    public void putAll (Map<String,Route> routes) {
        mRoutesMap.putAll(routes);
    }

    public void putAll (IFLRoutes routes) {
        mRoutesMap.putAll(routes.getMap());
    }

    public Map<String,Route> getMap() {
        return mRoutesMap;
    }

    public List<Route> getRoutes() {
        List list = new ArrayList();
        for (Map.Entry<String, Route> entry : mRoutesMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Route getRoute (String routeId) {
        Route route = mRoutesMap.get(routeId);
        return route;
    }

    public List<Route> getRoutesByName (String name) {
        List list = new ArrayList();
        for (Map.Entry<String, Route> entry : mRoutesMap.entrySet()) {
            Route routeEntry = entry.getValue();
            if (routeEntry.getName().equals(name)) {
                list.add(entry.getValue());
            }
        }
        return list;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        Collection c = mRoutesMap.values();
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            Route route = (Route)itr.next();
            String name = route.getName();
            nameList.add(name);
        }
        return nameList;
    }

    public List<String> getIds() {
        List idList = new ArrayList();
        for (Map.Entry<String, Route> entry : mRoutesMap.entrySet()) {
            idList.add(entry.getKey());
        }
        return idList;
    }

    public void resolveNamespaces (IFLNamespaces iflNamespaces, 
                                   String applicationName,
                                   IFLServices iflServices) throws IFLParseException {
        Collection c = mRoutesMap.values();
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            Route route = (Route)itr.next();
            route.resolveNamespaces(iflNamespaces,applicationName,iflServices);
        }
    }

    public String toString() {
        String outString = "";
        Set<String> set = mRoutesMap.keySet();
        for (String key : set)
        {
            Route route = mRoutesMap.get(key);
            outString += "\n" + route.toString();
        }
        return outString;
    }

}
