/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLModel.java - Last published on 4/28/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.ifl;

/**
 * Enumeration containing names of IFL flow constructs.
 * 
 */
public enum Flow {
    BROADCAST ("broadcast"),    // EIP - broadcast
    FILTER ("filter"),          // EIP - message filter
    AGGREGATE ("aggregate"),    // EIP - aggregator
    SERVICE ("service"),        // service invoked in a flow
    SPLIT ("split"),            // EIP - splitter
    TEE ("tee"),                // EIP - tee (wiretap)
    SELECT ("select"),          // EIP - content based router
    WHEN ("when"),              // EIP - content based router
    ELSE ("else"),              // EIP - content based router
    ROUTE("route"),             // EIP - route
    TO ("to"),                  // EIP - route
    NAME ("name"),              // EIP - route
    EIP_ID ("eip_id");          // 

    /** Keys used in Flow definition map */
    public enum Keys {NAME, TYPE, CONFIG, FROM, TO, ROUTE, WHEN, ELSE, SELECT, EIP_ID};
    
    /** Java Type Config Keys */
    public enum JavaConfigKeys {
        PACKAGENAME("packagename"), 
        CLASSNAME("classname");
        
        private String configKey_;
        
        JavaConfigKeys(String configKey){
            configKey_ = configKey;
        }
        public String toString() {
            return configKey_;
        }
    
    };
    
    private String name_;
    
    Flow(String name) {
        name_ = name;
    }
    
    public String toString() {
        return name_;
    }
    
    public static Flow fromString(String flowName) {
        if (flowName.equalsIgnoreCase(BROADCAST.name_)) {
            return BROADCAST;
        }
        else if (flowName.equalsIgnoreCase(FILTER.name_)) {
            return FILTER;
        }
        else if (flowName.equalsIgnoreCase(AGGREGATE.name_)) {
            return AGGREGATE;
        }
        else if (flowName.equalsIgnoreCase(SPLIT.name_)) {
            return SPLIT;
        }
        else if (flowName.equalsIgnoreCase(SELECT.name_)) {
            return SELECT;
        }
        else if (flowName.equalsIgnoreCase(SERVICE.name_)) {
            return SERVICE;
        }
        else if (flowName.equalsIgnoreCase(TEE.name_)) {
            return TEE;
        }
        else if (flowName.equalsIgnoreCase(WHEN.name_)) {
            return WHEN;
        }
        else if (flowName.equalsIgnoreCase(ELSE.name_)) {
            return ELSE;
        }
        else if (flowName.equalsIgnoreCase(TO.name_)) {
            return TO;
        }
        else if (flowName.equalsIgnoreCase(NAME.name_)) {
            return NAME;
        }
        else if (flowName.equalsIgnoreCase(EIP_ID.name_)) {
            return EIP_ID;
        }
        else {
            throw new IllegalArgumentException(flowName);
        }
    }
    
    public String getExtensionClassName(){
        
        String extClassname = "";
        if (this.equals(AGGREGATE)) {
            extClassname = "Aggregator";
        } else if (this.equals(SPLIT)) {
            extClassname = "Split";
        } 
        return extClassname;
    }
}
