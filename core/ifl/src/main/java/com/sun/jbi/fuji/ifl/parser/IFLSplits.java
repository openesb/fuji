/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl.parser;

import com.sun.jbi.fuji.ifl.parser.Split;
import com.sun.jbi.fuji.ifl.Localizer;
import com.sun.jbi.fuji.ifl.IFLUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedHashMap;

/**
 * @author markrs
 */

public class IFLSplits {

    private Map<String, Split> mSplitsMap = new LinkedHashMap<String, Split>();


    public IFLSplits() {
    }

    public void add (Split split) {
        String id = split.getId();
        mSplitsMap.put(id,split);
        IFLUtil.incrementIdInMap("split");
    }

    public boolean containsKey (String key) {
        return mSplitsMap.containsKey(key);
    }

    public boolean containsName (String name) {
        List<String> nameList = getNames();
        return nameList.contains(name);
    }

    public int getCount () {
        int totalSplits = mSplitsMap.size();
        return totalSplits;
    }

    public void putAll (Map<String,Split> splits) {
        mSplitsMap.putAll(splits);
    }

    public void putAll (IFLSplits splits) {
        mSplitsMap.putAll(splits.getMap());
    }

    public Map<String,Split> getMap() {
        return mSplitsMap;
    }

    public List<Split> getSplits() {
        List list = new ArrayList();
        for (Map.Entry<String, Split> entry : mSplitsMap.entrySet()) {
            list.add(entry.getValue());
        }
        return list;
    }

    public Split getSplit (String splitId) {
        Split split = mSplitsMap.get(splitId);
        return split;
    }

    public List<String> getNames() {
        List nameList = new ArrayList();
        Collection c = mSplitsMap.values();
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            Split split = (Split)itr.next();
            String name = split.getName();
            nameList.add(name);
        }
        return nameList;
    }

    public List<String> getIds() {
        List idList = new ArrayList();
        for (Map.Entry<String, Split> entry : mSplitsMap.entrySet()) {
            idList.add(entry.getKey());
        }
        return idList;
    }

    public String toString() {
        String outString = "";
        Set<String> set = mSplitsMap.keySet();
        for (String key : set)
        {
            Split split = mSplitsMap.get(key);
            outString += "\n" + split.toString();
        }
        return outString;
    }


}
