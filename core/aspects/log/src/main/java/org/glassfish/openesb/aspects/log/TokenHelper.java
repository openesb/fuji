/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.log;

import org.glassfish.openesb.aspects.common.MessageExchangeHelper;
import org.glassfish.openesb.aspects.common.XPathEvaluator;
import java.lang.reflect.InvocationTargetException;
import javax.jbi.messaging.MessageExchange;

import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPathExpressionException;

/**
 *
 * @author rdara
 */
public class TokenHelper {
    static Pattern tokenPattern = Pattern.compile("\\$\\{([^{^$]*?)\\}", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    static Pattern tokenValuePattern = Pattern.compile("[A-Za-z_][A-Za-z0-9_.:\\/]+");
    static Map<String,Method> preBuiltTokensMap = new Hashtable<String, Method>();
    /**
     * @param args the command line arguments
     */
    static {
        prepareBuiltInTokens();
    }

    public static Map<String, String> getuserDefinedTokens(Properties properties) {
        Map<String, String> tokensMap = new Hashtable<String, String>();
        for(Object property : properties.keySet()) {
            String prop = (String) property;
            if(prop.startsWith("log.token.")) {
                tokensMap.put(prop.substring("log.token.".length()), (String) properties.get(prop));
            }
        }
        return tokensMap;
    }
    public static String parse(String freeFormString, MessageExchange msgEx, Map<String, String> tokensMap) {
        // TODO code application logic here
        /*
        tokensMap.put("Source","File");
        tokensMap.put("Destination","JRuby");
        tokensMap.put("MyToken","Hi, this message is from {Source}, again {Source} to {Destination}, again {Destination}");
        tokensMap.put("token1", "{token2}");
        tokensMap.put("token2", "{token3}");
        tokensMap.put("token3", "{token4}");
        tokensMap.put("token4", "This is Token 4");
        */
        //String freeFormString = "{  token1  }. {token10}. {  xpath:\\emloyee\\empId} The messages originated from {Source} to {Destination} are intercepted. MyToken = {MyToken}";
        boolean found = true;
        while(found) {
            found = false;
            Matcher tokenMatcher = tokenPattern.matcher(freeFormString);
            StringBuffer sb = new StringBuffer();
            StringBuffer tokenValueSb = new StringBuffer();
            while (tokenMatcher.find()) {
                //System.out.println("Group count = " + tokenMatcher.groupCount());
                String group = tokenMatcher.group();
                Matcher tokenValueMatcher = tokenValuePattern.matcher(group);
                if(tokenValueMatcher.find()) {
                    String myToken = tokenValueMatcher.group();
                    if(myToken.startsWith("log.token.")){
                        myToken = myToken.substring("log.token.".length());
                    }
                    String tokenValue = getTokenValue(myToken, msgEx, tokensMap);
                    if(tokenValue == null) {
                        tokenValue = group;
                    } else {
                        found = true;
                    }
                    tokenMatcher.appendReplacement(sb, tokenValue.replaceAll("\\$", "\\\\\\$"));
//System.out.println("Token = [" + myToken +"]");
                }
                
            }
            tokenMatcher.appendTail(sb);
            freeFormString = sb.toString();
        }
        return freeFormString;
        //System.out.println(freeFormString);
    }
    
    public static String getTokenValue(String token, MessageExchange msgEx, Map<String, String> tokensMap) {
        String retValue = null;
        if(token.startsWith("log.token.")){
            token = token.substring("log.token.".length());
        }
        if(preBuiltTokensMap.containsKey(token.toLowerCase())){
            Method method = preBuiltTokensMap.get(token.toLowerCase());
            try {
                if(method.getDeclaringClass().getName().endsWith("MessageExchangeHelper")) {
                    retValue = (String) method.invoke(method.getClass(), msgEx);
                } else if(method.getDeclaringClass().getName().endsWith("MessageExchange")) {    //MessageExchnage
                    retValue = "" + method.invoke(msgEx);
                }
            } catch (IllegalAccessException ex) {
                Logger.getLogger(TokenHelper.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(TokenHelper.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(TokenHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
       } else if(tokensMap.containsKey(token)){
           retValue = tokensMap.get(token);
       } else if(token.startsWith("xpath:")){
            token = token.substring("xpath:".length());
            Source src = MessageExchangeHelper.getMessageSource(msgEx);
            try {
                retValue = XPathEvaluator.evaluateDocument((DOMSource) src, token);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(LogImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if(token.startsWith("property:")){
            token = token.substring("property:".length());
            String key;
            for (Iterator iter = msgEx.getPropertyNames().iterator(); iter.hasNext(); ){
                key = ((String) iter.next());
                if(key.toLowerCase().contains(token.toLowerCase())) {
                    retValue = (String) msgEx.getProperty(key);
                    break;
                }
            }
        }
        return retValue;
    }

    public static void prepareBuiltInTokens() {
        Class helperClass = MessageExchangeHelper.class;
        try {
            preBuiltTokensMap.put("role", MessageExchangeHelper.class.getMethod("getRole", MessageExchange.class));
        
            preBuiltTokensMap.put("endpoint.name", MessageExchangeHelper.class.getMethod("getEndpointName", MessageExchange.class));
            preBuiltTokensMap.put("endpointname", MessageExchangeHelper.class.getMethod("getEndpointName", MessageExchange.class));

            preBuiltTokensMap.put("message.type", MessageExchangeHelper.class.getMethod("getActiveMessageReference", MessageExchange.class));
            preBuiltTokensMap.put("messagetype", MessageExchangeHelper.class.getMethod("getActiveMessageReference", MessageExchange.class));
            
            preBuiltTokensMap.put("exchange.id", MessageExchange.class.getMethod("getExchangeId"));
            preBuiltTokensMap.put("exchangeid", MessageExchange.class.getMethod("getExchangeId"));

            preBuiltTokensMap.put("endpoint", MessageExchange.class.getMethod("getEndpoint"));
            preBuiltTokensMap.put("service", MessageExchange.class.getMethod("getService"));
            preBuiltTokensMap.put("operataion", MessageExchange.class.getMethod("getOperation"));
            preBuiltTokensMap.put("pattern", MessageExchange.class.getMethod("getPattern"));
            preBuiltTokensMap.put("status", MessageExchange.class.getMethod("getStatus"));
            preBuiltTokensMap.put("error", MessageExchange.class.getMethod("getError"));
            preBuiltTokensMap.put("fault", MessageExchange.class.getMethod("getFault"));

            preBuiltTokensMap.put("interface.name", MessageExchange.class.getMethod("getInterfaceName"));
            preBuiltTokensMap.put("interfacename", MessageExchange.class.getMethod("getInterfaceName"));
            
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(TokenHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(TokenHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
