/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LogImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.aspects.log;

import com.sun.jbi.interceptors.Intercept;
import org.glassfish.openesb.aspects.common.AspectImpl;
import org.glassfish.openesb.aspects.common.MessageExchangeHelper;
import org.glassfish.openesb.aspects.common.XPathEvaluator;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.XMLFormatter;
import javax.jbi.messaging.MessageExchange;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPathExpressionException;

/**
 * @author rdara
 */
public class LogImpl extends AspectImpl implements Log {
//public class LogImpl implements Log {

    private boolean bHeader = false;
    private boolean bStatus = false;
    private boolean bMessage = false;
    private boolean bAll = false;
    private boolean bProperties = false;
    private String mLogHeader = "";
    private String[] mXpathExpressions;
    private Level mLevel;
    private Level mConsoleLevel;
    private boolean mConsole;
    private String mLoggerName = "";
    private ConsoleHandler mConsoleHandler = null;
    private static String msValuesStarter = "        ";
    private static String msSubHeadingStarter = "    ";
    private static String msStartIndicator = " --- START";
    private static String msEndIndicator = " --- END";
    private Map<String, String> userDefinedTokensMap;
    private List<String> freeFormStringsList = new ArrayList<String>();
    private String mContentSeparator = Log.LOG_NONE;

    public LogImpl(Properties prop) {
        setProperties(prop);
    }

    public LogImpl() {
    }

    @Intercept(type = "log")
    public boolean execute(MessageExchange msgEx) {
        boolean bRetValue = true;
        if (!isActive()) {
            return bRetValue;
        }

        String ls = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        addHeaderInfo(sb, true);
        sb.append(ls);
        if (bAll || bHeader) {
            addContentSeparator(sb, "Header", true);
            sb.append(msValuesStarter + "Exchange ID = " + msgEx.getExchangeId() + ";" + ls);
            sb.append(msValuesStarter + "Service End Point = " + msgEx.getEndpoint() + ";" + ls);
            sb.append(msValuesStarter + "Service = " + msgEx.getService() + ";" + ls);
            sb.append(msValuesStarter + "Operation = " + msgEx.getOperation() + ";" + ls);
            sb.append(msValuesStarter + "Pattern = " + msgEx.getPattern() + ";" + ls);
            sb.append(msValuesStarter + "Interface Name = " + msgEx.getInterfaceName() + ";" + ls);
            sb.append(msValuesStarter + "Role = " + MessageExchangeHelper.getRole(msgEx) + ";" + ls);
            sb.append(msValuesStarter + "Message Type = " + MessageExchangeHelper.getActiveMessageReference(msgEx) + ";" + ls);
            addContentSeparator(sb, "Header", false);
        }
        if (bAll || bProperties) {
            addContentSeparator(sb, "Properties", true);
            sb.append(getPropertiesAsSB(msgEx, msValuesStarter, ls));
            addContentSeparator(sb, "Properties", false);
        }
        if (bAll || bStatus) {
            addContentSeparator(sb, "Status", true);
            sb.append(msValuesStarter + "Status = " + msgEx.getStatus() + ";" + ls);
            sb.append(msValuesStarter + "Error = " + msgEx.getError() + ";" + ls);
            sb.append(msValuesStarter + "Fault = " + msgEx.getFault() + ";" + ls);
            addContentSeparator(sb, "Status", false);
        }
        if (bAll || bMessage) {
            addContentSeparator(sb, "Message", true);
            sb.append(MessageExchangeHelper.getMessageContent(msgEx) + ls);
            addContentSeparator(sb, "Message", false);
        }
        if (mXpathExpressions != null) {
            addContentSeparator(sb, "XPath", true);
            Source src = MessageExchangeHelper.getMessageSource(msgEx);
            for (Object obj : mXpathExpressions) {
                try {
                    sb.append(msValuesStarter + obj + " = " + XPathEvaluator.evaluateDocument((DOMSource) src, (String) obj) + ls);
                } catch (XPathExpressionException ex) {
                    Logger.getLogger(LogImpl.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            addContentSeparator(sb, "XPath", false);
        }
        if (!freeFormStringsList.isEmpty()) {
            addContentSeparator(sb, "Freeform logging:", true);
            for (int i = 0; i < freeFormStringsList.size(); i++) {
                sb.append(msValuesStarter + TokenHelper.parse(freeFormStringsList.get(i), msgEx, userDefinedTokensMap) + ls);
            }
            addContentSeparator(sb, "Freeform logging:", false);
        }
        addHeaderInfo(sb, false);
        //sb.append(mLogHeader + msEndIndicator);
        sb.append(ls);

        Logger logger = Logger.getLogger(getLoggerName());
        handleConsole(logger);  //Console property can change and hence handle it.
        logger.log(getLogLevel(), sb.toString());
        return bRetValue;
    }

    private void addContentSeparator(StringBuffer sb, String contentName, boolean isItStart) {
        String ls = System.getProperty("line.separator");
        if (mContentSeparator.equalsIgnoreCase(Log.LOG_STANDARD)) {
            sb.append(msSubHeadingStarter + contentName);
            sb.append(isItStart ? msStartIndicator : msEndIndicator);
            sb.append(ls);
        } else {
            if (isItStart) {
                sb.append(mContentSeparator);
            }
        }
    }

    private void addHeaderInfo(StringBuffer sb, boolean isItStart) {
        if (mLogHeader.trim().length() != 0) {
            sb.append(mLogHeader);  //msStartIndicator
        } else {
            if (mContentSeparator.equalsIgnoreCase(Log.LOG_STANDARD)) {
                sb.append("-> Log Aspect ----> Logging Info");
            }
        }
        if (!isItStart) {
            if (!mContentSeparator.equalsIgnoreCase(Log.LOG_STANDARD)) {
                sb.append(mContentSeparator);
            }
        }
    }

    public String getLoggerName() {
        return mLoggerName;
    }

    public void resetProperties() {
        bHeader = false;

        bStatus = false;
        bMessage = false;
        bProperties = false;
        bAll = false;
        mLogHeader = "";
        mXpathExpressions = null;
        mLevel = Level.INFO;
        mContentSeparator = "";
        mLoggerName = "";
        /* Remove all Handlers */
        /*
        if(getIdentity() != null || getIdentity().trim().length() > 0) {
        Logger logger = Logger.getLogger(getIdentity());
        for(Handler handler : logger.getHandlers()) {
        handler.flush();
        handler.close();
        logger.removeHandler(handler);
        }
        }
         */
        userDefinedTokensMap = null;
        freeFormStringsList = new ArrayList();
    }

    public void setConfiguration(String interceptorName, Properties prop) {
        resetProperties();
        //Properties prop = getProperties();

        //mLogHeader = prop.getProperty(Log.LOG_HEADER, "-> Log Aspect ----> Logging Info");
        mLoggerName = prop.getProperty(Log.LOGGER_NAME, getIdentity());
        mLogHeader = prop.getProperty(Log.LOG_HEADER, "");
        String logDetail = prop.getProperty(Log.LOG_CONTENT, "Status");
        String[] logDetails = logDetail.split(",");
        //String str;
        //for(int i = 0; i < logDetails.length; i++){
        //str = logDetails[i].trim();
        for (String str : logDetails) {
            str = str.trim();
            if (str.equalsIgnoreCase("header")) {
                bHeader = true;
            } else if (str.equalsIgnoreCase("status")) {
                bStatus = true;
            } else if (str.equalsIgnoreCase("message")) {
                bMessage = true;
            } else if (str.equalsIgnoreCase("all")) {
                bAll = true;
            } else if (str.equalsIgnoreCase("properties")) {
                bProperties = true;
            } else {
                freeFormStringsList.add(str);
            }
        }

        if (isDefinedProperty(prop, Log.LOG_XPATHS)) {
            String xpathExpression = prop.getProperty(Log.LOG_XPATHS, "");
            mXpathExpressions = xpathExpression.split(",");
            for (int j = 0; j < mXpathExpressions.length; j++) {
                mXpathExpressions[j] = mXpathExpressions[j].trim();
            }
        }

        if (isDefinedProperty(prop, Log.LOG_CONTENT_SEPARATOR)) {
            mContentSeparator = prop.getProperty(Log.LOG_CONTENT_SEPARATOR, "");
            if (!mContentSeparator.equalsIgnoreCase(Log.LOG_STANDARD)) {
                if (mContentSeparator.equalsIgnoreCase(Log.LOG_NONE)) {
                    mContentSeparator = "";
                }
                mContentSeparator += System.getProperty("line.separator");
            }
        } else {
            mContentSeparator = System.getProperty("line.separator");
        }
        mLevel = Level.parse(prop.getProperty(Log.LOG_LEVEL, "INFO"));
        mConsoleLevel = Level.parse(prop.getProperty(Log.LOG_CONSOLE_LEVEL, "INFO"));
        /* Handle handlers..*/
        prepareLogHandlers(prop);
        userDefinedTokensMap = TokenHelper.getuserDefinedTokens(prop);

    }

    /*
    log.config.dir = C:/Temp
    logconfig..files = Log-1.txt
    log.config.uris = file:///C:/Temp/Log-1-URI.txt
    log.config.patterns =%t Log-2.txt  //http://java.sun.com/j2se/1.4.2/docs/api/java/util/logging/FileHandler.html
    log.console = false
    log.config.file.limit = 0
    log.config.file.count = 1
    log.config.file.append = true
    log.config.file.formatter = simple | xml
     */
    private void prepareLogHandlers(Properties prop) {
        int limit = new Integer(prop.getProperty(Log.LOG_CONFIG_FILE_LIMIT, "0")).intValue();
        int count = new Integer(prop.getProperty(Log.LOG_CONFIG_FILE_COUNT, "1")).intValue();
        boolean append = new Boolean(prop.getProperty(Log.LOG_CONFIG_FILE_APPEND, "true")).booleanValue();

        String formatterValue = prop.getProperty(Log.LOG_CONFIG_FILE_FORMATTER, "simple");
        Formatter formatter;
        if (formatterValue.equalsIgnoreCase("xml")) {
            formatter = new XMLFormatter();
        } else {
            formatter = new SimpleFormatter();
        }

        List patternsList = new ArrayList<String>();

        //Create handlers
        // Get all the files. And if the directory specified, add the directory to the file
        if (isDefinedProperty(prop, Log.LOG_CONFIG_FILES)) {
            String[] logFileNames = prop.getProperty(Log.LOG_CONFIG_FILES).split(",");
            if (isDefinedProperty(prop, Log.LOG_CONFIG_DIR)) {
                String logDirName = prop.getProperty(Log.LOG_CONFIG_DIR);
                File logDirFile = new File(logDirName);
                if (!logDirFile.exists()) {
                    logDirFile.mkdirs();
                }
                for (String logFileName : logFileNames) {
                    File file = new File(logDirName, logFileName);
                    patternsList.add(file.getAbsolutePath());
                }
            } else {
                for (String logFileName : logFileNames) {
                    File file = new File(logFileName);
                    createMissingDirectories(file);
                    patternsList.add(file.getAbsolutePath());
                }
            }
        }
        //Process Patterns
        if (isDefinedProperty(prop, Log.LOG_CONFIG_PATTERNS)) {
            String[] patterns = prop.getProperty(Log.LOG_CONFIG_PATTERNS).split(",");
            for (String pattern : patterns) {
                patternsList.add(pattern);
            }
        }
        //Process URIs
        if (isDefinedProperty(prop, Log.LOG_CONFIG_URIS)) {
            String[] uriStrings = prop.getProperty(Log.LOG_CONFIG_URIS).split(",");
            for (String uriString : uriStrings) {
                try {
                    URI uri = new URI(uriString);
                    if (uri.getScheme() != null && uri.getScheme().equalsIgnoreCase("file")) {
                        File file = new File(uri);
                        createMissingDirectories(file);
                        patternsList.add(file.getAbsolutePath());
                    } else {
                        Logger.getLogger(LogImpl.class.getName()).log(Level.WARNING,
                                "The " + uri.getScheme() + " scheme is not supported. Ignoring the specified URI");
                    }
                } catch (URISyntaxException ex) {
                    Logger.getLogger(LogImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        /*
         * If user didn't specify the uris/patterns, then use the default.
         * The defualt is already handles by Fuji Logging Manager and will be the parent logger for all the loggers.
         * Using the parent handler provides the default behaviour.
         */
        List handlersList = new ArrayList<Handler>();
        Logger logger = Logger.getLogger(getLoggerName());
        if (patternsList.isEmpty()) { //User didn't opt for any output
            logger.setUseParentHandlers(true);     //Use the parent handlers.
        } else {
            logger.setUseParentHandlers(false);
            for (Object pattern : patternsList) {
                FileHandler fileHandler;
                try {
                    fileHandler = new FileHandler((String) pattern, limit, count, append);
                    fileHandler.setFormatter(formatter);
                    fileHandler.setLevel(Level.parse(prop.getProperty(Log.LOG_CONFIG_FILE_LEVEL, "ALL")));
                    handlersList.add(fileHandler);
                } catch (IOException ex) {
                    Logger.getLogger(LogImpl.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(LogImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            logger.setUseParentHandlers(false);
            for (Object handler : handlersList) {
                logger.addHandler((Handler) handler);
            }
        }
        logger.setLevel(Level.parse(prop.getProperty(Log.LOG_LEVEL, "INFO")));

        setConsole(prop, handlersList.isEmpty());
    }

    private void createMissingDirectories(File file) {
        String dirName = file.getParent();
        if (dirName != null) {   //Check for existance of the directory
            File logDirFile = new File(dirName);
            if (!logDirFile.exists()) {
                logDirFile.mkdirs();
            }
        }
    }

    @Override
    public String getAspectType() {
        return Log.LOG_ASPECT_TYPE;
    }

    public Properties getSupportedProperties() {
        Properties prop = new Properties();
        for (Log.LogProperties e : Log.LogProperties.values()) {
            prop.put(e, "*");
        }
        return prop;
    }

    private static StringBuffer getPropertiesAsSB(MessageExchange msgEx, String columnSpacer, String lineSpacer) {
        StringBuffer retSB = new StringBuffer();
        String key;
        for (Iterator iter = msgEx.getPropertyNames().iterator(); iter.hasNext();) {
            key = (String) iter.next();
            retSB.append(columnSpacer + key + " = " + msgEx.getProperty(key) + ";" + lineSpacer);
        }
        return retSB;
    }

    private Level getLogLevel() {
        return mLevel;
    }

    private Level getConsoleLevel() {
        return mConsoleLevel;
    }

    private boolean getConsole() {
        return mConsole;
    }

    private void setConsole(Properties prop, boolean emptyHandlers) {
        if (isDefinedProperty(prop, Log.LOG_CONSOLE)) {
            mConsole = new Boolean(prop.getProperty(Log.LOG_CONSOLE)).booleanValue();
        } else {
            //mConsole = emptyHandlers ? true : false; //Default logging will be done, if no handlers are specified.
        }
    }

    private void handleConsole(Logger logger) {
        ConsoleHandler consoleHandler = null;

        for (Handler handler : logger.getHandlers()) {   //Assumed that there will be only one console handler
            if (handler instanceof ConsoleHandler) {
                consoleHandler = (ConsoleHandler) handler;
                break;
            }
        }
        if (getConsole() == false) { //No consolehandler is desired
            if (consoleHandler != null) {
                logger.removeHandler(consoleHandler);
            }
        } else {    //Consolehandler is needed
            if (consoleHandler == null) {
                consoleHandler = new ConsoleHandler();
                logger.addHandler(consoleHandler);
            }
            consoleHandler.setLevel(getConsoleLevel());
        }
    }

    public void destroy() {
        Logger logger = Logger.getLogger(getLoggerName());
        for (Handler handler : logger.getHandlers()) {
            handler.flush();
            handler.close();
            logger.removeHandler(handler);
        }
    }

    public static boolean isDefinedProperty(Properties prop, String keyString) {
        boolean bDefined = false;
        if (prop.containsKey(keyString)) {
            String value = prop.getProperty(keyString);
            if (value != null && value.trim().length() > 0) {
                bDefined = true;
            }
        }
        return bDefined;
    }
}

