/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.log;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.glassfish.openesb.aspects.api.Aspect;

/**
 *
 * @author rdara
 */
public interface Log extends Aspect {
    String LOG_ASPECT_TYPE = "log";
    String LOG_LEVEL = "log.level";
    String LOG_CONSOLE = "log.console";
    String LOG_CONSOLE_LEVEL = "log.console.level";
    String LOG_ENTRY_LEVEL = "log.entry.level"; // SEVERE (highest value)| WARNING|INFO|CONFIG|FINE|FINER|FINEST (lowest value) |OFF|ALL
    String LOG_HEADER = "log.header";
    String LOGGER_NAME = "log.logger_name";
    String LOG_CONTENT = "log.content";
    String LOG_CONTENT_SEPARATOR = "log.content.separator";
    String LOG_XPATHS = "log.xpaths";
    
    String LOG_CONFIG_DIR =  "log.config.dir";
    String LOG_CONFIG_FILES = "log.config.files";
    String LOG_CONFIG_PATTERNS = "log.config.patterns";
    String LOG_CONFIG_URIS = "log.config.uris";
    String LOG_CONFIG_FILE_LIMIT = "log.config.file.limit";
    String LOG_CONFIG_FILE_COUNT = "log.config.file.count";
    String LOG_CONFIG_FILE_APPEND = "log.config.file.append";
    String LOG_CONFIG_FILE_FORMATTER = "log.config.file.formatter"; //simple | xml
    String LOG_CONFIG_FILE_LEVEL = "log.config.file.level"; 
    
    String LOG_STANDARD = "Standard";
    String LOG_NONE = "None";
    

    enum LogProperties {
      LOG_HEADER {
            public String toString() {
                return "log.header";
            }
      },
      LOG_DETAILS,
      xpath,
      LOG_ENTRY_LEVEL,
      LOG_CONFIG_DIR,
      LOG_CONFIG_FILES,
      LOG_CONFIG_PATTERNS,
      LOG_CONIFG_URIS,
      LOG_CONSOLE,
      LOG_CONFIG_FILE_LIMIT,
      LOG_CONFIG_FILE_COUNT,
      LOG_CONFIG_FILE_APPEND,
      LOG_CONFIG_FILE_FORMATTER,
      LOG_CONFIG_FILE_LEVEL;

      static Map<String, LogProperties> mLookup = new HashMap<String, LogProperties>();

      static {
          for(LogProperties s : EnumSet.allOf(LogProperties.class)) {
               mLookup.put(s.toString().toLowerCase(), s);
          }
      }
      
      public static LogProperties get(String propName) {
          return mLookup.get(propName.toLowerCase());
      }
   };
    
    enum LOG_DETAILS {
        header,
        status,
        message,
        all
    };
    
    enum LogFilehandlerFormatter {
        simple,
        xml
    };
    enum LOG_LEVEL {
        SEVERE, //(highest value)
        WARNING,
        INFO,
        CONFIG,
        FINE,
        FINER,
        FINEST, // (lowest value) 
        OFF,
        ALL
    }    
}
