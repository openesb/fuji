/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CacheImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.aspects.policy;

import com.sun.jbi.interceptors.Intercept;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.Certificate;
import java.security.CodeSource;
import java.security.Principal;
import java.security.ProtectionDomain;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.jbi.messaging.MessagingException;
import org.glassfish.openesb.aspects.common.AspectImpl;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import org.glassfish.openesb.aspects.common.MessageExchangeHelper;
import org.glassfish.openesb.aspects.security.provider.PolicyFile;

/**
 * CacheImpl class is responsible for managing caching.
 * Its generic and deals with objects.
 * With the help of helper classes like MessageExchangeHelper, caching is achieved by exclusively dealing with those
 *     message formats and the protocol.
 * @author rdara
 */
public class PolicyImpl extends AspectImpl implements Policy {

    List<PolicyFile> policyFiles = Collections.synchronizedList(new LinkedList<PolicyFile>());

    /**
     * Constructor
     */
    public PolicyImpl() {
    }

    public PolicyImpl(Properties prop) {
        setProperties(prop);
        init(prop);
    }

    @Intercept(type="policy")
    public boolean execute(MessageExchange exchange) {
        boolean bRetValue = true;
        boolean isPermitted = false;
        
        if(policyFiles.isEmpty()) {
            //TODO: What if no policy file has been specified?
            //Technically, no policy file means, there is no one who got granted permission and hence every message fails.
            //This as well mean bring down the entire component and makes no sense.
            //Hence, if no policy file means, treat it as by mistake or parsing errors and allow grant
            Logger.getLogger(PolicyImpl.class.getName()).log(Level.WARNING,"Policy Aspect: The policy is not in effect as no policy files are successfully pointed." );
            return true;
        }
        
        Principal[] principals = MessageExchangeHelper.getPrincipals(exchange);

        ProtectionDomain pd = getProtectionDomain(principals);

        PolicyAspectPermission policyAspectPermission = null;
        String role = MessageExchangeHelper.getRole(exchange);
        String endPointName = MessageExchangeHelper.getEndpointName(exchange);
        if (role.equals(MessageExchangeHelper.CONSUMER)) {
            policyAspectPermission = new ConsumePermission(endPointName);
            Logger.getLogger(PolicyImpl.class.getName()).log(Level.INFO,"Policy Aspect: Authorizing Consume permission for - " + policyAspectPermission.getName());            
        } else if (role.equals(MessageExchangeHelper.PROVIDER)) {
            policyAspectPermission = new ProvidePermission(endPointName);
            Logger.getLogger(PolicyImpl.class.getName()).log(Level.INFO,"Policy Aspect: Authorizing Provide permission for - " + policyAspectPermission.getName());            
            
        } else {
            //Not Supported;
        }
        for (PolicyFile policyFile : policyFiles) {
            isPermitted |= policyFile.implies(pd, policyAspectPermission);
        }
        if (isPermitted == false) {  //Permission Denied
            Logger.getLogger(PolicyImpl.class.getName()).log(Level.WARNING,"Policy Aspect: Permission Denied.");
            Exception e;
            if (role.equals(MessageExchangeHelper.CONSUMER)) {
                e = new ConsumePermissionException(endPointName);
            } else if (role.equals(MessageExchangeHelper.PROVIDER)) {
                e = new ProvidePermissionException(endPointName);
            } else {
                e = new Exception("PolicyAspectPermission");
            }
            bRetValue = false;
            exchange.setError(e);
            try {
                exchange.setStatus(ExchangeStatus.ERROR);
            } catch (MessagingException ex) {
                Logger.getLogger(PolicyImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Logger.getLogger(PolicyImpl.class.getName()).log(Level.WARNING,"Policy Aspect: Permission Granted.");
        }
        return bRetValue;
    }

    private void init(Properties prop) {
        //Process URIs
        if (isDefinedProperty(prop, Policy.POLICY_OCNFIG_URIS)) {
            String[] uriStrings = prop.getProperty(Policy.POLICY_OCNFIG_URIS).split(",");
            for (String uriString : uriStrings) {
                try {
                    URI uri = new URI(uriString);
                    PolicyFile policyFile = new PolicyFile(uri.toURL());
                    policyFiles.add(policyFile);
                } catch (MalformedURLException ex) {
                    Logger.getLogger(PolicyImpl.class.getName()).log(Level.SEVERE, null, ex);
                } catch (URISyntaxException ex) {
                    Logger.getLogger(PolicyImpl.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(PolicyImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private ProtectionDomain getProtectionDomain(Principal[] principals) {
        //Generate ProtectionDomain
        Certificate[] certs = new Certificate[0];
        CodeSource myCS = new CodeSource(null, certs);
        //ProtectionDomain pd1 = new ProtectionDomain(myCS, pf.getPermissions(myCS), PolicyImpl.class.getClassLoader(), principals);
        ProtectionDomain pd = new ProtectionDomain(myCS, null, PolicyImpl.class.getClassLoader(), principals);
        return pd;
    }

    private void resetProperties() {
    }

    @Override
    public void setConfiguration(String interceptorName, Properties prop) {
        resetProperties();
    }

    @Override
    public String getAspectType() {
        return Policy.POLICY_ASPECT_TYPE;
    }

    public void destroy() {
        //TODO: Release the resources
    }

    public Properties getSupportedProperties() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public static boolean isDefinedProperty(Properties prop, String keyString) {
        boolean bDefined = false;
        if (prop.containsKey(keyString)) {
            String value = prop.getProperty(keyString);
            if (value != null && value.trim().length() > 0) {
                bDefined = true;
            }
        }
        return bDefined;
    }
}
