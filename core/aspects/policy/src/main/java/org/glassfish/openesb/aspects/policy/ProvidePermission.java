/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.policy;

/**
 *
 * @author rdara
 */
public class ProvidePermission extends PolicyAspectPermission {
    public ProvidePermission() {
        super("");
    }

    public ProvidePermission(String endPointName) {
        super(endPointName);
    }

    // Don't care about actions 
    public ProvidePermission(String endPointName, String actions) {
        super(endPointName);
    }
    
    public boolean isSameClass(Object obj) {
        return (obj instanceof ConsumePermission);
    }
}
