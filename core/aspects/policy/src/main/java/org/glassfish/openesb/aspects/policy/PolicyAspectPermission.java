/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.aspects.policy;

import java.io.Serializable;
import java.security.Permission;

/**
 *
 * @author rdara
 */
public abstract class PolicyAspectPermission extends Permission implements Serializable {

    public PolicyAspectPermission() {
        super("");
    }

    public PolicyAspectPermission(String endPointName) {
        super(endPointName);
    }

    // Don't care about actions 
    public PolicyAspectPermission(String endPointName, String actions) {
        super(endPointName);
    }

    @Override
    public boolean implies(Permission permission) {
        if (!isSameClass(permission)) {
            return false;
        }
        return areEqual(permission.getName(), getName());
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(!isSameClass(obj)){
            return false;
        }
        return areEqual(getName(), ((PolicyAspectPermission)obj).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public String getActions() {
        StringBuilder sb = new StringBuilder();
        return sb.toString();
    }

    public abstract boolean isSameClass(Object obj);
    //??TODO: The areEqual method can't handle if both the strings have "*" wild cards with them.
    // However, areEqual sufficiently handles, if either of them is having a wild card.
    private static boolean areEqual(String name1, String name2) {
        boolean bRetValue = false;
        if ((name1 == null) && (name2 == null)) {
            bRetValue = true;
        } else {
            if ((name1 ==  null) || (name2 == null)) {
                bRetValue = false;
            } else {
                bRetValue = areEqualWithStarWildCard(name1, name2);
                if (bRetValue == false) {
                    bRetValue = areEqualWithStarWildCard(name1, name2);
                }
            }
        }
        return bRetValue;
    }

    private static boolean areEqualWithStarWildCard(String name1, String name2) {
        boolean bRetValue = true;
        String[] pieces = name2.split("\\*");
        int index;
        for (String piece : pieces) {
            index = name1.indexOf(piece);
            if (index == -1) {
                bRetValue = false;
                break;
            }
            name1 = name1.substring(index + piece.length());
        }
        return bRetValue;
    }
}
