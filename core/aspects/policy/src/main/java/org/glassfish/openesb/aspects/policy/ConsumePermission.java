/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.policy;

import java.security.Permission;

/**
 *
 * @author rdara
 */
public class ConsumePermission extends PolicyAspectPermission {
    public ConsumePermission() {
        super("");
    }

    public ConsumePermission(String endPointName) {
        super(endPointName);
    }

    // Don't care about actions 
    public ConsumePermission(String endPointName, String actions) {
        super(endPointName);
    }
    
    public boolean isSameClass(Object obj) {
        return (obj instanceof ConsumePermission);
    }
}
