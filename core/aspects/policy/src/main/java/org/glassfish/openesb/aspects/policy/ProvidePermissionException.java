/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.policy;

/**
 *
 * @author rdara
 */
public class ProvidePermissionException extends Exception {
    public ProvidePermissionException(String exception){
        super(exception);
    }
}
