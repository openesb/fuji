/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.policy;

import org.glassfish.openesb.aspects.api.Aspect;

/**
 *
 * @author rdara
 */
public interface Policy extends Aspect {
    String POLICY_ASPECT_TYPE = "policy";
    
    String POLICY_OCNFIG_URIS = "policy.config.uris";
}
