/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.policy;

/**
 *
 * @author rdara
 */
public class ConsumePermissionException extends Exception {
    public ConsumePermissionException(String exception) {
        super(exception);
    }
}
