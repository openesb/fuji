/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FIFOCache.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.aspects.cache;

import com.sun.jbi.interceptors.Intercept;
import java.util.LinkedList;
import java.util.Properties;
import javax.jbi.messaging.MessageExchange;

/**
 * FIFO is one of the cache scheme, which removes the first cached item if the cache reaches maximum size.
 * @author rdara
 */
public class FIFOCache extends CacheImpl {

    /**
     * A Tracker of the cached items. Its a linked list best suites for the FIFO/LIFO. 
     */
    LinkedList<Object> cachedItemsList;

    /**
     * Constructor. 
     */
    public FIFOCache() {
        super();
        cachedItemsList = new LinkedList<Object>();
    }

    public FIFOCache(Properties prop) {
        super(prop);
        cachedItemsList = new LinkedList<Object>();
    }

    /**
     * Removes the first cached item
     */
    public void removeCachedItem() {
        Object removableItem = getCachedItemsList().getFirst();
        getCache().remove(removableItem);
        getCachedItemsList().removeFirst();
    }

    /**
     * Keeps track of order of the in messages/keys in the linked list.
     * @param inMessage
     */
    public void cacheKeeping(Object inMessage) {
        cachedItemsList.add(inMessage);
    }

    /**
     * Gets the list of the keys/in messages of the cached items
     * @return
     */
    protected LinkedList<Object> getCachedItemsList() {
        return cachedItemsList;
    }

    /**
     * Resets all the data structes. All the cache and tracking data gone.
     */
    public void resetCache() {
        super.resetCache();
        cachedItemsList = new LinkedList<Object>();
    }

    public Properties getSupportedProperties() {
        return new Properties();
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Intercept(type="cache")
    public boolean execute(MessageExchange exchange) {
        return cache(exchange);
    }

}
