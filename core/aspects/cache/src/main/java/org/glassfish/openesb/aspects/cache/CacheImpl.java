/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CacheImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package org.glassfish.openesb.aspects.cache;


import org.glassfish.openesb.aspects.common.AspectImpl;
import org.glassfish.openesb.aspects.common.MessageExchangeHelper;
import java.util.Hashtable;
import java.util.Properties;
import javax.jbi.messaging.MessageExchange;

/**
 * CacheImpl class is responsible for managing caching.
 * Its generic and deals with objects.
 * With the help of helper classes like MessageExchangeHelper, caching is achieved by exclusively dealing with those
 *     message formats and the protocol.
 * @author rdara
 */
public abstract class CacheImpl extends AspectImpl implements Cache {

    int DEFAULT_MAX_CACHE_SIZE = 64;
    /** 
     * The actual cache - mapping IN messages to OUT messages
     */
    private Hashtable cachedItemsWithReplies;
    /**
     * Its a buffer to keep the IN messages waiting for the replies. 
     * Mapping Message Exchange IDs or any IDs with the IN messages
     */
    private Hashtable cachedItemsWithExchangeIDs;
    /**
     * The current size of the cache
     */
    private int cacheSize = 0;
    /**
     * Maximum size of the cache
     */
    private int maxCacheSize = DEFAULT_MAX_CACHE_SIZE;

    /**
     * Constructor
     */
    public CacheImpl() {
        resetCache();
    }

    public CacheImpl(Properties prop) {
        resetCache();
        setProperties(prop);
    }

    /**
     * @return The cache itself.
     */
    protected Hashtable getCache() {
        return cachedItemsWithReplies;
    }

    /**
     * Indicates whether the particular object is cached or not 
     * @param key Key of the incoming message
     * @return true or flase
     */
    public boolean isCached(Object key) {
        return cachedItemsWithReplies.containsKey(key);
    }

    /**
     * Retrieves the cached value of the given key.
     * @param key
     * @return Cached values - out message
     */
    public Object getCachedMessage(Object key) {
        return cachedItemsWithReplies.get(key);
    }

    /**
     * Bufferes the incoming message with a ID which can later identifies the out message that needs to be cached.
     * @param messageExchangeID ID that any IN message can be corelated with the later available out message.
     * @param key The key that uniquely identifies the incoming message - it can be entire incoming message also.
     */
    public void addToBuffer(Object messageExchangeID, Object key) {
        cachedItemsWithExchangeIDs.put(messageExchangeID, key);
    }

    /**
     * Caches buffered in message and supplied out message pair.
     * @param messageExchangeID ID by which the corresponding buffered in message/key can be obtained
     * @param outMessage    The Out message that would need to be paired with In message in the cache.
     */
    public void addToCache(Object messageExchangeID, Object outMessage) {
        if (cacheSize > maxCacheSize) {
            System.out.println("Maximum Cached Reached. Removing cached item...");
            removeCachedItem();
            cacheSize--;
        }
        Object inMessage = cachedItemsWithExchangeIDs.get(messageExchangeID);
        cachedItemsWithReplies.put(inMessage, outMessage);
        cachedItemsWithExchangeIDs.remove(messageExchangeID);
        cacheKeeping(inMessage);
        cacheSize++;
    }

    /**
     * Similar to HouseKepping so as that FIFO, MRU and other caching schemes can be implemented by the correposnding derived classes.
     * @param inMessage
     */
    public void cacheKeeping(Object inMessage) {
    }

    /**
     * Derived caching schemes like FIFO, MRU need to remove a cached item if the cache is full.
     */
    public void removeCachedItem() {
    }

    public abstract boolean execute(MessageExchange exchange);
    /**
     * MessageExchange aware caching through this method.
     * With the help of MessageExchangeHelper, this method knows protocol and structre of MessageExchange.
     * @param exchange  MessageExchnage object
     * @return returns Either null or MessageExchange. Null if the the cached item is found.
     
    public MessageExchange cache(MessageExchange exchange) {
        String activeMessageRef = MessageExchangeHelper.getActiveMessageReference(exchange);

        if (!MessageExchangeHelper.isInOutPattern(exchange) || activeMessageRef == null) {
            return exchange;
        }
        if (activeMessageRef.equals(MessageExchangeHelper.FAULT)) {
            // What to do if its a fault message?
            // Pass on to the components?
            return exchange;
        }
        String messageExchangeID = exchange.getExchangeId();
        System.out.println("messageExchangeID = " + messageExchangeID);

        if (activeMessageRef.equals(MessageExchangeHelper.IN)) {
            String keyString = MessageExchangeHelper.getKey(exchange);
            if (isCached(keyString)) {
                System.out.println("Sending the cached item....");
                MessageExchangeHelper.setMessage(exchange, getCachedMessage(keyString), MessageExchangeHelper.OUT);
                return null;
            } else {
                System.out.println("Key not found and the ExchangeID got buffered...");
                addToBuffer(messageExchangeID, keyString);
            }
        } else if (activeMessageRef.equals(MessageExchangeHelper.OUT)) {
            if (cachedItemsWithExchangeIDs.containsKey(messageExchangeID)) {
                System.out.println("Caching...");
                addToCache(messageExchangeID, exchange.getMessage(MessageExchangeHelper.getActiveMessageReference(exchange)));
            }
        }
        return exchange;
    }
    */
    /**
     * MessageExchange aware caching through this method.
     * With the help of MessageExchangeHelper, this method knows protocol and structre of MessageExchange.
     * @param exchange  MessageExchnage object
     * @return returns Either null or MessageExchange. Null if the the cached item is found.
     */
    public boolean cache(MessageExchange exchange) {
        boolean bRetValue = true;
        String activeMessageRef = MessageExchangeHelper.getActiveMessageReference(exchange);
        
        if (!MessageExchangeHelper.isInOutPattern(exchange) || activeMessageRef == null) {
            return bRetValue;
        }
        if (activeMessageRef.equals(MessageExchangeHelper.FAULT)) {
            // What to do if its a fault message?
            // Pass on to the components?
            return bRetValue;
        }
        String messageExchangeID = exchange.getExchangeId();
        System.out.println("messageExchangeID = " + messageExchangeID);

        if (activeMessageRef.equals(MessageExchangeHelper.IN)) {
            String keyString = MessageExchangeHelper.getKey(exchange);
            if (isCached(keyString)) {
                System.out.println("Sending the cached item....");
                MessageExchangeHelper.setMessage(exchange, getCachedMessage(keyString), MessageExchangeHelper.OUT);
                //MessageExchangeHelper.setDoneStatus(exchange);
                bRetValue = false;
            } else {
                System.out.println("Key not found and the ExchangeID got buffered...");
                addToBuffer(messageExchangeID, keyString);
            }
        } else if (activeMessageRef.equals(MessageExchangeHelper.OUT)) {
            if (cachedItemsWithExchangeIDs.containsKey(messageExchangeID)) {
                System.out.println("Caching...");
                addToCache(messageExchangeID, exchange.getMessage(MessageExchangeHelper.getActiveMessageReference(exchange)));
            }
        }
        return bRetValue;
    }
    
    //Properties
    /**
     * Sets the maximum cache size.
     * @param size  The required maximum sixe of the cache.
     */
    public void setMaxCacheSize(int size) {
        if (size > 1) {
            maxCacheSize = size;
        } else {
            System.out.println("The cache size should be positive.");
        }
    }

    /**
     * Obtains the maximum cache size. 
     * @return maximum cache size
     */
    public int getMaxCahcheSize() {
        return maxCacheSize;
    }

    /**
     * Resets all the cache. All cached items are gone.
     */
    public void resetCache() {
        cachedItemsWithReplies = new Hashtable();
        cachedItemsWithExchangeIDs = new Hashtable();
        cacheSize = 0;
    }

    public void resetProperties() {
        maxCacheSize = DEFAULT_MAX_CACHE_SIZE;
    }

    @Override
    public void setConfiguration(String interceptorName, Properties prop) {
        resetProperties();
        String maxSizeString = prop.getProperty(Cache.CACHE_MAX_SIZE, new Integer(DEFAULT_MAX_CACHE_SIZE).toString());
        setMaxCacheSize(new Integer(maxSizeString).intValue());
    }
    
    @Override
    public String getAspectType(){
        return Cache.CACHE_ASPECT_TYPE;
    }
    
    public void destroy(){
        //TODO: Release the resources
    }
}
