/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.cache;

import javax.jbi.messaging.MessageExchange;
import org.glassfish.openesb.aspects.api.Aspect;

/**
 *
 * @author rdara
 */
public interface Cache extends Aspect {
    boolean cache(MessageExchange exchange);
    
    String CACHE_MAX_SIZE = "cache.max.size";
    String CACHE_ASPECT_TYPE = "cache";
}
