/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.composer;

import org.glassfish.openesb.aspects.api.Aspect;
import org.glassfish.openesb.aspects.api.AspectsFramework;
import org.glassfish.openesb.aspects.framework.FrameworkActivator;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Properties;
import org.w3c.dom.Document;

/**
 *
 * @author rdara
 */
public class DocumentProcessor {
    public static void createAspects(Document doc) {
        AspectsFramework aspectsFramework = FrameworkActivator.getAspectsFramework();
        if(aspectsFramework != null) {
            Map mapCreatableAspects = DocumentProcessHelper.getCreatableAspectsMap(doc);
            for(Object obj : mapCreatableAspects.values()) {
                aspectsFramework.create((Properties) obj);
            }
        }
    }

    public static void controlAspects(Document doc, boolean bDestroy) {
        AspectsFramework aspectsFramework = FrameworkActivator.getAspectsFramework();
        if(aspectsFramework != null) {
            Map mapControlAspects = DocumentProcessHelper.getAspectsControlsMap(doc);
            for(Object obj : mapControlAspects.keySet()) {
                if(bDestroy) {
                    if(((String) mapControlAspects.get(obj)).equalsIgnoreCase("destroy")) {
                        aspectsFramework.destroy((String)obj);
                    }
                }
                if(((String) mapControlAspects.get(obj)).equalsIgnoreCase("start")) {
                    aspectsFramework.start((String)obj);
                } else if(((String) mapControlAspects.get(obj)).equalsIgnoreCase("stop")) {
                    aspectsFramework.stop((String)obj);
                } else {
                    //??
                }            }
        }
    }

    public static void updateAspectProperties(Document doc) {
        AspectsFramework aspectsFramework = FrameworkActivator.getAspectsFramework();
        if(aspectsFramework != null) {
            Map mapChangedAspects = DocumentProcessHelper.getChangedPropertiesAspectsMap(doc, "aspects-update");
            Properties newProp;
            Properties oldProp;
            for(Object obj : mapChangedAspects.keySet()) {
                newProp = (Properties) mapChangedAspects.get((String) obj);
                oldProp = aspectsFramework.getProperties((String) obj);
                for(Object propKey : newProp.keySet()) {
                    if(oldProp.containsKey((String) propKey)) {
                        oldProp.put(propKey, oldProp.get(propKey) + "," + newProp.get(propKey));
                    } else {
                        oldProp.put(propKey, newProp.get(propKey));
                    }
                }
System.out.println("All in all Properties for " + obj + ": " + oldProp);                                
                aspectsFramework.setProperties((String) obj, oldProp);
            }
            mapChangedAspects = DocumentProcessHelper.getChangedPropertiesAspectsMap(doc, "aspects-replace");
            for(Object obj : mapChangedAspects.keySet()) {
                newProp = (Properties) mapChangedAspects.get((String) obj);
                newProp.put(Aspect.ASPECT_IDENTITY, (String) obj);
                aspectsFramework.setProperties((String) obj, newProp);
            }
        }
    }
    public static void processAspectsConfiguration(String url) {
        //Document doc = DocumentProcessHelper.getDocument(new File("C:\\temp\\config.xml"));
        Document doc = DocumentProcessHelper.getDocument(new File(url));
        controlAspects(doc, true);  //Destroy the aspects first to pave the way for creation
        createAspects(doc);
        updateAspectProperties(doc);
        controlAspects(doc, false); //Already destroyed. Don't destory now.
System.out.println("Obtaining Interceptors");        
        Map interceptorsMap =  DocumentProcessHelper.getInterceptorsMap(doc);
System.out.println("Registering Interceptors");        
        FrameworkActivator.registerInterceptors(interceptorsMap);
    }

}
