/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeHelper.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.aspects.common;

import java.io.StringWriter;
import java.net.URI;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchange.Role;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.security.auth.Subject;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;

/**
 *  Helper class which is aware of the structure and protocol of MessageExchange
 * @author rdara
 */
public class MessageExchangeHelper {

    public static final String IN = "in";
    public static final String OUT = "out";
    public static final String FAULT = "fault";
    public static final String IN_OUT = "in-out";
    public static final String CONSUMER = "Consumer";
    public static final String PROVIDER = "Provider";

    /**
     * 
     * @param exchange
     * @return
     */
    public static String getActiveMessageReference(MessageExchange exchange) {
        String activeMessageRef = null;
        ExchangeStatus currState = exchange.getStatus();
        if (!(ExchangeStatus.DONE == currState || ExchangeStatus.ERROR == currState)) {
            if (exchange.getMessage(OUT) != null) {
                activeMessageRef = OUT;
            } else if (exchange.getMessage(FAULT) != null) {
                activeMessageRef = FAULT;
            } else if (exchange.getMessage(IN) != null) {
                activeMessageRef = IN;
            }
        }
        return activeMessageRef;
    }

    /**
     * Denotes whether the message exchange is for wsdl in-out or not.
     * @param exchange
     * @return
     */
    public static boolean isInOutPattern(MessageExchange exchange) {
        boolean bRetValue = false;
        URI uri = exchange.getPattern();
        String path = uri.getPath();
        if (path.contains(IN_OUT)) {
            bRetValue = true;
        }
        return bRetValue;
    }

    public static String getKey(Source source) {
        return getMessageContent(source);
    }

    public static Source getMessageSource(MessageExchange exchange) {
        NormalizedMessage msg = exchange.getMessage(getActiveMessageReference(exchange));
        return msg != null ? msg.getContent() : null;
    }

    public static Principal[] getPrincipals(MessageExchange exchange) {
        Principal[] retValue = new Principal[0];
        String activeMessageRef = getActiveMessageReference(exchange);
        if (activeMessageRef != null) {
            NormalizedMessage msg = exchange.getMessage(getActiveMessageReference(exchange));
            Subject subject = msg.getSecuritySubject();
            if (subject != null) {
                retValue = msg.getSecuritySubject().getPrincipals().toArray(new Principal[0]);
            }
        }
        //return subject == null ? new Principal[0] : msg.getSecuritySubject().getPrincipals().toArray(new Principal[0]);
        return retValue;

    }

    /**
     * Obtains key from the source.
     * @param source
     * @return Key String
     */
    public static String getMessageContent(Source source) {
        String keyString = null;
        if (source != null) {
            StringWriter stringWriter = new StringWriter();
            if (source != null) {
                Result result = new StreamResult(stringWriter);
                // Write the DOM document to the file Get Transformer
                try {
                    Transformer xformer = TransformerFactory.newInstance().newTransformer();
                    /* Write to a file */
                    xformer.transform(source, result);
                } catch (TransformerConfigurationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (TransformerFactoryConfigurationError e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (TransformerException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            keyString = stringWriter.toString();
        }
        return keyString;
    }

    public static String getMessageContent(MessageExchange exchange) {
        return getKey(exchange);
    }

    /**
     * Obtains key from the MessageExchange
     * @param exchange
     * @return Key String
     */
    public static String getKey(MessageExchange exchange) {
        return getKey(exchange.getMessage(getActiveMessageReference(exchange)));
    }

    /**
     * Obtians key from the NormalizedMessage
     * @param normalizedMessage
     * @return key string
     */
    public static String getKey(NormalizedMessage normalizedMessage) {
        return normalizedMessage != null ? getKey(normalizedMessage.getContent()) : null;
    }

    /**
     * Sets the IN or OUT message within the MessageExchange.
     * @param exchange
     * @param normalizedMessage
     * @param inOrOut
     */
    public static void setMessage(MessageExchange exchange, Object normalizedMessage, String inOrOut) {
        try {
            exchange.setMessage((NormalizedMessage) normalizedMessage, inOrOut);
        } catch (MessagingException ex) {
            Logger.getLogger(MessageExchangeHelper.class.getName()).log(Level.SEVERE, "Error setting up the message", ex);
        }
    }

    public static void setDoneStatus(MessageExchange exchange) {
        try {
            exchange.setStatus(ExchangeStatus.DONE);
        } catch (MessagingException ex) {
            Logger.getLogger(MessageExchangeHelper.class.getName()).log(Level.SEVERE, "Error setting the message exchange status", ex);
        }
    }

    public static String getRole(MessageExchange msgEx) {
        String roleString = "";
        if (msgEx.getRole().equals(Role.CONSUMER)) {
            roleString = CONSUMER;
        } else if (msgEx.getRole().equals(Role.PROVIDER)) {
            roleString = PROVIDER;
        } else {
            roleString = msgEx.getRole().toString();
        }
        return roleString;
    }

    public static String getExchangeID(MessageExchange msgEx) {
        return msgEx.getExchangeId();
    }

    public static String getEndpointName(MessageExchange msgEx) {
        return msgEx.getEndpoint().getEndpointName();
    }
    /*
    public Properties getExchangeProperties(MessageExchange exchange)
    throws Exception
    {
    Properties exProps = new Properties();
    
    String msg =  ( getActiveMessageReference(exchange) == null ? "null" : getActiveMessageReference(exchange));
    exProps.setProperty(FilterProperty.MESSAGE.toString(), msg);
    
    // -- If this is the Consumer Exchange, then the endpoint returned is the
    //    endpoint link, we want the actual endpoint, which we can get
    //    from the Proxy by calling getActualEndpoint()
    if ( exchange instanceof MessageExchangeProxy )
    {
    MessageExchangeProxy proxy = (MessageExchangeProxy) exchange;
    ServiceEndpoint ep = proxy.getActualEndpoint(); 
    
    if ( ep != null )
    {
    exProps.setProperty(FilterProperty.SERVICE.toString(), 
    ep.getServiceName().toString());
    exProps.setProperty(FilterProperty.ENDPOINT.toString(), 
    ep.getEndpointName());
    
    }
    
    exProps.setProperty(FilterProperty.CONSUMER.toString(), 
    proxy.getSourceComponent());
    exProps.setProperty(FilterProperty.PROVIDER.toString(), 
    proxy.getTargetComponent());
    
    ServiceEndpoint epLink = proxy.getEndpointLink();
    
    if ( epLink != null )
    {
    exProps.setProperty(FilterProperty.ENDPOINT_LINK.toString(),
    epLink.getEndpointName());
    exProps.setProperty(FilterProperty.SERVICE_CONNECTION.toString(),
    epLink.getServiceName().toString());
    }
    exProps.setProperty(FilterProperty.STATUS.toString(), 
    proxy.getStatus().toString());
    
    }
    return exProps;
    }
     */
}
