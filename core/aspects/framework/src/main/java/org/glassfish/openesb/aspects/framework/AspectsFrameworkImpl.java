/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.aspects.framework;

import org.glassfish.openesb.aspects.api.Aspect;
import org.glassfish.openesb.aspects.api.AspectFactory;
import org.glassfish.openesb.aspects.api.AspectsFramework;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.MessageExchange;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author rdara
 */
public class AspectsFrameworkImpl implements AspectsFramework {
    
    private static Logger logger_ = Logger.getLogger(AspectsFrameworkImpl.class.getName());
    
    public AspectsFrameworkImpl() {
        
    }
    
    public Aspect create(Properties prop) {
        if(!prop.containsKey(Aspect.ASPECT_IDENTITY)) {
            logger_.log(Level.WARNING,"Aspect identity must be specified. Aspect create failed.");
            return null;    //??TODO Throw exception.
        }
        String identity = prop.getProperty(Aspect.ASPECT_IDENTITY);
        if(AspectsFrameworkHelper.isAspectDefined(identity)){
            logger_.log(Level.WARNING,"Aspect " + identity + " is already defined. Aspect creation failed. Destroy the aspect before creating again.");
            return null;        //??TODO Throw exception.
        } 

        String strAspectType = prop.getProperty(Aspect.ASPECT_TYPE);
        return create(strAspectType, prop);
    }
    public Aspect create(String aspectType, Properties prop) {
        String identity = prop.getProperty(Aspect.ASPECT_IDENTITY);
        AspectFactory aspectFactory = getService(aspectType);
        Aspect aspect = null;
        if(aspectFactory != null){
            aspect = aspectFactory.create(prop); 
            AspectsFrameworkHelper.addToAspectsActiveQueue(aspectType, prop, aspect);
        } else {    //Aspect service is not up. Queue the request
            AspectsFrameworkHelper.addToAspectsWaitQueue(aspectType, prop);
        }
        return aspect;
    }
/*   
    static AspectFactory getService(String aspectType) {
        AspectFactory aspectFactory = null;
        BundleContext bc = FrameworkActivator.getBundleContext();
        //ServiceReference sr = null;
        if (bc != null) {
            String filter = "(" + Aspect.ASPECT_TYPE + "=" + aspectType + ")";
            try {
                ServiceReference[] sr = bc.getServiceReferences(AspectFactory.class.getName(), filter);
                //sr = bc.getServiceReference(AspectsFrameworkHelper.getServiceClassName(aspectType));
                if(sr != null && sr.length >= 1) {
                    aspectFactory = (AspectFactory) bc.getService(sr[0]);   //TODO: Avoid duplicate services
                }
            } catch (InvalidSyntaxException ex) {
                Logger.getLogger(AspectsFrameworkImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            //sr = bc.getServiceReference(AspectsFrameworkHelper.getServiceClassName(aspectType));
        }
        return aspectFactory;
    }
*/
    static AspectFactory getService(String aspectType) {
        AspectFactory retAspectFactory = null;
        BundleContext bc = FrameworkActivator.getBundleContext();
        if (bc != null) {
            try {
                ServiceReference[] srArray = bc.getServiceReferences(AspectFactory.class.getName(), null);
                if(srArray != null && srArray.length >= 1) {
                    for(ServiceReference sr : srArray) {
                        AspectFactory aspectFactory = (AspectFactory) bc.getService(sr);
                        if(aspectType.equalsIgnoreCase(aspectFactory.getAspectType())) {
                            retAspectFactory = aspectFactory;
                            break;
                        }
                    }
                }
            } catch (InvalidSyntaxException ex) {
                Logger.getLogger(AspectsFrameworkImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retAspectFactory;
    }
    
    public Map getAspects() {   
        return AspectsFrameworkHelper.getAllAspects();  //TODO: Reference or cloned copy??? Make it read-only.
    }

    public Aspect getAspect(String identity) {
        return AspectsFrameworkHelper.getAllAspects().get(identity);
    }

    public  void start(String identity) {
        if(!AspectsFrameworkHelper.isAspectDefined(identity)){
            System.out.println("Aspect " + identity + " is not defined. Can't start.");            
            
        } else {
            System.out.println("Aspect " + identity + " is starting...");
            AspectsFrameworkHelper.start(identity);
        }
    }

    public void stop(String identity) {
        if(!AspectsFrameworkHelper.isAspectDefined(identity)){
            System.out.println("Aspect " + identity + " is not defined. Can't stop.");            
            
        } else {
            System.out.println("Aspect " + identity + " is stopping...");
            AspectsFrameworkHelper.stop(identity);
        }
    }

    public boolean isActive(String identity) {
        Aspect aspect = getAspect(identity);
        return aspect != null ? aspect.isActive() : false;
    }

    public void setProperties(String identity, Properties prop) {
        if(!AspectsFrameworkHelper.isAspectDefined(identity)){
            System.out.println("Aspect " + identity + " is not defined.Can't set properties.");            
        } else {
            AspectsFrameworkHelper.setProperties(identity, prop);
        }
    }

    public  Properties getProperties(String identity) {
        return AspectsFrameworkHelper.getProperties(identity);
    }

    public  boolean execute(String identity, MessageExchange msgEx) {
        boolean bRetValue = true;
        if(msgEx != null)   {//Cache may make this as null!
            Aspect aspect = getAspect(identity);
            if(aspect != null) {
                bRetValue = aspect.execute(msgEx);
            }
        }
        return bRetValue;
    }
    
    public void destroy(String identity) {
        if(!AspectsFrameworkHelper.isAspectDefined(identity)){
            System.out.println("Aspect " + identity + " is not defined. Can't destroy.");            
            
        } else {
            System.out.println("Aspect " + identity + " is destroying...");
            AspectsFrameworkHelper.destroy(identity);
        }
    }
    
    public void createAspectInterceptor(Properties prop) {
        String ref = prop.getProperty("name");
        if(ref == null || ref.trim().length() == 0) {
            ref = java.util.UUID.randomUUID().toString();  //??TODO: Interceptor name is created by interceptor framework. Feedback mechanism is needed. 
        }
        Aspect aspect = create(prop);
        AspectsFrameworkHelper.addAspectsToInterceptor(ref, prop.getProperty(Aspect.ASPECT_IDENTITY));
        FrameworkActivator.registerInterceptor(ref, prop);
        logger_.log(Level.INFO, "Registered Interceptor: " + prop.getProperty("name", ""));
    } 
    
    public void destroyAspectInterceptor(Properties prop) {
        AspectsFrameworkHelper.destroy(prop.getProperty(Aspect.ASPECT_IDENTITY));
        AspectsFrameworkHelper.destroyInterceptor(prop.getProperty("name"));
    }
}
