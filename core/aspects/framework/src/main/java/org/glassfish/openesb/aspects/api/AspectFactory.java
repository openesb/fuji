/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.api;

import java.util.Properties;

/**
 *
 * @author rdara
 */
public interface AspectFactory {
    public Aspect create(Properties prop);
    public String getAspectType();
}
