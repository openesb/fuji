/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.composer;

import org.glassfish.openesb.aspects.api.Aspect;
import org.glassfish.openesb.aspects.framework.AspectsFrameworkHelper;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author rdara
 */
public class DocumentProcessHelper {

    //static Map<Integer, List> mapOfExecutableAspects = new Hashtable();
    
    public static Method getMethod() {
       String className = "org.glassfish.openesb.aspects.composer.Composer";
       String methodName = "execute";
       Method retMethod = null;
        try {            
            Class c = Class.forName(className);
            Class parameterClass = Class.forName("javax.jbi.messaging.MessageExchange");
            retMethod = c.getMethod(methodName, new Class[]{parameterClass});
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentProcessHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(DocumentProcessHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(DocumentProcessHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retMethod;
    }

    public static List getAspectsExecutionList(Element interceptorNode) {
        List retList = new ArrayList();
        NodeList nodeList = interceptorNode.getElementsByTagName("aspects-execute");
        Map indexToIdentityMap = new Hashtable();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node curAspectsExecuteNode = nodeList.item(i);
            if(curAspectsExecuteNode.getNodeType() == Node.ELEMENT_NODE) {
                NodeList aspectNodes = ((Element) curAspectsExecuteNode).getElementsByTagName("aspect");
                for (int l = 0; l < aspectNodes.getLength(); l++) {
                    NamedNodeMap nodeMap = aspectNodes.item(l).getAttributes();
                    indexToIdentityMap.put(new Integer(nodeMap.getNamedItem("order").getNodeValue()), 
                                            nodeMap.getNamedItem(Aspect.ASPECT_IDENTITY).getNodeValue());
                }
            }
        }
        
        while(!indexToIdentityMap.isEmpty()) {      //TODO: Insane sorting. Needed a better way
            int iLeaseValue = Integer.MAX_VALUE;
            int curValue;
            Integer curInteger = null;
            for(Object obj : indexToIdentityMap.keySet()) {
                curValue = ((Integer) obj).intValue();
                if(iLeaseValue > curValue) {
                    iLeaseValue = curValue;
                    curInteger = (Integer) obj;
                }
            }
            if(curInteger != null) {
                retList.add(indexToIdentityMap.get(curInteger));
                indexToIdentityMap.remove(curInteger);
            }
        }
        return retList;
    }
    
    public static Properties getProperties(Element elem) {
        Properties properties = new Properties();
        NodeList propertyNodes = elem.getElementsByTagName("property");
        for (int i = 0; i < propertyNodes.getLength(); i++) {
            NamedNodeMap nodeMap = propertyNodes.item(i).getAttributes();
            properties.setProperty(nodeMap.getNamedItem("name").getNodeValue(), nodeMap.getNamedItem("value").getNodeValue());
        }
        return properties;
    }
    
    public static Document getDocument(File file) {
        Document retDoc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            //throw new RuntimeException(e);
            System.out.println("Failed to read - " + file.getAbsolutePath() + " -- " + file.getName());
            return retDoc;
        }
        try {
            retDoc = documentBuilder.parse(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retDoc;
    }

    public static Map getInterceptorsMap(Document doc) {
        Map mapOfInterceptorProperties = new Hashtable();        
        NodeList interceptorNodesList = doc.getElementsByTagName("interceptor");
        for (int i = 0; i < interceptorNodesList.getLength(); i++) {
            Node curInterceptorNode = interceptorNodesList.item(i);
            Properties properties = new Properties();
            if(curInterceptorNode.getNodeType() == Node.ELEMENT_NODE) {
                Integer curInteger = new Integer(i);
                mapOfInterceptorProperties.put(curInteger.toString(), 
                                               DocumentProcessHelper.getProperties((Element) curInterceptorNode));
                AspectsFrameworkHelper.addAspectsToInterceptor(curInteger.toString(), 
                                                               DocumentProcessHelper.getAspectsExecutionList((Element) curInterceptorNode));
                /*                                               
                mapOfExecutableAspects.put(curInteger, 
                                           DocumentProcessHelper.getAspectsExecutionList((Element) curInterceptorNode));
                */
            }
        }
        return mapOfInterceptorProperties;
    }
    
    public static Map getCreatableAspectsMap(Document doc) {
        Map<String, Properties> creatableAspectsMap = new Hashtable();

        NodeList aspectsCreateNodesList = doc.getElementsByTagName("aspects-create");
        for (int i = 0; i < aspectsCreateNodesList.getLength(); i++) {
            Node curAspectCreatesNode = aspectsCreateNodesList.item(i);
            NodeList aspectsNodeList = curAspectCreatesNode.getChildNodes();
            Node curAspectNode;
            NamedNodeMap nodeMap;
            String type = null;
            for(int j = 0; j < aspectsNodeList.getLength(); j++) {
                curAspectNode = aspectsNodeList.item(j);
                Properties properties = new Properties();
                if(curAspectNode.getNodeType() == Node.ELEMENT_NODE) {
                    //Get tye AspectType
                    nodeMap = curAspectNode.getAttributes();
                    if(nodeMap.getNamedItem("type") != null) {
                        type = nodeMap.getNamedItem("type").getNodeValue();
                    } else {
                        //TODO:  EXCEPTION HANDLING
                    }
                    properties = getProperties((Element) curAspectNode);
                    properties.putAll(getNonPropertyNodeValue((Element) curAspectNode));
                    properties.put(Aspect.ASPECT_TYPE, getAspectType(type));
                    creatableAspectsMap.put((String) properties.get(Aspect.ASPECT_IDENTITY), properties);
                }
            }
        }
       
        return creatableAspectsMap;
    }

    /**
     * 
     * @param doc
     * @param nodeType aspects-update or aspects-replace
     * @return
     */
    public static Map getChangedPropertiesAspectsMap(Document doc, String nodeType) {
        Map<String, Properties> changedPropertiesAspectsMap = new Hashtable();

        NodeList aspectsCreateNodesList = doc.getElementsByTagName(nodeType);
        for (int i = 0; i < aspectsCreateNodesList.getLength(); i++) {
            Node curAspectCreatesNode = aspectsCreateNodesList.item(i);
            NodeList aspectsNodeList = curAspectCreatesNode.getChildNodes();
            Node curAspectNode;
            NamedNodeMap nodeMap;
            String identity = null;
            for(int j = 0; j < aspectsNodeList.getLength(); j++) {
                curAspectNode = aspectsNodeList.item(j);
                Properties properties = new Properties();
                if(curAspectNode.getNodeType() == Node.ELEMENT_NODE) {
                    //Get tye AspectType
                    nodeMap = curAspectNode.getAttributes();
                    if(nodeMap.getNamedItem(Aspect.ASPECT_IDENTITY) != null) {
                        identity = nodeMap.getNamedItem(Aspect.ASPECT_IDENTITY).getNodeValue();
                    } else {
                        //TODO:  EXCEPTION HANDLING
                    }
                    properties = getProperties((Element) curAspectNode);
                    properties.putAll(getNonPropertyNodeValue((Element) curAspectNode));
                    changedPropertiesAspectsMap.put(identity, properties);
                }
            }
        }
       
        return changedPropertiesAspectsMap;
    }

    public static Map getAspectsControlsMap(Document doc) {
        Map<String, String> controlAspectsMap = new Hashtable();

        NodeList aspectsControlNodesList = doc.getElementsByTagName("aspects-control");
        NamedNodeMap nodeMap;
        String identity = null;
        String action = null;
        Node curAspect;
        for (int i = 0; i < aspectsControlNodesList.getLength(); i++) {
            Node curAspectControlNode = aspectsControlNodesList.item(i);
            NodeList aspectsNodes = curAspectControlNode.getChildNodes();
            for(int j = 0; j < aspectsNodes.getLength(); j++) {
                curAspect = aspectsNodes.item(j);
                if(curAspect.getNodeType() == Node.ELEMENT_NODE){
                    nodeMap = curAspect.getAttributes();
                    if(nodeMap.getNamedItem(Aspect.ASPECT_IDENTITY) != null) {
                        identity = nodeMap.getNamedItem(Aspect.ASPECT_IDENTITY).getNodeValue();
                    } else {
                        //TODO:  EXCEPTION HANDLING
                    }
                    if(nodeMap.getNamedItem("action") != null) {
                        action= nodeMap.getNamedItem("action").getNodeValue();
                        controlAspectsMap.put(identity, action);
                    } else {
                        //OK, no action. Maitain status quo and do nothing.
                    }
                }
            }
        }
        return controlAspectsMap;
    }
    
    public static Properties getNonPropertyNodeValue(Element elem) {
        Properties properties = new Properties();
        NodeList childNodes = elem.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node curNode = childNodes.item(i);
            String valueString = "";
            if(curNode.getNodeType() == Node.ELEMENT_NODE) {
                if(!curNode.getNodeName().equalsIgnoreCase("property")){
                    NodeList childValueNodes = curNode.getChildNodes();
                    boolean bFirstTime = true;
                    for(int j = 0; j < childValueNodes.getLength(); j++) {
                        Node childValueNode = childValueNodes.item(j);
                        if(childValueNode.getNodeType() == Node.ELEMENT_NODE) {
                            if(!bFirstTime)  //Not for the first
                            valueString += ",";    
                            valueString += childValueNode.getFirstChild().getNodeValue();
                            bFirstTime = false;
                        }
                    }
                    properties.setProperty(curNode.getNodeName(), valueString);
                }
            }
        }
        return properties;
    }
    
    
    public static String getAspectType(String str) {
        String retAspectType = "UNKNOWN";
        if(str.equalsIgnoreCase("log")){
            retAspectType = "log";
        } else if(str.equalsIgnoreCase("cache")){
            retAspectType = "cache";
        }
        return retAspectType;
    }
    
    /*
    public static List getAspectsExecutablesList(Integer iReference) {
        return mapOfExecutableAspects.get(iReference);
    }
    */
}
