/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.glassfish.openesb.aspects.framework;

import com.sun.jbi.interceptors.Interceptor;
import org.glassfish.openesb.aspects.api.AspectsFramework;
import org.glassfish.openesb.aspects.composer.Composer;
import org.glassfish.openesb.aspects.composer.DocumentProcessHelper;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.glassfish.openesb.aspects.api.AspectFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.SynchronousBundleListener;
import org.osgi.util.tracker.ServiceTracker;


/**
 *
 * @author rdara
 */
public class FrameworkActivator implements BundleActivator, SynchronousBundleListener {

    private ServiceRegistration registration;
    static BundleContext msContext;
    static ServiceTracker aspectsFactoryTracker;
    private static Map<String, ServiceRegistration> msRegisteredInterceptors = new Hashtable<String, ServiceRegistration>();

    /**
     * 
     */
    public FrameworkActivator() {
        // TODO Auto-generated constructor stub
    }

    /** 
     * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
     */
    static AspectsFramework aspectsFactory = null;
    //@Override
    public void start(BundleContext context) throws Exception {
        msContext = context;
        aspectsFactory = new AspectsFrameworkImpl();
        Properties properties = new Properties();

        context.addBundleListener(this);
        
        registration = msContext.registerService(AspectsFramework.class.getName(),
                aspectsFactory,
                properties);
        
        aspectsFactoryTracker = new ServiceTracker(msContext, AspectFactory.class.getName(), new FrameworkServiceTrackerCustomizer());
        aspectsFactoryTracker.open();
        
   }
    
   public static AspectsFramework getAspectsFramework() {
       return aspectsFactory;
   }

    /**
     * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    //@Override
    public void stop(BundleContext context) throws Exception {
        registration.unregister();
        aspectsFactoryTracker.close();
        for(ServiceRegistration serReg : msRegisteredInterceptors.values()) {
            serReg.unregister();
        }
    }

    public static BundleContext getBundleContext() {
        return msContext;
     }

    public static void unregisterInterceptor(String interceptorName){
        if(msRegisteredInterceptors.containsKey(interceptorName)) {
            ServiceRegistration serReg = msRegisteredInterceptors.get(interceptorName);
            if(serReg != null) {
                serReg.unregister();
            }
            msRegisteredInterceptors.remove(interceptorName);
        }
    }
     public static void registerInterceptor(String reference, Properties prop) {
        Composer composer = new Composer(reference);
        Method m = DocumentProcessHelper.getMethod();
        prop.put("method", m);
        ServiceRegistration serReg = msContext.registerService(Interceptor.class.getName(),
                                                                composer,
                                                                prop);
        msRegisteredInterceptors.put(reference, serReg);
     }
     public static void registerInterceptors(Map interceptorsMap) {
        for(Object obj : interceptorsMap.keySet()){
            registerInterceptor((String) obj, (Properties) interceptorsMap.get(obj));
        }
     }
     
     
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
    }

    public void bundleChanged(BundleEvent event) {
        switch (event.getType()) {
            case BundleEvent.INSTALLED :
                break;
            case BundleEvent.UNINSTALLED :
                break;
            case BundleEvent.STARTED :
                startBundle(event.getBundle());
                break;
            case BundleEvent.STOPPED :
                stopBundle(event.getBundle());
                break;
            case BundleEvent.UPDATED :
                break;
        }            
    }
    void startBundle(Bundle bundle) {
        List<Properties> propList = AspectsBundleHelper.getListOfProperties(bundle);
        AspectsFramework aspectsFramework = getAspectsFramework();
        for(Properties prop : propList) {
            //aspectsFramework.createAspectInterceptor(prop);
        }
    }
    void stopBundle(Bundle bundle) {
        AspectsFramework aspectsFramework = getAspectsFramework();
        List<Properties> propList = AspectsBundleHelper.getListOfProperties(bundle);
        for(Properties prop : propList) {
            aspectsFramework.destroyAspectInterceptor(prop);
        }
    }
}
