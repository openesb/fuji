/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.api;

import java.util.Map;
import java.util.Properties;
import javax.jbi.messaging.MessageExchange;

/**
 *
 * @author rdara
 */
public interface AspectsFramework {
    Aspect create(String aspectType, Properties prop);
    Aspect create(Properties prop);

    void createAspectInterceptor(Properties prop);   // The properties need to have aspect as well interceptor properties
    void destroyAspectInterceptor(Properties prop);
    
    //Aspect destroy(String identity);
    void destroy(String identity);
    
    Map getAspects();
    Aspect getAspect(String identity);
    
    void start(String identity);   // A stooped aspect can be started.
    void stop(String identity);    // An aspect can be stopped, which makes it inactive

    
    boolean isActive(String identity); //Indicates the state of the aspect. Active or Inactive
    
    void setProperties(String identity, Properties prop);    //Sets the properties of the object
    Properties getProperties(String identity);             //Get the properties of the object
    
    boolean execute(String identity, MessageExchange msgEx); //Executge the object
}
