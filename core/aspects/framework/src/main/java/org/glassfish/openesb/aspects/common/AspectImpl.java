/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.common;

import org.glassfish.openesb.aspects.api.Aspect;
import java.util.Properties;
import javax.jbi.messaging.MessageExchange;

/**
 *
 * @author rdara
 */
public abstract class AspectImpl implements Aspect {

    boolean mbActive = true;
    String msIdentity = "";
    Properties mProperties = new Properties();
    String mAspectType;

    public AspectImpl() {
        
    }

    public AspectImpl(Properties prop) {
        setProperties(prop);
    }

    public void start() {
        mbActive = true;
        mProperties.setProperty(Aspect.ACTIVE_STATE, "True");
    }

    public void stop() {
        mbActive = false; 
        mProperties.setProperty(Aspect.ACTIVE_STATE, "False");
    }

    public void setIdentity(String sIdentity) {
        if(sIdentity != null)
        msIdentity = sIdentity;
    }

    public String getIdentity() {
        if(msIdentity == null || msIdentity.trim().length() == 0){
            msIdentity = java.util.UUID.randomUUID().toString();
        }
        return msIdentity;
    }

    public boolean isActive() {
        return mbActive;
    }

    public final void setProperties(Properties prop) {    
        mProperties.clear();
        String name = "";
        if(prop != null) {
            mProperties.putAll(prop);
            setIdentity(prop.getProperty(ASPECT_IDENTITY));
            mbActive = new Boolean(prop.getProperty(ACTIVE_STATE, "True")).booleanValue();
            name = prop.getProperty("name","");
        }
        //propertiesUpdated();
        setConfiguration(name, getProperties());
    }

    public Properties getProperties() {
        Properties prop = new Properties();
        prop.putAll(mProperties);
        return prop;
    }
    
    public abstract String getAspectType();
    public abstract boolean execute(MessageExchange msgEx);
    public abstract void destroy();
    public abstract void setConfiguration(String interceptorName, Properties prop);

}
