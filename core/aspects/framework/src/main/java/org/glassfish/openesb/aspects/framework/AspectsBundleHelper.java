/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.glassfish.openesb.aspects.framework;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.aspects.api.Aspect;
import org.osgi.framework.Bundle;

/**
 *
 * @author rdara
 */
public class AspectsBundleHelper {

    private static String ASPECTS_PROPERTIES_DIR = "META-INF/Aspects";
    private static String PROPERTIES_FILE_PATTERN = "*.properties";

    static List<Properties> getListOfProperties(Bundle bundle) {
        List<Properties> propertiesList = new ArrayList<Properties>();
        Properties prop = null;

        Enumeration eURLs = bundle.findEntries(ASPECTS_PROPERTIES_DIR, PROPERTIES_FILE_PATTERN, true);
        InputStream in;

        if(eURLs == null) {
            //Logger.getLogger(AspectsBundleHelper.class.getName()).log(Level.WARNING, "No properties file under config/aspects are defined.");
            //This is not an aspect bundle
            return propertiesList;
        }

        while (eURLs.hasMoreElements()) {
            try {
                in = bundle.getEntry(((URL) eURLs.nextElement()).getPath()).openStream();
                if (in != null) {
                    prop = new Properties();
                    prop.load(in);
                    if(!isDefinedProperty(prop, Aspect.ASPECT_IDENTITY)){
                        String uniqueIdentity = java.util.UUID.randomUUID().toString();
                        prop.put(Aspect.ASPECT_IDENTITY, uniqueIdentity);
                    }
                    propertiesList.add(prop);
                }
            } catch (IOException ex) {
                Logger.getLogger(AspectsBundleHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return propertiesList;
    }
    
    public static boolean isDefinedProperty(Properties prop, String keyString) {
        boolean bDefined = false;
        if(prop.containsKey(keyString)){
            String value = prop.getProperty(keyString);
            if(value != null && value.trim().length() > 0) {
                bDefined = true;
            }
        }
        return bDefined;
    }

}