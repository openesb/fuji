/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.composer;

import org.glassfish.openesb.aspects.api.AspectsFramework;
import org.glassfish.openesb.aspects.framework.AspectsFrameworkHelper;
import org.glassfish.openesb.aspects.framework.FrameworkActivator;
import javax.jbi.messaging.MessageExchange;
import com.sun.jbi.interceptors.Interceptor;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.openesb.aspects.api.Aspect;

/**
 *
 * @author rdara
 */
public class Composer implements Interceptor {

    String mReference;
    public Composer(Integer reference) {
        mReference = reference.toString();
    }
    public Composer(String interceptorName) {
        mReference = interceptorName;
    }

    //Interceptor method
    public boolean execute(MessageExchange exchange) {
        boolean bRetValue = true;
        AspectsFramework aspectsFramework = FrameworkActivator.getAspectsFramework();
        if(aspectsFramework != null) {
            //List aspectsExecutableList = DocumentProcessHelper.getAspectsExecutablesList(mReference);
            List<String> aspectsExecutableList = AspectsFrameworkHelper.getAspectsExecutablesList(mReference);
            if(aspectsExecutableList != null) {
                for(String str : aspectsExecutableList) {
                    bRetValue &= aspectsFramework.execute((String) str, exchange);
                }
            }
        }
        return bRetValue;
    }
    
    public void setConfiguration(String interceptorName, Properties properties){
        if(properties.containsKey(Aspect.ASPECT_IDENTITY)) {
            String asepctIdentity = properties.getProperty(Aspect.ASPECT_IDENTITY);
            AspectsFramework aspectsFramework = FrameworkActivator.getAspectsFramework();
            aspectsFramework.setProperties(asepctIdentity, properties);
        } else {
            //TODO: This is a defensive mechanism to support unidentifiable aspects 
            Logger.getLogger(Composer.class.getName()).log(Level.WARNING, "Aspect Identity is not defined and hence setConfiguration is being ignored");
        }
    }

    
}

