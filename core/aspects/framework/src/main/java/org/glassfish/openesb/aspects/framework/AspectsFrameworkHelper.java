/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.framework;

import org.glassfish.openesb.aspects.api.Aspect;
import org.glassfish.openesb.aspects.api.AspectsFramework;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rdara
 */
public class AspectsFrameworkHelper {

    private static volatile Map<String, Aspect> mAspectsHolder  = new Hashtable<String, Aspect>(); 

    private static volatile Map<String, Map<String, Aspect>> msAspectsActiveQueue = new Hashtable<String, Map<String, Aspect>>();

    /*
     * An Aspect Service may be running or not. Or running service might be stopped.
     * If the aspects service is not active, queue up all the requests for the creation of aspects
     * If the aspects service is stopped, then add all the aspects of that service to the queue
     * Whenever aspects service is started, create all the needed aspects
     * msAspectsWaitQueue creates linked list queues per aspect
     */
    private static volatile Map<String, List<Properties>> msAspectsWaitQueue = new Hashtable<String, List<Properties>>();    
    
    /**
     * Given an interceptor name, provides all the associated aspects to that interceptor
     */
    private static Map<String, List<String>> msMapOfInterceptorToAspects = new Hashtable<String, List<String>>();
    
    /**
     * Given the aspect identity, provide all the assoicated interceptors
     */
    private static Map<String, List<String>> msMapOfAspectsToInterceptors = new Hashtable<String, List<String>>();
    
    
    private static Logger logger_ = Logger.getLogger(AspectsFrameworkHelper.class.getPackage().getName());

    
    static void addToAspectsWaitQueue(String aspectType, Properties prop) {
        List<Properties> aspectsCreateQueue = msAspectsWaitQueue.get(aspectType);
        if(aspectsCreateQueue == null) {
            aspectsCreateQueue = Collections.synchronizedList(new LinkedList<Properties>());
            msAspectsWaitQueue.put(aspectType, aspectsCreateQueue);
        }
        synchronized(aspectsCreateQueue) {
            aspectsCreateQueue.add(prop);
        }
    }
    
    static void addToAspectsActiveQueue(String aspectType, Properties prop, Aspect aspect) {
        Map<String, Aspect> aspectsQueue = msAspectsActiveQueue.get(aspectType);
        if(aspectsQueue == null) {
            //aspectsQueue = Collections.synchronizedList(new LinkedList<Aspect>());
            aspectsQueue = new Hashtable<String, Aspect>();
            msAspectsActiveQueue.put(aspectType, aspectsQueue);
        }
        aspectsQueue.put(aspect.getIdentity(),aspect);
        synchronized(mAspectsHolder) {
            mAspectsHolder.put(aspect.getIdentity(), aspect);
        }
        logger_.log(Level.INFO, "Added Aspect: " + aspect.getIdentity());
    }
    
    static void addAspectsToWaitQueue(final String aspectType) {
        Map<String, Aspect> mapOfAspects = getAspectsActiveQueue(aspectType);
        Properties prop;
        if(mapOfAspects != null) {
            for(Aspect aspect : mapOfAspects.values()) {
                if(aspect != null) {
                    prop = aspect.getProperties();
                    addToAspectsWaitQueue(aspectType, prop);
                    logger_.log(Level.INFO, "Waitlisted Aspect: " + prop.getProperty(Aspect.ASPECT_IDENTITY));
                    mAspectsHolder.remove(prop.getProperty(Aspect.ASPECT_IDENTITY));
                    aspect.destroy();
                }
            }   
            mapOfAspects.clear();
        }
    }

    static List<Properties> getAspectsWaitQueue(String aspectType) {
        return msAspectsWaitQueue.get(aspectType);
    }
    
    static Map<String, Aspect> getAspectsActiveQueue(String aspectType) {
        return msAspectsActiveQueue.get(aspectType);
    }

    
    static Map<String,Aspect> getAllAspects() {
        return mAspectsHolder;
    }
    
    static Aspect getAspect(String identity) {
        return mAspectsHolder.get(identity);
    }
    
    //static void createAspectsFromWaitQueue(final String aspectType) {
    static void createAspectsFromWaitQueue(final String aspectType) {
        (new Thread(){
            public void run() {
                List<Properties> listOfWaitingAspects = getAspectsWaitQueue(aspectType);
                if(listOfWaitingAspects != null) {
                    AspectsFramework aspectsFramework = FrameworkActivator.getAspectsFramework();
                    synchronized(listOfWaitingAspects) {
                        for(Properties prop : listOfWaitingAspects) {
                            aspectsFramework.create(aspectType, prop);
                        }
                        listOfWaitingAspects.removeAll(listOfWaitingAspects);
                    }
                }
            }
        }).start();
    }
    
    static void destroy(String identity) {
        Aspect aspect = getAspect(identity);
        if(aspect != null) {
            logger_.log(Level.INFO,"Destroying Aspect: " + identity);
            destroy(aspect);
        } else { //Aspect might be in other queues...
            synchronized(msAspectsWaitQueue) {
                for(List<Properties> propList : msAspectsWaitQueue.values()) {
                    if(removeAspectProperties(propList, identity)) {
                        logger_.log(Level.INFO,"Destroying Waitlisted Aspect: " + identity);
                        break;
                    }
                }
            }
        }
        destroyAspectInterceptorAssociation(msMapOfAspectsToInterceptors, msMapOfInterceptorToAspects, identity);
        
    }

    static void destroy(Aspect aspect) {
        String aspectType = aspect.getAspectType();
        String identity = aspect.getIdentity();
        //removeAspectProperties(getAspectsActiveQueue(aspectType), (identity));
        Map<String, Aspect> aspectsActiveQueue = getAspectsActiveQueue(aspectType);
        aspectsActiveQueue.remove(aspect.getIdentity());
        mAspectsHolder.remove(identity);
        destroyAspectInterceptorAssociation(msMapOfAspectsToInterceptors, msMapOfInterceptorToAspects, identity);
        aspect = null;
    }

    private static void destroyAspectInterceptorAssociation(Map map1, Map map2, String map1Key){
        if(map1.containsKey(map1Key)){
            List map1List = (List) map1.get(map1Key);
            for(Object obj : map1List){
                if(map2.containsKey(obj)){
                    List map2List = (List) map2.get(obj);
                    map2List.remove(map1Key);
                    if(map2List.isEmpty()){
                        map2.remove(obj);
                    }
                }
            }
            map1.remove(map1Key);
        }
    }
    
    static void destroyInterceptor(String interceptorName) {
        destroyAspectInterceptorAssociation(msMapOfInterceptorToAspects, msMapOfAspectsToInterceptors, interceptorName);
        FrameworkActivator.unregisterInterceptor(interceptorName);
    }

    public  static void start(String identity) {
        Aspect aspect = getAspect(identity);
        if(aspect != null) {
            aspect.start();
        } else {
            Properties prop = getPropertiesReference(msAspectsWaitQueue, identity);
            if(prop != null) {
                prop.put(Aspect.ACTIVE_STATE, "True");
            }
        }
    }

    public static void stop(String identity) {
        Aspect aspect = getAspect(identity);
        if(aspect != null) {
            aspect.stop();
        } else {
            Properties prop = getPropertiesReference(msAspectsWaitQueue, identity);
            if(prop != null) {
                prop.put(Aspect.ACTIVE_STATE, "False");
            }
        }
    }
    
    static Properties getProperties(String identity) {
        Properties retProperties = null;
        Aspect aspect = getAspect(identity);
        if(aspect != null) {
            retProperties = aspect.getProperties();
        } else {    //Get from wait queue
            Properties prop = getPropertiesReference(msAspectsWaitQueue, identity);
            if(prop != null) {
                retProperties = new Properties();
                retProperties.putAll(prop);
            }
        }
        return retProperties;
    }

    static void setProperties(String identity, Properties prop) {
        Aspect aspect = getAspect(identity);
        if(aspect != null) {
            aspect.setProperties(prop);
        } else {    //Get from wait queue
            Properties retProperties = null;
            synchronized(msAspectsWaitQueue) {
                for(List<Properties> propList : msAspectsWaitQueue.values()) {
                    retProperties = getPropertiesReference(propList, identity);
                    if(retProperties != null) {    //Properties found.
                        retProperties.clear();
                        retProperties.putAll(prop);
                        break;
                    }
                }                
            }
        }
    }

    static Properties getPropertiesReference(Map<String, List<Properties>> aspectsQueues, String identity) {
        Properties retProperties = null;
        synchronized(aspectsQueues) {
            Properties prop = null;
            for(List<Properties> propList : aspectsQueues.values()) {
                prop = getPropertiesReference(propList, identity);
                if(prop != null) {    //Properties found.
                    retProperties = prop;
                    break;
                }
            }                
        }
        return retProperties;
    }

    static Properties getPropertiesReference(List<Properties> list, String identity) {
        Properties retProperties = null;
        if(list != null) {
            synchronized(list) {
                for(Properties prop : list) {
                    if(prop.getProperty(Aspect.ASPECT_IDENTITY).equals(identity)) {
                        retProperties = prop;
                        break;
                    }
                }
            }
        }
        return retProperties;
    }
    
    private static int getIndex(List list, String identity) {
        int iIndex = -1;
        if(list != null) {
            synchronized(list) {
                for(Object obj : list) {
                    Properties prop = (Properties) obj;
                    if(prop.getProperty(Aspect.ASPECT_IDENTITY).equals(identity)) {
                        iIndex = list.indexOf(obj);
                        break;
                    }
                }
            }
        }
        return iIndex;
    }

    private static boolean removeAspectProperties(List<Properties> list, String identity) {
        boolean bRemoved = false;
        int iIndex = getIndex(list, identity);
        if(iIndex >= 0) {
            list.remove(iIndex);
            bRemoved = true;
        }
        return bRemoved;
    }
    
    public static boolean isAspectDefined(String identity) {
        boolean bDefined = false;
        if(mAspectsHolder.containsKey(identity)) {
            bDefined = true;
        } else {
            synchronized(msAspectsWaitQueue) {
                for(List<Properties> propList : msAspectsWaitQueue.values()) {
                    if(propList != null) {
                        synchronized(propList) {
                            for(Properties prop : propList) {
                                if(prop.getProperty(Aspect.ASPECT_IDENTITY).equals(identity)) {
                                    bDefined = true;
                                    break;
                                }
                            }
                        }
                        if(bDefined) {
                            break;
                        }
                    }
                }
            }
        }
        return bDefined;
    }

    public static List<String> getAspectsExecutablesList(String referenceString) {
        return msMapOfInterceptorToAspects.get(referenceString);
    }

    public static void addAspectsToInterceptor(String referenceString, String aspectIdentity){
        handleAspectsInterceptorsAssociation(msMapOfInterceptorToAspects, referenceString, aspectIdentity);
        handleAspectsInterceptorsAssociation(msMapOfAspectsToInterceptors, aspectIdentity, referenceString);
    }

    public static void addAspectsToInterceptor(String referenceString, List aspectsList){
        //msMapOfInterceptorToAspects.put(referenceString, aspectsList);
        for(Object obj : aspectsList){
            addAspectsToInterceptor(referenceString, (String) obj);
        }
    }
    
    private static void handleAspectsInterceptorsAssociation(Map<String, List<String>> map, String refString1, String refString2){
        List list = null;
        if(map.containsKey(refString1)){
            list = map.get(refString1);
        } else {
            list = new ArrayList();
        }
        list.add(refString2);
        map.put(refString1, list);
    }

    /*
    private static boolean removeAspectProperties(List list, String identity) {
        boolean bRemoved = false
        if(list != null) {
            synchronized(list) {
                for(Object obj : list) {
                    Properties prop = (Properties) obj;
                    if(prop.getProperty(Aspect.IDENTITY).equals(identity)) {
                        list.remove(obj);
                        bRemoved = true;
                        break;
                    }
                }
            }
        }
        return bRemoved;
    }
    */
}
