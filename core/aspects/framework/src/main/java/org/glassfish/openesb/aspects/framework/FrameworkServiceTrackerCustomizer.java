/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.framework;

import java.lang.reflect.Method;
import org.glassfish.openesb.aspects.api.AspectFactory;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author rdara
 */
public class FrameworkServiceTrackerCustomizer implements ServiceTrackerCustomizer {

    public Object addingService(ServiceReference sr) {
        //String aspectType = (String) sr.getProperty(Aspect.ASPECT_TYPE);
        AspectsFrameworkHelper.createAspectsFromWaitQueue(getAspectType(sr));
        return new Integer(1);
    }

    public void modifiedService(ServiceReference arg0, Object arg1) {
    }

    public void removedService(ServiceReference sr, Object arg1) {
        AspectsFrameworkHelper.addAspectsToWaitQueue(getAspectType(sr));
    }
    
    private String getAspectType(ServiceReference sr) {
        AspectFactory aspectFactory = (AspectFactory) FrameworkActivator.getBundleContext().getService(sr);
        return aspectFactory.getAspectType();
    }
    
}
