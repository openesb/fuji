/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.glassfish.openesb.aspects.api;

import java.util.Properties;
import javax.jbi.messaging.MessageExchange;

/**
 *
 * @author rdara
 */
public interface Aspect {

   
    String ASPECT_IDENTITY = "aspect.identity";
    String ASPECT_TYPE = "aspect.type";
    String ACTIVE_STATE = "ActiveState";
     
    void start();   // A stooped aspect can be started.
    void stop();    // An aspect can be stopped, which makes it inactive
    
    void destroy(); //Cleanup, Close

    //String setIdentity();   
    String getIdentity(); //Obtain the identity of the object
    
    String getAspectType();
        
    boolean isActive(); //Indicates the state of the aspect. Active or Inactive
    
    void setProperties(Properties prop);    //Sets the properties of the object
    Properties getProperties();             //Get the properties of the object
    
    boolean execute(MessageExchange msgEx); //Executge the object
    
    public Properties getSupportedProperties();
}
