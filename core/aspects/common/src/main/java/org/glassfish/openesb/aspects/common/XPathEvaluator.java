/**
 * 
 */
package org.glassfish.openesb.aspects.common;

import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * @author rdara
 * 
 */
public class XPathEvaluator {

	/**
	 * 
	 */
	public XPathEvaluator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param inputSource
	 * @param expression
	 * @return
	 * @throws XPathExpressionException
	 */
	public static final String evaluateDocument(DOMSource inputSource,
			String expression) throws XPathExpressionException {
		String result = null;
		XPathFactory factory = XPathFactory.newInstance();
		XPath xPath = factory.newXPath();
		XPathExpression xPathExpression = xPath.compile(expression);
		if(inputSource != null) {    //Excpetion??
                    result = xPathExpression.evaluate(inputSource.getNode());
                }
		return result;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
