/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentTaskResult.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.logging.Logger;

import javax.jbi.management.DeploymentException;

import org.w3c.dom.Element;

import com.sun.jbi.framework.result.TaskResultDetails.MessageType;
import com.sun.jbi.framework.result.TaskResultDetails.TaskId;
import com.sun.jbi.framework.result.TaskResultDetails.TaskResult;

/**
 * Object model for <code>component-task-result</code> element in a JBI 
 * management result message.
 * @author kcbabo
 */
public class ComponentTaskResult extends ResultElement {
    private static Logger mLogger = 
            Logger.getLogger(ComponentTaskResult.class.getName());
    
    public static void inspectResult(String result) throws DeploymentException {
        ComponentTaskResult taskResult = JbiTask.createComponentTaskResult(result);
        if (taskResult != null && taskResult.getTaskResultDetails() != null) {
            switch (taskResult.getTaskResultDetails().getMessageType()) {
                case ERROR:
                case WARNING: {
                    // will be logged when caught
                    throw new ResultException(
                            new DeploymentException(result), taskResult);
                }
                case INFO: {
                    mLogger.finer(taskResult.display());
                    break;
                }
            }
        }
    }
    
    public static String getLocalizedMessage(ComponentTaskResult taskResult) {
        if (taskResult != null && taskResult.getTaskResultDetails() != null) {
            if (taskResult.getTaskResultDetails().getTaskStatusMsgs().size() > 0) {
                LocalizedMessage msg = 
                        taskResult.getTaskResultDetails().getTaskStatusMsgs().get(0);
                return (msg == null)
                        ? taskResult.getComponentName() +" did not provide exception info!"
                        : msg.getMessage();
            }
        }
        
        return null;
    }
    
    /**
     * Builds a standard <code>SUCCESS</code> task message per 
     * the JBI specification.
     * 
     * @param task The task name.
     * @return An XML string representing a task message.
     */
    public static String createSuccessMessage(String componentName, TaskId taskId) {
        ComponentTaskResult result = JbiTask.createComponentTaskResult(
                componentName, taskId, TaskResult.SUCCESS, MessageType.INFO);
        return result.asString();
    }

    /**
     * Builds a standard <code>FAILED</code> task message containing 
     * one <code>msg-loc-info</code> element per the JBI specification.
     * 
     * @param task The task name.
     * @param locToken The message key for looking up localized text for the message.
     * @param locMessage The default message, using {@link java.text.MessageFormat} patterns.
     * @param locParams An array of parameters, may be <code>null</code>.
     * @return An XML string representing a task message.
     */
    public static String createExceptionMessage(String componentName, TaskId taskId,
                                                Throwable exception, String locToken,
                                                String locMessage, Object... locParams) {
        ComponentTaskResult result = JbiTask.createComponentTaskResult(
                componentName, taskId, TaskResult.FAILED, MessageType.WARNING);
        result.getTaskResultDetails().addTaskStatusMsg(locMessage, locToken, locParams);
        if (exception != null) {
            ExceptionInfo info = result.getTaskResultDetails().addExceptionInfo();
            info.setNestingLevel("1");
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw, true);
            exception.printStackTrace(pw);
            info.setStackTrace(sw.toString());
        }
        
        return result.asString();
    }

    /**
     * Builds a standard <code>FAILED</code> task message containing 
     * one <code>msg-loc-info</code> element per the JBI specification.
     * 
     * @param task The task name.
     * @param locToken The message key for looking up localized text for the message.
     * @param locMessage The default message, using {@link java.text.MessageFormat} patterns.
     * @param locParams An array of parameters, may be <code>null</code>.
     * @return An XML string representing a task message.
     */
    public static String createFailedMessage(String componentName, TaskId taskId,
                                             String locToken, String locMessage,
                                             Object... locParams) {
        ComponentTaskResult result = JbiTask.createComponentTaskResult(
                componentName, taskId, TaskResult.FAILED, MessageType.ERROR);
        result.getTaskResultDetails().addTaskStatusMsg(locMessage, locToken, locParams);
        
        return result.asString();
    }

    public ComponentTaskResult(Element element) {
        super(element);
    }
    
    public String display() {
        StringBuffer buff = new StringBuffer();
        /*
Details
========
Description: sun-xslt-engine raised a DeploymentException during init phase.
Message:
COMPTK-6007: Service unit "fuji2-sun-xslt-engine" failed to start provider endpoints: TRANSL-6053: sun-xslt-engine EndpointInfo[name=genHtml_endpoint,service={http://fuji.dev.java.net/application/fuji2}genHtml,provides] is missing from descriptor in service unit: fuji2-sun-xslt-engine
         */
        buff.append("Details\n========\nDescription: ")
            .append(getComponentName()).append(" reported with ")
            .append(getTaskResultDetails().getMessageType())
            .append(" status during ").append(getTaskResultDetails().getTaskId())
            .append(" phase.\nMessage:\n---------\n").append(getLocalizedMessage(this))
            .append("\n\nStacktrace (i.e. where the error occurred):")
            .append("\n---------------------------------------------");

        return buff.toString();
    }
    
    public TaskResultDetails getTaskResultDetails() {
        Element trd = getChildElement(TaskElem.component_task_result_details);
        List<Element> kids = findChildElements(trd, TaskElem.task_result_details);
        return (!kids.isEmpty()) ? new TaskResultDetails(kids.get(0)) : null;
    }
    
    public String getComponentName() {
        return getChildElement(TaskElem.component_name).getTextContent(); 
    }
    
    public void setComponentName(String componentName) {
        getChildElement(TaskElem.component_name).setTextContent(componentName);
    }

    @Override
    protected void initialize() {
        createChildElement(TaskElem.component_name);
        Element ctrdEle = createChildElement(TaskElem.component_task_result_details);
        ctrdEle.appendChild(ctrdEle.getOwnerDocument().createElement(
                TaskElem.task_result_details.toString()));
    }
}
