/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)filterType.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jbi.messaging.MessageExchange;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

/**
 * Interface for a filterter
 * 
 * @author Sun Microsystems, Inc.
 */
public class XpathFilter 
    implements MessageFilter{ 

    String              name_;
    XPathExpression     xpathExpression_;
    String              expression_;
    DocumentBuilder     builder_;
    Transformer         transformer_;
    
    /**
     * Create an Xpath filterter instance
     * @param expression - XPath expression
     */
    public XpathFilter(String name, String appNamespace, Map config)
        throws Exception
    {
        HashMap             namespaces = ConfigurationUtil.getNamespaces(config);
        NamespaceContext    namespaceCtx = new NamespaceContextImpl(appNamespace, namespaces);
        XPath               xpath = XPathFactory.newInstance().newXPath();

        xpath.setNamespaceContext(namespaceCtx);        
        name_ = name;
        xpathExpression_ = xpath.compile(expression_ = (String)config.get("exp"));
       
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setIgnoringElementContentWhitespace(true);
        builder_ = dbf.newDocumentBuilder();
        
        transformer_ = TransformerFactory.newInstance().newTransformer();
        transformer_.setOutputProperty (OutputKeys.METHOD, "xml");
    }
    
    /**
     *  @return true if the current active message in the exchange matches the 
     *         filter. 
     * 
     * @param exchange - exchange to filter
     */
    public boolean matches(MessageExchange exchange)
        throws Exception
    {
         ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
        
         Document document = getPayloadDocument(svcMsg); 
            
         boolean match = (Boolean)xpathExpression_.evaluate(document.getDocumentElement(), XPathConstants.BOOLEAN);

         return match;
    }
    
    /**
     *  @return true if the current active message in the exchange matches the
     *         filter.
     *
     * @param exchange - exchange to filter
     */
    public boolean matches(Object doc)
        throws Exception
    {
        if (doc instanceof Document) {
            boolean match = (Boolean)xpathExpression_.evaluate(doc, XPathConstants.BOOLEAN);
            return match;
        }
        return (false);
    }

    /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(MessageExchange exchange)
        throws Exception
     {
         ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
        
         Document document = getPayloadDocument(svcMsg); 
            
         String result = xpathExpression_.evaluate(document.getDocumentElement());
         return result;
     }
    
    /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(Object doc)
        throws Exception
     {
         if (doc instanceof Document) {
            String result = xpathExpression_.evaluate(doc);
            return result;
         }
         return (null);
     }
          
    /**
     * @return the Document instance for the payload
     */
     private Document getPayloadDocument(ServiceMessage svcMsg)
        throws Exception
     {         
        Object payload = svcMsg.getPayload();
         
        if ( payload instanceof Node )
        {
            Document document = null;
            Node doc = (Node) payload;

            // parse the XML as a W3C Document
            if ( doc.getChildNodes().getLength() == 0 && 
                 doc.getNodeType() == Node.TEXT_NODE )
            {
                document = builder_.parse(new InputSource(new StringReader(doc.getNodeValue())));
            }
            else if (doc.getNodeType() == Node.ELEMENT_NODE )
            {
                document = builder_.parse(new InputSource(NodeToString(doc)));
            } else if (doc.getNodeType() == Node.DOCUMENT_NODE) {
                document = (Document)doc;
            }

            return document;
        }
        else
        {
            throw new Exception("Cannot filter a non-XML message using Xpath");
        }
     }
    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private StringReader NodeToString(Node node)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        transformer_.transform(new DOMSource(node), new StreamResult(sw));
        return new StringReader(sw.toString());
    }

}
