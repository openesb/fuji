/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceProperty.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public enum ServiceProperty { 
    METHOD ("method"),
    NAME ("name"),
    RUNTIME_REGISTERED ("runtime.registered"),
    BUNDLE_ID ("bundle.id"),
    IS_DYNAMIC_INSTANCE("is.dynamic.instance"),
    SERVICE_ASSEMBLY_NAME("service.assembly.name"),
    PARENT_BUNDLE_ID("parent.bundle.id"); // This is the id of the bundle which has the aspect properties
    
    private String property_;
    
    ServiceProperty(String property) {
        property_ = property;
    }
    
    @Override
    public String toString() {
        return property_;
    }
}
