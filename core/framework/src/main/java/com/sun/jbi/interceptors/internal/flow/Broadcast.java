/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Broadcast.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import com.sun.jbi.fuji.ifl.Flow;
import org.glassfish.openesb.api.message.MessageProps;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;

/**
 * This class provides support for message broadcast in a IFL route definition.
 * @author Sun Microsystems, Inc.
 */
public class Broadcast extends AbstractFlow  {

    // List of services that we need to broadcast this message to
    private List<String> broadcastServices_;
    private String       broadcastName_;
    private String       gatherService_;
     // Log ref
    private Logger log_ = Logger.getLogger(Broadcast.class.getPackage().getName());
    
    public Broadcast() {};
    /**
     * Creates a new broadcast flow.
     * @param namespace for this endpoint
     * @param broadcastInfo used to configure this instance
     */
    public Broadcast(String name, String namespace, Map<String,Object> broadcastInfo)
            throws Exception {
        
        super(namespace, name, com.sun.jbi.fuji.ifl.Flow.BROADCAST);

        // register a single endpoint in the NMR endpoint registry for 
        // a broadcast endpoint
        broadcastName_ = name;
        activateEndpoint(getServiceQName(broadcastName_), broadcastName_, false);
        broadcastServices_ = (List<String>)broadcastInfo.get(Flow.Keys.TO.toString());
    }

    protected boolean acceptEvent(int id) { return (false); }

    protected void onRequest(AbstractFlow.InboundContext context) {
        InboundContext  ic = (InboundContext)context;
        List<String>    request;
        MessageExchange exchange = ic.getExchange();
        log_.fine("EIP: Broadcast Request (" + broadcastName_ + "," + ic.getExchange().getExchangeId() + ")");

        newRequest(ic);
    }

    protected void onReply(AbstractFlow.OutboundContext oc) {
        log_.fine("EIP: Broadcast Reply (" + broadcastName_ + "," + oc.getExchange().getExchangeId() + ")");
        handleResponse(oc);

    }

    protected void onStatus(AbstractFlow.OutboundContext oc) {
        InboundContext      ic = (InboundContext)oc.getInboundContext();

        log_.fine("EIP: Broadcast Status-Outbound (" + broadcastName_ + "," +
                ic.getExchange().getExchangeId() + ")");
        
        if (ic.isGatherPhase()) {
            ic.removeChild(oc);
        } else {
            handleResponse(oc);
        }
     }

    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Broadcast Status-Inbound (" + broadcastName_ + "," + ic.getExchange().getExchangeId() + ")");
    }

    private void newRequest(InboundContext ic) {
        AbstractFlow.OutboundContext    oc;
        MessageExchange         exchange = ic.getExchange();
        MessageExchange         broadcast;
 
        try {
            //
            // New request. Send to all recipients.
            //
            long    counter = 0;
            broadcast = exchange;
  
            for (String service : broadcastServices_) {
                MessageExchange         me;

                if (exchange instanceof InOut) {
                    me = getExchangeFactory().createInOutExchange();
                } else {
                    me = getExchangeFactory().createInOnlyExchange();
                }
                ServiceEndpoint endpoint = getEndpoint(service);
                // Don't fail the exchange at this point, since other messages
                // may have passed through already.  Need to weave this into our
                // general error handling logic for this flow type.
                if (endpoint == null) {
                    log_.warning("Unable to locate broadcast endpoint: " + service);
                    continue;
                }
                me.setEndpoint(getEndpoint(service));
                // make the interceptor exchange look like the intercepted exchange
                copy(exchange, me, false);
                NormalizedMessage   nm = makeMessageCopy(me, exchange.getMessage("in"));
                me.setMessage(nm, "in");

                //
                //  Add ME properties.
                //
                ((com.sun.jbi.ext.MessageExchange)exchange).setParent(exchange);

                //
                // Add the standard NM properties.
                //
                nm.setProperty(MessageProps.GROUP_ID.toString(), exchange.getExchangeId());
                nm.setProperty(MessageProps.MESSAGE_ID.toString(),Long.valueOf(++counter).toString());
                nm.setProperty(MessageProps.MESSAGE_CT.toString(), Long.valueOf(broadcastServices_.size()).toString());
                nm.setProperty(MessageProps.MESSAGE_TYPE.toString(), "sequence");

                //
                // Invoke the service
                //
                log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Child(" + me.getExchangeId() + ") STARTED");
                oc = newOutboundContext(me);
                oc.setInboundContext(ic);
                ic.addChild(oc);
                request(oc);
            }
       } catch (javax.jbi.messaging.MessagingException mEx) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            mEx.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: Broadcast(" + broadcastName_ +") NewRequest Exception: " + b.toString() + toString());
        }
    }

    private void handleResponse(OutboundContext oc) {
        MessageExchange             exchange = oc.getExchange();
        InboundContext              ic = (InboundContext)oc.getInboundContext();
        MessageExchange             broadcast = ic.getExchange();

        try {
            if (exchange.getStatus().equals(ExchangeStatus.DONE)) {
                //
                //  InOnly request completed successfully.
                //
                log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Child(" + exchange.getExchangeId() + ") DONE");
                ic.removeChild(oc);
            } else if (exchange.getStatus().equals(ExchangeStatus.ERROR)) {
                //
                //  InOnly or InOut completed in ERROR.
                //
                log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Child(" + exchange.getExchangeId() + ") ERROR");
                ic.removeChild(oc);
                if (exchange.getError() == null) {
                    broadcast.setStatus(ExchangeStatus.ERROR);
                } else {
                    broadcast.setError(exchange.getError());
                }
            } else {
                //
                //  InOut returns response.
                //
                log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Scatter(" + exchange.getExchangeId() + ")");
                ic.removeChild(oc);
                if (exchange.getMessage("out") != null) {
                    ic.addResponse(exchange.getMessage("out"));
                }
                exchange.setStatus(ExchangeStatus.DONE);
                status(oc);
            }

            //
            //  See if we are done handling this request.
            //
            if (!ic.hasChildren()) {
                completeRequest(ic);
            }
        }  catch (javax.jbi.messaging.MessagingException mEx) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            mEx.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: Broadcast(" + broadcastName_ +") HandleResponse Exception: " + b.toString() + toString());
        }
    }

    private void completeRequest(InboundContext ic) {
        MessageExchange         broadcast = ic.getExchange();

        try {
            if (broadcast.getStatus() == ExchangeStatus.ERROR) {
                //
                //  Could be InOnly or InOut. Just cleanup and return the saved ERROR.
                //
                log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Complete ERROR");
                status(ic);
            } else if (ic.childIsInOnly()) {
                //
                // Completed an InOnly. Just need to ACK the original request.
                //
                log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Complete DONE");
                broadcast.setStatus(ExchangeStatus.DONE);
                status(ic);
            } else {
                 QName                      serviceName = (QName)broadcast.getProperty(ROUTE_SERVICE);
                 List<NormalizedMessage>    responses = ic.getResponses();
                 long                       count = responses.size();

                 //
                 //  Forward the responses.
                 //
                 ic.setGatherPhase();
                 for (NormalizedMessage nm : responses) {
                     MessageExchange    me = getExchangeFactory().createInOnlyExchange();
                     OutboundContext    oc = newOutboundContext(me);

                     me.setService(serviceName);
                     copy(broadcast, me, true);
                     me.setMessage(nm, "in");
                     nm.setProperty(MessageProps.MESSAGE_CT.toString(), Long.valueOf(count).toString());
                     log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Scatter(" + me.getExchangeId() + ") STARTED");
                     oc.setInboundContext(ic);
                     ic.addChild(oc);
                     request(oc);
                 }

                 //
                 // Reply with an empty result.
                 //
                 log_.fine("EIP: Broadcast(" + broadcastName_ + "," + broadcast.getExchangeId() + ") Reply");
                 status(ic);
            }
        }  catch (javax.jbi.messaging.MessagingException mEx) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            mEx.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: Broadcast(" + broadcastName_ +") CompleteRequest Exception: " + b.toString() + toString());
        }

    }

    public InboundContext newInboundContext(MessageExchange me){
        return (new InboundContext(this, me));
    }

    class InboundContext extends AbstractFlow.InboundContext {
        List<NormalizedMessage>            response_;
        boolean                            phase_;
        boolean                            inonly_;

        InboundContext(AbstractFlow flow, MessageExchange me) {
            super(flow, me);
            response_ = new LinkedList();
            phase_ = false;
            inonly_ = me instanceof InOnly;
        }

        List<NormalizedMessage> getResponses() {
            return (response_);
        }

        void addResponse(NormalizedMessage nm) {
            response_.add(nm);
        }

        void setGatherPhase() {
            phase_ = true;
        }

        boolean isGatherPhase() {
            return (phase_);
        }

        boolean childIsInOnly() {
            return (inonly_);
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(super.toString());
            sb.append("          Phase: " + (phase_ ? "Gather" : "Scatter"));
            sb.append("          Type: " + (inonly_ ? "InOnly" : "InOut"));
            sb.append("          Response(s):\n");
            for (NormalizedMessage nm : response_) {
                sb.append("            Response:    " +
                        (nm.getContent() == null ? "null" : nm.getContent().getClass().getName()) + "\n");
                for (Object prop : nm.getPropertyNames()) {
                    Object      value = nm.getProperty((String)prop);
                    sb.append("              Name: ");
                    sb.append((String)prop);
                    if (value instanceof String)
                    {
                        sb.append("\n                Value(String): ");
                        sb.append((String)value);
                    }
                    else if (value instanceof QName)
                    {
                        sb.append("\n            Value(QName): ");
                        sb.append((QName)value);
                    }
                    else if (value instanceof Long)
                    {
                        sb.append("\n                Value(Long): ");
                        sb.append((Long)value);
                    }
                    else if (value != null)
                    {
                        sb.append("\n                Value(Type): ");
                        sb.append(value.getClass().getName());
                    }
                    sb.append("\n");
                }
            }
           return (sb.toString());
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        Broadcast (" + broadcastName_ + ")\n");
        return (sb.toString());
    }

}

