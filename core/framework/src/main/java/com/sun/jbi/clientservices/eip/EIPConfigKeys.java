/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPConfigKeys.java - Last published on May 20, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.clientservices.eip;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public enum EIPConfigKeys {
    APP_NS ("app-ns"),
    NAME ("name"),
    CONFIG ("config"),
    CONFIG_ID ("config-id"),
    CONFIG_IDS ("config-ids"),
    TYPE ("type");
    
    
    
    private String key_;
    
    EIPConfigKeys(String key) {
        key_ = key;
    }
    
    @Override
    public String toString() {
        return key_;
    }
}
