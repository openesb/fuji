/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyManager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.osgi.internal;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.DeploymentException;

import org.osgi.framework.Bundle;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.descriptor.ServiceUnit;
import com.sun.jbi.framework.result.ComponentTaskResult;
import com.sun.jbi.framework.result.ResultException;
import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.IFLReader;
import com.sun.jbi.interceptors.Interceptor;
import com.sun.jbi.interceptors.internal.aspects.AspectHelper;


/**
 * Manages service assembly bundles installed in the OSGi framework.
 * @author kcbabo
 */
public class ServiceAssemblyManager {
    
    // Service assemblies are exploded into this directory
    private static final String SA_DIR = "service-assemblies";
    private Logger logger_ = Logger.getLogger(
            ServiceAssemblyManager.class.getPackage().getName());
    // References to SA bundles that we know about
    private Map<Long, ServiceAssemblyBundle> bundles_ =
            new ConcurrentHashMap<Long, ServiceAssemblyBundle>();
    
    // Record associating all registered dynamic aspects with the parent SA
    private Map<String, List<String>> aspects_;
    
    /** Watches for changes to fuji-related archives and triggers a bundle update. */
    private UpdateWatchdog updateWatchdog_ = new UpdateWatchdog();

    // Singleton
    private static ServiceAssemblyManager saManager_; 
    
    /**
     * There is only one instance of this class
     * 
     */
    private ServiceAssemblyManager(){};
    
    /**
     * @return the instance of this class.
     */
    public static ServiceAssemblyManager getInstance() {
        if ( saManager_ == null ){
            saManager_ = new ServiceAssemblyManager();
        }
        
        return saManager_;
    }
    
    /**
     * Indicates whether a given bundle is known to the SA manager.
     * @param bundleId OSGi bundle id
     * @return true if the bundle has been registered with the SA manager, false
     * otherwise.
     */
    public boolean isRegistered(Long bundleId) {
        return bundles_.containsKey(bundleId);
    }

    /**
     * Returns a ServiceAssemblyBundle reference for the given bundle id.
     * @param bundleId bundle id
     * @return service assembly bundle if the bundle has been registered, 
     * otherwise null.
     */
    public ServiceAssemblyBundle getServiceAssembly(Long bundleId) {
        return bundles_.get(bundleId);
    }

    /**
     * Returns a ServiceAssemblyBundle for the given service assembly name.  
     * Be careful when using this method when multiple versions of an SA may
     * exist.
     * @param name service assembly name
     * @return service assembly bundle if the bundle has been registered, 
     * otherwise null.
     */
    public ServiceAssemblyBundle getServiceAssembly(String name) {
        ServiceAssemblyBundle assembly = null;

        for (ServiceAssemblyBundle sa : bundles_.values()) {
            if (sa.getName().equals(name)) {
                assembly = sa;
                break;
            }
        }

        return assembly;
    }
    
    /**
     * Get the service assembly for the specified application namespace
     * 
     */
    public ServiceAssemblyBundle getServiceAssemblyForNamespace(String appNamespace){
        
        Collection<ServiceAssemblyBundle> saBundles = getServiceAssemblies();
        ServiceAssemblyBundle theSaBundle = null;
        
        for (ServiceAssemblyBundle saBundle : saBundles){
            if ( saBundle.getNamespace().equals(appNamespace)){
                theSaBundle = saBundle;
                break;
            }
        }
        return theSaBundle;
    }
    
    /**
     * Read the configuration from the service assembly bundle
     * @param sa
     * @return
     */
    public Map readConfigurationFromSABundle(ServiceAssemblyBundle sa) throws Exception {
        
        Properties props = new Properties();
        Enumeration metaFiles = sa.getBundle().getEntryPaths("META-INF");
        URL configURL  =null;
        while (metaFiles.hasMoreElements()) {
            String path = (String) metaFiles.nextElement();
            if (path.endsWith("app.properties") ) {
                configURL = sa.getBundle().getEntry(path);
            }
        }
        
        if ( configURL != null){
            java.io.InputStream ios = configURL.openStream();
            props.load(ios);
            ios.close();
        }
        return props;
    }

    /**
     * Returns a list of all service assembly bundles that have been registered.
     * @return all registered SA bundles
     */
    public Collection<ServiceAssemblyBundle> getServiceAssemblies() {
        return bundles_.values();
    }

    /**
     * Start the service assembly bundle with the specified bundle id.
     * @param bundleId id of an SA bundle
     */
    public void startServiceAssembly(Long bundleId) {
        if (!isRegistered(bundleId)) {
            return;
        }
        
        ServiceAssemblyBundle sa = bundles_.get(bundleId);

        try {
            sa.start();
            
        } catch (Exception ex) {
            try
            {
                // -- Rollback the bundle 
                sa.getBundle().stop();
            }
            catch(org.osgi.framework.BundleException bex)
            {
                logger_.log(Level.WARNING, "Failed to rollback service assembly bundle " + 
                    sa.getName() + " to the stopped state", bex);
            }
        }
    }

    /**
     * Shutdown the service assembly bundle with the specified bundle id.
     * @param bundleId id of an SA bundle
     */
    public void shutDownServiceAssembly(Long bundleId) {
        if (!isRegistered(bundleId)) {
            return;
        }

        ServiceAssemblyBundle sa = bundles_.get(bundleId);

        try {
            sa.stop();
            sa.shutDown();
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Failed to shut down service assembly " +
                    sa.getName(), ex);
        }
    }

    /**
     * Deploy the service assembly contained in the OSGi bundle.
     * @param bundle bundle containing a service assembly
     * @param jbiXml jbi.xml descriptor
     */
    void deployServiceAssembly(Bundle bundle, Jbi jbiXml) {
        String saName = jbiXml.getServiceAssembly().getIdentification().getName();
        logger_.info("Deploying JBI service assembly " +
                saName);

        try {
            registerServiceAssembly(new ServiceAssemblyBundle(jbiXml, bundle));
        } catch (Exception ex) {
            logger_.log(Level.WARNING, ex.getMessage());
            logger_.log(Level.FINE, ex.getMessage(), ex);
            try
            {
                bundle.uninstall();
            }
            catch(org.osgi.framework.BundleException bex)
            {
                logger_.log(Level.WARNING, "Failed to rollback failed deploy for service assembly " + 
                    saName, bex);
            }
        }
    }
    
    /**
     * Update for an SA consists of replacing the deployed artifacts in the
     * old SA directory with the new stuff.  Component SPIs are not involved.
     * @param bundleId
     */
    void updateServiceAssembly(Bundle bundle) {
        if (!isRegistered(bundle.getBundleId())) {
            return;
        }

        ServiceAssemblyBundle sa = new ServiceAssemblyBundle(
                BundleUtil.getJbiXml(bundle), bundle);
        logger_.info("Updating JBI service assembly " + sa.getName());
        
        // Out with the old, in with the new
        try {
            File rootDir = getServiceAssemblyDirectory(sa.getName());
            if (rootDir != null) {
                FileUtil.cleanDirectory(rootDir);
                createDeploymentRoot(sa);
                // replace the old entry in our internal map
                bundles_.put(bundle.getBundleId(), sa);
            }
        }
        catch (Exception ex) {
            logger_.log(Level.WARNING, "Update of service assembly failed", ex);
        }
        
    }

    /**
     * Undeploy the service assembly with the specified bundle id.
     * @param bundleId id of the SA bundle
     */
    void undeployServiceAssembly(Long bundleId) {
        if (!isRegistered(bundleId)) {
            return;
        }

        ServiceAssemblyBundle sa = bundles_.get(bundleId);
        logger_.info("Undeploying JBI service assembly " + sa.getName());

        try {
            // Undeploy service units
            for (ServiceUnit su : sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
                ComponentBundle component =
                        Environment.getComponentManager().getComponent(su.getComponentName());

                // Start the target component if it's not already active
                if (component.getBundle().getState() != Bundle.ACTIVE) {
                    component.getBundle().start();
                }

                // Invoke undeploy on the component's ServiceUnitManager impl
                ServiceUnitManager sum = component.getComponent().getServiceUnitManager();
                String result = sum.undeploy(su.getIdentification().getName(),
                        getServiceUnitDirectory(sa.getName(), su).getAbsolutePath());
                // will throw ResultException wrapper if message is task result xml
                ComponentTaskResult.inspectResult(result);
            }

            unregisterServiceAssembly(sa, true);
        }
        catch (Exception ex) {
            logger_.log(Level.WARNING, "Undeployment of service assembly failed", ex);
        }
;
    }



    /**
     * Registers a service assembly bundle with the SA Manager.  The SA is 
     * recorded in the SA manager registry and each service unit in the SA is
     * deployed to its target component.
     * @param sa bundle to register
     * @throws java.lang.Exception failed to create deployment root or the
     * service unit deployment failed.
     */
    void registerServiceAssembly(ServiceAssemblyBundle sa) throws Exception {

        File rootDir = null;
        
        // Check for the deployment root
        try {
            if (!getServiceAssemblyDirectory(sa.getName()).exists()) {
                rootDir = createDeploymentRoot(sa);
                // If the deployment root didn't exist, that means the SUs have
                // not been deployed
                deployServiceUnits(sa);
            }
        }
        catch (Exception ex) {
            if (rootDir != null) {
                FileUtil.cleanDirectory(rootDir);
                rootDir.delete();
            }
            
            throw ex;
        }

        // Add it to the watch list
        updateWatchdog_.watch(sa.getBundle());
        // Add the service assembly to our registry
        bundles_.put(sa.getBundle().getBundleId(), sa);
    }

    /**
     * Deploys each service unit in the specified service assembly.
     * @param sa service assembly bundle to deploy
     * @throws java.lang.Exception one or more service units failed to deploy
     */
    void deployServiceUnits(ServiceAssemblyBundle sa) throws Exception {
        
        List<ServiceUnit> deployedServiceUnits = new ArrayList<ServiceUnit>();
        // Deploy each service unit in the SA
        try {
            for (ServiceUnit su : sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
                String compName = su.getComponentName();
                String suName = su.getIdentification().getName();

                if (compName == null) {
                    throw new Exception("Deployment of service assembly failed.  Could not find component name for service unit " + suName + " in service assembly descriptor");
                }

                ComponentBundle component = Environment.getComponentManager().
                        getComponent(compName);

                if (component == null) {
                   throw new Exception("Deployment of service assembly failed.  Could not find OSGi bundle for component " + compName);
                }

                // Start the target component if it's not already active
                if (component.getBundle().getState() != Bundle.ACTIVE) {
                    component.getBundle().start();
                }

                // Invoke deploy on the component's ServiceUnitManager impl
                ServiceUnitManager sum = 
                        component.getComponent().getServiceUnitManager();
                String result = sum.deploy(su.getIdentification().getName(),
                        getServiceUnitDirectory(sa.getName(), su).getAbsolutePath());
                // will throw ResultException wrapper if message is task result xml
                ComponentTaskResult.inspectResult(result);

                deployedServiceUnits.add(su);
            }
        }
        catch (Exception ex) {
            for (ServiceUnit su : deployedServiceUnits) {
                try {
                    ComponentBundle component = Environment.getComponentManager().
                        getComponent(su.getComponentName());
                    ServiceUnitManager sum = 
                        component.getComponent().getServiceUnitManager();
                    String result = sum.undeploy(su.getIdentification().getName(),
                        getServiceUnitDirectory(sa.getName(), su).getAbsolutePath());
                    ComponentTaskResult.inspectResult(result);
                }
                catch (Exception ex2) {
                    logger_.log(Level.FINE, "Failed clean up service unit " +
                            su.getIdentification().getName() + " after failed deploy.", ex2);
                }
            }

            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(ex.getMessage());
            throw ex;
        }
    }

    /**
     * Removes the service assembly bundle from the SA manager registry and
     * optionally deletes the deployment root.  The deployment root should only
     * be deleted when the service assembly is being undeployed.
     * @param sa service assembly to unregister
     * @param delete true to delete the deployment root, false otherwise
     */
    void unregisterServiceAssembly(ServiceAssemblyBundle sa, boolean delete) {

        if (delete) {
            // blow up the deployment root
            File saDir = getServiceAssemblyDirectory(sa.getName());
            FileUtil.cleanDirectory(saDir);
            saDir.delete();
        }

        // Remove it from the watch list
        updateWatchdog_.unwatch(sa.getBundle());
        // Remove the service assembly from our registry
        bundles_.remove(sa.getBundle().getBundleId());
    }

    /**
     * Create a deployment root for the specified service assembly.
     * @param sa service assembly bundle in need of a deployment root
     * @throws java.lang.Exception failed to extract service assembly into
     * deployment root.
     */
    File createDeploymentRoot(ServiceAssemblyBundle sa)
            throws Exception {
        // Create the top-level dir
        File rootDir = getServiceAssemblyDirectory(sa.getName());
        rootDir.mkdirs();

        // Create a directory for each service unit and explode the contents
        for (ServiceUnit su : sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
            File suDir = getServiceUnitDirectory(sa.getName(), su);
            suDir.mkdir();
            BundleUtil.explodeContents(
                    sa.getBundle(), su.getArtifactsZip(), suDir, true);
        }
        
        return rootDir;
    }

    /**
     * Retruns the deployment root for the specified service assembly.
     * @param saName service assembly name
     * @return deployment root directory
     */
    static File getServiceAssemblyDirectory(String saName) {
        return Environment.getFrameworkBundleContext().getDataFile(
                SA_DIR + File.separator + saName);
    }

    /**
     * Retruns the deployment root for the specified service unit.
     * @param saName service unit name
     * @return deployment root directory
     */
    static File getServiceUnitDirectory(String saName, ServiceUnit su) {
        return new File(getServiceAssemblyDirectory(saName),
                su.getComponentName() + "_" + su.getIdentification().getName());
    }

    /**
     * Find and parse IFL in application bundle
     * @param sa
     * @return
     */
    static protected  IFLModel readIFLFromSABundle(ServiceAssemblyBundle sa) throws Exception {
        IFLModel model = null;
        Enumeration metaFiles = sa.getBundle().getEntryPaths("META-INF");
        ArrayList<URL> iflURLList = new ArrayList<URL>();
        while (metaFiles.hasMoreElements()) {
            String path = (String) metaFiles.nextElement();
            if (path.endsWith(".ifl") || path.endsWith(".IFL")) {
                iflURLList.add(sa.getBundle().getEntry(path));
            }
        }
        IFLReader iflReader = new IFLReader();
        model = iflReader.read(iflURLList.toArray(new URL[0]));       
        return model;
    }
   
    
    /** 
     * Returns a property set for any filters declared in the specified 
     * interceptor class.  This property set is used to register an interceptor
     * in the OSGi service registry.
     * @param interceptor
     * @return
     */
    Properties getInterceptorProperties(Interceptor interceptor) {
        Properties p = new Properties();
        
        return p;
    }
}
