/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageDescriptorImpl.java -
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import com.sun.jbi.ext.ExchangeDescriptor;
import com.sun.jbi.ext.MessageDescriptor;
import com.sun.jbi.messaging.MessageExchangeProxy;
import java.net.URI;
import java.util.HashMap;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;
import javax.xml.namespace.QName;

/**
 *
 * @author derek
 */
public class ExchangeDescriptorImpl implements ExchangeDescriptor {
    private String                  mId;
    private String                  mConsumer;
    private String                  mProvider;
    private String                  mRole;
    private String                  mStatus;
    private String                  mAction;
    private URI                     mPattern;
    private QName                   mOperation;
    private QName                   mInterface;
    private QName                   mService;
    private Exception               mError;
    private MessageDescriptor       mIn;
    private MessageDescriptor       mOut;
    private MessageDescriptor       mFault;
    private HashMap<String,String>  mProperties;

    public ExchangeDescriptorImpl(MessageExchange me, boolean isSend)
    {
        MessageExchangeProxy    mep = (MessageExchangeProxy)me;
        NormalizedMessage       nm;
        mId = me.getExchangeId();
        mConsumer = mep.getSourceComponent();
        mProvider = mep.getTargetComponent();
        mRole = mep.getRole().toString();
        mStatus = mep.getStatus().toString();
        mOperation = mep.getOperation();
        mInterface = mep.getInterfaceName();
        mService = mep.getService();
        mPattern = mep.getPattern();
        mAction = isSend ? "send" : "accept";
        nm = mep.getActiveMessage(isSend);
        if (nm != null)
        {
            if (nm == mep.getMessage("in"))
            {
                mIn = (MessageDescriptor)new MessageDescriptorImpl(nm);
            }
            else if (nm == mep.getMessage("out"))
            {
                mOut = (MessageDescriptor)new MessageDescriptorImpl(nm);
            }
            else if (nm == mep.getFault())
            {
                mFault = (MessageDescriptor)new MessageDescriptorImpl(nm);
            }
        }
        mProperties = new HashMap();
        for (Object n : me.getPropertyNames())
        {
            String  name = (String)n;
            Object  v = me.getProperty(name);
            String  value = null;

            if (v != null)
            {
                if (v instanceof String)
                    value = (String)v;
                else
                    value = v.toString();
                mProperties.put(name, value);
            }
        }
    }

    public String getId()
    {
        return (mId);
    }
    public String getConsumer()
    {
        return (mConsumer);
    }
    public String getProvider()
    {
        return (mProvider);
    }
    public String getRole()
    {
        return (mRole);
    }
    public String getStatus()
    {
        return (mStatus);
    }
    public String getAction()
    {
        return (mAction);
    }
    public QName getOperation()
    {
        return (mOperation);
    }
    public QName getInterface()
    {
        return (mInterface);
    }
    public QName getService()
    {
        return (mService);
    }
    public Exception getError()
    {
        return (mError);
    }
    public URI getPattern()
    {
        return (mPattern);

    }
    public MessageDescriptor getIn()
    {
        return (mIn);
    }
    public MessageDescriptor getOut()
    {
        return (mOut);
    }
    public MessageDescriptor getFault()
    {
        return (mFault);
    }
    public HashMap<String,String>   getProperties()
    {
        return (mProperties);
    }
    
    //---------------------------Overrides from Object -------------------------
    public int hashCode()
    {
        return (mId.hashCode() ^ mRole.hashCode());
    }

    public boolean equals(Object obj)
    {
        ExchangeDescriptor  target;
        boolean             isEqual = false;

        if (obj != null && this.getClass() == obj.getClass())
        {
            target = (ExchangeDescriptor)obj;

            if (target.getId().equals(mId) &&
                target.getRole().equals(mRole))
            {
                isEqual = true;
            }
        }

        return isEqual;
    }
}
