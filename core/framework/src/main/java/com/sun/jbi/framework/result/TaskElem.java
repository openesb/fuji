/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TaskElem.java
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.result.TaskResultDetails.MessageType;
import com.sun.jbi.framework.result.TaskResultDetails.TaskResult;

/**
 * 
 *
 * @author ksimpson
 */
public enum TaskElem {
    /**
     * The document element, which, along with the document�s namespace, 
     * identifies this as a JBI management task result/status report.
     */
    jbi_task,
    /**
     * Used to report the results of the implementation�s execution of the task,
     * and, optionally, the result of the component�s part of executing the 
     * task. This latter feature will appear only if applicable (the task 
     * involves interaction with a component, and the task proceeds to the point
     * where that interaction was started.)
     */
    jbi_task_result,
    /**
     * Used to report the results of the implementation�s execution of the task. 
     * This includes an optional element, is-cause-framework, which the 
     * implementation MUST set to �YES� in cases where the JBI implementation 
     * is the cause of the task failing to execute.
     */
    frmwk_task_result,
    /**
     * Used to contain a detailed task report, as well as locale information 
     * about the report (supporting I18N/L10N).
     */
    frmwk_task_result_details,
    /**
     * MUST set to �YES� in cases where the JBI implementation 
     * is the cause of the task failing to execute.
     */
    is_cause_framework,
    /** Locale information about the task report. */
    locale,
    /**
     * Used to report results of interaction(s) with a component. This includes 
     * the component�s unique name and details about the component task results.
     */
    component_task_result,
    /** The component's unique name.*/
    component_name,
    /** Used to report task result details as performed by the component. */
    component_task_result_details,
    /**
     * Used to report the task ID, result (SUCCESS or FAILED), and, optionally, 
     * a typed (ERROR, WARNING, or INFO) message. Zero or more task status 
     * messages can be included (for dealing with multiple interactions with 
     * components). Finally, optional exception information can be provided.
     */
    task_result_details,
    /** The id of the task. */
    task_id,
    /** MUST be either {@link TaskResult#SUCCESS} or {@link TaskResult#FAILED}.*/
    task_result,
    /** 
     * MUST be {@link MessageType#ERROR}, {@link MessageType#WARNING}, or
     * {@link MessageType#INFO}.
     */
    message_type,
    /** One or more task status messages. */
    task_status_msg,
    /** 
     * Used to report a message in the form of a format string and zero or more 
     * text parameters. This structure supports I18N/L10N.*/
    msg_loc_info,
    /** The message key for looking up localized text for the message.*/
    loc_token,
    /**
     * The default message. All messages must use the java.text.MessageFormat
     * patterns to define the message, and where parameters are placed within the message. 
     */
    loc_message,
    /** Zero or more parameters for the message.*/
    loc_param,
    /**
     * Used to report exceptions. It contains the following items:
     * <ul><li></li>
     *     <li></li>
     *     <li></li></ul>
     */
    exception_info,
    /** 
     * This indicates by integer value what the exception�s level was within a 
     * nested set of exceptions.
     */
    nesting_level,
    /** Used to report a stack trace at the point where the exception was thrown.*/
    stack_trace;

    /**
     * This method should be used in place of {@link #valueOf(String)} as it
     * accounts for the discrepancy between the underscore in this enums' names
     * and the hyphen in the corresponding xml element.
     * 
     * @param str The string to evaluate.
     * @return A <code>TaskElem</code> or <code>null</code>.
     */
    public static TaskElem toTaskElem(String str) {
        TaskElem elem = null;
        if (str != null && str.length() > 0) {
            elem = valueOf(str);
            if (elem == null) {
                elem = valueOf(str.replace("-", "_"));
            }
        }
            
        return elem;
    }
    
    @Override
    public String toString() {
        return super.toString().replace("_", "-");
    }
}
