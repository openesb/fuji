/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OpenBufferOutputStream.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import java.io.InputStream;
import java.io.ByteArrayInputStream;

/** Special version of <code>ByteArrayOutputStream</code> which allows direct 
 * access to the underlying byte buffer.  If left on it's own, BAOS will copy
 * the entire buffer every time you access it.  Basically, we lose a bit a 
 * safety in order to gain a ton of performance.  So be careful!
 * @author Sun Microsystems, Inc.
 */
public class OpenBufferOutputStream extends java.io.ByteArrayOutputStream
{
    /** Initializes a new buffer stream. */
    public OpenBufferOutputStream()
    {
        super();
    }
    
    /** Initializes a new buffer stream, with the specified buffer size. 
     *  @param size initial size of the buffer.
     */
    public OpenBufferOutputStream(int size)
    {
        super(size);
    }
    
    /** Retrieves the raw byte buffer underneath the covers.
     * @return raw bytes underlying the buffer. 
     */
    public byte[] getBuffer()
    {
        return buf;
    }
    
    /** Returns the valid number of bytes written to the buffer.
     * @return number of valid bytes in the buffer.
     */
    public int getCount()
    {
        return count;
    }
    
    /** Utility method used to create an appropriately sized stream based off
     * of the underlying buffer.  The stream is clipped to size, so it will not 
     * contain any garbage bytes from the end of the buffer.
     * @return input stream pointing to bytes in the buffer 
     */
    public InputStream asInputStream()
    {
        return new ByteArrayInputStream(buf, 0, count);
    }
}
