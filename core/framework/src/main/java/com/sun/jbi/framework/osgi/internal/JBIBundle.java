/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceRegistration;

/**
 * Base class for all JBI bundle types (component, service assembly, etc.).
 * @author kcbabo
 */
public abstract class JBIBundle {
    
    private Jbi descriptor_;
    private Bundle bundle_;
    private List<ServiceRegistration> services_ = 
            new ArrayList<ServiceRegistration>();
    
    /** 
     * Called by specific bundle types.
     * @param descriptor jbi.xml
     * @param bundle OSGi bundle
     */
    protected JBIBundle(Jbi descriptor, Bundle bundle) {
        descriptor_ = descriptor;
        bundle_ = bundle;
    }
    
    /**
     * Returns the OSGi bundle containing the JBI artifact.
     * @return OSGi bundle reference
     */
    public Bundle getBundle() {
        return bundle_;
    }
    
    /**
     * Returns jbi.xml for the bundle.
     * @return jbi.xml
     */
    public Jbi getDescriptor() {
        return descriptor_;
    }
    
    /** 
     * Returns services that this bundle has registered in the OSGi service
     * registry.
     * @return list of ServiceRegistration
     */
    public synchronized List<ServiceRegistration> getServices() {
        return Collections.unmodifiableList(services_);
    }
    
    /**
     * Adds a service to the registered service list for this bundle.
     * @param service
     */
    public void addService(ServiceRegistration service) {
        services_.add(service);
    }
    
    /**
     * Unregisters all services attached to this bundle and removes them 
     * from the service list.
     */
    public synchronized void unregisterServices() {
        for (ServiceRegistration service : services_) {
            try {
                service.unregister();
            }
            catch (IllegalStateException isEx) {
                // service has already been unregistered
            }
        }
        services_.clear();
    }
    
    /**
     * All bundles must declare their type.
     * @return bundle type
     */
    public abstract BundleType getType();
    
    /**
     * Hook to get at the unique JBI identifier for a bundle without clawing
     * through the descriptor.
     * @return JBI identifer/name for the bundle
     */
    public abstract String getName();
}
