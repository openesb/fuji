/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AspectType.java - Last published on May 13, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.aspects;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public enum AspectType { 
    POLICY("execute", "PolicyImpl", "org.glassfish.openesb.aspects.policy"),
    CACHE("execute", "FIFOCache", "org.glassfish.openesb.aspects.cache" ),
    LOG("execute", "LogImpl", "org.glassfish.openesb.aspects.log");

    private String method_;
    private String classname_;
    private String packagename_;
    
    AspectType(String method, String classname, String packagename) {
        method_ = method;
        classname_ = classname;
        packagename_ = packagename;
    }
    
    public String getMethodName() {
        return method_;
    }
    
    public String getClassName() {
        return classname_;
    }
    
    public String getPackageName() {
        return packagename_;
    }
    
    public String getBasicName() {
        return getPackageName() + "." + getClassName() + "." + getMethodName();
    }
    public static AspectType valueOfString(String type){
        AspectType aspect = AspectType.LOG;
        
        if ( type.equalsIgnoreCase("policy")){
            aspect =  AspectType.POLICY;
        }  else if ( type.equalsIgnoreCase("cache")){
            aspect = AspectType.CACHE;
        }
        return aspect;
    }
}
