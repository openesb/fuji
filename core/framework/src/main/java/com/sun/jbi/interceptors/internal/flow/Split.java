/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Split.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;

import javax.xml.namespace.QName;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.glassfish.openesb.api.message.MessageProps;
import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.eip.split.Splitter;

import org.w3c.dom.Node;

import org.osgi.framework.Bundle;

/**
 * This class provides support for message splits in a IFL route definition.
 * @author Sun Microsystems, Inc.
 */
public class Split extends AbstractFlow {

    // Map containing the information for the split
    private Map<String, Object>     splitInfo_;
    private Splitter                splitter_;
    private String                  splitName_;
    private int                     batchSize_;
    private Logger                  log_ = Logger.getLogger(Split.class.getPackage().getName());

    public Split() {};
    /**
     * Creates a new Split flow.
     * @param splitInfo - split IFL definition
     * @param consumedService - the consumed service 
     * @param Map<String, String> - map containing the split information
     */
    public Split(String name, String namespace, Map splitInfo, Bundle bundle) throws Exception {
        super(namespace, name, com.sun.jbi.fuji.ifl.Flow.SPLIT);
        splitInfo_ = splitInfo;
        config_ = (Properties)splitInfo_.get(Flow.Keys.CONFIG.toString());
        
        // Insert the name into the config
        config_.setProperty(Flow.Keys.NAME.toString(), name);
        SplitType splitType = SplitType.fromString((String)splitInfo_.get(Flow.Keys.TYPE.toString()));
        splitter_ = SplitterFactory.getInstance().createSplitter(namespace,
                splitType, config_, bundle);

        //
        //  See if we are in serial or parallel mode.
        //
        String batchSize = config_.getProperty("batchSize");
        batchSize_ = 1000;
        if (batchSize != null) {
            try {
                batchSize_ = Integer.valueOf(batchSize);
            } catch (NumberFormatException nEx) {
                log_.warning("EIP: Split (" + splitName_ + ") Exception setting batchSize to (" +
                    batchSize + ")");
                batchSize_ = 1;
            }
        }

        // register a single endpoint in the NMR endpoint registry for 
        // a split endpoint
        splitName_ = name;
        activateEndpoint(getServiceQName(splitName_), splitName_, false);
    }

    protected boolean useNew() { return (true); }
    protected boolean acceptEvent(int id) { return (true); }

    protected void onRequest(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Split (" + splitName_ + "," + ic.getExchange().getExchangeId() + ") Complete");
        doSplit((InboundContext)ic);
    }

//    protected void onReply(AbstractFlow.OutboundContext oc) {
//        handleChild(oc);
//    }

    protected void onStatus(AbstractFlow.OutboundContext oc) {
        log_.fine("EIP: Split (" + splitName_ + "," +
                oc.getInboundContext().getExchange().getExchangeId() +
                ") Child (" + oc.getExchange().getExchangeId() + ") Complete");
        handleChild(oc);
    }

    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Split (" + splitName_ + "," + ic.getExchange().getExchangeId() + ") Complete");
    }

    protected void onSuspend(AbstractFlow.OutboundContext oc) {
        if (log_.isLoggable(Level.FINE)) {
            log_.fine("EIP: Split(" + splitName_ + 
                    oc.getInboundContext().getExchange().getExchangeId() + ") onSuspend("
                    + oc.getExchange().getExchangeId() + "");
        }
        InboundContext  ic = (InboundContext)oc.getInboundContext();
        ic.addSuspend(oc);
        if (ic.hasNext()) {
            sendSplit(ic);
        } else if (ic.getCount() == ic.suspendedCount()) {
            signal(ic.getSuspened().getExchange());
        }
    }

    /**
     *  Handle the completion of a child split MessageExchange.
     *
     * @param request Initiating ME
     * @param split Child ME
     */
    private void handleChild(OutboundContext oc) {
        MessageExchange     child = oc.getExchange();
        InboundContext      ic = (InboundContext)oc.getInboundContext();
        MessageExchange     parent = ic.getExchange();

        //
        //  Stop tracking the split child and remove it from the active list.
        //
        ic.removeChild(oc);
        ic.removeSuspended(oc);

        //
        // If the split child completed with an ERROR make sure that the
        // request will return an ERROR.
        //
        if (child.getStatus().equals(ExchangeStatus.ERROR)) {
            parent.setError(child.getError());
        }

        //
        //  Start the next request if any documents remaining.
        //  Otherwise, conplete the request if no children are still outstanding.
        //
        if (ic.hasNext()) {
            sendSplit(ic);
        } else if (!ic.hasChildren()) {
            completeSplit(ic);
        } else if (ic.getChildCount() == ic.suspendedCount()) {
            signal(ic.getSuspened().getExchange());
        }
    }

    /**
     * Split the input document into a set of child split requests.
     * Start as many requests in parallel as allowed.
     *
     * @param exchange Initiating ME
    */
    private void doSplit(InboundContext ic)
    {
        MessageExchange exchange = ic.getExchange();

        try {
            ServiceMessage svcMsg = MessageExchangeUtil.getInMessage(exchange);
            ArrayList      splitDocs = splitter_.split(svcMsg);

            //
            //  Remember state about the Request.
            //
            ic.setSplitDocuments(splitDocs);
            if (ic.hasNext()) {
                //
                //  Start a batch sized amount of parallel children.
                //  If the exchange contains an XA transaction, limit the
                //  parallelism to 1 so that the TM doesn't complain if
                //  multiple threads are involved.
                //
                for (int i = 0; i < batchSize_; i++) {
                    if (ic.hasNext()) {
                        sendSplit(ic);
                    } else {
                        break;
                    }
                    if (exchange.isTransacted()) {
                        break;
                    }
                }
            } else {
                completeSplit(ic);
            }
        } catch (Exception ex) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            ex.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: Split (" + splitName_ + ") Exception:" + b.toString() + toString());
        
            // If there is an error in processing, set Exchange status as Error
            // and short-circuit it
            exchange.setError(ex);
            try {
                status(ic);
            } catch (javax.jbi.messaging.MessagingException mEx) {
                log_.warning("EIP: Split (" + splitName_ + "," + exchange.getExchangeId() +
                        ") ERROR-ERROR");
            }
        }
    }

    /**
     * Send the next split the input document as a split child.
     *
     * @param request Information about initiating ME
    */
    void sendSplit(InboundContext ic) {
        MessageExchange exchange = ic.getExchange();
        QName           serviceName = (QName)exchange.getProperty(ROUTE_SERVICE);
        OutboundContext oc;

        try {
            ServiceMessage  in = MessageExchangeUtil.getInMessage(exchange);
            Node            doc = (Node)ic.nextDocument();

            if (log_.isLoggable(Level.FINER)) {
                log_.finer("EIP: Split (" + splitName_ + "," + exchange.getExchangeId()
                        + ") Document is:\n" + nodeToString(doc));
            }

            //
            //  Split is always InOnly.
            //
            InOnly me = getExchangeFactory().createInOnlyExchange();
            oc = newOutboundContext(me);

            //
            // Set the NM properties.
            //
            ServiceMessage  msg = MessageExchangeUtil.getPrototype(exchange, in);
            msg.setPayload(doc);
            msg.setProperty(MessageProps.GROUP_ID.toString(), ic.getMessageId());
            msg.setProperty(MessageProps.MESSAGE_ID.toString(),
                    new Integer(ic.getNumber()).toString());
            msg.setProperty(MessageProps.MESSAGE_CT.toString(),
                    new Integer(ic.getCount()).toString());
            msg.setProperty(MessageProps.MESSAGE_TYPE.toString(), "sequence");

            //
            //  We use the Routing Slip facility to fork the current route.
            //  1)  Copy the routing information.
            //  2)  Set the service to the controlling route.
            //  3)  Assign a new groupid and messageid. The group-id is based on
            //      the input message so that QOS replay will cause the same
            //      id to be used.
            //
            copy(exchange, me, true);
            me.setService(serviceName);
            me.setProperty("com.sun.jbi.messaging.groupid",
                    ic.getMessageId() + "-" + ic.getNumber());
            me.setProperty("com.sun.jbi.messaging.messageid", "1");
            me.setInMessage(msg);
            ((com.sun.jbi.ext.MessageExchange)me).setParent(exchange);

            //
            // Operation should be "oneWay"
            //
            me.setOperation(new QName(serviceName.getNamespaceURI(), "oneWay"));

            //
            // Invoke the service
            //
            ic.addChild(oc);
            oc.setInboundContext(ic);
            log_.fine("EIP: Split (" + splitName_ + "," + exchange.getExchangeId() +
                    ") Child(" + me.getExchangeId() + ") STARTED");
            request(oc);
        } catch (Exception ex) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            ex.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: Split (" + splitName_ + ") Exception:" + b.toString() + toString());

            // If there is an error in processing, set Exchange status as Error
            // and short-circuit it
            exchange.setError(ex);
            try {
                status(ic);
            } catch (javax.jbi.messaging.MessagingException mEx)
            {
                log_.warning("EIP: Split (" + splitName_ + "," + exchange.getExchangeId() +
                        ") ERROR-ERROR");
            }
        }
    }

    /**
     * Complete a split request.
     *
     * @param request Information about initiating ME
    */
    void completeSplit(InboundContext ic) {
        MessageExchange exchange = ic.getExchange();

        try {
            //
            //  If its still ACTIVE then either return a null NM if InOut or
            //  a status of DONE if InOnly.
            //
            if (exchange.getStatus().equals(ExchangeStatus.ACTIVE)) {
                //
                //  ACK the original request.
                //
                if (exchange instanceof InOnly) {
                    exchange.setStatus(ExchangeStatus.DONE);
                }
            }
            log_.fine("EIP: Split (" + splitName_ + "," + exchange.getExchangeId() +
                    ") Status=" + exchange.getStatus().toString());

            //
            //  Send the ACK/NAK.
            //
            status(ic);

        } catch (javax.jbi.messaging.MessagingException mEx) {
            //
            //  The only way to get here is if the request is already
            //  completed. This would normally happen if the DeliveryChannel
            //  has been closed. So just clean-up a little and ignore it.
            //
            log_.warning("EIP: Split (" + splitName_ + "," + exchange.getExchangeId() +
                    ") Complete-ERROR");
        }

    }

    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private String nodeToString(Node node)
        throws Exception
    {
        String nodeAsString = null;
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();

        if ( node.getNodeType() == Node.ELEMENT_NODE)
        {
            // xsd:anyType
            StringWriter sw = new StringWriter();
            transformer.transform(new DOMSource(node), new StreamResult(sw));
            nodeAsString = sw.toString();
        }
        else if ( node.getNodeType() == Node.TEXT_NODE )
        {
            // xsd:string
            nodeAsString = node.getNodeValue();
        }

        return nodeAsString;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        Split (" + splitName_ + ")\n");
        return (sb.toString());
    }

    public InboundContext newInboundContext(MessageExchange me){
        return (new InboundContext(this, me));
    }

    class InboundContext extends AbstractFlow.InboundContext {
        List<Object>            documents_;
        LinkedList<AbstractFlow.OutboundContext>  suspended_;
        String                  msgid_;
        Exception               exception_;
        Object                  content_;
        int                     msgCount_;
        int                     msgNumber_;

        InboundContext(AbstractFlow flow, MessageExchange me) {
            super(flow, me);
        }

        void setSplitDocuments(List splits) {
            msgid_ = (String)getExchange().getProperty("com.sun.jbi.messaging.groupid");
            documents_ = splits;
            msgCount_ = splits.size();
            msgNumber_ = 0;
        }

        Object nextDocument() {
            msgNumber_++;
            return (documents_.remove(0));
        }

        boolean hasNext() {
            return !documents_.isEmpty();
        }

        int getCount() {
            return (msgCount_);
        }

        int getNumber() {
            return (msgNumber_);
        }

        String getMessageId() {
            return (msgid_);
        }

        void addSuspend(OutboundContext oc) {
            if (suspended_ == null) {
                suspended_ = new LinkedList();
            }
            suspended_.addLast(oc);
        }

        OutboundContext getSuspened() {
            return (suspended_.removeFirst());
        }

        void removeSuspended(OutboundContext oc) {
            if (suspended_ != null) {
                suspended_.remove(oc);
            }
        }

        int suspendedCount() {
            if (suspended_ == null) {
                return (0);
            }
            return (suspended_.size());
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(super.toString());
            sb.append("          Split:\n");
            sb.append("            GroupId:  " + msgid_ + "\n");
            sb.append("            Count:    " + msgCount_ + "\n");
            sb.append("            Number:   " + msgNumber_ + "\n");
            return (sb.toString());
        }

    }
}
