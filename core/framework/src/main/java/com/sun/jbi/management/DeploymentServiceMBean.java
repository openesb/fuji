/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentServiceMBean.java
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * This DeploymentServiceMBean extends the public interface. This has additional
 * operations to :
 * <br/>
 * <ul>
 * <li> Get the name of the Service Assembly that contains a Service Unit</li>
 * </ul>
 * @author Sun Microsystems, Inc.
 */
public interface DeploymentServiceMBean
    extends javax.jbi.management.DeploymentServiceMBean
{
    /**
     * Get the Service Assembly name for a specific service unit deployed to a
     * specific component.
     *
     * @param serviceUnitName - service unit identifier
     * @param componentName - component identifier
     */
    String getServiceAssemblyName(String serviceUnitName, String componentName) throws Exception;
}
