/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AspectHelper.java - Last published on May 13, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.aspects;

import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.interceptors.internal.ConfigurationHelper;
import com.sun.jbi.interceptors.internal.ServiceProperty;
import com.sun.jbi.interceptors.internal.InterceptionHelper;
import com.sun.jbi.interceptors.Interceptor;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.lang.reflect.Method;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.ServiceReference;

/**
 * Helper class to create/delete aspects dynamically.
 * 
 * @author nikki
 */
public class AspectHelper {
        
    private static AspectHelper aspectHelper_;
    
     /** The Logger **/
    private  Logger log_ = Logger.getLogger(AspectHelper.class.getPackage().getName());
    
    /**
     * This is a map of registered aspects [ aspect instance name | svc registration ]
     */
    private HashMap<String, ServiceRegistration> registeredAspects_;
        
    private List<HashMap> dynamicAspectConfigs_;
    
    private static String ASPECTS_DIR = "/META-INF/aspects/";
    
    private AspectHelper( ){
        
        dynamicAspectConfigs_ = new ArrayList();
    };
    
    public static AspectHelper getInstance(){
        if(aspectHelper_ == null ){
            aspectHelper_ = new AspectHelper();
        }
        return aspectHelper_;
        
    }
    /**
     * Create a new aspect instance
     * 
     * @param config - the aspect configuration
     */
    public String create(HashMap config) throws Exception{
        
        // Get the name and type from the config
        //  name should be unique
        String name = (String)config.get(ServiceProperty.NAME.toString());
        String type = (String)config.get(AspectProperty.TYPE.toString());
        
        if ( type == null ){
                throw new Exception("Failed to create aspect instance, since type is not specified in the configuration");
        }
        
       
        //  from the type get the class and method name.
        ServiceReference aspectSvc = getRegisteredAspectService(type);
        
        if ( aspectSvc != null ){ 
            Long bundleId = getRegisteredAspectBundleId(aspectSvc);
            String method = getRegisteredAspectMethodName(aspectSvc);
        
            if ( name == null ){
                // Generate a name
            int i=1;
            
            for ( ; getRegisteredAspectsMap().containsKey(method +"-"+i); i++){};
                name = method +"-"+i;
                config.put(ServiceProperty.NAME.toString(), name);
            }

            if ( getRegisteredAspectsMap().containsKey(name)){
                throw new Exception("Duplicate aspect instance. An instance with name " + name + " already exists");
            }

            config.put(ServiceProperty.IS_DYNAMIC_INSTANCE.toString(), true);
            ServiceRegistration svcReg = createNewInstance(method, bundleId, config);
            registeredAspects_.put(name, svcReg); 
        } else {
            throw new Exception("Failed to create aspect with name " + name + " interceptor service of type " + type + " is not registered");
        }
        
        return name;
    }
            

    /**
     * Delete an aspect instance
     * 
     * @param name - aspect instance name
     */
    public void delete(String name) throws Exception{
        
        // Lookup the service registration and delete the service registration
        if ( getRegisteredAspectsMap().containsKey(name)){
            ServiceRegistration svcReg = getRegisteredAspectsMap().get(name);
            svcReg.unregister();
            getRegisteredAspectsMap().remove(name);
            
            // delete the Configuration for the aspect
            ConfigurationHelper.deleteConfiguration(name);
        }
    }
    
    /**
     * Look at the registered interceptor configurations for
     * dynamic aspects. For each dynamic aspect, get the bundle id and 
     * create an interceptor instance.
     * 
     */
     public void registerDynamicAspects(boolean startup) throws Exception{
         
         if ( startup ){
             java.util.Dictionary[] dynConfigs = ConfigurationHelper.getDynamicConfigurations();

             HashMap configMap = new HashMap();
             for(java.util.Dictionary config : dynConfigs ){

                 if ( config != null ){
                     java.util.Enumeration keys = config.keys();
                     while ( keys.hasMoreElements()){
                         Object key = keys.nextElement();
                         configMap.put(key, config.get(key));
                     }

                     try{
                        create(configMap);
                        log_.finest(" Found and created Dynamic Configuration with keys" + config.toString());
                     } catch ( Exception ex){
                         // some problem, most likely interceptor not registered for type try later
                        dynamicAspectConfigs_.add(configMap); 
                        log_.finest("Deferred creation of Dynamic Configuration with keys" + config.toString());
                     }
                     
                 }
             }
         } else {
             log_.finest("Executing Deferred creation of Dynamic Configuration");
             if (!dynamicAspectConfigs_.isEmpty()){
                List<HashMap> cfgsToDelete = new ArrayList();
                for(HashMap config : dynamicAspectConfigs_){
                    try{
                        create(config); 
                        log_.fine("Found and created Dynamic Configuration with keys" + config.toString());
                        cfgsToDelete.add(config);
                    } catch ( Exception ex){
                        // some problem, most likely interceptor not registered for type try later
                        log_.finest("Deferred creation of Dynamic Configuration with keys" + config.toString());
                    }
                }
                
                if (!cfgsToDelete.isEmpty()){
                    for(HashMap config : cfgsToDelete){
                        dynamicAspectConfigs_.remove(config);
                    }
                }
             }
         }
         
     }
    
    /**
     * Create aspects defined in the service assembly. The aspect properties are
     * included in the service assembly in the META-INF/aspects folder.
     * 
     * @param bundle - the  OSGi Bundle
     */
    public void createAspectsInBundle(Bundle bundle){
        
        // If there is an aspects folder in the bundle, get all aspect configurations
        Enumeration<String> paths = bundle.getEntryPaths(ASPECTS_DIR);
        
        if ( paths != null ) {
            while ( paths.hasMoreElements() )
            {
                String path = paths.nextElement();
                if ( path.endsWith("/") ){
                    // Process all the folders
                    Enumeration<String> files = bundle.getEntryPaths(path);
                    
                    if ( files != null ){
                        while (files.hasMoreElements()){
                            String file = files.nextElement();
                            
                            if ( file.endsWith(".properties")){
                                log_.finest("Creating an aspect from properties file " + file + " in path   :" + path + " in bundle " + bundle.getSymbolicName());
                                
                                createFromConfiguration(bundle, path, file);
                            }
                        }
                    }
                } else if (path.endsWith(".properties")) {
                    createFromConfiguration(bundle, null, path);
                }
            }
        }
    }
    
    /**
     * Delete all aspects which were created based on properties defined in the bundle. 
     */
    public void deleteAspectsInBundle(long bundleId){
        
        // Get all dynamic configurations with the service.assembly.name = saName
        java.util.Dictionary[] dynConfigs = ConfigurationHelper.getDynamicConfigurations();

        for(java.util.Dictionary config : dynConfigs ){

            if ( config != null ){
                Object value = config.get(ServiceProperty.PARENT_BUNDLE_ID.toString());
                if ( value != null ){
                    Long aspectBundleId = Long.valueOf(value.toString());

                    if (aspectBundleId != null ){
                        if ( aspectBundleId == bundleId){
                            // Delete the configuration
                            String name = (String)config.get(ServiceProperty.NAME.toString());
                            try{

                                delete(name);
                                log_.finest("Deleted aspect with name " + name + " in bundle " + aspectBundleId);
                            } catch (Exception ex ){
                                log_.log(Level.FINEST, "Failed to delete aspect with name " + name + " in bundle " + aspectBundleId, ex);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private HashMap<String, ServiceRegistration> getRegisteredAspectsMap(){
        
        if ( registeredAspects_ == null ){
            registeredAspects_ = new HashMap();
        }
        return registeredAspects_;
    }
    
    private ServiceReference getRegisteredAspectService(String type)
        throws Exception{
        
        ServiceReference aspectSvc = null;
        BundleContext ctx = Environment.getFrameworkBundleContext();
        
        
        ServiceReference[] svcRefs = ctx.getServiceReferences("com.sun.jbi.interceptors.Interceptor", "(type="+ type +")");
        
        if ( svcRefs != null ){
            for ( ServiceReference svcRef : svcRefs) {

                Object dynProp = svcRef.getProperty(ServiceProperty.IS_DYNAMIC_INSTANCE.toString());

                Boolean isDynamic = false;

                if (dynProp != null ){
                    isDynamic =  (Boolean)dynProp;
                }
                if ( !isDynamic ){
                    aspectSvc = svcRef;
                }    
            } 
        }
        
        return aspectSvc;
    }
    
    private Long getRegisteredAspectBundleId(ServiceReference svcRef) 
            throws Exception{
        
        return (Long)svcRef.getProperty(ServiceProperty.BUNDLE_ID.toString());
    }
    
    private String getRegisteredAspectMethodName(ServiceReference svcRef) 
            throws Exception{
        
        Method mtd = (Method)svcRef.getProperty(ServiceProperty.METHOD.toString());
        
        if ( mtd != null ){
            StringBuffer strBuff = new StringBuffer();
            strBuff.append(mtd.getDeclaringClass().getName());
            strBuff.append(".");
            strBuff.append(mtd.getName());
            return strBuff.toString();
        } else {
            throw new Exception("The method property is not defined for the aspect service " + svcRef.toString());
        }
    }
    
    private ServiceRegistration createNewInstance(String methodName, Long bundleId, HashMap config)
        throws Exception{
        
        Bundle aspectBundle = Environment.getFrameworkBundleContext().getBundle(bundleId);
        
        if ( aspectBundle != null ){
            

            // The method name is of the form packagename.classname.methodname
            int lastDot = methodName.lastIndexOf(".");
            
            String className = methodName.substring(0, lastDot);
            String method = methodName.substring(lastDot+1, methodName.length());
            
            return InterceptionHelper.registerInterceptorInBundle(
                    aspectBundle, 
                    className, 
                    method,
                    new java.util.Hashtable(config));
           
        }
        else{
            throw new Exception("Bundle with Id " + bundleId + " not found");
        }
    }
    
    /**
     * Create an aspect from properties file in a bundle
     */ 
    private void createFromConfiguration(Bundle bundle, String path, String file){
        java.net.URL resURL = bundle.getEntry(file);  
        if ( resURL != null )
        {
            try
            {
                java.io.InputStream ios = resURL.openStream();
                Properties props = new Properties();
                props.load(ios);
                HashMap configMap = new HashMap();

                if ( props.size() > 0 ){
                     java.util.Enumeration keys = props.keys();
                     while ( keys.hasMoreElements()){
                         Object key = keys.nextElement();
                         configMap.put(key, props.get(key));
                     }
                     configMap.put(ServiceProperty.PARENT_BUNDLE_ID.toString(), bundle.getBundleId());

                     // If the "type" is missing, then the parent folder name is the type
                     if (!configMap.containsKey(AspectProperty.TYPE.toString()) && path != null ){

                        String tmp = path.substring(0, path.length()-1);
                        String parentDir = tmp.substring(tmp.lastIndexOf("/")+1, tmp.length());
                        log_.finest("Deriving aspect type from parent folder, so  " + AspectProperty.TYPE.toString() + " = " + parentDir);
                        configMap.put(AspectProperty.TYPE.toString(), parentDir);
                     }
                     try{
                        // -- Create the Aspect
                        create(configMap);
                        log_.finest(" Found and created Dynamic Configuration with keys" + props.toString());
                     } catch ( Exception ex){
                         // some problem, most likely interceptor not registered for type try later
                        dynamicAspectConfigs_.add(configMap); 
                        log_.info("Deferred creation of Dynamic Configuration with keys" + props.toString());
                     }

                 }

            }
            catch ( java.io.IOException ioex )
            {
                log_.warning("Failed to create aspect based on properties file " + file + " in bundle " + bundle.getSymbolicName());
            }
        } else {
            log_.warning("Could not find properties file " + file + " in bundle " + bundle.getSymbolicName());
        }
     }
}
