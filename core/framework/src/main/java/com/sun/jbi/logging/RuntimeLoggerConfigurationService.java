/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeLoggerConfigurationService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;


import java.util.Dictionary;
import java.util.Collection;
import java.util.logging.Level;
import java.util.Iterator;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.framework.BundleContext;

import com.sun.jbi.configuration.Constants;


/**
 * This is a managed service that is registered to provide a configuration interface
 * to the admin tools. This class is used to provide a service to configure various 
 * runtime loggers. Whenever a runtime logger is modified it updates the configuration of the
 * corresponding display service. Also it updates the configuration of a list of 
 * ComponentLoggerConfiguration services. 
 */
public class RuntimeLoggerConfigurationService extends LoggerConfigurationService {
    
    
    /**
     * the service PID.
     */
    private static String SERVICE_PID = Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR +
                                        "runtime-logger" + LoggingService.SEPARATOR + "runtime-logger";
                
    /**
     * the display service PID
     */
    private static String DISPLAY_SERVICE_PID = Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR +
                                        "runtime-logger-display" + LoggingService.SEPARATOR + "runtime-logger-display";
    
    /**
     * Constructor
     * Creates a new LogConfigurationService with the provided PID
     * @param initalConf the inital configuration
     */
    public RuntimeLoggerConfigurationService(BundleContext bundleCtx) {
        super(SERVICE_PID, DISPLAY_SERVICE_PID, RuntimeLoggingService.getLoggers());
    }
    
    
    /**
     * This method is used to update the log display service with a list of 
     * loggers and their effective log levels
     * @param configuration loggers and their log levels
     */
    public void updated(Dictionary configuration) {
        super.updated(configuration);
        updateAllComponentLoggerConfigurationServices();
    }
        
       

    /**
     * This method is used to update the configuration of all component log 
     * configuration services. 
     * Jbi root logger com.sun.jbi is the parent for runtime loggers and all
     * componet loggers. When there is any change to the root logger the levels
     * of component loggers are affected naturally. But we need to do this 
     * refresh in order to update the display service.      
     */ 
    private void updateAllComponentLoggerConfigurationServices() {
        
        ConfigurationAdmin configAdmin;

        try {
            
            if ((configAdmin = getConfigurationAdminService()) != null) {
                
                Collection<ComponentLoggerConfigurationService> compLogConfigServices = 
                ComponentLoggerConfigurationServiceFactory.getComponentLoggerConfigurationServices().values();
                    
                for ( ComponentLoggerConfigurationService logService : compLogConfigServices) {
                    String servicePID = logService.getLoggerConfigurationServicePID();
                    if (servicePID != null) {
                        Configuration config = configAdmin.getConfiguration(servicePID, null);
                        //if the service is registered but the config has not been updated, getProperties() will be null
                        if (config.getProperties() != null) {
                            config.update(config.getProperties());
                        }
                    }
                }
             }
            
         } catch (Exception ex) {
            log_.log(Level.WARNING, translator_.getString(LocalStringKeys.CONFIG_UPDATE_FAILED), ex);
         }        
    }        

}
