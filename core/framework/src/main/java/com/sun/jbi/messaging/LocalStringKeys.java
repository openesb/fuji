/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

/** i18n Message Keys.
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{    
    /** Message. */
    String FAULT_ON_MSG = "FAULT_ON_MSG";
    
    /** Message. */
    String STATUS_ON_MSG = "STATUS_ON_MSG";
    
    /** Message. */
    String ERROR_STATUS = "ERROR_STATUS";

    /** Message.*/
    String ADDR_NO_ENDPOINT = "ADDR_NO_ENDPOINT";
    
    /** Message. */
    String CANT_FIND_ENDPOINT_FOR_SERVICE = "CANT_FIND_ENDPOINT_FOR_SERVICE";
    
    /** Message. */
    String CANT_FIND_ENDPOINT_FOR_INTERFACE = "CANT_FIND_ENDPOINT_FOR_INTERFACE";
    
    /** Message. */
    String DUPLICATE_ENDPOINT = "DUPLICATE_ENDPOINT";
    
    /** Message.*/
    String INACTIVE_ENDPOINT = "INACTIVE_ENDPOINT";
    
    /** Message.*/
    String INVALID_MEP_URI = "INVALID_MEP_URI";
    
    /** Message.*/
    String INVALID_DESTINATION = "INVALID_DESTINATION";    
    
    /** Message.*/
    String FAULT_NOT_SUPPORTED = "FAULT_NOT_SUPPORTED";    
    
    /** Message.*/
    String PATTERN_INCONSISTENT = "PATTERN_INCONSISTENT";
    
    /** Message.*/
    String CHANNEL_CLOSED = "CHANNEL_CLOSED";

    /** Message.*/
    String SEND_NOT_LEGAL = "SEND_NOT_LEGAL";

    /** Message.*/
    String SENDSYNCH_NOT_LEGAL = "SENDSYNCH_NOT_LEGAL";

    /** Message.*/
    String ILLEGAL_STATE_CHANGE = "ILLEGAL_STATE_CHANGE";

    /** Message.*/
    String CANT_GET_DEFAULT_TRANSACTION = "CANT_GET_DEFAULT_TRANSACTION";

    /** Message. */
    String EXCHANGE_TERMINATED = "EXCHANGE_TERMINATED";

    /** Message.*/
    String CANT_SUSPEND = "CANT_SUSPEND";

    /** Message.*/
    String MUST_SUSPEND = "MUST_SUSPEND";

    /** Message.*/
    String CANT_RESUME = "CANT_RESUME";

    /** Message.*/
    String CANT_RESUME_INVALID = "CANT_RESUME_INVALID";
    
    /** Used when a channel attempts to deactivate an endpoint it didn't register. */
    String DEACTIVATE_NOT_OWNER = "DEACTIVATE_NOT_OWNER";
    
    /** Used when a null or empty service/endpoint name is used. */
    String ACTIVATE_NOT_NULL = "ACTIVATE_NOT_NULL";
    
    /** Used when sending a MessageExchange that has timed out.     */
    String MESSAGE_TIMEOUT = "MESSAGE_TIMEOUT";
    
    /** Message. */
    String WSDL_IMPORT_ERROR = "WSDL_IMPORT_ERROR";

    /** Message. */
    String ENDPOINT_CONNECTION_EXISTS = "ENDPOINT_CONNECTION_EXISTS";

    /** Message. */
    String INTERFACE_CONNECTION_EXISTS = "INTERFACE_CONNECTION_EXISTS";

    /** Message. */
    String ENDPOINT_NO_DESCRIPTOR = "ENDPOINT_NO_DESCRIPTOR";
    
    /** Message. */
    String NONEXISTENT_OPERATION = "NONEXISTENT_OPERATION";
    
    /** Message. */
    String CAPABILITY_NO_MATCH = "CAPABILITY_NO_MATCH";

    /** Message. */
    String SERVICE_CONNECTION_NO_ENDPOINT = "SERVICE_CONNECTION_NO_ENDPOINT";

    /** Message. */
    String EXTERNAL_ENDPOINT_NOT_VALID = "EXTERNAL_ENDPOINT_NOT_VALID";

    /** Message. */    
    String SOURCE = "SOURCE";
    
    /** Message. */    
    String TARGET = "TARGET";
}
