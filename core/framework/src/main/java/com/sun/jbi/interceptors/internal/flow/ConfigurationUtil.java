/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationUtil.java - Last published on May 27, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * Configuration handler.
 * 
 * @author Sun Microsystems
 */
public class ConfigurationUtil {

    /**
     * Utility method to get the namespaces from the configuration. The namespace
     * is specified in the configuration as: xmlns.prefix = namespace
     * 
     * @param config - configuration map which may have namespace configuration.
     */
    public static  HashMap<String, List<String>> getNamespaces(Map config){
     
        HashMap<String, List<String>> namespaces = new HashMap();
        
        java.util.Set<String> keys = config.keySet();
        
        for(String key: keys){
            
            if ( key.startsWith("xmlns.")) {
                String prefix = key.substring(6);
                String nsURI = (String)config.get(key);
                
                if ( namespaces.containsKey(nsURI))
                {
                    // another prefix for the same URI
                    List<String> prefixes = (List)namespaces.get(nsURI); 
                    prefixes.add(prefix);
                } else {
                    List<String> prefixes = new java.util.ArrayList();
                    prefixes.add(prefix);
                    namespaces.put(nsURI, prefixes);
                }
            }
        }
        
        return namespaces;
    }
}
