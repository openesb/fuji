/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationManagedService.java - Last published on Nov 10, 2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import com.sun.jbi.framework.StringTranslator;
import com.sun.jbi.interceptors.LocalStringKeys;

import java.lang.reflect.Method;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ConfigurationException;

import java.util.Properties;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * A Managed Service for interceptor configuration. One instance of this
 * class is registered for each interceptor instance.
 * 
 * @author Sun Microsystems
 */
public class ConfigurationManagedService
        implements ManagedService{
    
    // Framework Bundle Context
    BundleContext context_;
    
    // Interceptor Service Reference
    ServiceReference serviceRef_;
    
    // The Logger 
    private static Logger 
            log_ = Logger.getLogger(ConfigurationManagedService.class.getPackage().getName());
    
    // The StringTranslator
    private static StringTranslator 
            translator_ = new StringTranslator("com.sun.jbi.interceptors", null);
    /**
     * Ctor.
     * 
     * @param context BundleContext
     * @param serviceRef - interceptor service reference 
     */
    public ConfigurationManagedService(BundleContext context, ServiceReference serviceRef)
    {
        context_    = context;
        serviceRef_ = serviceRef;
    }
        
    /**
     * ManagedService Interface Impl
     */
    
    /**
     * Configuration for a interceptor has been updated, forward the 
     * configuration change to the interceptor.
     * 
     * @param properties
     * @throws ConfigurationException
     */
    public void updated(java.util.Dictionary dictionary)
             throws ConfigurationException
    {
        if ( dictionary != null )
        {
            // Invoke setConfiguration() on the actual service instance
            Method mtd = InterceptionHelper.getSetConfigurationMethod(serviceRef_);

            if ( mtd != null )
            {
                try
                {
                    Properties properties = new Properties();
                    java.util.Enumeration keys = dictionary.keys();
                    java.util.ArrayList systemProps = ConfigurationHelper.getSystemProperties(true);
                    while ( keys.hasMoreElements() )
                    {
                        String key = (String)keys.nextElement();
                        if (!systemProps.contains(key))
                        {
                            properties.setProperty(key, dictionary.get(key).toString());
                        }
                    }

                    log_.fine("Updating configuration " + properties.toString());
                    mtd.invoke(
                            InterceptionHelper.getInterceptorInstance(serviceRef_), 
                            properties.getProperty(ServiceProperty.NAME.toString()), 
                            properties);      
                }
                catch (java.lang.IllegalAccessException ex)
                {
                    // log the warning
                    String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_RESTRICTED_SET_CONFIGURATION, 
                        mtd.getName(), mtd.getDeclaringClass().getName());
                    log_.log(Level.WARNING, errMsg, ex);
                }
                catch (java.lang.reflect.InvocationTargetException ex)
                {
                    // log the warning
                    String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_FAILED_SET_CONFIGURATION, 
                        mtd.getName(), mtd.getDeclaringClass().getName());
                    log_.log(Level.WARNING, errMsg, ex);
                }
            }
            else
            {
                String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_MISSING_SET_CONFIGURATION);
                log_.fine(errMsg);
            }
        }
    }

}
