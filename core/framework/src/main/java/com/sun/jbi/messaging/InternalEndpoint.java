/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InternalEndpoint.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.xml.namespace.QName;

import org.w3c.dom.DocumentFragment;

/** Represents a service endpoint which has been activated in the NMR by a 
 * JBI component.
 * @author Sun Microsystems, Inc.
 */
public class InternalEndpoint extends RegisteredEndpoint
{
    private DocumentFragment mReference;
    
    /** Creates a new InternalEndpoint.
     *  @param service service name
     *  @param endpoint endpoint name
     *  @param ownerId id of component which is registering the service
     *  @param type endpoint type
     */
    public InternalEndpoint(QName service, String endpoint, String ownerId)
    {
        super(service, endpoint, ownerId);
    }
    
    public int getType() 
    {
        return INTERNAL;
    }    
   
    
    public String toString()
    {
        StringBuilder   sb = new StringBuilder();
        
        sb.append("        Endpoint Type: Internal\n");
        sb.append(super.toString());
        return (sb.toString());
    }

}
