/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Environment.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.messaging.MessageService;
import org.osgi.framework.BundleContext;

/**
 * Global vars rule!!!!!!
 * 
 * @author kcbabo
 */
public class Environment {
    
    private static ServiceAssemblyManager   saManager_;
    private static ComponentManager         compManager_;
    private static MessageService           messageService_;
    private static BundleContext            bundleContext_;
    
    
    public static MessageService getMessageService() {
        return messageService_;
    }
    
    public static BundleContext getFrameworkBundleContext() {
        return bundleContext_;
    }
    
    public static ComponentManager getComponentManager() {
        return compManager_;
    }
    
    public static ServiceAssemblyManager getServiceAssemblyManager() {
        return saManager_;
    }
    
    protected static void setMessageService(MessageService messageService) {
        messageService_ = messageService;
    }
    
    protected static void setServiceAssemblyManager(ServiceAssemblyManager saManager) {
        saManager_ = saManager;
    }
    
    protected static void setComponentManager(ComponentManager compManager) {
        compManager_ = compManager;
    }
    
    protected static void setFrameworkBundleContext(BundleContext bundleContext) {
        bundleContext_ = bundleContext;
    }
    
}
