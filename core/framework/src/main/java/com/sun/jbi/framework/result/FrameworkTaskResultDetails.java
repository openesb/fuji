/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkTaskResultDetails.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.util.Locale;
import org.w3c.dom.Element;

/**
 * Object model for <code>frmwk-task-result-details</code> element in a JBI 
 * management result message.
 * @author kcbabo
 */
public class FrameworkTaskResultDetails extends ResultElement {
    
//    public static final String ELEMENT_NAME =
//            "frmwk-task-result-details";
//    
//    private static final String LOCALE = 
//            "locale";
    
    public FrameworkTaskResultDetails(Element element) {
        super(element);
    }
    
    public TaskResultDetails getTaskResultDetails() {
        Element trd = getChildElement(TaskElem.task_result_details);
        return trd != null ? new TaskResultDetails(trd) : null;
    }
    
    public String getLocale() {
        return getChildElement(TaskElem.locale).getTextContent(); 
    }
    
    public void setLocale(String locale) {
        getChildElement(TaskElem.locale).setTextContent(locale); 
    }
    
    @Override
    protected void initialize() {
        createChildElement(TaskElem.task_result_details);
        createChildElement(TaskElem.locale);
        // set to runtime locale by default
        setLocale(Locale.getDefault().toString());
    }
}
