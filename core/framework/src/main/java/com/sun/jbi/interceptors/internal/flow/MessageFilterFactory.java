/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageFilterFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import java.util.Properties;

import com.sun.jbi.interceptors.internal.flow.contentroute.FilterCollection;

/**
 * Interface for a splitter
 * 
 * @author Sun Microsystems, Inc.
 */
public class MessageFilterFactory { 
    
    /**
     * Singleton
     * 
     */
    
    private static MessageFilterFactory factory_;
    
    private MessageFilterFactory()
    {
        
    }
    
    /**
     * Get an instance of the factory.
     * 
     */
    public static MessageFilterFactory getInstance()
    {
        if ( factory_ == null)
        {
            factory_ = new MessageFilterFactory();
        }
        return factory_;
    }
    
    /**
     * Create a message filter based on the filter information
     *
     * @param type - Filter Type
     * @param config - Map containing the filter configuration
     */
    public MessageFilter createFilter(String name, String appNamespace, String type, Properties config)
        throws Exception
    {
        if ( FilterType.XPATH.toString().equalsIgnoreCase(type))
        {
            return new XpathFilter(name, appNamespace, config);
        }
        else if ( FilterType.REGEX.toString().equalsIgnoreCase(type))
        {
            return new RegexFilter(name, config);
        }
        else if ( FilterType.HEADER.toString().equalsIgnoreCase(type))
        {
            return new HeaderFilter(name, config);
        }
        else
        {
            throw new Exception("Unsupported filter type " + type);
        }
    }

    /**
     * Create a message filter based on the filter information
     *
     * @param type - Filter Type
     * @param config - Map containing the filter configuration
     */
    public MessageFilter createFilterCollection(String name, String appNamespace, String type, Properties config)
        throws Exception
    {
        if (FilterType.XPATH.toString().equalsIgnoreCase(type) ||
            FilterType.REGEX.toString().equalsIgnoreCase(type) ||
            FilterType.HEADER.toString().equalsIgnoreCase(type) ||
            FilterType.ANY.toString().equalsIgnoreCase(type))
        {
            return new FilterCollection(name, type, config);
        }
        else
        {
            throw new Exception("Unsupported filter collection type " + type);
        }
    }
}
