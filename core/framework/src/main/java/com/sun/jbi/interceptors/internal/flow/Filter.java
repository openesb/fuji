/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Filter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import com.sun.jbi.fuji.ifl.Flow;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;



/**
 * This class provides support for message Filters in a IFL route definition.
 * @author Sun Microsystems, Inc.
 */
public class Filter extends AbstractFlow {

    // Map containing the information for the Filter
    private Map<String, Object> filterInfo_;
    private MessageFilter       filter_;
    private String              filterName_;
    private Logger              log_        = Logger.getLogger(Filter.class.getPackage().getName());

    public Filter() {};
    /**
     * Creates a new Filter flow.
     * @param FilterInfo - Filter IFL definition
     * @param consumedService - the consumed service 
     * @param Map<String, String> - map containing the Filter information
     */
    public Filter(String name, String namespace, Map<String, Object> filterInfo) throws Exception {
        super(namespace, name, com.sun.jbi.fuji.ifl.Flow.FILTER);
        filterInfo_ = filterInfo;
        config_ = (Properties)filterInfo_.get(Flow.Keys.CONFIG.toString());
        filter_ = MessageFilterFactory.getInstance().createFilter(name, namespace,
                (String)filterInfo_.get(Flow.Keys.TYPE.toString()), config_);
        
        // register a single endpoint in the NMR endpoint registry for 
        // a Filter endpoint
        filterName_ = name;
        activateEndpoint(getServiceQName(filterName_), filterName_, false);
    }

    protected boolean useNew() { return (true); }
    protected boolean acceptEvent(int id) { return (false); }

    protected void onRequest(AbstractFlow.InboundContext ic) {
        MessageExchange     request = ic.getExchange();

        try
        {
            boolean filterMatches = filter_.matches(request);

            log_.fine("EIP:Filter (" + filterName_ + "," + request.getExchangeId() + ") Matched(" + filterMatches + ")");
            if (filterMatches) {
                request.setMessage(request.getMessage("in"), "out");
            }
            reply(ic);
        }
        catch (Exception ex)
        {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            ex.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP:Filter (" + filterName_ + "," + request.getExchangeId() + ") Exception: " + b.toString() + toString());

            // If there is an error in processing, set Exchange status as Error
            // and short-circuit it
            try {
                request.setError(ex);
                status(ic);
            } catch (javax.jbi.messaging.MessagingException mex) {
                log_.warning("EIP:Filter (" + filterName_ + ") ERROR-ERROR " + mex);
            }
        }
    }

    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Filter (" + filterName_ + "," + ic.getExchange().getExchangeId() + ") Complete");
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        Filter (" + filterName_ + ")\n");
        return (sb.toString());
    }

}
