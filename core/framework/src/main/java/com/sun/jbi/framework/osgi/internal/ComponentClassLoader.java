/**
 * 
 */
package com.sun.jbi.framework.osgi.internal;

import java.security.SecureClassLoader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This ClassLoader is used for extending the OSGI ModuleClassLoader based on
 * the environment in which the JBI runtime is running, if the JBI runtime is
 * running with GlassFish then the intention is make use of the GlassFish
 * ClassLoading capability
 * 
 * @author Sujit Biswas
 * 
 */
public class ComponentClassLoader extends SecureClassLoader {

    /**
     * holds the list of other ClassLoader, which may be required based on the
     * environment the JBI runtime is running, for example if JBI is running
     * within GlassFish and the requirement is to leverage the AppServer
     * capability then we need to extend the module ClassLoader capability,
     */
    private ArrayList<ClassLoader> extendedClassloaders = new ArrayList<ClassLoader>();

    /**
     * The parent of this ClassLoader is always expected to be the module
     * ClassLoader
     * 
     * @param parent
     */
    public ComponentClassLoader(ClassLoader parent) {
	super(parent);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.ClassLoader#findClass(java.lang.String)
     */
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
	boolean found = false;
	Class<?> classZ = null;

	for (Iterator<ClassLoader> cItr = extendedClassloaders.iterator(); cItr.hasNext();) {
	    try {
		ClassLoader nextCL = cItr.next();
		classZ = nextCL.loadClass(name);

		// class is found
		found = true;
		break;
	    } catch (ClassNotFoundException cnfe) {
		continue;
	    }
	}

	// class is not found
	if (!found) {
	    throw new ClassNotFoundException(name);
	}

	return classZ;
    }

    public void addClassLoader(ClassLoader classloader) {
	if (classloader == null) {
	    throw new IllegalArgumentException("null argument");
	}
	extendedClassloaders.add(classloader);

    }

}
