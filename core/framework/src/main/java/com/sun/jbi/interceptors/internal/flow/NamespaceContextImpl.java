/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NamespaceContextImpl.java - Last published on Apr 28, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyBundle;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Set;
import java.util.HashMap;

/**
 *
 * @author Sun Microsystems
 */
public class NamespaceContextImpl implements javax.xml.namespace.NamespaceContext {
    
    // This is a map of [ namespace URI | List of prefixes ]
    HashMap<String, List<String>> namespaces_;
    
    HashMap<String, List<String>>  globalNamespaces_;
    
    Logger log_ = Logger.getLogger(NamespaceContextImpl.class.getPackage().getName());
    
    public NamespaceContextImpl(String appNamespace, HashMap<String, List<String>> ns) {
        namespaces_ = ns;
        
        // The global namespaces defined for the application
        globalNamespaces_ = getGlobalNamespaces(appNamespace);
    }
    
    /**
     * Get Namespace URI bound to a prefix in the current scope.
     * 
     * @param prefix
     * @return
     */
    public String getNamespaceURI(String prefix){
        
        Set<String> nsURIs = getNamespaces().keySet();
        
        String namespaceURI = null;
        
        for( String nsURI : nsURIs ){
            List<String> prefixes = (List)getNamespaces().get(nsURI);
            if ( prefixes.contains(prefix)){
                namespaceURI = nsURI;
            }
        }
        
        // If not defined in the local NS look in the Global NS
        for( String nsURI : globalNamespaces_.keySet() ){
            List<String> prefixes = (List)globalNamespaces_.get(nsURI);
            if ( prefixes.contains(prefix)){
                namespaceURI = nsURI;
            }
        }
        
        return namespaceURI;
    }
    
    /**
     * Get prefix bound to Namespace URI in the current scope.
     * 
     * @param namespaceURI
     * @return the prefix bound to the namespaceURI
     */
    public String getPrefix(String namespaceURI){
        
        String prefix = null;
        
        Iterator itr = getPrefixes(namespaceURI);
        
        if ( itr.hasNext() )
        {
            prefix = (String)itr.next();
        }
        
        return prefix;
    }
      
    /**
     * Get all the prefixes bound to Namespace URI in the current scope.
     */
    public Iterator getPrefixes(String namespaceURI) {
        List<String> prefixes = (List)getNamespaces().get(namespaceURI);
        
        if (prefixes == null ){
            // Not defined in local NS, look in Global NS defined for the Application
            prefixes =(List)globalNamespaces_.get(namespaceURI);
        }
        
        return prefixes.iterator();
    }
     
    private HashMap getNamespaces(){
        if ( namespaces_ == null ){
            namespaces_ = new HashMap();
        }
            
        return namespaces_;
    }
    
    /**
     * Get the glabally defined namespace definitions from the app.properties
     * file
     */
    private HashMap<String, List<String>> getGlobalNamespaces(String appNamespace){
        
        HashMap<String, List<String>> ns = new HashMap<String, List<String>>();
        ServiceAssemblyManager saMgr = Environment.getServiceAssemblyManager();
        
        ServiceAssemblyBundle saBundle = saMgr.getServiceAssemblyForNamespace(appNamespace);
        
        if ( saBundle != null ){
            try{
                java.util.Map config = saMgr.readConfigurationFromSABundle(saBundle);
                if (config != null){
                    ns = ConfigurationUtil.getNamespaces(config);
                }
            } catch (Exception ex){
                log_.log(Level.WARNING, " Failed to read global namespace information", ex);
            }
        }
        return ns;
    }
}
