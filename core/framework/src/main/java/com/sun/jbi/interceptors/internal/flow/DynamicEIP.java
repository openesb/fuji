/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicEIP.java - Last published on May 8, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import java.util.List;

/**
 * Interface for dynamic eip configuration.
 * 
 * All EIP implementations which are dynamically configurable, will 
 * implement this interface.
 * 
 * @author Sun Microsystems
 */
public interface DynamicEIP {
    
     /**
      * Add to the existing configuration
      * @param config - configuration data
      */
     public void add(Object config) throws Exception;
     
     /** 
      * Remove the specified configuration
      * @param configId - configuration id
      */
     public void remove(String configId) throws Exception;
     
     /**
      * Update existing configuration 
      * @param config - configuration data
      */
     public void modify(Object config) throws Exception;
     
     /**
      * Generate a configuration template which can be used to add/modify
      * a configuration. The template is specific to each eip.
      * @return
      */
     public Object generateTemplate() throws Exception;
     
     /**
      * 
      * @param config
      * @return
      */
     public Object get(String config) throws Exception; 
     
     /**
      * List identifiers for all existing configuration
      */
     public List<String> listConfigs() throws Exception; 

}
