/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Channel.java
 *
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.service.ServiceConsumerImpl;
import org.osgi.framework.ServiceRegistration;
import com.sun.jbi.ext.SubChannel;
import com.sun.jbi.messaging.DeliveryChannelImpl;
import com.sun.jbi.messaging.MessageExchangeProxy;
import com.sun.jbi.messaging.ExchangePattern;
import com.sun.jbi.messaging.util.WSDLHelper;
import com.sun.jbi.framework.osgi.internal.Environment;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Map;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Used as a coordination point between EIP instances & a DeliveryChannel
 * @author Sun Microsystems, Inc.
 */
public class Channel
        implements SubChannel,
            java.lang.Runnable,
            javax.jbi.component.Component,
            com.sun.jbi.ext.ResolverExtension

{
    private DeliveryChannelImpl                  dc_;
    private Map<AbstractFlow,AbstractFlow>       flows_;
    private Map<ServiceEndpoint,AbstractFlow>    endpoints_;
    private Map<ServiceEndpoint,Map<String,Document>>        descriptions_;
    private Map<MessageExchange,AbstractFlow>    exchanges_;
    private Map<MessageExchange,Object>          contexts_;
    private Map<MessageExchange,MessageExchange> tracking_;
    private LinkedList<Event>                    events_;
    private HashMap<ServiceEndpoint, ServiceRegistration> osgiServices_;
    private Thread                               thread_;
    private boolean                              shutdown_;
    private Logger log_ = Logger.getLogger(Channel.class.getPackage().getName());

    Channel(DeliveryChannelImpl dc) {
        dc_ = dc;
        exchanges_ = new HashMap();
        contexts_ = new HashMap();
        endpoints_ = new HashMap();
        flows_ = new HashMap();
        osgiServices_ = new HashMap();
        tracking_ = new HashMap();
        events_ = new LinkedList();
        descriptions_ = new HashMap();
    }

    public void request(AbstractFlow.OutboundContext oc)
            throws javax.jbi.messaging.MessagingException {
        AbstractFlow        af = oc.getFlow();
        MessageExchange     me = oc.getExchange();

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.request("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Request" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Request\n" + me.toString());

        //
        //  Remember the request and perform a send.
        //
        try {
            contexts_.put(me, oc);
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                contexts_.remove(oc);
            }
        }
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: After Request\n" + toString());
   }

    public void reply(AbstractFlow.InboundContext ic)
            throws javax.jbi.messaging.MessagingException {
        AbstractFlow        af = ic.getFlow();
        MessageExchange     me = ic.getExchange();

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.reply("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Reply" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Reply\n" + me.toString());

        //
        //  Remember the request and perform a send.
        //
        try {
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                contexts_.remove(me);
            }
        }
    }

    public void status(AbstractFlow.InboundContext ic)
            throws javax.jbi.messaging.MessagingException {
        AbstractFlow        af = ic.getFlow();
        MessageExchange     me = ic.getExchange();

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.status("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Status" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Status" + me.toString());

        //
        //  Remember the request and perform a send.
        //
        try {
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                contexts_.remove(ic);
            }
        }
    }

    public void status(AbstractFlow.OutboundContext oc)
            throws javax.jbi.messaging.MessagingException {
        AbstractFlow        af = oc.getFlow();
        MessageExchange     me = oc.getExchange();

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.status("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Status" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Status" + me.toString());

        //
        //  Remember the request and perform a send.
        //
        try {
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                contexts_.remove(oc);
            }
        }
    }

    public void fault(AbstractFlow.InboundContext ic)
            throws javax.jbi.messaging.MessagingException {
        AbstractFlow        af = ic.getFlow();
        MessageExchange     me = ic.getExchange();

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.fault("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Fault" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Fault" + me.toString());

        //
        //  Remember the request and perform a send.
        //
        try {
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                contexts_.remove(ic);
            }
        }
    }

    public void fault(AbstractFlow.OutboundContext oc)
            throws javax.jbi.messaging.MessagingException {
        AbstractFlow        af = oc.getFlow();
        MessageExchange     me = oc.getExchange();

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.fault("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Fault" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Fault\n" + me.toString());

        //
        //  Remember the request and perform a send.
        //
        try {
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                contexts_.remove(oc);
            }
        }
    }


    public void send(AbstractFlow af, MessageExchange me)
        throws javax.jbi.messaging.MessagingException {
        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.send("+ af.getClass().getSimpleName() +"," + me.getExchangeId() + "," + me.getStatus() + ")");
        if (log_.isLoggable(Level.FINER))
            log_.finer("Channel: Before Send" + toString());
        if (log_.isLoggable(Level.FINEST))
            log_.finest("Channel: Exchange Before Send\n" + me.toString());
        
        //
        //  Remember the request and perform a send.
        //
        try {
            exchanges_.put(me, af);
            dc_.send(me);
        } finally {
            //
            //  If get get here normally or via an exception, see if we need to
            //  cleanup the exchange tracking.
            //
            if (!dc_.isActive(me)) {
                exchanges_.remove(me);
            }
        }
   }

    ServiceEndpoint activateEndpoint(AbstractFlow af, QName service, String endpoint, boolean includeOSGi)
        throws javax.jbi.messaging.MessagingException {
        ServiceEndpoint se;

        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.activateEndpoint("+ af.getClass().getSimpleName() +") " + service + "," + endpoint);
        endpoints_.put(se = dc_.activateEndpoint(service, endpoint), af);

        //
        //  Create the service description for this endpoint.
        //
        try {
            Map<QName,List<QName>>  interfaces = new HashMap();
            Map<QName,String>       operations = new HashMap();
            List<QName>             opnames = new LinkedList();
            Document                doc;
            String                  ns = service.getNamespaceURI();
            HashMap                 docs = new HashMap();

            opnames.add(new QName(ns, "oneWay"));
            opnames.add(new QName(ns, "requestReply"));
            operations.put(new QName(ns, "oneWay"), ExchangePattern.IN_ONLY.toString());
            operations.put(new QName(ns, "requestReply"), ExchangePattern.IN_OUT.toString());
            interfaces.put(new QName(ns, "interface_1"), opnames);

            doc = WSDLHelper.buildBasicWSDL(WSDLHelper.WSDL_11, ns,
                service.getLocalPart(), endpoint, interfaces, operations);
            docs.put("standalone.wsdl", doc);
            doc = WSDLHelper.buildBindingWSDL(WSDLHelper.WSDL_11, ns,
                service.getLocalPart(), endpoint, interfaces, operations);
            docs.put("binding.wsdl", doc);
            doc = WSDLHelper.buildInterfaceWSDL(WSDLHelper.WSDL_11, ns,
                service.getLocalPart(), endpoint, interfaces, operations);
            docs.put("interface.wsdl", new Resolver(doc));
            descriptions_.put(se, docs);
        } catch (Exception e ) {

        }
        //
        // See if we should make this endpoint accessible as an OSGi service.
        //
        if (includeOSGi) {
            try {
                // register a service channel for the endpoint within the OSGi registry
                ServiceConsumerImpl sc = new ServiceConsumerImpl(dc_, se);
                ServiceRegistration sr =
                        Environment.getFrameworkBundleContext().registerService(
                        "org.glassfish.openesb.api.service.ServiceConsumer", sc, sc.getProperties());
                osgiServices_.put(se, sr);
            }
            catch (Exception ex) {
                throw new javax.jbi.messaging.MessagingException(ex);
            }
        }
        return (se);
    }

    void deactivateEndpoint(AbstractFlow af, ServiceEndpoint se)
            throws javax.jbi.messaging.MessagingException {
        if (log_.isLoggable(Level.FINE))
            log_.fine("Channel.deactivateEndpoint("+ af.getClass().getSimpleName() +") " + se.getServiceName()  + "," + se.getEndpointName());
        endpoints_.remove(se);
        descriptions_.remove(se);
        if (osgiServices_.containsKey(se)) {
            osgiServices_.remove(se).unregister();
        }
        dc_.deactivateEndpoint(se);
    }

    synchronized void addFlow(AbstractFlow af) {
        if (flows_.isEmpty())
        {
            thread_ = new Thread(this);
            thread_.setDaemon(true);
            thread_.setName("EIP-" + af.getClass().getSimpleName() + "-Acceptor");
            shutdown_ = false;
            thread_.start();
        }
        flows_.put(af,af);
    }

    synchronized void removeFlow(AbstractFlow af) {
        flows_.remove(af);
        if (flows_.isEmpty()) {
            shutdown_ = true;
            thread_.interrupt();
            thread_ = null;
        }
    }

    public void run() {
        MessageExchange                 me;
        AbstractFlow                    af;
        AbstractFlow.InboundContext     ic;
        AbstractFlow.OutboundContext    oc;
        Object                          o;

        for (;;) {
            try {
                //
                //  Handle any EIP Events before dealing with MessageExchanges.
                //
                while (!events_.isEmpty()) {
                    Event e = events_.removeFirst();
                    o = contexts_.get(e.me_);
                    if (o instanceof AbstractFlow.OutboundContext) {
                        oc = (AbstractFlow.OutboundContext)o;
                        if (e.id_ == com.sun.jbi.ext.DeliveryChannel.EVENT_SUSPEND) {
                            doSuspend(oc);
                        } else if (e.id_ == com.sun.jbi.ext.DeliveryChannel.EVENT_RESUME) {
                            doResume(oc);
                        }
                    } else if (o instanceof AbstractFlow.InboundContext) {
                        ic = (AbstractFlow.InboundContext)o;
                        if (e.id_ == com.sun.jbi.ext.DeliveryChannel.EVENT_SIGNAL) {
                            doSignal(ic);
                        }
                    }
                }

                //
                //  Make sure we get a real MessageExchange. Various reasons can result in
                //  an empty message.
                //
                me = dc_.accept();
                if (me == null) {
                    continue;
                }

                //
                //  If we know nothing about this message it must be a new inbound
                //  request. Otherwise, use the Context type to determine if this is
                //  inbound or outbound.
                //
                o = contexts_.get(me);
                if (o == null) {
                    af = endpoints_.get(me.getEndpoint());
                    contexts_.put(me, ic = af.newInboundContext(me));
                    doRequest(ic);
                } else if (o instanceof AbstractFlow.OutboundContext) {
                    oc = (AbstractFlow.OutboundContext)o;
                    if (me.getStatus().equals(ExchangeStatus.ACTIVE)) {
                        if (me.getFault() != null) {
                            doOutFault(oc);
                        } else {
                            doReply(oc);
                        }
                    } else {
                        doOutStatus(oc);
                    }
                 } else {
                    ic = (AbstractFlow.InboundContext)o;
                    if (me.getStatus().equals(ExchangeStatus.ACTIVE)) {
                        if (me.getFault() != null) {
                            doInFault(ic);
                        } else {
                            ;
                        }
                    } else {
                        doInStatus(ic);
                    }
                }
                if (!dc_.isActive(me)) {
                    contexts_.remove(me);
                }
            } catch (Exception e) {
                //
                //  We get an interrupted exception for shutdown cases.
                //  Kill the chattyness for this common case.
                //
                boolean interrupted = !(e instanceof java.lang.InterruptedException);

                if (!interrupted || (interrupted && !shutdown_)) {
                    log_.warning("EIP Thread(" + Thread.currentThread().getName() + ") Got Exception:" + getTraceback(e) + toString());
                }
                if (interrupted && shutdown_) {
                    break;
                }
            }
        }
    }

    private void doSignal(AbstractFlow.InboundContext ic)
            throws ChannelException {
        try {
            ic.getFlow().onSignal(ic);
        } catch (Exception e) {
            log_.warning("onSignal() for Flow: " + ic.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

    private void doResume(AbstractFlow.OutboundContext oc)
            throws ChannelException {
        try {
            oc.getFlow().onResume(oc);
        } catch (Exception e) {
            log_.warning("onResume() for Flow: " + oc.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

    private void doSuspend(AbstractFlow.OutboundContext oc)
            throws ChannelException {
        try {
            oc.getFlow().onSuspend(oc);
        } catch (Exception e) {
            log_.warning("onSuspend() for Flow: " + oc.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }


    private void doRequest(AbstractFlow.InboundContext ic)
            throws ChannelException {
        try {
            ic.getFlow().onRequest(ic);
        } catch (Exception e) {
            log_.warning("onRequest() for Flow: " + ic.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

   private void doReply(AbstractFlow.OutboundContext oc)
            throws ChannelException {
        try {
            oc.getFlow().onReply(oc);
        } catch (Exception e) {
            log_.warning("onReply() for Flow: " + oc.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

   private void doInFault(AbstractFlow.InboundContext ic)
            throws ChannelException {
        try {
            ic.getFlow().onFault(ic);
        } catch (Exception e) {
            log_.warning("onFault() for Flow: " + ic.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

   private void doOutFault(AbstractFlow.OutboundContext oc)
            throws ChannelException {
        try {
            oc.getFlow().onFault(oc);
        } catch (Exception e) {
            log_.warning("onFault() for Flow: " + oc.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

   private void doInStatus(AbstractFlow.InboundContext ic)
            throws ChannelException {
        try {
            ic.getFlow().onStatus(ic);
        } catch (Exception e) {
            log_.warning("onStatus() for Flow: " + ic.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

   private void doOutStatus(AbstractFlow.OutboundContext oc)
            throws ChannelException {
        try {
            oc.getFlow().onStatus(oc);
        } catch (Exception e) {
            log_.warning("onStatus() for Flow: " + oc.getFlow().getName() + " Failed: " + getTraceback(e));
        }
    }

   private String getTraceback(Exception e) {
        ByteArrayOutputStream   b = new ByteArrayOutputStream();
        PrintStream             ps = new PrintStream(b);
        e.printStackTrace(ps);
        ps.flush();
        return (b.toString());
   }

    public String getChannelId(MessageExchange me)
    {
        AbstractFlow    af = (AbstractFlow)exchanges_.get(me);
        String          result = null;

        if (af == null)
        {
            if (me.getRole().equals(MessageExchange.Role.CONSUMER)) {
                af = endpoints_.get(((MessageExchangeProxy)me).getEndpoint());
            } else {
                af = endpoints_.get(((MessageExchangeProxy)me).getActualEndpoint());
            }
        }
        if (af != null)
        {
            result = af.getName().toString();
        }
        return (result);
    }

    public javax.jbi.component.Component getComponent() {
        return (this);
    }

    public boolean hasEvents() {
        return (!events_.isEmpty());
    }

    public void suspend(MessageExchange me) {
        dc_.suspend(me);
    }

    public void resume(MessageExchange me) {
        dc_.resume(me);
    }

    public void signal(MessageExchange me) {
        MessageExchange child = tracking_.remove(me);

        if (child != null) {
            dc_.signal(child);
        }
    }

    public boolean onEvent(MessageExchange me, int id, Object arg) {
        Object o = contexts_.get(me);
        if (o instanceof AbstractFlow.OutboundContext) {
            AbstractFlow.OutboundContext oc = (AbstractFlow.OutboundContext)o;
            if (oc.getFlow().acceptEvent(id)) {
                if (id == com.sun.jbi.ext.DeliveryChannel.EVENT_SUSPEND) {
                    tracking_.put(me, (MessageExchange)arg);
                }
                events_.addLast(new Event(id, me, arg));
                return (true);
            }
        } else if (o instanceof AbstractFlow.InboundContext) {
            AbstractFlow.InboundContext ic = (AbstractFlow.InboundContext)o;
            if (ic.getFlow().acceptEvent(id)) {
                events_.addLast(new Event(id, me, arg));
                return (true);
            }
        }
        return (false);
    }

    // ------------------- javax.jbi.component.Component ----------------------

    public javax.jbi.component.ComponentLifeCycle getLifeCycle() { return (null); }
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager() { return (null); }
    public boolean isExchangeWithConsumerOkay(ServiceEndpoint se, MessageExchange me) { return (true); }
    public boolean isExchangeWithProviderOkay(ServiceEndpoint se, MessageExchange me) { return (true); }
    public ServiceEndpoint resolveEndpointReference(org.w3c.dom.DocumentFragment df) { return (null); }
    public org.w3c.dom.Document getServiceDescription(ServiceEndpoint se) {
        return (descriptions_.get(se).get("binding.wsdl"));
    }
    public EntityResolver   getServiceDescriptionResolver(ServiceEndpoint se) {
        return ((EntityResolver)descriptions_.get(se).get("interface.wsdl")); }

   // ------------------- java.lang.Object ----------------------

    public String toString() {
        StringBuffer    sb = new StringBuffer();

        if (endpoints_.size() != 0 || exchanges_.size() != 0) {
            sb.append("    EIP Channel Info\n");
            for (Map.Entry<ServiceEndpoint, AbstractFlow> e : endpoints_.entrySet()) {
                sb.append("      Flow (" + e.getValue().getName() + ")\n");
                sb.append("        Endpoint: " + e.getKey().getServiceName() + "," + e.getKey().getEndpointName() + "\n");
            }
            for (Map.Entry<MessageExchange, Object> c : contexts_.entrySet()) {
                sb.append("      Exchange: " + c.getKey().getExchangeId() + (dc_.isActive(c.getKey()) ? " " : "?") + "\n");
                sb.append(c.getValue().toString());
            }
            for (Event e : events_) {
                sb.append("    Event Type:" + (e.id_ == 1 ? "Suspend" :
                    (e.id_ == 2 ? "Resume" : "Signal")) + "\n");
                sb.append("        ExchangeId: " + e.me_.getExchangeId() + "\n");
                if (e.child_ != null) {
                    sb.append("        Child:      " +
                            ((MessageExchange)e.child_).getExchangeId());
                }
            }
            if (!flows_.isEmpty()) {
                sb.append("      Details:\n");
                for (AbstractFlow af : flows_.keySet()) {
                    sb.append(af.toString());
                }
            }
        }
        return (sb.toString());

    }

    class Event
    {
        final int             id_;
        final MessageExchange me_;
        final MessageExchange child_;

        Event(int id, MessageExchange me, Object child)
        {
            id_ = id;
            me_ = me;
            child_ = (MessageExchange)child;
        }
    }

    class Resolver implements EntityResolver {
        String    entity_;

        public Resolver(Document entity) {
           try
           {
               TransformerFactory tf = TransformerFactory.newInstance();
               Transformer transformer = tf.newTransformer();
               StringWriter sw = new StringWriter();
               transformer.transform(new DOMSource(entity.getDocumentElement()), new StreamResult(sw));
               entity_ = sw.toString();
           } catch (Exception ignore) {
                entity = null;
           }
        }

        // ------------------- org.xml.sax.EntityResolver -------------------------

        public InputSource resolveEntity (String publicId, String systemId)
        {
            if (systemId.equals("interface.wsdl")) {
                return (new InputSource(new ByteArrayInputStream(entity_.getBytes())));
            }
            return (null);
        }

        String getEntity() {
            return (entity_);
        }
    }
}