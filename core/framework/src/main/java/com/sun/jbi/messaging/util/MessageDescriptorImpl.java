/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageDescriptorImpl.java -
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */


package com.sun.jbi.messaging.util;

import com.sun.jbi.ext.MessageDescriptor;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import javax.jbi.messaging.NormalizedMessage;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author derek
 */
public class MessageDescriptorImpl implements MessageDescriptor {
    HashMap<String,String>      mProperties;
    String                      mMessage;
    HashMap<String,String>      mAttachmentInfo;
    HashMap<String,String>      mAttachmentContent;

    public MessageDescriptorImpl(NormalizedMessage nm)
    {
        Source      content;

        mProperties = new HashMap();
        for (Object n : nm.getPropertyNames())
        {
            String  name = (String)n;
            Object  v = nm.getProperty(name);
            String  value = null;

            if (v != null)
            {
                if (v instanceof String)
                    value = (String)v;
                else
                    value = v.toString();
                mProperties.put(name, value);
            }
        }
        mAttachmentInfo = new HashMap();
        mAttachmentContent = new HashMap();
        for (Object n : nm.getAttachmentNames())
        {
            String          name = (String)n;
            DataHandler     v = nm.getAttachment(name);
            String          type = "";;
            String          value = "";

            if (v != null)
            {
                Object  c;

                type = v.getContentType();
                if (type.startsWith("text/") |
                        type.startsWith("application/xml") |
                        type.startsWith("application/json"))
                {
                    try
                    {
                        c = v.getContent();
                    }
                    catch (Exception ignore)
                    {
                        c = null;
                    }
                    if (c instanceof String)
                    {
                        value = (String)c;
                    }
                    if (c instanceof InputStream)
                    {
                        InputStreamReader   isr = new InputStreamReader((InputStream)c);
                        char[]              buffer = new char[128];
                        int                 count;
                        StringBuffer        sb = new StringBuffer();
                        try
                        {
                            while ((count = isr.read(buffer)) > 0)
                            {
                                sb.append(buffer, 0, count);
                            }
                            value = sb.toString();
                        }
                        catch (java.io.IOException ignore)
                        {
                            value = sb.toString();
                        }
                    }
                }
            }
            mAttachmentInfo.put(name, type);
            mAttachmentContent.put(name, value);
        }
        content = nm.getContent();
        if (content != null)
        {
            if (content instanceof StreamSource)
            {
                if (!(content instanceof ReusableStreamSource))
                {
                    try
                    {
                         nm.setContent(new ReusableStreamSource((StreamSource)content));
                    }
                    catch (javax.jbi.messaging.MessagingException ignore)
                    {

                    }
                }
                if (mProperties.get("com.sun.jbi.fuji.objectPayload") == null)
                {
                   mMessage = ((ReusableStreamSource)nm.getContent()).getString();
                }
                else
                {
                    try
                    {
                        ObjectInputStream   ois;
                        Object              o;
                        ois = new ObjectInputStream(
                            ((ReusableStreamSource)nm.getContent()).getInputStream());
                        o = ois.readObject();
                        if (o instanceof String)
                            mMessage = (String)o;
                    } catch (Exception ignore)
                    {
                    }
               }
            }
            else
            {
                mMessage = stringSource(content);
            }
        }
    }
    
    public String getMessage()
    {
        return (mMessage);
    }

    public HashMap<String,String> getProperties()
    {
        return (mProperties);
    }

    public HashMap<String,String> getAttachmentInfo()
    {
        return (mAttachmentInfo);
    }

    public HashMap<String,String> getAttachmentContent()
    {
        return (mAttachmentContent);
    }

    /**
     * Helper method to debug, will be deleted
     *
     * @param src - Source to print contents of
     * @return string representation of the XML source
     * @throws java.lang.Exception
     */
    private String stringSource(Source src)
    {
        try
        {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty (OutputKeys.METHOD, "xml");
            transformer.setOutputProperty (OutputKeys.INDENT, "yes");
            StringWriter sw = new StringWriter();
            transformer.transform(src, new StreamResult(sw));
            return sw.toString();
        }
        catch (Exception e)
        {
            return ("{" + e.getLocalizedMessage() + "}");
        }
    }

}
