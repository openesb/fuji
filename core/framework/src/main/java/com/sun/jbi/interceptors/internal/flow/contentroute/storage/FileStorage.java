/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileStorage.java
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.contentroute.storage;

import com.sun.jbi.interceptors.internal.flow.contentroute.Storage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import org.ho.yaml.Yaml;

/**
 *
 * @author Sun Microsystems
 */
public class FileStorage implements Storage {
    File            file_;

    public FileStorage(File directory) throws java.io.IOException {
        file_ = directory;
        if (!file_.isDirectory()) {
            throw new java.io.FileNotFoundException(file_.getCanonicalPath());
        }
    }

    public void createName(String name, String contents)
            throws java.io.IOException {
        File    file = new File(file_.getCanonicalPath(), name + ".yml");

        if (!file.exists()) {
            FileOutputStream    fs = null;
            PrintStream         ps = null;

            try {
                fs = new FileOutputStream(file);
                ps = new PrintStream(fs);
                ps.print(contents);
            } finally {
                if (ps != null) {
                    ps.close();
                }
                if (fs != null) {
                    fs.close();
                }
            }
        }
    }

    public String[] listNames() {
        List<String>    list = new LinkedList();

        for (File f : file_.listFiles()) {
            if (f.getName().endsWith(".yml")) {
                list.add(f.getName().substring(0, f.getName().length() - 4));
            }
        }
        return (list.toArray(new String[0]));
    }

    public void removeName(String fileName) throws java.io.IOException {
        File    file = new File(file_.getCanonicalPath(), fileName + ".yml");

        if (!file.delete()) {
            throw new java.io.IOException("Can't delete file: " + file.getAbsolutePath());
        }
    }

    public void addName(String fileName, Object content) throws java.io.IOException {
        File                file = new File(file_.getCanonicalPath(), fileName + ".yml");
        FileOutputStream    fs = new FileOutputStream(file);

        Yaml.dump(content, fs, true);
        fs.close();
    }

    public Object getName(String fileName) throws java.io.IOException {
        File    file = new File(file_.getCanonicalPath(), fileName + ".yml");
        FileInputStream    fs = new FileInputStream(file);
        Object              result;


        result = Yaml.load(fs);
        fs.close();
        return (result);
    }

    public boolean existsName(String fileName) {
        try {
          File    file = new File(file_.getCanonicalPath(), fileName + ".yml");

          return (file.exists());
        } catch (java.io.IOException ignore) {
            return (false);
        }
    }

}
