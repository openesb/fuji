/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BundleUtil.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.osgi.framework.Bundle;

/**
 * Kitchen sink for bundle-related utilities.
 * @author kcbabo
 */
public class BundleUtil {
    
    private static Logger logger_ = Logger.getLogger(
            BundleUtil.class.getPackage().getName());
    
    /**
     * Looks for jbi.xml in the specified bundle.
     * @param bundle the bundle to check
     * @return jbi.xml if present in bundle, otherwise null.
     */
    public static Jbi getJbiXml(Bundle bundle) {
        Jbi jbiXml = null;
        Enumeration e = bundle.findEntries("META-INF", "jbi.xml", false);
        
        try {
            if (e != null && e.hasMoreElements()) {
                jbiXml = Jbi.newJbi(((URL)e.nextElement()).openStream());
            }
        } catch (Exception ex) {
            logger_.log(Level.INFO, "Failed to parse jbi.xml", ex);
        }
        
        return jbiXml;
    }
    
    /**
     * Blows up a bundle and sweeps the pieces into a directory of your choice.
     * @param bundle bundle to explode
     * @param destDir where you want the pieces
     * @throws java.lang.Exception
     */
    public static void explodeContents(Bundle bundle, File destDir) 
            throws Exception {
        explodeBundlePath(bundle, destDir, "/");
    }
    
    /**
     * Blows up a bundle and sweeps the pieces into a directory of your choice.
     * @param bundle bundle to explode
     * @param entryName entry inside the bundle you want to explode
     * @param destDir where you want the pieces
     * @param isZip true if we're pull out a zip, false otherwise
     * @throws java.lang.Exception
     */
    public static void explodeContents(Bundle bundle, String entryName, 
            File destDir, boolean isZip) throws Exception {
        URL url = bundle.getEntry(entryName);
        
        if (url == null) {
            return;
        }
        
        InputStream is = url.openStream();
        
        try {
            if (isZip) {
                writeZipStream(is, destDir);
            }
            else {
                writeEntry(is, entryName, destDir);
            }
        }
        finally {
            if (is != null) {
                try { is.close(); } catch (Exception ex) {}
            }
        }
    }
        
    /**
     * Given a bundle determine if it has any classes which implement MessageInterceptor,
     * maintain a list of such classes and return it.
     * 
     * @param iface - interface to check for
     * @param bunlde - the OSGi bundle to introspect
     * @return a list of classes from the Bundle that support message interceptors
     */
    public static java.util.List<Class> getClassesForInterface(Class iface, Bundle bundle)
    {
        
        // Get all the .class entries in the bundle
        java.util.Enumeration<java.net.URL> entries = bundle.findEntries("/", "*.class", true);
        java.util.List<Class> classes = new java.util.ArrayList();   

        if ( entries != null )
        {
            while ( entries.hasMoreElements() )
            {
                java.net.URL entry = entries.nextElement();
                String path = entry.getPath();
                int startIndex = ( path.startsWith("/") ? 1 : 0 );
                String className =  path.substring(startIndex, path.indexOf(".class"));
                className = className.replace("/", ".");
                try
                {
                    Class clazz = bundle.loadClass(className);
                    if ( implementsInterface(clazz, iface) )
                    {
                        classes.add(clazz);
                    }
                }
                catch ( ClassNotFoundException cnf )
                {
                    continue;
                }
            }
        }
        
        return classes;
    }
    
    /**
     * @return a list of all the classes in the Bundle which have interceptor
     *         methods. If there aren't any such classes an empty list is returned.
     */
    public static java.util.List<Class> getClassesWithInterceptAnnotations(Bundle bundle)
    {
        // Get all the .class entries in the bundle
        java.util.Enumeration<java.net.URL> entries = bundle.findEntries("/", "*.class", true);
        java.util.List<Class> classes = new java.util.ArrayList();   

        if ( entries != null )
        {
            while ( entries.hasMoreElements() )
            {
                java.net.URL entry = entries.nextElement();
                String path = entry.getPath();
                int startIndex = ( path.startsWith("/") ? 1 : 0 );
                String className =  path.substring(startIndex, path.indexOf(".class"));
                className = className.replace("/", ".");
                
                // -- If the class name starts with java or javax we can skip it,
                //    since it is not a user defined class.
                if (!( className.startsWith("java") || className.startsWith("javax")))
                {
                    try
                    {
                        Class clazz = bundle.loadClass(className);
                        /**
                         * If the class implements the com.sun.jbi.interceptors.Interceptor 
                         * interface, it means that the bundle will register the class itself
                         * as a service.
                         */
                        Class iface = com.sun.jbi.interceptors.Interceptor.class;

                        if ( !implementsInterface(clazz, iface) )
                        {
                            java.lang.reflect.Method[] all = clazz.getDeclaredMethods();

                            for(java.lang.reflect.Method method : all )
                            {
                                if ( method.isAnnotationPresent(com.sun.jbi.interceptors.Intercept.class) )
                                {
                                    classes.add(clazz);
                                    // Just one method is sufficient, continue with the rest
                                    break;
                                }
                            }
                        }
                    }
                    catch ( ClassNotFoundException cnf )
                    {
                        continue;
                    }
                }
            }
        }
        
        return classes;
    }
    
    /**
     * Explodes the specified bundle path into the destination directory.  This
     * method recurses subpaths, so keep in mind that all child files/directories
     * of the path will be written.
     * @param bundle
     * @param destDir
     * @param path
     * @throws java.lang.Exception
     */
    public static void explodeBundlePath(Bundle bundle, File destDir, String path) 
                throws Exception {
        Enumeration<String> entries = bundle.getEntryPaths(path);
        while (entries != null && entries.hasMoreElements()) {
            String entry = entries.nextElement();
            if (entry.endsWith("/")) {
                new File(destDir, entry).mkdirs();
            }
            else {
                writeEntry(bundle.getEntry(entry).openStream(), entry, destDir);
            }
            explodeBundlePath(bundle, destDir, entry);
        }
    }
    
    /**
     * Determine if a class implements a specific interface.
     *
     * @param clazz - class object to check
     * @param iface - interface which is to be checked for 
     * @return true if clazz implements interface iface
     */
    public static boolean implementsInterface(Class clazz, Class iface)
    {
        boolean isMsgInterceptor = false;
        if ( !clazz.isInterface() )
        {
            Class[] interfaces = clazz.getInterfaces();
            // check if MessageInterceptor is one of these
            for ( Class interfaze : interfaces )
            {
                if ( interfaze.equals(iface))
                {
                    isMsgInterceptor = true;
                }
            }
            
            if ( !isMsgInterceptor )
            {
                // -- Check to see if any of the super classes implement the 
                //    the Interceptor interface
                Class parent = clazz.getSuperclass();

                if ( parent != null )
                {
                    isMsgInterceptor = implementsInterface(parent, iface);
                }
            }
        }
        return isMsgInterceptor;
    }
    
    /** Extracts an entire zip contained in the input stream to the specified
     * directory.
     * @param input
     * @param destDir
     * @throws java.lang.Exception
     */
    private static void writeZipStream(InputStream input, File destDir)
            throws Exception {
        
        ZipEntry entry;
        ZipInputStream zis = new ZipInputStream(input);
        try {
            while ((entry = zis.getNextEntry()) != null) {
                if (entry.isDirectory()) {
                    new File(destDir, entry.getName()).mkdirs();
                }
                else {
                    writeEntry(zis, entry.getName(), destDir);
                }
            }
        }
        finally {
            if (zis != null) {
                try { zis.close(); } catch (Exception ex) {}
            }
        }
    }
    
    /** Utility method used to read content from an stream and write it to 
     *  an entry path.
     */
    private static void writeEntry(InputStream is, String path, File destDir) 
        throws Exception {
        
        FileOutputStream fos = null;
        File f = new File(destDir, path);
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        
        try {
            fos = new FileOutputStream(f);
            byte[] buf = new byte[4096];
            int count;
            while ((count = is.read(buf)) != -1) {
                fos.write(buf, 0, count);
            }

            fos.flush();
        }
        finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (Exception ex) {}
        }
    }
}
