/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LoggingService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.HashMap;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Enumeration;


import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.framework.ServiceReference;

import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.StringTranslator;

/**
 * The LoggingService is used to track the list of configurable loggers. Configurable here means
 * the ability to control the log level of a logger independent of its parent logger's level. 
 *  
 * All runtime loggers are tracked by a subclass called RuntimeLoggingService.
 * Each component's loggers are tracked by an instance of a subclass called ComponentLoggingService.
 * 
 * The actual interface to the admin tools for log level configuration is a LoggerConfigurationService.
 * 
 * All runtime loggers can be configured by updating the configuration of LoggerConfigurationService 
 * that is registered with the PID "com.sun.jbi.configuration:runtime-logger".
 * 
 * For each component an instance of LoggerConfigurationService is registered to configure its loggers,
 * with the PID, "com.sun.jbi.configuration:<component-name>-logger"
 * 
 * When a new configurable logger is added, this class updates the configuration of a corresponding 
 * LoggerConfigurationService. This sync is done to ensure that the admin tools will be able to verify 
 * the user provided logger name when the user tries to modify the log level of a logger. 
 */
public class LoggingService {
    
    
    /**
     * list of tracked loggers 
     */
    private HashMap<String, String> loggers_ = new HashMap<String, String>();
    
    /** 
     * default logger level. Null cannot be stored in a service's configuration, so use 
     * the default level in the dictionary. 
     */
    private static final String DEFAULT_LOG_LEVEL = "DEFAULT";

    /**
     * jbi root logger name
     */
    public static final String JBI_ROOT_LOGGER_NAME = "com.sun.jbi";
  
    /**
     * jbi root logger level
     */
    public static final Level JBI_ROOT_LOGGER_LEVEL = Level.INFO;

   /**
     * key for the property containing actual log levels
     */
    public static final String LOGGERS_KEY = "loggers";
    
    /**
     * equals - used to separate the logger name and value in the vector
     */
    public static String EQUALS = "=";
    
    /**
     * separator between the domain, name and type
     */
    public static String SEPARATOR = ".";

    /** 
     * logger
     */ 
    protected static Logger log_;

    /**
     * translator
     */
    private StringTranslator translator_ = new StringTranslator(
                LoggingService.class.getPackage().getName(), null); 

    /** 
     * service PID of loggerconfigurationservice 
     * When a new logger is added, this logging service will update the
     * corresponding  loggerconfigurationservice.
     */
    private String loggerConfigServicePID_;
    
    /**
     * Constructor
     * This constructor is used to initialize the loggingservice.
     * This service will track the list of loggers and update a logger configuration service 
     * when a new logger is added.
     * @param logLevelConfigServicePID the service that should be updated when a new 
     * logger is added.
     */
    LoggingService(String loggerConfigServicePID) {
        loggerConfigServicePID_ = loggerConfigServicePID;
        log_ = Logger.getLogger(LoggingService.class.getPackage().getName());
    }
    
            
    /**
     * This method is used to get a logger. This method adds the provided logger 
     * name to the list of tracked loggers, updates the configuration of the log level
     * configuration service and returns the instance of logger from JDK.
     * @param loggerName the logger name
     */
    public Logger getLogger(String loggerName) {

        loggers_.put(loggerName, DEFAULT_LOG_LEVEL);
        updateLogConfigurationService();
        return Logger.getLogger(loggerName);
    }
    
    /**
     * This method is used to get a logger. This method adds the provided logger 
     * name to the list of tracked loggers, updates the configuration of the log level
     * configuration service and returns the instance of logger from JDK.
     * @param loggerName the logger name
     * @param resourceBundleName  the resource bundle name
     */
    public Logger getLogger(String loggerName, String resourceBundleName) {
        loggers_.put(loggerName, DEFAULT_LOG_LEVEL);
        updateLogConfigurationService();
        return Logger.getLogger(loggerName);      
    }
        
    /**
     * This method is used to get a logger. This method adds the provided logger 
     * name to the list of tracked loggers, updates the configuration of the log level
     * configuration service and returns the instance of logger from JDK.
     * @param loggerName the logger name
     * @param resourceBundleName  the resource bundle name
     * @param logLevel log level
     */
    public Logger getLogger(String loggerName, String resourceBundleName, Level logLevel) {
        
        loggers_.put(loggerName, logLevel.toString());
        updateLogConfigurationService();
        return Logger.getLogger(loggerName);
    }

    /**
     * This method is used to initialize the list of loggers maintained by this service
     * @loggers logLevel log level
     */
    public void initializeLoggerList(Vector<String> loggers) {

        if (loggers != null && loggers.capacity() > 0) {
            Enumeration elements = loggers.elements();
            String loggerName = null;
            String loggerValue = null;
            while(elements.hasMoreElements()) {
                try {
                    String value = (String)elements.nextElement();
                    loggerName = value.substring(0, value.indexOf(LoggingService.EQUALS));
                    loggerValue = value.substring(value.indexOf(LoggingService.EQUALS) + 1);
                    loggers_.put(loggerName, loggerValue);
                } catch (Exception ex) {
                    String msg = translator_.getString(
                            LocalStringKeys.FAILED_LOGGER_LEVEL_CONFIGURATION,
                            new Object[] { loggerName, loggerValue } );

                    log_.log(Level.WARNING, msg, ex);
                }
            }
        }

    }

    /**
     * This method is used to update the logLevelConfigurationService
     */
    private void updateLogConfigurationService() {
        
        try {
            ConfigurationAdmin configAdmin;

            if ((configAdmin = getConfigurationAdminService()) != null) {
                      
                Configuration config = configAdmin.getConfiguration(  
                            loggerConfigServicePID_, 
                            null);  

                if (config != null) {
                    Dictionary properties = config.getProperties();  
                    if (properties == null) {  
                        properties = new Hashtable();
                    }  
                    
                    Vector<String> logLevels = new Vector<String>();
                    for (String loggerName : loggers_.keySet()) {
                        logLevels.add(loggerName + EQUALS + loggers_.get(loggerName));  
                    }
                    properties.put(LOGGERS_KEY, logLevels);
                    config.update(properties);
                }
             }
            
         } catch (Exception ex) {
            log_.log(Level.WARNING, translator_.getString(LocalStringKeys.CONFIG_UPDATE_FAILED), ex);
         }
    }
    
    /**
     * This method is used to obtain the list of loggers tracked by the logging service.
     * @return HashMap the list of loggers tracked
     */
    public HashMap<String, String> getLoggers() {
        return loggers_;
    }

    /**
     * This method is used to get a reference to the config admin service
     */
    private ConfigurationAdmin getConfigurationAdminService() {
        
        ConfigurationAdmin configAdmin = null;
        if (Environment.getFrameworkBundleContext() != null) {
            ServiceReference  configAdminRef =
               Environment.getFrameworkBundleContext().getServiceReference(ConfigurationAdmin.class.getName());
 
            if (configAdminRef != null) {
                configAdmin = (ConfigurationAdmin)  
                         Environment.getFrameworkBundleContext().getService(configAdminRef);  

            }
        }
        return configAdmin;
    }

    
}
