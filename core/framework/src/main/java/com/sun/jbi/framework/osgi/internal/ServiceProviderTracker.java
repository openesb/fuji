/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceProviderTracker.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import org.glassfish.openesb.api.service.ServiceProvider;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;
import org.glassfish.openesb.api.service.ServiceProps;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * This class tracks registrations of the ServiceProvider interface in the 
 * OSGi service registry.  All ServiceProvider instances are made available
 * as addressable endpoints on the NMR.
 * @author kcbabo
 */
public class ServiceProviderTracker implements ServiceTrackerCustomizer {
    
    /** If the registered service does not have an endpoint name, we use this. */
    private static final String DEFAULT_ENDPOINT_NAME = "_endpoint";
    
    private Logger logger_ = Logger.getLogger(
            ServiceProviderTracker.class.getPackage().getName());

    /** Used to track services of interest to the JBI OSGi framework. */
    private ServiceTracker serviceTracker_;
    
    /** Reference to the framework's bundle context. */
    private BundleContext context_;
    
    /** Used to get ComponentContext instances for bundles. */
    private ComponentContextFactory contextFactory_;
    
    /** Map of ServiceMeditors, which handle communication between JBI and OSGi */
    private HashMap<ServiceReference, ServiceMediator> serviceMediators_ = 
            new HashMap<ServiceReference, ServiceMediator>();
    
    public ServiceProviderTracker(ComponentContextFactory contextFactory) {
        // create service tracker to keep track of services we care about
        context_ = Environment.getFrameworkBundleContext();
        contextFactory_ = contextFactory;
        serviceTracker_ = new ServiceTracker(
                context_, ServiceProvider.class.getName(), this);
    }
    
    /** Start tracking services. */
    public void start() {
        serviceTracker_.open();
    }
    
    /** Stop tracking services. */
    public void stop() {
        serviceTracker_.close();
    }
    
    public Object addingService(ServiceReference reference) {
        
        // No service, no shirt, no shoes
        if (reference.getProperty(ServiceProps.SERVICE_NAME.toString()) == null) {
            logger_.warning("Unable to register ServiceProvider instance for " +
                    "bundle " + reference.getBundle().getSymbolicName() + 
                    ".  Service name is null.");
            return null;
        }
        
        try {
            ServiceMediator mediator = createServiceMediator(reference);
            mediator.start();
            serviceMediators_.put(reference, mediator);
        }
        catch (Exception ex) {
            logger_.log(Level.WARNING, 
                    "Failed to create service mediator " + reference, ex);
        }
        
        return context_.getService(reference);
    }

    public void modifiedService(ServiceReference reference, Object service) {
        /// hmmm ... what to do here?
    }

    public void removedService(ServiceReference reference, Object service) {
        ServiceMediator mediator = serviceMediators_.remove(reference);
        if (mediator != null) {
            mediator.stop();
        }
    }
    
    private ServiceMediator createServiceMediator(ServiceReference osgiService)
            throws Exception {
        
        QName serviceName = QName.valueOf((String)
                osgiService.getProperty(ServiceProps.SERVICE_NAME.toString()));
        String endpointName = (String)
                osgiService.getProperty(ServiceProps.ENDPOINT_NAME.toString());
        
        // If an endpoint name isn't specified, use a reasonable default
        if (endpointName == null) {
            endpointName = serviceName.getLocalPart() + DEFAULT_ENDPOINT_NAME;
        }
        
        // Need a component context to activate the endpoint in JBI
        ComponentContext context = contextFactory_.getComponentContext(
                osgiService.getBundle());
        //ensure that the delivery channel is initialized
        context.getDeliveryChannel();
        ServiceEndpoint jbiService = context.activateEndpoint(
                serviceName, endpointName);
        
        return new ServiceMediator(context.getDeliveryChannel(),
                jbiService, (ServiceProvider)context_.getService(osgiService));
        
    }
}
