/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeLoggingService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.HashMap;
import java.util.Dictionary;
import java.util.Vector;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.framework.ServiceReference;

import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.configuration.Constants;
import com.sun.jbi.framework.StringTranslator;

/**
 * The <code>RuntimeLoggingService</code> is used to track the list of configurable runtime loggers. 
 * This class contains an instance of <code>LoggingService</code> and when it is initialized, adds a 
 * a list of pre-determined runtime loggers to the list of loggers tracked.
 * 
 * Any other runtime logger, other than the list of pre-determined loggers, that should  
 * be a added as configurable logger, should be obtained from the RuntimeLoggingService 
 * as follows (as opposed to getting it directly from the JDK Logger).
 * 
 * <code>Logger logger = RuntimeLoggingService.getLogger(loggerName);</code>
 * 
 * This will add the provided logger to the list of loggers tracked by the 
 * RuntimeLoggingService.
 *  
 * Only these loggers can be configured via admin tools. The other loggers created directly
 * as Logger.getLogger() will be children of one of these loggers and can thus be configured
 * indirectly. 
 * 
 * The RuntimeLoggingService updates the configuration of LogLevelConfigurationService. 
 * This sync is done to ensure that the admin tools will be able to verify the user 
 * provided logger name when the user tries to modify the log level of a logger. 
 * 
 */
public class RuntimeLoggingService {
    
    /** 
     * an instance of LoggingService to track runtime loggers 
     */
    private static LoggingService  runtimeLoggingService_;
    
 
    /**
     * list of other runtime loggers that can be configured
     */
    private static final String[] RUNTIME_LOGGERS  = new String[] {
        
        "com.sun.jbi.framework",
        "com.sun.jbi.fuji",
        "com.sun.jbi.interceptors",
        "com.sun.jbi.messaging",
        "com.sun.jbi.logging"
                       
    };
    
    /** 
     * logger
     */ 
    private static Logger log_;
    
    /**
     * translator
     */
    private static StringTranslator translator_ = new StringTranslator(
                LoggingService.class.getPackage().getName(), null); 
    
    /**
     * runtime logger service PID
     */
    private static final String RUNTIME_LOGGER_PID =
            Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR + "runtime-logger" + LoggingService.SEPARATOR + "runtime-logger";
    
    /**
     * This method is used to add key runtime loggers to the list of 
     * configurable loggers tracked by the RuntimeLoggerConfigurationService
     */
    public static void initializeService() {

        log_ = Logger.getLogger(LoggingService.class.getPackage().getName());
        runtimeLoggingService_ = new LoggingService(RUNTIME_LOGGER_PID);
        initializeRuntimeLoggers();
    }
    
    /**
     * This method is used to intialize the list of runtime loggers.
     * First existing configuration of the runtime-logger object is retrieved.
     * Only if it is null (first time startup or deleted config) the loggers are
     * initialized. In other cases, the configuration persisted by configuration admin
     * is given preference.
     * 
     */
    private static void initializeRuntimeLoggers(){
    
     
        try {
            ConfigurationAdmin configAdmin;

            if ((configAdmin = getConfigurationAdminService()) != null) {
             
                Configuration config = configAdmin.getConfiguration(  
                            RUNTIME_LOGGER_PID, 
                            null);  

                if (config != null) {

                    Dictionary properties = config.getProperties();
                    
                    /* Only if the config is null (first time startup or deleted config) the loggers are
                     * initialized. In other cases, the configuration persisted by configuration admin
                     * is given preference, to handle log config persistence across system restarts.*/
                    
                    if (properties == null) {  
                        runtimeLoggingService_.getLogger(
                                LoggingService.JBI_ROOT_LOGGER_NAME, 
                                null, 
                                LoggingService.JBI_ROOT_LOGGER_LEVEL);
                        for(String logger : RUNTIME_LOGGERS) {
                            runtimeLoggingService_.getLogger(logger);
                        }
                    } else {
                        //not the first time, so initialize the list from existing config
                        runtimeLoggingService_.initializeLoggerList((Vector<String>)properties.get(LoggingService.LOGGERS_KEY));
                    }
                }
            }
        } catch (Exception ex) {
            log_.log(Level.WARNING, translator_.getString(LocalStringKeys.CONFIG_UPDATE_FAILED), ex);

        }

        
    }
    
    /**
     * This method is used to get a logger
     * @param loggerName logger name
     */
     public static Logger getLogger(String loggerName) {
         return runtimeLoggingService_.getLogger(loggerName);
     }
     
    /**
     * This method is used to get a logger
     * @param loggerName logger name
     */
     public static Logger getLogger(String loggerName, String resourceBundleName) {
         return runtimeLoggingService_.getLogger(loggerName, resourceBundleName);
     }
     
     /**
      * This method is used to return the list of available runtime loggers
      * that have been registered before the configuration service was registered
      * 
      */
     public static HashMap<String, String> getLoggers() {
         return runtimeLoggingService_.getLoggers();
     }
     
    
    /**
     * This method is used to get a reference to the config admin service
     */
    private static ConfigurationAdmin getConfigurationAdminService() {
        
        ConfigurationAdmin configAdmin = null;
        if (Environment.getFrameworkBundleContext() != null) {
            ServiceReference  configAdminRef =
               Environment.getFrameworkBundleContext().getServiceReference(ConfigurationAdmin.class.getName());
 
            if (configAdminRef != null) {
                configAdmin = (ConfigurationAdmin)  
                         Environment.getFrameworkBundleContext().getService(configAdminRef);  

            }
        }
        return configAdmin;
    }

}
