/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Route.java - Last published on 3/13/2008
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.xml.namespace.QName;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;
import com.sun.jbi.interceptors.internal.MessageName;
import com.sun.jbi.messaging.MessageExchangeProxy;

import org.glassfish.openesb.api.service.ServiceMessageImpl;

/**
 * This class provides support for service filtering in a IFL route definition.
 *
 * @author Sun Microsystems, Inc.
 */
public class Route extends AbstractFlow {
    
    /** Default operation used for service filters */
    static String FILTER_OPERATION_NAME = "requestReply";
    /** Default operation used for service provider */
    static String PROVIDER_OPERATION_NAME = "oneWay";
    /** Route state that goes along with each exchange. */
    private Logger log_ = Logger.getLogger(Route.class.getPackage().getName());

    private IFLModel    ifl_;

    // List of services that form the route path
    private List<String>                            routePath_;
    private String                                  routeName_;
    private QName                                   routeService_;

    public Route(){};
    /**
     * Creates a new flow filter.
     * @param filterServices list of services that serve as filters
     */
    public Route(String routeName, String namespace, QName fromService, List<String> routePath, IFLModel ifl) 
        throws Exception {
        
        super(namespace, routeName, com.sun.jbi.fuji.ifl.Flow.ROUTE);
        routeName_ = routeName;
        routePath_ = routePath;
        routeService_ = new QName(ifl.getNamespace(routeName), routeName);
        ifl_ = ifl;
        log_.fine("Route(" + routeName + ") Path(\n" + routePath + "\n    )");

        // see if we have a consume service
//        if (!routePath.get(0).equals(fromService)) {
//            activateEndpoint(fromService, routeName + "_endpoint", true);
//        }
//        else
//        {
            activateEndpoint(routeService_, routeName + "_endpoint", true);
//        }
    }
    
    protected boolean acceptEvent(int id) { return (false); }

    protected void onRequest(AbstractFlow.InboundContext context) {
        InboundContext  ic = (InboundContext)context;
        List<String>    request;
        MessageExchange exchange = ic.getExchange();

        //
        //  This is a new request. It could be a request from a forked route in
        //  which case a RoutePath will exist. It could contain a routePath for
        //  a different route. The RouteService property is used to determine
        //  which case.
        //
        QName   targetService = (QName)exchange.getProperty(ROUTE_SERVICE);

        if (targetService != null) {
            if (targetService.equals(routeService_)) {
                request = (List<String>)exchange.getProperty(ROUTE_PATH);
                if (request != null) {
                    if (request.size() > 0) {
                        List<String>    history;

                        if ((history = ((List<String>)exchange.getProperty(ROUTE_HISTORY))) != null) {
                            history.add("*");
                        }
                    }
                } else {
                    request = routePath_;
                }
            } else {
                //exchange.setProperty(ROUTE_HISTORY_PROPERTY, null);
                request = routePath_;
            }
        } else {
            request = routePath_;
        }

        log_.fine("EIP: Route Request(" + routeName_ + "," + exchange.getExchangeId() + ") Path(" + request + ") STARTED");
        ic.setRouteList(request);
        doRoute(ic, null);
    }

    protected void onReply(AbstractFlow.OutboundContext oc) {
        log_.fine("EIP: Route Reply (" + routeName_ + "," + oc.getExchange().getExchangeId() + ")");
        if (handleRoute(oc) != null) {
            doRoute((InboundContext)oc.getInboundContext(), oc);
        }
    }

    protected void onStatus(AbstractFlow.OutboundContext oc) {
        log_.fine("EIP: Route Status-Outbound (" + routeName_ + "," +
                oc.getInboundContext().getExchange().getExchangeId() + ")");
        if (handleRoute(oc) != null) {
            doRoute((InboundContext)oc.getInboundContext(), oc);
        }
     }

    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Route Status-Inbound (" + routeName_ + "," + ic.getExchange().getExchangeId() + ")");
    }

    private void doRoute(InboundContext ic, AbstractFlow.OutboundContext oc) {
        MessageExchange     route = ic.getExchange();
        MessageExchange     exchange = oc != null ? oc.getExchange() : null;
        List<String>        request = ic.getRouteList();
        ServiceMessageImpl  message;
        String              service = request.get(0);
        String              defOperation;
        boolean             lastItem = request.size() == 1;
        List<String>        history;

        try {
            //
            //  If an InOnly service edited the route we need to pick up the IN message.
            //
            if (exchange == null) {
                message = (ServiceMessageImpl)MessageExchangeUtil.getInMessage(route);
            } else if (exchange instanceof InOnly) {
                message = (ServiceMessageImpl)MessageExchangeUtil.getInMessage(exchange);
            } else {
                message = (ServiceMessageImpl)MessageExchangeUtil.getOutMessage(exchange);
            }
            //
            //  All requests are InOut, except for the last. It will be InOut if we
            //  were called with an InOut.
            //
            if (lastItem && route instanceof InOnly) {
                exchange = getExchangeFactory().createInOnlyExchange();
                defOperation = PROVIDER_OPERATION_NAME;
//                exchange.setOperation(getOperation(service, PROVIDER_OPERATION_NAME));
            }
//            else if (lastItem && route instanceof InOut) {
////                route.setMessage(route.getMessage("in"), "out");
//                exchange = getExchangeFactory().createInOutExchange();
//                defOperation = FILTER_OPERATION_NAME;
////                exchange.setOperation(getOperation(service, FILTER_OPERATION_NAME));
//            }
            else {
                exchange = getExchangeFactory().createInOutExchange();
                defOperation = FILTER_OPERATION_NAME;
//                exchange.setOperation(getOperation(service, FILTER_OPERATION_NAME));
            }
            oc = newOutboundContext(exchange);
            addressExchange(oc, service, route.getOperation(), defOperation, message);
            exchange.setService(getService(service));

            //
            // Setup message and properties.
            //
            preProcess(route, exchange, message);

            //
            // Update routing properties.
            //
            List<String>    requestCopy = new ArrayList();

            request = request.subList(1, request.size());
            requestCopy.addAll(request);
            exchange.setProperty(ROUTE_PATH, requestCopy);
            exchange.setProperty(PARENT_EXCHANGE, route);
            exchange.setProperty(ROUTE_SERVICE, getEndpoint().getServiceName());
            ((com.sun.jbi.ext.MessageExchange)exchange).setParent(route);

            //
            //  Update the routing history.
            //
            if ((history = (List<String>)exchange.getProperty(ROUTE_HISTORY)) == null) {
                history = new ArrayList();
                exchange.setProperty(ROUTE_HISTORY, history);
            }
            history.add(service);

//            //
//            //  Set the input type.
//            //
//            QName   messageType = message.getMessageType();
//            QName   serviceType = oc.getInputType();
//            if (!messageType.equals(serviceType)) {
//                if (messageType.equals(ServiceMessageImpl.ANYTYPE)) {
//                    message.setMessageType(serviceType);
//                }
//            }

            //
            //  Invoke the route item. Remember request, path and route.
            //
            log_.fine("EIP: Route(" + routeName_ + ":" + route.getExchangeId() + ") Child(" + exchange.getExchangeId() + ") Service(" + service + ")");
            oc = newOutboundContext(exchange);
            oc.setInboundContext(ic);
            ic.addChild(oc);
            ic.setRouteList(request);
            request(oc);
        } catch (javax.jbi.messaging.MessagingException mEx) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            mEx.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP:Route (" + routeName_ + ") Exception:" + b.toString() + toString());
            route.setProperty(ROUTE_PATH, null);
            route.setError(mEx);
            ic.removeChild(oc);
            try {
                status(ic);
            } catch (Exception ignore) {
               log_.warning("EIP:Route (" + routeName_ + ") Exception while handling error:" + ignore + toString());
            }
       }
    }

    private List<String> handleRoute(OutboundContext oc) {
        InboundContext          ic = (InboundContext)oc.getInboundContext();
        MessageExchange         exchange = oc.getExchange();
        MessageExchange         route = ic.getExchange();
        NormalizedMessage       message;
        List<String>            request = null;

        try {
            if (exchange.getStatus() == ExchangeStatus.ERROR) {
                //
                //  Route returned an ERROR. Terminate the Exchange with an ERROR.
                //
                log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() +
                        ") Child(" + exchange.getExchangeId() + ") completed with ERROR");
                route.setProperty(ROUTE_PATH, null);
                route.setError(exchange.getError());
                ic.removeChild(oc);
                status(ic);
                return (null);
            } else if (exchange.getStatus() == ExchangeStatus.DONE) {
                //
                //  Route returned DONE (no out message). Just forget about the route.
                //
                log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() +
                        ") Child(" + exchange.getExchangeId() + ") DONE Mark DONE");
                ic.removeChild(oc);
             } else {
                //
                //  Route is still ACTIVE. ACK the reply.
                //
                log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() +
                        ") Child(" + exchange.getExchangeId() + ") ACTIVE Send DONE");
                exchange.setStatus(ExchangeStatus.DONE);
                ic.removeChild(oc);
                status(oc);
            }

            //
            // See if the route was updated by the service.
            // The route path is deleted by the Route EIP when it returns so that we
            // can handle nested routes correctly. We just restore the route from our
            // saved state. A route isn't allowed to edit the previous route.
            //
            List<String>    updatedPath = (List<String>)exchange.getProperty(ROUTE_PATH);
            List<String>    history;
            request = ic.getRouteList();
            if (updatedPath != null) {
                if (!request.equals(updatedPath))
                {
                    if ((history = ((List<String>)route.getProperty(ROUTE_HISTORY))) != null) {
                        history.add(":");
                    }
                    request = updatedPath;
                    log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() + ") Route updated: " + request);
                }
            } else {
                exchange.setProperty(ROUTE_SERVICE, getEndpoint().getServiceName());
            }

            //
            //  Handle based on the Exchange type.
            //
            if (exchange instanceof InOnly && route instanceof InOnly) {
                //
                //  InOnly normally means the end of the route. But, if the last service
                //  edited the route we will continue.
                //
                if (request.size() == 0) {
                    log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() + ") inOnly DONE");
                    route.setProperty(ROUTE_PATH, null);
                    route.setStatus(ExchangeStatus.DONE);
                    ic.removeChild(oc);
                    status(ic);
                    request = null;
                }
            } else {
                //
                // If we have an output message and this is the end of the route and
                // the route expects a result, copy the message to the reply.
                // Null output message indicates that the exchange should turn around.
                //
                message = postProcess(exchange, route);
                if (message != null) {
                    if (request.size() == 0) {
                        log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() + ") Reply");
                        if (route instanceof InOut) {
                            route.setMessage(message, "out");
                        }
                    }
                } else {
                    log_.fine("EIP: Route(" + routeName_ + "," + route.getExchangeId() + ") no output, so DONE");
                    if ((history = ((List<String>)route.getProperty(ROUTE_HISTORY))) != null) {
                        history.add(".");
                    }
                    if (route instanceof InOnly) {
                        route.setStatus(ExchangeStatus.DONE);
                    }
                }
                if (message == null || request.size() == 0) {
                    route.setProperty(ROUTE_PATH, null);
                    status(ic);
                    request = null;
                }
            }
       } catch (javax.jbi.messaging.MessagingException mEx) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            mEx.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP:Route (" + routeName_ + ") Exception:" + b.toString() + toString());
       }
       if (request != null) {
           ic.setRouteList(request);
       }
       return (request);
    }

    public InboundContext newInboundContext(MessageExchange me){
        return (new InboundContext(this, me));
    }

    class InboundContext extends AbstractFlow.InboundContext {
        List<String>            route_;

        InboundContext(AbstractFlow flow, MessageExchange me) {
            super(flow, me);
        }

        List<String> getRouteList() {
            return (route_);
        }

        void setRouteList(List<String> list) {
            route_ = list;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(super.toString());
            sb.append("          Route:\n");
            sb.append("            List:  " + route_ + "\n");
            return (sb.toString());
        }
    }


    void preProcess(MessageExchange source, MessageExchange target, NormalizedMessage message )
        throws MessagingException {
        
        // Copy the message, operation, and properties over
        // NOTE: operation should probably come from the IFL filter definition
        target.setMessage(message, "in");
        for (Object prop : source.getPropertyNames()) {
            if (!prop.toString().equals("com.sun.jbi.messaging.parent")) {
                target.setProperty(prop.toString(), source.getProperty(prop.toString()));
            }
        }
    }
    
    NormalizedMessage postProcess(MessageExchange source, MessageExchange target)
        throws MessagingException {
        // merge properties
        for (Object prop : source.getPropertyNames()) {
            if (!prop.toString().equals("com.sun.jbi.messaging.parent")) {
                target.setProperty(prop.toString(), source.getProperty(prop.toString()));
            }
        }
        return (source.getMessage(MessageName.OUT.toString()));
    }
    
    String getProviderServiceForExchange(MessageExchange exchange) {
        String providerService = null;
        MessageExchangeProxy pEx = ((MessageExchangeProxy)exchange).getTwin();
        
        if (pEx != null) {
             QName name = pEx.getEndpoint().getServiceName();
             if (name != null) {
                 providerService = name.getLocalPart();
             }
        }
        
        return providerService;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        Route (" + routeName_ + ")\n");
        return (sb.toString());
    }

}
