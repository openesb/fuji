/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Connection;
import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.descriptor.ServiceAssembly;
import com.sun.jbi.framework.descriptor.ServiceUnit;
import com.sun.jbi.framework.result.ComponentTaskResult;
import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.interceptors.Interceptor;
import com.sun.jbi.interceptors.internal.flow.aggregate.AggregateController;
import com.sun.jbi.interceptors.internal.flow.Broadcast;
import com.sun.jbi.interceptors.internal.flow.ContentRoute;
import com.sun.jbi.interceptors.internal.flow.Filter;
import com.sun.jbi.interceptors.internal.flow.Route;
import com.sun.jbi.interceptors.internal.flow.Split;
import com.sun.jbi.interceptors.internal.flow.Tee;
import com.sun.jbi.interceptors.internal.FilterProperty;
import com.sun.jbi.interceptors.internal.InterceptionHelper;
import com.sun.jbi.interceptors.internal.MessageName;
import com.sun.jbi.interceptors.internal.ServiceProperty;

import com.sun.jbi.interceptors.internal.flow.AbstractFlow;
import com.sun.jbi.messaging.EndpointRegistry;
import com.sun.jbi.messaging.Link;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.DeploymentException;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceRegistration;

/**
 * OSGi bundle representing a JBI service assembly.
 * @author kcbabo
 */
public class ServiceAssemblyBundle extends StatefulJBIBundle {
    
    private static String DEFAULT_APP_NS_ROOT = 
            "http://fuji.dev.java.net/application/";
    
    private static String FLOW_ROOT = "/META-INF/flow/"; 
    private static String FLOW_CONFIG_NAME = "flow.properties";
    private static String EXP = "exp";
    
    private String                  appNamespace_;
    private Logger                  logger_ = Logger.getLogger(
                                        ServiceAssemblyBundle.class.getPackage().getName());
    private List<AbstractFlow>      flows_ = new ArrayList();
    
    /**
     * Creates a new ServiceAssemblyBundle.
     * @param descriptor jbi.xml
     * @param bundle containing OSGi bundle
     */
    public ServiceAssemblyBundle(Jbi descriptor, Bundle bundle) {
        super(descriptor, bundle);
        appNamespace_ = DEFAULT_APP_NS_ROOT + bundle.getSymbolicName();
    }
    
    /**
     * I'm a service assembly.
     * @return service assembly type
     */
    public BundleType getType() {
        return BundleType.SERVICE_ASSEMBLY;
    }
    
    /**
     * Returns service assembly name.
     * @return SA name
     */
    public String getName() {
        return getDescriptor().getServiceAssembly().getIdentification().getName();
    }
    
    /**
     * @return the application namespace uri string
     */
    public String getNamespace()
    {
        return appNamespace_;
    }
    
    /**
     * @return all the flows registered by this application
     */
    public List<AbstractFlow> getFlows(){
        return flows_;
    }
    
    public synchronized void unregisterFlowInterceptors() {
        for (AbstractFlow af : flows_) {
            af.shutdown();
        }
    }

    /**
     * Get the flow configuration from the flow.properties file packaged in
     * META-INF/flow/[flow]/[name]/
     * 
     * The contents of the properties file are returned in a String. An example
     * string return value for aggregate would be : "count=5,timeout=2s"
     * 
     * @return the contents of the external flow configuration file as a string.
     *         An empty string if there is no configuration file.
     */
    public Properties getFlowConfiguration(Flow flow, String name)
    {
        Properties      flowProps = null;
        StringBuffer    res = new StringBuffer(FLOW_ROOT);
        res.append(flow.toString());
        res.append("/");
        res.append(name);
        res.append("/");
        res.append(FLOW_CONFIG_NAME);
        java.net.URL resURL = this.getBundle().getEntry(res.toString()); 
        
        if ( resURL != null )
        {
            try
            {
                java.io.InputStream ios = resURL.openStream();
                flowProps = new Properties();
                flowProps.load(ios);
            }
            catch ( java.io.IOException ioex )
            {
                logger_.log(Level.FINE, "Could not read external configuration for flow " + flow.toString() 
                        + " with name " + name, ioex);
            }
        }
        return (flowProps);
    }

    /**
     * Start the service assembly. 
     * 
     * @exception Exception on failure to start the bundle.
     */
    public void start() throws Exception
    {
        if ( getState() != JBIState.STARTED )
        {
            logger_.info("Starting service assembly " + this.getName());
            Environment.getMessageService().toogleMessageTrace(logger_.isLoggable(Level.FINEST));

            List<ServiceUnit> startedServiceUnits = new ArrayList<ServiceUnit>();
            List<ServiceUnit> initializedServiceUnits = new ArrayList<ServiceUnit>();
                    
            try {
                // Activate service connections before starting service units
                activateServiceConnections(this.getDescriptor().getServiceAssembly());
                // Register flow extensions
                registerFlowExtensions(this);
                // Register interceptors
                registerInterceptors(this);
                
                // First initialize all the service units
                for (ServiceUnit su : this.getDescriptor().getServiceAssembly().getServiceUnits()) {
                    ComponentBundle component = Environment.getComponentManager().
                            getComponent(su.getComponentName());

                    // Start the target component if it's not already active
                    if (component.getBundle().getState() != Bundle.ACTIVE) {
                        component.getBundle().start();
                    }

                    initServiceUnit(component, this, su);
                    initializedServiceUnits.add(su);
                }
                
                // Start the initialized service units
                for (ServiceUnit su : this.getDescriptor().getServiceAssembly().getServiceUnits()) {
                    ComponentBundle component = Environment.getComponentManager().
                            getComponent(su.getComponentName());
                    
                    // Start the target component if it's not already active
                    if (component.getBundle().getState() != Bundle.ACTIVE) {
                        component.getBundle().start();
                    }

                    startServiceUnit(component, this, su);
                    // Remove from initialized list and add to started list
                    initializedServiceUnits.remove(su);
                    startedServiceUnits.add(su);
                }
                
                setState(JBIState.STARTED);
                Environment.getMessageService().dumpState();
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to start service assembly " +
                        this.getName(), ex);
                
                // Rollback state for SUs that have already been initialized/started
                for (ServiceUnit su : initializedServiceUnits) {
                    ComponentBundle component = Environment.getComponentManager().
                            getComponent(su.getComponentName());
                    try {
                        shutDownServiceUnit(component, this, su);
                    }
                    catch (Exception ex2) {
                        logger_.log(Level.FINE, "Failed to clean up service unit " +
                                su.getIdentification().getName() + " after failed init.", ex2);
                        continue;
                    }
                }
                
                for (ServiceUnit su : startedServiceUnits) {
                    ComponentBundle component = Environment.getComponentManager().
                            getComponent(su.getComponentName());
                    try {
                        shutDownServiceUnit(component, this, su);
                    }
                    catch (Exception ex2) {
                        logger_.log(Level.FINE, "Failed to clean up service unit " +
                                su.getIdentification().getName() + " after failed start.", ex2);
                        continue;
                    }
                }

                // Clean up SA bits and pieces
                // unregister services exposed by this bundle
                this.unregisterServices();
                //
                //  unregister any flows registered on behalf of the SA.
                //
                this.unregisterFlowInterceptors();

                // Deactivate service connections after shutting down all service units
                try {
                    deactivateServiceConnections(
                            this.getDescriptor().getServiceAssembly());
                }
                catch (Exception ex2) {
                    logger_.log(Level.FINE, "Failed to clean up service " +
                            "connection after failed deploy.", ex2);
                }
                
                // propagate the exception up
                throw ex;
            }
        }
        
    }
    
    /**
     * Stop the JBI entity.
     * 
     * @exception Exception on failure to stop the bunle.
     */
    public void stop() throws Exception
    {
        if ( getState() == JBIState.STARTED) {
            
            logger_.info("Stopping service assembly " + this.getName());
            if (logger_.isLoggable(Level.FINEST)) {
                Environment.getMessageService().dumpState();
            }
             Environment.getMessageService().toogleMessageTrace(logger_.isLoggable(Level.FINEST));
           
            try {

                // Stop service units
                for (ServiceUnit su : this.getDescriptor().getServiceAssembly().getServiceUnits()) {
                    try{
                        ComponentBundle component =
                                Environment.getComponentManager().getComponent(su.getComponentName());

                        if ( component != null ){
                            // Start the target component if it's not already active
                            if (component.getBundle().getState() != Bundle.ACTIVE) {
                                component.getBundle().start();
                            }

                            stopServiceUnit(component, this, su);
                        }
                    }
                    catch ( Exception ex)
                    {
                        logger_.log(Level.WARNING, "Failed to stop service unit " +
                            su.getIdentification().getName() + " in service assembly this.getName()", ex);
                        // Continue with trying to stop the remaining service units
                        continue;
                    }
                }
                    
                setState(JBIState.STOPPED);
                
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to stop service assembly " +
                        this.getName(), ex);
            }  
        }
        
    }
    
    /**
     * Shutdown the JBI entity.
     * 
     * @exception Exception on failure to start the bundle.
     */
    public void shutDown() throws Exception
    {
        if ( getState() != JBIState.SHUTDOWN ) {
            
            logger_.info("Shutting down service assembly " + this.getName());

            try {
                // Shutdown service units
                for (ServiceUnit su : this.getDescriptor().getServiceAssembly().getServiceUnits()) {
                    try{
                        ComponentBundle component =
                                Environment.getComponentManager().getComponent(su.getComponentName());

                        if ( component != null ){
                            // Start the target component if it's not already active
                            if (component.getBundle().getState() != Bundle.ACTIVE) {
                                component.getBundle().start();
                            }

                            shutDownServiceUnit(component, this, su);
                        }
                    }
                    catch ( Exception ex)
                    {
                        logger_.log(Level.WARNING, "Failed to shut down service unit " +
                            su.getIdentification().getName() + " in service assembly " + this.getName(), ex);
                        // Continue with trying to shut down the remaining service units
                        continue;
                    }
                }

                // unregister services exposed by this bundle
                this.unregisterServices();
                //
                //  unregister any flows registered on behalf of the SA.
                //
                this.unregisterFlowInterceptors();

                // Deactivate service connections after shutting down all service units
                deactivateServiceConnections(this.getDescriptor().getServiceAssembly());
                
                setState(JBIState.SHUTDOWN);
                
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to shut down service assembly " +
                        this.getName(), ex);
            }  
        }
        
    }
    
 
    /**
     * Invokes the appropriate component SPIs to start a service unit contained
     * in an SA bundle.
     * @param component component targeted by the service unit
     * @param sa service assembly bundle containing the service unit
     * @param su service unit descriptor
     */
    void startServiceUnit(ComponentBundle component, ServiceAssemblyBundle sa, 
            ServiceUnit su) throws Exception {

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            // Get a reference to the component's service unit manager
            ServiceUnitManager sum = component.getComponent().getServiceUnitManager();
            // Set the context class loader just in case the component needs it
            Thread.currentThread().setContextClassLoader(component.getClassLoader());
            sum.start(su.getIdentification().getName());
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }
    
 
    /**
     * Invokes the appropriate component SPIs to initialize a service unit contained
     * in an SA bundle.
     * @param component component targeted by the service unit
     * @param sa service assembly bundle containing the service unit
     * @param su service unit descriptor
     */
    void initServiceUnit(ComponentBundle component, ServiceAssemblyBundle sa, 
            ServiceUnit su) throws Exception {

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            // Get a reference to the component's service unit manager
            ServiceUnitManager sum = component.getComponent().getServiceUnitManager();
            // Set the context class loader just in case the component needs it
            Thread.currentThread().setContextClassLoader(component.getClassLoader());
            // Invoke init/start on the component's ServiceUnitManager impl
            sum.init(su.getIdentification().getName(),
                    ServiceAssemblyManager.getServiceUnitDirectory(
                            sa.getName(), su).getAbsolutePath());
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }    
    
  
    /**
     * Invokes the appropriate component SPIs to stop a service unit 
     * contained in an SA bundle.
     * @param component component targeted by the service unit
     * @param sa service assembly bundle containing the service unit
     * @param su service unit descriptor
     */
    void stopServiceUnit(ComponentBundle component, ServiceAssemblyBundle sa, 
            ServiceUnit su) throws Exception {

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            // Get a reference to the component's service unit manager
            ServiceUnitManager sum = component.getComponent().getServiceUnitManager();
            // Set the context class loader just in case the component needs it
            Thread.currentThread().setContextClassLoader(component.getClassLoader());
            // Invoke stop/shutDown on the component's ServiceUnitManager impl
            sum.stop(su.getIdentification().getName());
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }
    
    /**
     * Invokes the appropriate component SPIs to shut down a service unit 
     * contained in an SA bundle.
     * @param component component targeted by the service unit
     * @param sa service assembly bundle containing the service unit
     * @param su service unit descriptor
     */
    void shutDownServiceUnit(ComponentBundle component, ServiceAssemblyBundle sa, 
            ServiceUnit su) throws Exception {

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            // Get a reference to the component's service unit manager
            ServiceUnitManager sum = component.getComponent().getServiceUnitManager();
            // Set the context class loader just in case the component needs it
            Thread.currentThread().setContextClassLoader(component.getClassLoader());
            // Invoke stop/shutDown on the component's ServiceUnitManager impl
            sum.shutDown(su.getIdentification().getName());
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }
    
    /**
     * Activate service connections for the specified service assembly.
     * @param sa service assembly descriptor
     * @throws java.lang.Exception failed to activate a service connection
     */
    void activateServiceConnections(ServiceAssembly sa) throws Exception {
        for (Connection conn : sa.getConnections()) {
            Connection.Consumer consumer = conn.getConsumer();
            Connection.Provider provider = conn.getProvider();

            if (consumer.getInterfaceName() != null) {
                // Interface connection
                Environment.getMessageService().getConnectionManager().
                        addInterfaceConnection(consumer.getInterfaceName(),
                        provider.getServiceName(), provider.getEndpointName());
            } else {
                // Endpoint connection
                Environment.getMessageService().getConnectionManager().
                        addEndpointConnection(
                        consumer.getServiceName(), consumer.getEndpointName(),
                        provider.getServiceName(), provider.getEndpointName(),
                        Link.STANDARD);
            }
        }
    }

    /**
     * Deactivate service connections for the specified service assembly.
     * @param sa service assembly descriptor
     * @throws java.lang.Exception failed to deactivate a service connection
     */
    void deactivateServiceConnections(ServiceAssembly sa) throws Exception {
        for (Connection conn : sa.getConnections()) {
            Connection.Consumer consumer = conn.getConsumer();
            Connection.Provider provider = conn.getProvider();

            if (consumer.getInterfaceName() != null) {
                // Interface connection
                Environment.getMessageService().getConnectionManager().
                        removeInterfaceConnection(consumer.getInterfaceName(),
                        provider.getServiceName(), provider.getEndpointName());
            } else {
                // Endpoint connection
                Environment.getMessageService().getConnectionManager().
                        removeEndpointConnection(
                        consumer.getServiceName(), consumer.getEndpointName(),
                        provider.getServiceName(), provider.getEndpointName());
            }
        }
    }
    
    /**
     * Search for interceptors in the service assembly and register them in the
     * OSGi service registry.
     * @param sa service assembly bundle
     * @throws java.lang.Exception failed to add interceptor to OSGi registry
     */
    void registerInterceptors(ServiceAssemblyBundle sa) throws Exception {
        
        for (ServiceRegistration svc : 
            InterceptionHelper.registerInterceptorsInBundle(sa.getBundle())) {
            sa.addService(svc);
        }
        
    }

    /** 
     * Register any flow extensions defined in the application's IFL.
     * @param sa
     * @throws java.lang.Exception
     */
    void registerFlowExtensions(ServiceAssemblyBundle sa) throws Exception {      

        try {           
            // Find and parse IFL in application bundle
            IFLModel ifl = ServiceAssemblyManager.readIFLFromSABundle(sa);
            ifl.setApplicationId(sa.getName());
            ifl.close();

            // Create and register flow interceptors as needed
            registerRoutes(ifl, sa);
            registerBroadcasts(ifl, sa);
            registerSplits(ifl, sa);
            registerAggregates(ifl, sa);
            registerFilters(ifl, sa);
            registerTees(ifl, sa);
            registerContentRoutes(ifl, sa);
        } catch (Exception ex) {
            throw new Exception("Failed to register flow extensions.", ex);
        }
    }    
    
    
    /**
     * Register all the Routes in the Service Assembly. 
     * 
     * @param service - the service to register the filters for
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerRoutes(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        for (String routeName : ifl.getRoutes().keySet()) {
            Map.Entry<String, List<String>> route = ifl.getRoute(routeName);
            // register routes
            if (route.getValue().size() > 0) {
                QName serviceName = getServiceQName(
                        sa.getDescriptor().getServiceAssembly(), routeName);
                Route r = new Route(routeName, sa.getNamespace(), serviceName, route.getValue(), ifl);
                flows_.add(r);
            }
        }
    }
    
    /**
     * Register Broadcasts defined for a service in the application. Broadcasts defined for 
     * the consumed service are registered.
     * 
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerBroadcasts(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        // register broadcasts
        for (Map<String,Object> broadcastInfo : ifl.getBroadcasts().values())
        {
            String      broadcastName = (String)broadcastInfo.get(Flow.Keys.NAME.toString());
            Broadcast   broadcast = new Broadcast(broadcastName, sa.getNamespace(), broadcastInfo);
            flows_.add(broadcast);
        }
    }
    
    /**
     * Register Splits defined for a service in the application. Splits defined for 
     * the consumed service are registered.
     * 
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerSplits(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        // register filters
        Collection<Map<String,Object>> splits =  ifl.getSplits().values();
        if (splits.size() > 0) {
            for (Map<String,Object> splitInfo : splits )
            {
                String      splitName = (String)splitInfo.get(Flow.Keys.NAME.toString());
                /**Properties  splitConfig = (Properties)splitInfo.get(Flow.Keys.CONFIG.toString());
                
                if ( splitConfig == null )
                {*/
                    // -- no inline configuration then get external configuration
                    Properties splitConfig = getFlowConfiguration(Flow.SPLIT, splitName);
                    splitInfo.put(Flow.Keys.CONFIG.toString(), splitConfig);
                /**}*/
                Split split = new Split(splitName, sa.getNamespace(), splitInfo, sa.getBundle());
                flows_.add(split);
            }
        }
    }
    
    /**
     * Register Filters defined for a service in the application. Filters defined for 
     * the consumed service are registered.
     * 
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerFilters(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        // register filters
        Collection<Map<String,Object>> filters =  ifl.getFilters().values();
        if (filters.size() > 0) {
            for ( Map<String,Object> filterInfo : filters )
            {
                String filterName = (String)filterInfo.get(Flow.Keys.NAME.toString());
                Properties filterConfig = (Properties)filterInfo.get(Flow.Keys.CONFIG.toString());
                
                if ( filterConfig == null )
                {
                    // -- no inline configuration then get external configuration
                    Properties extConfig = getFlowConfiguration(Flow.FILTER, filterName);
                    filterInfo.put(Flow.Keys.CONFIG.toString(), extConfig);
                    filterConfig = extConfig;
                }

                Filter filter = new Filter(filterName, sa.getNamespace(), filterInfo);
                flows_.add(filter);
            }
        }
    }
    
    /**
     * Register Tees defined for a service in the application. Tees defined for 
     * the consumed service are registered.
     * 
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerTees(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        // register tees
        for (Map<String,Object> teeInfo : ifl.getTees().values())
        {
            String  teeName = (String)teeInfo.get(Flow.Keys.NAME.toString());
            Tee     tee = new Tee(teeName, sa.getNamespace(), teeInfo);
            flows_.add(tee);
        }
    }
    
    
    /**
     * Register ContentRouters defined for a service in the application. 
     * 
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerContentRoutes(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        // register ContentRouter
        for (Map<String,Object> contentInfo : ifl.getSelects().values())
        {
            
            Properties selectConfig = (Properties)contentInfo.get(Flow.Keys.CONFIG.toString());
            String selectName = (String)contentInfo.get(Flow.Keys.NAME.toString());
            
            if ( selectConfig == null )
            {
                // -- no inline configuration then get external configuration
                Properties extConfig = getFlowConfiguration(Flow.SELECT, selectName);
                contentInfo.put(Flow.Keys.CONFIG.toString(), extConfig);
                selectConfig = extConfig;
            }
            ContentRoute cr = new ContentRoute(selectName, sa.getNamespace(), contentInfo, ifl);
            flows_.add(cr);
        }
    }
    
    /**
     * Register Aggregates defined for a service in the application. Aggregates defined for 
     * the consumed service are registered.
     * 
     * @param ifl - in memory IFL Model
     * @param sa - ServiceAssemblyBundle instance
     * 
     */
    private void registerAggregates(IFLModel ifl, ServiceAssemblyBundle sa)
        throws Exception
    {
        // register filters
        Collection<Map<String,Object>> aggregates =  ifl.getAggregates().values();
        if (aggregates.size() > 0) {
            for ( Map<String,Object> aggregateInfo : aggregates )
            {
                String aggregateName = (String)aggregateInfo.get(Flow.Keys.NAME.toString());
                
                // Get both inline and external config from the packaged flow.properties
                Map aggregateConfig = getFlowConfiguration(Flow.AGGREGATE, aggregateName);
                aggregateInfo.put(Flow.Keys.CONFIG.toString(), aggregateConfig);
                
                AggregateController aggregate = new AggregateController(aggregateName, sa.getNamespace(), aggregateInfo, sa.getBundle());
                
                flows_.add(aggregate);
            }
        }
    }
    
    /** Parses the connections info in jbi.xml to figure out the QName
     *  for the service.
     * @param sa service assembly descriptor
     * @param service local part of service name
     * @return qualified service name or null if the jbi.xml does not reference
     *  the service
     */
    private QName getServiceQName(ServiceAssembly sa, String service) {
        QName serviceName = null;
        boolean foundService = false;
        for (Connection conn : sa.getConnections()) {
            QName  providerServiceName = conn.getProvider().getServiceName();
            serviceName = conn.getConsumer().getServiceName();

            if (serviceName.getLocalPart().equals(service) ||
                providerServiceName.getLocalPart().equals(service)) {
                foundService = true;
                break;
            }
        }
        // Cannot determine namespace from jbi.xml, use application namespace
        if (!foundService) {
            serviceName = new QName(appNamespace_, service);
        }
        return serviceName;
    }
}
