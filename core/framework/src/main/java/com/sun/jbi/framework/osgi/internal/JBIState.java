package com.sun.jbi.framework.osgi.internal;

/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIState.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */


/**
 * JBIState Enumeration
 *
 * @author Sun Microsystems, Inc.
 */
public enum JBIState
{
    RECOVERING("Recovering"),
    SHUTDOWN("Shutdown"), 
    STOPPED("Stopped"), 
    STARTED("Started");

    /** The String value */
    private String mString;

    JBIState(String strValue)
    {
	mString = strValue;
    }
    
    /**
     * @return the String value for the state
     */
    public String toString()
    {
        return mString;
    }
    
    /**
     * @return a JBIState based on the String value. 
     * @param valStr - the string whose equivalent JBIState
     *                 instance is required.  This operation ignores
     *                 the case, i.e. Stopped or STOPPED or sToPped
     *                 would return JBIState.STOPPED
     */
    public static JBIState valueOfString(String valStr)
    {
        return JBIState.valueOf(valStr.toUpperCase());
    }
};
