/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentContextFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.messaging.DeliveryChannelImpl;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;

/**
 * Service factory for ComponentContext intances.  This class is called upon
 * by the OSGi framework each time a bundle requests a component context via
 * the OSGi service registry.
 * @author kcbabo
 */
public class ComponentContextFactory implements ServiceFactory {
    
    private ConcurrentHashMap<Long, ComponentContext> contexts_ = 
            new ConcurrentHashMap<Long, ComponentContext>();
    
    private Logger logger_ = Logger.getLogger(
            ComponentContextFactory.class.getPackage().getName());
    
    private ServiceRegistration serviceRegistration_;
    

    /** Called each time a bundle attempts to bind to a service.  The bundle's
     *  id (instead of symbolic name) is used for the delivery channel id to 
     *  allow for multiple versions of a given bundle.
     */
    public Object getService(Bundle bundle, ServiceRegistration registration) {
        return getComponentContext(bundle);
    }

    /** OSGI framework is telling us the given service is no longer used by
     *  any bundles.
     */
    public void ungetService(Bundle bundle, ServiceRegistration registration, 
            Object service) {
        
        if (contexts_.contains(bundle.getBundleId())) {
            ComponentContext context = contexts_.get(bundle.getBundleId());
            try {
                context.getDeliveryChannel().close();
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                    "Failed to clean up DeliveryChannel for bundle " + 
                    bundle.getSymbolicName(), ex);
            }
            contexts_.remove(bundle.getBundleId());
        }
    }
    
    /**
     * Returns a ComponentContext instance for the specified bundle, creating
     * one if necessary.  If a context instance has already been requested for
     * the bundle, that reference is returned.
     * @param bundle
     * @return
     */
    public ComponentContext getComponentContext(Bundle bundle) {
        ComponentContext context = null;
        
        // Check to see if we have a local reference
        if (contexts_.contains(bundle.getBundleId())) {
            context = contexts_.get(bundle.getBundleId());
        }
        
        // Looks like we need to create a fresh one
        try {
            context = new ComponentContext(bundle.getSymbolicName(), null, null);
            contexts_.put(bundle.getBundleId(), context);
        } catch (Exception ex) {
            logger_.log(Level.WARNING, 
                    "Failed to allocate DeliveryChannel for bundle " + 
                    bundle.getSymbolicName(), ex);
        }
        
        return context;
    }

}
