/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XpathSplitter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import com.sun.jbi.framework.StringTranslator;
import org.glassfish.openesb.api.message.MessageProps;
import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.interceptors.LocalStringKeys;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Interface for a splitter
 * 
 * @author Sun Microsystems, Inc.
 */
public class XpathSplitter 
     
    extends org.glassfish.openesb.api.eip.split.BaseSplitter
    implements org.glassfish.openesb.api.eip.split.Splitter
{ 
    
    String              expression_;
    DocumentBuilder     builder_;
    Transformer         transformer_;
    
    StringTranslator    translator_ = new StringTranslator("com.sun.jbi.interceptors", null);
    
    /**
     * Create an Xpath Splitter instance
     */
    public XpathSplitter()
        throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setIgnoringElementContentWhitespace(true);
        builder_ = dbf.newDocumentBuilder();
        
        transformer_ = TransformerFactory.newInstance().newTransformer();
        transformer_.setOutputProperty (OutputKeys.METHOD, "xml");
    }
    
    /**
     * Set the namespace context
     *
    public void setNamespaceContext(NamespaceContext nsCtx)
        throws Exception{
        namespaceCtx_ = nsCtx;
    }*/
    
    /**
     * Set Configuration
     *
    public void setConfiguration(HashMap config)
        throws Exception{
        super.setConfiguration(config);
        expression_ = (String)config_.get("exp");
    }*/
    
    /**
     * 
     * @return the Correlation ID from the message
     */
    public Object getCorrelationID(ServiceMessage message){
        return (String)message.getProperty(MessageProps.GROUP_ID.toString());
    }
    
    
    /**
     * Split the current active message in the exchange and return the split documents ina 
     * ArrayList
     * 
     * @param svcMsg - message to split
     */
    public ArrayList split(ServiceMessage svcMsg)
        throws Exception
    {
        expression_ = (String)config_.get("exp");
        Object payload = svcMsg.getPayload();
        
        if ( payload instanceof Node )
        {
            Document document = null;
            ArrayList splitResultDocs = new ArrayList();
            
            Node doc = (Node) payload;
            
            // parse the XML as a W3C Document
            if ( doc.getChildNodes().getLength() == 0 && 
                 doc.getNodeType() == Node.TEXT_NODE )
            {
                document = builder_.parse(new InputSource(new StringReader(doc.getNodeValue())));
            }
            else if (doc.getNodeType() == Node.ELEMENT_NODE )
            {
                document = builder_.parse(new InputSource(NodeToStringReader(doc)));
            } else if (doc.getNodeType() == Node.DOCUMENT_NODE) {
                document = (Document)doc;
            }
            
            XPath xpath = XPathFactory.newInstance().newXPath();
            xpath.setNamespaceContext(namespaceCtx_);
            Object splitResult = null;
            try
            {
                splitResult = xpath.evaluate(expression_, document.getDocumentElement(), XPathConstants.NODESET);
            }
            catch (javax.xml.xpath.XPathExpressionException ex)
            {
                String errMsg = translator_.getString(LocalStringKeys.SPLIT_ERROR_BAD_XPATH_EXPRESSION, expression_);
                throw new Exception(errMsg);
            }
            
            if ( (splitResult != null) && ( splitResult instanceof NodeList) )
            {
                NodeList nodes = (NodeList) splitResult;

                for ( int i=0; i<nodes.getLength(); i++ )
                {
                    splitResultDocs.add(nodes.item(i));
                }
            }
            
            return splitResultDocs;
            
        }
        else
        {
            throw new Exception("Cannot split a non-XML message using Xpath");
        }
    }
    
    /**
     * Create a Source instance from the object passed in. The object here is
     * of the same type as returned by split.
     * 
     * @param doc - the Object instance to be converted to a Source
     * @throws Exception
     */
    public Object createPayload(Object doc)
        throws Exception
     {
         if ( doc instanceof Node )
         {
             return new DOMSource((Node)doc);
             //return new StreamSource(NodeToStringReader((Node)doc));
         }
         else
         {
             throw new Exception("Unknown document object " + doc);
         }
         
     }
    
             
    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private StringReader NodeToStringReader(Node node)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        transformer_.transform(new DOMSource(node), new StreamResult(sw));
        return new StringReader(sw.toString());
    }

    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private String NodeToString(Node node)
        throws Exception
    {
        String nodeAsString = null;
        if ( node.getNodeType() == Node.ELEMENT_NODE)
        {
            // xsd:anyType
            StringWriter sw = new StringWriter();
            transformer_.transform(new DOMSource(node), new StreamResult(sw));
            nodeAsString = sw.toString();
        }
        else if ( node.getNodeType() == Node.TEXT_NODE )
        {
            // xsd:string
            nodeAsString = node.getNodeValue();
        }

        return nodeAsString;
    }

}
