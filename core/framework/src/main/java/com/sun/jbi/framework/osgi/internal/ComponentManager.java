/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentManager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.ComponentLifeCycle;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;

import com.sun.jbi.framework.InstallationContext;
import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.logging.ComponentLoggerConfigurationService;
import com.sun.jbi.logging.ComponentLoggerConfigurationServiceFactory;
import com.sun.jbi.logging.ComponentLoggerDisplayService;


/**
 * ComponentManager is the part of the JBI framework responsible for managing
 * JBI binding components and service engines.  The top-level framework 
 * receives bundle event notifications from the OSGi framework and delegates
 * the work related to components to this class.
 * @author kcbabo
 */
public class ComponentManager {
    
    /** Component directory under OSGi framework root. */
    private static final String COMPONENTS_DIR = "components";

    private Logger logger_ = Logger.getLogger(
            ComponentManager.class.getPackage().getName());
    
    /** List of registered bundles. */
    private Map<Long, ComponentBundle> bundles_ = 
            new ConcurrentHashMap<Long, ComponentBundle>();
    
    /**
     * Indicates whether the specified bundle has been registered with the
     * ComponentManager.
     * @param bundleId bundle identifier for a component bundle
     * @return true if the bundle has been registered, false otherwise
     */
    public boolean isRegistered(Long bundleId) {
        return bundles_.containsKey(bundleId);
    }
    
    /**
     * Fetches a component by OSGi identifier.
     * @param bundleId bundle identifier for component
     * @return ComponentBundle object, or null if the component has not 
     *  been registered.
     */
    public ComponentBundle getComponent(Long bundleId) {
        return bundles_.get(bundleId);
    }
    
    /**
     * Fetches a component by JBI identifier.
     * @param name JBI identifier for component
     * @return ComponentBundle object, or null if the component has not 
     *  been registered
     */
    public ComponentBundle getComponent(String name) {
        ComponentBundle comp = null;
        
        for (ComponentBundle c : bundles_.values()) {
            if (c.getName().equals(name)) {
                comp = c;
                break;
            }
        }
        
        return comp;
    }
    
    /**
     * Return all component bundles registered with the ComponentManager.
     * @return list of component bundles
     */
    public Collection<ComponentBundle> getComponents() {
        return bundles_.values();
    }
    
    /**
     * Register a component bundle and call the component's 208 install
     * SPI.  This method normally would be called in response to an OSGi
     * bundle INSTALLED event.
     * @param bundle OSGi bundle containing a component implementation
     * @param jbiXml component descriptor
     */
    void installComponent(Bundle bundle, Jbi jbiXml) {

        logger_.info("Installing JBI component " +  
            jbiXml.getComponent().getIdentification().getName());
        
        try {
            registerComponent(new ComponentBundle(jbiXml, bundle));
                   
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Installation of component failed", ex);
            try
            {
                bundle.uninstall();
            }
            catch(org.osgi.framework.BundleException bex)
            {
                logger_.log(Level.WARNING, "Failed to rollback failed install for component bundle " + 
                    jbiXml.getComponent().getIdentification().getName(), bex);
            }
        }
    }
    
    /**
     * Unregister a component bundle and call the component's 208 uninstall
     * SPI. This method normally would be called in response to an OSGi
     * bundle UNINSTALLED event.
     * @param bundleId bundle identifier for the component to uninstall
     */
    void uninstallComponent(Long bundleId) {
        if (!isRegistered(bundleId)) {
            return;
        }
        
        ComponentBundle component = getComponent(bundleId);
        logger_.info("Uninstalling JBI component " + component.getName());
        
        try {
            unregisterComponent(component, true);
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Uninstallation of component failed", ex);
        }
    }

    /**
     * Start a component bundle and call the component's 208 lifecycle
     * SPI.  Calling start on an OSGi bundle translates to init() + start() for
     * a JBI component.  This method normally would be called in response to an
     * OSGi bundle STARTING event.
     * @param bundleId bundle identifier for the component to start
     */
    void recoverComponent(Long bundleId){
        if (!isRegistered(bundleId)) {
            return;
        }

        ComponentBundle     component = getComponent(bundleId);
        ComponentContext    ctx = component.getComponentContext();

        try {
            ctx.getDeliveryChannel();
            component.recover();
        }
        catch (Exception ex) {
            String errMsg = "Failed to recover component "+ component.getName()
                    +": "+ ex.getMessage();
            logger_.log(Level.WARNING, errMsg, ex);
            System.err.println(errMsg);

            try {
                component.getBundle().stop();
            }
            catch(org.osgi.framework.BundleException bex) {
                logger_.log(Level.WARNING, "Failed to rollback component bundle " +
                    component.getName() + " to the stopped state", bex);
            }
        }
    }

    /**
     * Start a component bundle and call the component's 208 lifecycle
     * SPI.  Calling start on an OSGi bundle translates to init() + start() for
     * a JBI component.  This method normally would be called in response to an
     * OSGi bundle STARTING event.
     * @param bundleId bundle identifier for the component to start
     */
    void startComponent(Long bundleId){
        if (!isRegistered(bundleId)) {
            return;
        }
        
        ComponentBundle     component = getComponent(bundleId);
        ComponentContext    ctx = component.getComponentContext();

        try {
            ctx.getDeliveryChannel();
            component.start();
        }
        catch (Exception ex) {
            String errMsg = "Failed to start component "+ component.getName() 
                    +": "+ ex.getMessage();
            logger_.log(Level.WARNING, errMsg, ex);
            System.err.println(errMsg);
            
            try {
                component.getBundle().stop();
            }
            catch(org.osgi.framework.BundleException bex) {
                logger_.log(Level.WARNING, "Failed to rollback component bundle " + 
                    component.getName() + " to the stopped state", bex);
            }
        }
    }
    
    /**
     * Shut down a component bundle and call the component's 208 lifecycle
     * SPI.  Calling stop on an OSGi bundle translates to stop() + shutDown() for
     * a JBI component.  This method normally would be called in response to an
     * OSGi bundle STOPPING event.
     * @param bundleId bundle identifier for the component to shut down
     */
    void shutDownComponent(Long bundleId) {
        if (!isRegistered(bundleId)) {
            return;
        }
        ComponentBundle     component = getComponent(bundleId);
        ComponentContext    ctx = component.getComponentContext();
        
        
        try {
            component.stop();
            component.shutDown();
            ctx.closeDeliveryChannel(null);
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Failed to stop component " + 
                    component.getName(), ex);
        }
    }
    
    /** 
     * Register a ComponentBundle with the ComponentManager.  This method takes
     * care of invoking the component's intall SPI if it has not been installed
     * yet.
     * @param component component to register
     * @throws java.lang.Exception failed to register/install component
     */
    public void registerComponent(ComponentBundle component) throws Exception {
        // Create the component object and initialize its environment
        File installRoot = Environment.getFrameworkBundleContext().getDataFile(
                COMPONENTS_DIR + File.separator + component.getName());        
        ComponentContext ctx = new ComponentContext(component.getName(), 
                installRoot.getAbsolutePath(), component.getComponent());
        component.setComponentContext(ctx);

        // If the install root doesn't exist, the component hasn't been through
        // the JBI installation process yet.
        if (!installRoot.exists()) {
            invokeComponentInstaller(component);
        }
        
        // Add the component to our registry
        bundles_.put(component.getBundle().getBundleId(), component);
        
       //register the service to configure component loggers
        registerComponentLogServices(component);
        
    }
    
    
 
    /** 
     * Register an instance of the ManagedService ComponentLogConfigurationService 
     * @param componentBundle the component bundle
     * @throws java.lang.Exception failed to register/install component
     */
    public void registerComponentLogServices(ComponentBundle component) throws Exception {
      
        ComponentLoggerDisplayService componentLogDisplayService = 
                new ComponentLoggerDisplayService(component.getComponentContext());
        
        component.getComponentContext().setLogDisplayServiceRef(
            Environment.getFrameworkBundleContext().registerService(
                ManagedService.class.getName(),
                componentLogDisplayService,
                componentLogDisplayService.getConfiguration()));
        
        ComponentLoggerConfigurationService componentLogConfigService = 
                 ComponentLoggerConfigurationServiceFactory.
                getComponentLoggerConfigurationService(component.getComponentContext());
                    
        
        component.getComponentContext().setLogConfigServiceRef(
            Environment.getFrameworkBundleContext().registerService(
                ManagedService.class.getName(),
                componentLogConfigService,
                componentLogConfigService.getConfiguration()));         
    }
    
     
    /** 
     * Unregisters an instance of the ManagedService ComponentLogConfigurationService 
     * @param componentBundle the component bundle
     * @throws java.lang.Exception failed to register/install component
     */
    public void unregisterComponentLogConfigServices(ComponentBundle component) throws Exception {
        
        //remove component logger service reference and delete configurations.
        ComponentLoggerConfigurationServiceFactory.removeComponentLoggerConfigurationService(
                component.getComponentContext().getComponentName());

        ServiceRegistration serviceReg = component.getComponentContext().getLogConfigServiceRef();
        if (serviceReg != null) {
            serviceReg.unregister();
        }
        
        serviceReg = component.getComponentContext().getLogDisplayServiceRef();
        if (serviceReg != null) {
            serviceReg.unregister();
        }
        
    }
    
    
    /** 
     * Unregister a ComponentBundle with the ComponentManager.  This method takes
     * care of invoking the component's uninstall SPI and deleting its component
     * root if the delete flag is set to true.
     * @param component component to unregister/uninstall
     * @param delete true to uninstall the component, false otherwise
     * @throws java.lang.Exception failed to register/install component
     */
    void unregisterComponent(ComponentBundle component, boolean delete)
            throws Exception {
       
        unregisterComponentLogConfigServices(component);

        // Make sure the component's DeliveryChannel is closed
        if (component.getComponentContext().getDeliveryChannel() != null) {
            component.getComponentContext().getDeliveryChannel().close();
        }
       
        // If we're deleting the component, remove its install root
        if (delete) {
            
            // This will call the 208 uninstall apis for component uninstallation
            component.uninstall();
        
            File compDir = new File(
                    component.getComponentContext().getInstallRoot());
            FileUtil.cleanDirectory(compDir);
            compDir.delete();
        }
        

        bundles_.remove(component.getBundle().getBundleId());
    }
    
    /**
     * Invoke a component's 208 installer SPI.  This method also creates the
     * install and workspace roots for a component.
     * @param component the component bundle to install
     * @throws java.lang.Exception installation failed
     */
    void invokeComponentInstaller(ComponentBundle component)
            throws Exception {
        // Create the component's install and workspace roots
        File installRoot = new File(
                component.getComponentContext().getInstallRoot());
        File workspaceRoot = new File(
                component.getComponentContext().getWorkspaceRoot());
        
        installRoot.mkdirs();
        workspaceRoot.mkdirs();
        
        // JSR 208 requires an expanded component archive in install root
        BundleUtil.explodeContents(component.getBundle(), installRoot);
        
        /* Extract the component's install descriptor
        File metaInf = new File(installRoot, "META-INF");
        metaInf.mkdir();
        FileOutputStream fos = new FileOutputStream(
                new File(metaInf, "jbi.xml"));
        component.getDescriptor().writeTo(fos);
        fos.close();
         */
        
        // Load the component's bootstrap class and perform install
        Class installerClazz = component.getBundle().loadClass(
                component.getDescriptor().getComponent().getBootstrapClassName());
        InstallationContext installContext = new InstallationContext(component, true);
        Bootstrap installer = (Bootstrap)installerClazz.newInstance();
        installer.init(installContext);
        installer.onInstall();
        installer.cleanUp();
    }

}
