/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptionHelper.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import com.sun.jbi.framework.StringTranslator;
import org.glassfish.openesb.api.service.ServiceMessage;

import com.sun.jbi.framework.osgi.internal.BundleUtil;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.interceptors.Intercept;
import com.sun.jbi.interceptors.Interceptor;
import com.sun.jbi.interceptors.LocalStringKeys;


import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Properties;
import java.util.PriorityQueue;
import java.util.logging.Logger;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;
import javax.xml.namespace.QName;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Interception helper class with is used at the interception points to decide 
 * if a interceptor is to be triggered.
 */
public class InterceptionHelper 
{
    // TBD
    static int numberSuffix_ = 0;
    
    private static StringTranslator translator_ = new StringTranslator(
                "com.sun.jbi.interceptors", null);
    private static Logger mLog = Logger.getLogger("com.sun.jbi.interceptors");
    
    /**
     * Introspects the bundle and registers all interceptors in the OSGi service
     * registry.
     * @param bundle bundle to inspect
     * @return list of service registrations corresponding to the interceptors
     * that have been registered in the OSGi service registry
     * @throws java.lang.Exception failed while parsing bundle or registering
     * services
     */
    public static List<ServiceRegistration> registerInterceptorsInBundle(Bundle bundle) 
            throws Exception {
        mLog.finest("Registering interceptors in bundle " + bundle.getSymbolicName());
        List<ServiceRegistration> serviceList = new ArrayList<ServiceRegistration>();
        // Introspect each and every class and check for those with  
        // annotated methods, if there are any, wrap them and register them in 
        // the OSGi Service Registry
        List<Class> annotatedClasses = BundleUtil.getClassesWithInterceptAnnotations(bundle);
        
        for (Class annotatedClass : annotatedClasses) {
            Object instance  = annotatedClass.newInstance();
            // Wrap It
            InterceptorWrapper interceptor = new InterceptorWrapper(instance);
            
            Method[] interceptorMethods = interceptor.getInterceptorMethods();
            
            for ( Method method : interceptorMethods )
            {
            
                Intercept intAnnotation = (Intercept) method.getAnnotation(Intercept.class);

                Hashtable intMtdProps = getInterceptProperties(intAnnotation);
                intMtdProps = ConfigurationHelper.getInitialConfiguration(bundle, method, intMtdProps, true);
                
                if ( intMtdProps.get(ServiceProperty.NAME.toString()) == null )
                {
                    String name = generateInterceptorName(method);
                    intMtdProps.put(ServiceProperty.NAME.toString(), name.toString());
                }
                
                intMtdProps.put(ServiceProperty.METHOD.toString(), method);
                intMtdProps.put(ServiceProperty.RUNTIME_REGISTERED.toString(), true);
                intMtdProps.put(ServiceProperty.BUNDLE_ID.toString(), bundle.getBundleId());
                
                mLog.finest("Registering interceptor service for  " + bundle.getSymbolicName() + " with properties " + intMtdProps );
                ServiceRegistration svc = registerInterceptorService(interceptor, intMtdProps);
                serviceList.add(svc);
            }
        }
        
        return serviceList;
    }
    
    public static ServiceRegistration 
            registerInterceptorInBundle(Bundle bundle, 
                                        String className, 
                                        String methodName, 
                                        Hashtable config)
            throws Exception{
        
            Class clazz = bundle.loadClass(className);
        
            Object instance = clazz.newInstance();
            Object interceptor = instance;
            
            if ( !(instance instanceof Interceptor) ){
                 interceptor = new InterceptorWrapper(instance);
            }
            Method[] all = instance.getClass().getMethods();
            
            for ( Method method : all ){
                if ( method.getName().equals(methodName)){
                    config.put(ServiceProperty.METHOD.toString(), method);
                    config.put(ServiceProperty.RUNTIME_REGISTERED.toString(), true);
                    config.put(ServiceProperty.BUNDLE_ID.toString(), bundle.getBundleId());

                    ServiceRegistration svc = registerInterceptorService(interceptor, config);
                    return svc;
                }
            }
            throw new Exception("Method with name " + methodName
                    + " not declared in class " + clazz + " in bundle " + bundle.getSymbolicName() 
                    + " and id " + bundle.getBundleId());
    }
      
    
    
    /**
     * @return all the interceptor methods i.e. those that have been annotated 
     *         with the Intercept annotation. An empty array is returned if the 
     *         class does not have any interceptor methods.
     */
    public static Method[] getInterceptorMethods(Object instance)
    {
        if ( instance == null )
        {
            return new Method[0];
        }
        else
        {
            InterceptorWrapper interceptor = new InterceptorWrapper(instance);
            
            return interceptor.getInterceptorMethods();
        }
    }
    
    /**
     * @return the setConfiguration Method instance in the service class, if 
     *         one exists, null otherwise
     * 
     */
    public static Method getSetConfigurationMethod(ServiceReference serviceRef)
    {
            Object interceptor = getInterceptorInstance(serviceRef);
            
            Method mtd = null;
            try
            {
                mtd = interceptor.getClass().getMethod("setConfiguration", String.class, java.util.Properties.class);
            } 
            catch(java.lang.NoSuchMethodException ex)
            {
                String msg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_MISSING_SET_CONFIGURATION, 
                        "setConfiguration", interceptor.getClass().getName());
                mLog.fine(msg);
            }
            return mtd;
    }
 
    /**
     * @return the intercetopn object from the service reference
     * 
     */
    public static Object getInterceptorInstance(ServiceReference serviceRef)
    {
            Object svc = Environment.getFrameworkBundleContext().getService(serviceRef);
            Object interceptor = svc;
            
            // If wrapped ( runtime registered ) interceptor, get the actual object
            if ( svc instanceof InterceptorWrapper )
            {
                interceptor = ((InterceptorWrapper)svc).getInterceptor();
            }
            
            return interceptor;
    }
    
    /**
     * @return the Interceptor Method instance from the service properties
     *         if defined, null otherwise.
     *         Log a warning if method not defined
     */
    public static Method getInterceptorMethod(ServiceReference serviceRef)
    {
            Object intMtd =  serviceRef.getProperty(ServiceProperty.METHOD.toString());
            
            if ( intMtd != null )
            {
                if ( intMtd instanceof Method )
                {
                    return (Method) intMtd;
                }
                else
                {
                    String msg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_SVC_INVALID_METHOD_PROP, 
                        getInterceptorInstance(serviceRef).getClass().getName());
                    mLog.warning(msg);
                }
            }
            else
            {
                String msg = translator_.getString(
                    LocalStringKeys.INTERCEPTOR_SVC_MISSING_METHOD_PROP, 
                    getInterceptorInstance(serviceRef).getClass().getName());
                mLog.warning(msg);
            }
            return null;
    }
    
        
    /**
     * @return the Interceptor name from the service properties
     *         if defined, null otherwise.
     *         Log a warning if method not defined.
     */
    public static String getInterceptorName(ServiceReference serviceRef)
    {
            Object intName =  serviceRef.getProperty(ServiceProperty.NAME.toString());
            
            if ( intName != null )
            {
                return intName.toString(); 
            }
            else
            {
                String msg = translator_.getString(
                    LocalStringKeys.INTERCEPTOR_SVC_MISSING_NAME_PROP, 
                    getInterceptorInstance(serviceRef).getClass().getName());
                mLog.warning(msg);
            }
            return null;
    }
    
    /**
     * @return the bundle id of the bundle which contains the interceptor class. 
     *         For instance the runtime registers interceptor services for all
     *         interceptors detected in bundles. This would return the id of the
     *         bundle that contains the interceptor class and not the runtime 
     *         bundle id 
     */ 
     public static long getInterceptorBundleId(ServiceReference serviceRef)
     {
         long bundleId = serviceRef.getBundle().getBundleId();
         if ( isRuntimeRegisteredService(serviceRef) )
         {
             bundleId = (Long) serviceRef.getProperty(ServiceProperty.BUNDLE_ID.toString());
         }
         return bundleId;
     }
    
    /** 
     * Determines if a given bundle contains interceptors.
     * @param bundle bundle to inspect
     * @return true if the bundle contains interceptors, false otherwise
     */
    public static boolean isInterceptorBundle(Bundle bundle) {
        // We consider a bundle an interceptor if it specifically imports 
        // the interceptor package.  This is probably sub-optimal (e.g. a
        // dynamic-import will not qualify)
        String ipHeader = (String)bundle.getHeaders().get(org.osgi.framework.Constants.IMPORT_PACKAGE);
        return ipHeader != null &&
               ipHeader.contains(Interceptor.class.getPackage().getName());
    }
    
    /**
     * @return true if the service is registered by the runtime. 
     * 
     */
    public static boolean isRuntimeRegisteredService(ServiceReference svcRef)
    {
        Object value = svcRef.getProperty(ServiceProperty.RUNTIME_REGISTERED.toString());
        
        return (!(value == null));
        
    }
    
    /**
     *
     */
    public static boolean match(Properties actualProps, Properties svcProps)
    {
        boolean match = true;
        boolean svcMatchFound = false;
        boolean epMatchFound = false;
        int numTimesSvcCompared = 0;
        int numTimesEpCompared  = 0;
        
        if ( svcProps.isEmpty())
        {
            // If no properties specifies, default is to exclude
            return false;
        }

        if ( svcProps.containsKey(FilterProperty.ENDPOINT.toString()) && 
             svcProps.containsKey(FilterProperty.SERVICE.toString()) )
        {
            // If both service and endpoint are specified then we make sure
            // it matches the endpoint or linked endpoint
            match = matchServiceEndpoint(actualProps, svcProps);
            if (!match) 
            {
                return match;
            }
            
            // No need to compare the service / endpoints for equality again
            svcProps.remove(svcProps.getProperty(FilterProperty.ENDPOINT.toString()));
            svcProps.remove(svcProps.getProperty(FilterProperty.SERVICE.toString()));
        }
        
        Set exKeys = actualProps.keySet();
        
        for ( Object exKey : exKeys )
        {
            String key = (String) exKey;
            String exValue = (String) actualProps.get(key);
            
            if ( FilterProperty.ENDPOINT_LINK.toString().equals(key) )
            {
                key = FilterProperty.ENDPOINT.toString();
            }
            
            if ( FilterProperty.SERVICE_CONNECTION.toString().equals(key) )
            {
                key = FilterProperty.SERVICE.toString();
            }
            
            if ( svcProps.containsKey(key) )
            {
                // If the key is "service" then check for equlality with the
                // complete QName or just the local part
                if ( key.equals(FilterProperty.SERVICE.toString())  )
                {
                    numTimesSvcCompared++;
                    if ( !svcMatchFound )
                    {
                        match = compareServiceNames(exValue, svcProps.getProperty(key));
                        svcMatchFound = match;
                        if ( numTimesSvcCompared == 2 && !match )
                        {
                            break;
                        }
                    }
                }
                else if ( key.equals(FilterProperty.ENDPOINT.toString())  )
                {
                    numTimesEpCompared++;
                    if ( !epMatchFound )
                    {
                        match = compareStrings(exValue, svcProps.getProperty(key));
                        epMatchFound = match;
                        if ( numTimesEpCompared == 2 && !match )
                        {
                            break;
                        }
                    }
                }
                else if (!(compareStrings(exValue, svcProps.getProperty(key))))
                {
                    match = false;
                    break;
                }
            }
        }
        return match;
    }
    
    /**
     * @return true if the service reference properties match that of the exchange.
     * @param svcRef - OSGi Service Reference
     * @param actualProps  - Message Exchange Properties
     */
    public static boolean shouldTrigger(ServiceReference svcRef, Properties actualProps)
    {
        Set exKeys = actualProps.keySet();
        Properties intConfig = ConfigurationHelper.getConfiguration(svcRef);
        java.util.Enumeration svcKeys = intConfig.keys();
        Properties svcProps = new Properties();
        while ( svcKeys.hasMoreElements() )
        {
            String svcKey = svcKeys.nextElement().toString();
            if ( exKeys.contains(svcKey))
            {
                Object value = intConfig.getProperty(svcKey);
                svcProps.setProperty(svcKey, value.toString() );
            }
        }
        
        return match(actualProps, svcProps);
    }
    
    /**
     *
     */
    static Hashtable getInterceptProperties(Intercept intercept)
    {
        Hashtable table = new Hashtable();
        
        if ( !"".equals(intercept.consumer()))
        {
            table.put(FilterProperty.CONSUMER.toString(), intercept.consumer());
        }
        
        if ( !"".equals(intercept.provider()))
        {
           table.put(FilterProperty.PROVIDER.toString(), intercept.provider());
        }
        
        if ( !"".equals(intercept.message()))
        {
            table.put(FilterProperty.MESSAGE.toString(), intercept.message());
        }
        
        if ( !"".equals(intercept.status()))
        {
            table.put(FilterProperty.STATUS.toString(), intercept.status());
        }        
        
        if ( !"".equals(intercept.service()))
        {
            table.put(FilterProperty.SERVICE.toString(), intercept.service());
        }
        
        if ( !"".equals(intercept.endpoint()))
        {
            table.put(FilterProperty.ENDPOINT.toString(), intercept.endpoint());
        }
        
        if ( !"".equals(intercept.name()))
        {
            table.put(ServiceProperty.NAME.toString(), intercept.name());
        }
        
        if ( !"".equals(intercept.type()))
        {
            table.put(FilterProperty.TYPE.toString(), intercept.type());
        }
        
        table.put(FilterProperty.PRIORITY.toString(), intercept.priority());
        
        return table;
    }      
        
    /**
     * 
     * @param refs - service references to be sorted in order of ascending priority
     * @return - the prioritized list
     */
    public static PriorityQueue<ServiceReference> prioritySort(ServiceReference[] refs) {
        final String priority = FilterProperty.PRIORITY.toString();
        final Integer defaultPriority = new Integer(100);
        Comparator<ServiceReference> comp = new Comparator<ServiceReference> () {
            public int compare(ServiceReference ref1, ServiceReference ref2) {
                Properties ref1Config = ConfigurationHelper.getConfiguration(ref1);
                Properties ref2Config = ConfigurationHelper.getConfiguration(ref2);
                Integer p_ref1 = ref1Config.getProperty(priority) == null ? 
                    defaultPriority : Integer.parseInt((String)ref1Config.getProperty(priority));
                
                Integer p_ref2 = ref2Config.getProperty(priority) == null ? 
                    defaultPriority : Integer.parseInt((String)ref2Config.getProperty(priority));
                
                return p_ref1.compareTo(p_ref2);
            }
        };
        
        PriorityQueue<ServiceReference> set = new PriorityQueue<ServiceReference> (5, comp);
        for (ServiceReference ref : refs) {
            set.add(ref);
        }
        return set;
    }


    /**
     * Invoke the interceptor method. Based on the parameter type of the
     * method pass in the Exchange, the current message, it's payload
     * or message properties. If the method returns an object return that 
     * else return null.
     *
     * @param intMethod the interceptor method to invoke.
     * @param interceptor the interceptor object instance
     * @return boolean flag indicating whether the exchange is to be shorted.
     *         true=proceed normally, false = short the exchange.
     * @exception if the interceptor method throws an exception.
     */
    public  static boolean invokeInterceptor(Method intMethod, Object interceptor, 
            MessageExchange exchange) throws Exception
    {
        boolean proceed = true;

        Boolean invoked = false;
        
        if ( intMethod != null )
        {
            Class[] mtdParams = intMethod.getParameterTypes();
            
            if ( mtdParams.length <= 0 )
            {
                mLog.warning(translator_.getString(LocalStringKeys.INTERCEPTOR_WITH_ZERO_PARAMS,
                    new Object[] {intMethod.getName(), interceptor.getClass().getName() }));

            }
            else if ( mtdParams.length > 1 )
            {
                mLog.warning(translator_.getString(LocalStringKeys.INTERCEPTOR_WITH_EXTRA_PARAMS,
                    new Object[] {intMethod.getName(), interceptor.getClass().getName() }));
            }
            
            if ( mtdParams[0].isAssignableFrom(MessageExchange.class))
            {
                Object result = intMethod.invoke(interceptor, new Object[]{exchange});
                invoked = true;
                proceed = handleResult(exchange, intMethod, interceptor, result, InterceptorType.MESSAGE_EXCHANGE);
                
            }
            else if ( mtdParams[0].isAssignableFrom(ServiceMessage.class))
            {
                ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);

                // proceed only if there is a message to intercept
                if ( svcMsg != null )
                {
                    Object result = intMethod.invoke(interceptor, new Object[]{svcMsg});
                    invoked = true;
                    proceed = handleResult(exchange, intMethod, interceptor, result, InterceptorType.MESSAGE);
                }
                else
                {
                    mLog.warning(translator_.getString(LocalStringKeys.INTERCEPTOR_NOT_INVOKED_NULL_MSG,
                        new Object[] {intMethod.getName(), intMethod, interceptor.getClass().getName() }));
                }
            }
            else 
            {
                ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
                if ( svcMsg != null )
                {
                    Object payload = svcMsg.getPayload();
                    if ( mtdParams[0].isAssignableFrom(payload.getClass()))
                    {
                        Object result = intMethod.invoke(interceptor, new Object[]{payload});
                        invoked = true;
                        proceed = handleResult(exchange, intMethod, interceptor, result, InterceptorType.MESSAGE_PAYLOAD);
                    }
                }
            }
            if ( !invoked )
            {
                mLog.warning(translator_.getString(LocalStringKeys.INTERCEPTOR_NOT_INVOKED_AS_INVALID,
                    new Object[] {intMethod.getName(), 
                    interceptor.getClass().getName(), mtdParams[0].getName() }));
            }                
        }
        return proceed;
    }
    
    /**
     *
     */
    private static boolean handleResult(MessageExchange exchange, 
            Method intMethod, Object interceptor, Object result, 
            InterceptorType type)
        throws Exception
    {
        boolean proceed = true;
        boolean unsupportedResultType = false;
        
        // if the return type of the interceptor method is void, do nothing, return true
        if ( !( intMethod.getReturnType() == void.class ) )
        {
            if ( result != null & result instanceof Boolean )
            {
                if ( type.equals(InterceptorType.MESSAGE_EXCHANGE) )
                {
                    proceed = ((Boolean) result).booleanValue();
                }
            }
            else
            {
              switch (type)
              {
                  case MESSAGE_EXCHANGE:
                      // result can be : MessageExchange or null
                      if ( result == null )
                      {
                          proceed = false;
                      }
                      else
                      {
                          unsupportedResultType = !( result instanceof MessageExchange );
                      }
                      break;
                  case MESSAGE:
                      // result can be instance of NormalizedMessage 
                      unsupportedResultType = !( result instanceof NormalizedMessage );
                      break;
                      
                  case MESSAGE_PAYLOAD:
                      // result can be any non null object
                      if ( result != null )
                      {
                          ServiceMessage msg = MessageExchangeUtil.getActiveMessage(exchange);
                          if ( msg != null )
                          {
                              msg.setPayload(result);
                          }
                      }
                      break;

              }
              if ( unsupportedResultType )
              {
                  mLog.warning(translator_.getString(LocalStringKeys.INTERCEPTOR_RETURNS_UNSUPPORTED_TYPE,
                            new Object[] {type.toString(), intMethod.getName(), 
                                interceptor.getClass().getName(), Boolean.class.getName() }));
              }
            }
        }
        return proceed;
    }
    
    /**
     * Compare two service names. If the intServiceName matches either local part
     * or the entire qualified servicename exServiceName, a true value is returned.
     * 
     * @param exServiceName - fully qualified service name 
     * @param intServiceName - either partial ( local part only ) or 
     *        fully qualified service name.
     * @return true if the interceptor service name matches the exchange service
     *         names local part or the fully qualified QName
     */
    public static boolean compareServiceNames(String exServiceName, String intServiceName)
    {
        boolean isEqual;
        
        QName exSvcQName  = QName.valueOf(exServiceName);
        QName intSvcQName = QName.valueOf(intServiceName);
        
        isEqual = compareServiceQNames(exSvcQName, intSvcQName);
        
        if ( (intSvcQName.getNamespaceURI() == null || "".equals(intSvcQName.getNamespaceURI())) && !isEqual )
        {
            // If Interceptor service only has the local part
            isEqual = compareStrings(exSvcQName.getLocalPart(), intSvcQName.getLocalPart());
        }
        
        return isEqual;
        
    }
    
    /**
     * Compare two QNames. The value of the namespace URI or the local part can 
     * be a regular expression. 
     * 
     * @param qname1 - first  QName for comparison
     * @param qname2 - second QName for comparison
     */
    static boolean compareServiceQNames(QName qname1, QName qname2)
    {
        boolean matches = qname1.equals(qname2);
        
        if (!matches)
        {
            String qname1URI = qname1.getNamespaceURI();
            String qname2URI = qname2.getNamespaceURI();

            if ( compareStrings(qname1URI, qname2URI) )
            {
                String qname1LocalPart = qname1.getLocalPart();
                String qname2LocalPart = qname2.getLocalPart();

                matches = compareStrings(qname1LocalPart, qname2LocalPart);
            }
        }
        
        return matches;
    }
    
    /**
     * Look for a match for the service and endpoint duo in the exchange 
     * properties either a endpointLink/ServiceConnection should match 
     * or an endpoint/service.
     * 
     *  @param exchangeProps - exchange properties
     *  @param svcProps - service properties
     *  @return true if a match is found false otherwise.
     */
    static boolean matchServiceEndpoint(Properties exchangeProps, Properties svcProps)
    {
        boolean match = false;
        String intService = svcProps.getProperty(FilterProperty.SERVICE.toString());
        String intEndpoint = svcProps.getProperty(FilterProperty.ENDPOINT.toString());
        
        String exService = exchangeProps.getProperty(FilterProperty.SERVICE.toString());
        String exEndpoint = exchangeProps.getProperty(FilterProperty.ENDPOINT.toString());
        
        if ( compareServiceNames(exService, intService) )
        {
            // Endpoints should match
            match = compareStrings( exEndpoint, intEndpoint);
            if ( !match ) return match;
        }
        
        // Compare with the EndpointLink
        String exServiceCon = exchangeProps.getProperty(FilterProperty.SERVICE_CONNECTION.toString());
        String exEndpointLink = exchangeProps.getProperty(FilterProperty.ENDPOINT_LINK.toString());
        
        if ((exServiceCon != null) && (exEndpointLink != null))
        {
            if ( compareServiceNames(exServiceCon, intService) )
            {
                // Endpoints should match
                match = compareStrings(exEndpointLink, intEndpoint);
            }
        }
        return match;
    }
    
    /**
     * Compare the string literal to the second string which may be a regex
     * First do a string compare and then a regex match.
     * 
     * @param literal - string literal
     * @param regex - string which could be a literal or a regex
     * @return true if a macth is found 
     */
    static boolean compareStrings(String literal, String regex)
    {
        boolean match = true;
        
        if ( !literal.equalsIgnoreCase(regex))
        {
            match = literal.matches(regex);
        }
        
        return match;
    }
    
    /**
     * This method is used to generate the interceptor name when a name is 
     * not provided in the Intercept annotation, service properties and the
     * interceptor configuration file is missing.
     * @param method - the interceptor Method instance
     * @return a unique string name for the interceptor
     * 
     */
    public static String generateInterceptorName(Method method)
    {
        StringBuffer genName = new StringBuffer();
        
        // Package/Classname
        genName.append(method.getDeclaringClass().getName());
        genName.append(".");

        // Method
        genName.append(method.getName());

        return genName.toString();
    }
    
    /**
     * 
     * @param propName
     * @param value
     * @return true if an interceptor with the same property value is registered
     * @throws java.lang.Exception
     */
    private static boolean isDuplicateInterceptor(String propName, String value)
        throws Exception {
        
        boolean isDup = false;
        BundleContext ctx = Environment.getFrameworkBundleContext();
        
        StringBuffer propQuery = new StringBuffer("(");
        propQuery.append(propName);
        propQuery.append("=");
        propQuery.append(value);
        propQuery.append(")");
        ServiceReference[] svcRefs = ctx.getServiceReferences("com.sun.jbi.interceptors.Interceptor", propQuery.toString());
        
        if (svcRefs != null ){
            if ( svcRefs.length > 0 ){
                isDup = true;
            }
        }
        
        return isDup; 
    }

    /**
     * Register an interceptor service
     * 
     * @param interceptor
     * @param intMtdProps
     * @return the OSGi Service Registration for the interceptor.
     * @throws java.lang.Exception
     */ 
    private static ServiceRegistration registerInterceptorService(Object interceptor, Hashtable intMtdProps)
        throws Exception {
        
                boolean isDynamicInstance = false;
                Object val = intMtdProps.get(ServiceProperty.IS_DYNAMIC_INSTANCE.toString());
                if ( val != null ){
                    isDynamicInstance = (Boolean)val;
                }
                
                // Check for duplicate type
                if ( isDuplicateInterceptor(FilterProperty.TYPE.toString(), (String)intMtdProps.get(FilterProperty.TYPE.toString()) )
                        && !isDynamicInstance){
                    String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_DUPLICATE_TYPE, intMtdProps.get(FilterProperty.TYPE.toString()));
                    throw new Exception(errMsg);
                }
                
                // Check for duplicate name
                String tmpName = (String)intMtdProps.get(ServiceProperty.NAME.toString());
                String oldName = tmpName;
                int i=1;
                while ( isDuplicateInterceptor(ServiceProperty.NAME.toString(), tmpName ) ){
                    mLog.fine("Interceptor with name " + tmpName + " exists, generating a new name");
                    tmpName = tmpName.concat(Integer.toString(i++));
                }
                
                if ( !oldName.equals(tmpName) ){
                    String infoMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_RENAMED, tmpName, oldName ); 
                    mLog.info(infoMsg);
                    intMtdProps.put(ServiceProperty.NAME.toString(), tmpName); 
                }
                
                ServiceRegistration svc = Environment.getFrameworkBundleContext().
                        registerService(Interceptor.class.getName(), 
                        interceptor, intMtdProps); 
                return svc;
    }    
    

}
