/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)IFLReader.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.ifl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Logger;
/**
 * This class has been deprecated in favour of using the 
 * com.sun.jbi.fuji.ifl.IFLReader from the IFL Library.
 * @deprecated use com.sun.jbi.fuji.ifl.IFLReader
 */
public class SimpleIFLReader {

    /** IFL Keywords **/
    private static final String FROM    = "from";
    private static final String TO      = "to";
    private static final String ROUTE   = "route";
    private static final String END     = "end";
    
    private HashMap<String, String> services_ = new HashMap<String, String>();
    private HashMap<String, String> connections_ = new HashMap<String, String>();
    private List<String> consumedServices_ = new ArrayList<String>();
    private List<String> providedServices_ = new ArrayList<String>();
    private HashMap<String, List<String>> serviceFilters_ = 
            new HashMap<String, List<String>>();
    private Logger log_ = Logger.getLogger(
            SimpleIFLReader.class.getPackage().getName());

    public SimpleIFLReader() {
        
    }
    
    
    public void parse(File iflFile) throws Exception {
        try {
            parseIFL(new FileInputStream(iflFile));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void parse(InputStream in) throws Exception {
        parseIFL(in);
    }
    
    public void parse(File ... files) throws Exception {
        for (File f : files) {
            parse(f);
        }
    }
    
    public void reset() {
        services_.clear();
        consumedServices_.clear();
        providedServices_.clear();
    }
    
    public List<String> getServicesForType(String serviceType) {
        List<String> services = new ArrayList<String>();
        for (String serviceName : services_.keySet()) {
            if (services_.get(serviceName).equals(serviceType)) {
                services.add(serviceName);
            }
        }
        return services;
    }
    
    public Set<Map.Entry<String, String>> getConnections() {
        return Collections.unmodifiableSet(connections_.entrySet());
    }
    
    public List<String> getConsumedServices() {
        return Collections.unmodifiableList(consumedServices_);
    }
    
    public List<String> getProvidedServices() {
        return Collections.unmodifiableList(providedServices_);
    }
    
    public boolean isConsumed(String serviceName) {
        return consumedServices_.contains(serviceName);
    }
    
    public boolean isProvided(String serviceName) {
        return providedServices_.contains(serviceName);
    }
    
    public Set<String> getServiceTypes() {
        return new HashSet(services_.values());
    }
    
    public List<String> getFiltersForService(String serviceName) {
        return serviceFilters_.get(serviceName);
    }
    
    /**
     * This parser is D-U-M-B.  It expects a strict structure of:
     *    service declaration(s)
     *    [whitespace-only line]
     *    flow instructions
     * 
     * So you can only declare services up top and you must put a 
     * line of whitespace between the declarations and flow instructions.
     * No real point in making this any prettier, since Andi is working
     * on a full-blown language parser.
     */
    private void parseIFL(InputStream in) throws Exception {
        
        BufferedReader iflReader = null;
        
        try {
            iflReader = new BufferedReader(new InputStreamReader(in));

            String iflLine;
            
            while ((iflLine = iflReader.readLine()) != null) {
                // comments are ignored
                if (iflLine.startsWith("#") || iflLine.trim().length() == 0) {
                    continue;
                }
                
                // check for route declaration
                if (iflLine.startsWith(ROUTE)) {
                    parseRoute(iflReader);
                    continue;
                }

                // looks like we have a service declaration
                StringTokenizer st = new StringTokenizer(iflLine, " ");
                if (st.countTokens() != 2) {
                    // not something we understand
                    log_.warning("Ignoring IFL service declaration: [" +
                            iflLine + "]");
                    continue;
                }
                String serviceType = st.nextToken().toLowerCase();
                String serviceName = st.nextToken().replace("\"", "").toLowerCase();
                services_.put(serviceName, serviceType);
            }
        } finally {
            if (iflReader != null) {
                try {iflReader.close();} 
                catch (Exception ex) {}
            }
        }
    }
    
    private void parseRoute(BufferedReader reader) throws Exception {
        String iflLine;
        String fromService = null;
        ArrayList<String> toServices = new ArrayList<String>();
        
        while ((iflLine = reader.readLine()) != null) {
            // comments are ignored
            if (iflLine.startsWith("#") || iflLine.trim().length() == 0) {
                continue;
            }

            iflLine = iflLine.trim();
            // see if we have a consume or provide declaration
            if (iflLine.startsWith(TO)) {
                toServices.add(getServiceNameFromRoute(iflLine));
            }
            else if (iflLine.startsWith(FROM)) {
                fromService = getServiceNameFromRoute(iflLine);
            }
            else if (iflLine.startsWith(END)) {
                consumedServices_.add(fromService);
                for (String provides : toServices) {
                    providedServices_.add(provides);
                }
                if (toServices.size() > 0) {
                    // get the provider service and remove from the "to" list
                    // so we can process any other filter services
                    String providerService = toServices.remove(toServices.size() - 1);
                    connections_.put(fromService, providerService);
                    // add the rest of the to addresses to the filter list
                    serviceFilters_.put(fromService, toServices);
                }
                
                // done with route processing
                break;
            }
        }
    }
    
    /** Yank the service name out of a routing instruction.
     * 
     * @param route
     * @return
     */
    private String getServiceNameFromRoute(String route) {
        // trim out the whitespace and keywords
        String serviceName = route.
                replace(FROM, "").
                replace(TO, "").
                replace("\"", "").
                trim();
        // routing may include operation/message name, so we 
        // need to remove that too
        if (serviceName.contains(".")) {
            serviceName = serviceName.substring(0, serviceName.indexOf("."));   
        }
        
        return serviceName.toLowerCase();
    }
}
