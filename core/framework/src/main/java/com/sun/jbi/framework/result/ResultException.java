/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ResultException.java
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import javax.jbi.management.DeploymentException;

/**
 * Utility exception to wrap {@link DeploymentException}s with messages which
 * conform to the component-task-result type from the schema given in the 
 * <iMBean Status and Result Strings</i> section of the Management chapter of 
 * the JBI specification.
 * 
 * @author ksimpson
 */
public class ResultException extends DeploymentException {
    private ComponentTaskResult mCompTaskResult;
    
    public ResultException(DeploymentException cause) {
        this(cause, null);
    }
    
    public ResultException(DeploymentException dex, ComponentTaskResult taskResult) {
        super(dex);
        mCompTaskResult = (taskResult != null) ? taskResult
                : JbiTask.createComponentTaskResult(dex.getMessage());;
    }
    
    public ComponentTaskResult getComponentTaskResult() {
        return mCompTaskResult;
    }

    /**
     * Prints a user-friendly version of the {@link ComponentTaskResult}, if present.
     * @see java.lang.Throwable#toString()
     */
    @Override
    public String toString() {
        return (getComponentTaskResult() == null)
                ? super.toString() : getComponentTaskResult().display();
    }
    
}
