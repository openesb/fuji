/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.jbi.framework.osgi.internal;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;

/**
 * Service factory for MessageService intances.  This class is called upon
 * by the OSGi framework each time a bundle requests
 *
 * @author AlexanderPermyakov
 */
public class MessageServiceFactory implements ServiceFactory {

    public Object getService(Bundle arg0, ServiceRegistration arg1) {
        return com.sun.jbi.framework.osgi.internal.Environment.getMessageService();
    }

    public void ungetService(Bundle arg0, ServiceRegistration arg1, Object arg2) {
        return;
    }

}
