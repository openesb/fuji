/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import com.sun.jbi.framework.osgi.internal.Environment;

import java.io.File;
import java.util.logging.Logger;
import javax.management.ObjectName;

/**
 *
 * @author kcbabo
 */
public class InstallationService 
        implements javax.jbi.management.InstallationServiceMBean {
    
    private Logger logger_ = Logger.getLogger(
            InstallationService.class.getPackage().getName());
    
    public InstallationService() {
    }

    public String installSharedLibrary(String slZipURL) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ObjectName loadInstaller(String componentName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ObjectName loadNewInstaller(String installZipURL) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean uninstallSharedLibrary(String slName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean unloadInstaller(String componentName, boolean isToBeDeleted) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
