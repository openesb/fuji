/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageService.java - Last published on 3/13/2008
 *
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;


import com.sun.jbi.ext.ExchangeListener;
import com.sun.jbi.ext.ExchangeDescriptor;
import com.sun.jbi.ext.MessageDescriptor;
import com.sun.jbi.ext.MessageServices;
import com.sun.jbi.ext.TimeoutListener;
import com.sun.jbi.ext.ResolverExtension;

import com.sun.jbi.framework.StringTranslator;
import com.sun.jbi.framework.osgi.internal.ComponentManager;
import com.sun.jbi.messaging.stats.METimestamps;
import com.sun.jbi.messaging.stats.Value;
import com.sun.jbi.messaging.stats.Value.Kind;
import com.sun.jbi.messaging.util.ExchangeDescriptorImpl;
import com.sun.jbi.messaging.util.Translator;
import com.sun.jbi.messaging.util.XMLUtil;
import com.sun.jbi.messaging.util.WSDLHelper;
import com.sun.jbi.interceptors.Interceptor;
import com.sun.jbi.interceptors.internal.InterceptorWrapper;
import com.sun.jbi.interceptors.internal.InterceptionHelper;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.jbi.component.Component;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;

import javax.xml.namespace.QName;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;

/**
 * Entry point to NMS for framework and management code.
 * @author Sun Microsystems, Inc.
 */
public class MessageService implements MessageServices, ExchangeListener
{
    private EndpointRegistry    mRegistry;

    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
    /** Hook to framework to retrieve component instances.*/
    private ComponentManager mComponentManager;
    
    /** Reference to TransactionManager if one exists. */
    private TransactionManager mTransactionManager;
    
    /** Table used to store channel references. */
    private Hashtable               mChannels;

    /** ExchangeIdGenerator for unique exchange Ids. */
    private ExchangeIdGenerator     mGenerator;
    
    /** The framework bundle context */
    private BundleContext    mBundleContext;

    private Hashtable<String, TimeoutListener>    mListeners;
    
    private Map<ExchangeListener, String>          mExchangeListeners;
    private Object[]  mELs;

    /** XAResources private to JBI Components. */
    LinkedList mResources;

       /** Basic statistics.       */
    private long                    mStatisticsEpoch;
    private long                    mTotalChannels;
    long                            mAcceptCount;
    long                            mAcceptTimeoutCount;
    long                            mSendCount;
    long                            mSendSyncCount;
    private long                    mSendRequest;
    private long                    mReceiveRequest;
    private long                    mSendReply;
    private long                    mReceiveReply;
    private long                    mSendFault;
    private long                    mReceiveFault;
    private long                    mSendDONE;
    private long                    mReceiveDONE;
    private long                    mSendERROR;
    private long                    mReceiveERROR;
    private long                    mInOutMEPs;
    private long                    mInOnlyMEPs;
    private long                    mRobustInOnlyMEPs;
    private long                    mInOptionalOutMEPs;
    private Value                   mResponseTime;
    private Value                   mStatusTime;
    private Value                   mNMRTime;
    private Value                   mConsumerTime;
    private Value                   mProviderTime;
    private Value                   mChannelTime;

    private AtomicLong              mActiveExchanges;
    private AtomicLong              mTotalExchanges;

    private boolean                 mMonitoringEnabled;

    /** Create a new instance of the NMS. */
    public MessageService()
    {
        mChannels  = new Hashtable();
        mListeners = new Hashtable();
        mResources = new LinkedList();
        mExchangeListeners = new IdentityHashMap();
        mELs = null;
        mRegistry  = EndpointRegistry.getInstance();
        mActiveExchanges = new AtomicLong();
        mTotalExchanges = new AtomicLong();
        mResponseTime = new Value(Kind.INTERVAL_NS);
        mProviderTime = new Value(Kind.INTERVAL_NS);
        mConsumerTime = new Value(Kind.INTERVAL_NS);
        mChannelTime = new Value(Kind.INTERVAL_NS);
        mStatusTime = new Value(Kind.INTERVAL_NS);
        mNMRTime = new Value(Kind.INTERVAL_NS);
        
        // init string translator
        Translator.setStringTranslator(new StringTranslator(
                "com.sun.jbi.messaging", null));
        //addListener((ExchangeListener)this, null);
        mMonitoringEnabled = false;
    }

    void createMBeans()
    {
    }
    /******************************************
     *      ServiceLifecycle methods          *
     ******************************************/
    
    
    /** Start the NMS. */
    public void startService() 
        throws javax.jbi.JBIException
    {        
        mGenerator = (com.sun.jbi.messaging.ExchangeIdGenerator)new ExchangeIdGeneratorImpl();
        mRegistry.setMessageService(this);

        //
        //  Register MessageService Statistics MBean.
        //
        try {
            javax.management.MBeanServer mbs = java.lang.management.ManagementFactory.getPlatformMBeanServer();
            javax.management.ObjectName name;
            MessageServiceStatistics    mss = new MessageServiceStatistics(this);


            name = new javax.management.ObjectName("com.sun.jbi:ServiceName=MessagingService,ControlType=Statistics,ComponentType=System");
            if (mbs.isRegistered(name)) {
                mbs.unregisterMBean(name);
            }
            mbs.registerMBean(mss, name);
            mss.setLastRestartTime(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            mLog.warning("Can't register MessageService MBeans: " + e);
        }
    }
    
    /** Stop the NMS. */
    public void stopService()
        throws javax.jbi.JBIException
    {
        DeliveryChannelImpl channel;
        Enumeration         e;

        // make sure all of the channels are closed
        e = mChannels.elements();
        while (e.hasMoreElements())
        {
            channel = (DeliveryChannelImpl)e.nextElement();
            channel.close();
        }
        
        // clear the channel table
        mChannels.clear();
        // clear the registry
        mRegistry.clear();


        //
        //  Unregister MessageService Statistics MBean.
        //
        try {
            javax.management.MBeanServer mbs = java.lang.management.ManagementFactory.getPlatformMBeanServer();
            javax.management.ObjectName name;

            name = new javax.management.ObjectName("com.sun.jbi:ServiceName=MessagingService,ControlType=Statistics,ComponentType=System");
            if (mbs.isRegistered(name)) {
                mbs.unregisterMBean(name);
            }
        } catch (Exception ignore) {

        }
    }
    
    /******************************************
     *          Framework methods             *
     ******************************************/
    
    /** Create a new delivery channel.  If the channel has already been created,
     *  this method throws an exception.
     *  @param componentId component id
     *  @param component component's implementation of Component interface
     *  @throws javax.jbi.messaging.MessagingException attempt to activate 
     *   duplicate channel.
     */
    public DeliveryChannelImpl activateChannel(String componentId,
        Component component)
        throws javax.jbi.messaging.MessagingException
    {
        DeliveryChannelImpl dc;
        
        if (mChannels.containsKey(componentId))
        {
            dc = (DeliveryChannelImpl)mChannels.get(componentId);
        }
        else
        {
            if (mLog.isLoggable(Level.FINE))
            {
                mLog.fine("Created DeliveryChannel for component: " + componentId);
            }
            dc = new DeliveryChannelImpl(componentId, component, this);
            addChannel(dc);
        }
        
        return dc;
    }       
    
    /** Get a reference to the NMR ConnectionManager.
     *  @return instance of ConnectionManager.
     */
    public ConnectionManager getConnectionManager()
    {
        return mRegistry;
    }
    
    /**
     * Set the MessageInterceptor reference
     */
    public void setBundleContext(BundleContext ctx)
    {
        mBundleContext = ctx;
    }
    
    public BundleContext getBundleContext()
    {
        return (mBundleContext);
    }
    
    public void addTimeoutListener(String id, TimeoutListener tl)
    {
        if (tl == null)
        {
            mListeners.remove(id);
        }
        else
        {
            mListeners.put(id, tl);
        }
    }
    
    synchronized public void announceTimeout(MessageExchange me)
            throws java.lang.Exception
    {
        for (TimeoutListener tl : mListeners.values())
        {
            tl.onTimeout(me);
        }
    }
    
    /******************************************
     *         Internal NMS methods           *
     ******************************************/
    
    /** Adds a channel to the NMS routing table.
     */
    void addChannel(DeliveryChannelImpl channel)
    {
        mChannels.put(channel.getChannelId(), channel);
    }
    
    /** Removes a channel from the NMS routing table.
     */
    void removeChannel(String channelId)
    {
        mChannels.remove(channelId);
    }
    
    /** Retrieves a channel based on it's ID from the NMS routing table.
     */
    DeliveryChannelImpl getChannel(String channelId)
    {
        return (DeliveryChannelImpl)mChannels.get(channelId);
    }

    EndpointRegistry getEndpointRegistry()
    {
        return (mRegistry);
    }

    /** Get the TransactionManager
     */
    javax.transaction.TransactionManager getTransactionManager()
    {
        // See if we can find a TransactionManager
        if (mTransactionManager == null)
        {
            try {
                javax.naming.InitialContext ic = new javax.naming.InitialContext();
                mTransactionManager = (javax.transaction.TransactionManager)ic.lookup("java:appserver/TransactionManager");
            } catch (javax.naming.NamingException nEx) {
                mLog.fine("JBI-MessageService: No TransactionManager");
            }
        }
        return mTransactionManager;
    }
    
    public void addXAResource(XAResource resource)
    {
        mResources.add(resource);
    }
    
    public javax.transaction.xa.XAResource[] getXAResources()
    {
        XAResource[]        resources = new XAResource[mResources.size()];
        
        mResources.toArray(resources);
        return (resources);
    }
    
    public void purgeXAResources()
    {
        mResources.clear();
    }
    
    /**
     * Set the TransactionManager. Used by the junit test harness to bypass using
     * EnvironmentContext (which is not available.)
     */
    void setTransactionManager(javax.transaction.TransactionManager tm)
    {
        mTransactionManager = tm;
    }
    
    /** Perform the exchange operation.
     */
    boolean doExchange(DeliveryChannelImpl channel, MessageExchangeProxy exchange)
        throws javax.jbi.messaging.MessagingException
    {
        DeliveryChannelImpl     targetChannel;
        MessageExchangeProxy    targetExchange;
        boolean                 isLast = false;
        
        addressExchange(exchange, channel);
        exchange.validate(channel, false);
        notifyExchangeListeners(exchange, true);

        boolean proceed = interceptSendExchange(channel, exchange, false);
        
        if ( !proceed ) 
        {
            return (true);
        }
        
        targetChannel = exchange.getSendChannel();
        targetExchange = exchange.getTwin();
        if (exchange.handleSend(channel))
        {
            channel.removeSendReference(exchange);
            isLast = true;
        }
        else
        {
            channel.addSendReference(exchange);
        }

        javax.jbi.messaging.MessagingException e = targetChannel.queueExchange(targetExchange);
        if (e != null) {
            channel.removeActive(exchange);
            throw e;
        }
        return (isLast);
    }
    
    /** Perform the exchange operation.
     */
    boolean doSynchExchange(DeliveryChannelImpl channel, 
            MessageExchangeProxy exchange,
            long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        DeliveryChannelImpl     targetChannel;
        MessageExchangeProxy    targetExchange;
        boolean                valid = true;
        boolean                timedout = false;
        
        addressExchange(exchange, channel);
        exchange.validate(channel, true);
        notifyExchangeListeners(exchange, true);
        
        // -- Trigger the Message Exchange Interceptor, this will
        //    further trigger all the MessageValidators
        boolean proceed = interceptSendExchange(channel, exchange, true);
        
        if ( !proceed )
        {
            return valid;
        }
        
        targetChannel = exchange.getSendChannel();
        targetExchange = exchange.getTwin();
        exchange.handleSendSync(channel);
        channel.addSendSyncReference(exchange);
        synchronized (exchange)
        {
            exchange.setSynchState(MessageExchangeProxy.WAIT);
            javax.jbi.messaging.MessagingException e = targetChannel.queueExchange(targetExchange);
            if (e != null) {
                channel.removeActive(exchange);
                throw e;
            }
            try
            {
                for (;;)
                {
                    int		state;
					
                    exchange.wait(timeout);
                    state = exchange.getSynchState();
                    if (state == MessageExchangeProxy.HALF_DONE)
                    {
                        exchange.setSynchState(MessageExchangeProxy.DONE);
                        break;
                    }
                    else if (state == MessageExchangeProxy.ERROR)
                    {
                        valid = false;
                        break;
                    }
                    if (timeout != 0)
                    {
                        if (state == MessageExchangeProxy.WAIT)
                        {
                            e = new javax.jbi.messaging.MessagingException(
                                Translator.translate(LocalStringKeys.MESSAGE_TIMEOUT));
                            exchange.terminate(e, false);
                            valid = false;
                            break;
                        }
                        else if (state == MessageExchangeProxy.WAIT_TIMEOUT)
                        {
                            timedout = true;
                            break;
                        }
                    }
                }
            }
            catch (java.lang.InterruptedException iEx)
            {
                e = new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MESSAGE_TIMEOUT));
                exchange.terminate(e, false);
                valid = false;
            }
        }
        /*
        if (timedout)
        {
            // Derek - what the heck does this do?
            if (mTimeout.checkTimeout(channel, exchange))
            {
                exchange.terminate();
            }
            else
            {
                exchange.setSynchState(MessageExchangeProxy.NONE);
                valid = false;
            }
        }
         */
        return (valid);
    }
    
    
    void addressExchange(MessageExchangeProxy exchange, DeliveryChannelImpl channel)
        throws javax.jbi.messaging.MessagingException
    {        
        RegisteredEndpoint  endpoint;
        QName               serviceName;
        QName               interfaceName;
        
        if (exchange.getSendChannel() != null)
        {
            return;
        }
        //
        // Check addressing of exchange in this order:
        //    (1) endpoint
        //    (2) service name
        //    (3) interface name
        //
        endpoint = (RegisteredEndpoint)exchange.getActualEndpoint();
        
        if (endpoint == null)
        {
            if ((serviceName = exchange.getService()) != null)
            {
                RegisteredEndpoint[]    endpoints;
                RegisteredEndpoint      match;
                
                endpoints = mRegistry.getInternalEndpointsForService(serviceName, true);
                if (endpoints.length != 0)
                {                    
                    endpoint = matchConsumerAndProvider(channel.getChannelId(), endpoints, exchange);
                }
                else
                {
                    throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(
                            LocalStringKeys.CANT_FIND_ENDPOINT_FOR_SERVICE,
                            new Object[] { serviceName.toString() }));
                }
            }
            else if ((interfaceName = exchange.getInterfaceName()) != null)
            {                
                RegisteredEndpoint[]    endpoints;
                RegisteredEndpoint      match;
                
                endpoints = mRegistry.getInternalEndpointsForInterface(interfaceName);
                if (endpoints.length != 0)
                {
                    endpoint = matchConsumerAndProvider(channel.getChannelId(), endpoints, exchange);
                }
                else
                {
                    throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(
                            LocalStringKeys.CANT_FIND_ENDPOINT_FOR_INTERFACE,
                            new Object[] { interfaceName.toString() }));
                }
            }
            else
            {      
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.ADDR_NO_ENDPOINT));
            }
            exchange.setEndpoint(endpoint);
        }
            
        /** Check to see if we are dealing with a real endpoint or a 
         *  linked endpoint from a service connection.
         */
        if (endpoint.isLinked())
        {            
            exchange.setEndpointLink(endpoint);
            endpoint = mRegistry.resolveLinkedEndpoint((LinkedEndpoint)endpoint);
            
            // check for resolution failure
            if (endpoint == null)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.SERVICE_CONNECTION_NO_ENDPOINT,
                        new Object[] {exchange.getEndpointLink().getServiceName(),
                                      exchange.getEndpointLink().getEndpointName()})); 
            }
            exchange.setEndpoint(endpoint);
        }
    }
    
    /** Facilitates intercomponent meta data queries.
     */
    Document queryDescriptor(ServiceEndpoint ref, boolean flatten)
    {
        Component   component;
        Document    descriptor = null;
        
        if (ref instanceof LinkedEndpoint)
        {
            ref = mRegistry.resolveLinkedEndpoint((LinkedEndpoint)ref);
            // return immediately if this is a dead link
            if (ref == null)
            {
                return null;
            }
        }
        
        component = getChannel(
                ((RegisteredEndpoint)ref).getOwnerId()).getComponentInstance();
        if (component != null)
        {
            descriptor = component.getServiceDescription(ref);
            if (flatten && (component instanceof ResolverExtension)) {
                WSDLHelper.flatten(ref.getServiceName(),  descriptor, ((ResolverExtension)component).getServiceDescriptionResolver(ref));
            }
        }
        
        return descriptor;
    }

    EntityResolver queryResolver(ServiceEndpoint ref)
    {
        Component   component;
        EntityResolver  resolver = null;

        if (ref instanceof LinkedEndpoint)
        {
            ref = mRegistry.resolveLinkedEndpoint((LinkedEndpoint)ref);
            // return immediately if this is a dead link
            if (ref == null)
            {
                return null;
            }
        }

        component = getChannel(
                ((RegisteredEndpoint)ref).getOwnerId()).getComponentInstance();

        if (component != null)
        {
            if (component instanceof ResolverExtension) {
                resolver = ((ResolverExtension)component).getServiceDescriptionResolver(ref);
            }
        }

        return (resolver);
    }
    
    DynamicEndpoint resolveEndpointReference(org.w3c.dom.DocumentFragment reference)
    {
        Set             channels;
        DynamicEndpoint dep = null;
        
        channels = ((Hashtable)mChannels.clone()).keySet();
        
        for (Iterator i = channels.iterator(); i.hasNext(); )
        {
            String           id;
            Component        comp;
            ServiceEndpoint  ep;
            
            id   = (String)i.next();
            comp = getChannel(id).getComponentInstance();
            if (comp != null)
            {
                ep   = comp.resolveEndpointReference(reference);

                if (ep != null)
                {
                    dep = new DynamicEndpoint(ep, id, reference);
                    break;
                }
            }
        }
        
        return dep;
    }
    
    /** Convenience method used in unit tests. */
    void setComponentManager(ComponentManager compMgr)
    {
        mComponentManager = compMgr;
    }
    
    /** Make sure that consumer and provider are cool with exchange. */
    RegisteredEndpoint matchConsumerAndProvider(String consumerId,
        RegisteredEndpoint[] endpoints, MessageExchangeProxy exchange)
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint  match = null;
        Component           consumer;
        Component           provider;
        
        consumer = getChannel(consumerId).getComponentInstance();        
        
        if (endpoints.length == 1)
        {
            // No room to be picky
            match = endpoints[0];
        }
        else
        {            
            try
            {
                for (int i = 0; i < endpoints.length; i++)
                {
                    provider = getChannel(endpoints[i].getOwnerId()).
                            getComponentInstance();
                    exchange.beforeCapabilityCheck(endpoints[i]);
                    
                    // make sure provider and consumer are cool with values
                    if ((consumer == null || consumer.isExchangeWithProviderOkay(endpoints[i], exchange)) &&
                        (provider == null || provider.isExchangeWithConsumerOkay(endpoints[i], exchange)))
                    {                    
                        match = endpoints[i];
                        break;
                    }
                }
            }
            finally
            {
                exchange.afterCapabilityCheck();
            }
        }
        
        if (match == null)
        {            
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CAPABILITY_NO_MATCH));
        }
        
        return match;
    }    
    
    //-----------------------------MessageServiceMBean--------------------------------
    
    /** Returns the total number of DeliveryChannels that have been activated
     *  in the NMR.
     *  @return number of active DeliveryChannels
     */
    public int getActiveChannelCount()
    {
        return mChannels.size();
    }
    
    /** Returns the total number of ServiceEndpoints that have been activated
     *  in the NMR.
     *  @return number of active ServiceEndpoints
     */
    public String[] getActiveChannels()
    {
        return (String[])mChannels.keySet().toArray(new String[0]);
    }
    
    /** Returns a list of component IDs corresponding to active channels in the NMR.
     *  @return list of component IDs
     */
    public int getActiveEndpointCount()
    {
        return mRegistry.getEndpointCount();
    }
    
    /** Returns a list of active endpoints in the NMR.  Endpoint references consist
     *  of a service QName and endpoint NCName.  The returned array has two columns:
     *  [0] = service QName as a string, [1] = endpoint name as string.  Each row
     *  in the array represents a single endpoint reference.
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints()
    {
        return (getEndpointNames());
    }
    
    /** Identical to getActiveEndpoints(), but list is limited to endpoints 
     *  registered by the specified component.
     *  @param ownerId component identifier
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints(String ownerId)
    {
        ServiceEndpoint[]       endpoints;
        String[]                list;
        DeliveryChannelImpl     dc = (DeliveryChannelImpl)this.getChannel(ownerId);
        
        endpoints = dc.getEndpoints();
        
        list = new String[endpoints.length];
        
        for (int i = 0; i < list.length; i++)
        {
            list[i] = ((RegisteredEndpoint)endpoints[i]).toExternalName();
        }
        
        return list;
    }

    public String[] getActiveConsumingEndpoints(String ownerId)
    {
        DeliveryChannelImpl     dc = (DeliveryChannelImpl)this.getChannel(ownerId);

        return (dc.getConsumingEndpointNames());
    }

    /**
     * Dump the state of the MessageService to the log.
     */
    public void dumpState()
    {
        mLog.info(toString());
    }

    public void toogleMessageTrace(boolean enabled)
    {
        if (enabled) {
            addListener((ExchangeListener)this, null);
        } else {
            removeListener((ExchangeListener)this);
        }
    }

    void setExchangeIdGenerator(ExchangeIdGenerator generator)
    {
        mGenerator = generator;
    }
    
    String generateNextId()
    {
        return (mGenerator.nextId());
    }
    
    boolean isExchangeOkay(MessageExchange me)
    {
        Component provider;
        
        provider = getChannel(((RegisteredEndpoint)((MessageExchangeProxy)me).getActualEndpoint()).
                getOwnerId()).getComponentInstance();
        return (provider == null || provider.isExchangeWithConsumerOkay(me.getEndpoint(), me));

    }

    void updateStatistics(MessageExchangeProxy mep)
    {
        int             phaseMask = mep.getPhaseMask();
        METimestamps    ts = mep.getTimestamps();


        synchronized  (this)
        {
            if ((phaseMask & MessageExchangeProxy.PM_SEND_REQUEST) != 0)
            {
                mSendRequest++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_REPLY) != 0)
            {
                mSendReply++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_FAULT) != 0)
            {
                mSendFault++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_DONE) != 0)
            {
                mSendDONE++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_ERROR) != 0)
            {
                mSendERROR++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_REQUEST) != 0)
            {
                mReceiveRequest++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_REPLY) != 0)
            {
                mReceiveReply++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_FAULT) != 0)
            {
                mReceiveFault++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_DONE) != 0)
            {
                mReceiveDONE++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_ERROR) != 0)
            {
                mReceiveERROR++;
            }
            if (ts != null)
            {
                mResponseTime.addSample(ts.mResponseTime);
                mNMRTime.addSample(ts.mNMRTime);
                mProviderTime.addSample(ts.mProviderTime);
                mChannelTime.addSample(ts.mProviderChannelTime);
                mChannelTime.addSample(ts.mConsumerChannelTime);
                if (ts.mConsumerTime != 0)
                {
                    mConsumerTime.addSample(ts.mConsumerTime);
                }
                if (ts.mStatusTime != 0)
                {
                    mStatusTime.addSample(ts.mStatusTime);
                }
            }
            if (mep instanceof InOnlyImpl)
            {
                mInOnlyMEPs++;
            }
            else if (mep instanceof InOutImpl)
            {
                mInOutMEPs++;

            }
            else if (mep instanceof RobustInOnlyImpl)
            {
                mRobustInOnlyMEPs++;

            }
            else if (mep instanceof InOptionalOutImpl)
            {
                mInOptionalOutMEPs++;
            }
        }

     }

    //-----------------------------NMRStatistics--------------------------------

    synchronized public void enableStatistics()
    {
        mMonitoringEnabled = true;
    }

    synchronized public void disableStatistics()
    {
        mMonitoringEnabled = false;

    }
    synchronized public boolean areStatisticsEnabled()
    {
        return (mMonitoringEnabled);
    }

    synchronized public void zeroStatistics()
    {
        mAcceptCount = 0;
        mAcceptTimeoutCount = 0;
        mSendCount = 0;
        mSendSyncCount = 0;
        mSendRequest = 0;
        mReceiveRequest = 0;
        mSendReply = 0;
        mReceiveReply = 0;
        mSendFault = 0;
        mReceiveFault = 0;
        mSendDONE = 0;
        mReceiveDONE = 0;
        mSendERROR = 0;
        mReceiveERROR = 0;
        mInOutMEPs = 0;
        mInOnlyMEPs = 0;
        mRobustInOnlyMEPs = 0;
        mInOptionalOutMEPs = 0;
        mResponseTime.zero();
        mStatusTime.zero();
        mNMRTime.zero();
        mConsumerTime.zero();
        mProviderTime.zero();
        mChannelTime.zero();

       for (DeliveryChannelImpl d : ((Hashtable<String,DeliveryChannelImpl>)mChannels.clone()).values())
       {
            d.zeroStatistics();
       }
       mRegistry.zeroStatistics();
    }

    public String[] getChannelNames()
    {
        return (getActiveChannels());
    }

    public ChannelStatistics  getChannelStatistics(String name)
    {
        return (getChannel(name));
    }

    public String[] getEndpointNames()
    {
        ServiceEndpoint[] endpoints;
        ServiceEndpoint[] lendpoints;
        String[]          list;

        endpoints   = mRegistry.listEndpoints(RegisteredEndpoint.INTERNAL);
        lendpoints   = mRegistry.listEndpoints(RegisteredEndpoint.LINKED);
        list        = new String[endpoints.length + lendpoints.length];

        for (int i = 0; i < endpoints.length; i++)
        {
            list[i] = ((RegisteredEndpoint)endpoints[i]).toExternalName();
        }
        for (int i = 0; i < lendpoints.length; i++)
        {
            list[endpoints.length + i] = ((RegisteredEndpoint)lendpoints[i]).toExternalName();
        }

        return list;
    }

    public String[] getConsumingEndpointNames()
    {
        ServiceEndpoint[] endpoints;
        String[]          list;

        endpoints   = mRegistry.listEndpoints(RegisteredEndpoint.LINKED);
        list        = new String[endpoints.length];

        for (int i = 0; i < list.length; i++)
        {
            list[i] = ((RegisteredEndpoint)endpoints[i]).toExternalName();
        }

        return list;
    }

    public EndpointStatistics  getEndpointStatistics(String name)
    {
        EndpointStatistics  es;

        es = mRegistry.getLinkedEndpointByName(name);
        if (es == null)
        {
            es = mRegistry.getInternalEndpointByName(name);
        }
        return (es);
    }

   /**
     * List of item names for CompositeData construction.
     */
    private static final int ITEMS_BASE = 20;
    private static final int ITEMS_EXTRA = 24;
    private static final String[] ITEM_NAMES = {
        "Send",
        "SendSync",
        "Accept",
        "AcceptTimeout",
        "SendRequest",
        "ReceiveRequest",
        "SendReply",
        "ReceiveReply",
        "SendFault",
        "ReceiveFault",
        "SendDONE",
        "ReceiveDONE",
        "SendERROR",
        "ReceiveERROR",
        "InOnlyMEPs",
        "RobustInOnlyMEPs",
        "InOutMEPs",
        "InOptionalOutMEPs",
        "DeliveryChannels",
        "Endpoints",
        "ResponseTimeMin",
        "ResponseTimeAvg",
        "ResponseTimeMax",
        "ResponseTimeStd",
        "StatusTimeMin",
        "StatusTimeAvg",
        "StatusTimeMax",
        "StatusTimeStd",
        "NMRTimeMin",
        "NMRTimeAvg",
        "NMRTimeMax",
        "NMRTimeStd",
        "ConsumerTimeMin",
        "ConsumerTimeAvg",
        "ConsumerTimeMax",
        "ConsumerTimeStd",
        "ProviderTimeMin",
        "ProviderTimeAvg",
        "ProviderTimeMax",
        "ProviderTimeStd",
        "ChannelTimeMin",
        "ChannelTimeAvg",
        "ChannelTimeMax",
        "ChannelTimeStd"
    };

    /**
     * List of descriptions of items for ComponsiteData construction.
     */
    private static final String ITEM_DESCRIPTIONS[] = {
        "Number of exchanges sent",
        "Number of exchanges send synchronously",
        "Number of accepts",
        "Number of timed out accepts",
        "Number of requests sent",
        "Number of requests received",
        "Number of replies sent",
        "Number of replies received",
        "Number of faults sent",
        "Number of faults received",
        "Number of DONE requests sent",
        "Number of DONE requests received",
        "Number of ERROR requests sent",
        "Number of ERROR requests received",
        "Number of InOnly MEP's",
        "Number of RobustInOnly MEP's",
        "Number of InOut MEP's",
        "Number of InOptionalOut MEP's",
        "Number of DeliveryChannels",
        "Number of Endpoints",
        "Response Time Min",
        "Response Time Avg",
        "Response Time Max",
        "Response Time Std",
        "Status Time Min",
        "Status Time Avg",
        "Status Time Max",
        "Status Time Std",
        "NMR Time Min",
        "NMR Time Avg",
        "NMR Time Max",
        "NMR Time Std",
        "Consumer Time Min",
        "Consumer Time Avg",
        "Consumer Time Max",
        "Consumer Time Std",
        "Provider Time Min",
        "Provider Time Avg",
        "Provider Time Max",
        "Provider Time Std",
        "Channel Time Min",
        "Channel Time Avg",
        "Channel Time Max",
        "Channel Time Std"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType ITEM_TYPES[] = {
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING,
         SimpleType.STRING
    };

    public CompositeData        getStatistics()
    {
        try
        {
            Object      values[];
            String      names[];
            String      descs[];
            OpenType    types[];
            boolean     enabled = // areStatisticsEnabled() ||
                                    mChannelTime.getCount() != 0;

            if (enabled)
            {
                values = new Object[ITEMS_BASE + ITEMS_EXTRA];
                types = ITEM_TYPES;
                names = ITEM_NAMES;
                descs = ITEM_DESCRIPTIONS;
            }
            else
            {
                values = new Object[ITEMS_BASE];
                types = new OpenType[ITEMS_BASE];
                System.arraycopy(ITEM_TYPES, 0, types, 0, ITEMS_BASE);
                names = new String[ITEMS_BASE];
                System.arraycopy(ITEM_NAMES, 0, names, 0, ITEMS_BASE);
                descs = new String[ITEMS_BASE];
                System.arraycopy(ITEM_DESCRIPTIONS, 0, descs, 0, ITEMS_BASE);
            }


            values[0] = mSendCount;
            values[1] = mSendSyncCount;
            values[2] = mAcceptCount;
            values[3] = mAcceptTimeoutCount;
            values[4] = mSendRequest;
            values[5] = mReceiveRequest;
            values[6] = mSendReply;
            values[7] = mReceiveReply;
            values[8] = mSendFault;
            values[9] = mReceiveFault;
            values[10] = mSendDONE;
            values[11] = mReceiveDONE;
            values[12] = mSendERROR;
            values[13] = mReceiveERROR;
            values[14] = mInOnlyMEPs;
            values[15] = mRobustInOnlyMEPs;
            values[16] = mInOutMEPs;
            values[17] = mInOptionalOutMEPs;
            values[18] = (long)mChannels.size();
            values[19] = (long)mRegistry.getEndpointCount();
            if (enabled)
            {
                values[20] = mResponseTime.getMinNice();
                values[21] = mResponseTime.getAverageNice();
                values[22] = mResponseTime.getMaxNice();
                values[23] = mResponseTime.getSdNice();
                values[24] = mStatusTime.getMinNice();
                values[25] = mStatusTime.getAverageNice();
                values[26] = mStatusTime.getMaxNice();
                values[27] = mStatusTime.getSdNice();
                values[28] = mNMRTime.getMinNice();
                values[29] = mNMRTime.getAverageNice();
                values[30] = mNMRTime.getMaxNice();
                values[31] = mNMRTime.getSdNice();
                values[32] = mConsumerTime.getMinNice();
                values[33] = mConsumerTime.getAverageNice();
                values[34] = mConsumerTime.getMaxNice();
                values[35] = mConsumerTime.getSdNice();
                values[36] = mProviderTime.getMinNice();
                values[37] = mProviderTime.getAverageNice();
                values[38] = mProviderTime.getMaxNice();
                values[39] = mProviderTime.getSdNice();
                values[40] = mChannelTime.getMinNice();
                values[41] = mChannelTime.getAverageNice();
                values[42] = mChannelTime.getMaxNice();
                values[43] = mChannelTime.getSdNice();
            }

            return new CompositeDataSupport(
                new CompositeType(
                    "MessagingStatistics",
                    "Messaging statistics",
                    names, descs, types), names, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            System.out.println(odEx.toString()); // ignore this for now
        }
        return (null);
    }

    //-----------------------------MessagingStatistics--------------------------------

    /**
     * List of item names for CompositeData construction.
     */
    private static final String[] MS_ITEM_NAMES = {
        "ActiveExchanges",
        "ActiveExchangeRate",
        "InOnlyExchanges",
        "RobustInOnlyExchanges",
        "InOptionalOutExchanges",
        "InOutExchanges",
        "ExchangeMeanTimeToClose",
        "ExchangeSuccessRate",
        "FailedExchanges",
        "TransactedExchanges",
        "FaultedExchanges",
        "SentExchanges",
        "SynchronousSentExchanges",
        "AcceptExchanges",
        "AcceptExchangesTimeout"
    };

    /**
     * List of descriptions of items for ComponsiteData construction.
     */
    private static final String MS_ITEM_DESCRIPTIONS[] = {
        "Number of message exchanges currently active",
        "Current rate of active exchanges per hour",
        "Total number of InOnly message exchanges processed",
        "Total number of RobustInOnly message exchanges processed",
        "Total number of InOptionalOut message exchanges processed",
        "Total number of InOut message exchanges  processed",
        "Mean time to close a message exchange",
        "Message exchange success rate",
        "Total number of failed message exchanges",
        "Total number of transacted message exchanges",
        "Total number of faulted message exchanges",
        "Total number of message exchange send operations",
        "Total number of message exchange sendSync operations",
        "Total number of message exchange accept operations",
        "Total number of message exchange accept timeouts"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType MS_ITEM_TYPES[] = {
         SimpleType.INTEGER,
         SimpleType.DOUBLE,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.FLOAT,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG
    };

    /**
     * CompositeType used to describe CompositeData representing messaging
     * statistics.
     */
    private static CompositeType sMessagingStatisticsType;

    /**
     * Static initializer.
     */
    {
        try
        {
            sMessagingStatisticsType = new CompositeType(
                "MessagingStatistics",
                "Message exchange statistics",
                MS_ITEM_NAMES, MS_ITEM_DESCRIPTIONS, MS_ITEM_TYPES);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            ; // ignore this for now
        }
    }

    //-----------------------------Exchange Listener----------------------------
    public void addListener(ExchangeListener el, String ns)
    {
        synchronized (mExchangeListeners)
        {
            mExchangeListeners.put(el, ns);
            mELs = mExchangeListeners.entrySet().toArray();
        }
    }

    public void removeListener(ExchangeListener el)
    {
      synchronized (mExchangeListeners)
      {
          mExchangeListeners.remove(el);
          mELs = mExchangeListeners.entrySet().toArray();
      }
    }

    synchronized void notifyExchangeListeners(MessageExchange me, boolean isSend)
    {
        Object[]    els = mELs;
        
        if (els != null && els.length > 0)
        {
            String                  ens = me.getEndpoint().getServiceName().getNamespaceURI();
            ExchangeDescriptor      md = null;

            synchronized (els)
            {
                for (Object o : els)
                {
                    Map.Entry<ExchangeListener,String>  m = (Map.Entry<ExchangeListener,String>)o;
                    ExchangeListener    el = m.getKey();
                    String              ns = m.getValue();

                    if (ns == null || ns.equals(ens))
                    {
                        if (md == null)
                        {
                            md = new ExchangeDescriptorImpl(me, isSend);
                        }
                        try
                        {
                            el.onReceive(md);
                        }
                        catch (Exception e)
                        {
                            removeListener(el);
                        }
                    }
                }
            }
        }
    }

    public void send(ExchangeDescriptor md)
    {
    }

    public void onReceive(ExchangeDescriptor ed)
    {
        StringBuffer            sb = new StringBuffer();
        MessageDescriptor       md;
        HashMap<String,String>  p;
        String                  from;
        String                  to;

        sb.append("NMR-ExchangeListener: Action: " + ed.getAction() + "  Status: " + ed.getStatus() + "\n");
        sb.append("    Id:         " + ed.getId() + "  Role: " + ed.getRole() + "\n");
        if (ed.getRole().equals("CONSUMER"))
        {
            if (ed.getAction().equals("send"))
            {
                from = ed.getConsumer();
                to = ed.getProvider();
            }
            else
            {
                from = ed.getProvider();
                to = ed.getConsumer();
            }
        }
        else
        {
             if (ed.getAction().equals("send"))
            {
                from = ed.getProvider();
                to = ed.getConsumer();
            }
            else
            {
                from = ed.getConsumer();
                to = ed.getProvider();
            }
       }
        sb.append("    From:       " + from + "\n");
        sb.append("    To:         " + to + "\n");
        sb.append("    Pattern:    " + ed.getPattern() + "\n");
        if (ed.getError() != null)
        {
            sb.append("    Error:      " + ed.getError() + "\n");
        }
        if (ed.getOperation() != null)
        {
            sb.append("    Operation:  " + ed.getOperation() + "\n");
        }
        if (ed.getInterface() != null)
        {
            sb.append("    Interface:  " + ed.getInterface() + "\n");
        }
        if (ed.getService() != null)
        {
            sb.append("    Service:    " + ed.getService() + "\n");
        }
        if ((p = ed.getProperties()) != null)
        {
            if (!p.isEmpty())
            {
                sb.append("    Properties:\n");
                for (Map.Entry<String,String> m : p.entrySet())
                {
                    sb.append("      Name:  " + m.getKey() + "\n");
                    sb.append("      Value:   " + m.getValue() + "\n");
                }
            }
        }
        if ((md = ed.getIn()) != null)
        {
            sb.append(displayMessage("in", md));
        }
        if ((md = ed.getOut()) != null)
        {
            sb.append(displayMessage("out", md));
        }
        if ((md = ed.getFault()) != null)
        {
            sb.append(displayMessage("fault", md));
        }
        mLog.warning(sb.toString());
    }

    private String displayMessage(String name, MessageDescriptor md)
    {
        StringBuffer            sb = new StringBuffer();
        HashMap<String,String>  p;
        HashMap<String,String>  ai;
        HashMap<String,String>  ac;

        sb.append("    Message: " + name + "\n");
        p = md.getProperties();
        if (!p.isEmpty())
        {
            sb.append("      Properties:\n");
            for (Map.Entry<String,String> m : p.entrySet())
            {
                sb.append("        Name:  " + m.getKey() + "\n");
                sb.append("        Value:   " + m.getValue() + "\n");
            }
        }
        String  message = md.getMessage();
        if (message != null)
        {
            sb.append("------Content:----------------------------------------\n");
            sb.append(md.getMessage());
            if (sb.charAt(sb.length() -1) != '\n')
                sb.append("\n");
            sb.append("------------------------------------------------------\n");
        }
        ai = md.getAttachmentInfo();
        if (ai != null && ai.size() > 0)
        {
        ac = md.getAttachmentContent();
            for (String att : ai.keySet())
            {
                sb.append("      Attachment:  " + att + "\n");
                sb.append("        ContentType: " + ai.get(att) + "\n");
                String content = ac.get(att);
                if (content != null) {
                    sb.append("--------Content:----------------------------------------\n" + content);
                    sb.append("--------------------------------------------------------\n");
                }
            }
        }
        return (sb.toString());
    }

    //-------------------------------Object-------------------------------------
   
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("\nMessage Service Status\n");        sb.append("  InOnlyMEPs:       " + mInOnlyMEPs);
        sb.append("  InOutMEPs:         " + mInOutMEPs);
        sb.append("\n  RobustInOnlyMEPs: " + mRobustInOnlyMEPs);
        sb.append("  InOptionalOutMEPs: " + mInOptionalOutMEPs);
        sb.append("\n  Send:      " + mSendCount);
        sb.append("  SendSync:  " + mSendSyncCount);
        sb.append("\n  Accept:    " + mAcceptCount);
        sb.append("  AcceptTO:  " + mAcceptTimeoutCount);
        sb.append("\n  SendReply: " + mSendReply);
        sb.append("  RecvReply: " + mReceiveReply);
        sb.append("\n  SendDONE: " + mSendDONE);
        sb.append("  RecvDONE: " + mReceiveDONE);
        sb.append("\n  SendERROR: " + mSendERROR);
        sb.append("  RecvERROR: " + mReceiveERROR);
        sb.append("\n  SendFault: " + mSendFault);
        sb.append("  RecvFault: " + mReceiveFault);
        if (mChannelTime.getCount() != 0)
        {
            sb.append("\n   ResponseTime:  " + mResponseTime.toString());
            sb.append("\n   StatusTime:    " + mStatusTime.toString());
            sb.append("\n   ConsumerTime:  " + mConsumerTime.toString());
            sb.append("\n   ProviderTime:  " + mProviderTime.toString());
            sb.append("\n   ChannelTime:   " + mChannelTime.toString());
            sb.append("\n   NMRTime:       " + mNMRTime.toString());
        }
        sb.append("\n  Delivery Channel Count: ");
        sb.append(mChannels.size());
        sb.append("\n");

        //
        //  Compute a quick summary of MEP's showing participants,
        //  owner of the ME, and the service and operation.
        //
        SortedMap<String,String> sm = new TreeMap();
        for (Iterator i = mChannels.values().iterator(); i.hasNext(); )
        {
            ((DeliveryChannelImpl)i.next()).activeSummary(sm);
        }
        if (sm.size() != 0)
        {
            sb.append("  Active Exchange Summary Count: " + sm.size() + "\n");
            for (Map.Entry<String,String> e : sm.entrySet())
            {
                sb.append("    " + e.getKey() + "\n      " + e.getValue() + "\n");
            }
            sb.append("\n");
        }

        for (Iterator i = mChannels.values().iterator(); i.hasNext(); )
        {
            sb.append(i.next().toString());
            sb.append("\n");
        }
        sb.append(mRegistry.toString());
        return (sb.toString());
    }
    
    // -------------------------- private helpers ------------------------------
    
    void sendEvent(Event e)
    {
        if (mBundleContext != null)
        {
            try
            {
                ServiceReference[] listeners =
                    mBundleContext.getServiceReferences(
                        EventAdmin.class.getName(), null);
                if (listeners != null)
                {
                    for (ServiceReference sr : listeners)
                    {
                        EventAdmin  ea = (EventAdmin)(mBundleContext.getService(sr));

                        if (ea != null)
                        {
                            ea.postEvent(e);
                        }
                    }
                }
            }
            catch (org.osgi.framework.InvalidSyntaxException ignore)
            {
                
            }
        }        
    }
    
    /**
     * Intercept the exchange being sent.
     * 
     * @param channel - DeliveryChannel the exchange is being sent on
     * @param exchange - MessageExchange being sent
     * @param isSync - flag indicating whether the send is synchronous 
     *                 or asynchronous
     * @return true if the message exchange flow is to continue normally 
     *         after interceptio or is to be shorted.
     * 
     */
    private boolean interceptSendExchange(DeliveryChannelImpl channel, 
                                       MessageExchangeProxy exchange,
                                       boolean isSync)
        throws javax.jbi.messaging.MessagingException
    {   
        // -- Trigger the Message Exchange Interceptor, this will
        //    further trigger all the MessageValidators
        if (mLog.isLoggable(Level.FINEST)) {
            mLog.finest(" Message Service intercepting send of exchange  "
                + exchange.toString());
        }

        boolean proceed        = true;
        
        try
        {
            if ( mBundleContext != null )
            {
                ServiceReference[] interceptors =
                    mBundleContext.getServiceReferences(
                        Interceptor.class.getName(), null);
                
                if ( interceptors != null )
                {
                    java.util.Properties exProps = MessageExchangeUtil.getExchangeProperties(exchange);
                    
                    /** 
                     * Sort all the service references.
                     */
                    PriorityQueue<ServiceReference> 
                            orderedInterceptors = InterceptionHelper.prioritySort(interceptors);
                    while ( orderedInterceptors.peek() != null )
                    {
                        ServiceReference orderedInterceptor = (ServiceReference) orderedInterceptors.poll();
                        if ( InterceptionHelper.shouldTrigger(orderedInterceptor, exProps) )
                        {
                            Object aInterceptor =  mBundleContext.getService(orderedInterceptor);
                            
                            if ( aInterceptor instanceof InterceptorWrapper )
                            {
                                aInterceptor = ((InterceptorWrapper) aInterceptor).getInterceptor();
                            }
                            Object mtdObj = orderedInterceptor.getProperty(
                                    com.sun.jbi.interceptors.internal.ServiceProperty.METHOD.toString());
                            
                            if ( mtdObj != null && aInterceptor != null )
                            {
                                Method mtd = (Method) mtdObj;

                                exchange.beforeIntercept();
                                proceed = InterceptionHelper.invokeInterceptor(mtd, aInterceptor, exchange);

                                if ( !proceed )
                                {
                                    mLog.finest(" Interceptor " + mtd.getName() + " shorted the exchange ");

                                    exchange.completeSend();
                                    if (!isSync) {
                                        channel.queueExchange(exchange);
                                    }
                                    else
                                    {
                                        exchange.finishSync();
                                    }
                                    break;
                                }
                                else
                                {
                                    exchange.revertIntercept();
                                    addressExchange(exchange, channel);
                                    exchange.validate(channel, isSync);
                                }    
                            }
                        }
                    }
                }
            }
        }
        catch ( Exception ex )
        {
            proceed = false;
            Throwable cause = ex;
            while ( cause != null && cause instanceof java.lang.reflect.InvocationTargetException )
            {
                cause = cause.getCause();
            }
            
            if ( cause != null )
            {
                mLog.log(Level.FINE, "Exception occured when exchange was " +
                        "intercepted by  an Interceptor" 
                        , cause );
                DeliveryChannelImpl targetChannel = exchange.getSendChannel();
                exchange.terminate((Exception)cause, false);
                javax.jbi.messaging.MessagingException e = targetChannel.queueExchange(exchange);
                if (e != null) {
                    mLog.log(Level.WARNING, "Interception of message exchange " + 
                            exchange.getExchangeId() + " failed.", e);
                }
                throw new javax.jbi.messaging.MessagingException((Exception)cause);
            }
        }
        finally
        {
            exchange.afterIntercept();
        }
        return proceed;
    }

    protected void sendEvent(int id, MessageExchange me) {
        
        //
        //  Signal event is delivered to the actual ME.
        //
        if (id == com.sun.jbi.ext.DeliveryChannel.EVENT_SIGNAL) {
            MessageExchangeProxy    mep = (MessageExchangeProxy)me;
            DeliveryChannelImpl     dc = mep.getTwin().getSendChannel();

            if (dc != null) {
                dc.onEvent(mep, id, null);
            }
        } else {
            //
            //  All other events traverse towards the parent.
            //
            if (me.getRole().equals(MessageExchange.Role.PROVIDER)) {
                //
                //  Walk up to the first parent that accepts the request.
                //
                MessageExchangeProxy    mep = (MessageExchangeProxy)me;
                for (;;) {
                    DeliveryChannelImpl dc;

                    dc = mep.getSendChannel();
                    if (dc == null) {
                        break;
                    }
                    if (dc.onEvent(mep.getTwin(), id, me)) {
                        break;
                    }
                    mep = mep.getParent();
                    if (mep == null) {
                        break;
                    }
                }
            } else {
                throw new java.lang.RuntimeException("ToDo: MS.sendEvent in CONSUMER context!");
            }
        }
    }

    protected boolean isChild(DeliveryChannelImpl dc, MessageExchange me) {
        if (me.getRole().equals(MessageExchange.Role.PROVIDER)) {
            //
            //  Walk up to the first parent that accepts the request.
            //
            MessageExchangeProxy    mep = (MessageExchangeProxy)me;
            for (;;) {
                DeliveryChannelImpl parent;

                parent = mep.getSendChannel();
                if (parent == dc) {
                    return (true);
                }
                mep = mep.getParent();
                if (mep == null) {
                    return (false);
                }
            }
        } else {
            throw new java.lang.RuntimeException("ToDo: MS.isChild in CONSUMER context!");
        }
    }
}