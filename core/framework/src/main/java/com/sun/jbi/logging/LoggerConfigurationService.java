/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LoggerConfigurationService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.framework.ServiceReference;

import com.sun.jbi.framework.StringTranslator;
import com.sun.jbi.framework.osgi.internal.Environment;

/**
 * This is a managed service that is registered to provide a configuration interface
 * to the admin tools. This class is inherited by <code>ComponentLoggerConfigurationService</code>
 * and <code>RuntimeLoggerConfigurationService</code>.
 */
public class LoggerConfigurationService implements ManagedService {
    
    /**
     * initial configuration for the configuration service
     */
    private Dictionary initialConfiguration_ = new Properties();
    
    /**
     * key for the property containing logger levels
     */
    private static final String LOGGER_LEVELS = "loggers";
    
    /**
     * vector containing the logger level  mapping as
     * logger-name=logger-level
     */
    private Vector<String> loggers = new Vector<String>();
    
    /**
     * null string. When a logger inherits the level from its parent
     * when its level is set to null
     */
    private static String DEFAULT = "DEFAULT";
    

    /** 
     * logger 
     */
    protected Logger log_ = Logger.getLogger(
            LoggerConfigurationService.class.getPackage().getName()); 
    
   /** 
    * string translator instance     
    */
    protected static StringTranslator translator_ = new StringTranslator(
                LoggerConfigurationService.class.getPackage().getName(), null);    
    
    /**
     * service PID for the log config service
     */
    protected String logConfigServicePID_;
    
    /**
     * servie PID for the log display service
     */
    protected String logDisplayServicePID_;
    
    
    /** Construtor for LoggerConfiurationService 
     * 
     * @param servicePID service PID
     * @param initialConfig intialConfig all the loggers that are active when the service is created
     */
    public LoggerConfigurationService(String servicePID, String displayServicePID, HashMap<String, String> initialConfig) {
        logConfigServicePID_ = servicePID;
        logDisplayServicePID_ = displayServicePID;
        
        initialConfiguration_.put(org.osgi.framework.Constants.SERVICE_PID, servicePID);
        
        for (String key : initialConfig.keySet()) {
            loggers.add(key + LoggingService.EQUALS + initialConfig.get(key));
        }
        initialConfiguration_.put(LoggingService.LOGGERS_KEY, loggers);

        updateLogDisplayService(initialConfiguration_);
    }
    
    /**
     * This method use to return the configuration of the LoggerConfiurationService
     */
    public Dictionary getConfiguration() {
        return initialConfiguration_;
    }
    
    /**
     * This method is used to obtain the service PID of the logger configuration service
     * @return String the service PID of the logger configuration service
     */
    public String getLoggerConfigurationServicePID() {
        return logConfigServicePID_;
    }
            
    /**
     * This method is called by the admin tools when the 
     * configuration for this service is updated.
     * @param configuration
     */
    public void updated(Dictionary configuration) {
        
        if (configuration != null) {
            
            Enumeration keys = configuration.keys();

            while(keys.hasMoreElements()) {
                
                String key = (String)keys.nextElement();               
                if (key.equals(LOGGER_LEVELS)) {
                           
                    String loggerName = null;
                    String loggerValue = null;
                            
                    Vector<String> logLevels = (Vector<String>)configuration.get(LOGGER_LEVELS);
                    if (logLevels != null && logLevels.capacity() > 0) {
                        
                        Enumeration elements = logLevels.elements();
                        
                        while(elements.hasMoreElements()) {
                        

                            try {
                                
                                String value = (String)elements.nextElement();
                                loggerName = value.substring(0, value.indexOf(LoggingService.EQUALS));
                                loggerValue = value.substring(value.indexOf(LoggingService.EQUALS) + 1);
                 
                                Logger logger = Logger.getLogger(loggerName);
                                
                                // if the level is null, don't parse it
                                if (loggerValue == null) {
                                    logger.setLevel(null);
                                } else if (loggerValue.equalsIgnoreCase(DEFAULT)) {
                                    logger.setLevel(null);
                                } else {          
                                    //todo : cost of setLevel vs comparing if the level has changed
                                    logger.setLevel(Level.parse(loggerValue));
                                }
                                log_.finer("Set the logger " + loggerName + " at level " + loggerValue);

                            } catch (Exception ex) {
                                String msg = translator_.getString(
                                        LocalStringKeys.FAILED_LOGGER_LEVEL_CONFIGURATION,
                                        new Object[] { loggerName, loggerValue } );

                                log_.log(Level.WARNING, msg, ex);
                            }
                        }
                    }
                }
            }
            // now update the display service with effective log levels
            updateLogDisplayService(configuration);
        }
    }
    
    /**
     * This method is used to update the log display service with a list of 
     * loggers and their effective log levels
     * @param configuration loggers and their log levels
     */
     private void updateLogDisplayService(Dictionary configuration) {
        
        try {
            
            ConfigurationAdmin configAdmin;
            if ((configAdmin = getConfigurationAdminService()) != null) {
                      
                Configuration config = configAdmin.getConfiguration(  
                            logDisplayServicePID_, 
                            null);

                if (configuration != null) {
                     config.update(calculateEffectiveLogLevels(configuration));
                }
             }
            
         } catch (Exception ex) {
            log_.log(Level.WARNING, translator_.getString(LocalStringKeys.CONFIG_UPDATE_FAILED), ex);
         }         
         
     }
     
     /**
     * This method is used to get a reference to the config admin service
     */
    protected ConfigurationAdmin getConfigurationAdminService() {
        
        ConfigurationAdmin configAdmin = null;
        if (Environment.getFrameworkBundleContext() != null) {
            ServiceReference  configAdminRef =
               Environment.getFrameworkBundleContext().getServiceReference(ConfigurationAdmin.class.getName());
 
            if (configAdminRef != null) {
                configAdmin = (ConfigurationAdmin)  
                         Environment.getFrameworkBundleContext().getService(configAdminRef);  

            }
        }
        return configAdmin;
    } 
    
    /** 
     * This method is used to calculate effective log levels 
     * @param loggers - logger names mapped to actual log levels
     * @return Dictionary - logger names mapped to effective log levels
     */
    private Dictionary calculateEffectiveLogLevels(Dictionary loggers) {
        Dictionary config = new Hashtable();
        Vector<String> effectiveLogLevels = new Vector<String>();
        
        if (loggers != null) {
            Enumeration keys = loggers.keys();
            while(keys.hasMoreElements()) {
                String key = (String)keys.nextElement();
                if (key.equals(LOGGER_LEVELS)) {
                    Vector<String> logLevels = (Vector<String>)loggers.get(key);
                    Enumeration elements = logLevels.elements();
                    while(elements.hasMoreElements()) {
                        String value = (String)elements.nextElement();
                        String loggerName = value.substring(0, value.indexOf(LoggingService.EQUALS));
                        String loggerValue = value.substring(value.indexOf(LoggingService.EQUALS) + 1);
                        effectiveLogLevels.add(loggerName + LoggingService.EQUALS + calculateEffectiveLogLevel(loggerName));

                    }
                    config.put(LOGGER_LEVELS, effectiveLogLevels);
                }
           }
        }
        return config;
    }
    
    
    /**
     * Get the effective log level of this logger. If the level is null, search the
     * parent logger chain until a logger is found with a level set.
     * @return String representing log level or DEFAULT if no level is set.
     */
    public String calculateEffectiveLogLevel(String logger)
    {
        Logger log = Logger.getLogger(logger);
        Level level = log.getLevel();

        while ( null == level )
        {
            log = log.getParent();
            if ( null != log )
            {
                level = log.getLevel();
            }
            else
            {
                break;
            }
        }
        if ( null != level )
        {
            return level.toString();
        }
        else
        {
            return DEFAULT;
        }
    }
}
