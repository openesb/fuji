/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XSLSplitter.java - Last published on 3/13/2008
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.eip.split.BaseSplitter;
import org.glassfish.openesb.api.service.ServiceMessage;

import com.sun.jbi.fuji.ifl.Flow;
import java.util.ArrayList;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * XSL Split EIP.
 * 
 * @author 
 */
public class XSLSplitter extends BaseSplitter{

    String              stylesheet_;
    String              splitName_;
    Transformer         transformer_;
    TransformerFactory  transformerFactory_;
    Source              xsltSource_;
    
    
    private static String SPLIT_DIR = "/META-INF/flow/split/";
    
    
    public XSLSplitter(){
        // the factory pattern supports different XSLT processors
        transformerFactory_ = TransformerFactory.newInstance();
    }
    
    /**
     * Split the current active message in the exchange and return the split documents ina 
     * ArrayList
     * 
     * @param svcMsg - svcMsg to split
     */
    public ArrayList split(ServiceMessage svcMsg)
        throws Exception{
        
        stylesheet_ = (String)config_.get("stylesheet");
        splitName_  = (String)config_.get(Flow.Keys.NAME.toString());
        if (xsltSource_ == null || transformer_==null ){
            xsltSource_ = getXSLSource(stylesheet_);
            transformer_ = transformerFactory_.newTransformer(xsltSource_);
        }
        
        Object payload = svcMsg.getPayload();
        ArrayList splitDocs = new ArrayList();
        
        if ( payload instanceof Node ){
            
            Source xmlSource = new DOMSource((Node)payload);
            DOMResult result = new DOMResult();
            transformer_.transform(xmlSource, result); 
            
            if ( result != null ){
                Node parent = result.getNode();
                NodeList children = parent.getChildNodes().item(0).getChildNodes();
                
                for ( int i = 0; i < children.getLength(); i++){
                    splitDocs.add(children.item(i));
                }
            }
        } else {
            // Not XML? just return the payload
            splitDocs.add(payload);
        }

        return splitDocs;
    }
    
    /**
     * 
     * @param name
     * @return
     */
    
     public Source getXSLSource(String name)
        throws Exception
    {
        Source          src = null;
        StringBuffer    res = null;
        if ( !isPath(name)) {
            res = new StringBuffer(SPLIT_DIR);
            res.append(splitName_);
            res.append("/");
            res.append(name);
        } else {
            res = new StringBuffer(name);
        }
        
        java.net.URL resURL = bundle_.getEntry(res.toString()); 
        if ( resURL != null )
        {
            try
            {
                java.io.InputStream ios = resURL.openStream();
                src = new StreamSource(ios);
            }
            catch ( java.io.IOException ioex )
            {
               throw new Exception("Failed to read not read xsl file " + res.toString() + ". " + ioex.toString() );
            }
        } else {
            throw new Exception("Could not find xsl stylesheet: " + res.toString());
        }
        return src;
    }
    
     private boolean isPath(String name){
         boolean isPath = false;
         if(name != null){
             isPath = (name.contains("/") || name.contains("\\"));
         }
         return isPath;
     }
}