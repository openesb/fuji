/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;

import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

/**
 * OSGi bundle representing a JBI service unit.
 * @author kcbabo
 */
public class ServiceUnitBundle extends JBIBundle {
    
    private static final String COMPONENT_NAME_HDR = "Component-Name";
    
    /**
     * Creates a new ServiceUnitBundle.
     * @param descriptor jbi.xml
     * @param bundle containing OSGi bundle
     */
    public ServiceUnitBundle(Jbi descriptor, Bundle bundle) {
        super(descriptor, bundle);
    }
    
    /**
     * I'm a service unit.
     * @return service unit type
     */
    public BundleType getType() {
        return BundleType.SERVICE_UNIT;
    }
    
    /**
     * Returns service unit name.
     * @return SU name
     */
    public String getName() {
        return (String)getBundle().getHeaders().get(Constants.BUNDLE_SYMBOLICNAME);
    }
    
    /**
     * Returns the target component for this service unit.  This information
     * is normally stored in the SA jbi.xml, so we expect a header to be 
     * defined for stand-alone service units.
     * @return target component name
     */
    public String getTargetComponent() {
        return (String)getBundle().getHeaders().get(COMPONENT_NAME_HDR);
    }
    
}
