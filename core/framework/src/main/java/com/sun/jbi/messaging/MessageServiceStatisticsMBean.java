/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageServiceStatisticsMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.Date;

import javax.management.openmbean.CompositeData;

import javax.xml.namespace.QName;


/**
 * This interface implements the MBean for collection of statistics for the
 * messaging service. All statistics are since the last message service startup;
 * they are all reset when the message service is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public interface MessageServiceStatisticsMBean
{
    /**
     * Enable timing of NMR interactions.
     */
    void enableTimingStatistics();
    void disableTimingStatistics();
    void resetStatistics();
    
    /**
     * Get the time that the message service was last started.
     * @return The time of the last successful start() call.
     */
    Date getLastRestartTime();

    /**
     * Get the current number of services registered with the message service.
     * @return The total number of registered services.
     */
    int getRegisteredServices();

    /**
     * Get the current number of endpoints registered with the message service.
     * @return The total number of registered endpoints.
     */
    int getRegisteredEndpoints();

    /**
     * Returns a list of component IDs corresponding to active channels in the NMR.
     *  @return list of component IDs
     */
    public String[] getActiveChannels();
    
    /**
     * Returns a list of active endpoints (includes consuming endpoints)  in the NMR.  
     *  @return list of active endpoints
     */
    public String[] getActiveEndpoints();
    
    /**
     * Get the list of endpoints for a specific DeliveryChannel.
     * @return The delivery channel statistics in a CompositedData instance
     */
    public String[]  getEndpointsForDeliveryChannel(String dcName);
    
    /**
     * Get the list of consuming endpoints for a specific DeliveryChannel.
     * @return The delivery channel statistics in a CompositedData instance
     */
    public String[]  getConsumingEndpointsForDeliveryChannel(String dcName);
    
    
    /**
     * Get the CompositeData instance that represents the current values for 
     * MessageService statistics.
     * @return The messaging statistics in a CompositedData instance
     */
    CompositeData getMessagingStatistics();
    
    /**
     * Get the CompositeData instance that represents the current values for specific
     * DeliveryChannel statistics.
     * @return The delivery channel statistics in a CompositedData instance
     */
    CompositeData getDeliveryChannelStatistics(String dcName);
    
    /**
     * Get the CompositeData instance that represents the current values for specific
     * Endpoint statistics.
     * @return The delivery channel statistics in a CompositedData instance
     */
    CompositeData getEndpointStatistics(String epName);

    void dumpState();
}
