/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Aggregate.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.aggregate;

import com.sun.jbi.framework.StringTranslator;
//import com.sun.jbi.framework.descriptor.Connection;
//import com.sun.jbi.framework.descriptor.ServiceUnit;
import org.glassfish.openesb.api.eip.aggregate.Aggregate;
import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.message.MessageProps;

import org.osgi.framework.Bundle;

import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.interceptors.LocalStringKeys;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;
import com.sun.jbi.interceptors.internal.flow.AbstractFlow;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;

/**
 * This class provides support for message Aggregates in a IFL route definition.
 * 
 * @author Sun Microsystems, Inc.
 */
public class AggregateController 
        extends AbstractFlow {
    
    private static final String DOCUMENT_ROOT = "aggregatedDocument";
    private static final String WAIT_INTERVAL = "[\\d]+[\\x2E]*[\\d]*";
    private static final String WAIT_UNIT = "[[hms]?|[ms]?]";
    private static final String HOUR = "h";
    private static final String MIN  = "m";
    private static final String SEC  = "s";
    private static final String MS   = "ms";
            
    private              Pattern unitPattern_ = Pattern.compile(WAIT_UNIT);


    private enum Config
    {
        TIMEOUT    ("timeout"),
        ON_TIMEOUT ("on-timeout");

        private String config_;

        Config(String config) {
            config_ = config;
        }

        @Override
        public String toString() {
            return config_;
        }
    }
    
    
    private enum OnTimeout
    {
        // Timer Expires: ERROR
        ERROR    ("error"),
        
        // Timer Expires: SEND
        SEND   ("send");

        private String value_;

        OnTimeout(String value) {
            value_ = value;
        }

        @Override
        public String toString() {
            return value_;
        }
    }
    

    // Map containing the information for the Aggregate
    private Map<String, Object> aggregateInfo_;
    private String              aggregateName_;
    private Logger              log_        = Logger.getLogger(AggregateController.class.getPackage().getName());
    StringTranslator            translator_ = new StringTranslator("com.sun.jbi.interceptors", null);
    
    private HashMap<MessageExchange,Object>             correlations_;
     
    // Aggregate is stateful
    /** [ correlationId | timer ]  */
    private HashMap<Object, AggregateTimer>     aggregateTimers_;
    /** [ correlationId | aggregated messages ] */
    private HashMap<Object, AggregatedMessages> AggregatedMessages_;
    
    // Configuration
    private OnTimeout  onTimeout_ = OnTimeout.SEND;
    private float      timeout_; // in ms
    
    // Pluggable implementation
    private Aggregate  aggregateImpl_;
    

    
    /**
     * Creates a new Aggregate flow.
     * @param consumedService - the consumed service 
     * @param Map<String, String> - map containing the Aggregate information
     */
    public AggregateController(String name, String namespace, Map<String, Object> aggregateInfo, Bundle bundle) throws Exception {
        
        super(namespace, name, com.sun.jbi.fuji.ifl.Flow.AGGREGATE);
        
        // Configuration
        timeout_ = 0;
        aggregateInfo_ = aggregateInfo;
        config_ = (Properties)aggregateInfo_.get(Flow.Keys.CONFIG.toString());
        initConfig();
        
        // Create the type implementation and initialize it with it's configuration
        AggregateType aggType = AggregateType.fromString((String)aggregateInfo.get(Flow.Keys.TYPE.toString()));
        aggregateImpl_ = AggregateFactory.createAggregator(aggType, config_, bundle);
        aggregateImpl_.setConfiguration(config_);
        correlations_ = new HashMap();

        // register a single endpoint in the NMR endpoint registry for 
        // a aggregate endpoint
        aggregateName_ = (String)aggregateInfo.get(Flow.Keys.NAME.toString());
        activateEndpoint(getServiceQName(aggregateName_), aggregateName_, false);
        
        
    }

    protected boolean useNew() { return (true); }
    protected boolean acceptEvent(int id) {
        return (id == com.sun.jbi.ext.DeliveryChannel.EVENT_SIGNAL);
    }

    protected void onRequest(AbstractFlow.InboundContext ic) {
        MessageExchange     request = ic.getExchange();

        try {
            if (log_.isLoggable(Level.FINER)) {
                log_.finer("EIP: Aggregate(" + aggregateName_ + ") Exchange: \n" + request.toString());
            }
            log_.fine("EIP: Aggregate(" + aggregateName_ + "," + request.getExchangeId() + ")");
            aggregateExchange(ic);
         } catch (javax.jbi.messaging.MessagingException mEx) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            mEx.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: Aggregate(" + aggregateName_ + "," + request.getExchangeId() + ") Exception: " + b.toString() + toString());
        }
    }
    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Aggregate (" + aggregateName_ + "," + ic.getExchange().getExchangeId() + ") Complete");
    }
    protected void onSignal(AbstractFlow.InboundContext ic) {
        MessageExchange     exchange = ic.getExchange();
        Object              correlation = correlations_.get(exchange);

        log_.fine("EIP:Aggregate(" + aggregateName_ + "," + exchange.getExchangeId() +
                ") Correlation " + (correlation == null ? "not found" : "found"));
        if (correlation != null) {
            try {
                fireExchange(correlation);
            } catch (Exception e) {
            }
        }
    }
    /**
     * Keep aggregating the exchange till the aggregate conditions are met and message can be sent.
     * 
     * @param exchange - the current message exchange
     */
    synchronized private void aggregateExchange(AbstractFlow.InboundContext ic)
            throws javax.jbi.messaging.MessagingException
    {
        // The correlation id comes from the type impl
        MessageExchange exchange = ic.getExchange();
        Object      corrId = null;

        try
        {
            ServiceMessage msg = MessageExchangeUtil.getActiveMessage(exchange);
            corrId = aggregateImpl_.getCorrelationID(msg);
            
            AggregatedMessages messages = getAggregatedMessages(corrId);
            if ( messages == null )
            {
                // First message with the correlation id
                messages = new AggregatedMessages(getExchangeFactory(), exchange, corrId);
                addAggregatedMessages(corrId, messages);
            }
            messages.addMessageFromExchange(exchange);
            if ( aggregateImpl_.isAggregateComplete(corrId, getAggregatedMessages(corrId).getMessages()) ) {
                fireExchange(corrId);
                return;
            }
            
            //
            //  The aggregated exchange will be used to trigger the output.
            //  If we are not the chosen exchange, then just reply with an empty message.
            //
            if (messages.getAggregatedExchange() != exchange) {
                reply(ic);
            } else {
                //
                //  Suspend this exchange while waiting for more work.
                //
                suspend(exchange);
                correlations_.put(exchange, corrId);
            }

            //
            //  See if we should start a timer.
            //
            if (timeout_ > 0 && getTimer(corrId) == null) {
                AggregateTimer timer = new AggregateTimer(corrId, new Float(timeout_).longValue(), this);
                addTimer(corrId, timer);
            }
        }
        catch (Exception ex){
            // If there is an error in processing, set Exchange status as Error 
            // and short-circuit it
            log_.log(Level.WARNING, "Aggregate of exchange failed.", ex);
            exchange.setError(ex);
            exchange.setStatus(ExchangeStatus.ERROR);
        }        
    }

    
    /**
     * Send out all the aggregated messages. This is called when one of the
     * aggregate conditions { timeout, size, count } is met to send the messages.
     * 
     * @corrId - correlation id
     * @throws java.lang.Exception
     */
    synchronized private void fireExchange(Object corrId)
        throws Exception
    {
        AggregatedMessages aggregatedMessages = getAggregatedMessages(corrId); 
        
        if ( aggregatedMessages == null )
        {
            return;
        }
        
        MessageExchange aggregatedExchange = aggregatedMessages.getAggregatedExchange();
        if ( aggregatedExchange != null )
        {
            log_.finest("Aggregated exchange " + aggregatedExchange.getExchangeId() + "  will be sent.");

            Object  payload = aggregateImpl_.aggregateMessages(corrId, aggregatedMessages.getMessages() );
            // Set the InMsg
            ServiceMessage msg = MessageExchangeUtil.getActiveMessage(aggregatedExchange);         
            if ( msg != null )
            {
                msg.setPayload(payload);
            }
            HashMap messageProperties = aggregatedMessages.getAggregatedMessageProperties();
            // Set the message properties
            if ( messageProperties != null )
            {
                Set<String> propertyNames = messageProperties.keySet();

                for (String name : propertyNames )
                {
                    ((NormalizedMessage)msg).setProperty(name, messageProperties.get(name) );
                }
            }
//            msg.setProperty("org.glassfish.openesb.messaging.messageid", "1");
//            msg.setProperty("org.glassfish.openesb.messaging.finalmessageid", "1");
            aggregatedExchange.setProperty("com.sun.jbi.messaging.groupid", corrId + "-A-1");
            aggregatedExchange.setProperty("com.sun.jbi.messaging.messageid", "1");
         

            ((InOut)aggregatedExchange).setOutMessage(msg);
             // invoke the service
            send(aggregatedExchange);
            // Once the message is fired, reset state            
            reset(corrId);
            correlations_.remove(aggregatedExchange);
        }
        else
        {
            log_.finest("Asked to send aggregated exchange, but there is no exchange");
        }
    }
    
    /**
     * Send out all the aggregated messages. This is called when one of the
     * aggregate conditions { timeout, size, count } is met to send the messages.
     * 
     * @throws java.lang.Exception
     */
    private void exchangeTimedOutError(Object corrId)
        throws Exception
    {
        if ( getAggregatedMessages(corrId).getAggregatedExchange() != null )
        {
            String timedOutErr = translator_.getString(LocalStringKeys.AGGREGATE_ERROR_TIMED_OUT, timeout_);
            
            log_.warning(timedOutErr);
        }
        else
        {
            log_.finest("Timeout interval of " + timeout_ + " ms expired, but there is no aggregated exchange");
        }
    }
    /**
     * Once the exchange is fired, reset the state
     */
    void reset(Object corrId){        
        AggregateTimer timer = getTimer(corrId);
        
        if ( timer != null )
        {
            log_.finest("Purging aggregate timer : " + timer.toString());
            timer.purge();
            removeTimer(corrId);
            timer = null;
        }
        
        removeAggregatedMessages(corrId);
    }
    
    /**
     * Parse the aggregate configuration and save the information. 
     * 
     * @param config - aggregate configuration properties in a string
     */
    void initConfig()
        throws Exception
    {
        if ( config_ != null ){
            Enumeration keySet = config_.propertyNames();
            
            while (keySet.hasMoreElements()) {
                String key  = (String)keySet.nextElement();
                String value = (String)config_.getProperty(key);
                
                if ( !"".equals(value) ){
                    if ( key.equalsIgnoreCase(Config.ON_TIMEOUT.toString())){
                        for ( OnTimeout to: OnTimeout.values() ){
                            if (to.toString().equals(value)){
                                onTimeout_ = to;
                            }
                        }
                    }else if ( key.equalsIgnoreCase(Config.TIMEOUT.toString())){
                        // TODO: take care of h,m,s,ms at end and convert to ms timeout 
                        if ( !Pattern.matches(WAIT_INTERVAL + WAIT_UNIT, value) ){
                            throw new Exception("Incorrectly formatted timeout interval " + value );
                        }else{
                            String[] timeout = unitPattern_.split(value);
                            float interval = Float.parseFloat(timeout[0]);

                            String unit = value.substring(timeout[0].length(), value.length());
                            if ( unit != null ){
                                if (HOUR.equalsIgnoreCase(unit)){
                                    timeout_ = interval *60*60*1000;
                                }else if (MIN.equalsIgnoreCase(unit)){
                                    timeout_ = interval *60*1000;
                                } else if (SEC.equalsIgnoreCase(unit)){
                                    timeout_ = interval *1000;
                                } else {
                                    timeout_ = interval;
                                }
                            }
                        }
                    }  
                }
            }
        }
    }

    /**
     * A  Timer expired
     * 
     * @param corrId
     */
     synchronized public void timedOut(AggregateTimer timer){
           try{
               log_.finest("Aggregating timer for " + timer.getCorrelationId() + " with timeout = " + timer.getTimeout() + " ms expired");

               if ( OnTimeout.ERROR.equals(onTimeout_)){
                   exchangeTimedOutError(timer.getCorrelationId());
               }else{
                   fireExchange(timer.getCorrelationId());
               }

           } catch(Exception ex ){
                   log_.log(Level.FINEST, "Failed to fire aggregated exchange after timer expired ", ex);
           }
     }
     
     /*-----------------------------------------------------------------------*\
                                 Timer Management 
     \*-----------------------------------------------------------------------*/
     
     /***
      * @return map of active timers keyed by correlation id
      */
     private HashMap<Object, AggregateTimer> getTimers() {
         if ( aggregateTimers_ == null ) {
             aggregateTimers_ = new HashMap();
         }
         return aggregateTimers_;
     }
     
     /**
      * Add a new timer
      * 
      * @param corrId - correlation Id
      * @param timer - new timer to add
      */
     private void addTimer(Object corrId, AggregateTimer timer) {
         getTimers().put(corrId, timer);
     }
        
     /**
      * Remove a timer
      * 
      * @param corrId - correlation Id
      */
     private void removeTimer(Object corrId) {
         getTimers().remove(corrId);
     }
     
     /**
      * Get an aggregate timer
      * 
      * @param corrId - correlation Id
      */
     private AggregateTimer getTimer(Object corrId) {
         return getTimers().get(corrId);
     }
     
     /*-----------------------------------------------------------------------*\
                                Aggregated Messages Management 
     \*-----------------------------------------------------------------------*/
     /**
      * @return map of AggregatedMessages keyed by correlation id
      */
     private HashMap<Object, AggregatedMessages> getAggregatedMessagesMap() {
         if ( AggregatedMessages_ == null ) {
             AggregatedMessages_ = new HashMap();
         }
         return AggregatedMessages_;
     }
     
     /**
      * Add a new AggregatedMessages instance to the map.
      * 
      * @param corrId - correlation Id
      * @param messages - AggregatedMessages instance to add
      */
     private void addAggregatedMessages(Object corrId, AggregatedMessages messages) {
         getAggregatedMessagesMap().put(corrId, messages);
     }
        
     /**
      * Remove a timer
      * 
      * @param corrId - correlation Id
      */
     private void removeAggregatedMessages(Object corrId) {
         getAggregatedMessagesMap().remove(corrId);
     }
     
     /**
      * Get aggregated messages corresponding to a correlation id.
      * 
      * @param corrId - correlation Id
      */
     private AggregatedMessages getAggregatedMessages(Object corrId) {
         return getAggregatedMessagesMap().get(corrId);
     }

     public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        Aggregate (" + aggregateName_ + ")\n");
        if (AggregatedMessages_ != null) {
            for (Map.Entry<Object,AggregatedMessages> t : AggregatedMessages_.entrySet()) {
                String              id = (String)(t.getKey());
                AggregatedMessages  am = t.getValue();

                sb.append("          CorrelationId:   " + id + "\n");
                sb.append("            Count:           " + am.getMessagesList().size() + "\n");
                if (getTimer(id) != null) {

                    sb.append("            Timer:           Enabled");
               }
            }
        }
        for (Map.Entry<MessageExchange, Object> e : correlations_.entrySet()) {
            sb.append("        ExchangeId:      " + e.getKey().getExchangeId() + "\n");
            sb.append("          CorrelationId: " + e.getValue().toString() + "\n");
        }
        return (sb.toString());
     }
}
