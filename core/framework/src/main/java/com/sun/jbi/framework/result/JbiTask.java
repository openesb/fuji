/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiTask.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.io.InputStream;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import com.sun.jbi.framework.result.TaskResultDetails.MessageType;
import com.sun.jbi.framework.result.TaskResultDetails.TaskId;
import com.sun.jbi.framework.result.TaskResultDetails.TaskResult;

/**
 * Simple object representation of a JBI management message.
 * @author kcbabo
 */
public class JbiTask extends ResultElement {
    
    private static Logger logger_ = Logger.getLogger(
            JbiTask.class.getPackage().getName());
    
    private static final String DEFAULT_VERSION =
            "1.0";
//    private static final String JBI_TASK =
//            "jbi-task";
    private static final String VERSION =
            "version";
    
    private static DocumentBuilder docBuilder;  
    
    
    static {
        try {
            // initialize DOM
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // most components do not include correct NS URI...
//            dbf.setNamespaceAware(true);
            docBuilder = dbf.newDocumentBuilder();
            
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public JbiTask(Element element) throws Exception {
        super(element);
    }
    
    /**
     * Creates a new JbiTask object from a stream containing an XML management
     * message.
     * @param input input stream containing an XML management message
     * @return JbiTask object corresponding to the XML management message
     * @throws java.lang.Exception failed to parse XML management message
     */
    public static synchronized JbiTask newJbiTask(InputStream input) 
            throws RuntimeException {
        try {
            Document doc = docBuilder.parse(input);
            return new JbiTask(doc.getDocumentElement());
        } 
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (Exception ex) { ; }
        }
    }
    
    public static synchronized JbiTask newJbiTask() throws RuntimeException {
        
        try {
            Document doc = docBuilder.newDocument();
            JbiTask jt = new JbiTask(
                    doc.createElement(TaskElem.jbi_task.toString()));
            doc.appendChild(jt.getElement());
            return jt;
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public static synchronized ComponentTaskResult createComponentTaskResult(String xml) {
        Element elem = null;
        try {
            elem = stringToElement(xml);
            return new ComponentTaskResult(elem);
        }
        catch (Exception ex) {
            logger_.log(Level.INFO, "Failed to parse component response mesage.", ex);
        }
        
        return null;
    }

    public static synchronized ComponentTaskResult createComponentTaskResult(
            String compName, TaskId taskId, TaskResult result, MessageType msgType) {
        String mmxml = "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"+
                "<component-name>{0}</component-name>"+                     // 0=COMP NAME
                "<component-task-result-details><task-result-details>"+     // 1=TASK ID
                "<task-id>{1}</task-id><task-result>{2}</task-result>"+     // 2=TASK RESULT
                "<message-type>{3}</message-type>" +                        // 3=MSG TYPE
                "</task-result-details>"+
                "</component-task-result-details></component-task-result>";

        try {
            mmxml = MessageFormat.format(mmxml, compName, taskId, result, msgType);
            return createComponentTaskResult(mmxml);
        }
        catch (Exception e) {
            logger_.warning("Failed to build management message: "+ e.getMessage());
        }

        return null;
    }

    /**
     * Create a framework result message with the specified message.  Specifying
     * isSuccess = true will produce a SUCCESS/INFO result, while isSuccess = 
     * false yields an ERROR/FAILED message.  
     * @param message result message
     * @param isSuccess true for SUCCESS, false for FAILED
     * @return object representation of XML management message
     */
    public static JbiTask createTaskResult(
            String message, boolean isSuccess) {
        
        JbiTask jt = newJbiTask();
        FrameworkTaskResult ftr = jt.getJbiTaskResult().getFrameworkTaskResult();
        TaskResultDetails trd = ftr.getFrameworkTaskResultDetails().getTaskResultDetails();
        
        // Set the result status information
        if (isSuccess) {
            trd.setMessageType(TaskResultDetails.MessageType.INFO);
            trd.setTaskResult(TaskResultDetails.TaskResult.SUCCESS);
        }
        else {
            ftr.setIsCauseFramework(FrameworkTaskResult.IsCauseFramework.YES);
            trd.setMessageType(TaskResultDetails.MessageType.ERROR);
            trd.setTaskResult(TaskResultDetails.TaskResult.FAILED);
        }
        
        // Set the result message
        trd.addTaskStatusMsg().setMessage(message);
        return jt;
    }
        
    /**
     * Create a framework error message with the specifed message and component
     * result.  This method assumes that the component result message is an 
     * XML management message indicating failure.
     * @param message framework result message
     * @param componentResult string returned from component SPI method
     * @return object representation of XML management message
     */
    public static JbiTask createTaskResult(
            String message, String componentResult) {
        
        JbiTask jt = createTaskResult(message, false);
        jt.getJbiTaskResult().getFrameworkTaskResult().setIsCauseFramework(
                FrameworkTaskResult.IsCauseFramework.NO);
        
        // Try and parse the component response into an XML mgmt message
        try {
            Element element = stringToElement(componentResult);
            jt.getJbiTaskResult().addComponentTaskResult(new ComponentTaskResult(element));
        }
        catch (Exception ex) {
            logger_.log(Level.INFO, "Failed to parse component response mesage.", ex);
        }
        
        return jt;
    }
    
    /**
     * Create a framework error message from details provided in the exception.
     * @param error exception with failure details
     * @return
     */
    public static JbiTask createTaskResult(Exception error) {
        
        JbiTask jt = newJbiTask();
        FrameworkTaskResult ftr = jt.getJbiTaskResult().getFrameworkTaskResult();
        TaskResultDetails trd = ftr.getFrameworkTaskResultDetails().getTaskResultDetails();
        
        // Set the result status information
        ftr.setIsCauseFramework(FrameworkTaskResult.IsCauseFramework.YES);
        trd.setMessageType(TaskResultDetails.MessageType.ERROR);
        trd.setTaskResult(TaskResultDetails.TaskResult.FAILED);
        
        // Set the result message
        ExceptionInfo ei = trd.addExceptionInfo();
        LocalizedMessage lm = ei.getLocalizedMessage();
        System.out.println(lm);
        lm.setMessage(error.getMessage());
        
        return jt;
    }

    @Override
    protected void initialize() {
        setVersion(DEFAULT_VERSION);
        createChildElement(TaskElem.jbi_task_result);
    }
    
    /**
     * Returns the version of the management message.
     * @return management message version
     */
    public String getVersion() {
        return element_.getAttribute(VERSION);
    }
    
    public void setVersion(String version) {
        element_.setAttribute(VERSION, version);
    }
    
    /**
     * Returns object corresponding to jbi-task-result element.
     * @return JbiTaskResult if the element exists, null otherwise
     */
    public JbiTaskResult getJbiTaskResult() {
        Element jtr = getChildElement(TaskElem.jbi_task_result);
        return jtr != null ? new JbiTaskResult(jtr) : null;
    }
    
    private static Element stringToElement(String xml) throws Exception {
        return docBuilder.parse(new InputSource(
                new StringReader(xml))).getDocumentElement();
    }
}
