/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.Translator;
import com.sun.jbi.messaging.stats.METimestamps;
import java.net.URI;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.transaction.Transaction;

import javax.xml.namespace.QName;

/** This serves as the container about all of the state of a message exchange.
 *  The objects presented to the binding and engine are instances of
 *  MessageExchangeProxy. The MessageExchangeProxy forwards all get/set
 *	requests to here. This class maintains a reference to each.
 * @author Sun Microsystems, Inc.
 */
public class MessageExchangeImpl
    implements javax.jbi.messaging.MessageExchange, Cloneable 
{
    /** NMR sendSync spec value. Note: no name was given in the spec. */
    private static final String         SENDSYNC_PROPERTY_NAME = "javax.jbi.messaging.sendSync";

    /** Default factory for normalized messages. */
    private static MessageFactory       sMsgFac;
    
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
    /** Service reference for this exchange. */
    private QName                       mService;
    /** Interface name for this exchange. */
    private QName                       mInterfaceName;
    /** Endpoint reference for this exchange. */
    private ServiceEndpoint             mEndpoint;
    /** Operation name for this exchange. */
    private QName                       mOperation;
    /** WSDL Fault. */
    private Fault                       mFault;
    /** Stores messages for this exchange. */
    private HashMap                     mMessages;
    /** Stores exchange properties. */
    private HashMap                     mProperties;
    private HashMap                     mDeltaProperties;
    
    /** Unique Id for exchange. */
    private String                      mExchangeId;
    /** Status of the exchange. */
    private ExchangeStatus              mStatus;
    /** Error information. */
    private Exception                   mError;

    private MessageExchangeProxy        mParent;
    private List<MessageExchange>       mChildren;
    
    /** Transaction for this exchange.  Since the javax.transaction is not 
     *  part of Java SE, we need to store the Transaction reference as an object.
     */
    private Object                      mTransaction;
    
    private boolean                     mAutoSuspendResume;
    /** Source MessageExchange.	*/
    private MessageExchangeProxy        mSource;
    /** MessageService          */
    private MessageService              mMsgSvc;
    /** Created Remotely    */
    private boolean                     mRemote;
    /** SendSync used on last send */
    private boolean                     mSendSync;
    /** MessageExchange statistics */
    private METimestamps                mStamps;

    /**
     * Linked endpoint enabled through a service connection.  This endpoint
     * is only visible to the service consumer; the provider sees the 
     * actual endpoint that was activated.
     */
    private ServiceEndpoint                     mEndpointLink;    
    
    /** Static init of random number generator and message factory. */
    static 
    {
         sMsgFac = MessageFactory.getInstance();
    }
    
    /**
     * Create a new message exchange for the given proxy.
     * @param proxy MessageExchangeProxy for pattern
     */
    MessageExchangeImpl(MessageExchangeProxy proxy, MessageService msgSvc) 
    {
        boolean statisticsEnabled = msgSvc.areStatisticsEnabled();
        
        mMsgSvc = msgSvc;
        mSource = proxy;
        mAutoSuspendResume = false;
        proxy.setMessageExchange(this, statisticsEnabled);
        if (statisticsEnabled)
        {
            mStamps = new METimestamps();
        }
        init();
    }
    
    /** Initialize instance vars and set state to 0. */
    private void init() 
    {
        mExchangeId = mMsgSvc.generateNextId();
        mProperties = new HashMap();
        mDeltaProperties = new HashMap();
        mMessages   = new HashMap();
        mStatus = ExchangeStatus.ACTIVE;
        mRemote = false;
    }
    
    /** Deferred to message factory. */
    public Fault createFault()
        throws MessagingException 
    {
        return sMsgFac.createFault();
    }
    
    /** Deferred to message factory. */
    public NormalizedMessage createMessage()
        throws MessagingException 
    {
        return sMsgFac.createMessage();
    }
    
    /** Obvious. */
    public ServiceEndpoint getEndpoint() 
    {
        return mEndpoint;
    }
    
    /** Obvious. */
    public Exception getError() 
    {
        return mError;
    }
    
    /** Obvious. */
    public String getExchangeId() 
    {
        return mExchangeId;
    }
    
    /** Obvious. */
    public Fault getFault() 
    {
        return mFault;
    }
    
    /** Return the message associated with the given reference id. */
    public NormalizedMessage getMessage(String name) 
    {
        return (NormalizedMessage)mMessages.get(name);
    }
    
    /** Obvious. */
    public QName getOperation() 
    {
        return mOperation;
    }
    
    /** Obvious. */
    public URI getPattern() 
    {
        return (mSource == null ? null : mSource.getPattern());
    }
    
    /** Obvious. */
    public Object getProperty(String name) 
    {
        Object prop = mDeltaProperties.get(name);
        if (prop == null)
        {
            prop = mProperties.get(name);
        }
        if (prop == null)
        {
            if (mSendSync && name.equals(SENDSYNC_PROPERTY_NAME))
            {
                prop = Boolean.valueOf(true);
            }
            else if (mParent != null && name.equals("com.sun.jbi.messaging.parent"))
            {
                prop = mParent.getExchangeId();
            }
        }
        return (prop);
    }
    
    /** Obvious. */
    public Set getPropertyNames()
    {
        Set names = new HashSet();
        
        names.addAll(mProperties.keySet());
        names.addAll(mDeltaProperties.keySet());
        if (mSendSync)
        {
            names.add(SENDSYNC_PROPERTY_NAME);
        }
        if (mParent != null)
        {
            names.add("com.sun.jbi.messaging.parent");
        }
        return names;
    }
    
    public MessageExchange.Role getRole()
    {
        return (null);
    }
    
    /** Obvious. */
    public QName getService() 
    {
        return mService;
    }
    
    /** Obvious. */
    public ExchangeStatus getStatus() 
    {
        return mStatus;
    }
  
    /** Special for serialization.  */
    Set getDeltaProperties()
    {
        return (mDeltaProperties.entrySet());
    }
    
    void setEndpointLink(ServiceEndpoint se)
    {
        mEndpointLink = se;
    }
    
    public ServiceEndpoint getEndpointLink()
    {
        return mEndpointLink;
    } 
    
    /** Used for deserialization support. */
    void setExchangeId(String id)
    {
        mExchangeId = id;
        mRemote = true;
    }
    
    /** Only allowed on a freshly created exchange. */
    public void setEndpoint(ServiceEndpoint endpoint) 
    {
        mEndpoint = endpoint;
    }
    
    /** Sets the exception and automatically advances status to Error. */
    public void setError(Exception error) 
    {
        mError  = error;
    }
    
    /** Set a fault on the exchange. */
    public void setFault(Fault fault)
    {
        mFault = fault;
    }
    
    /** Set a message on the exchange. */
    public void setMessage(NormalizedMessage msg, String name)
    {
        if (msg == null)
        {
            mMessages.remove(name);
        }
        else
        {
            mMessages.put(name, msg);
        }
    }
    
    /** Only allowed on a freshly created exchange. */
    public void setOperation(QName name) 
    {
        mOperation = name;
    }
    
    /** Obvious. */
    public void setProperty(String name, Object obj) 
    {
        if (!name.equals(SENDSYNC_PROPERTY_NAME))
        {
            if (name.equals(JTA_TRANSACTION_PROPERTY_NAME))
            {
                try
                {
                    setTransactionContext((Transaction)obj);
                }
                catch (javax.jbi.messaging.MessagingException msgEx)
                {
                    mLog.warning(msgEx.toString());
                }
            }
            else if (name.equals("com.sun.jbi.messaging.parent") && mParent == null)
            {
                MessageExchangeProxy    mep = (MessageExchangeProxy)obj;

                mParent = mep;
                return;
            }
            mDeltaProperties.put(name, obj);
        }
    }
    
    public void mergeProperties()
    {
        mProperties.putAll(mDeltaProperties);
        mDeltaProperties.clear();
    }
    
    void setSyncProperty(boolean isSync)
    {
       mSendSync = isSync;
    }
    
    boolean isRemote()
    {
        return (mRemote);
    }
    
    /** Only allowed on a freshly created exchange. */
    public void setService(QName service) 
    {
        mService = service;
    }
    
    /** Set status of the exchange.  ERROR status is allowed at any point. */
    public void setStatus(ExchangeStatus status) 
    {
        mStatus = status;
    }    
    
    public QName getInterfaceName()
    {
        return mInterfaceName;
    }
    
    public void setInterfaceName(QName interfaceName)
    {
        mInterfaceName = interfaceName;
    }
    
    /** Only allowed on a freshly created exchange. */
    void setTransactionContext(Transaction xact)
        throws javax.jbi.messaging.MessagingException 
    {
        if (xact == null)
        {
            try 
            {
                xact = mMsgSvc.getTransactionManager().getTransaction();
            }
            //catch (javax.transaction.SystemException se) 
            catch (Exception ex)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_GET_DEFAULT_TRANSACTION));
            }
        }
        mTransaction = xact;
        
    }
    
    /** Only allowed on a freshly created exchange. */
    public Transaction getTransactionContext()
    {
        return (Transaction)mTransaction;
    }
    
    /** Only allowed on a freshly created exchange. */
    public boolean isTransacted() 
    {
        return (mTransaction != null);
    }
    
    public MessageExchangeProxy getSource()
    {
        return (mSource);
    }

    public synchronized void addChild(MessageExchange me)
    {
        if (mChildren == null)
        {
            mChildren = new ArrayList();
        }
        mChildren.add(me);
    }

    public synchronized void removeChild(MessageExchange me)
    {
        if (mChildren != null)
        {
            mChildren.remove(me);
        }
    }

    public synchronized MessageExchange[] getChildren()
    {
        if (mChildren != null)
        {
            return (mChildren.toArray(new MessageExchange[mChildren.size()]));
        }
        return (null);
    }

    public void setParent(MessageExchangeProxy me)
    {
        mParent = me;
    }

    public MessageExchangeProxy  getParent()
    {
        return (mParent);
    }

    /** Suspend the current transaction if defined. */
    void suspendTX()
        throws javax.jbi.messaging.MessagingException 
    {
        if (mTransaction != null) 
        {
            try 
            {
                if (mAutoSuspendResume)
                {
                    if (mLog.isLoggable(Level.FINE))
                    {
                        mLog.fine("Suspending transaction on exchange " + mExchangeId);
                    }
                    if (mTransaction == mMsgSvc.getTransactionManager().getTransaction()) 
                    {
                        mMsgSvc.getTransactionManager().suspend();
                    }
                }
                else
                {
                    if (mMsgSvc.getTransactionManager().getTransaction() == mTransaction)
                    {
                        throw new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.MUST_SUSPEND));
                    }
                }
            }
            //catch (javax.transaction.SystemException se) 
            catch (Exception ex) 
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_SUSPEND));
            }
        }
    }
    
    /** Resume the current transaction if defined. */
    void resumeTX()
        throws javax.jbi.messaging.MessagingException 
    {
        if (mTransaction != null && mAutoSuspendResume) 
        {
            try 
            {
                if (mLog.isLoggable(Level.FINE))
                {
                    mLog.fine("Resuming transaction on exchange " + mExchangeId);
                }
                mMsgSvc.getTransactionManager().resume((Transaction)mTransaction);
            }
            catch (javax.transaction.InvalidTransactionException ite) 
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_RESUME_INVALID));
            }
            //catch (javax.transaction.SystemException se) 
            catch (Exception ex) 
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.CANT_RESUME));
            }
        }
    }
    
    //
    //  Capture a timestamp associated with the given tag.
    //
    void capture(byte tag)
    {
        if (mStamps != null)
        {
            mStamps.capture(tag);
        }
    }

    METimestamps getTimestamps()
    {
        return (mStamps);
    }
    
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        sb.append("\n        Status: ");
        sb.append(mStatus.equals(ExchangeStatus.ACTIVE) ? "ACTIVE" :
            (mStatus.equals(ExchangeStatus.DONE) ? "DONE" : "ERROR"));
        sb.append("  Location: ");
        sb.append(mRemote ? "REMOTE" : "LOCAL");
        if (mTransaction != null)
        {
            sb.append(" Transaction:   ");
            sb.append(mTransaction == null ? "None" : mTransaction.toString());
        }
        sb.append("\n");
        if (mEndpointLink != null)
        {
            sb.append("        EndpointLink: ");
            sb.append(((RegisteredEndpoint)mEndpointLink).toExternalName());
            sb.append("\n");
        }
        if (mService != null)
        {
            sb.append("        Service:   " + mService + "\n");
        }
        if (mEndpoint != null)
        {
            sb.append("        Endpoint:  \n" + ((RegisteredEndpoint)mEndpoint).toStringBrief());
        }
        if (mOperation != null)
        {
            sb.append("        Operation: " + mOperation + "\n");
        }
        if (mInterfaceName != null)
        {
            sb.append("        Interface: " + mInterfaceName + "\n");
        }
        if (mParent != null)
        {
            sb.append("        ParentId:  " + mParent.getExchangeId() + "\n");
        }
        if (mProperties.size() > 0 || mSendSync)
        {
            sb.append("        Properties Count: " + mProperties.size() + "\n");
            if (mSendSync)
            {
                sb.append("          Name: " + SENDSYNC_PROPERTY_NAME);
                sb.append("\n          Value: true\n");
            }
            for (Iterator p = mProperties.entrySet().iterator(); p.hasNext(); )
            {
                Map.Entry   me = (Map.Entry)p.next();
                sb.append("          Name: ");
                sb.append((String)me.getKey());
                if (me.getValue() instanceof String)
                {
                    sb.append("\n            Value: ");
                    sb.append((String)me.getValue());
                }
                else if (me.getValue() instanceof QName)
                {
                    sb.append("\n            Value(QName): ");
                    sb.append((QName)me.getValue());
                }
                else if (me.getValue() instanceof Long)
                {
                    sb.append("\n            Value(Long): ");
                    sb.append((Long)me.getValue());
                }
                else if (me.getValue() instanceof AbstractCollection)
                {
                    sb.append("\n            Collection(Type): " + me.getValue().getClass().getSimpleName());

                    for (Object o : ((AbstractCollection)me.getValue()))
                    {
                        if (o instanceof String)
                        {
                            sb.append("\n              Value(String)(" + (String)o + ")");
                        }
                        else
                        {
                            sb.append("\n              Value(Type): " + o.getClass().getSimpleName());
                        }
                    }
                }
                else
                {
                    sb.append("\n          Value(Type): ");
                    sb.append(me.getValue() == null ? "null" : me.getValue().getClass().getName());
                }
                sb.append("\n");
            }
        }
        if (mDeltaProperties.size() > 0)
        {
            sb.append("        DeltaProperties Count: " + mDeltaProperties.size() + "\n");
            for (Iterator p = mDeltaProperties.entrySet().iterator(); p.hasNext(); )
            {
                Map.Entry   me = (Map.Entry)p.next();
                sb.append("          Name: ");
                sb.append((String)me.getKey());
                if (me.getValue() instanceof String)
                {
                    sb.append("\n            Value(String): ");
                    sb.append((String)me.getValue());
                }
                else if (me.getValue() instanceof QName)
                {
                    sb.append("\n            Value(QName): ");
                    sb.append((QName)me.getValue());
                }
                else if (me.getValue() instanceof Long)
                {
                    sb.append("\n            Value(Long): ");
                    sb.append((Long)me.getValue());
                }
                else if (me.getValue() instanceof AbstractCollection)
                {
                    sb.append("\n            Collection(Type): " + me.getValue().getClass().getSimpleName());

                    for (Object o : ((AbstractCollection)me.getValue()))
                    {
                        if (o instanceof String)
                        {
                            sb.append("\n              Value(String)(" + (String)o + ")");
                        }
                        else
                        {
                            sb.append("\n              Value(Type): " + o.getClass().getSimpleName());
                        }
                    }
                }
                else
                {
                    sb.append("\n            Value(Type): ");
                    sb.append(me.getValue() == null ? "null" : me.getValue().getClass().getName());
                }
                sb.append("\n");
            }
        }
        if (mMessages.size() > 0)
        {
            sb.append("        Message Count:   ");
            sb.append(mMessages.size());
            sb.append("\n");
            for (Iterator m = mMessages.entrySet().iterator(); m.hasNext(); )
            {
                Map.Entry   me = (Map.Entry)m.next();
                sb.append("          Message Name: ");
                sb.append((String)me.getKey());
                sb.append("\n");
                sb.append(me.getKey() == null ? "null" : me.getValue().toString());
            }
        }
        if (mFault != null)
        {
            sb.append("        Fault: \n");
            sb.append(mFault.toString());
        }
       return (sb.toString());
    }
}
