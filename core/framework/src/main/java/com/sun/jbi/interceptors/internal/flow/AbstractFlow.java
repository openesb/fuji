/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractFlow.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.service.ServiceMessageImpl;

import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.interceptors.Interceptor;
import com.sun.jbi.messaging.DeliveryChannelImpl;
import com.sun.jbi.messaging.RegisteredEndpoint;

/**
 * Base class for all message flow constructs.
 * @author Sun Microsystems, Inc.
 */
public class AbstractFlow implements Interceptor {

    //
    // EIP related NM properties.
    //
    public static String ROUTE_PATH =
            Route.class.getPackage().getName() + ".routePath";
    public static String ROUTE_HISTORY =
            Route.class.getPackage().getName() + ".routeHistory";
    public static String ROUTE_SERVICE =
            Route.class.getPackage().getName() + ".routeService";
    public static String PARENT_EXCHANGE = "com.sun.jbi.messaging.parent";

    // Endpoint activated in the NMR
    private ServiceEndpoint endpoint_;
    
    // Special runtime ComponentContext we create
    private DeliveryChannelImpl     deliveryChannel_;
    private Channel                 channel_;
    
    // Used to create exchanges
    private MessageExchangeFactory exchangeFactory_;
    
    protected String namespace_;
    private String name_;
    private QName  fullName_;
    private Flow   type_;
    protected Properties config_;
    
    public AbstractFlow(){};
    
    protected AbstractFlow(String namespace, String name, Flow type) {
        try {
            namespace_ = namespace + "/eip";
            name_ = name;
            type_ = type;
            fullName_ = new QName(namespace_, name_);
            deliveryChannel_ = Environment.getMessageService().activateChannel(
                this.getClass().getName(), null);
            channel_ = (Channel)deliveryChannel_.enableAcceptNotification(
                    new Channel(deliveryChannel_));
            channel_.addFlow(this);
            exchangeFactory_ = deliveryChannel_.createExchangeFactory();
        }
        catch (Exception ex) {
            // Unlikely and fatal
            throw new RuntimeException(ex);
        }
    }

    public void init() {};

    public void shutdown() {
        try {
            if (endpoint_ != null) {
                channel_.deactivateEndpoint(this, endpoint_);
            }
            channel_.removeFlow(this);
        } catch (javax.jbi.messaging.MessagingException mEx)
        {

        }
    }

    public QName getName() {
        return (fullName_);
    }
    
    public String getType(){
        return type_.toString();
    }
    
    public Properties getConfiguration(){
        return config_;
    }

    public ServiceEndpoint getEndpoint() {
        return endpoint_;
    }
        
    /**
     * @return true if the flow is dynamic
     * 
     */
    public boolean isDynamic(){
       return false; 
    }
      
    
    protected MessageExchangeFactory getExchangeFactory() {
        return exchangeFactory_;
    }

    protected void send(MessageExchange me)
            throws javax.jbi.messaging.MessagingException {
        channel_.send(this, me);
    }


    protected boolean acceptEvent(int id) { return (false); }

    // ------------------- EIP processing primitives -------------------------//

    protected void request(OutboundContext oc)
            throws javax.jbi.messaging.MessagingException {
//        ((com.sun.jbi.ext.MessageExchange)me).setParent(me);
        channel_.request(oc);
    }

    protected void reply(InboundContext ic)
            throws javax.jbi.messaging.MessagingException {
        channel_.reply(ic);
    }

    protected void status(InboundContext ic)
            throws javax.jbi.messaging.MessagingException {
        channel_.status(ic);
    }

    protected void status(OutboundContext oc)
            throws javax.jbi.messaging.MessagingException {
        channel_.status(oc);
    }

    protected void fault(InboundContext ic)
            throws javax.jbi.messaging.MessagingException {
        channel_.fault(ic);
    }

    protected void fault(OutboundContext oc)
            throws javax.jbi.messaging.MessagingException {
        channel_.fault(oc);
    }


    // ------------------- EIP processing callbacks --------------------------//

    protected void onRequest(AbstractFlow.InboundContext c) {
         throw new java.lang.RuntimeException("ERROR: AbstractFlow.onRequest() called in class " + this.getClass().getName());
    }

    protected void onStatus(InboundContext ic) {
         throw new java.lang.RuntimeException("ERROR: AbstractFlow.onStatus() called in class " + this.getClass().getName());
    }

    protected void onStatus(OutboundContext oc) {
         throw new java.lang.RuntimeException("ERROR: AbstractFlow.onStatus(,) called in class " + this.getClass().getName());
    }

    protected void onFault(InboundContext ic) {
         throw new java.lang.RuntimeException("ERROR: AbstractFlow.onFault() called in class " + this.getClass().getName());
    }

    protected void onFault(OutboundContext oc ) {
         throw new java.lang.RuntimeException("ERROR: AbstractFlow.onFault(,) called in class " + this.getClass().getName());
    }

    protected void onReply(OutboundContext oc) {
         throw new java.lang.RuntimeException("ERROR: AbstractFlow.onReply() called in class " + this.getClass().getName());
    }

    protected void onSuspend(OutboundContext oc) {
        throw new java.lang.RuntimeException("ERROR: AbstractFlow.onSuspend() called in class " + this.getClass().getName());
    }

    protected void onSignal(InboundContext oc) {
        throw new java.lang.RuntimeException("ERROR: AbstractFlow.onSignal() called in class " + this.getClass().getName());
    }

    protected void onResume(OutboundContext oc) {
        throw new java.lang.RuntimeException("ERROR: AbstractFlow.onResume() called in class " + this.getClass().getName());
    }

    // ------------------ EIP helpers ---------------------------------------//

    protected void resume(MessageExchange me) {
        channel_.resume(me);
    }

    protected void suspend(MessageExchange me) {
        channel_.suspend(me);
    }

    protected void signal(MessageExchange me) {
        channel_.signal(me);
    }

    protected void activateEndpoint(QName service, String endpoint, boolean includeOSGi)
        throws javax.jbi.messaging.MessagingException {
        
        endpoint_ = channel_.activateEndpoint(this, service, endpoint, includeOSGi);
    }
    
    protected QName getServiceQName(String serviceName) {
        return new QName(namespace_, serviceName);
    }

    protected void addressExchange(AbstractFlow.OutboundContext oc, String service,
                                   QName requestOperation, String defaultOp,
                                   NormalizedMessage nm)
            throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     exchange = oc.getExchange();
        QName               serviceName = getService(service);
        QName               operationOverride = getOperationOverride(service);
        QName               operation = null;
        QName               type = nm instanceof ServiceMessageImpl ? ((ServiceMessageImpl)nm).getMessageType() : null;
        ServiceEndpoint[]   se = deliveryChannel_.getEndpointsForService(serviceName);
        RegisteredEndpoint  re;

        if (se.length == 0) {
            throw new javax.jbi.messaging.MessagingException(
                    "Endpoint not found for service: " + service);
        }
        for (;;) {
            HashMap         ops;

            re = (RegisteredEndpoint)se[0];
            ops = re.getOperations();

            //
            //  An explicit operation in the route has first priority.
            //
            if (operationOverride != null) {
                operation = operationOverride;
                break;
            }

            //
            //  See if there is a unique operation based on the Exchange Type.
            //
            if (ops != null) {
                QName bestMatch = null;
                boolean multiple = false;
                for (Object o : ops.entrySet()) {
                    HashMap op = (HashMap)((Map.Entry)o).getValue();
                    String  pattern = ((QName)op.get("pattern")).getLocalPart();

                    if (exchange.getPattern().toString().equals(pattern)) {
                        if (bestMatch == null) {
                            bestMatch = (QName)((Map.Entry)o).getKey();
                        } else {
                            multiple = true;
                        }
                    }
                }
                if (bestMatch != null && multiple == false) {
                    HashMap op = (HashMap)ops.get(bestMatch);
                    operation = bestMatch;
                    oc.setInputType((QName)op.get("in-type"));
                    oc.setOutputType((QName)op.get("out-type"));
                    break;
                }
            }

            //
            // Use the operation that invoked the Route as the operation for this step.
            //
            // *UNLESS: the aforementioned operation is not Fuji standard Provider or Filter
            String routeOp = requestOperation.getLocalPart();
            if (!Route.FILTER_OPERATION_NAME.equals(routeOp) &&
                !Route.PROVIDER_OPERATION_NAME.equals(routeOp)) {
                operation = requestOperation;
            }
            else if (!defaultOp.equals(routeOp)) {
                operation = new QName(requestOperation.getNamespaceURI(), 
                                      defaultOp, requestOperation.getPrefix());
            }
            else {
                operation = requestOperation;
            }
            break;
        }

        exchange.setEndpoint(re);
        exchange.setOperation(operation);
        exchange.setMessage(nm, "in");
    }

    //
    //  A Route List string can be in one of the following formats:
    //
    //  service
    //  {ns}service
    //  service.operation
    //  {ns}service.operation
    //  service.{ns}operation
    //  {ns}service.{ns}operation
    //
    //

    protected QName getService(String route) {
        QName   service;
        int     period;
        int     rbracket;

        if (route.startsWith("{")) {
            rbracket = route.indexOf("}");
            period = route.indexOf(".", rbracket);
        } else {
            period = route.indexOf(".");
        }

        //
        //  Strip the operation name.
        //
        if (period > 0) {
            route = route.substring(0,period);
        }

        //
        //  See if we have a namespace.
        //
        if (route.startsWith("{"))
        {
            service = QName.valueOf(route);
        } else {
            service = new QName(namespace_, route);
        }
        return (service);
    }

    protected QName getOperationOverride(String route) {
        QName   operation;
        int     period;
        int     rbracket;

        //
        //  Find service name.
        //
        if (route.startsWith("{")) {
            rbracket = route.indexOf("}");
            period = route.indexOf(".", rbracket);
        } else {
            period = route.indexOf(".");
        }
        //
        //  Strip the operation name.
        //
        if (period > 0) {
            route = route.substring(period + 1);
        } else {
            return (null);
        }

        //
        //  See if we have a namespace.
        //
        if (route.startsWith("{"))
        {
            operation = QName.valueOf(route);
        } else {
            operation = new QName(namespace_, route);
        }
        return (operation);
    }

    
    /**
     * Copies one exchange to another.
     * @param source this is the source of the copy
     * @param target this is the target of the copy
     * @param boolean keepRoute true if any routing information should be kept.
     * @throws javax.jbi.messaging.MessagingException
     */
    protected void copy(MessageExchange source, MessageExchange target, boolean keepRoute) 
        throws javax.jbi.messaging.MessagingException {
        
        // Copy the message, operation, and properties over
        // NOTE: operation should probably come from the IFL filter definition
        // NOTE: only handling "in" message for now
        NormalizedMessage msg = source.getMessage("in");
        if (msg != null) {
            target.setMessage(msg, "in");
        }
//        msg = source.getMessage("out");
//        if (msg != null) {
//            target.setMessage(msg, "out");
//        }
//        msg = source.getFault();
//        if (msg != null) {
//            target.setFault((Fault)msg);
//        }
        target.setOperation(source.getOperation());
        for (Object prop : source.getPropertyNames()) {
            //
            // Don't propagate route properties if requested and always don't propogate
            // empty routes.
            //
            if (prop.toString().equals(Route.ROUTE_PATH)) {
                if (!keepRoute) {
                    continue;
                }
                List<String>    route = (List<String>)source.getProperty(Route.ROUTE_PATH);

                if (route == null || route.size() == 0) {
                    continue;
                }
            } else if (prop.toString().equals(Route.ROUTE_HISTORY) ||
                    prop.toString().equals("com.sun.jbi.messaging.parent")) {
                continue;
            }

            target.setProperty(prop.toString(), source.getProperty(prop.toString()));
        }
    }

    protected NormalizedMessage makeMessageCopy(MessageExchange me, NormalizedMessage nm)
            throws javax.jbi.messaging.MessagingException {
        NormalizedMessage result = me.createMessage();

        if (nm instanceof ServiceMessage) {
            result = new ServiceMessageImpl(result);
        }
        result.setContent(nm.getContent());
        for (Object prop : nm.getPropertyNames()) {
            result.setProperty((String)prop, nm.getProperty((String)prop));
        }
        for (Object a : nm.getAttachmentNames()) {
            result.addAttachment((String)a, nm.getAttachment((String)a));
        }
        return (result);
     }

    /**
     * Finds a concrete ServiceEndpoint for the given service name.
     * @param service
     * @return
     */
    protected ServiceEndpoint getEndpoint(String service) {
        ServiceEndpoint endpoint = null;
        ServiceEndpoint[] eps = deliveryChannel_.getEndpointsForService(
                getService(service));
        if (eps.length > 0) {
            endpoint = eps[0];
        }
        return endpoint;
    }
    
    /**
     * Finds a concrete ServiceEndpoint for the given service name.
     * @param service
     * @return
     */
    protected ServiceEndpoint getEndpoint(QName serviceName, String endpointName) {
        ServiceEndpoint[] eps = deliveryChannel_.getEndpointsForService(serviceName);

        for (ServiceEndpoint se: eps) {
            if (se.getEndpointName().equals(endpointName)) {
                return (se);
            }
        }
        return (null);
    }
    
    public InboundContext newInboundContext(MessageExchange me) {
        return (new InboundContext(this, me));
    }

    public class InboundContext {
        private       ArrayList<OutboundContext>      children_;
        private final MessageExchange                 exchange_;
        private final AbstractFlow                    flow_;

        InboundContext(AbstractFlow af, MessageExchange me) {
            exchange_ = me;
            flow_ = af;
        }

        public MessageExchange[] getChildren() {
            MessageExchange children[] = null;
            
            if (children_ != null) {
                children = children_.toArray(new MessageExchange[children_.size()]);
            }
            return (children);
        }

        public void addChild(OutboundContext oc) {
            if (children_ == null) {
                children_ = new ArrayList();
            }
            children_.add(oc);
        }

        public void removeChild(OutboundContext oc) {
            if (children_ != null) {
                children_.remove(oc);
            }
        }

        public boolean hasChildren() {
            boolean children = false;

            if (children_ != null) {
                children = !children_.isEmpty();
            }
            return (children);
        }

        public int getChildCount() {
            int children = 0;

            if (children_ != null) {
                children = children_.size();
            }
            return (children);
        }

        public MessageExchange getExchange() {
            return (exchange_);
        }

        public AbstractFlow getFlow() {
            return (flow_);
        }

        public String toString() {
            StringBuffer    sb = new StringBuffer();

            sb.append("        InboundContext:\n");
            sb.append("          Flow:       " + flow_.getName() + "\n");
            sb.append("          ExchangeId: " + exchange_.getExchangeId() + "\n");
            sb.append("          Children:   " +
                    ((children_ == null || children_.size() == 0) ? "(None)\n" : "\n"));
            if (children_ != null && children_.size() != 0) {
                for (OutboundContext oc : children_) {
                    sb.append("            Id: " + oc.getExchange().getExchangeId() + "\n");
                }
            }
            return (sb.toString());
        }
    }

    public OutboundContext newOutboundContext(MessageExchange me) {
        return (new OutboundContext(this, me));
    }

    public class OutboundContext {
        private final   AbstractFlow    flow_;
        private final   MessageExchange exchange_;
        private         InboundContext  inContext_;
        private         QName           outputType_;
        private         QName           inputType_;

        OutboundContext(AbstractFlow af, MessageExchange me) {
            flow_ = af;
            exchange_ = me;
            outputType_ = ServiceMessageImpl.ANYTYPE;
            inputType_ = ServiceMessageImpl.ANYTYPE;
        }

        public void setInboundContext(InboundContext ic) {
            inContext_ = ic;
        }

        public MessageExchange getExchange() {
            return (exchange_);
        }

        public AbstractFlow getFlow() {
            return (flow_);
        }

        public InboundContext getInboundContext() {
            return (inContext_);
        }

        public void setOutputType(QName type) {
            outputType_ = type;
        }

        public QName getOutputType() {
            return (outputType_);
        }

        public void setInputType(QName type) {
            inputType_ = type;
        }

        public QName getInputType() {
            return (inputType_);
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append("        Namespace:        " + namespace_ + "\n");
            sb.append("        OutboundContext:\n");
            sb.append("          Flow:          " + flow_.getName() + ")\n");
            sb.append("          InExchangeId:  " +
                    (inContext_ == null ? "(None)" : inContext_.getExchange().getExchangeId()) + "\n");
            sb.append("          ExchangeId:    " + exchange_.getExchangeId() + "\n");
            sb.append("          InputType:     " + inputType_ + "\n");
            sb.append("          OutputType:    " + outputType_ + "\n");
            return (sb.toString());
        }
    }

}
