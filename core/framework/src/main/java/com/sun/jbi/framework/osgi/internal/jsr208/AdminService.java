/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import com.sun.jbi.framework.descriptor.Component;
import com.sun.jbi.framework.osgi.internal.ComponentBundle;
import com.sun.jbi.framework.osgi.internal.ComponentManager;
import com.sun.jbi.framework.osgi.internal.Environment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.management.ObjectName;

/**
 * This class implements a set of administrative methods allowing a JMX-based
 * administrative tool to perform a variety of administrative tasks.
 * <ul>
 *   <li>Find component lifecycle MBean names for:
 *     <ul>
 *       <li>Individual components, by name</li>
 *       <li>All binding components</li>
 *       <li>All service engines</li>
 *     </ul>
 *   </li>
 *   <li>Find lifecyle MBeans names for implementation-defined services:
 *     <ul>
 *       <li>Individual implementation services, by name</li>
 *       <li>All implementation services</li>
 *     </ul>
 *   </li>
 *   <li>Query whether an individual component is a binding or engine</li>
 *   <li>Query implementation information (version etc.)</li>
 * </ul>
 *
 * @author mwhite
 */
public class AdminService implements javax.jbi.management.AdminServiceMBean {

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all binding components
     * currently installed in the JBI system.
     *
     * @return array of JMX object names of component life cycle MBeans for all
     *         installed binding components; must be non-null; may be empty.
     */
    public ObjectName[] getBindingComponents() {
        ComponentManager cm = Environment.getComponentManager();
        Collection<ComponentBundle> cbColl = cm.getComponents();
        Iterator<ComponentBundle> cbIter = cbColl.iterator();
        ArrayList<ObjectName> bindings = new ArrayList();
        while ( cbIter.hasNext() ) {
            ComponentBundle cb = cbIter.next();
            if ( cb.getComponentType().equals(Component.ComponentType.BINDING.getType()) ) {
                ComponentMBeanNames mbn = (ComponentMBeanNames)
                    cb.getComponentContext().getMBeanNames();
                bindings.add(mbn.createComponentMBeanName(
                    ComponentMBeanNames.ControlType.LifeCycle));
            }
        }
        return bindings.toArray(new ObjectName[bindings.size()]);
    }

    /**
     * Find the {@link ComponentLifeCycleMBean} of a binding component or
     * service engine by its unique name.
     *
     * @param name the name of the service engine or binding component; must
     *        be non-null and non-empty.
     * @return the JMX object name of the component's life cycle MBean, or
     *         <code>null</code> if there is no such component with the given
     *         <code>name</code>.
     */
    public ObjectName getComponentByName(String name) {
        ComponentBundle cb = Environment.getComponentManager().getComponent(name);
        if (cb != null) {
            ComponentMBeanNames mbn =
                (ComponentMBeanNames) cb.getComponentContext().getMBeanNames();
            return mbn.createComponentMBeanName(
                ComponentMBeanNames.ControlType.LifeCycle);
        }
        else {
            return null;
        }
    }

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all service engines
     * currently installed in the JBI system.
     *
     * @return array of JMX object names of component life cycle MBeans for all
     *         installed service engines; must be non-null; may be empty.
     */
    public ObjectName[] getEngineComponents() {
        ComponentManager cm = Environment.getComponentManager();
        Collection<ComponentBundle> cbColl = cm.getComponents();
        Iterator<ComponentBundle> cbIter = cbColl.iterator();
        ArrayList<ObjectName> engines = new ArrayList();
        while ( cbIter.hasNext() ) {
            ComponentBundle cb = cbIter.next();
            if ( cb.getComponentType().equals(Component.ComponentType.ENGINE.getType()) ) {
                ComponentMBeanNames mbn = (ComponentMBeanNames)
                    cb.getComponentContext().getMBeanNames();
                engines.add(mbn.createComponentMBeanName(
                    ComponentMBeanNames.ControlType.LifeCycle));
            }
        }
        return engines.toArray(new ObjectName[engines.size()]);
    }

    /**
     * Return current version and other info about this JBI implementation.
     * The contents of the returned string are implementation dependent.
     * @return information string about the implementation, including version
     *         information. Must be non-null and non-empty.
     */
    public String getSystemInfo() {
        return "Open ESB V3 for OSGi";
    }

    /**
     * Lookup a system service {@link LifeCycleMBean} by name. System services
     * are implementation-defined services which can administered through JMX,
     * and have a life cycle.
     *
     * @param serviceName name of the system service; must be non-null and non-
     *        empty; values are implementation-dependent.
     * @return JMX object name of the system service's life cycle MBean, or
     *         <code>null</code> if there is no system service with the given
     *         <code>name</code>.
     */
    public ObjectName getSystemService(String serviceName) {
        return null;
    }

    /**
     * Looks up all system service {@link LifeCycleMBean}s. System services are
     * implementation-defined services which can be administered through JMX,
     * and have a life cycle.
     *
     * @return array of JMX object names of system services' life cycle MBeans,
     *         must be non-null; may be empty.
     */
    public ObjectName[] getSystemServices() {
        ObjectName[] on = new ObjectName[0];
        return on;
    }

    /**
     * Determine whether a given JBI component is a Binding Component.
     *
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty.
     * @return <code>true</code> if the component is a binding component;
     *         <code>false</code> if the component is a service engine or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI system.
     */
    public boolean isBinding(String componentName) {
        ComponentBundle cb =
            Environment.getComponentManager().getComponent(componentName);
        if (cb != null) {
            return cb.getComponentType().equals(
                Component.ComponentType.BINDING.getType());
        }
        else {
            return false;
        }
    }

    /**
     * Determine whether a given JBI component is a Service Engine.
     *
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty.
     * @return <code>true</code> if the component is a service engine;
     *         <code>false</code> if the component is a binding component, or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI system.
     */
    public boolean isEngine(String componentName) {
        ComponentBundle cb =
            Environment.getComponentManager().getComponent(componentName);
        if (cb != null) {
            return cb.getComponentType().equals(
                Component.ComponentType.ENGINE.getType());
        }
        else {
            return false;
        }
    }
    
}
