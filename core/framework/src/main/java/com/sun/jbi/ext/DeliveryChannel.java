/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeliveryChannel.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ext;

import com.sun.jbi.messaging.events.AbstractEvent;

import java.net.URI;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * This is the interface for extensions to the JSR 208 MessageExchange interface.
 */
public interface DeliveryChannel      
{
    /**
     * This is used to register a EndpointListener callback.
     */
    void addTimeoutListener(TimeoutListener tl);
    
    /** 
     * This is used to activate any of the family of Endpoints.
     *
     * @param pattern - the Exchange pattern to create
     * @param id - the id that should be used.
     */
    MessageExchange createExchangeWithId(URI pattern, String id)
        throws javax.jbi.messaging.MessagingException;
    
    /** 
     * This is used to create a temporary endpoint.
     * @param service - the service name to create
     * @param endpoint - the endpoint name to create
     */
    public ServiceEndpoint createEndpoint(QName service, String endpoint)
        throws javax.jbi.messaging.MessagingException;
    
    /**
     * This is used to handle any Endpoint event actions.
     * 
     * @param ae - The event describing the endpoint action.
     */
    void handleEndpointEvent(AbstractEvent ae)
            throws javax.jbi.messaging.MessagingException;
    
    /** 
     * This operation is used to get the active set of Endpoints.
     *
     * @return An array of activated Endpoints.
     */
    AbstractEvent[]     getAllEndpointEvents();
    
    /** 
     * This is used check is a ME is still active in the NMR.
     *
     * @return  true - The MessageExchange is still active.
     *           false - The MessageExchange is not active.
     */
    boolean isActive(javax.jbi.messaging.MessageExchange me);
    
    /** 
     * This is used to assist in routing handshakes.
     *
     * @return  true - This MessageExchange's route is okay.
     *           false - This MessageExchange's is not okay.
     */
    boolean isExchangeOkay(javax.jbi.messaging.MessageExchange me);
    
    Object enableAcceptNotification(SubChannel o);

    public static final int EVENT_SUSPEND = 1;
    public static final int EVENT_RESUME = 2;
    public static final int EVENT_SIGNAL = 3;
}
