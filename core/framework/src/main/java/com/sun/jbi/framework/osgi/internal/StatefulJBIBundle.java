/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StatefulJBIBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;
import org.osgi.framework.Bundle;

/**
 * Interface for a stateful JBI Bundle.  Defines the operations to start/stop
 * a JBI Bundle and retrieve it's state.
 * 
 * @author nikki
 */
public abstract class StatefulJBIBundle extends JBIBundle {
    
    /** The state of the entity */
    private JBIState state_;
       
    protected StatefulJBIBundle(Jbi descriptor, Bundle bundle) {
        super(descriptor, bundle);
        state_ = JBIState.SHUTDOWN;
    }
    
    /**
     * Start the JBI entity. 
     * 
     * @exception Exception on failure to start the bundle.
     */
    abstract void start() throws Exception;
    
    /**
     * Stop the JBI entity.
     * 
     * @exception Exception on failure to stop the bunle.
     */
    abstract void stop() throws Exception;
    
    /**
     * Shutdown the JBI entity.
     * 
     * @exception Exception on failure to start the bundle.
     */
    abstract void shutDown() throws Exception;
    
    /**
     * @return the state of the JBI Entity
     */
    public JBIState getState()
    {
        return state_;
    }
    
    /**
     * @param state the JBIState to set
     */
    public void setState(JBIState state)
    {
        state_ = state;
    }

}
