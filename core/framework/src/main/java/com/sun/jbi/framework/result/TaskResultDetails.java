/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TaskResultDetails.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * 
 * @author kcbabo
 */
public class TaskResultDetails extends ResultElement {
    
//    public static final String ELEMENT_NAME =
//            "task-result-details";
//    
//    private static final String TASK_ID = 
//            "task-id";
//    
//    private static final String TASK_RESULT = 
//            "task-result";
//    
//    private static final String MESSAGE_TYPE = 
//            "message-type";
//    
//    private static final String TASK_STATUS_MSG = 
//            "task-status-msg";
    
    public enum TaskResult { SUCCESS, FAILED };
    
    public enum MessageType { ERROR, WARNING, INFO };
    
    public enum TaskId { deploy, init, start, stop, shutDown, undeploy }


    public TaskResultDetails(Element element) {
        super(element);
    }
    
    public String getTaskId() {
        return getChildElement(TaskElem.task_id).getTextContent();
    }
    
    public TaskResult getTaskResult() {
        String tr = getChildElement(TaskElem.task_result).getTextContent(); 
        return tr != null ? TaskResult.valueOf(tr) : null; 
    }
    
    public MessageType getMessageType() {
        Element elem = getChildElement(TaskElem.message_type);
        
        if (elem != null && elem.getTextContent() != null) {
            try {
                return MessageType.valueOf(elem.getTextContent());
            }
            catch (Exception e) {
                // ignore, component too lazy too provide valid message type
            }
        }

        return MessageType.INFO;
    }
    
    public List<LocalizedMessage> getTaskStatusMsgs() {
        
        List<LocalizedMessage> statusMsgs = new ArrayList<LocalizedMessage>();
        List<Element> eleList = getChildElements(TaskElem.task_status_msg);
        
        for (Element ele : eleList) {
            NodeList nl = ele.getElementsByTagName(
                    TaskElem.msg_loc_info.toString());
            if (nl.getLength() == 1) {
                statusMsgs.add(new LocalizedMessage(ele));
            }
        }
        
        return statusMsgs;
    }
    
    public List<ExceptionInfo> getExceptionInfos() {
        
        List<ExceptionInfo> exInfos = new ArrayList<ExceptionInfo>();
        List<Element> eleList = getChildElements(TaskElem.exception_info);
        
        for (Element ele : eleList) {
            exInfos.add(new ExceptionInfo(ele));
        }
        
        return exInfos;
    }
    
    public void setTaskId(String taskId) {
        getChildElement(TaskElem.task_id).setTextContent(taskId); 
    }
    
    public void setTaskResult(TaskResult taskResult) {
        getChildElement(TaskElem.task_result).setTextContent(taskResult.toString()); 
    }
    
    public void setMessageType(MessageType messageType) {
        if (getChildElement(TaskElem.message_type) == null) {
            if (getChildElement(TaskElem.task_status_msg) != null) {
                createChildElementBefore(TaskElem.message_type, TaskElem.task_status_msg);
            }
            else {
                createChildElementBefore(TaskElem.message_type, TaskElem.exception_info);
            }
            
        }
        getChildElement(TaskElem.message_type).setTextContent(messageType.toString());
    }
    
    public ExceptionInfo addExceptionInfo() {
        return new ExceptionInfo(createChildElement(TaskElem.exception_info));
    }
    
    public LocalizedMessage addTaskStatusMsg() {
        Element taskStatusEle = createChildElementBefore(
                TaskElem.task_status_msg, TaskElem.exception_info);
        Element locMsgEle = taskStatusEle.getOwnerDocument().
                createElement(TaskElem.msg_loc_info.toString());
        taskStatusEle.appendChild(locMsgEle);
        return new LocalizedMessage(locMsgEle);
    }

    public LocalizedMessage addTaskStatusMsg(String msg, String tkn, Object... params) {
        LocalizedMessage loc = addTaskStatusMsg();
        loc.setMessage(msg);
        loc.setToken(tkn);
        if (params != null) {
            for (Object param : params) {
                loc.addParameter(String.valueOf(param));
            }
        }
        
        return loc;
    }

    @Override
    protected void initialize() {
        createChildElement(TaskElem.task_id);
        createChildElement(TaskElem.task_result);
    }

}
