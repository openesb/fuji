/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import com.sun.jbi.framework.descriptor.ServiceUnit;
import com.sun.jbi.framework.osgi.internal.ComponentBundle;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyBundle;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyManager;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.Bundle;


/**
 * This class implements the JSR208 DeploymentServiceMBean. This MBean allows
 * administrative tools to manage service assembly deployments. The tasks
 * supported are:
 * <ul><li>Deploying a service assembly.</li>
 *     <li>Undeploying a previously deployed service assembly.</li>
 *     <li>Querying deployed service assemblies:
 *     <ul><li>For all components in the system.</li>
 *         <li>For a particular component.</li>
 *     </ul>
 *     <li>Control the state of deployed service assemblies:
 *     <ul><li>Start the service units contained in the SA.</li>
 *         <li>Stop the service units contained in the SA.</li>
 *         <li>Shut down the service units contained in the SA.</li>
 *     </ul>
 *     <li>Query the service units deployed to a particular component.</li>
 *     <li>Check if a service unit is deployed to a particular component.</li>
 *     <li>Query the deployment descriptor for a particular service assembly.</li>
 * </ul>
 * @author kcbabo
 * @author mwhite
 */
public class DeploymentService 
        implements com.sun.jbi.management.DeploymentServiceMBean {
    /**
     * Local reference to the runtime Service Assembly manager.
     */
    private ServiceAssemblyManager saManager_;

    /**
     * Logger for this class.
     */
    private Logger logger_ = Logger.getLogger(
            DeploymentService.class.getPackage().getName());

    /**
     * Constructor to get the runtime Service Assembly manager reference.
     */
    public DeploymentService() {
        saManager_ = Environment.getServiceAssemblyManager();
    }

    /**
     * Returns true if the the given component accepts the deployment of service
     * units. This is used by admin tools to determine which components can be
     * named in service assembly deployment descriptors.
     *
     * @param componentName name of the component; must be non-null and
     * non-empty
     * @return true if the named component accepts deployments; false if the
     * named component does not accept deployments or it does not exist.
     */
    public boolean canDeployToComponent(String componentName) {
        boolean canDeploy = false;

        try {
            ComponentBundle comp = Environment.getComponentManager().
                    getComponent(componentName);
            canDeploy = comp.getComponent().getServiceUnitManager() != null;
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Failed to query component's SUM", ex);
        }
        
        return canDeploy;
    }

    /**
     * Deploys the given Service Assembly to the JBI environment.
     *
     * @param serviceAssemblyZipURL String containing the location URL of the
     * Service Assembly ZIP file; must be non-null, non-empty, and a legal URL.
     * @return result/status of the current deployment; must conform to JBI
     * management result/status XML schema; must be non-null and non-empty.
     * @throws Exception if the complete deployment fails.
     */
    public String deploy(String serviceAssemblyZipURL) throws Exception {
        Environment.getFrameworkBundleContext().installBundle(serviceAssemblyZipURL);
        return "SUCCESS";
    }

    public String[] getComponentsForDeployedServiceAssembly(String serviceAssemblyName) 
            throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        ServiceAssemblyBundle sa = saManager_.getServiceAssembly(serviceAssemblyName);
        for (ServiceUnit su : 
             sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
            list.add(su.getIdentification().getName());
         }
        
        return list.toArray(new String[] {});
    }

    public String[] getDeployedServiceAssemblies() throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        
        for (ServiceAssemblyBundle sa : 
             saManager_.getServiceAssemblies()) {
            list.add(sa.getName());
        }
             
        return list.toArray(new String[] {});
    }

    public String[] getDeployedServiceAssembliesForComponent(String componentName)
            throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        
        for (ServiceAssemblyBundle sa : saManager_.getServiceAssemblies()) {
            for (ServiceUnit su : 
                 sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
                if (su.getComponentName().equals(componentName)) {
                    list.add(sa.getName());
                    break;
                }
             }
        }
             
        return list.toArray(new String[] {});
    }

    public String[] getDeployedServiceUnitList(String componentName)
            throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        
        for (ServiceAssemblyBundle sa : saManager_.getServiceAssemblies()) {
            for (ServiceUnit su : 
                 sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
                if (su.getComponentName().equals(componentName)) {
                    list.add(su.getIdentification().getName());
                }
             }
        }
             
        return list.toArray(new String[] {});
    }

    public String getServiceAssemblyDescriptor(String serviceAssemblyName)
            throws Exception {
        return saManager_.getServiceAssembly(
                serviceAssemblyName).getDescriptor().asString();
    }

    public String getState(String serviceAssemblyName) throws Exception {
        int bundleState = saManager_.getServiceAssembly(
                serviceAssemblyName).getBundle().getState();
        
        if (bundleState == Bundle.ACTIVE) {
            return STARTED;
        }
        else {
            return SHUTDOWN;
        }
    }

    public boolean isDeployedServiceUnit(String componentName, String serviceUnitName)
            throws Exception {
        boolean isDeployed = false;
        
        for (ServiceAssemblyBundle sa : saManager_.getServiceAssemblies()) {
            for (ServiceUnit su : 
                 sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
                if (su.getComponentName().equals(componentName) &&
                    su.getIdentification().getName().equals(serviceUnitName)) {
                    isDeployed = true;
                }
             }
        }
        
        return isDeployed;
    }

    public String shutDown(String serviceAssemblyName) throws Exception {
        ServiceAssemblyBundle sa = saManager_.getServiceAssembly(serviceAssemblyName);
        Environment.getFrameworkBundleContext().getBundle(sa.getBundle().getBundleId()).stop();
        return "SUCCESS";
    }

    public String start(String serviceAssemblyName) throws Exception {
        ServiceAssemblyBundle sa = saManager_.getServiceAssembly(serviceAssemblyName);
        Environment.getFrameworkBundleContext().getBundle(sa.getBundle().getBundleId()).start();
        return "SUCCESS";
    }

    public String stop(String serviceAssemblyName) throws Exception {
        ServiceAssemblyBundle sa = saManager_.getServiceAssembly(serviceAssemblyName);
        Environment.getFrameworkBundleContext().getBundle(sa.getBundle().getBundleId()).stop();
        return "SUCCESS";
    }

    public String undeploy(String serviceAssemblyName) throws Exception {
        ServiceAssemblyBundle sa = saManager_.getServiceAssembly(serviceAssemblyName);
        Environment.getFrameworkBundleContext().getBundle(sa.getBundle().getBundleId()).uninstall();
        return "SUCCESS";
    }

    /**
     * Get the Service Assembly name for a specific service unit deployed to a
     * specific component.
     *
     * @param serviceUnitName - service unit identifier
     * @param componentName - component identifier
     */
    public String getServiceAssemblyName(String serviceUnitName, String componentName) throws Exception
     {
        String serviceAssemblyName = null;
        for (ServiceAssemblyBundle sa : saManager_.getServiceAssemblies()) {
            for (ServiceUnit su : 
                 sa.getDescriptor().getServiceAssembly().getServiceUnits()) {
                if (su.getIdentification().getName().equals(serviceUnitName)) {
                    if (su.getComponentName().equals(componentName)) {
                        serviceAssemblyName = sa.getName();
                        break;
                    }
                }
             }
        }
        return serviceAssemblyName;
     }
}
