/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceMediator.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.service.ServiceMessageImpl;
import org.glassfish.openesb.api.service.ServiceProvider;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.servicedesc.ServiceEndpoint;

/**
 * Handles communication between the JBI-facing DeliveryChannel contract and 
 * our ServiceProvider contract.
 * @author kcbabo
 */
public class ServiceMediator {
    
    // Keeping it single-threaded for now, although this value should be exposed
    // as a user configuration option (probably a property of the service
    // registration.
    private static final int NUM_THREADS = 1;
    private ServiceEndpoint jbiService_;
    private ServiceProvider osgiService_;
    private ExecutorService threadPool_;
    private DeliveryChannel channel_;
    
    private Logger logger_ = Logger.getLogger(
            ServiceMediator.class.getPackage().getName());
    
    public ServiceMediator(DeliveryChannel channel, 
            ServiceEndpoint jbiService, ServiceProvider osgiService) {
        channel_     = channel;
        jbiService_  = jbiService;
        osgiService_ = osgiService;
        threadPool_  = Executors.newFixedThreadPool(NUM_THREADS);
    }
    
    public void start() {
        threadPool_.execute(new ExchangeHandler());
    }
    
    public void stop() {
        threadPool_.shutdownNow();
    }
    
    /**
     * Returns the ServiceProvider instance corresponding to the OSGi service
     * that this mediator invokes.
     * @return OSGi-based ServiceProvider
     */
    public ServiceProvider getOSGiService() {
        return osgiService_;
    }
    
    /**
     * Returns the JBI endpoint used by this mediator.
     * @return JBI service endpoint
     */
    public ServiceEndpoint getJBIService() {
        return jbiService_;
    }
    
    
    /**
     * ExchangeHandler is a little worker thread that maps message exchanges
     * received from a delivery channel into the ServiceProvider contract.
     */
    class ExchangeHandler implements Runnable {

        public void run() {
            
            while (true) {
                try {
                    // wait for an exchange to arrive in our channel
                    MessageExchange me = channel_.accept();
                    
                    if (me == null) {
                        // has been known to happen in certain situations
                        continue;
                    }
                    else if (me.getStatus() == ExchangeStatus.ACTIVE) {
                        processExchange(me);
                    }
                    else if (me.getStatus() == ExchangeStatus.ERROR) {
                        // make a note in the log
                        logger_.log(Level.WARNING,
                                "Exchange " + me.getExchangeId() + "failed with error:",
                                me.getError());
                    }
                    else if (me.getStatus() == ExchangeStatus.DONE) {
                        // nothing much to do here
                        continue;
                    }
                }
                catch (javax.jbi.messaging.MessagingException msgEx) {
                    if (msgEx.getCause() instanceof InterruptedException) {
                        // no need to complain - this is what happens when
                        // the channel is closed during shutdown
                    }
                    else {
                        logger_.log(Level.WARNING, 
                            "Error while accepting for service " + 
                            jbiService_.getServiceName(), msgEx);
                    }
                    
                    // Stop trying to accept
                    break;
                }
            }
        }
        
        /**
         * Feeds the input message to our ServiceProvider and processes the 
         * out message, if one exists.
         * @param me message exchange received by service mediator
         */
        void processExchange(MessageExchange me) {
            
            ServiceMessage inMsg;
            ServiceMessage outMsg;
            
            try {
                inMsg = new ServiceMessageImpl(me.getMessage("in"));
                outMsg = osgiService_.invoke(inMsg);
                
                if (me instanceof InOnly || me instanceof RobustInOnly) {
                    me.setStatus(ExchangeStatus.DONE);
                }
                else {
                    me.setMessage(outMsg, "out");
                }
                
                channel_.send(me);
            }
            catch (Exception ex) {
                logger_.log(Level.WARNING,
                        "Error while invoking service " + 
                        jbiService_.getServiceName(), ex);
            }
        }
    }
}

