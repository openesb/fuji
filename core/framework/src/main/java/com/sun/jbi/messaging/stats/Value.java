/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Value.java
 * Copyright 2004-2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging.stats;


/** Simple value sampler that computes basic statistics.
 * @author Sun Microsystems, Inc.
 */
public class Value
{
    public enum Kind { COUNT, BYTES, INTERVAL_NS, INTERVAL_MS };
    private double  sum;
    private double  squaredsum;
    private long    min;
    private long    max;
    private long    count;
    private int     scale;
    private int     bias;
    private char    unit;

    public Value(Kind kind)
    {
        if (kind == Kind.COUNT) {
            scale = 1000;
            bias = 5;
            unit = ' ';
        } else if (kind == Kind.INTERVAL_NS) {
            scale = 1000;
            bias = 2;
            unit = 's';
        } else if (kind == Kind.BYTES) {
            scale = 1024;
            bias = 5;
            unit = 'B';
        } else if (kind == Kind.INTERVAL_MS) {
            scale = 1000;
            bias = 4;
            unit = 's';
        }
    }
    
    public void zero()
    {
        sum = 0.0;
        squaredsum = 0.0;
        min = 0;
        max = 0;
        count = 0;
    }
    
    public void addSample(long sample)
    {
        if (sample < min || count == 0)
        {
            min = sample;
        }
        if (sample > max || count == 0)
        {
            max = sample;
        }
        count++;
        sum += sample;
        squaredsum += ((double)sample)*((double)sample);
    }
    
    public double getAverage()
    {
        return (sum / count);
    }
    
    public long getMin()
    {
        return (min);
    }
    
    public long getMax()
    {
        return (max);
    }
    
    public long getCount()
    {
        return (count);
    }
    
    public double getSd()
    {
        return (Math.sqrt((1.0 / (count - 1)) * (squaredsum - ((sum * sum) / count))));
    }
    
    public String getAverageNice()
    {
        return (niceNum(getAverage(), scale, bias, unit));
    }

    public String getMinNice()
    {
        return (niceNum(min, scale, bias, unit));
    }

    public String getMaxNice()
    {
        return (niceNum(max, scale, bias, unit));
    }

    public String getSdNice()
    {
        return (niceNum(getSd(), scale, bias, unit));
    }

    public String toString()
    {
        StringBuilder        sb = new StringBuilder();
        
        sb.append("Count("+count+")");
        sb.append("Min("+niceNum(min, scale, bias, unit)+")");
        sb.append("Avg("+niceNum(getAverage(), scale, bias, unit)+")");
        sb.append("Max("+niceNum(max, scale, bias, unit)+")");
        sb.append("Std("+niceNum(getSd(), scale, bias, unit)+")");
        
        return (sb.toString());
    }

    private String niceNum(long number, int scale, int bias, char unit)
    {
        String  letter = "fpnum KMGTE";
        String  result;
        int     prefix = 0;
        long    mag = 1;
        long    n = number;

        while (n > scale) {
            n /= scale;
            mag *= scale;
            prefix++;
        }
        if (prefix == 0) {
            result = String.format("%d%c", number, unit);
        } else if ((number % mag) == 0) {
            result = String.format("%d%c%c", n, letter.charAt(prefix + bias), unit);
        } else {
            result = String.format("%.2f%c%c", (double)number / mag, letter.charAt(prefix + bias), unit);
            if (result.length() > 6) {
                result = String.format("%.1f%c%c", (double)number / mag, letter.charAt(prefix + bias), unit);
                if (result.length() > 6 ) {
                    result = String.format("%.0f%c%c", (double)number / mag, letter.charAt(prefix + bias), unit);
                }
            }
        }
        return (result);
    }

    private String niceNum(double number, int scale, int bias, char unit)
    {
        String  letter = "fpnum KMGTE";
        String  result;
        int     prefix = 0;
        long    mag = 1;
        double  n = number;

        while (n > scale) {
            n /= scale;
            mag *= scale;
            prefix++;
        }
        while (n < 0) {
            n *= scale;
            mag /= scale;
            prefix--;
        }
        {
            result = String.format("%.2f%c%c", (double)number / mag, letter.charAt(prefix + bias), unit);
            if (result.length() > 6 ) {
                result = String.format("%.1f%c%c", (double)number / mag, letter.charAt(prefix + bias), unit);
                if (result.length() > 6) {
                    result = String.format("%.0f%c%c", (double)number / mag, letter.charAt(prefix + bias), unit);
                }
            }
        }
        return (result);
    }

}

    
