/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Installer.java - Last published on 4/15/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import com.sun.jbi.framework.InstallationContext;
import com.sun.jbi.framework.osgi.internal.ComponentBundle;

import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;
import javax.jbi.management.InstallerMBean;
import javax.management.ObjectName;

import org.osgi.framework.Bundle;

/**
 * This class implements the InstallerMBean for a component. This allows a
 * JMX client to control the installation or uninstallation of the component.
 *
 * Note that in the current implementation, this MBean cannot actually be used
 * to install a component because of the way the framework handles a bundle
 * installation. But this MBean can be used to uninstall a component.
 *
 * @author mwhite
 */
public class Installer implements InstallerMBean {
    /**
     * Component bundle.
     */
    private ComponentBundle component_;

    /**
     * Logger for this class.
     */
    private Logger logger_ = Logger.getLogger(
            Installer.class.getPackage().getName());

    /**
     * Constructor.
     *
     * @param component the ComponentBundle representing the component.
     */
    Installer(ComponentBundle component)
                      
    {
        component_ = component;
    }

    /**
     * Get the installer configuration MBean <code>ObjectName</code> for this
     * component.
     *
     * Note that this MBean is useless in the current implementation because
     * its purpose is to provide pre-install configuration. Because the
     * component has already been installed by the time this method can be
     * called, calling the MBean returned by this method has no effect.
     *
     * @return the JMX object name of the Installer Configuration MBean.
     * @throws javax.jbi.JBIException if any errors occur.
     */
    public ObjectName getInstallerConfigurationMBean()
        throws javax.jbi.JBIException
    {
        try{
            Bootstrap installer = component_.getBootstrap();
            return installer.getExtensionMBeanName();
        } catch (Throwable ex){
            throw new javax.jbi.JBIException(ex.getMessage(), ex);
        }
    }

    /**
     * Get the installation root directory path for this component.
     *
     * @return the full installation path of this component.
     */
    public String getInstallRoot()
    {
        InstallationContext installContext =
            new InstallationContext(component_, true);
        return installContext.getInstallRoot();
    }

    /**
     * Install this component.
     *
     * Note that in the current implementation, by the time this method is
     * called, the component has already been installed. All this method does
     * is return the component's LifeCycle MBean <code>ObjectName</code>.
     *
     * @return JMX ObjectName representing the ComponentLifeCycleMBean for this
     * component.
     * @throws javax.jbi.JBIException if the installation fails.
     */
    public ObjectName install()
        throws javax.jbi.JBIException
    {
        ComponentMBeanNames mbn = (ComponentMBeanNames)
            component_.getComponentContext().getMBeanNames();
        return mbn.createComponentMBeanName(
            ComponentMBeanNames.ControlType.LifeCycle);
            
    }

    /**
     * Determine whether or not this component is installed.
     *
     * @return true if this component is currently installed, false if not.
     */
    public boolean isInstalled()
    {
        return (component_.getBundle().getState() != Bundle.UNINSTALLED);
    }

    /**
     * Uninstall this component. This completely removes the component from the
     * JBI system.
     *
     * @throws javax.jbi.JBIException if the uninstallation fails.
     */
    public void uninstall()
        throws javax.jbi.JBIException
    {
        try {
            component_.getBundle().uninstall();
        }
        catch (org.osgi.framework.BundleException bEx) {
            throw new javax.jbi.JBIException(
                "Failed to uninstall component", bEx);
        }
    }
}
