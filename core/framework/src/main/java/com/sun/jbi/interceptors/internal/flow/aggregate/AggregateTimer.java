/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Aggregate.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.aggregate;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Stateful timer for aggregate. State = correlationId, timeout interval in ms
 * 
 * @author Sun Microsystems, Inc.
 */
public class AggregateTimer  
    extends TimerTask{
            
    private Timer     timer_;
    private Object    correlationID_;
    private long      timeout_;
    private Logger    log_ = Logger.getLogger("com.sun.jbi.interceptors");
    private AggregateController aggregateController_;
  
    
    /**
     * Creates a timer with self as the timer task
     * 
     * @param correlationID
     * @param timeout
     */
    public AggregateTimer(Object correlationID, long timeout, AggregateController controller){
        
        aggregateController_ = controller;
        timeout_ = timeout;
        
        correlationID_ = correlationID;
        if ( correlationID_ == null ){
            log_.warning("Cannot create a timer if the correlation id is NULL.");
            return;
        }
        
        String timerName = correlationID.toString();
        
        timer_ = new Timer(timerName);
        log_.finest("Scheduling the aggregating timer for " + timeout_ + " ms");
        timer_.schedule( this, timeout_);
    };

     
    /**
     * @return the correlationId
     */
    public Object getCorrelationId(){
        return correlationID_;
    }
    
    /**
     * @return timeout interval
     */
    public long getTimeout(){
        return timeout_;
    }
    
    /**
     * purge the timer
     */
    public void purge()
    {
        timer_.purge();
    }
    
    /**
     * TimerTask for the timeout. When this task is scheduled it notifies the 
     * aggregate controller.
     * 
     */
    public void run(){
       try
       {
           log_.finest("Aggregating timer for " + timeout_ + " ms expired");

           // Notify the aggregate manager instance that the timeout expired
           aggregateController_.timedOut(this);

       } catch(Exception ex )
       {
               log_.log(Level.FINEST, "Failed to fire aggregated exchange after timer expired ", ex);
       }
    }
}
