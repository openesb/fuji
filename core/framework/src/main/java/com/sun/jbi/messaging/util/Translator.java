/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Translator.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import com.sun.jbi.StringTranslator;

/** Utility class used to handle static string translation for NMR log and 
 *  exception messages.
 * @author Sun Microsystems, Inc.
 */
public final class Translator
{
    private static StringTranslator sTranslator;
    
    public static void setStringTranslator(StringTranslator translator)
    {
        sTranslator = translator;
    }
    
    public static String translate(String id, Object[] parms)
    {
        String str;
        
        if (sTranslator == null)
        {
            return id;
        }
        
        if (parms != null && parms.length > 0)
        {
            str = sTranslator.getString(id, parms);
        }
        else
        {
            str = sTranslator.getString(id);
        }
        return str;
    }
    
    public static String translate(String id)
    {
        return translate(id, null);
    }
    
    public static String translate(String id, Object p1)
    {
        return translate(id, new Object[] { p1 });
    }

    public static String translate(String id, Object p1, Object p2)
    {
        return translate(id, new Object[] { p1, p2 });
    }

    public static String translate(String id, Object p1, Object p2, Object p3)
    {
        return translate(id, new Object[] { p1, p2, p3 });
    }
}
