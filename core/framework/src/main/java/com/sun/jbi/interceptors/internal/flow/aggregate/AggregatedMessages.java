/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AggregatedMessages.java
 *
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.aggregate;

import com.sun.jbi.interceptors.internal.MessageExchangeUtil;
import com.sun.jbi.interceptors.internal.flow.AbstractFlow;
import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.message.MessageProps;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * Container for aggregated messages for a particular correlation id.
 * 
 * @author Sun Microsystems, Inc.
 */
public class AggregatedMessages {
            
    private Logger               log_ = Logger.getLogger("com.sun.jbi.interceptors");
    
    private Object                  correlationID_;
    private List<ServiceMessage>    messages_;
    private HashMap                 messageProperties_;
    private MessageExchange         aggregatedExchange_;
    
    /**
     * Creates an instance of this class
     * 
     * @param correlationID
     * @param timeout
     */
    public AggregatedMessages(MessageExchangeFactory mef, MessageExchange exchange, Object correlationID){        
        correlationID_    = correlationID;
        aggregatedExchange_ = exchange;
    };

     
    /**
     * Add a message to the set of aggregated messages. The current active 
     * message from the exchange is added. The aggregate exchange is also updated
     * with properties from the exchange.
     * 
     * @return the correlationId
     */
    public void addMessageFromExchange(MessageExchange exchange)
        throws Exception {
        
        // Update the aggregateExchange_
        updateExchange(exchange);
        cacheMessage(exchange);
    }
    
    /**
     * @return timeout interval
     */
    public ServiceMessage[] getMessages(){
        ServiceMessage[] msgs = new ServiceMessage[getMessagesList().size()];
        
        return getMessagesList().toArray(msgs);
    }

    /**
     * @return the aggregated message properties, these are properties which 
     *         will be set on the eventual aggregated message
     * 
     */
    public HashMap getAggregatedMessageProperties()
    {
        if ( messageProperties_ == null )
        {
            messageProperties_ = new HashMap();
        } 
        return messageProperties_;
    }
    
    /** 
     * @return the aggregated exchange 
     */
    public MessageExchange getAggregatedExchange()
    {
        return aggregatedExchange_;
    }
    
    /**
     * @return the aggregated exchange
     */
    public void setAggregatedExchange(MessageExchange exchange)
    {
        aggregatedExchange_ = exchange;
    }

    /**
     * Any cleanup to be done with the active messages
     */
    public void purge()
    {
        aggregatedExchange_ = null; 
        messageProperties_ = null;
        messages_ = null;
    }
    
     /*-----------------------------------------------------------------------*\
                                Private Stuff 
     \*-----------------------------------------------------------------------*/
    
    /**
     * Keep the aggregated exchange up to date based on the source exchanges
     * 
     * @param source - source InOnlyExchange
     */
    void updateExchange(MessageExchange exchange)
        throws Exception
    {
        for (Object p : exchange.getPropertyNames()) {
            String  prop = (String)p;
            if (prop.equals("com.sun.jbi.messaging.groupid")) {
                String  newGroup = (String)exchange.getProperty(prop);
                String  existingGroup = (String)aggregatedExchange_.getProperty(prop);
                
                //
                //  Make sure that the resulting group id is deterministic. In this
                //  case we just pick the minimum value.
                //
                if (existingGroup == null || existingGroup.compareTo(newGroup) > 0) {
                    aggregatedExchange_.setProperty(prop, newGroup);
                }
            } else {
                aggregatedExchange_.setProperty((String)prop, exchange.getProperty(prop.toString()));
            }
        }        
    }
    
    /**
     * Cache the message from the exchange
     * 
     * @param source - the source MessageExchange
     * 
     */
    void cacheMessage(MessageExchange exchange)
        throws Exception
    {
        log_.finest("Aggregate caching message in exchange ..." + exchange.toString()); 
        
        ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
        NormalizedMessage  nm = (NormalizedMessage)svcMsg;
        HashMap            map = getAggregatedMessageProperties();

        // Set the creation TS from Exchange Id
        int start = exchange.getExchangeId().lastIndexOf("-");
        String ts = exchange.getExchangeId().substring(start + 1);
        svcMsg.setProperty(MessageProps.MESSAGE_TS.toString(), ts);
        
        addMessage(svcMsg);
        
        java.util.Set<String> names = nm.getPropertyNames();
        for ( String name : names )
        {
            if (name.equals(MessageProps.MESSAGE_ID.toString())) {
                String  newGroup = (String)nm.getProperty(name);
                String  existingGroup = (String)map.get(name);

                //
                //  Make sure that the resulting message id is deterministic. In this
                //  case we just pick the minimum value.
                //
                if (existingGroup == null || existingGroup.compareTo(newGroup) > 0) {
                    map.put(name, newGroup);
                }
            } else {
                map.put(name, nm.getProperty(name));
            }
        }
    }
    
     /*-----------------------------------------------------------------------*\
                                Message Management 
     \*-----------------------------------------------------------------------*/
     
        /**
     * @return timeout interval
     */
    public List<ServiceMessage> getMessagesList(){
        if ( messages_ == null )
        {
            messages_ = new ArrayList();
        }
        return messages_;
    }
    
     /**
      * Add a new message to the list
      * 
      * @param messages - Service Message instance to add
      */
     private void addMessage(ServiceMessage message) {
         getMessagesList().add(message);
     }

}
