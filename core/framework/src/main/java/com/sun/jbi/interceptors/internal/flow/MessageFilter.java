/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageFilter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import javax.jbi.messaging.MessageExchange;
import org.w3c.dom.Document;

/**
 * Interface for a message filter
 * 
 * @author Sun Microsystems, Inc.
 */
public interface MessageFilter { 
    
    /**
     * @return true if the current active message in the exchange matches the 
     *         filter. 
     * 
     * @param exchange - exchange to filter
     */
    boolean matches(MessageExchange exchange)
        throws Exception;
    
    /**
     * @return true the given object can be matched
     *
     * @param various - document to filter
     */
    boolean matches(Object various)
        throws Exception;

    /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     String execute(MessageExchange exchange)
        throws Exception;
    
    /**
     * Apply the filter to the given object
     * @param various - object to filter
     * @return the result of executing the filter
     */
     String execute(Object various)
        throws Exception;

}
