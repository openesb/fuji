/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicEndpoint.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.DocumentFragment;

/** Represents a dynamic endpoint supplied by a component in response to a 
 *  call to resolveEndpointReference().  Dynamic endpoints are not registered
 *  with the NMR, they are merely used to route exchanges using an endpoint
 *  reference.
 * @author Sun Microsystems, Inc.
 */
public class DynamicEndpoint extends RegisteredEndpoint
{
    private DocumentFragment mReference;
    private ServiceEndpoint mDelegate;
    
    /** Creates a new DynamicEndpoint.
     *  @param service service name
     *  @param endpoint endpoint name
     *  @param ownerId id of component which is registering the service
     *  @param reference endpoint reference for this endpoint
     *  @param type endpoint type
     */
    public DynamicEndpoint(ServiceEndpoint endpoint,
        String ownerId, DocumentFragment reference)
    {
        super(endpoint.getServiceName(), endpoint.getEndpointName(), ownerId);        
        mReference = reference;
        mDelegate = endpoint;
    }
    
    public int getType() 
    {
        return DYNAMIC;
    }
    
    public DocumentFragment getAsReference(QName operationName)
    {
        return mReference;
    }
    
    public ServiceEndpoint getDelegate()
    {
        return (mDelegate);
    }
    
    public String toString()
    {
        StringBuilder   sb = new StringBuilder();
        
        sb.append("      Endpoint Type: Dynamic(Delegate=");
        sb.append(mDelegate == null ? "Null" : mDelegate.getClass().getCanonicalName());
        sb.append(")\n");
        sb.append(super.toString());
        return (sb.toString());
    }

}
