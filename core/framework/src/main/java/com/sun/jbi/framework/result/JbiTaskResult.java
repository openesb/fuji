/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiTaskResult.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;

/**
 * Object model for <code>jbi-task-result</code> element in a JBI 
 * management result message.
 * @author kcbabo
 */
public class JbiTaskResult extends ResultElement {
    
    public static final String ELEMENT_NAME =
            "jbi-task-result";
    
    public JbiTaskResult(Element element) {
        super(element);
    }
    
    public ComponentTaskResult addComponentTaskResult() {
        return new ComponentTaskResult(createChildElement(TaskElem.component_task_result));
    }
    
    public void addComponentTaskResult(ComponentTaskResult result) {
        element_.appendChild(element_.getOwnerDocument().importNode(
                result.getElement(), true));
    }
    
    public FrameworkTaskResult getFrameworkTaskResult() {
        Element ftr = getChildElement(TaskElem.frmwk_task_result);
        return ftr != null ? new FrameworkTaskResult(ftr) : null;
    }
    
    public List<ComponentTaskResult> getComponentTaskResults() {
        
        List<ComponentTaskResult> compList = new ArrayList<ComponentTaskResult>();
        List<Element> eleList = getChildElements(TaskElem.component_task_result);
        
        for (Element ele : eleList) {
            compList.add(new ComponentTaskResult(ele));
        }
        
        return compList;
    }

    @Override
    protected void initialize() {
        createChildElement(TaskElem.frmwk_task_result);
    }
    

}
