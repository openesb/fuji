/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLoggingService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;
import java.util.Dictionary;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.Configuration;
import com.sun.jbi.configuration.Constants;
import org.osgi.framework.ServiceReference;
import com.sun.jbi.framework.StringTranslator;

import com.sun.jbi.framework.osgi.internal.Environment;
        
/**
 * The <code>ComponentLoggingService</code> is used to track the list of configurable component loggers. 
 * Whenever a new component logger is added this service updates the list of loggers
 * it maintains. After that it updates the configuration of LogLevelConfigurationService. 
 * This sync is done to ensure that the admin tools will be able to verify the user 
 * provided logger name when the user tries to modify the log level of a logger. 
 */
public class ComponentLoggingService extends LoggingService {
    
    
    /** 
     * component logger name prefix 
     **/
    private String componentLoggerPrefix_;

   /**
     * translator
     */
    private static StringTranslator translator_ = new StringTranslator(
                LoggingService.class.getPackage().getName(), null);
 
    /**
     * Constructor
     * creates an instance of ComponentLoggingService
     * @param componentName
     */
    public ComponentLoggingService(String componentName) {
        super(Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR + "component-logger" + LoggingService.SEPARATOR +  componentName);
        componentLoggerPrefix_ = LoggingService.JBI_ROOT_LOGGER_NAME + "." + componentName;
        initializeComponentLoggers(componentName);
    }
            
    /**
     * This method is used to get a logger
     * @param loggerName logger name
     */
     public Logger getLogger(String loggerName) {
         //using componentName_ + ":" + loggerName gives  - contains illegal character
         //at org.apache.felix.cm.impl.CaseInsensitiveDictionary.checkKey(CaseInsensitiveDictionary.java:246)
  
         return super.getLogger(componentLoggerPrefix_ + "." + loggerName);
     }
     
    /**
     * This method is used to get a logger
     * @param loggerName logger name
     */
     public Logger getLogger(String loggerName, String resourceBundleName) {
         return super.getLogger(componentLoggerPrefix_ + "." + loggerName, resourceBundleName);
     }


    /**
     * This method is used to intialize the list of component loggers.
     * First existing configuration of the component-logger object is retrieved.
     * Only if it is null (first time startup or deleted config) the loggers are
     * initialized. In other cases, the configuration persisted by configuration admin
     * is given preference.
     *
     */
    private void initializeComponentLoggers(String componentName){


        try {
            ConfigurationAdmin configAdmin;

            if ((configAdmin = getConfigurationAdminService()) != null) {

                Configuration config = configAdmin.getConfiguration(
                            Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR + "component-logger" + LoggingService.SEPARATOR + componentName,
                            null);

                if (config != null) {

                    Dictionary properties = config.getProperties();

                    /* Only if the config is null (first time startup or deleted config) the loggers are
                     * initialized. In other cases, the configuration persisted by configuration admin
                     * is given preference, to handle log config persistence across system restarts.*/

                    if (properties == null) {
                        super.getLogger(componentLoggerPrefix_);
                    } else {
                        //not the first time, so initialize the list from existing config
                        initializeLoggerList((Vector<String>)properties.get(LoggingService.LOGGERS_KEY));
                    }
                }
            }
        } catch (Exception ex) {
            log_.log(Level.WARNING, translator_.getString(LocalStringKeys.CONFIG_UPDATE_FAILED), ex);

        }
    }



    /**
     * This method is used to get a reference to the config admin service
     */
    private static ConfigurationAdmin getConfigurationAdminService() {

        ConfigurationAdmin configAdmin = null;
        if (Environment.getFrameworkBundleContext() != null) {
            ServiceReference  configAdminRef =
               Environment.getFrameworkBundleContext().getServiceReference(ConfigurationAdmin.class.getName());

            if (configAdminRef != null) {
                configAdmin = (ConfigurationAdmin)
                         Environment.getFrameworkBundleContext().getService(configAdminRef);

            }
        }
        return configAdmin;
    }

    
}
