/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptorServiceListener.java - Last published on Nov 10, 2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import com.sun.jbi.configuration.Constants;
import com.sun.jbi.configuration.ConfigurationType;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.StringTranslator;
import com.sun.jbi.interceptors.LocalStringKeys;

import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ManagedService;

/**
 * This is a ServiceListener for Interceptor Services. 
 * 
 * When a Interceptor Service is :
 *  <ul>
 *  <li> Registered - A ManagedService is created for the interceptor and registered.
 *                    All the interceptor service properties are set as properties
 *                    in the configuration.</li>
 *  <li>  Unregistered - The Configuration for the interceptor is deleted and the 
 *                       ManagedService registered for it is unregistered.</li>
 *  <li>  Modified - Updates to the service properties are propagated to the 
 *                   service's configuration in the Config Admin Service</li>
 *  </ul>
 * 
 * @author Sun Microsystems
 */
public class InterceptorServiceListener
    implements ServiceListener
{
    
    /** The Logger **/
    private static Logger log_ = Logger.getLogger(InterceptorServiceListener.class.getPackage().getName());
    
    /** The StringTranslator **/
    private static StringTranslator 
            translator_ = new StringTranslator("com.sun.jbi.interceptors", null);
    
    /** 
     * Map of interceptor configuration managed service registrations 
     *        [ interceptor serviceRef | managedServiceRef ]
     */
    Map<ServiceReference, ServiceRegistration> managedServices_;
    
    
    /**
     * Receives notification that a service has had a lifecycle change. 
     * 
     * @param event
     */
    public void serviceChanged(ServiceEvent event)
    {
        switch ( event.getType() )
        {
            case ServiceEvent.REGISTERED :
            {
                injectBundleContext(event.getServiceReference());
                ServiceRegistration mgSvc = registerManagedService(event.getServiceReference());
                getManagedServices().put(event.getServiceReference(), mgSvc);
                break;
            }
            case ServiceEvent.UNREGISTERING :
            {   
                // Unregister the managed service registered for this service
                ServiceRegistration mgSvcReg = getManagedServices().get(event.getServiceReference());
                
                if ( mgSvcReg != null )
                {
                    mgSvcReg.unregister();
                }
                break;
            }
            case ServiceEvent.MODIFIED :
            {
                // TODO : With Task 1017 
                break;
            }
                        
        }
    }
    
    
    /**
     * @return the ManagedServices Map [ interceptor service ref | managed service ref ]
     */
    private Map<ServiceReference, ServiceRegistration> getManagedServices()
    {
        if ( managedServices_ == null )
        {
            managedServices_ = new HashMap();
        }
        
        return managedServices_;
    }
    
    /**
     * Register a Managed Service for Interceptor configuration 
     * 
     * @param svcRef - service reference of interceptor being registered.
     */
    ServiceRegistration registerManagedService(ServiceReference svcRef)
    {
        // Register a Managed Service for the Interceptor
        ConfigurationManagedService configSvc = 
                        new ConfigurationManagedService(getBundleContext(), svcRef);
                
        Properties props = new Properties();
        props.put(org.osgi.framework.Constants.SERVICE_PID, ConfigurationHelper.getServicePID(svcRef, null));
        ServiceRegistration mgSvc = getBundleContext().
                registerService(ManagedService.class.getName(), configSvc, props); 
            
        java.lang.reflect.Method mtd = InterceptionHelper.getInterceptorMethod(svcRef);
        
        if ( mtd != null )
        {
            if (!ConfigurationHelper.configExists(mtd, InterceptionHelper.getInterceptorName(svcRef)))
            {
                ConfigurationHelper.createConfiguration(svcRef);
            }
        }
        return mgSvc;
    }
    
    /**
     * @return the framework bundle context
     */
    public static org.osgi.framework.BundleContext getBundleContext()
    {
        return Environment.getFrameworkBundleContext();
    }
    
    /**
     * Inject the Bundle Context into the Interceptor Instance.
     * 
     * If there are any Context annotated fields set the BundleContext.
     * Field access modifiers are overriden, the field has to be of Type 
     * org.osgi.framework.BundleContext or of a Type derived from it.
     * 
     * @param svcRef - Interceptor Service Reference
     */ 
    public void injectBundleContext(ServiceReference svcRef)
    {
        Object interceptor = InterceptionHelper.getInterceptorInstance(svcRef);
        
        if ( interceptor != null )
        {
            java.lang.reflect.Field[] all = interceptor.getClass().getDeclaredFields();

            for(java.lang.reflect.Field field : all )
            {
                if ( field.isAnnotationPresent(com.sun.jbi.interceptors.Context.class) )
                {
                    // Set the context only if type of field is a subtype of BundleContext
                    if ( org.osgi.framework.BundleContext.class.isAssignableFrom(field.getType()))
                    {
                        // set the context
                        try
                        {
                            boolean isAccesible = field.isAccessible();
                            if (!isAccesible)
                            {
                                field.setAccessible(true);
                            }
                            field.set(interceptor, getBundleContext());

                            if (!isAccesible)
                            {
                                field.setAccessible(false);
                            }
                        }
                        catch(Exception ex)
                        {
                            log_.log(Level.FINE, "Failed to inject the Bundle Context", ex);
                        }
                    }
                    else
                    {
                        String warning = translator_.getString(LocalStringKeys.INTERCEPTOR_CONTEXT_WRONG_TYPE,
                                field.getName(), interceptor.getClass().getName(), field.getType().getName());
                        log_.warning(warning);
                    }
                    
                }
            }
        }
    }
}
