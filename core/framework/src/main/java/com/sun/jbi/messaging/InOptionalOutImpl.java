/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InOptionalOutImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.net.URI;

import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.NormalizedMessage;

/** Implementation of In Optional Out Message Exchange Pattern.
 * @author Sun Microsystems, Inc.
 */
public class InOptionalOutImpl 
        extends MessageExchangeProxy 
        implements InOptionalOut
{    
    /** Simple state machine for the SOURCE side of the  pattern. */
    private static final int[][]  SOURCE  =
    {
        { SET_IN | ADDRESS | SET_TRANSACTION |
            SET_PROPERTY | DO_SEND | DO_SENDSYNCH | SUSPEND_TX | MARK_ACTIVE | IN_SET,
          1, -1, -1, -1
        },
        { DO_ACCEPT | CHECK_STATUS_OR_FAULT | RESUME_TX,
          -1, 2, 9, 6 
        },
        { SET_DONE | SET_ERROR | SET_FAULT | CREATE_FAULT | SET_PROPERTY | OUT_SET,
          -1, -1, 3, 4 
        },
        { SET_DONE | SET_ERROR | DO_SEND | SET_PROPERTY,
          8, -1, 3, -1 
        },
        { SET_FAULT | CREATE_FAULT | DO_SEND | DO_SENDSYNCH | SET_PROPERTY | 
                  SUSPEND_TX | FAULT_SET,
          5, -1, -1, 4
        },
        { DO_ACCEPT | STATUS | RESUME_TX,
          -1, 9, -1, -1 
        },
        { SET_DONE | SET_ERROR | SET_PROPERTY | FAULT_SET,
          -1, -1, 7, -1 
        },
        { SET_DONE | SET_ERROR | DO_SEND | SET_PROPERTY,
          8, -1, 7, -1 
        },
        { MARK_DONE,
          -1, -1, -1, -1
        },
        { MARK_DONE | COMPLETE,
          -1, -1, -1, -1
        },
    };
    private static final String[] SOURCE_TEXT =
    {
        "C-CREATE", "C-ACCEPT-RESPONSE", "C-PROCESSING-RESPONSE", "C-DECIDE-STATUS", "C-DECIDE-FAULT",
        "C-ACCEPT-STATUS", "C-PROCESSING-FAULT", "C-FAULT-STATUS", "C-DONE", "C-DONE"
    };

    /** Simple state machine for the TARGET side of the pattern */
    private static final int[][]  TARGET  =
    {
        { DO_ACCEPT | REQUEST | RESUME_TX,
          -1, 1, -1, -1
        },
        { SET_OUT | SET_ERROR | SET_DONE | SET_FAULT | CREATE_FAULT | DO_SEND |
                        DO_SENDSYNCH | SUSPEND_TX | SET_PROPERTY | IN_SET | OUT_SET,
           2, -1, 6, 5 
        },
        { DO_ACCEPT | CHECK_STATUS_OR_FAULT,
           -1, -1, 8, 3 
        },
        { SET_ERROR | SET_DONE | SET_PROPERTY | FAULT_SET,
           -1, -1, 4, -1 
        },
        { SET_ERROR | SET_DONE | DO_SEND | SUSPEND_TX | SET_PROPERTY,
           9, -1, 4, -1 
        },
        { SET_FAULT | CREATE_FAULT | DO_SEND | DO_SENDSYNCH | SUSPEND_TX |
                  SET_PROPERTY | FAULT_SET,
           7, -1, -1, 5 
        },
        { SET_ERROR | SET_DONE | DO_SEND | SET_PROPERTY,
           9, -1, 6, -1 
        },
        { DO_ACCEPT | STATUS,
           -1, 8, -1, -1 
        },
        { MARK_DONE | COMPLETE,
           -1, -1, -1, -1 
        },
        { MARK_DONE,
           -1, -1, -1, -1 
        }
    };
    private static final String[] TARGET_TEXT =
    {
        "P-ACCEPT-REQUEST", "P-PROCESSING", "P-ACCEPT-RESPONSE", "P-RESPONSE-FAULT", "P-DECIDE-FAULT-STATUS",
        "P-DECIDE-FAULT", "P-DECIDE-STATUS", "P-ACCEPT-STATUS", "P-DONE", "P-DONE"
    };

    /**
     * Default constructor.
     */
    InOptionalOutImpl()
    {
        this(SOURCE, SOURCE_TEXT);
    }
    

    /** Create a new InOptionalOutImpl in the forward or reverse direction.
     */
    InOptionalOutImpl(int[][] state, String[] text)
    {
        super(state, text);
    }

    /**
     * Return a new instance of ourselves in the target role.
     */
    MessageExchangeProxy newTwin()
    {
        return (new InOptionalOutImpl(TARGET, TARGET_TEXT));
    }
    
    /** Get the pattern.
     * @return the message exchange pattern.
     */
    public URI getPattern()
    {
        return (ExchangePattern.IN_OPTIONAL_OUT.getURI());
    }

    /** Retrieve the message with reference id "in" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */ 
    public NormalizedMessage getInMessage()
    {
        return getMessage(IN_MSG);
    }
    
    /** Retrieve the message with reference id "out" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */  
    public NormalizedMessage getOutMessage()
    {
        return getMessage(OUT_MSG);        
    }    
    
    /** Specifies the "in" message reference for this exchange.
     *  @param msg in message
     *  @throws javax.jbi.messaging.MessagingException invalid message or the 
     *  current state of the exchange does not permit this operation.
     */
    public void setInMessage(NormalizedMessage msg)
        throws javax.jbi.messaging.MessagingException
    {
        setMessage(msg, IN_MSG);
    }
    
    /** Specifies the "out" message reference for this exchange.
     *  @param msg out message
     *  @throws javax.jbi.messaging.MessagingException invalid message or the 
     *  current state of the exchange does not permit this operation.
     */   
    public void setOutMessage(NormalizedMessage msg)
        throws javax.jbi.messaging.MessagingException
    {
        setMessage(msg, OUT_MSG);
    }

}
