/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDLHelper.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import com.sun.jbi.messaging.ExchangePattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

/** Silly little utility object to help out in parsing WSDL for some NMR-specific
 *  requirements.  NOTE: this is built as a layer on top of our existing WSDL
 *  API.
 * @author Sun Microsystems, Inc.
 */
public final class WSDLHelper
{
    public static final String WSDL_11 = "http://schemas.xmlsoap.org/wsdl/";
    public static final String WSDL_20 = "http://www.w3.org/ns/wsdl";
    
    private static final String TARGET_NS       = "targetNamespace";
    private static final String DEFINITIONS     = "definitions";
    private static final String BINDING         = "binding";
    private static final String BINDING_TYPE    = "type";
    private static final String PORTTYPE        = "portType";
    private static final String SERVICE         = "service";
    private static final String PORT            = "port";
    private static final String NAME            = "name";
    private static final String MESSAGE         = "message";
    private static final String IMPORT          = "import";
    private static final String PART            = "part";
    private static final String TYPES           = "types";
    private static final String ELEMENT         = "element";
    private static final String SCHEMA          = "schema";
    private static final String INTERFACE       = "interface";
    private static final String OPERATION       = "operation";
    private static final String EXTENDS         = "extends";
    private static final String PATTERN         = "pattern";
    private static final String IN_MSG          = "input";
    private static final String OUT_MSG         = "output";
    private static final String LOCATION        = "location";
    private static final String NAMESPACE       = "namespace";
    private static final String ELEMENT_ORDER = TYPES + "." + MESSAGE + "." +  PORTTYPE+ "." +  BINDING+ "." +  SERVICE ;
    private static final String ELEMENT_ORDER20 = TYPES + "." + INTERFACE + "." + BINDING+ "." +  SERVICE;

    private static final Logger mLog = Logger.getLogger("com.sun.jbi.messaging");

    public static Document buildBasicWSDL(String wsdl, String namespace,
            String serviceName,
            String endpointName, Map<QName,List<QName>> interfaces,
            Map<QName, String> operations)
                throws javax.jbi.messaging.MessagingException
    {
        Document    doc = XMLUtil.getInstance().newDocument();
        int         index = namespace.lastIndexOf("/");
        String      ifns = namespace.substring(0, index + 1) + "interface/" + namespace.substring(index + 1);
        String      msgns = namespace.substring(0, index + 1) + "service/message/" + namespace.substring(index + 1);
        String      wsdlns = wsdl.equals(WSDL_11) ? WSDL_11 : WSDL_20;
        Element     defs;

        //
        //  Create the definitions.
        //
        defs = doc.createElementNS(wsdlns, DEFINITIONS);
        defs.setAttribute(NAME, serviceName);
        defs.setAttribute(TARGET_NS, namespace);
        defs.setAttribute("xmlns", wsdl);
        defs.setAttribute("xmlns:wsdl", wsdl);
        defs.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        defs.setAttribute("xmlns:tns", namespace);
        defs.setAttribute("xmlns:msg", msgns);
        defs.setAttribute("xmlns:if", ifns);

        //
        // Create the types
        //
        Element types;
        Element schema;
        Element mport;

        types = doc.createElementNS(wsdlns, TYPES);
        schema = doc.createElement("xsd:schema");
        schema.setAttribute(TARGET_NS, msgns);
        types.appendChild(schema);
        mport = doc.createElement("xsd:import");
        mport.setAttribute("schemaLocation", "message.xsd");
        mport.setAttribute("namespace", msgns);
        types.appendChild(mport);
        defs.appendChild(types);

        //
        //  Create the binding(s)
        //
        Element binding;
        int i = 1;

        for (QName iface : interfaces.keySet())
        {
            binding = doc.createElementNS(wsdlns, BINDING);
            binding.setAttribute(NAME,"EIP-" + serviceName + "-" + i);
            binding.setAttribute(BINDING_TYPE, iface.getLocalPart());
            defs.appendChild(binding);
        }
        
        //
        // Create the message(s).
        //
        Element msg;
        Element part;

        msg = doc.createElementNS(wsdlns, MESSAGE);
        msg.setAttribute(NAME, "anyMsg");
        part = doc.createElementNS(wsdlns, PART);
        part.setAttribute(NAME, "part1");
        part.setAttribute(ELEMENT, "xsd:anyType");
        msg.appendChild(part);
        defs.appendChild(msg);

        //
        // Create the portType(s).
        //
        for (Map.Entry<QName,List<QName>> m : interfaces.entrySet())
        {
            Element portType;

            portType = doc.createElementNS(wsdlns, PORTTYPE);
            portType.setAttribute(NAME, m.getKey().getLocalPart());
            for (QName op : m.getValue())
            {
                Element operation;
                Element input;
                Element output;
                String  exchangeType = operations.get(op);

                operation = doc.createElementNS(wsdlns, OPERATION);
                operation.setAttribute(NAME, op.getLocalPart());
                input = doc.createElementNS(wsdlns, IN_MSG);
                input.setAttribute(NAME, "in");
                input.setAttribute(MESSAGE, "tns:anyMsg");
                operation.appendChild(input);
                if (exchangeType.equals(ExchangePattern.IN_OUT.toString()))
                {
                    output = doc.createElementNS(wsdlns, OUT_MSG);
                    output.setAttribute(NAME, "out");
                    output.setAttribute(MESSAGE, "tns:anyMsg");
                    operation.appendChild(output);
                }
                portType.appendChild(operation);
            }
            defs.appendChild(portType);
        }

        //
        //  Create the service type.
        //
        Element service;
        Element port;
        i = 1;
        service = doc.createElementNS(wsdlns, SERVICE);
        service.setAttribute(NAME, serviceName);
        for (QName iface : interfaces.keySet())
        {
            port = doc.createElementNS(wsdlns, PORT);
            port.setAttribute(NAME, endpointName);
            port.setAttribute(BINDING, "tns:EIP-" + serviceName + "-" + i);
            service.appendChild(port);
        }
        defs.appendChild(service);

        //
        //  Return the document.
        //
        doc.appendChild(defs);

        return (doc);
    }

    public static Document buildInterfaceWSDL(String wsdl, String namespace,
            String serviceName,
            String endpointName, Map<QName,List<QName>> interfaces,
            Map<QName, String> operations)
                throws javax.jbi.messaging.MessagingException {
        Document    doc = XMLUtil.getInstance().newDocument();
        int         index = namespace.lastIndexOf("/");
        String      ifns = namespace.substring(0, index + 1) + "interface/" + namespace.substring(index + 1);
        String      msgns = namespace.substring(0, index + 1) + "service/message/" + namespace.substring(index + 1);
        String      wsdlns = wsdl.equals(WSDL_11) ? WSDL_11 : WSDL_20;
        Element     defs;
         //
        //  Create the definitions.
        //
        defs = doc.createElementNS(wsdlns, DEFINITIONS);
        defs.setAttribute(NAME, serviceName);
        defs.setAttribute(TARGET_NS, namespace);
        defs.setAttribute("xmlns", wsdl);
        defs.setAttribute("xmlns:wsdl", wsdl);
        defs.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        defs.setAttribute("xmlns:tns", namespace);
        defs.setAttribute("xmlns:msg", msgns);
        defs.setAttribute("xmlns:if", ifns);

//        //
//        // Create the types
//        //
//        Element types;
//        Element schema;
//        Element mport;
//
//        types = doc.createElementNS(wsdlns, TYPES);
//        schema = doc.createElement("xsd:schema");
//        schema.setAttribute(TARGET_NS, msgns);
//        types.appendChild(schema);
//        mport = doc.createElement("xsd:import");
//        mport.setAttribute("schemaLocation", "message.xsd");
//        mport.setAttribute("namespace", msgns);
//        types.appendChild(mport);
//        defs.appendChild(types);

        //
        // Create the message(s).
        //
        Element msg;
        Element part;

        msg = doc.createElementNS(wsdlns, MESSAGE);
        msg.setAttribute(NAME, "anyMsg");
        part = doc.createElementNS(wsdlns, PART);
        part.setAttribute(NAME, "part1");
        part.setAttribute(BINDING_TYPE, "xsd:anyType");
        msg.appendChild(part);
        defs.appendChild(msg);

        //
        // Create the portType(s).
        //
        for (Map.Entry<QName,List<QName>> m : interfaces.entrySet())
        {
            Element portType;

            portType = doc.createElementNS(wsdlns, PORTTYPE);
            portType.setAttribute(NAME, m.getKey().getLocalPart());
            for (QName op : m.getValue())
            {
                Element operation;
                Element input;
                Element output;
                String  exchangeType = operations.get(op);

                operation = doc.createElementNS(wsdlns, OPERATION);
                operation.setAttribute(NAME, op.getLocalPart());
                input = doc.createElementNS(wsdlns, IN_MSG);
                input.setAttribute(NAME, "in");
                input.setAttribute(MESSAGE, "tns:anyMsg");
                operation.appendChild(input);
                if (exchangeType.equals(ExchangePattern.IN_OUT.toString()))
                {
                    output = doc.createElementNS(wsdlns, OUT_MSG);
                    output.setAttribute(NAME, "out");
                    output.setAttribute(MESSAGE, "tns:anyMsg");
                    operation.appendChild(output);
                }
                portType.appendChild(operation);
            }
            defs.appendChild(portType);
        }

        //
        //  Return the document.
        //
        doc.appendChild(defs);

        return (doc);
   }

    public static Document buildBindingWSDL(String wsdl, String namespace,
            String serviceName,
            String endpointName, Map<QName,List<QName>> interfaces,
            Map<QName, String> operations)
                throws javax.jbi.messaging.MessagingException
    {
        Document    doc = XMLUtil.getInstance().newDocument();
        int         index = namespace.lastIndexOf("/");
        String      ifns = namespace.substring(0, index + 1) + "interface/" + namespace.substring(index + 1);
        String      msgns = namespace.substring(0, index + 1) + "service/message/" + namespace.substring(index + 1);
        String      wsdlns = wsdl.equals(WSDL_11) ? WSDL_11 : WSDL_20;
        Element     defs;

        //
        //  Create the definitions.
        //
        defs = doc.createElementNS(wsdlns, DEFINITIONS);
        defs.setAttribute(NAME, serviceName);
        defs.setAttribute(TARGET_NS, namespace);
        defs.setAttribute("xmlns", wsdl);
        defs.setAttribute("xmlns:wsdl", wsdl);
        defs.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
        defs.setAttribute("xmlns:tns", namespace);
        defs.setAttribute("xmlns:msg", msgns);
        defs.setAttribute("xmlns:if", ifns);

        //
        // Import the interface.
        //
        Element wsdlimport;

        wsdlimport = doc.createElementNS(wsdlns, IMPORT);
        wsdlimport.setAttribute(LOCATION, "interface.wsdl");
        wsdlimport.setAttribute(NAMESPACE, ifns);
        defs.appendChild(wsdlimport);

        //
        //  Create the binding(s)
        //
        Element binding;
        int i = 1;

        for (QName iface : interfaces.keySet())
        {
            binding = doc.createElementNS(wsdlns, BINDING);
            binding.setAttribute(NAME,"EIP-" + serviceName + "-" + i);
            binding.setAttribute(BINDING_TYPE, iface.getLocalPart());
            defs.appendChild(binding);
        }

        //
        //  Create the service type.
        //
        Element service;
        Element port;
        i = 1;
        service = doc.createElementNS(wsdlns, SERVICE);
        service.setAttribute(NAME, serviceName);
        for (QName iface : interfaces.keySet())
        {
            port = doc.createElementNS(wsdlns, PORT);
            port.setAttribute(NAME, endpointName);
            port.setAttribute(BINDING, "tns:EIP-" + serviceName + "-" + i);
            service.appendChild(port);
        }
        defs.appendChild(service);

        //
        //  Return the document.
        //
        doc.appendChild(defs);

        return (doc);
    }

    public static Document flatten(QName serviceName, Document doc, EntityResolver resolver) {
        Element definitions = doc.getDocumentElement();
        String  version = definitions.getNamespaceURI();
        String  namespace = definitions.getAttribute(TARGET_NS);
        NodeList imports;

        if (version == null || version.equals(WSDL_11)) {
            imports = definitions.getElementsByTagNameNS(WSDL_11, IMPORT);
        } else {
            imports = definitions.getElementsByTagNameNS(WSDL_20, IMPORT);
        }
        for (int i = 0; i < imports.getLength(); i++) {
            Element     imp = (Element)imports.item(i);
            Document    impdoc = resolveImport(resolver, serviceName, imp.getAttribute("location"), imp.getAttribute("namespace"));

            if (impdoc.getDocumentElement().getNamespaceURI().equals(namespace)) {
                definitions.removeChild(imp);
                impdoc = flatten(serviceName, impdoc, resolver);
                if (version == null || version.equals(WSDL_11)) {
                    merge11(doc, impdoc);
                } else {
                    merge20(doc, impdoc);
                }
            }
        }
        if (mLog.isLoggable(Level.FINE)) {
            try
            {
                mLog.fine("Flattened ServiceDescription for Service(" + serviceName + ")" +
                    "\n------------------->\n" + new XMLUtil().asString(doc) +
                    "\n------------------->\n");
            } catch (javax.jbi.messaging.MessagingException ignore) {
            }
        }

        return (doc);
    }

    private static void merge11(Document head, Document include) {
        Element ie = include.getDocumentElement();
        NodeList iTypes = ie.getElementsByTagNameNS(WSDL_11, TYPES);
        NodeList iMessages = ie.getElementsByTagNameNS(WSDL_11, MESSAGE);
        NodeList iPortTypes = ie.getElementsByTagNameNS(WSDL_11, PORTTYPE);

        for (int i = 0; i < iTypes.getLength(); i++)
        {
            Element e = (Element)iTypes.item(i);
            insertElement(head, (Element)head.importNode(e, true), ELEMENT_ORDER);
        }
        for (int i = 0; i < iMessages.getLength(); i++)
        {
            Element e = (Element)iMessages.item(i);
            insertElement(head, (Element)head.importNode(e, true), ELEMENT_ORDER);
        }
        for (int i = 0; i < iPortTypes.getLength(); i++)
        {
            Element e = (Element)iPortTypes.item(i);
            insertElement(head, (Element)head.importNode(e, true), ELEMENT_ORDER);
        }
    }

    private static void merge20(Document head, Document include) {
        Element ie = include.getDocumentElement();
        NodeList iTypes = ie.getElementsByTagNameNS(WSDL_20, TYPES);
        NodeList iInterfaces = ie.getElementsByTagNameNS(WSDL_20, INTERFACE);

        for (int i = 0; i < iTypes.getLength(); i++)
        {
            Element e = (Element)iTypes.item(i);
            insertElement(head, (Element)head.importNode(e, true), ELEMENT_ORDER20);
        }
        for (int i = 0; i < iInterfaces.getLength(); i++)
        {
            Element e = (Element)iInterfaces.item(i);
            insertElement(head, (Element)head.importNode(e, true), ELEMENT_ORDER20);
        }
    }

    private static void insertElement(Document head, Element element, String order) {
        Element  definition = head.getDocumentElement();
        NodeList nodes = definition.getChildNodes();
        Element lastMatch = null;

        for (int i = 0; i < nodes.getLength(); i++) {
            Node    node = nodes.item(i);

            if (node instanceof Element) {
                Element  e = (Element)node;
                if (e.getNamespaceURI().equals(element.getNamespaceURI())) {
                    if (order.indexOf(e.getTagName()) >
                            order.indexOf(element.getTagName()))
                    {
                        definition.insertBefore(element, node);
                        return;
                    }
                }
            }
        }
        definition.appendChild(element);
    }

    public static void getOperationsAndInterfaces(Document serviceDesc,
            QName serviceName, EntityResolver resolver,
            Map<QName,Map<String,QName>> operations, List<QName> interfaces)
            throws javax.jbi.messaging.MessagingException {
        Element                         definitions = serviceDesc.getDocumentElement();
        String                          tns = definitions.getAttribute(TARGET_NS);
        Map<String,String>              ns = getNamespaces(definitions);
        String                          version = definitions.getNamespaceURI();
        Map<QName,QName>                bindingInfo = new HashMap();
        Map<QName,Map<String,QName>>    portTypeInfo = new HashMap();
        Map<QName,Map<String,QName>>    messageInfo = new HashMap();
        Map<QName,List<QName>>          operationInfo = new HashMap();
        Map<String,QName>               portInfo = new HashMap();
        List<QName>                     interfaceInfo = new LinkedList();

         if (mLog.isLoggable(Level.INFO)) {
            try
            {
                mLog.info("Analyze ServiceDescription for Service(" +
                    serviceName +
                    ")\n------------------->\n" + new XMLUtil().asString(serviceDesc) +
                    "\n------------------->\n");
            } catch (javax.jbi.messaging.MessagingException ignore) {
            }
        }
        if (version == null || version.equals(WSDL_11))
        {
            importDefinitions11(definitions, serviceName, resolver, bindingInfo, portTypeInfo, operationInfo, messageInfo);
            importBindings11(definitions, tns, ns, bindingInfo);
            importPortTypes11(definitions, tns, ns, operationInfo, portTypeInfo);
            importMessages11(definitions, tns, ns, messageInfo);
            importService11(definitions, serviceName, tns, ns, portInfo);
            resolveInterfaces11(definitions, portInfo, bindingInfo, interfaces);
            resolveOperations11(definitions, interfaces, operationInfo, portTypeInfo, messageInfo, operations);
        }
        else
        {
            importDefinitions20(definitions, serviceName, resolver, bindingInfo, interfaceInfo, operations, messageInfo);
            importInterfaces20(definitions, serviceName, tns, ns, operations, interfaces);
        }

        if (mLog.isLoggable(Level.INFO)) {
            StringBuffer    sb = new StringBuffer();
            sb.append("\n  Found Interfaces (" + interfaces.size() + ") for Service(" +
                serviceName + ")\n");
            for (QName i : interfaces) {
                sb.append("    Interface: " + i + "\n");
            }
            sb.append("  Found Operations (" + operations.size() + ") for Service(" +
                serviceName + ")\n");
            for (Map.Entry<QName,Map<String,QName>> i : operations.entrySet()) {
                sb.append("    Operation: " + i.getKey() + "\n");
                for (Map.Entry<String,QName> j : i.getValue().entrySet()) {
                    sb.append("      Key: " + j.getKey() + "\n");
                    sb.append("        Value: " + j.getValue() + "\n");
                }
            }
            mLog.info(sb.toString());
        }
    }

    private static void importDefinitions11(Element definitions,
            QName serviceName,
            EntityResolver resolver,
            Map<QName,QName> bindings,
            Map<QName,Map<String,QName>> portTypes,
            Map<QName,List<QName>> operations,
            Map<QName,Map<String,QName>> messages) {
        String version = definitions.getNamespaceURI();
        String namespace = definitions.getAttribute(TARGET_NS);

        if (version == null || version.equals(WSDL_11)) {
            NodeList imports = definitions.getElementsByTagNameNS(WSDL_11, IMPORT);
            for (int i = 0; i < imports.getLength(); i++) {
                Element     imp = (Element)imports.item(i);
                Document    doc = resolveImport(resolver, serviceName, imp.getAttribute("location"), imp.getAttribute("namespace"));

                if (doc != null) {
                    Element             def = doc.getDocumentElement();
                    Map<String,String>  ns = getNamespaces(def);

                    importDefinitions11(def, serviceName, resolver, bindings, portTypes, operations, messages);
                    importBindings11(def, namespace, ns, bindings);
                    importPortTypes11(def, namespace, ns, operations, portTypes);
                    importMessages11(def, namespace, ns, messages);
                }
            }
        }
    }

    private static void importDefinitions20(Element definitions,
            QName serviceName,
            EntityResolver resolver,
            Map<QName,QName> bindings,
            List<QName> interfaces,
            Map<QName,Map<String,QName>> operations,
            Map<QName,Map<String,QName>> messages) {
        String version = definitions.getNamespaceURI();
        String namespace = definitions.getAttribute(TARGET_NS);

        if (version.equals(WSDL_20)) {
            NodeList imports = definitions.getElementsByTagNameNS(WSDL_20, IMPORT);
            for (int i = 0; i < imports.getLength(); i++) {
                Element     imp = (Element)imports.item(i);
                Document    doc = resolveImport(resolver, serviceName, imp.getAttribute("location"), imp.getAttribute("namespace"));

                if (doc != null) {
                    Element             def = doc.getDocumentElement();
                    Map<String,String>  ns = getNamespaces(def);

                    importDefinitions20(def, serviceName, resolver, bindings, interfaces, operations, messages);
                    importInterfaces20(def, serviceName, namespace, ns, operations, interfaces);
                }
            }
        }
    }

    private static Document resolveImport(EntityResolver resolver, QName serviceName, String location, String namespace) {
        Document doc = null;

        if (resolver != null) {
            try {

                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                dbf.setNamespaceAware(true);
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = resolver.resolveEntity(null, location);
                doc = db.parse(is.getByteStream());
                if (mLog.isLoggable(Level.FINE)) {
                    try
                    {
                        mLog.fine("Analyze Entity(" + location + ") for Service(" +
                            serviceName +
                            ")\n------------------->\n" + new XMLUtil().asString(doc) +
                            "\n------------------->\n");
                    } catch (javax.jbi.messaging.MessagingException ignore) {
                    }
                }
            } catch (Exception ignore) {
                System.out.print(ignore);
            }
        }
        return (doc);
    }


    private static void importBindings11(Element def, String tns, Map<String,String> ns,
            Map<QName,QName> bindingInfo) {
        NodeList bindings = def.getElementsByTagNameNS(WSDL_11, BINDING);

        for (int i = 0; i < bindings.getLength(); i++)
        {
            Element binding = (Element)bindings.item(i);
            String  name    = binding.getAttribute(NAME);

            bindingInfo.put(new QName(tns, name),
                    getQualifiedName(binding.getAttribute(BINDING_TYPE), tns, ns));
        }
    }

    private static void importPortTypes11(Element def, String tns, Map<String,String> ns,
            Map<QName,List<QName>> portTypeInfo,
            Map<QName,Map<String,QName>> operationInfo) {
        NodeList portTypes = def.getElementsByTagNameNS(WSDL_11, PORTTYPE);

        for (int i = 0; i < portTypes.getLength(); i++)
        {
            Element portType = (Element)portTypes.item(i);
            QName  ptname    = new QName(tns, portType.getAttribute(NAME));
            NodeList operations = portType.getElementsByTagNameNS(WSDL_11, OPERATION);
            List<QName> ops = new LinkedList();

            for (int j = 0; j < operations.getLength(); j++) {
                Element     operation = (Element)operations.item(j);
                QName       opname = new QName(tns, operation.getAttribute(NAME));
                NodeList    ins  = operation.getElementsByTagNameNS(WSDL_11, IN_MSG);
                NodeList    outs = operation.getElementsByTagNameNS(WSDL_11, OUT_MSG);

                // All supported meps must have an in message
                if (ins.getLength() > 0)
                {
                    Element  in = (Element)ins.item(0);
                    Map<String,QName>  inout = new HashMap();
                    QName  msg = getQualifiedName(in.getAttribute(MESSAGE), tns, ns);
                    inout.put(IN_MSG, msg);
                    if (outs.getLength() > 0)
                    {
                        Element  out = (Element)outs.item(0);
                        msg = getQualifiedName(out.getAttribute(MESSAGE), tns, ns);
                        inout.put(OUT_MSG, msg);
                        inout.put(PATTERN, new QName(ExchangePattern.IN_OUT.toString()));
                    }
                    else
                    {
                        inout.put(PATTERN, new QName(ExchangePattern.IN_ONLY.toString()));
                    }
                    operationInfo.put(opname, inout);
                    ops.add(opname);
                }
            }
            portTypeInfo.put(ptname, ops);
        }
    }

    private static void importInterfaces20(Element def, QName serviceName, String tns, Map<String,String> ns,
            Map<QName,Map<String,QName>> operations, List<QName> interfaces) {
        Map<QName,QName>    types = getTypes(def, ns);

        if (tns != null && serviceName.getNamespaceURI().equals(tns))
        {
            NodeList serviceList = def.getElementsByTagNameNS(WSDL_20, SERVICE);
            for (int i = 0; i < serviceList.getLength(); i++) {
                Element service = (Element)serviceList.item(i);
                if (service.getAttribute(NAME).equals(serviceName.getLocalPart())) {
                    getInterfaceChain(def, tns,
                        getLocalName(service.getAttribute(INTERFACE)), interfaces);
                    break;
                }
            }
        }

        NodeList nl = def.getElementsByTagNameNS(WSDL_20, INTERFACE);
        Element interfaze;
        for (int i = 0; i < nl.getLength(); i++) {
            interfaze = (Element)nl.item(i);
            if (!interfaces.contains(new QName(tns, interfaze.getAttribute(NAME)))) {
                continue;
            }

            // Found an interface we're looking for, figure out the ops
            NodeList operationList =
                    interfaze.getElementsByTagNameNS(WSDL_20, OPERATION);
            for (int k = 0; k < operationList.getLength(); k++) {
                Element operation = (Element)operationList.item(k);
                NodeList    ins  = operation.getElementsByTagNameNS(WSDL_20, IN_MSG);
                NodeList    outs = operation.getElementsByTagNameNS(WSDL_20, OUT_MSG);
                QName       name = new QName(tns, operation.getAttribute(NAME));
                Map<String,QName>  inout = new HashMap();

                if (ins.getLength() > 0)
                {
                    Element  in = (Element)ins.item(0);
                    QName  element = getQualifiedName(in.getAttribute(ELEMENT), null, ns);
                    QName  typeInfo = types.get(element);
                    inout.put("in-message", element);
                    inout.put("in-type", typeInfo);
                }
                if (outs.getLength() > 0)
                {
                    Element  out = (Element)outs.item(0);
                    QName  element = getQualifiedName(out.getAttribute(ELEMENT), null, ns);
                    QName  typeInfo = types.get(element);
                    inout.put("out-message", element);
                    inout.put("out-type", typeInfo);
                }
                inout.put(PATTERN, new QName(operation.getAttribute(PATTERN)));
                operations.put(name, inout);
            }
       }
    }

    

    private static void importMessages11(Element def, String tns, Map<String,String> ns,
            Map<QName,Map<String,QName>> messageInfo) {
        NodeList    messages = def.getElementsByTagNameNS(WSDL_11, MESSAGE);;

        // Get a list of messages
        for (int i = 0; i < messages.getLength(); i++)
        {
            Element     message = (Element)messages.item(i);
            NodeList    parts = message.getElementsByTagNameNS(WSDL_11, PART);

            for (int j = 0; j< parts.getLength(); j++)
            {
                Element part = (Element)parts.item(j);
                HashMap<String,QName>   partInfo = new HashMap();

                partInfo.put(NAME, new QName(part.getAttribute(NAME)));
                if (!part.getAttribute(BINDING_TYPE).equals(""))
                {
                    partInfo.put(BINDING_TYPE,
                            getQualifiedName(part.getAttribute(BINDING_TYPE), tns, ns));
                }
                else
                {
                    partInfo.put(ELEMENT, getQualifiedName(part.getAttribute(ELEMENT), tns, ns));
                }
                messageInfo.put(new QName(tns, message.getAttribute(NAME)), partInfo);
            }
        }
    }

    private static void importService11(Element def, QName serviceName,
            String tns,
            Map<String,String> ns,
            Map<String,QName> portInfo) {
        NodeList    services;

        // get a list of service bindings
        services = def.getElementsByTagNameNS(WSDL_11, SERVICE);
        for (int i = 0; i < services.getLength(); i++)
        {
            Element service = (Element)services.item(i);
            if (service.getAttribute(NAME).equals(serviceName.getLocalPart()))
            {
                NodeList ports = service.getElementsByTagNameNS(WSDL_11, PORT);
                for (int j = 0; j < ports.getLength(); j++)
                {
                    Element port = (Element)ports.item(j);
                    portInfo.put(port.getAttribute(NAME), getQualifiedName(port.getAttribute(BINDING), tns, ns));
                }
                break;
            }
        }
    }

    private static void resolveOperations11(Element def,
            List<QName> interfaces,
            Map<QName,List<QName>> operationInfo,
            Map<QName,Map<String,QName>> portTypes,
            Map<QName,Map<String,QName>> messages,
            Map<QName,Map<String,QName>> operations) {

        for (QName interfaceName : interfaces) {
            List<QName>         ops = operationInfo.get(interfaceName);
            if (ops != null) {
                for (QName operationName : ops) {
                    Map<String,QName>   portType = portTypes.get(operationName);
                    QName               input = portType.get("input");
                    QName               output = portType.get("output");
                    QName               pattern = portType.get("pattern");
                    Map<String,QName>   in = messages.get(input);
                    Map<String,QName>   out = messages.get(output);
                    QName               element = in.get("element");
                    QName               type = in.get("type");
                    Map<String,QName>   msg = new HashMap();

                    msg.put("in-message", input);
                    msg.put("pattern", pattern);
                    if (type != null) {
                        msg.put("in-type", type);
                    }
                    if (element != null) {
                        msg.put("in-element", element);
                    }
                    if (out != null) {
                        type = out.get("type");
                        element = out.get("element");
                        msg.put("out-message", output);
                        if (type != null) {
                            msg.put("out-type", type);
                        }
                        if (element != null) {
                            msg.put("out-element", element);
                        }
                    }
                    operations.put(operationName, msg);
                }
            }
        }
    }

    private static void resolveInterfaces11(Element def,
            Map<String,QName> ports,
            Map<QName,QName> bindings,
            List<QName> interfaces) {

        for (Map.Entry<String, QName> p : ports.entrySet()) {
            QName   portBinding = p.getValue();
            QName   bindingType;

            bindingType = bindings.get(portBinding);
            if (bindingType != null && !interfaces.contains(bindingType)) {
                interfaces.add(bindingType);
            }
        }
    }
            
    private static QName getQualifiedName(String name, String tns, Map<String,String> namespaces)
    {
        String  ns;
        String  local;
        String[]  parts = name.split(":");

        if (parts.length == 1)
        {
            ns = tns;
            local = parts[0];
        }
        else
        {
            ns = namespaces.get(parts[0]);
            local = parts[1];
        }
        return (new QName(ns, local));
    }
            
    private static List<QName> getInterfaceChain(
            Element def, String tns, String interfaceName, List<QName> chain) {

        if (interfaceName != null) {
            NodeList nl = def.getElementsByTagNameNS(WSDL_20, INTERFACE);
            for (int i = 0; i < nl.getLength(); i++) {
                Element ie = (Element)nl.item(i);
                if (!ie.getAttribute(NAME).equals(interfaceName)) {
                    continue;
                }
                getInterfaceChain(def, tns,
                        getLocalName(ie.getAttribute(EXTENDS)), chain);
                chain.add(new QName(tns, ie.getAttribute(NAME)));
            }
        }

        return chain;
    }

    private static Map<String,String> getNamespaces(Element definitions)
    {
        NamedNodeMap    atts = definitions.getAttributes();
        Map<String,String>  ns = new HashMap();

        for (int i = 0; i < atts.getLength(); i++)
        {
            Node    att = atts.item(i);
            String  name = att.getNodeName();

            if (name.equals("name") || name.equals(TARGET_NS) || name.equals("xmlns"))
            {
                continue;
            }
            ns.put(name.substring(6), att.getNodeValue());
        }
        return (ns);
    }

    private static Map<QName,QName> getTypes(Element definitions, Map<String,String> namespaces)
    {
        NodeList    types;
        HashMap<QName,QName>  typeInfo = new HashMap();

        // Get a list of messages
        types = definitions.getElementsByTagNameNS(WSDL_20, TYPES);
        Element type = (Element)types.item(0);
        if (type != null)
        {
            NodeList    schemas = type.getElementsByTagNameNS(WSDL_20, SCHEMA);
            for (int i = 0; i < schemas.getLength(); i++)
            {
                Element schema = (Element)schemas.item(i);
                String  tns = schema.getAttribute(TARGET_NS);
                NodeList    elements = schema.getElementsByTagNameNS(WSDL_20, ELEMENT);
                for (int j = 0; j< elements.getLength(); j++)
                {
                    Element element = (Element)elements.item(j);
                    typeInfo.put(new QName(tns, element.getAttribute(NAME)),
                            getQualifiedName(element.getAttribute(BINDING_TYPE), null, namespaces));
                }
            }
        }

        return (typeInfo);
    }

    private static String getLocalName(String qname)
    {
        if (qname.indexOf(':') != -1)
        {
            qname = qname.split(":")[1];
        }
        
        return qname;
    }
    
}
