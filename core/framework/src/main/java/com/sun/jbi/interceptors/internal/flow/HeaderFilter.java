/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegexFilter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.service.ServiceMessageImpl;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.jbi.messaging.MessageExchange;
import javax.xml.namespace.QName;

/**
 * Regex filter
 *
 * @author Sun Microsystems, Inc.
 */
public class HeaderFilter
    implements MessageFilter {

    Matcher             matcher_;
    String              name_;
    String              meProp_;
    String              nmProp_;
    String              meOper_;
    String              meInter_;
    String              regex_;
    String              nmAttach_;
    String              nmType_;

    /**
     * Create a Regex Filter instance
     * @param expression - regex
     */
    public HeaderFilter(String name, Properties config) {
        meProp_ = config.getProperty("me.prop");
        if (meProp_ == null) {
            meProp_ = config.getProperty("me.property");
        }
        meOper_ = config.getProperty("me.oper");
        if (meOper_ == null) {
            meOper_ = config.getProperty("me.operation");
        }
        meInter_ = config.getProperty("me.interface");
        nmProp_ = config.getProperty("nm.prop");
        if (nmProp_ == null) {
            nmProp_ = config.getProperty("nm.property");
        }
        nmAttach_ = config.getProperty("nm.attach");
        if (nmAttach_ == null) {
            nmAttach_ = config.getProperty("nm.attachment");
        }
        regex_ = config.getProperty("exp");
        if (regex_ != null) {
            matcher_ = Pattern.compile(regex_).matcher("");
        }
        nmType_ = config.getProperty("nm.type");
        if (meProp_ == null && nmProp_ == null && nmAttach_ == null &&
                meOper_ == null && meInter_ == null && nmType_ == null) {
            throw new RuntimeException("HeaderFilter: must have a selection criteria.");
        }
    }

    /**
     *
     * @return true if the active message in the message exchange contains the
     *         specified header expression.
     * @param exchange - exchange to filter
     */
    public boolean matches(MessageExchange exchange)
        throws Exception
    {
        ServiceMessageImpl svcMsg = (ServiceMessageImpl)MessageExchangeUtil.getActiveMessage(exchange);
        String  value = null;

        if (meProp_ != null) {
            value = (String)exchange.getProperty(meProp_);
        }
        else if (meOper_ != null) {
            QName   operation = exchange.getOperation();
            if (operation != null) {
                value = (String)operation.toString();
                if (!meOper_.equals("*") && !value.equals(meOper_)) {
                    value = null;
                }
            }
        } else if (meInter_ != null) {
            QName   iface = exchange.getInterfaceName();
            if (iface != null) {
                value = (String)iface.toString();
                if (!meInter_.equals("*") && !value.equals(meInter_)) {
                    value = null;
                }
            }
        } else if (svcMsg != null && nmProp_ != null) {
            value = (String)svcMsg.getProperty(nmProp_);
        } else if (svcMsg != null && nmAttach_ != null) {
            DataHandler     dh = svcMsg.getAttachment(nmAttach_);
            value = dh == null ? "" : dh.getContentType();
        } else if (svcMsg != null && nmType_ != null) {
            value = svcMsg.getMessageType().toString();
            if (!nmType_.equals("*") && !value.equals(nmType_)) {
                value = null;
            }
        }
        if (value != null) {
            if (matcher_ != null) {
                matcher_.reset(value);
                return (matcher_.find());
            }
            return (true);
        }
        return false;
    }

    /**
     *
     * @return true if the active message in the message exchange contains the
     *         specified header expression.
     * @param exchange - exchange to filter
     */
    public boolean matches(Object input)
        throws Exception
    {
        return (false);
    }

    /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(MessageExchange exchange)
        throws Exception
     {
        ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
        String  value = null;

        if (meProp_ != null) {
            value = (String)exchange.getProperty(meProp_);
        } else if (meOper_ != null) {
            QName   operation = exchange.getOperation();
            if (operation != null) {
                value = (String)operation.toString();
                if (!meOper_.equals("*") && !value.equals(meOper_)) {
                    value = null;
                }
            }
        } else if (meInter_ != null) {
            QName   iface = exchange.getInterfaceName();
            if (iface != null) {
                value = (String)iface.toString();
                if (!meInter_.equals("*") && !value.equals(meInter_)) {
                    value = null;
                }
            }
        } else if (svcMsg != null && nmAttach_ != null) {
            DataHandler     dh = svcMsg.getAttachment(nmAttach_);
            value = dh == null ? "" : dh.getContentType();
        }
        if (value != null) {
            if (matcher_ != null) {
                matcher_.reset(value);
                if (matcher_.find()) {
                    return (matcher_.group(matcher_.groupCount() > 0 ? 1 : 0));
                }
            }
        }
        return null;
     }

   /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(Object input)
        throws Exception
     {
         return (null);
     }

}
