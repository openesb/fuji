/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FilterCollection.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.contentroute;

import com.sun.jbi.clientservices.eip.EIPConfigKeys;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.interceptors.internal.flow.contentroute.storage.FileStorage;
import javax.jbi.messaging.MessageExchange;
import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.File;
import java.util.ArrayList;
import com.sun.jbi.interceptors.internal.flow.DynamicEIP;

import java.util.List;
import java.util.Properties;
import java.util.HashMap;
import com.sun.jbi.interceptors.internal.flow.MessageFilter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;


/**
 * This class implements a simple memory based set of rules with file backup.
 *
 * @author Sun Microsystems, Inc.
 */
public class FilterCollection implements MessageFilter, DynamicEIP {
    
    public FilterCollection() {
    }
    private static String       USAGE = "config-id: <name>\napp-ns: <namespace>\ntype: select\nname: <name>\nconfig: \n";
    private HashMap             templateMap_;
    private DocumentBuilder     builder_;
    private Transformer         transformer_;
    private String              name_;
    private RulesSetManager     ruleStorage_;
    private boolean             firstMatch_;
    private boolean             lastMatch_;

    /**
     * Create an Xpath filterter instance
     * @param expression - XPath expression
     */
    public FilterCollection(String name, String type, Properties config)
        throws Exception
    {
        name_ = name;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setIgnoringElementContentWhitespace(true);
        builder_ = dbf.newDocumentBuilder();

        transformer_ = TransformerFactory.newInstance().newTransformer();
        transformer_.setOutputProperty (OutputKeys.METHOD, "xml");

        File directory = Environment.getFrameworkBundleContext().getDataFile(
                "cbr" + File.separator + name);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String  match = (String)config.get("match");
        firstMatch_ = true;
        lastMatch_ = false;
        if (match != null) {
            if (match.equals("last")) {
                firstMatch_ = false;
                lastMatch_ = true;
            }
            else if (match.equals("unique")) {
                firstMatch_ = false;
            }
        }
        FileStorage fs = new FileStorage(directory);
        if (config != null && config.containsKey("defaultRules")) {
             if (fs.existsName("defaultRules")) {
                 fs.removeName("defaultRules");
             }
            fs.createName("defaultRules", config.getProperty("defaultRules"));
        }
        ruleStorage_ = new RulesSetManager(fs, type);
    }

    /**
     *  @return true if the current active message in the exchange matches the
     *         filter.
     *
     * @param exchange - exchange to filter
     */
    public boolean matches(MessageExchange exchange)
        throws Exception
    {
         ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
         Document document = getPayloadDocument(svcMsg);
         RulesSetManager.Rule[]    rules = ruleStorage_.getRules();

         for (RulesSetManager.Rule rule : rules) {
             if (rule.mf_.matches(document)) {
                 return (true);
             }
         }
         return false;
    }

    /**
     *  @return true if the current active message in the exchange matches the
     *         filter.
     *
     * @param exchange - exchange to filter
     */
    public boolean matches(Object input)
        throws Exception
    {
         RulesSetManager.Rule[]    rules = ruleStorage_.getRules();

         for (RulesSetManager.Rule rule : rules) {
             if (rule.mf_.matches(input)) {
                 return (true);
             }
         }
         return false;
    }

    /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(MessageExchange exchange)
        throws Exception
     {
         ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);

         Document document = getPayloadDocument(svcMsg);

         RulesSetManager.Rule[]     rules = ruleStorage_.getRules();
         String                     toRule = null;
         String                     lastRule = null;

         for (RulesSetManager.Rule rule : rules) {
             if (rule.mf_.matches(document)) {
                 if (firstMatch_) {
                    return (rule.to_);
                 } else if (lastMatch_) {
                    lastRule = rule.to_;
                 } else if (toRule == null) {
                    toRule = rule.to_;
                 } else {
                    return null;
                 }
             }
         }
         if (lastRule != null) {
            toRule = lastRule;
         }        
         return toRule;
     }

         /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(Object input)
        throws Exception
     {
         RulesSetManager.Rule[]     rules = ruleStorage_.getRules();
         String                     toRule = null;
         String                     lastRule = null;


         for (RulesSetManager.Rule rule : rules) {
             if (rule.mf_.matches(input)) {
                 if (firstMatch_) {
                    return (rule.to_);
                 } else if (lastMatch_) {
                    lastRule = rule.to_;
                 } else if (toRule == null) {
                    toRule = rule.to_;
                 } else {
                    return null;
                 }
             }
         }
         if (lastRule != null) {
            toRule = lastRule;
         }        
         return toRule;
     }

    /**
     * @return the Document instance for the payload
     */
     private Document getPayloadDocument(ServiceMessage svcMsg)
        throws Exception
     {
        Object payload = svcMsg.getPayload();

        if ( payload instanceof Node )
        {
            Document document = null;
            Node doc = (Node) payload;

            // parse the XML as a W3C Document
            if ( doc.getChildNodes().getLength() == 0 &&
                 doc.getNodeType() == Node.TEXT_NODE )
            {
                document = builder_.parse(new InputSource(new StringReader(doc.getNodeValue())));
            }
            else if (doc.getNodeType() == Node.ELEMENT_NODE )
            {
                document = builder_.parse(new InputSource(NodeToString(doc)));
            } else if (doc.getNodeType() == Node.DOCUMENT_NODE) {
                document = (Document)doc;
            }

            return document;
        }
        else
        {
            throw new Exception("Cannot filter a non-XML message using Xpath");
        }
     }

    /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private StringReader NodeToString(Node node)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        transformer_.transform(new DOMSource(node), new StreamResult(sw));
        return new StringReader(sw.toString());
    }

    //
    //--------------------DynamicEIP------------------------------
    //
    //config-id:myConfig
    //app-ns: http:///fuji/.dev.java.net/application/my-maven-project
    //type: select
    //name: dynamic-select
    //config:
    //  - type: xpath
    //    condition: "//../...>10"
    //    to: endpoint-1
    //  - type: xpath
    //    condition: "//../...<10"
    //    to: named-route-x

    /**
    * Add to the existing configuration
    * @param config - configuration data
    */
    public void add(Object config) throws Exception {
        HashMap     map = (HashMap)config;
        String      configId =(String)map.get(EIPConfigKeys.CONFIG_ID.toString());
        //

        //  Valid, save and install the new rules.
        //
        ruleStorage_.addRuleSet(configId, map);
    }

    /**
    * Remove the specified configuration
    * @param configId - configuration id
    */
    public void remove(String configId) throws Exception {
        ruleStorage_.removeRuleSet(configId);
    }

    /**
    * Update existing configuration
    * @param config - configuration data
    */
    public void modify(Object config) throws Exception {
        HashMap     map = (HashMap)config;
        String      configId =(String)map.get(EIPConfigKeys.CONFIG_ID.toString());
        ruleStorage_.modifyRuleSet(configId, map);
    }

    /**
    * Generate a configuration template which can be used to add/modify
    * a configuration. The template is specific to each eip.
    * @return
    */
    public Object generateTemplate() throws Exception {
        return getTemplateMap();
    }

   /**
    * Generate the Configuration YAML template hashmap
    * @return the yaml HashMap for the template
    */
    private HashMap getTemplateMap(){
        if ( templateMap_ == null ){
            templateMap_ = new HashMap();
            templateMap_.put(EIPConfigKeys.CONFIG_ID.toString(), "<config-id>");
            templateMap_.put(EIPConfigKeys.APP_NS.toString(),    "<namespace>");
            templateMap_.put(EIPConfigKeys.TYPE.toString(),      com.sun.jbi.fuji.ifl.Flow.SELECT.toString());
            templateMap_.put(EIPConfigKeys.NAME.toString(),      "<eip-name>");
            templateMap_.put(EIPConfigKeys.CONFIG.toString(),    "");
        }
        return templateMap_;
    }
   /**
    *
    * @param config
    * @return
    */
    public Object get(String config) throws Exception {
        Object   content = ruleStorage_.getRuleSet(config);
        return (content);
    }

    /**
    * List identifiers for all existing configuration
    */
    public List<String> listConfigs() throws Exception {
        List<String>    list = new ArrayList();

        for (String config : ruleStorage_.listRuleSets()) {
            list.add(config);
        }
        return (list);
    }
}

