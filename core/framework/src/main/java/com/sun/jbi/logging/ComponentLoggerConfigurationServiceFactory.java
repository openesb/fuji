/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLoggerConfigurationServiceFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;

import java.util.HashMap;

import org.osgi.framework.BundleContext;

import com.sun.jbi.configuration.Constants;
import com.sun.jbi.framework.osgi.internal.ComponentContext;

/**
 * This is a factory class to handout instances of ComponentLoggerConfigurationService.
 * The instances created are tracked by the factory and come in handy when the 
 * configurations of all of these services has to be refreshed, in cases
 * when the log level of the jbi root level logger is modified.
 * 
 */
public class ComponentLoggerConfigurationServiceFactory {
    
    
    /**
     * list of ComponentLoggerConfigurationService instances
     */
    private static HashMap<String, ComponentLoggerConfigurationService>
            componentLogConfigServiceList_ = new HashMap<String, ComponentLoggerConfigurationService>();
    
    /**
     * This method is used to get an instance of ComponentLoggerConfigurationService.
     * @param bundleCtx component bundle context
     * @param componentCtx component context
     */
    public static ComponentLoggerConfigurationService 
    getComponentLoggerConfigurationService(ComponentContext componentCtx) 
    {
        ComponentLoggerConfigurationService compLogConfig = new ComponentLoggerConfigurationService(componentCtx);
        componentLogConfigServiceList_.put(componentCtx.getComponentName(), compLogConfig);
        return compLogConfig;
    }
     
    /**
     * This method is used to provide a list of the ComponentLogConfigServices maintained by the
     * factory.
     */
     public static HashMap<String, ComponentLoggerConfigurationService> getComponentLoggerConfigurationServices(){
         return componentLogConfigServiceList_;
     }
     
     /**
      * This method is used to remove an instance of ComponentLoggerConfigurationService 
      * from the list of services tracked. This is invoked by the component manager
      * when a component is unregistered.
      */
     public static void removeComponentLoggerConfigurationService(String componentName) {
         
         
         if (componentLogConfigServiceList_ != null) {
            ComponentLoggerConfigurationService compLogService =  componentLogConfigServiceList_.get(componentName);
            if (compLogService != null) {
                compLogService.deleteConfigurations();
                componentLogConfigServiceList_.remove(componentName);
            }
         } 
                
     }
}
