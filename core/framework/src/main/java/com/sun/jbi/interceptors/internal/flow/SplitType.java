/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitType.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

/**
 * Enum for split types.
 * 
 * @author Sun Microsystems, Inc.
 */
public enum SplitType { 
    XPATH ("xpath"),
    XSL   ("xsl"),
    JAVA  ("java"),
    REGEX ("regex"),
    HEADER ("header");
    
    private String type_;
    
    SplitType(String type) {
        type_ = type;
    }
    
    @Override
    public String toString() {
        return type_;
    }
    
    public static SplitType fromString(String type) {
        if (type.equalsIgnoreCase(XPATH.type_)) {
            return XPATH;
        } 
        else if (type.equalsIgnoreCase(REGEX.type_)) {
            return REGEX;
        } 
        else if (type.equalsIgnoreCase(JAVA.type_)) {
            return JAVA;
        } 
        else if (type.equalsIgnoreCase(XSL.type_)) {
            return XSL;
        } 
        else {
            throw new IllegalArgumentException(type);
        }  
    }
}
