/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RulesSetManager.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.contentroute;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Properties;
import com.sun.jbi.fuji.ifl.IFLUtil;
import com.sun.jbi.interceptors.internal.flow.FilterType;
import com.sun.jbi.interceptors.internal.flow.MessageFilter;
import com.sun.jbi.interceptors.internal.flow.HeaderFilter;
import com.sun.jbi.interceptors.internal.flow.RegexFilter;
import com.sun.jbi.interceptors.internal.flow.XpathFilter;


/**
 * This class implements a simple memory based set of rules with file backup.
 *
 * @author Sun Microsystems, Inc.
 */
public class RulesSetManager {
    Storage                                 store_;
    Rule[]                                  rules_;
    Map<String,RuleSet>                     ruleSets_;
    String                                  type_;
    
    public RulesSetManager(Storage store, String type) {
        store_ = store;
        ruleSets_ = new HashMap();
        type_ = type;
        loadRulesets();
        rebuild();

    }

    void loadRulesets() {
        String[]    rules = store_.listNames();

        try
        {
            for (String rule : rules) {
                Map ruleObject = (Map)store_.getName(rule);
                loadRuleSet(ruleObject);
            }
        } catch (java.io.IOException iEx) {
        }
    }

    void rebuild() {
        RuleSet[]   ruleSets = (RuleSet[])ruleSets_.values().toArray(new RuleSet[0]);
        int         size = 0;
        Rule[]      rules = null;

        if (ruleSets.length > 0) {
            Arrays.sort(ruleSets, 0, ruleSets.length);
            for (RuleSet rs : ruleSets) {
                if (rs.enabled_) {
                    size += rs.rules_.length;
                }
            }
            rules = new Rule[size];
            size = 0;
            for (RuleSet rs : ruleSets) {
                if (rs.enabled_) {
                    System.arraycopy(rs.rules_, 0, rules, size, rs.rules_.length);
                    size += rs.rules_.length;
                }
            }
            rules_ = rules;
        }
    }

    private void loadRuleSet(Map set) {
        String          config = (String)set.get("config-id");
        Object          priorityValue = set.get("priority");
        Object          enabledValue = set.get("enabled");
        List<HashMap>   configs = (List)set.get("config");
        Rule[]          rules;
        RuleSet         rs;

        if (enabledValue != null) {
            if (enabledValue instanceof String) {
                String enabled = (String)enabledValue;
                if (enabled.equalsIgnoreCase("true") ||
                    enabled.equalsIgnoreCase("yes"))
                {
                    enabledValue = true;
                } else if (enabled.equalsIgnoreCase("false") ||
                    enabled.equalsIgnoreCase("no")) {
                    enabledValue = false;
                } else {
                    throw new IllegalArgumentException("Enabled must be one of {true,yes,false,no}.");
                }
            }
        } else {
            enabledValue = true;
        }
        if (enabledValue != null && !(enabledValue instanceof Boolean)) {
            throw new IllegalArgumentException("Enabled must be a boolean.");
        }
        if (priorityValue != null)
        {
            if (priorityValue instanceof String) {
                priorityValue = Double.valueOf((String)priorityValue);
            }
            if (priorityValue instanceof Integer) {
                priorityValue = ((Integer)priorityValue).doubleValue();
            }
            if (priorityValue instanceof Long) {
                priorityValue = ((Long)priorityValue).doubleValue();
            }
            if (!(priorityValue instanceof Double)) {
                throw new IllegalArgumentException("Priority must be a number.");
            }
        } else {
            priorityValue = 100.0;
        }
        rules = buildRules(configs, (String)set.get("app-ns"));
        rs = new RuleSet(config, (Double)priorityValue, (Boolean)enabledValue, rules);
        ruleSets_.put(config, rs);
    }

    private Rule[] buildRules(List<HashMap> rules, String namespace) {
        LinkedList<Rule>                        result = new LinkedList();
        String                                  type = null;
        String                                  condition;
        String                                  to;

        if (rules == null) {
            return (new Rule[0]);
        }
        
        try {
            for (HashMap rule : rules) {
                MessageFilter  mf = null;

                //
                //  Collect arguments.
                //
                condition = (String)rule.get("condition");
                to = (String)rule.get("to");
                type = (String)rule.get("type");

                //
                //  Validate arguments.
                //
                if (condition == null || to == null) {
                    throw new IllegalArgumentException(
                            "Each filter rule requires 2 values: condition and to.");
                }
                if (type == null) {
                    if (type_.equals("any")) {
                        throw new IllegalArgumentException(
                            "Each filter rule requires 3 values: condition, type and to.");
                    } else {
                        type = type_;
                        if (rule.size() != 2) {
                            throw new IllegalArgumentException(
                                    "Extra value found, only condition and to are required.");
                        }
                   }
                } else {
                    if (!type_.equals("any")) {
                        if (!type.equals(type_)) {
                             throw new IllegalArgumentException(
                                    "If type is specified it must match filter definition value of (" + type_+")");
                        }
                    }
                }

                //
                //  Create the requested filter.
                //
                Properties config = IFLUtil.parseNVpairs(condition, true);
                if (type.equals(FilterType.REGEX.toString())) {
                    mf = new RegexFilter("", config);
                } else if (type.equals(FilterType.XPATH.toString())) {
                    mf = new XpathFilter("", namespace, config);
                } else if (type.equals(FilterType.HEADER.toString())) {
                    mf = new HeaderFilter("", config);
                } else {
                    throw new IllegalArgumentException("Filter type (" + type + ") not supported.");
                }

                //
                //  Add rule to set.
                //
                result.addLast(new Rule(mf, to));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Error extracting rule or compiling expression type (" + type + ") :" + e);
        }
        return (result.toArray(new Rule[0]));
    }

    public void addRuleSet(String setName, HashMap rules) throws Exception {
        if (setName.equals("defaultRules")) {
            throw new java.lang.IllegalArgumentException("Ruleset 'defaultRules' is predefined any can't be added.");
        }
        if (ruleSets_.get(setName) == null) {
            loadRuleSet(rules);
            store_.addName(setName, rules);
            rebuild();
        } else {
            throw new java.lang.IllegalArgumentException("Ruleset named '" + setName + "' already exists.");
        }
    }

    public void removeRuleSet(String setName) throws Exception {
        if (setName.equals("defaultRules")) {
            throw new java.lang.IllegalArgumentException("Ruleset 'defaultRules' is predefined any can't be removed.");
        }
        if (ruleSets_.get(setName) != null) {
            store_.removeName(setName);
            ruleSets_.remove(setName);
            rebuild();
        } else {
            throw new java.lang.IllegalArgumentException("Ruleset named '" + setName + "' not found.");
        }
    }

    public void modifyRuleSet(String setName, HashMap rules) throws Exception {
        if (ruleSets_.get(setName) != null) {
            if (rules.get("config") == null) {
                Object          enabledValue = rules.get("enabled");
                Object          priorityValue = rules.get("priority");
                double          priority = 0.0;
                HashMap         oldRules = (HashMap)getRuleSet(setName);

                if (enabledValue != null) {
                    if (enabledValue instanceof String) {
                        String  enabled = (String)enabledValue;
                        if (enabled.equalsIgnoreCase("true") ||
                            enabled.equalsIgnoreCase("yes"))
                        {
                            enabledValue = true;
                        } else if (enabled.equalsIgnoreCase("false") ||
                            enabled.equalsIgnoreCase("no")) {
                            enabledValue = false;
                        } else {
                        throw new IllegalArgumentException("Enabled must be one of {true,yes,false,no}.");
                        }
                        oldRules.put("enabled", enabledValue);
                    }
                    oldRules.put("enabled", enabledValue);
                }


                if (enabledValue != null && !(enabledValue instanceof Boolean)) {
                    throw new IllegalArgumentException("Enabled must be a boolean.");
                }
                if (priorityValue != null)
                {
                    if (priorityValue instanceof String) {
                        priorityValue = Double.valueOf((String)priorityValue);
                    }
                    else if (priorityValue instanceof Integer) {
                        priorityValue = ((Integer)priorityValue).doubleValue();
                    }
                    else if (priorityValue instanceof Long) {
                        priorityValue = ((Long)priorityValue).doubleValue();
                    }
                    else if (!(priorityValue instanceof Double)) {
                        throw new IllegalArgumentException("Priority must be a number.");

                    }
                    oldRules.put("priority", priorityValue);
                }
                rules = oldRules;
            }

            store_.removeName(setName);
            ruleSets_.remove(setName);
            loadRuleSet(rules);
            store_.addName(setName, rules);
            rebuild();
        } else {
            throw new java.lang.IllegalArgumentException("Ruleset named '" + setName + "' not found.");
        }

    }

    public String[] listRuleSets() {
        RuleSet[]   ruleSets = (RuleSet[])ruleSets_.values().toArray(new RuleSet[0]);
        String[]    setNames = new String[ruleSets.length];
        int         i = 0;

        Arrays.sort(ruleSets, 0, ruleSets.length);
        for (RuleSet rule : ruleSets) {
            setNames[i++] = rule.name_;
        }
        return (setNames);
    }

    public Object getRuleSet(String setName) throws Exception {
       if (ruleSets_.get(setName) != null) {
          return (store_.getName(setName));
        } else {
            throw new java.lang.IllegalArgumentException("Ruleset named '" + setName + "' not found.");
        }
    }

    public Rule[]	getRules() {
        return (rules_);
    }

    public class Rule {
        public final MessageFilter             mf_;
        public final String                    to_;

        Rule (MessageFilter mf, String to) {
            mf_ = mf;
            to_ = to;
        }
    }


    class RuleSet implements Comparable {
        String      name_;
        double      priority_;
        boolean     enabled_;
        Rule[]      rules_;

        RuleSet (String name, Double priority, Boolean enabled, Rule[] rules) {
            name_ = name;
            priority_ = priority;
            enabled_ = enabled;
            rules_ = rules;
        }
        public int compareTo(Object to) {
            if (to instanceof RuleSet) {
                double   other = ((RuleSet)to).priority_;
                if (priority_ > other) {
                    return (-1);
                } else if (priority_ < other) {
                    return (1);
                }
            }
            return (0);
        }
    }
}
 

