/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Tee.java
 *
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.service.ServiceMessageImpl;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.interceptors.Intercept;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;


/**
 * This class provides support for message tee in a IFL route definition.
 * @author Sun Microsystems, Inc.
 */
public class Tee extends AbstractFlow  {

    // Service that is the destination of this message.
    private String      teeEndpoint_;
    private String      teeName_;

    // Log ref
    private Logger      log_ = Logger.getLogger(Tee.class.getPackage().getName());
    private boolean     enabled_ = true;


    //
    //  Used to support XML annotated with MessageExchange information.
    //
    private boolean annotate_;
    private boolean exchangeProperties_;
    private String  mePropPrefix_;
    private boolean messageProperties_;
    private String  nmPropPrefix_;
    private String  setError_;

    private DocumentBuilder builder_;
    public Tee(){};
    
    /**
     * Creates a new tee flow.
     * @param namespace to be used
     * @param teeInfo used to configure the Tee
     */
    public Tee(String name, String namespace, Map<String,Object> teeInfo)
            throws Exception {
        
        super(namespace, name, com.sun.jbi.fuji.ifl.Flow.TEE);
        Properties  config;
        String      prop;

        // register a single endpoint in the NMR endpoint registry for 
        // a tee endpoint
        teeName_ = name;
        activateEndpoint(getServiceQName(teeName_), teeName_, false);
        teeEndpoint_ = (String)teeInfo.get("TO");
        config = (Properties)teeInfo.get(Flow.Keys.CONFIG.toString());
        if (config != null) {
            prop = config.getProperty("annotate");
            if (prop != null && (prop.equals("yes") || prop.equals("true"))) {
                annotate_ = true;
                prop = config.getProperty("me.props");
                exchangeProperties_ = true;
                if (prop != null) {
                    if (prop.equals("no") || prop.equals("false")) {
                        exchangeProperties_ = false;
                    } else if (!prop.equals("yes") && !prop.equals("true")) {
                        mePropPrefix_ = prop;
                    }
                }
                prop = config.getProperty("nm.props");
                messageProperties_ = true;
                if (prop != null) {
                    if (prop.equals("no") || prop.equals("false")) {
                        messageProperties_ = false;
                    } else if (!prop.equals("yes") && !prop.equals("true")) {
                        nmPropPrefix_ = prop;
                    }
                }
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                dbf.setNamespaceAware(true);
                dbf.setIgnoringElementContentWhitespace(true);
                builder_ = dbf.newDocumentBuilder();
            }
            prop = config.getProperty("setError");
            if (prop != null) {
                setError_ = prop;
            }
        }
    }
    
    @Intercept
    public boolean onSend(MessageExchange exchange) throws Exception {
        if (enabled_) {
            // We don't do anything with DONE exchanges
            if (exchange.getStatus().equals(ExchangeStatus.DONE)) {
                return true;
            }

            if (exchange.getStatus().equals(ExchangeStatus.ERROR)) {
                log_.log(Level.WARNING, "Tee of exchange failed.", exchange.getError());
            }
            
            if (teeEndpoint_ != null) {
                InOnly me = getExchangeFactory().createInOnlyExchange();
                ServiceEndpoint endpoint = getEndpoint(teeEndpoint_);
                // Don't fail the exchange. Tee is fire and forget
                if (endpoint != null) {
                    me.setEndpoint(endpoint);
                    // make the interceptor exchange look like the intercepted exchange
                    copy(exchange, me, false);
                    me.setOperation(new QName(exchange.getEndpoint().getServiceName().getNamespaceURI(), "oneWay"));
                    // invoke the service
                    send(me);
                } else {
                    log_.warning("Unable to locate tee endpoint: " + teeEndpoint_);
                }
             }
        }
        if (exchange instanceof InOut) {
            exchange.setMessage(exchange.getMessage("in"), "out");
        }
        else {
            exchange.setStatus(ExchangeStatus.DONE);
        }
        
        
        return false;
                
    }
    
    protected boolean useNew() { return (true); }
    protected boolean acceptEvent(int id) { return (false); }

    protected void onRequest(AbstractFlow.InboundContext ic) {
        MessageExchange     request = ic.getExchange();

         log_.fine("EIP: Tee (" + teeName_ + "," + request.getExchangeId() + ") Request");
        try {
            if (log_.isLoggable(Level.FINER))
                log_.finer("EIP: Tee(" + teeName_ + ") Exchange: \n" + request.toString());

            //
            //  New request. Just ACK the request (Tee is fire and forget.)
            //
            try {
                if (setError_ == null) {
                    if (request instanceof InOut) {
                        log_.fine("EIP:Tee (" + teeName_ + "," + request.getExchangeId() + ") Reply");
                        request.setMessage(request.getMessage("in"), "out");
                        reply(ic);
                    } else {
                        log_.fine("EIP:Tee (" + teeName_ + "," + request.getExchangeId() + ") Done");
                        request.setStatus(ExchangeStatus.DONE);
                        status(ic);
                    }
                } else {
                    request.setError(new Exception(setError_));
                    status(ic);
                }
            } catch (Exception e) {
                request.setError(e);
                try {
                    status(ic);
                } catch (Exception ignore) {
                }
                return;
            }

            //
            //  Now tee off the request.
            //
            if (teeEndpoint_ != null) {
                InOnly me = getExchangeFactory().createInOnlyExchange();
                AbstractFlow.OutboundContext    oc = newOutboundContext(me);
                QName                           operation;

                // make the interceptor exchange look like the intercepted exchange
                copy(request, me, false);
                if (annotate_) {
                    doAnnotation(me);
                }
                me.setService(getService(teeEndpoint_));
                operation = getOperationOverride(teeEndpoint_);
                if (operation == null) {
                    operation = request.getOperation();
                }
                me.setOperation(new QName(namespace_, "oneWay"));

//                //
//                //  Set the input type.
//                //
//                ServiceMessageImpl      message = (ServiceMessageImpl)MessageExchangeUtil.getInMessage(request);
//                QName   messageType = message.getMessageType();
//                QName   serviceType = oc.getInputType();
//                if (!messageType.equals(serviceType)) {
//                    if (messageType.equals(ServiceMessageImpl.ANYTYPE)) {
//                        message.setMessageType(serviceType);
//                    }
//                }

                // invoke the service
                log_.fine("EIP:Tee (" + teeName_ + "," + request.getExchangeId() + ") Tee(" +
                    me.getExchangeId() + ") STARTED");
                try {
                    request(oc);
                } catch (javax.jbi.messaging.MessagingException ignore) {
                    return;
                }
            }

        } catch (javax.jbi.messaging.MessagingException e) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            e.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP:Tee (" + teeName_ + ") Exception:" + b.toString() + toString());
        }
    }

    protected void onStatus(AbstractFlow.OutboundContext oc) {
        log_.fine("EIP: Tee (" + teeName_ + "," +
                oc.getExchange().getExchangeId() + ") Child Complete");
    }

    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: Tee (" + teeName_ + "," + ic.getExchange().getExchangeId() + ") Complete");
    }

    void doAnnotation(MessageExchange me) {
        try {
            ServiceMessage      svcMsg = MessageExchangeUtil.getInMessage(me);
            ServiceMessage      newMsg = MessageExchangeUtil.getPrototype(me, svcMsg);
            Document            document = getClonedPayloadDocument(svcMsg);
            Element             element = document.getDocumentElement();
            Node                node = element.getFirstChild();

            Comment             comment;
            element.insertBefore(document.createTextNode("\n"), node);
            comment = document.createComment(" MessageExchangeId: " + me.getExchangeId() + " ");
            element.insertBefore(comment, node);
            element.insertBefore(document.createTextNode("\n"), node);
            if (exchangeProperties_) {
                element.insertBefore(comment, node);
                element.insertBefore(document.createTextNode("\n"), node);
                for (Object prop: me.getPropertyNames()) {
                    if (mePropPrefix_ == null ||
                            ((String)prop).startsWith(mePropPrefix_)) {
                        comment = document.createComment("   MEprop: " + prop.toString() + " ");
                        element.insertBefore(comment, node);
                        element.insertBefore(document.createTextNode("\n"), node);
                        comment = document.createComment("    Value: " + me.getProperty((String)prop).toString() + " ");
                        element.insertBefore(comment, node);
                        element.insertBefore(document.createTextNode("\n"), node);
                    }
                }
            }
            if (messageProperties_) {
                element.insertBefore(comment, node);
                element.insertBefore(document.createTextNode("\n"), node);
                for (Object prop: svcMsg.getPropertyNames()) {
                    if (nmPropPrefix_ == null ||
                            ((String)prop).startsWith(nmPropPrefix_)) {
                        comment = document.createComment("   NMprop: " + prop.toString() + " ");
                        element.insertBefore(comment, node);
                        element.insertBefore(document.createTextNode("\n"), node);
                        comment = document.createComment("    Value: " + svcMsg.getProperty((String)prop).toString() + " ");
                        element.insertBefore(comment, node);
                        element.insertBefore(document.createTextNode("\n"), node);
                    }
                }
            }
            newMsg.setPayload(document.getDocumentElement());
            me.setMessage(newMsg, "in");
        } catch (Exception ignore) {

        }
    }

    /**
     * @return the Document instance for the payload
     */
     private Document getClonedPayloadDocument(ServiceMessage svcMsg)
        throws Exception
     {
        Object payload = svcMsg.getPayload();

        if ( payload instanceof Node )
        {
            Document document = null;
            Node doc = (Node) payload;

            // parse the XML as a W3C Document
            if ( doc.getChildNodes().getLength() == 0 &&
                 doc.getNodeType() == Node.TEXT_NODE )
            {
                document = builder_.parse(new InputSource(new StringReader(doc.getNodeValue())));
            }
            else if (doc.getNodeType() == Node.ELEMENT_NODE )
            {
                document = builder_.newDocument();
                document.appendChild(document.importNode(doc, true));
            } else if (doc.getNodeType() == Node.DOCUMENT_NODE) {
                document = (Document)doc;
            }

            return document;
        }
        else
        {
            throw new Exception("Cannot extract a non-XML message");
        }
     }

     public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        Tee (" + teeName_ + ")\n");
        return (sb.toString());
    }
}
