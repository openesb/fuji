/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExternalEndpoint.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.events;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.xml.namespace.QName;
import org.osgi.service.event.Event;

public class ExternalEndpointEvent
        extends AbstractEvent 
        implements java.io.Serializable
{
   public static final String ADDED_EVENT_TOPIC = "com/sun/jbi/messaging/events/ExternalEndpointEvent/ADD";
   public static final String REMOVED_EVENT_TOPIC = "com/sun/jbi/messaging/events/ExternalEndpointEvent/REMOVE";
   public static final String ENDPOINT = "endpoint";
   public static final String SERVICE  = "service";
   private QName        _serviceName;
   private String       _endpointName;
   private ACTION       _action;
   
    static public boolean isExternalEndpointEvent(org.osgi.service.event.Event event)
    {
        String	topic = (String)event.getTopic();

	if (topic.equals(ADDED_EVENT_TOPIC) || topic.equals(REMOVED_EVENT_TOPIC))
        {
	    return (true);
        }
        return (false);
    }
    
    static public ExternalEndpointEvent valueOf(org.osgi.service.event.Event event)
    {
        ExternalEndpointEvent   ee = new ExternalEndpointEvent();
        
        ee._serviceName = (QName)event.getProperty(SERVICE);
        ee._endpointName = (String)event.getProperty(ENDPOINT);
        ee._owner = (String)event.getProperty(OWNER);
        if (event.getTopic().equals(ADDED_EVENT_TOPIC))
        {
            ee._action = ACTION.ADDED;
        }
        else
        {
            ee._action = ACTION.REMOVED;
        }
        return (ee);
    }
    
    static public ExternalEndpointEvent valueOf(Dictionary props)
    {
        ExternalEndpointEvent   ee = new ExternalEndpointEvent();
        
        ee._serviceName = (QName)props.get(SERVICE);
        ee._endpointName = (String)props.get(ENDPOINT);
        ee._owner = (String)props.get(OWNER);
        return (ee);
    }
    
    static public ExternalEndpointEvent valueOf(ACTION action, String owner, QName service, String endpoint)
    {
        ExternalEndpointEvent   ee = new ExternalEndpointEvent();
        
        ee._owner = owner;
        ee._serviceName = service;
        ee._endpointName = endpoint;
        ee._action = action;
        return (ee);
    }
    
    public Event getEvent()
    {
        Event       e;
        Dictionary  d = (Dictionary)new Hashtable();
        
        d.put(SERVICE, _serviceName);
        d.put(ENDPOINT, _endpointName);
        d.put(OWNER, _owner);
        e = new Event(_action == ACTION.ADDED 
                ? ADDED_EVENT_TOPIC : REMOVED_EVENT_TOPIC, d);
        return (e);                
    }

    public AbstractEvent getOpposite()
    {
        ExternalEndpointEvent  event = 
                ExternalEndpointEvent.valueOf(_action == ACTION.ADDED ?
                    ACTION.REMOVED : ACTION.ADDED, _owner, _serviceName, _endpointName);
        return (event);
    }

    public Dictionary getProperties()
    {
        return (null);
    }
    
    public ACTION getAction()
    {
        return (_action);
    }
    
    public QName getServiceName()
    {
        return (_serviceName);
    }
    
    public String getEndpointName()
    {
        return (_endpointName);
    }
}

