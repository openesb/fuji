/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Link.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

/** Typesafe enumeration containing link types.
 *
 * @author JSR208 Expert Group
 */
public final class Link
{    
    /** Indicates that the provided service endpoint must be routed according 
     *  to standard normalized message routing rules.
     */
    public static final Link STANDARD = new Link("standard");
    
    /** Indicates that the provided service endpoint name must match a service 
     *  providerís service endpoint name; indirect connections are not allowed.
     */
    public static final Link HARD = new Link("hard");
    
    /** Indicates that the provided service endpoint must not match a service 
     *  providerís service endpoint name; rather, it must match an indirect 
     *  connection name.
     */
    public static final Link SOFT  = new Link("soft");
    
    /** String representation of link. */
    private String mLink;
    
    /** Private constructor used to create a new Link type.
     *  @param status value
     */
    private Link(String link)
    {
        mLink = link;
    }
    
    /** Returns string value of enumerated type.
     *  @return String representation of status value.
     */
    public String toString()
    {
        return mLink;
    }
    
    /** Equality test.
     * @param status value to be compared for equality
     * @return boolean result of test.
     */
    public boolean equals(Link link)
    {
        return (mLink.equals(link.mLink));
    }
    
    /** Returns instance of Link that corresponds to given string.
     *  @param status string value of status
     *  @return ExchangeStatus 
     *  @throws java.lang.IllegalArgumentException if string can't be translated
     */
    public static Link valueOf(String link)
    {
        Link instance;
        
        //
        //  Convert symbolic name to object reference.
        //
        if (link.equalsIgnoreCase(HARD.toString()))
        {
            instance = HARD;
        }
        else if (link.equalsIgnoreCase(SOFT.toString()))
        {
            instance = SOFT;
        }
        else if (link.equalsIgnoreCase(STANDARD.toString()))
        {
            instance = STANDARD;
            
        }
        else
        {
            // Someone has a problem.
            throw new java.lang.IllegalArgumentException(link);
        }
       
        return (instance);
    }
    
    /** Returns hash code value for this object.
     *  @return hash code value
     */
    public int hashCode()
    {
        return mLink.hashCode();
    }
}
