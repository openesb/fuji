/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegexSplitter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.eip.split.Splitter;
import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.message.MessageProps;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import javax.xml.namespace.NamespaceContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Interface for a splitter
 * 
 * @author Sun Microsystems, Inc.
 */
public class RegexSplitter 
    extends org.glassfish.openesb.api.eip.split.BaseSplitter
    implements org.glassfish.openesb.api.eip.split.Splitter{ 
                   
    Pattern             pattern_;
    String              expression_;
    
    /**
     * Create a Regex Splitter instance
     * @param expression - regex 
     */
    public RegexSplitter()
    {
        
    }
    
        /**
     * Set the namespace context
     *
    public void setNamespaceContext(NamespaceContext nsCtx)
        throws Exception{
    }
    
    /**
     * Set Configuration
     *
    public void setConfiguration(HashMap config)
        throws Exception{
        super.setConfiguration(config);
        expression_ = (String)config_.get("exp");
        pattern_   = Pattern.compile(expression_);
    }*/
    
    /**
     * 
     * @return the Correlation ID from the message
     *
    public Object getCorrelationID(ServiceMessage message){
        return (String)message.getProperty(MessageProps.GROUP_ID.toString());
    }*/
    
    /**
     * Split the current active message in the exchange and return the split  
     * documents in a ArrayList
     * 
     * @param svcMsg - message to split
     */
    public ArrayList split(ServiceMessage svcMsg)
        throws Exception
    {
        
        expression_ = (String)config_.get("exp");
        pattern_   = Pattern.compile(expression_);
        Object payload = svcMsg.getPayload();
        
        if ( payload instanceof String )
        {
            String[] docs = pattern_.split((String)payload);
            ArrayList splitDocs = new ArrayList();
            for(int i=0; i<docs.length; i++ )
            {
                splitDocs.add(expression_ + docs[i]);
            }
            return splitDocs;
        }
        else
        {
            throw new Exception("Cannot split a non-text message using Regex");
        }
    }
    
    /**
     * Create a Source instance from the object passed in. The object here is
     * of the same type as returned by split.
     * 
     * @param doc - the Object instance to be converted to a Source
     * @throws Exception
     */
    public Object createPayload(Object doc)
        throws Exception
     {
         if ( doc instanceof String )
         {
            return doc;
         }
         else
         {
             throw new Exception("Unknown document object " + doc);
         }
         
     }
    
}
