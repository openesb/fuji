/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalizedMessage.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;

/**
 * Object model for <code>msg-loc-info</code> element in a JBI 
 * management result message.
 * @author kcbabo
 */
public class LocalizedMessage extends ResultElement {

//    public static final String ELEMENT_NAME = "msg-loc-info";
//    
//    private static final String TOKEN =
//            "loc-token";
//    private static final String MESSAGE =
//            "loc-message";
//    private static final String PARAM =
//            "loc-param";

    public LocalizedMessage(Element element) {
        super(element);
    }

    public String getToken() {
        return getChildElement(TaskElem.loc_token).getTextContent();
    }

    public String getMessage() {
        return getChildElement(TaskElem.loc_message).getTextContent();
    }
    
    public void setToken(String token) {
        getChildElement(TaskElem.loc_token).setTextContent(token);
    }
    
    public void setMessage(String message) {
        getChildElement(TaskElem.loc_message).setTextContent(message);
    }

    public List<String> getParameters() {
        ArrayList<String> parms = new ArrayList<String>();
        for (Element e : getChildElements(TaskElem.loc_param)) {
            parms.add(e.getTextContent());
        }
        
        return parms;
    }
    
    public void addParameter(String parameter) {
        createChildElement(TaskElem.loc_param).setTextContent(parameter);
    }

    @Override
    protected void initialize() {
        createChildElement(TaskElem.loc_token);
        createChildElement(TaskElem.loc_message);
    }
    
}
