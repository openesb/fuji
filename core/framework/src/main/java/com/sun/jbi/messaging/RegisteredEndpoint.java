/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegisteredEndpoint.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.WSDLHelper;
import com.sun.jbi.messaging.stats.METimestamps;
import com.sun.jbi.messaging.stats.Value;
import com.sun.jbi.messaging.stats.Value.Kind;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import java.util.concurrent.atomic.AtomicLong;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

import org.xml.sax.EntityResolver;

/** Abstract base class representing WSDL endpoint registered with the NMS.  
 * Specific endpoint types (dynamic, internal, external, mapped) extend this
 * class to address specific endpoint behavior.
 * @author Sun Microsystems, Inc.
 */
public abstract class RegisteredEndpoint 
        implements ServiceEndpoint, EndpointStatistics
{
    /******************** ENDPOINT TYPES ***********************/
    
    // provided by component when resolving an endpoint reference
    public static final int DYNAMIC    = 10;
    // registered using traditional NMR activation methods
    public static final int INTERNAL    = 20;
    // external endpoints
    public static final int EXTERNAL    = 30;
    // service connection
    public static final int LINKED      = 40;
    
    /** Qualified name of the service. */
    private QName   mService;
    /** Local name of the endpoint. */
    private String  mEndpoint;
    /** Id of the component which registered this endpoint. */
    String  mOwnerId;
    /** Endpoint status. */
    private boolean mActive;
    /** Operations available for this endpoint. */
    private HashMap mOperations;
    /** Interfaces implemented by this endpoint. */
    private QName[] mInterfaces;

    /** Primary statistics for external use.    */
    long                mActivationTimestamp;
    AtomicLong          mActiveExchanges;
    long                mSendRequest;
    long                mReceiveReply;
    long                mReceiveRequest;
    long                mSendReply;
    long                mSendFault;
    long                mReceiveFault;
    long                mLastFaultTime;
    long                mSendDONE;
    long                mLastDONETime;
    long                mReceiveDONE;
    long                mSendERROR;
    long                mLastERRORTime;
    long                mReceiveERROR;
    Value               mResponseTime;
    Value               mChannelTime;
    Value               mNMRTime;
    Value               mComponentTime;
    Value               mStatusTime;
    
    /** Creates a new RegisteredEndpoint.
     *  @param service service name
     *  @param endpoint endpoint name
     *  @param ownerId id of component which is registering the service
     *  @param type endpoint type
     */
    public RegisteredEndpoint(QName service, String endpoint, String ownerId)
    {
        mOwnerId    = ownerId;
        mService    = service;
        mEndpoint   = endpoint;
        mActive     = true;
        mResponseTime = new Value(Kind.INTERVAL_NS);
        mNMRTime = new Value(Kind.INTERVAL_NS);
        mComponentTime = new Value(Kind.INTERVAL_NS);
        mChannelTime = new Value(Kind.INTERVAL_NS);
        mStatusTime = new Value(Kind.INTERVAL_NS);
        mActivationTimestamp = System.currentTimeMillis();
        mActiveExchanges = new AtomicLong();
}    
        
    /** Returns the local name of the registered endpoint.
     *  @return qualified name of service.
     */
    public String getEndpointName()
    {
        return mEndpoint;
    }
    
    /** Returns the qualified name of the service offered at this endpoint.
     *  @return qualified name of service.
     */
    public QName getServiceName()
    {
        return mService;
    }
    
    public String getOwnerId()
    {
        return mOwnerId;
    }
      
    /** Indicates whether this reference is active.
     *  @return true if the reference is active, false otherwise.
     */
    public boolean isActive()
    {
        return mActive;
    }
    
    /** Indicates the type of endpoint: INTERNAL, EXTERNAL, or DYNAMIC.
     */
    public abstract int getType();
    
    /** Specifies whether the reference is active.
     *  @param isActive true for yes, false for no
     */
    public void setActive(boolean isActive)
    {
        mActive = isActive;
    }
    
    public HashMap getOperations()
    {
        if (mOperations == null)
        {
            EndpointRegistry.getInstance().getInterfacesForEndpoint(this);
        }
        return (mOperations);
    }
    
    public void setOperations(HashMap map)
    {
        mOperations = map;
    }
    
    public QName[] getInterfaces()
    {
        if (mInterfaces == null)
        {
            EndpointRegistry.getInstance().getInterfacesForEndpoint(this);
        }
        return (mInterfaces);
    }
    
    QName[] getInterfacesInternal()
    {
        return (mInterfaces);
    }
    
    public void setInterfaces(QName[] interfaces)
    {
        mInterfaces = interfaces;
    }
    
    public boolean isInternal()
    {
        return getType() == RegisteredEndpoint.INTERNAL;
    }
    
    public boolean isExternal()
    {
        return getType() == RegisteredEndpoint.EXTERNAL;
    }
    
    public boolean isLinked()
    {
        return getType() == RegisteredEndpoint.LINKED;
    }
    
    public boolean isDynamic()
    {
        return getType() == RegisteredEndpoint.DYNAMIC;
    }
    
    /** Returns true if this endpoint implements the specified interface. */
    public boolean implementsInterface(QName interfaceName)
    {
        boolean implemented = false;
        
        for (QName in : getInterfaces())
        {
            if (in.equals(interfaceName))
            {
                implemented = true;
                break;
            }
        }
        
        return implemented;
    }
     
    /** Parse the endpoint descriptor for operation and interface details and
     *  load them into the endpoint reference.
     */
    public void parseDescriptor(Document descriptor, EntityResolver resolver)
        throws javax.jbi.messaging.MessagingException
    {
        List<QName>         interfaces = new LinkedList();

        /** The descriptor should never be null, but this is a new requirement and
         *  some components may not be up-to-date yet.  In the future, we should
         *  throw an exception in this case.
         */
        mOperations = new HashMap();
        if (descriptor != null)
        {
            WSDLHelper.getOperationsAndInterfaces(descriptor, mService, resolver, mOperations, interfaces);
        }
        mInterfaces = interfaces.toArray(new QName[interfaces.size()]);
    }
    
    /**
     * <b>ExternalEndpoint implementation should override this method. </b>
     * <br><br>
     * Get a reference to this endpoint, using an endpoint reference vocabulary
     * that is known to the provider.
     * @param operationName the name of the operation to be performed by a
     * consumer of the generated endpoint reference. Set to <code>null</code>
     * if this is not applicable.
     * @return endpoint reference as an XML fragment; <code>null</code> if the
     * provider does not support such references.
     */
    public DocumentFragment getAsReference(QName operationName)
    {
        DocumentFragment doc = null;
        
        try
        {
            doc = EndpointReference.asReference(this);
        }
        catch (javax.jbi.messaging.MessagingException ignore)
        {
        }
        
        return doc;
    }
    
    
   //-------------------------EndpointStatistics-------------------------------

    public String getName()
    {
        return (toExternalName());
    }

    /**
     * List of item names for CompositeData construction.
     */
    private static final int  ITEMS_BASE = 14;
    private static final int  ITEMS_EXTRA = 16;
    private static final String[] ITEM_NAMES = {
        "OwningChannel",
        "ActivationTimestamp",
        "ActiveExchanges",
        "ReceiveRequest",
        "SendReply",
        "SendFault",
        "ReceiveFault",
        "LastFaultTime",
        "SendDONE",
        "ReceiveDONE",
        "LastDONETime",
        "SendERROR",
        "ReceiveERROR",
        "LastERRORTime",
        "ResponseTimeMin (ns)",
        "ResponseTimeAvg (ns)",
        "ResponseTimeMax (ns)",
        "ResponseTimeStd (ns)",
        "NMRTimeMin (ns)",
        "NMRTimeAvg (ns)",
        "NMRTimeMax (ns)",
        "NMRTimeStd (ns)",
        "ComponentTimeMin (ns)",
        "ComponentTimeAvg (ns)",
        "ComponentTimeMax (ns)",
        "ComponentTimeStd (ns)",
        "ChannelTimeMin (ns)",
        "ChannelTimeAvg (ns)",
        "ChannelTimeMax (ns)",
        "ChannelTimeStd (ns)"
    };

    /**
     * List of descriptions of items for CompositeData construction.
     */
    private static final String ITEM_DESCRIPTIONS[] = {
        "Owning DeliveryChannel",
        "Activation Timestamp (ms)",
        "Active Exchanges",
        "Number of requests received",
        "Number of replies sent",
        "Number of faults sent",
        "Number of faults received",
        "Timestamp of last fault",
        "Number of DONE requests sent",
        "Number of DONE requests received",
        "Timestamp of last DONE",
        "Number of ERROR requests sent",
        "Number of ERROR requests received",
        "Timestamp of last ERROR",
        "Response Time Min",
        "Response Time Avg",
        "Response Time Max",
        "Response Time Std",
        "NMR Time Min",
        "NMR Time Avg",
        "NMR Time Max",
        "NMR Time Std",
        "Component Time Min",
        "Component Time Avg",
        "Component Time Max",
        "Component Time Std",
        "Channel Time Min",
        "Channel Time Avg",
        "Channel Time Max",
        "Channel Time Std"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType ITEM_TYPES[] = {
         SimpleType.STRING,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG
    };

    public CompositeData        getStatistics()
    {
        try
        {
            Object      values[];
            String      names[];
            String      descs[];
            OpenType    types[];
            boolean     enabled = // EndpointRegistry.getInstance().statisticsEnabled() ||
                                  mChannelTime.getCount() != 0;

            if (enabled)
            {
                values = new Object[ITEMS_BASE + ITEMS_EXTRA];
                types = ITEM_TYPES;
                names = ITEM_NAMES;
                descs = ITEM_DESCRIPTIONS;
            }
            else
            {
                values = new Object[ITEMS_BASE];
                types = new OpenType[ITEMS_BASE];
                System.arraycopy(ITEM_TYPES, 0, types, 0, ITEMS_BASE);
                names = new String[ITEMS_BASE];
                System.arraycopy(ITEM_NAMES, 0, names, 0, ITEMS_BASE);
                descs = new String[ITEMS_BASE];
                System.arraycopy(ITEM_DESCRIPTIONS, 0, descs, 0, ITEMS_BASE);
            }


            values[0] = mOwnerId;
            values[1] = mActivationTimestamp;
            values[2] = mActiveExchanges.get();
            values[3] = mReceiveRequest;
            values[4] = mSendReply;
            values[5] = mSendFault;
            values[6] = mReceiveFault;
            values[7] = mLastFaultTime;
            values[8] = mSendDONE;
            values[9] = mReceiveDONE;
            values[10] = mLastDONETime;
            values[11] = mSendERROR;
            values[12] = mReceiveERROR;
            values[13] = mLastERRORTime;
            if (enabled)
            {
                values[14] = mResponseTime.getMin();
                values[15] = (long)mResponseTime.getAverage();
                values[16] = mResponseTime.getMax();
                values[17] = (long)mResponseTime.getSd();
                values[18] = mNMRTime.getMin();
                values[19] = (long)mNMRTime.getAverage();
                values[20] = mNMRTime.getMax();
                values[21] = (long)mNMRTime.getSd();
                values[22] = mComponentTime.getMin();
                values[23] = (long)mComponentTime.getAverage();
                values[24] = mComponentTime.getMax();
                values[25] = (long)mComponentTime.getSd();
                values[26] = mChannelTime.getMin();
                values[27] = (long)mChannelTime.getAverage();
                values[28] = mChannelTime.getMax();
                values[29] = (long)mChannelTime.getSd();
            }

            return new CompositeDataSupport(
                new CompositeType(
                    "EndpointStatistics",
                    "Endpoint statistics",
                    names, descs, types), names, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            ; // ignore this for now
        }
        return (null);
    }
   
    public void setInUse()
    {
        mActiveExchanges.incrementAndGet();
    }

    public void resetInUse()
    {
        mActiveExchanges.decrementAndGet();
    }

    synchronized void updateStatistics(MessageExchangeProxy me)
    {
        int             mask = me.getPhaseMask();
        METimestamps    ts = me.getTimestamps();

        if ((mask & MessageExchangeProxy.PM_SEND_REPLY) != 0)
        {
            mSendReply++;
        }
        if ((mask & MessageExchangeProxy.PM_SEND_FAULT) != 0)
        {
            mSendFault++;
            mLastFaultTime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_SEND_DONE) != 0)
        {
            mSendDONE++;
            mLastDONETime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_SEND_ERROR) != 0)
        {
            mSendERROR++;
            mLastERRORTime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_REQUEST) != 0)
        {
            mReceiveRequest++;
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_FAULT) != 0)
        {
            mReceiveFault++;
            mLastFaultTime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_DONE) != 0)
        {
            mReceiveDONE++;
            mLastDONETime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_ERROR) != 0)
        {
            mReceiveERROR++;
            mLastERRORTime = System.currentTimeMillis();
        }
        if (ts != null)
        {
            mResponseTime.addSample(ts.mResponseTime);
            mNMRTime.addSample(ts.mNMRTime);
            mChannelTime.addSample(ts.mProviderChannelTime);
            mComponentTime.addSample(ts.mProviderTime);
        }
    }

    synchronized void zeroStatistics()
    {
        mSendRequest = 0;
        mReceiveReply = 0;
        mReceiveRequest = 0;
        mSendReply = 0;
        mSendFault = 0;
        mReceiveFault = 0;
        mLastFaultTime = 0;
        mSendDONE = 0;
        mLastDONETime = 0;
        mReceiveDONE = 0;
        mSendERROR = 0;
        mLastERRORTime = 0;
        mReceiveERROR = 0;
        mResponseTime.zero();
        mChannelTime.zero();
        mNMRTime.zero();
        mComponentTime.zero();
        mStatusTime.zero();
    }

    
    //-------------------------------Object-----------------------------------

    public int hashCode()
    {
        return (mService.hashCode() ^ mEndpoint.hashCode());
    }
 	 
    /** Deep check for equality. */
    public boolean equals(Object obj)
    {
        RegisteredEndpoint  target;
        boolean             isEqual = false;
        
        if (obj != null && this.getClass() == obj.getClass())
        {
            target = (RegisteredEndpoint)obj;
            
            if (target.mService.equals(mService) &&
                target.mEndpoint.equals(mEndpoint))
            {
                isEqual = true;
            }
        }
        
        return isEqual;
    }
    
    public String toExternalName()
    {
        StringBuilder       sb = new StringBuilder();
        sb.append("Service{");
        sb.append(mService.toString());
        sb.append("}Endpoint{");
        sb.append(mEndpoint);
        sb.append("}");
        return (sb.toString());
    }
    
    public String toStringBrief()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("          Owner:     ");
        sb.append(mOwnerId);
        sb.append("  Active: ");
        sb.append(mActive ? "True" : "False");
        sb.append("\n          Service:   ");
        sb.append(mService);
        sb.append("\n          Endpoint:  ");
        sb.append(mEndpoint);
        sb.append("\n");
        if (mOperations != null)
        {
            sb.append("          Operations Count: ");
            sb.append(mOperations.size());
            sb.append("\n");
            for (Iterator i = mOperations.values().iterator(); i.hasNext(); )
            {
                sb.append("            ");
                sb.append(i.next().toString());
                sb.append("\n");
            }
        }
        if (mInterfaces != null)
        {
            sb.append("          Interfaces Count: ");
            sb.append(mInterfaces.length);
            sb.append("\n");
            for (int i = 0; i < mInterfaces.length; i++ )
            {
                sb.append("            ");
                sb.append(mInterfaces[i].toString());
                sb.append("\n");
            }
        }
        return (sb.toString());
    }
    
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append(toStringBrief());
        sb.append("            RecvRequest: " + mReceiveRequest);
        sb.append("  SendReply: " + mSendReply);
        sb.append("\n            RecvDONE:  " + mReceiveDONE);
        sb.append("  SendDONE:  " + mSendDONE);
        sb.append("\n            RecvERROR: " + mReceiveERROR);
        sb.append("  SendERROR: " + mSendERROR);
        sb.append("\n            RecvFault: " + mReceiveFault);
        sb.append("  SendFault: " + mSendFault);
        sb.append("\n");
        if (mChannelTime.getCount() != 0)
        {
            sb.append("            ResponseTime:  " + mResponseTime.toString());
            sb.append("\n            ComponentTime: " + mComponentTime.toString());
            sb.append("\n            ChannelTime:   " + mChannelTime.toString());
            sb.append("\n            NMRTime:       " + mNMRTime.toString());
            sb.append("\n");
        }
        return (sb.toString());
    }    

}
