/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentContext.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;


import org.glassfish.openesb.api.service.ServiceConsumerImpl;
import com.sun.jbi.framework.osgi.internal.jsr208.ComponentMBeanNames;
import com.sun.jbi.messaging.DeliveryChannelImpl;
import com.sun.jbi.logging.ComponentLoggingService;
        
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.JBIException;
import javax.jbi.component.Component;
import javax.jbi.management.MBeanNames;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessagingException;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.management.MBeanServer;
import javax.naming.InitialContext;
import javax.xml.namespace.QName;

import org.osgi.framework.ServiceRegistration;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.xml.sax.EntityResolver;

/**
 *
 * @author kcbabo
 */
public class ComponentContext 
        implements javax.jbi.component.ComponentContext,
                   com.sun.jbi.ext.ResolverSupport {
    
    private String                  componentName_;
    private String                  installRoot_;
    private Component               component_;
    private DeliveryChannelImpl     channel_;
    private HashMap<ServiceEndpoint, ServiceRegistration> osgiServices_;
    
    /** reference to component logging service     */
    ComponentLoggingService compLogService_;
    
    /** reference to the log configurations service */
    ServiceRegistration loggingServiceReference_;
 
    /** reference to the log display service */
    ServiceRegistration logDisplayServiceReference_;

    /**
     * Logger for this class.
     */
    private Logger logger_ = Logger.getLogger(
            ComponentContext.class.getPackage().getName());

            
    public ComponentContext(String componentName, String installRoot,
            Component component) {
        componentName_ = componentName;
        installRoot_   = installRoot;
        component_ = component;
        osgiServices_  = new HashMap<ServiceEndpoint, ServiceRegistration>();
        compLogService_ = new ComponentLoggingService(componentName); 
    }

    public ServiceEndpoint activateEndpoint(QName serviceName, String endpointName) 
            throws JBIException {
        // create the endpoint in the NMR
        ServiceEndpoint ep = channel_.activateEndpoint(serviceName, endpointName);
        
        try {
            // register a service channel for the endpoint within the OSGi registry
            ServiceConsumerImpl sc = new ServiceConsumerImpl(channel_, ep);
            ServiceRegistration sr = 
                    Environment.getFrameworkBundleContext().registerService(
                    "org.glassfish.openesb.api.service.ServiceConsumer", sc, sc.getProperties());
            osgiServices_.put(ep, sr);
        }
        catch (Exception ex) {
            throw new JBIException(ex);
        }
        
        return ep;
    }

    public void deactivateEndpoint(ServiceEndpoint endpoint) 
            throws JBIException {
        channel_.deactivateEndpoint(endpoint);
        
        if (osgiServices_.containsKey(endpoint)) {
            try {
                osgiServices_.remove(endpoint).unregister();
            }
            catch (IllegalStateException isEx) {
                // Service already unregistered - no big deal
            }
        }
    }
    
    /**
     * This method is used to get the ComponentLoggingService used by this component
     * to track the list of loggers for this component
     */
    public ComponentLoggingService getLoggingService() {
        return compLogService_;
    }

    /**
     * This method is used to store an instance of the ComponentLoggerConfigurationService
     * that is registered as a ManagedService to configure the log levels of various loggers
     * for this component.
     */
    public void setLogConfigServiceRef(ServiceRegistration serviceRef) {
        loggingServiceReference_ = serviceRef; 
    }
        
    /**
     * This method is used to get the instance of the ComponentLoggerConfigurationService
     * that is registered as a ManagedService to configure the log levels of various loggers
     * for this component.
     */
    public ServiceRegistration getLogConfigServiceRef() {
        return loggingServiceReference_; 
    } 
    
    /**
     * This method is used to store an instance of the ComponentLoggerConfigurationService
     * that is registered as a ManagedService to configure the log levels of various loggers
     * for this component.
     */
    public void setLogDisplayServiceRef(ServiceRegistration serviceRef) {
        logDisplayServiceReference_ = serviceRef; 
    }
        
    /**
     * This method is used to get the instance of the ComponentLoggerConfigurationService
     * that is registered as a ManagedService to configure the log levels of various loggers
     * for this component.
     */
    public ServiceRegistration getLogDisplayServiceRef() {
        return logDisplayServiceReference_; 
    }   
    
    
    public void deregisterExternalEndpoint(ServiceEndpoint externalEndpoint) 
            throws JBIException {
        channel_.deregisterExternalEndpoint(externalEndpoint);
    }

    public String getComponentName() {
        return componentName_;
    }

    void closeDeliveryChannel(DeliveryChannelImpl dc) {
        if (dc != null && !dc.isClosed()) {
            try {
                dc.close();
            } catch (MessagingException ignore) {
            }
        }
        channel_ = null;
    }

    public DeliveryChannel getDeliveryChannel() throws MessagingException {
        if (channel_ == null) {
            channel_ = Environment.getMessageService().
                activateChannel(componentName_, component_);
        }
        return (channel_);
    }

    public ServiceEndpoint getEndpoint(QName serviceName, String endpointName) {
        return channel_.getEndpoint(serviceName, endpointName);
    }

    public Document getEndpointDescriptor(ServiceEndpoint endpoint) 
            throws JBIException {
        return channel_.getEndpointDescriptor(endpoint, true);
    }

    public ServiceEndpoint[] getEndpoints(QName interfaceName) {
        return channel_.getEndpoints(interfaceName);
    }

    public ServiceEndpoint[] getEndpointsForService(QName serviceName) {
        return channel_.getEndpointsForService(serviceName);
    }

    public ServiceEndpoint[] getExternalEndpoints(QName interfaceName) {
        return channel_.getExternalEndpoints(interfaceName);
    }

    public ServiceEndpoint[] getExternalEndpointsForService(QName serviceName) {
        return channel_.getExternalEndpointsForService(serviceName);
    }

    public String getInstallRoot() {
        return installRoot_;
    }

    public Logger getLogger(String suffix, String resourceBundle) 
            throws MissingResourceException, JBIException {
        return compLogService_.getLogger(suffix, resourceBundle);
    }

    public MBeanNames getMBeanNames() {
        return new ComponentMBeanNames(componentName_);
    }

    public MBeanServer getMBeanServer() {
        return ManagementFactory.getPlatformMBeanServer();
    }

    public InitialContext getNamingContext() {
        
        try
        {
            return new InitialContext();
        }
        catch (javax.naming.NamingException namingException)
        {
            logger_.log(Level.WARNING, namingException.getMessage(), namingException);
            return null;
        }
    }

    public Object getTransactionManager() {
        return null;
    }

    public String getWorkspaceRoot() {
        return getInstallRoot();
    }

    public void registerExternalEndpoint(ServiceEndpoint endpoint) 
            throws JBIException {
        channel_.registerExternalEndpoint(endpoint);
    }

    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr) {
        return channel_.resolveEndpointReference(epr);
    }

    // -------------------------- com.sun.jbi.ext.ResolverSupport -------------

    public Document getEndpointDescriptor(ServiceEndpoint se, boolean flattened)
            throws javax.jbi.JBIException {
        return (getEndpointDescriptor(se, flattened));
    }

    public EntityResolver   getEndpointDescriptorResolver(ServiceEndpoint se) {
        return (null);
    }


}
