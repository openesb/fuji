/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)HeaderSplitter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import java.util.ArrayList;
import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.message.MessageProps;
import org.glassfish.openesb.api.service.ServiceMessageImpl;

/**
 *
 * @author dfrankforth
 */
public class HeaderSplitter 
    extends org.glassfish.openesb.api.eip.split.BaseSplitter
    implements org.glassfish.openesb.api.eip.split.Splitter
{
    /**
     * Create an Header Splitter instance
     */
    public HeaderSplitter()
        throws Exception
    {
    }

    /**
     *
     * @return the Correlation ID from the message
     */
    public Object getCorrelationID(ServiceMessage message){
        return (String)message.getProperty(MessageProps.GROUP_ID.toString());
    }


    /**
     * Split the current active message in the exchange and return the split documents ina
     * ArrayList
     *
     * @param svcMsg - message to split
     */
    public ArrayList split(ServiceMessage svcMsg)
        throws Exception
    {
        ServiceMessageImpl  msg = (ServiceMessageImpl)svcMsg;
        return (null);
    }
}
