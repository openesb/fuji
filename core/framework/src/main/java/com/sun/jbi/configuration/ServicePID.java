/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServicePID.java - Last published on Jun 25, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.configuration;


/**
 * Abstraction for the the configuration service pid
 * 
 * The service pid has the format:
 *   [domain].[type].[name]
 * @author nikki
 */
public class ServicePID {

    public String domain_ = "";
    public String type_ = "";
    public String name_ = "";
    
    public ServicePID(String pidStr){
      
        if (pidStr != null ){
            
            String[] tokens = pidStr.split("\\x2E");
            
            if ( tokens.length >= 3 ){
                domain_ = tokens[0];
                type_ = tokens[1];
                if ( tokens.length == 3){
                    name_ = tokens[2];
                } else {
                    int index = domain_.length() + type_.length() + 2;
                    name_ = pidStr.substring(index);
                }
            }
        }
    }
  
    /**
     * @return the name from the pid string
     */
    public String getName(){
        return name_;
    }
 
    /**
     * @return the type from the pid string
     */    
    public String getType(){
        return type_;
    }
    
    /**
     * @return the domain from the pid string
     */    
    public String getDomain(){
        return domain_;
    }
    
    /**
     * 
     * @param type
     * @param name
     * @return
     */
    public static String getServicePID(String type, String name){
        StringBuffer pidStr = new StringBuffer();

            // Domain
            pidStr.append(Constants.CONFIG_DOMAIN);
            pidStr.append(".");
            
            // Type
            if ( type == null ){
                type = "*";
            }
            pidStr.append(type);
           
            // Name
            if ( name == null){
                name = "*";
            }
            pidStr.append(".");
            pidStr.append(name);
        
        return pidStr.toString();
    }

    public static String generateQuery(String type, String name){
        StringBuffer servicePidFilter = new StringBuffer("(");
        servicePidFilter.append(org.osgi.framework.Constants.SERVICE_PID);
        servicePidFilter.append("=");
        servicePidFilter.append(getServicePID(type, name));
        servicePidFilter.append(")");

        return servicePidFilter.toString();
    }
}
