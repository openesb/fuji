/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptorInstance.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import com.sun.jbi.interceptors.Intercept;
import java.lang.reflect.Method;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class InterceptorInstance
{
    public Object mObject;
    public Method mMethod;

    public InterceptorInstance(Object object, Method method)
    {
        mObject = object;
        mMethod = method;
    }

    public Object getObject()
    {
        return mObject;
    }

    public Method getMethod()
    {
        return mMethod;
    }
    
    public int getPriority()
    {
        int priority = 100;
        Intercept intercept = (Intercept) mMethod.getAnnotation(Intercept.class);
        if ( intercept != null )
        {
            priority = intercept.priority();
        }
        return priority;
    }
}
