/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionManager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.xml.namespace.QName;

/**
 * This interface provides a facility for adding and deleting service 
 * connnections in the NMR.  The primary consumer of this interface will 
 * be the DeploymentService.  Connection information is contained in the
 * deployment descriptor for SAs and SUs.
 * @author Sun Microsystems, Inc.
 */
public interface ConnectionManager
{
    /**
     * Add an interface-to-endpoint mapping to the NMR connection table.
     * @param fromInterface interface used by the consumer
     * @param toService service name used by the provider
     * @param toEndpoint endpoint name used by the provider
     */
    void addInterfaceConnection(
        QName fromInterface, QName toService, String toEndpoint)
        throws javax.jbi.messaging.MessagingException;
    
    /**
     * Add an endpoint-to-endpoint mapping to the NMR connection table.
     * @param fromService service name used by the consumer
     * @param fromEndpoint endpoint name used by the consumer
     * @param toService service name used by the provider
     * @param toEndpoint endpoint name used by the provider
     */
    void addEndpointConnection(
        QName fromService, String fromEndpoint,
        QName toService, String toEndpoint,
        Link linkType)
        throws javax.jbi.messaging.MessagingException;
    
    /**
     * Removes an interface-to-endpoint mapping from the NMR connection table.
     * @param fromInterface interface used by the consumer
     * @param toService service name used by the provider
     * @param toEndpoint endpoint name used by the provider
     * @return true if the connection was removed, false otherwise.
     */
    boolean removeInterfaceConnection(
        QName fromInterface, QName toService, String toEndpoint);
    
    /**
     * Removes an endpoint-to-endpoint mapping from the NMR connection table.
     * @param fromService service name used by the consumer
     * @param fromEndpoint endpoint name used by the consumer
     * @param toService service name used by the provider
     * @param toEndpoint endpoint name used by the provider
     */
    boolean removeEndpointConnection(
        QName fromService, String fromEndpoint,
        QName toService, String toEndpoint);
}
