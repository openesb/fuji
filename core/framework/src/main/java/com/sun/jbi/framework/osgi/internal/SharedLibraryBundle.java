/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SharedLibraryBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;

import org.osgi.framework.Bundle;

/**
 * OSGi bundle representing a JBI shared library.
 * @author kcbabo
 */
public class SharedLibraryBundle extends JBIBundle {
    
    /**
     * Creates a new SharedLibraryBundle.
     * @param descriptor jbi.xml
     * @param bundle containing OSGi bundle
     */
    public SharedLibraryBundle(Jbi descriptor, Bundle bundle) {
        super(descriptor, bundle);
    }
    
    /**
     * I'm a shared library.
     * @return <code>BundleType.SHARED_LIBRARY</code>
     */
    public BundleType getType() {
        return BundleType.SHARED_LIBRARY;
    }
    
    /**
     * Returns shared library name.
     * @return shared library name
     */
    public String getName() {
        return getDescriptor().getSharedLibrary().getIdentification().getName();
    }
}
