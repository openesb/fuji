/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileUtil.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import java.io.File;

/**
 * Keeping messy file manipulation code in one place.
 * @author kcbabo
 */
public class FileUtil {
    
    /**
     * Removes all files and child directories in the specified directory. 
     * @param dir
     * @return false if unable to delete the file
     */
    public static boolean cleanDirectory(File dir)
    {
        File[] tmps = dir.listFiles();
        for (int i = 0; i < tmps.length; i++)
        {
            if (tmps[i].isDirectory())
            {
                if (!cleanDirectory(tmps[i]))
                {
                    return false;
                }
            }
            
            if ( !tmps[i].delete() )
            {
                return false;
            }
        }
        
        return true;
    }
}
