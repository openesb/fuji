/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeliveryChannelImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import com.sun.jbi.ext.SubChannel;
import com.sun.jbi.ext.TimeoutListener;

import com.sun.jbi.messaging.events.AbstractEvent;
import com.sun.jbi.messaging.stats.METimestamps;
import com.sun.jbi.messaging.stats.Value;
import com.sun.jbi.messaging.stats.Value.Kind;
import com.sun.jbi.messaging.util.Translator;

import java.net.URI;
import java.util.HashSet;
import java.util.Iterator;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.SortedMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jbi.component.Component;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.transaction.xa.XAResource;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;

/** Implementation of DeliveryChannel interface.
 * @author Sun Microsystems, Inc.
 */
public class DeliveryChannelImpl 
        implements javax.jbi.messaging.DeliveryChannel,
                   com.sun.jbi.ext.DeliveryChannel,
                   ChannelStatistics
        
{
    public static final String REG_NOTIFICATION_TYPE = "Registration";
    
    /** Component id of channel owner. */
    private String              mChannelId;
    /** ClassLoader used by the owner of the channel. */
    private ClassLoader         mClassLoader;
    /** Reference to message service. */
    private MessageService      mMsgSvc;
    /** Flag indicating state of channel */
    private boolean             mIsClosed;
    /** Flag indicating transactional state. */
    private boolean             mTransactional;
    /** Map of in process exchanges for this Channel. */
    private IdentityHashMap     mActive;
    private long                mMaxActive;
    /** Queue of MessageExchanges waiting for delivery. */
    private LinkedList          mQueue;

    private long                mMaxQueued;
    private int                 mAcceptors;
    /** EventAdmin support for activity notifcation. */
    private SubChannel          mAcceptor;
    
    /** Basic statistics used for DeliveryChannel development. */
    private long                mDeadWait;
    private long                mTimeOut;
    private long                mSendCount;
    private long                mSendSyncCount;
    private long                mAcceptCount;
    
    /** Primary statistics for external use.    */
    private long                mStatisticsEpoch;
    private long                mSendRequest;
    private long                mReceiveRequest;
    private long                mSendReply;
    private long                mReceiveReply;
    private long                mSendFault;
    private long                mReceiveFault;
    private long                mSendDONE;
    private long                mReceiveDONE;
    private long                mSendERROR;
    private long                mReceiveERROR;
    private Value                mResponseTime;
    private Value                mStatusTime;
    private Value                mNMRTime;
    private Value                mComponentTime;
    private Value                mChannelTime;

 /** Reference to the JBI Component instance using this channel. */
    private Component mComponent;
    
    
    /** Set of Internal endpoints activiated. */
    private HashSet<RegisteredEndpoint>             mInternalEndpoints;
    private HashMap<String, RegisteredEndpoint>      mEndpoints;
    
    /** Set of External endpoints registered. */
    private HashSet<RegisteredEndpoint>             mExternalEndpoints;
    
    private EndpointRegistry    mRegistry;
    
    private int mNotifyIdx = 0;
    
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());


    /** 
     *  Create a new DeliveryChannel.
     *  @param channelId component id
     *  @param msgSvc message service
     *  @param component the component using this delivery channel
    */
    DeliveryChannelImpl(String channelId, Component component, MessageService msgSvc)
    {
        mChannelId  = channelId;
        mComponent = component;
        mMsgSvc     = msgSvc;
        mIsClosed   = false;
        mTransactional = true;
        mQueue      = new LinkedList();
        mActive     = new IdentityHashMap();
        mInternalEndpoints = new HashSet();
        mExternalEndpoints = new HashSet();
        mEndpoints = new HashMap();
        mRegistry   = EndpointRegistry.getInstance();
        mResponseTime = new Value(Kind.INTERVAL_NS);
        mStatusTime = new Value(Kind.INTERVAL_NS);
        mNMRTime = new Value(Kind.INTERVAL_NS);
        mComponentTime = new Value(Kind.INTERVAL_NS);
        mChannelTime = new Value(Kind.INTERVAL_NS);
    }
        
    /*#####################################################################
     *#                                                                   #
     *#                      Public API Methods                           #
     *#                                                                   #
     *#####################################################################*/
    
    
    /*######################### Communication #############################*/

    /** Blocking call which samples the channel at a pre-determined interval
    *  for available exchanges.
    *  @return message exchange, or null if channel was interrupted.
    *  @throws javax.jbi.messaging.MessagingException error while reading
    *  exchange.
    */
    public MessageExchange accept() 
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchangeProxy me = null;
       
        me = acceptInternal(0);
        if (me != null)
        {
            if (me.capture(METimestamps.TAG_CACCEPT, METimestamps.TAG_PACCEPT))
            {
                propagateStatistics(me);
            }
        }
        return (me);
    }
    
    
    public MessageExchange accept(long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchangeProxy me = null;
       
        me = acceptInternal(timeout);
        if (me != null)
        {
            if (me.capture(METimestamps.TAG_CACCEPT, METimestamps.TAG_PACCEPT))
            {
                propagateStatistics(me);
            }
        }
        return (me);
    }
    
    private MessageExchangeProxy acceptInternal(long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchangeProxy me = null;
        

        if (mIsClosed)
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CHANNEL_CLOSED));
        }
        if (timeout == 0)
        {
            me = dequeueExchange();            
        }
        else
        {
            me = dequeueExchange(timeout);
        }
        
        if (me != null)
        {
            if (me.handleAccept(this))
            {
                removeAcceptReference(me);
                if (mLog.isLoggable(Level.FINER))
                {
                    mLog.finer("Exchange " + me.getExchangeId() + " accepted on channel" + mChannelId);
                }
            }
            else
            {
                updateStatistics(me);
            }
            mMsgSvc.notifyExchangeListeners(me, false);
        }
        return me;        
    }
    
    /** Defers routing to the message service. */
    public void send(MessageExchange exchange)
        throws javax.jbi.messaging.MessagingException
    {
        //
        //  Capture timestamp.
        //
        ((MessageExchangeProxy)exchange).capture(METimestamps.TAG_CSEND, METimestamps.TAG_PSEND);

        if (isClosed())
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CHANNEL_CLOSED)); 
        }
        mMsgSvc.doExchange(this, (MessageExchangeProxy)exchange);
    }

    public boolean sendSync(MessageExchange exchange)
        throws javax.jbi.messaging.MessagingException
    {
        //
        //  Capture timestamp.
        //
        ((MessageExchangeProxy)exchange).capture(METimestamps.TAG_CSEND, METimestamps.TAG_PSEND);

        return (sendSyncInternal((MessageExchangeProxy)exchange, 0));
    }
    
    public boolean sendSync(MessageExchange exchange, long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        //
        //  Capture timestamp.
        //
        ((MessageExchangeProxy)exchange).capture(METimestamps.TAG_CSEND, METimestamps.TAG_PSEND);


        return (sendSyncInternal((MessageExchangeProxy)exchange, timeout));
    }
    
    private boolean sendSyncInternal(MessageExchangeProxy mep, long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        boolean                    available;
        
        if (isClosed())
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CHANNEL_CLOSED)); 
        }       

        if (mLog.isLoggable(Level.FINER))
        {
            mLog.finer("Exchange " + mep.getExchangeId() + " sent on channel" + mChannelId);
        }
        if (available = mMsgSvc.doSynchExchange(this, mep, timeout))
        {
            if (mep.handleAccept(this))
            {
                this.removeAcceptReference(mep);                
            }
            else
            {
                updateStatistics(mep);
            }
            if (mep.capture(METimestamps.TAG_CACCEPT, METimestamps.TAG_PACCEPT))
            {
                propagateStatistics(mep);
            }
             mMsgSvc.notifyExchangeListeners(mep, false);
        }
    
        return available;
    }
        
    
    /*###################### Exchange Factories #############################*/
    
    /** Create a message exchange factory. This factory will create exchange
     *  instances with all appropriate properties set to null.
     *  @return a message exchange factory
     */
    public MessageExchangeFactory createExchangeFactory()
    {
        return new ExchangeFactory(mMsgSvc);
    }
    
    /** Create a message exchange factory for the given interface name.
     *  @param interfaceName name of the interface for which all exchanges 
     *   created by the returned factory will be set
     *  @return name of the interface for which all exchanges created by the 
     *   returned factory will be set
     */
    public MessageExchangeFactory createExchangeFactory(QName interfaceName)
    {
        return ExchangeFactory.newInterfaceFactory(mMsgSvc, interfaceName);
    }
    
     /** Create a message exchange factory for the given service name.
     *  @param serviceName name of the service for which all exchanges 
     *   created by the returned factory will be set
     *  @return an exchange factory that will create exchanges for the given 
     *   service; must be non-null
     */
    public MessageExchangeFactory createExchangeFactoryForService(QName serviceName)
    {
        return ExchangeFactory.newServiceFactory(mMsgSvc, serviceName);
    }
    
    /** Create a message exchange factory for the given endpoint.
     *  @param endpoint endpoint for which all exchanges created by the
     *   returned factory will be set for
     *  @return an exchange factory that will create exchanges for the 
     *   given endpoint
     */
    public MessageExchangeFactory createExchangeFactory(ServiceEndpoint endpoint)
    {
        return ExchangeFactory.newEndpointFactory(mMsgSvc, endpoint);
    }    
    
    /*################# Framework Passthrough Methods #######################*/

    /**
     * Register an XAResource
     * @param resource to be added.
     */
    public void registerXAResource(XAResource resource)
    {
        mMsgSvc.addXAResource(resource);
    }
    
    /** Activate an endpoint.
     * @param service QName of service
     * @param endpoint NCName of endpoint
     * @return activated endpoint
     * @throws javax.jbi.messaging.MessagingException failed to register endpoint
     */
    public RegisteredEndpoint activateEndpoint(QName service, String endpoint)
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint re;
        
        if (service == null || endpoint == null || endpoint.equals(""))
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.ACTIVATE_NOT_NULL));
        }

        re = mRegistry.registerInternalEndpoint(service, endpoint, getChannelId());
        
        synchronized (mInternalEndpoints)
        {
            mInternalEndpoints.add(re);
            mEndpoints.put(re.toExternalName(), re);
        }
        
        return re;
    }
    
    /** Used by a BC to deactivate an endpoint which it has registered.
     * @param ref endpoint reference
     * @throws javax.jbi.messaging.MessagingException failed to deactivate 
     *  endpoint.
     */
    public void deactivateEndpoint(ServiceEndpoint ref) 
        throws javax.jbi.messaging.MessagingException 
    {
        RegisteredEndpoint re;
        
        re = (RegisteredEndpoint)ref;
        
        if (!re.getOwnerId().equals(mChannelId))
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.DEACTIVATE_NOT_OWNER));
        }
        
        synchronized (mInternalEndpoints)
        {
            mInternalEndpoints.remove(ref);
            mEndpoints.remove(((RegisteredEndpoint)ref).toExternalName());
        }
        
        mRegistry.removeEndpoint(re);
    }
    
     /**
     * Registers the specified external endpoint with the NMR.  This indicates
     * to the NMR that the specified endpoint is used as a proxy for external
     * service consumers to access an internal service by the same name.
     * @throw javax.jbi.messaging.MessagingException if the specified endpoint
     * has already been registered.
     */
    public void registerExternalEndpoint(ServiceEndpoint externalEndpoint)
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint re;
        
        try
        {
            re = mRegistry.registerExternalEndpoint(externalEndpoint, getChannelId()); 
            synchronized (mExternalEndpoints)
            {
                mExternalEndpoints.add(re);
            }
        }
        catch (javax.jbi.messaging.MessagingException msgEx)
        {
            mLog.warning(msgEx.getMessage());
            throw msgEx;
        }
    }
      
    /**
     * Deregisters the specified external endpoint with the NMR.  This indicates
     * to the NMR that external service consumers can no longer access the
     * internal service by this name.
     */
    public void deregisterExternalEndpoint(ServiceEndpoint externalEndpoint)
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint re;
        
        re = mRegistry.getExternalEndpoint(
            externalEndpoint.getServiceName(), externalEndpoint.getEndpointName());
        
        if (!re.getOwnerId().equals(mChannelId))
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.DEACTIVATE_NOT_OWNER));
        }

        synchronized (mExternalEndpoints)
        {
            mExternalEndpoints.remove(re);
        }
        
        
        mRegistry.removeEndpoint(re);
    }
    
    /**
     * Get the service description for the named endpoint, if any exists.
     * @param service the qualified name of the endpoint's service.
     */
    public ServiceEndpoint getEndpoint(QName service, String name)
    {
        ServiceEndpoint[]   list;
        ServiceEndpoint     endpoint = null;
        
        list = getEndpointsForService(service);
        
        for (int i = 0; i < list.length; i++)
        {
            if (list[i].getEndpointName().equals(name))
            {
                endpoint = list[i];
                break;
            }
        }
        
        return endpoint;
    }
    
    synchronized RegisteredEndpoint getEndpointByExternalName(String name)
    {
        return (mEndpoints.get(name));
    }
    
    /** Uses the owner id of the reference to query the appopriate channel. */
    public Document getEndpointDescriptor(ServiceEndpoint endpoint, boolean flat)
        throws javax.jbi.messaging.MessagingException
    {
        return mMsgSvc.queryDescriptor(endpoint, flat);
    }
    
    /**
     * Queries the NMR for external endpoints that implement the specified
     * interface name.
     */
    public ServiceEndpoint[] getExternalEndpoints(QName interfaceName)
    {
        return mRegistry.getExternalEndpointsForInterface(interfaceName);
    }    
    
    /**
     * Queries the NMR for active endpoints that implement the given interface.
     * This will return the endpoints for all services and endpoints that 
     * implement the named interface (portType in WSDL 1.1). This method does 
     * NOT include external endpoints (those registered using 
     * registerExternalEndpoint(ServiceEndpoint)).
     * @param interfaceName qualified name of interface/portType that is 
     *  implemented by the endpoint
     * @return ServiceEndpoint[] list of available endpoints for the specified 
     *  interface name; potentially zero-length.
     */
    public ServiceEndpoint[] getEndpoints(QName interfaceName)
    {
        return mRegistry.getInternalEndpointsForInterface(interfaceName);
    }
    
    public ServiceEndpoint[] getEndpoints()
    {
        return (mInternalEndpoints.toArray(new ServiceEndpoint[mInternalEndpoints.size()]));
    }
    
    /**
     * Queries the NMR for external endpoints that are part of the specified
     * service.
     */
    public ServiceEndpoint[] getExternalEndpointsForService(QName serviceName)
    {
        return mRegistry.getExternalEndpointsForService(serviceName);
    }
    
  
    /**
     * Resolves the specified endpoint reference into a service endpoint. This
     * is called by the component when it has an endpoint reference that it
     * needs to resolve into a service endpoint.
     */
    public ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment endpointReference)
    {
        return mMsgSvc.resolveEndpointReference(endpointReference);
    }

    
    /** Used by ComponentContext.getEndpointsForService(). */
    public ServiceEndpoint[] getEndpointsForService(QName service)
    {
        // return endpoints from registry without converting connection links
        return mRegistry.getInternalEndpointsForService(service, false);
    }
    
    /*###################### RI Private Methods ###############################*/
    
    public void setTransactional(boolean transactional)
    {
        mTransactional = transactional;
    }
    
    public final boolean isTransactional()
    {
        return (mTransactional);
    }
    
    public ClassLoader getClassLoader()
    {
        return (mClassLoader);
    }
    
    /*########################## Lifecycle #################################*/

    /** Close the channel.
    *  @throws javax.jbi.messaging.MessagingException failure during close.
    */
    public void close() 
        throws javax.jbi.messaging.MessagingException
    {
        if (mIsClosed)
        {
            return;
        }
        mIsClosed = true;
        
        mMsgSvc.addTimeoutListener(mChannelId, null);
        for (RegisteredEndpoint ref : mInternalEndpoints)
        {
            mRegistry.removeEndpoint(ref);
        }
        for (RegisteredEndpoint ref : mExternalEndpoints)
        {
            mRegistry.removeEndpoint(ref);
        }
        
        // Terminate active, flush accept queue and notify waiters.
        cleanActive();
        
        // Terminate any event notifications.
        if (mAcceptor != null)
        {
        }

        // Remove channel from message service registry
        mMsgSvc.removeChannel(mChannelId);
    }
    
    public boolean isClosed()
    {
        return (mIsClosed);
    }
    
    /*################# com.sun.jbi.ext.DeliveryChannel ####################*/
    
    public void handleEndpointEvent(AbstractEvent ae)
            throws javax.jbi.messaging.MessagingException
    {
        mRegistry.handleEndpointEvent(ae, mChannelId);
    }

    public AbstractEvent[] getAllEndpointEvents()
    {
        return (mRegistry.getAllEndpoints());
    }

    public MessageExchange createExchangeWithId(URI pattern, String id) 
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me;
        
        me = createExchangeFactory().createExchange(pattern);
        ((MessageExchangeProxy)me).getMessageExchange().setExchangeId(id);
        return (me);
    }
    
    public ServiceEndpoint createEndpoint(QName service, String endpoint)
        throws javax.jbi.messaging.MessagingException 
    {
        return (mRegistry.getInternalEndpoint(service, endpoint));
    }

    /** Check if a MessageExchange is being tracked. */
    
    synchronized public boolean isActive(MessageExchange me)
    {
        return (mActive.containsKey(me));
    }
    
    public boolean isExchangeOkay(MessageExchange me)
    {
        return (mMsgSvc.isExchangeOkay((com.sun.jbi.messaging.MessageExchangeImpl)me));
    }
    
    public void addTimeoutListener(TimeoutListener tl)
    {
        mMsgSvc.addTimeoutListener(this.getChannelId(), tl);
    }
    
    synchronized public Object enableAcceptNotification(SubChannel o)
    {
        if (mAcceptor == null)
        {
           mAcceptor = o;
           mComponent = o.getComponent();
        }
        return (mAcceptor);
    }

    public boolean onEvent(MessageExchange parent, int id, MessageExchange child) {
        boolean accepted = false;

        if (mAcceptor != null) {
            accepted = mAcceptor.onEvent(parent, id, child);
            if (accepted) {
                synchronized (this) {
                    //
                    //  If we are queuing an event to a SunChannel that has
                    //  an empty queue, make sure that we make it up.
                    //
                    if (mQueue.size() == 0)
                    {
                        this.notifyAll();
                    }
                }
            }
        }
        return (accepted);
    }

    public void resume(MessageExchange me) {
        mMsgSvc.sendEvent(com.sun.jbi.ext.DeliveryChannel.EVENT_RESUME, me);
    }

    public void suspend(MessageExchange me) {
        mMsgSvc.sendEvent(com.sun.jbi.ext.DeliveryChannel.EVENT_SUSPEND, me);
    }

    public void signal(MessageExchange me) {
        if (mMsgSvc.isChild(this, me)) {
            mMsgSvc.sendEvent(com.sun.jbi.ext.DeliveryChannel.EVENT_SIGNAL, me);
        }
    }

    /*#####################################################################
     *#                                                                   #
     *#                      Internal Methods                             #
     *#                                                                   #
     *#####################################################################*/
    
    /** 
     * Returns a reference to the owning components javax.jbi.component.Component
     * implementation.
     * @return Component reference for the owning component.
     */
    Component getComponentInstance() 
    {
        return mComponent;
    }
        
    /** Retrieve the id for this channel. */
    String getChannelId()
    {
        return mChannelId;
    }

    String getChannelId(MessageExchange me)
    {
        String  result = null;

        if (mAcceptor != null)
        {
            result = mAcceptor.getChannelId(me);
        }
        if (result == null)
        {
            result = mChannelId;
        }
        return (result);
    }

    /** Track the given MessageExchange.    */
    synchronized void addSendReference(MessageExchangeProxy me)
    {
        if (mActive.put(me, me) == null)
        {
            me.setInUse(mChannelId);
            if (mActive.size() > mMaxActive)
            {
                mMaxActive = mActive.size();
            }
            if (me.getParent() != null)
            {
                me.getParent().addChild(me);
            }
        }
        mSendCount++;
        mMsgSvc.mSendCount++;
        switch (me.getPhase())
        {
            case MessageExchangeProxy.PHASE_REPLY:
                mSendReply++;
                break;

            case MessageExchangeProxy.PHASE_REQUEST:
                mSendRequest++;
                break;

            case MessageExchangeProxy.PHASE_DONE:
                mSendDONE++;
                break;

            case MessageExchangeProxy.PHASE_ERROR:
                mSendERROR++;
                break;

            case MessageExchangeProxy.PHASE_FAULT:
                mSendFault++;
                break;
        }
    }

    /** Track the given MessageExchange.    */
    synchronized void addSendSyncReference(MessageExchangeProxy me)
    {
        if (mActive.put(me, me) == null)
        {
            me.setInUse(mChannelId);
            if (mActive.size() > mMaxActive)
            {
                mMaxActive = mActive.size();
            }
            if (me.getParent() != null)
            {
                me.getParent().addChild(me);
            }
        }
        mSendSyncCount++;
        mMsgSvc.mSendSyncCount++;
        switch (me.getPhase())
        {
            case MessageExchangeProxy.PHASE_REPLY:
                mSendReply++;
                break;

            case MessageExchangeProxy.PHASE_REQUEST:
                mSendRequest++;
                break;

            case MessageExchangeProxy.PHASE_DONE:
                mSendDONE++;
                break;

            case MessageExchangeProxy.PHASE_ERROR:
                mSendERROR++;
                break;

            case MessageExchangeProxy.PHASE_FAULT:
                mSendFault++;
                break;
        }
    }

    /** Stop tracking the given MessageExchange.    */
    synchronized void removeSendReference(MessageExchangeProxy me)
    {
        mActive.remove(me);
        me.resetInUse();
        if (me.getParent() != null)
        {
            me.getParent().removeChild(me);
        }
        mSendCount++;
        mMsgSvc.mSendCount++;
        switch (me.getPhase())
        {
            case MessageExchangeProxy.PHASE_REPLY:
                mSendReply++;
                break;

            case MessageExchangeProxy.PHASE_REQUEST:
                mSendRequest++;
                break;

             case MessageExchangeProxy.PHASE_DONE:
                mSendDONE++;
                break;

            case MessageExchangeProxy.PHASE_ERROR:
                mSendERROR++;
                break;

            case MessageExchangeProxy.PHASE_FAULT:
                mSendFault++;
                break;
        }
    }

    /** Stop tracking the given MessageExchange.    */
    synchronized void removeAcceptReference(MessageExchangeProxy me)
    {
        if (mActive.remove(me) != null)
        {
            me.resetInUse();
        }
        switch (me.getPhase())
        {
            case MessageExchangeProxy.PHASE_REPLY:
                mReceiveReply++;
                break;

            case MessageExchangeProxy.PHASE_REQUEST:
                mReceiveRequest++;
                break;

             case MessageExchangeProxy.PHASE_DONE:
                mReceiveDONE++;
                break;

            case MessageExchangeProxy.PHASE_ERROR:
                mReceiveERROR++;
                break;

            case MessageExchangeProxy.PHASE_FAULT:
                mReceiveFault++;
                break;
        }
    }

    synchronized void addAcceptReference(MessageExchangeProxy me)
    {
        if (mActive.put(me, me) == null)
        {
            me.setInUse(null);
            if (mActive.size() > mMaxActive)
            {
                mMaxActive = mActive.size();
            }
        }
    }

    synchronized void removeActive(MessageExchangeProxy me) {
        mActive.remove(me);
    }

    void propagateStatistics(MessageExchangeProxy me)
    {
        me.updateStatistics();
        mMsgSvc.updateStatistics(me);
    }

    synchronized void updateStatistics(MessageExchangeProxy me)
    {
        switch (me.getPhase())
        {
            case MessageExchangeProxy.PHASE_REPLY:
                mReceiveReply++;
                break;

            case MessageExchangeProxy.PHASE_REQUEST:
                mReceiveRequest++;
                break;

             case MessageExchangeProxy.PHASE_DONE:
                mReceiveDONE++;
                break;

            case MessageExchangeProxy.PHASE_ERROR:
                mReceiveERROR++;
                break;

            case MessageExchangeProxy.PHASE_FAULT:
                mReceiveFault++;
                break;
        }
    }

    synchronized void updateProviderStatistics(METimestamps ts)
    {
        if (ts != null)
        {
            mResponseTime.addSample(ts.mResponseTime);
            mNMRTime.addSample(ts.mNMRTime);
            mComponentTime.addSample(ts.mProviderTime);
            mChannelTime.addSample(ts.mProviderChannelTime);
        }
    }

    synchronized void updateConsumerStatistics(METimestamps ts)
    {
        if (ts != null)
        {
            if (ts.mStatusTime != 0)
            {
                mStatusTime.addSample(ts.mStatusTime);
            }
            if (ts.mConsumerTime != 0)
            {
                mComponentTime.addSample(ts.mConsumerTime);
            }
            mNMRTime.addSample(ts.mNMRTime);
            mChannelTime.addSample(ts.mConsumerChannelTime);
        }
    }

    synchronized void zeroStatistics()
    {
        mDeadWait = 0;;
        mTimeOut = 0;
        mSendCount = 0;
        mSendSyncCount = 0;
        mAcceptCount = 0;
        mSendRequest = 0;
        mReceiveRequest = 0;
        mSendReply = 0;
        mReceiveReply = 0;
        mSendFault = 0;
        mReceiveFault = 0;
        mSendDONE = 0;
        mReceiveDONE = 0;
        mSendERROR = 0;
        mReceiveERROR = 0;
        mResponseTime.zero();
        mStatusTime.zero();
        mNMRTime.zero();
        mComponentTime.zero();
        mChannelTime.zero();
    }

    /** Queue a message exchange. Notify any waiters on 0->1 transition. */
    synchronized javax.jbi.messaging.MessagingException queueExchange(MessageExchangeProxy mep)
    {        
        if (mIsClosed)
        {
            return (new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CHANNEL_CLOSED)));
        }
        else if (mep.getSynchState() == MessageExchangeProxy.ERROR)
        {
             return (new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.EXCHANGE_TERMINATED)));
        }

        mActive.put(mep, mep);
        if (mep.getSynchState() == MessageExchangeProxy.NONE)
        {
            //
            // Give priority to completed messages.
            //
            if (mep.getStatus().equals(ExchangeStatus.ACTIVE))
            {
                mQueue.addLast(mep);
            }
            else
            {
                mQueue.addFirst(mep);
            }
            mep.setSynchState(MessageExchangeProxy.DONE);
            if (mQueue.size() == 1)
            {
                this.notifyAll();
            }
            mep.capture(METimestamps.TAG_CQUEUE, METimestamps.TAG_PQUEUE);
        }
        else
        {
            synchronized (mep)
            {
                if (mep.getSynchState() == MessageExchangeProxy.WAIT ||
                        mep.getSynchState() == MessageExchangeProxy.WAIT_TIMEOUT)
                {
                    mAcceptCount++;
                    mMsgSvc.mSendSyncCount++;
                    mep.setSynchState(MessageExchangeProxy.HALF_DONE);
                    mep.capture(METimestamps.TAG_CQUEUE, METimestamps.TAG_PQUEUE);
                    mep.notify();
                }
                else if (mep.getSynchState() == MessageExchangeProxy.ERROR)
                {
                    javax.jbi.messaging.MessagingException   e = new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MESSAGE_TIMEOUT));
                    mActive.remove(mep);
                    mep.terminate(e, false);
                    return e;
                }
            }
        }
        return (null);
    }
 
    /** Dequeue a message exchange. Wait if the queue is empty.  */
    synchronized MessageExchangeProxy dequeueExchange()
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchangeProxy  me = null;
        boolean              dead = false;
        try
        {
            mAcceptCount++;
            mMsgSvc.mAcceptCount++;
            mAcceptors++;
            while (!mIsClosed)
            {
                if (mQueue.size() > 0)
                {
                    me = (MessageExchangeProxy)mQueue.removeFirst();
                    break;
                } else if (mAcceptor != null && mAcceptor.hasEvents())
                {
                    break;
                }
                this.wait();
                if (dead)
                {
                    mDeadWait++;
                }
                dead = true;
            }
        }
        catch (InterruptedException intEx)
        {
            throw new javax.jbi.messaging.MessagingException(intEx);
        }   
        finally
        {
            mAcceptors--;
        }

        return (me);
    }
    
    /** Dequeue a message exchange. Wait for the given number of milliseconds if the queue is empty.  */
    synchronized MessageExchangeProxy dequeueExchange(long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchangeProxy    me = null;
        boolean                timedOut = false;        
        boolean                dead = false;
        long                    endTime = System.currentTimeMillis() + timeout;
        
        try
        {           
            mAcceptCount++;
            mMsgSvc.mAcceptCount++;
            mAcceptors++;
            while (!mIsClosed)
            {
                if (mQueue.size() > 0)
                {
                    me = (MessageExchangeProxy)mQueue.removeFirst();
                    break;
                }
                if (timedOut)
                {
                    mTimeOut++;
                    mMsgSvc.mAcceptTimeoutCount++;
                    break;
                }

                /* We can wake up for two reasons:
                 *    (1) notify() was called
                 *    (2) the wait timer expired
                 * The wait() call itself doesn't provide this information,
                 * so we need to sample the time to see what happened.
                 */
                this.wait(timeout);
                timeout = endTime - System.currentTimeMillis();
                timedOut = timeout <= 0;
                if (dead)
                {
                    mDeadWait++;
                }
                dead = true;
            }
        }
        catch (InterruptedException intEx)
        {
            throw new javax.jbi.messaging.MessagingException(intEx);
        }        
        finally
        {
            mAcceptors--;
        }

        return (me);
    }

    /** Clean up in preparation for closing the channel. */
    void cleanActive()
    {
        Set         s;
        Iterator    i;

        //
        //  Try and cleanup any active MEP's.
        //
        synchronized (this)
        {
            s = mActive.entrySet();

            for (i = s.iterator(); i.hasNext();)
            {
                Exception   e = new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.MESSAGE_TIMEOUT));
                MessageExchangeProxy    mep;
                mep = (MessageExchangeProxy)((Map.Entry)i.next()).getKey();
                if (mep.terminate(e, mQueue.contains(mep)))
                {
                    i.remove();
                    mLog.warning("DeliveryChannel:Close Cleanup Exchange\n" + mep);
               }
            }
                
            mQueue.clear();
            this.notifyAll();
        }
        
        //
        //  Any MEP's left over need to perform a send() to trigger processing 
        //  on the other side.
        //
        for (i = s.iterator(); i.hasNext();)
        {
            MessageExchangeProxy    mep;
            javax.jbi.messaging.MessagingException  mEx;

            mep = (MessageExchangeProxy)((Map.Entry)i.next()).getKey();
            mEx = mep.getSendChannel().queueExchange(mep.getTwin());
            if (mEx != null)
            {
                //
                //  Just swallow any errors as it means that the target is also
                //  closed.
                //
                mLog.warning("MessagingException Id(" + mep.getExchangeId() + ") : " + mEx);
            }           
        }
        mActive.clear();
    }

    /** Helper the forward channel lookup to MessageService. */
    DeliveryChannelImpl getChannel(String id)
    {
        return ((DeliveryChannelImpl)mMsgSvc.getChannel(id));
    }
    
    
    public void setExchangeIdGenerator(ExchangeIdGenerator generator)
    {
        mMsgSvc.setExchangeIdGenerator(generator);
    }
    
    public void dumpState()
    {
        mMsgSvc.dumpState();
    }

    //----------------------------ChannelStatistics-----------------------------

    /**
     * List of item names for CompositeData construction.
     */
    private static final int ITEMS_BASE = 20;
    private static final int ITEMS_EXTRA = 20;
    private static final String[] ITEM_NAMES = {
        "ActiveExchanges",
        "MaxActiveExchanges",
        "QueuedExchanges",
        "MaxQueuedExchanges",
        "ActiveEndpoints",
        "SendRequest",
        "ReceiveRequest",
        "SendReply",
        "ReceiveReply",
        "SendFault",
        "ReceiveFault",
        "SendDONE",
        "ReceiveDONE",
        "SendERROR",
        "ReceiveERROR",
        "DeadWait",
        "TimeOut",
        "SendCount",
        "SendSyncCount",
        "AcceptCount",
        "ResponseTimeMin (ns)",
        "ResponseTimeAvg (ns)",
        "ResponseTimeMax (ns)",
        "ResponseTimeStd (ns)",
        "StatusTimeMin (ns)",
        "StatusTimeAvg (ns)",
        "StatusTimeMax (ns)",
        "StatusTimeStd (ns)",
        "NMRTimeMin (ns)",
        "NMRTimeAvg (ns)",
        "NMRTimeMax (ns)",
        "NMRTimeStd (ns)",
        "ComponentTimeMin (ns)",
        "ComponentTimeAvg (ns)",
        "ComponentTimeMax (ns)",
        "ComponentTimeStd (ns)",
        "ChannelTimeMin (ns)",
        "ChannelTimeAvg (ns)",
        "ChannelTimeMax (ns)",
        "ChannelTimeStd (ns)"
    };

    /**
     * List of descriptions of items for ComponsiteData construction.
     */
    private static final String ITEM_DESCRIPTIONS[] = {
        "Number of active MessageExchanges",
        "Number of active MessageExchanges Max",
        "Number of queued MessageExchanges",
        "Number of queued MessageExchanges Max",
        "Number of active Endpoints",
        "Number of requests sent",
        "Number of requests received",
        "Number of replies sent",
        "Number of replies received",
        "Number of faults sent",
        "Number of faults received",
        "Number of DONE requests sent",
        "Number of DONE requests received",
        "Number of ERROR requests sent",
        "Number of ERROR requests received",
        "Number of waits that didn't find any work",
        "Number of requests that timed out",
        "Number of Send operations",
        "Number of SendSync operations",
        "Number of Accept operations",
        "Response Time Min",
        "Response Time Avg",
        "Response Time Max",
        "Response Time Std",
        "Status Time Min",
        "Status Time Avg",
        "Status Time Max",
        "Status Time Std",
        "NMR Time Min",
        "NMR Time Avg",
        "NMR Time Max",
        "NMR Time Std",
        "Component Time Min",
        "Component Time Avg",
        "Component Time Max",
        "Component Time Std",
        "Channel Time Min",
        "Channel Time Avg",
        "Channel Time Max",
        "Channel Time Std"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType ITEM_TYPES[] = {
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG
    };

    public String               getName()
    {
        return (mChannelId);
    }

    public String[]             getEndpointNames()
    {
        String[]        results;
        Object[]        eps = mInternalEndpoints.toArray();

        results = new String[eps.length];
        for (int i = 0; i < eps.length; i++)
        {
            results[i] = ((RegisteredEndpoint)eps[i]).toExternalName();
        }
        return (results);
    }

    public String[]             getConsumingEndpointNames()
    {
        return mRegistry.getLinkedEndpointsByChannel(mChannelId);
    }

    public EndpointStatistics getEndpointStatistics(String name)
    {
        return (getEndpointByExternalName(name));
    }

    public CompositeData        getStatistics()
    {
        try
        {
            Object      values[];
            String      names[];
            String      descs[];
            OpenType    types[];
            boolean     enabled = // mMsgSvc.areStatisticsEnabled() ||
                                  mChannelTime.getCount() != 0;

            if (enabled)
            {
                values = new Object[ITEMS_BASE + ITEMS_EXTRA];
                types = ITEM_TYPES;
                names = ITEM_NAMES;
                descs = ITEM_DESCRIPTIONS;
            }
            else
            {
                values = new Object[ITEMS_BASE];
                types = new OpenType[ITEMS_BASE];
                System.arraycopy(ITEM_TYPES, 0, types, 0, ITEMS_BASE);
                names = new String[ITEMS_BASE];
                System.arraycopy(ITEM_NAMES, 0, names, 0, ITEMS_BASE);
                descs = new String[ITEMS_BASE];
                System.arraycopy(ITEM_DESCRIPTIONS, 0, descs, 0, ITEMS_BASE);
            }

            values[0] = (long)(mActive.size());
            values[1] = mMaxActive;
            values[2] = (long)(mQueue.size());
            values[3] = mMaxQueued;
            values[4] = (long)(mInternalEndpoints.size());
            values[5] = mSendRequest;
            values[6] = mReceiveRequest;
            values[7] = mSendReply;
            values[8] = mReceiveReply;
            values[9] = mSendFault;
            values[10] = mReceiveFault;
            values[11] = mSendDONE;
            values[12] = mReceiveDONE;
            values[13] = mSendERROR;
            values[14] = mReceiveERROR;
            values[15] = mDeadWait;
            values[16] = mTimeOut;
            values[17] = mSendCount;
            values[18] = mSendSyncCount;
            values[19] = mAcceptCount;
            if (enabled)
            {
                values[20] = mResponseTime.getMin();
                values[21] = (long)mResponseTime.getAverage();
                values[22] = mResponseTime.getMax();
                values[23] = (long)mResponseTime.getSd();
                values[24] = mStatusTime.getMin();
                values[25] = (long)mStatusTime.getAverage();
                values[26] = mStatusTime.getMax();
                values[27] = (long)mStatusTime.getSd();
                values[28] = mNMRTime.getMin();
                values[29] = (long)mNMRTime.getAverage();
                values[30] = mNMRTime.getMax();
                values[31] = (long)mNMRTime.getSd();
                values[32] = mComponentTime.getMin();
                values[33] = (long)mComponentTime.getAverage();
                values[34] = mComponentTime.getMax();
                values[35] = (long)mComponentTime.getSd();
                values[36] = mChannelTime.getMin();
                values[37] = (long)mChannelTime.getAverage();
                values[38] = mChannelTime.getMax();
                values[39] = (long)mChannelTime.getSd();
            }

            return new CompositeDataSupport(
                new CompositeType(
                    "EndpointStatistics",
                    "Endpoint statistics",
                    names, descs, types), names, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            ; // ignore this for now
        }
        return (null);
    }
    
    //----------------------------DebugHelper-----------------------------

    void activeSummary(SortedMap<String,String> summary)
    {
        for (Iterator m = mActive.values().iterator(); m.hasNext();)
        {
            MessageExchangeProxy        mep = (MessageExchangeProxy)m.next();

            if (!summary.containsKey(mep.getExchangeId()))
            {
                summary.put(mep.getExchangeId(), mep.getSummary());
            }
        }
    }

    //-------------------------------Object-------------------------------------

    public String toString()
    {
         StringBuilder        sb = new StringBuilder();
        
        sb.append("  DeliveryChannel: ");
        sb.append(mChannelId);
        sb.append("\n    State: ");
        sb.append(mIsClosed ? "CLOSED" : "OPEN");
        sb.append("  Transactional: ");
        sb.append(mTransactional ? "YES" : "NO");
        sb.append("  Acceptors: " + mAcceptors);
        sb.append("\n    DeadWaitCount: " + mDeadWait);
        sb.append("  TimeoutCount: " + mTimeOut);
        sb.append("\n    SendCount: " + mSendCount);
        sb.append("  SendSyncCount:" + mSendSyncCount);
        sb.append("  AcceptCount: " + mAcceptCount);
        sb.append("\n    SendRequest: " + mSendRequest);
        sb.append("  RecvRequest: " + mReceiveRequest);
        sb.append("  SendReply: " + mSendReply);
        sb.append("  RecvReply: " + mReceiveReply);
        sb.append("\n    SendDONE: " + mSendDONE);
        sb.append("  RecvDONE: " + mReceiveDONE);
        sb.append("  SendERROR: " + mSendERROR);
        sb.append("  RecvERROR: " + mReceiveERROR);
        sb.append("\n    SendFault: " + mSendFault);
        sb.append("  RecvFault: " + mReceiveFault);
        sb.append("\n");
        if (mChannelTime.getCount() != 0)
        {
            sb.append("    ResponseTime:  " + mResponseTime.toString());
            sb.append("\n    StatusTime:    " + mStatusTime.toString());
            sb.append("\n    ComponentTime: " + mComponentTime.toString());
            sb.append("\n    ChannelTime:   " + mChannelTime.toString());
            sb.append("\n    NMRTime:       " + mNMRTime.toString());
            sb.append("\n");
        }
        sb.append("    InternalEndpoints Count: ");
        sb.append(mInternalEndpoints.size());
        sb.append("\n");
        for (Iterator i = mInternalEndpoints.iterator(); i.hasNext(); )
        {
            sb.append(i.next().toString());
        }
        sb.append("    ExternalEndpoints Count: ");
        sb.append(mExternalEndpoints.size());
        sb.append("\n");
        for (Iterator i = mExternalEndpoints.iterator(); i.hasNext(); )
        {
            sb.append(i.next().toString());
            sb.append("\n");
        }
        String  cen[];
        sb.append("    ConsumingEndpoints Count: ");
        sb.append((cen = getConsumingEndpointNames()).length);
        sb.append("\n");
        for (String i : cen)
        {
            sb.append(mRegistry.getLinkedEndpointByName(i).toString());
        }
        sb.append("    Active MEP's  Count: ");
        sb.append(mActive.size());
        sb.append(" Max: " + mMaxActive + "\n");
        for (Iterator m = mActive.values().iterator(); m.hasNext();)
        {
            MessageExchangeProxy        mep = (MessageExchangeProxy)m.next();
            sb.append(mep.toString());
            sb.append("\n");
        }
        sb.append("    Queued MEP's Count: ");
        sb.append(mQueue.size());
        sb.append(" Max: " + mMaxQueued + "\n");
        for (Iterator m = mQueue.iterator(); m.hasNext();)
        {
            MessageExchangeProxy        mep = (MessageExchangeProxy)m.next();
            sb.append("      Exchange Id(");
            sb.append(mep.getExchangeId());
            sb.append(")\n");
        }
        if (mAcceptor != null)
        {
            sb.append(mAcceptor.toString());
        }
        return (sb.toString());
    }
}
