/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Interceptor.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;


import org.osgi.framework.Bundle;

/**
 * OSGi bundle containing JBI interceptors.
 * @author kcbabo
 */
public class InterceptorBundle extends JBIBundle {
    
    /**
     * Creates a new InterceptorBundle.
     * @param descriptor jbi.xml
     * @param bundle containing OSGi bundle
     */
    public InterceptorBundle(Bundle bundle) {
        super(null, bundle);
    }
    
    /**
     * I'm an interceptor.
     * @return interceptor type
     */
    public BundleType getType() {
        return BundleType.INTERCEPTOR;
    }
    
    /**
     * Returns symbolic name of bundle containing interceptor(s).
     * @return interceptor bundle symbolic name
     */
    public String getName() {
        return getBundle().getSymbolicName();
    }
    
}
