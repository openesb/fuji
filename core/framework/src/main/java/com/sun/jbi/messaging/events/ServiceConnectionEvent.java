/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceConnectionEvent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.events;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.xml.namespace.QName;
import org.osgi.service.event.Event;

public class ServiceConnectionEvent 
        extends AbstractEvent 
        implements java.io.Serializable
{
   public static final String ADDED_EVENT_TOPIC = "com/sun/jbi/messaging/events/ServiceConnectionEvent/ADD";
   public static final String REMOVED_EVENT_TOPIC = "com/sun/jbi/messaging/events/ServiceConnectionEvent/REMOVE";
   public static final String FROM_ENDPOINT = "from_endpoint";
   public static final String FROM_SERVICE  = "from_service";
   public static final String TO_SERVICE = "to_service";
   public static final String TO_ENDPOINT = "to_endpoint";
   public static final String TYPE = "type";
   private QName        _fromServiceName;
   private String       _fromEndpointName;
   private QName        _toServiceName;
   private String       _toEndpointName;
   private String       _type;
   private ACTION       _action;
   
    static public boolean isServiceConnectionEvent(org.osgi.service.event.Event event)
    {
        String	topic = (String)event.getTopic();

	if (topic.equals(ADDED_EVENT_TOPIC) || topic.equals(REMOVED_EVENT_TOPIC))
        {
	    return (true);
        }
        return (false);
    }
    
    static public ServiceConnectionEvent valueOf(org.osgi.service.event.Event event)
    {
        ServiceConnectionEvent   ee = new ServiceConnectionEvent();
        
        ee._fromServiceName = (QName)event.getProperty(FROM_SERVICE);
        ee._fromEndpointName = (String)event.getProperty(FROM_ENDPOINT);
        ee._toServiceName = (QName)event.getProperty(TO_SERVICE);
        ee._toEndpointName = (String)event.getProperty(TO_ENDPOINT);
        ee._type = (String)event.getProperty(TYPE);
        ee._owner = (String)event.getProperty(OWNER);
        if (event.getTopic().equals(ADDED_EVENT_TOPIC))
        {
            ee._action = ACTION.ADDED;
        }
        else
        {
            ee._action = ACTION.REMOVED;
        }
        return (ee);
    }
    
    static public ServiceConnectionEvent valueOf(Dictionary props)
    {
        ServiceConnectionEvent   ee = new ServiceConnectionEvent();

        ee._fromServiceName = (QName)props.get(FROM_SERVICE);
        ee._fromEndpointName = (String)props.get(FROM_ENDPOINT);
        ee._toServiceName = (QName)props.get(TO_SERVICE);
        ee._toEndpointName = (String)props.get(TO_ENDPOINT);
        ee._type = (String)props.get(TYPE);
        ee._owner = (String)props.get(OWNER);
        return (ee);
    }
    
    static public ServiceConnectionEvent valueOf(ACTION action, String ownerId, QName fromService, String fromEndpoint,
            QName toService, String toEndpoint, String type)
    {
        ServiceConnectionEvent   ee = new ServiceConnectionEvent();

        ee._owner = ownerId;
        ee._fromServiceName = fromService;
        ee._fromEndpointName = fromEndpoint;
        ee._toServiceName = toService;
        ee._toEndpointName = toEndpoint;
        ee._type = type;
        ee._action = action;
        return (ee);
    }
    
    public AbstractEvent getOpposite()
    {
        ServiceConnectionEvent  event = 
                ServiceConnectionEvent.valueOf(_action == ACTION.ADDED ?
                    ACTION.REMOVED : ACTION.ADDED, _owner, _fromServiceName,
                    _fromEndpointName, _toServiceName, _toEndpointName,
                    _type);
        return (event);
    }
    
    public Dictionary getProperties()
    {
        return (null);
    }
    
    public Event getEvent()
    {
        Event       e;
        Dictionary  d = (Dictionary)new Hashtable();
        
        d.put(FROM_SERVICE, _fromServiceName);
        d.put(FROM_ENDPOINT, _fromEndpointName);
        d.put(TO_SERVICE, _toServiceName);
        d.put(TO_ENDPOINT, _toEndpointName);
        d.put(TYPE, _type);
        d.put(OWNER, _owner);
        e = new Event(_action == ACTION.ADDED 
                ? ADDED_EVENT_TOPIC : REMOVED_EVENT_TOPIC, d);
        return (e);                
    }
    
    public ACTION getAction()
    {
        return (_action);
    }
    
    public QName getFromServiceName()
    {
        return (_fromServiceName);
    }
    
    public String getFromEndpointName()
    {
        return (_fromEndpointName);
    }
    
    public QName getToServiceName()
    {
        return (_toServiceName);
    }
    
    public String getToEndpointName()
    {
        return (_toEndpointName);
    }
    
    public String getType()
    {
        return (_type);
    }
}

