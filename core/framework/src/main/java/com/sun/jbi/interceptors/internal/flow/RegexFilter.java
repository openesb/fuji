/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegexFilter.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.interceptors.internal.MessageExchangeUtil;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jbi.messaging.MessageExchange;


/**
 * Regex filter
 * 
 * @author Sun Microsystems, Inc.
 */
public class RegexFilter 
    implements MessageFilter {
    
    Matcher             matcher_;
    String              name_;

    /**
     * Create a Regex Filter instance
     * @param expression - regex
     */
    public RegexFilter(String name, String expression)
    {
        name_ = name;
        matcher_ = Pattern.compile(expression).matcher(null);
    }

    public RegexFilter(String name, Map config) {
        name_ = name;
        matcher_ = Pattern.compile((String)config.get("exp")).matcher(null);
    }

    /**
     *  
     * @return true if the active message in the message exchange contains the
     *         specified regular expression.
     * @param exchange - exchange to filter
     */
    public boolean matches(MessageExchange exchange)
        throws Exception
    {
        boolean matches = false;
        ServiceMessage svcMsg = MessageExchangeUtil.getActiveMessage(exchange);
        
        Object payload = svcMsg.getPayload();
        
        if ( payload instanceof String )
        {
            if ( payload != null )
            {
                String message = (String) payload;
                matcher_.reset(message);
                matches = matcher_.matches();
            }
        }
        else
        {
            throw new Exception("Cannot filter a non-text message using Regex");
        }
        return matches ;
    }

    /**
     *
     * @return true if the active message in the message exchange contains the
     *         specified regular expression.
     * @param exchange - exchange to filter
     */
    public boolean matches(Object input)
        throws Exception
    {
        if (input instanceof String) {
            matcher_.reset((String)input);
            return (matcher_.matches());
        }
        return (false);
    }
    
    /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(MessageExchange exchange)
        throws Exception
     {
        return new Boolean(matches(exchange)).toString();
     }
    
   /**
     * Apply the filter to the current message in the exchange.
     * @param exchange - exchange to filter
     * @return the result of executing the filter
     */
     public String execute(Object input)
        throws Exception
     {
         if (input instanceof String) {
            return new Boolean(matches(input)).toString();
         }
         return (null);
     }

}
