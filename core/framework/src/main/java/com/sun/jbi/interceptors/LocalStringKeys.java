/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors;

/** i18n Message Keys.
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{    
    
    /** Interceptor */
    String INTERCEPTOR_WITH_ZERO_PARAMS       = "INTERCEPTOR_WITH_ZERO_PARAMS";
    String INTERCEPTOR_WITH_EXTRA_PARAMS      = "INTERCEPTOR_WITH_EXTRA_PARAMS";
    String INTERCEPTOR_NOT_INVOKED_NULL_MSG   = "INTERCEPTOR_NOT_INVOKED_NULL_MSG";
    String INTERCEPTOR_NOT_INVOKED_AS_INVALID = "INTERCEPTOR_NOT_INVOKED_AS_INVALID";
    String INTERCEPTOR_RETURNS_UNSUPPORTED_TYPE = "INTERCEPTOR_RETURNS_UNSUPPORTED_TYPE";
    String INTERCEPTOR_FAILED_INITIAL_UPDATE_CONFIG = "INTERCEPTOR_FAILED_INITIAL_UPDATE_CONFIG";
    String INTERCEPTOR_MISSING_SET_CONFIGURATION = "INTERCEPTOR_MISSING_SET_CONFIGURATION";
    String INTERCEPTOR_RESTRICTED_SET_CONFIGURATION  = "INTERCEPTOR_RESTRICTED_SET_CONFIGURATION";
    String INTERCEPTOR_FAILED_SET_CONFIGURATION  = "INTERCEPTOR_FAILED_SET_CONFIGURATION";
    String INTERCEPTOR_FAILED_DELETE_CONFIG      = "INTERCEPTOR_FAILED_DELETE_CONFIG";
    String INTERCEPTOR_SVC_MISSING_METHOD_PROP   = "INTERCEPTOR_SVC_MISSING_METHOD_PROP";
    String INTERCEPTOR_SVC_INVALID_METHOD_PROP   = "INTERCEPTOR_SVC_INVALID_METHOD_PROP";
    String INTERCEPTOR_FAILED_GET_CONFIG         = "INTERCEPTOR_FAILED_GET_CONFIG";
    String INTERCEPTOR_WITH_MULTIPLE_CONFIG      = "INTERCEPTOR_WITH_MULTIPLE_CONFIG";
    String INTERCEPTOR_SVC_MISSING_NAME_PROP     = "INTERCEPTOR_SVC_MISSING_NAME_PROP";
    String INTERCEPTOR_WITHOUT_CONFIG            = "INTERCEPTOR_WITHOUT_CONFIG";
    String INTERCEPTOR_CONTEXT_WRONG_TYPE        = "INTERCEPTOR_CONTEXT_WRONG_TYPE";
    String INTERCEPTOR_DUPLICATE_TYPE            = "INTERCEPTOR_DUPLICATE_TYPE";
    String INTERCEPTOR_RENAMED                   = "INTERCEPTOR_RENAMED";
    
    /** Split **/
    String SPLIT_ERROR_BAD_XPATH_EXPRESSION   = "SPLIT_ERROR_BAD_XPATH_EXPRESSION";
    String AGGREGATE_ERROR_TIMED_OUT          = "AGGREGATE_ERROR_TIMED_OUT";
}
