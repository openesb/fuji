/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeLoggerConfigurationService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;


import java.util.HashMap;
import java.util.Properties;
import java.util.Dictionary;
import org.osgi.framework.BundleContext;

import com.sun.jbi.configuration.Constants;
import org.osgi.service.cm.ManagedService;


/**
 * This is a managed service that is registered to provide a configuration interface
 * to the admin tools. This class is used to provide a service to configure various 
 * runtime loggers.
 */
public class RuntimeLoggerDisplayService implements ManagedService {
    
    /**
     * initial configuration for the configuration service
     */
    private Dictionary configuration_ = new Properties();
    
   
    /**
     * the service PID.
     */
    private static String SERVICE_PID = 
            
        Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR + "runtime-logger-display" +
                                  LoggingService.SEPARATOR + "runtime-logger-display";
                
    
    /**
     * Constructor
     * Creates a new LogConfigurationService with the provided PID
     * @param initalConf the inital configuration
     */
    public RuntimeLoggerDisplayService(BundleContext bundleCtx) {
        configuration_.put(org.osgi.framework.Constants.SERVICE_PID, SERVICE_PID);
    }

    
    /**
     * This method use to return the configuraiton of the LoggerConfiurationService
     */
    public Dictionary getConfiguration() {
        return configuration_;
    }
    
    /**
     * This method is called by the admin tools when the 
     * configuration for this service is updated.
     * @param configuration
     */
    public void updated(Dictionary configuration) {
    
        //no-op this service is only used by list-loggers
        
    }            

}
