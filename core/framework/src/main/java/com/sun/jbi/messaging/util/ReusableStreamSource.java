/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ReusableStreamSource.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

/**
 * <p>Acts as an holder for a transformation Source in the form
 * of a stream of XML markup.</p>
 *
 */
public class ReusableStreamSource extends StreamSource {
    /**
     * Construct a StreamSource from a byte stream.  Normally,
     * a stream should be used rather than a reader, so
     * the XML parser can resolve character encoding specified
     * by the XML declaration.
     *
     * <p>If this constructor is used to process a stylesheet, normally
     * setSystemId should also be called, so that relative URI references
     * can be resolved.</p>
     *
     * @param inputStream A valid InputStream reference to an XML stream.
     */
    public ReusableStreamSource(StreamSource source) {
        stream = source;
        reader = source.getReader();
        inputStream = source.getInputStream();
        systemId = source.getSystemId();
        publicId = source.getPublicId();
    }

    /**
     * Get the byte stream that was set with setByteStream.
     *
     * @return The byte stream that was set with setByteStream, or null
     * if setByteStream or the ByteStream constructor was not called.
     */
    public InputStream getInputStream() {
        if (inputStream != null) {
            if (streamBuffer == null)
                makeStreamCopy();
            if (streamBuffer != null)
                return (streamBuffer.getBufferStream());
        }
        return inputStream;
    }

    /**
     * Get the character stream that was set with setReader.
     *
     * @return The character stream that was set with setReader, or null
     * if setReader or the Reader constructor was not called.
     */
    public Reader getReader() {
        if (reader != null) {
            if (readerCopy == null)
                makeReaderCopy();
            if (readerCopy != null)
                return (new CharArrayReader(readerCopy));
        }
        return reader;
    }

    /**
     * Get the public identifier that was set with setPublicId.
     *
     * @return The public identifier that was set with setPublicId, or null
     * if setPublicId was not called.
     */
    public String getPublicId() {
        return publicId;
    }

    /**
     * Get the system identifier that was set with setSystemId.
     *
     * @return The system identifier that was set with setSystemId, or null
     * if setSystemId was not called.
     */
    public String getSystemId() {
        return systemId;
    }
    
    public Source getSource()
    {
        if (readerCopy == null && streamBuffer == null)
        {
            return ((Source)stream);
        }
        return (null);
    }

    public String getString()
    {
        String  result = "";

        if (reader != null) {
            if (readerCopy == null)
                makeReaderCopy();
            result = new String(readerCopy);
        } else if (inputStream != null) {
            if (streamBuffer == null)
                makeStreamCopy();
            result = streamBuffer.toString();
        }
        return (result);
    }
    
    private void makeReaderCopy()
    {
        CharArrayWriter car = new CharArrayWriter();
        char[]          buffer = new char[4096];
        int             count;
        
        try
        {
            while ((count = reader.read(buffer)) > 0) {
                car.write(buffer, 0, count);
            }
            readerCopy = car.toCharArray();        
        }
        catch (java.io.IOException ioEx) {            
        }
    }
    
    private void makeStreamCopy()
    {
        ByteBuffer       bb = new ByteBuffer();
        byte[]           buffer = new byte[4096];
        int              count;
        
        try
        {
            while ((count = inputStream.read(buffer)) > 0) {
                bb.write(buffer, 0, count);
            }
            streamBuffer = bb;        
        } 
        catch (java.io.IOException ioEx) {
        }
        
    }
    
    //////////////////////////////////////////////////////////////////////
    // Internal state.
    //////////////////////////////////////////////////////////////////////
    private Source  stream;
    
    /**
     * The public identifier for this input source, or null.
     */
    private String publicId;

    /**
     * The system identifier as a URL string, or null.
     */
    private String systemId;

    /**
     * The byte stream for this Source, or null.
     */
    private InputStream     inputStream;
    private ByteBuffer      streamBuffer;
    
    /**
     * The character stream for this Source, or null.
     */
    private Reader  reader;
    private char[]  readerCopy;
}

/**
 * Overrides BAOS to allow direct access to the underlying byte array.  This
 * gets around the implicit copy of toByteArray when it's not needed.
 */
class ByteBuffer extends ByteArrayOutputStream {
    /** Returns stream pointing directly to underlying byte buffer. */ 
    ByteArrayInputStream getBufferStream() {
        return new ByteArrayInputStream(buf, 0, count);
    }
}
