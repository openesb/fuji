/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchange.java - Last published on 3/13/2008
 *
 * Copyright 2008-2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ext;

import java.util.Set;

/**
 * This is the interface for extensions to the JSR 208 MessageExchange interface.
 */
public interface MessageExchange      
{
    /** 
     * This is used to check is a MessageExchange has timed out.
     *
     * @return true - This MessageExchange has timed out.
     */
    boolean     checkTimeout();

    /** 
     * This is used to terminate a MessageExchange.
     *
     * @return true - This MessageExchange was terminated.
     *          false - No change made.
     */    
    boolean     terminate(Exception e);
    
    /** 
     * This is used to determine if a ME is from a remote system.
     *
     * @return true - This MessageExchange represents a remote invocation.
     *          false - This MessageExchange represents a local invocation.
     */    
    boolean     isRemoteInvocation();
    
    /** 
     * This is used to return the set of properties that have changed since the 
     * last call to mergeProperties().
     *
     * @return Set  - Properties that have changed.
     */    
    Set         getDeltaProperties();
    
    /**
     * This is used to merge the changed properties and reset.
     */
    void        mergeProperties();

    void        setParent(javax.jbi.messaging.MessageExchange me);
}
