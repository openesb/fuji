/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPManager.java - Last published on May 6, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.clientservices.eip;

import com.sun.jbi.framework.StringTranslator;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyBundle;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyManager;
import com.sun.jbi.interceptors.internal.flow.AbstractFlow;
import com.sun.jbi.interceptors.internal.flow.DynamicEIP;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.xml.namespace.QName;

import org.ho.yaml.Yaml;

/**
 *
 * @author Sun Microsystems
 */
public class EIPManagerImpl implements EIPManager{
    
    /** The ServiceAssembly Manager. */
    private ServiceAssemblyManager saManager_;
    
    private Logger log_;
    
    private static StringTranslator translator_ = new StringTranslator(
            "com.sun.jbi.clientservices.eip", null);
    
    public EIPManagerImpl(){
        saManager_ = ServiceAssemblyManager.getInstance();
        log_ = Logger.getLogger(EIPManagerImpl.class.getPackage().getName());
    }
            
    /** Augment an existing eip with new configuration
     * 
     * @param config
     * @throws java.lang.Exception
     */
    public void add(HashMap config) 
            throws Exception
    {
        QName eipName = getEipName(config);
        add(eipName, config);
    }
    
    /** Augment an existing eip with new configuration
     * 
     * @param name
     * @param config
     * @throws java.lang.Exception
     */
    public void add(QName name, Object config) 
            throws Exception
    {
        DynamicEIP flow = getDynamicEipInstance(name);
        
        // If dynamic add the new config
        if (flow != null){ 
            
            // Check if duplicate
            String configId = getConfigId((HashMap)config);
            List<String> configIds = flow.listConfigs();
            if(configIds.contains(configId)){
                String errMsg = translator_.getString(LocalStringKeys.EIP_CFG_DUPLICATE, configId,  
                        name.getNamespaceURI() == null ? name.getLocalPart() : name.toString());
                throw new Exception(errMsg);
            }
            // Add it
            flow.add(config);
        }
    }
    

    
    /**
     * Remove identified configuration from the eip. 
     * 
     * @param name
     * @param configId
     * @throws java.lang.Exception
     */
    public void remove(QName name, String configId)
     throws Exception
    {
                
        DynamicEIP flow = getDynamicEipInstance(name);
        
        // If dynamic delete the config
        if (flow != null){
            // Check it exists
            List<String> configIds = flow.listConfigs();
            if(!configIds.contains(configId)){
                String errMsg = translator_.getString(LocalStringKeys.EIP_CFG_MISSING, configId,  
                        name.getNamespaceURI() == null ? name.getLocalPart() : name.toString());
                throw new Exception(errMsg);
            }
            
            // Remove it
            flow.remove(configId);
        }
    }
    
        
    /** Update existing eip configuration.
     * 
     * @param config
     * @throws java.lang.Exception
     */
    public void modify(HashMap config) 
            throws Exception
    {
        QName eipName = getEipName(config);
        modify(eipName, config);
    }
    
    /**
     * Update existing eip configuration.
     * 
     * @param name
     * @param config
     * @throws java.lang.Exception
     */
    public void modify(QName name, Object config) throws Exception
    {
                
        DynamicEIP flow = getDynamicEipInstance(name);
        
        // If dynamic update the config
        if (flow != null){
            // Check it exists
            String configId = getConfigId((HashMap)config);
            List<String> configIds = flow.listConfigs();
            if(!configIds.contains(configId)){
                String errMsg = translator_.getString(LocalStringKeys.EIP_CFG_MISSING, configId,  
                        name.getNamespaceURI() == null ? name.getLocalPart() : name.toString());
                throw new Exception(errMsg);
            }
            // Update it
            flow.modify(config);
        }
    }
    
    /**
     * List all the eip instance of a specific type.
     * 
     * @param name
     * @param type - type of flow
     * @return
     * @throws java.lang.Exception
     */
    public List<QName> list(String type) throws Exception
    {
        List<QName> flows = getAllFlows(type);
        return flows;
    }
    

    /**
     * List all the configuration identifiers for the eip instance
     * 
     * @param name - eip instance name
     * @return a list of configuration ids
     * @throws java.lang.Exception
     */
    public List<String> listConfigIds(QName name) throws Exception{
                
        List<String> configIds = new java.util.ArrayList();
        DynamicEIP flow = getDynamicEipInstance(name);
        
        if (flow != null ){
            // Get the flow information and return a YAML object
            configIds = flow.listConfigs();
        }
        return configIds;
    }
    
    /**
     * List all the configuration identifiers for the eip instance
     * 
     * @param name - eip instance name
     * @return YAML data for the eip instance and the configuration ids.
     * @throws java.lang.Exception
     */
    public HashMap listConfigIdsInfo(QName name) throws Exception{
                
        List<String> configIds = new java.util.ArrayList();
        DynamicEIP flow = getDynamicEipInstance(name);
        
        if (flow != null ){
            // Get the flow information and return a YAML object
            configIds = flow.listConfigs();
        }
        
        // Create a YAML friendly hashmap with the information
        HashMap yamlData = new HashMap();
        yamlData.put(EIPConfigKeys.TYPE.toString(), ((AbstractFlow)flow).getType());
        yamlData.put(EIPConfigKeys.NAME.toString(), ((AbstractFlow)flow).getName().getLocalPart());
        yamlData.put(EIPConfigKeys.APP_NS.toString(),((AbstractFlow)flow).getName().getNamespaceURI());
        yamlData.put(EIPConfigKeys.CONFIG_IDS.toString(), convertToString(configIds));
        return yamlData;
        
    }
    
    
    /**
     * Get the specified configuration for a  specific eip instance. 
     * A YAML object containing all pertinent configuration
     * information is returned.
     * 
     * @param name
     * @param configId - identifies a configuration
     * @return
     * @throws java.lang.Exception
     */
    public Object get(QName name, String configId) throws Exception
    {
        Object config = null;
        
        DynamicEIP flow = getDynamicEipInstance(name);
        
        if (flow != null ){
            // Get the flow information and return a YAML object
            config = flow.get(configId);
        }
        return config;
    }
    
     /**
      * Generate a configuration template which can be used to add/modify
      * a configuration. The template is specific to each eip.
      * @return template specific to eip
      */
     public Object generateTemplate(String type) throws Exception{
        
         Object template = null;
         // TODO: this is a static thing
        List<QName> flows = getAllFlows(type);
        
        if (!flows.isEmpty() ){
            
            DynamicEIP anyFlow = getDynamicEipInstance(flows.get(0));
            // Get the flow information and return a YAML object
            template = anyFlow.generateTemplate();
        } else {
            String errMsg = translator_.getString(LocalStringKeys.EIP_TEMPLATE_GEN_NO_INSTANCE, type);
            throw new Exception(errMsg);
        }     
        return template;
     }
     
    /** -------------------------------------------------------------------- **\
     *                       Private Helpers 
    \** -------------------------------------------------------------------- **/
    
    /**
     *  Get a specific eip instance. TODO: this would return a ManageableEIP 
     *  instance and not AbstractFlow.
     * 
     * @param type
     * @param name
     * @return
     */
    private AbstractFlow getEipInstance(QName name) throws Exception {
        
        log_.fine(" Looking up EIP instance with app-ns " + name.getNamespaceURI() + " and name " + name.getLocalPart());
        String nsURI = name.getNamespaceURI();   
        
        if ( nsURI == null || "".equals(nsURI)){
            
            // Look in all the service assemblies
            Collection<ServiceAssemblyBundle> saBundles = saManager_.getServiceAssemblies();
            AbstractFlow namedFlow = null;
            for ( ServiceAssemblyBundle saBundle : saBundles ){
                
                QName flowName = new QName(saBundle.getNamespace(), name.getLocalPart());
                // Get the flow with the specific name
                List<AbstractFlow> flows = saBundle.getFlows();
                log_.fine(" Looking up EIP instance with app-ns " + flowName.getNamespaceURI() + " and name " + flowName.getLocalPart());
                for(AbstractFlow flow: flows){
                    if ( flow.getName().equals(flowName)){
                        if ( namedFlow != null ){
                            String errMsg = translator_.getString(LocalStringKeys.EIP_INSTANCE_NEEDS_QUALIFICATION, 
                                        name.getLocalPart());
                            throw new Exception(errMsg);
                        } 
                        namedFlow = flow;
                    }
                }
            }
            if ( namedFlow != null ){
                return namedFlow;
            }
        } else {
            // Look in namespace specific assembly
            ServiceAssemblyBundle parentSaBundle = getServiceAssemblyBundle(nsURI);

            if ( parentSaBundle != null ){
                // Get the flow with the specific name
                List<AbstractFlow> flows = parentSaBundle.getFlows();

                for(AbstractFlow flow: flows){
                    if ( flow.getName().equals(name)){

                        return flow;
                    }
                }
            }
        }
        
        String errMsg = translator_.getString(LocalStringKeys.EIP_INSTANCE_NOT_FOUND, 
                        name.getNamespaceURI() == null ? name.getLocalPart() : name.toString());
        log_.fine(errMsg);
        return null;
        
    }
    
    /**
     *  Get a specific eip dynamic instance, if the eip instance is not dynamic
     *  or does not exist an exception is thrown.
     * 
     * @param type
     * @param name
     * @return
     */
    private DynamicEIP getDynamicEipInstance(QName name) throws Exception {
        
        AbstractFlow flow = getEipInstance(name);
        
        if ( flow != null ){
            
            if ( flow.isDynamic() && (flow instanceof DynamicEIP) ){
                return ((DynamicEIP) flow);
            } else {
                String errMsg = translator_.getString(LocalStringKeys.EIP_NOT_DYNAMIC, 
                        name.getNamespaceURI() == null ? name.getLocalPart() : name.toString());
                throw new Exception(errMsg);
            }
            
        } else {
            String errMsg = translator_.getString(LocalStringKeys.EIP_INSTANCE_NOT_FOUND, 
                        name.getNamespaceURI() == null ? name.getLocalPart() : name.toString());
            throw new Exception(errMsg);
        }  
    }
    
    
    /** Get a specific Service Assembly Bundle
     * 
     * @param String nsURI
     */
    private ServiceAssemblyBundle getServiceAssemblyBundle(String nsURI){
                
        ServiceAssemblyBundle theSaBundle = null;
        Collection<ServiceAssemblyBundle> saBundles = saManager_.getServiceAssemblies();
        for ( ServiceAssemblyBundle saBundle : saBundles ){
            if ( saBundle.getNamespace().equals(nsURI)){
                theSaBundle = saBundle;
            }
        }
        return theSaBundle;
    }
    
    private List<QName> getAllFlows(String type){
        
        List<QName> flowNames = new java.util.ArrayList();
        
        Collection<ServiceAssemblyBundle> saBundles = saManager_.getServiceAssemblies();
        for ( ServiceAssemblyBundle saBundle : saBundles ){
            List<AbstractFlow> flows = saBundle.getFlows();
            
            for(AbstractFlow flow: flows){
                
                if ( type != null ){
                    if (!type.equals(flow.getType())){
                        continue;
                    }
                }
                
                if ( flow.isDynamic() && (flow instanceof DynamicEIP) ) {
                    flowNames.add(flow.getName());
                }
            }
        }
        
        return flowNames;
        
    }
    
    /**
     * 
     * @param config
     * @return eip QName after parsing the configuration
     * @throws java.lang.Exception
     */
    private QName getEipName(HashMap config) throws Exception{
        String appNS = (String)config.get(EIPConfigKeys.APP_NS.toString());
        String name  = (String)config.get(EIPConfigKeys.NAME.toString());
        
        if ( name == null ){
            String errMsg = translator_.getString(LocalStringKeys.EIP_CFG_MISSING_EIP_NAME, config.toString());
            throw new Exception(errMsg);
        }
        return new QName(appNS, name);
    }
    
    /**
     * 
     */
    private String getConfigId(HashMap config) throws Exception{
        String configId = (String)config.get(EIPConfigKeys.CONFIG_ID.toString());
            
        if ( configId == null ){
            String errMsg = translator_.getString(LocalStringKeys.EIP_CFG_MISSING_CONFIG_ID, config.toString());
            throw new Exception(errMsg);
        }
        return configId;
    }
    
     /**
     * Convert the objects in the collection to string.
     *
     * @param set a non-empty set
     */
    private String convertToString(java.util.Collection colxn)
    {
        StringBuffer strBuf = new StringBuffer("");
        if ( !colxn.isEmpty() )
        {
            java.util.Iterator itr = colxn.iterator();
            while( itr.hasNext() )
            {
                strBuf.append(itr.next().toString());
                if ( itr.hasNext() )
                {
                    strBuf.append(",  ");
                };
            }
        }
        return strBuf.toString();
    }    

}
