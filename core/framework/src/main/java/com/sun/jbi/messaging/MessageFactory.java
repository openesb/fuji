/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.jbi.messaging.Fault;
import javax.jbi.messaging.NormalizedMessage;

/** Default message factory for NMS.
 * @author Sun Microsystems, Inc.
 */
public class MessageFactory
{
    /** Singleton reference. */
    private static MessageFactory mMe;
    
    /** No direct instantiation. */
    private MessageFactory()
    {
        
    }
    
    /** Retrieve an instance of this object.
     *  @return MessageFactory reference.
     */
    public static final MessageFactory getInstance()
    {
        if (mMe == null)
        {
            mMe = new MessageFactory();
        }
        
        return mMe;
    }
    
    /** Create a new normalized message.
     *  @return normalized message.
     *  @throws javax.jbi.messaging.MessagingException failed to create message.
     */
    public NormalizedMessage createMessage()
        throws javax.jbi.messaging.MessagingException
    {
        return new MessageImpl();
    }
    
    /** Create a new fault.
     *  @return fault
     *  @throws javax.jbi.messaging.MessagingException failed to create fault.
     */
    public Fault createFault()
        throws javax.jbi.messaging.MessagingException
    {
        return new FaultImpl();
    }
    
}
