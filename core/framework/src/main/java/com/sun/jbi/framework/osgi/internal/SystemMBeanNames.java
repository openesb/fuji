/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SystemMBeanNames.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.ObjectName;

/**
 * Helper class for creating system MBean Object Names.
 * @author kcbabo
 */
public class SystemMBeanNames {
    /** JMX domain root **/
    private static final String DOMAIN_ROOT = "com.sun.jbi";
    /** Carried over from current implementation. **/
    private static final String TYPE_KEY = "ControlType";
    /** Carried over from current implementation. **/
    private static final String SERVICE_KEY = "ServiceName";
    
    private static Logger logger_ = 
            Logger.getLogger(SystemMBeanNames.class.getPackage().getName());
    
    /**
     * Creates a system MBean name with the following type and local name.
     * @param type MBean type
     * @param name MBean local name 
     * @return shiny new ObjectName
     */
    public static ObjectName createSystemMBeanName(String type, String name) {
        ObjectName mbeanName = null;
        Hashtable props = new Hashtable();
        props.put(TYPE_KEY, type);
        props.put(SERVICE_KEY, name);
        
        try {
            mbeanName = new ObjectName(DOMAIN_ROOT, props);
        } catch (javax.management.MalformedObjectNameException ex) {
            logger_.log(Level.WARNING, "Failed to create custom MBean name", ex);
        }
        
        return mbeanName;
    }

    /**
     * Utility method to grab the JMX domain name.
     * @return JMX domain name
     */
    public static String getJmxDomainName() {
        return DOMAIN_ROOT;
    }
}
