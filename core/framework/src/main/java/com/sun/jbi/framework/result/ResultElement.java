/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ResultElement.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Base class for objects representing elements in a 208 management result
 * message.
 * @author kcbabo
 */
public abstract class ResultElement {
    private static Templates mTemplates = null;
    
    static {
        try {
            String xsl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
                    "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">"+
                        "<!-- Identify transform used to copy input to output. -->"+
                        "<xsl:template match=\"@*|node()\">"+
                            "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>"+
                        "</xsl:template>"+
                        "</xsl:stylesheet>";
            mTemplates = TransformerFactory.newInstance()
                    .newTemplates(new StreamSource(new StringReader(xsl)));
        }
        catch (Exception ex) { /* ignore */ }
    }
    
    public static final String NS_URI =
            "http://java.sun.com/xml/ns/jbi/management-message";
    
    protected Element element_;
    
    protected ResultElement(Element element) {
        element_ = element;
        
        // Check to see if we need to create a new element structure
        if (!element.hasChildNodes() && !element.hasAttributes()) {
            initialize();
        }
    }
    
    protected List<Element> findChildElements(Element parent, TaskElem child) {
        ArrayList<Element> list = new ArrayList<Element>();
        if (parent != null) {
            NodeList nodes = parent.getElementsByTagName(child.toString());
        
            for (int i = 0; i < nodes.getLength(); i++) {
                list.add((Element) nodes.item(i));
            }
        }
        
        return list;
    }
    
    protected Element getChildElement(TaskElem name) {
        Element element = null;
        NodeList nodes = element_.getElementsByTagName(name.toString());
        
        if (nodes.getLength() > 0) {
            element = (Element)nodes.item(0);
        }
        
        return element;
    }
    
    protected List<Element> getChildElements(TaskElem name) {
        return findChildElements(element_, name);
    }
    
    protected Element createChildElement(TaskElem name) {
        Element ele = element_.getOwnerDocument().createElement(name.toString());
        element_.appendChild(ele);
        return ele;
    }
    
    protected Element createChildElementBefore(TaskElem name, TaskElem beforeName) {
        Element ele = element_.getOwnerDocument().createElement(name.toString());
        element_.insertBefore(ele, getChildElement(beforeName));
        return ele;
    }
    
    /**
     * This method is called when a result object is being created from scratch,
     * as opposed to starting with a pre-populated element structure.  
     * Implementing classes should create the underlying element structure when
     * this method is called.
     */
    protected abstract void initialize();
    
    public Element getElement() {
        return element_;
    }
    
    public void writeTo(OutputStream output) throws Exception {
//        Transformer transformer = 
//                TransformerFactory.newInstance().newTransformer();
//        transformer
        newTransformer().transform(new DOMSource(element_), new StreamResult(output));
    }
    
    public String asString() {
        StringWriter sw = new StringWriter();

        try {
            newTransformer().transform(new DOMSource(element_), new StreamResult(sw));
        }
        catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).warning(
                    "Failed to convert management message to DOM element: "+ e.getMessage());
        }
        
        return sw.toString();
    }
    
    protected Transformer newTransformer() throws Exception {
        return mTemplates.newTransformer();
    }
}
