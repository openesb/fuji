/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationHelper.java - Last published on Nov 19, 2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import com.sun.jbi.configuration.Constants;
import com.sun.jbi.configuration.ConfigurationType;
import com.sun.jbi.configuration.ServicePID;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.StringTranslator;
import com.sun.jbi.interceptors.LocalStringKeys;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.Configuration;


/**
 * Helper for Configuration operations like getting the 
 * Configuration Admin Service, loading configuration 
 * properties from bundle etc.
 * 
 * @author Sun Microsystems
 */
public class ConfigurationHelper {
    
    /** The Logger **/
    private static Logger log_ = Logger.getLogger(ConfigurationHelper.class.getPackage().getName());
    
    /** The StringTranslator **/
    private static StringTranslator 
            translator_ = new StringTranslator("com.sun.jbi.interceptors", null);
    
    /**
     * @return the framework bundle context
     */
    public static org.osgi.framework.BundleContext getBundleContext()
    {
        return Environment.getFrameworkBundleContext();
    }
    
    /**
     * The service pid has the format : [domain].[type].[name]
     * @param ref - interceptor service reference
     * @param configName - configuration name, will eventually come from service reference
     * @return a Service PID String for the service reference.
     * 
     */
    public static String getServicePID(ServiceReference ref, String configName)
    {
        String pid = "";
        if ( ref != null )
        {   
            String name =  InterceptionHelper.getInterceptorName(ref);
            name = ( name == null ? configName : name);
            pid = ServicePID.getServicePID(Constants.INTERCEPTOR_TYPE, name.toString());
            
        }
        
        return pid;
    }
    
    /**
     * 
     * @return the properties for the service 
     * @param svcRef - reference to the service
     */ 
    public static Hashtable getServiceProperties(ServiceReference svcRef)
    {
        Hashtable props = new Hashtable();
        
        String[] keys = svcRef.getPropertyKeys();
        
        for ( String key : keys )
        {
            Object value = svcRef.getProperty(key);
            if ( value != null )
            {
                props.put(key, value.toString());
            }
        }
        
        // If the service is not registered by the runtime then get the 
        // properties from the configuration file, if it exists.
        // If the service is registered by the runtime, the initial
        // configuration is already included in the service properties above
        if ( !InterceptionHelper.isRuntimeRegisteredService(svcRef))
        {
            Method mtd =  InterceptionHelper.getInterceptorMethod(svcRef);
            
            if ( mtd != null)
            {
                Object name = props.get(ServiceProperty.NAME.toString());
                if ( name == null )
                {
                    name = InterceptionHelper.generateInterceptorName(mtd);
                    props.put(ServiceProperty.NAME.toString(), name.toString());
                }
                props = getInitialConfiguration(svcRef.getBundle(), mtd, props, false );
            }
        }
        return props;
    }
    
    /** 
     * @return a reference to the Configuration Admin Service, if one exists,
     *         null otherwise
     */
    public static ConfigurationAdmin getConfigurationAdminService()
    {
        ConfigurationAdmin configAdminSvc = null;
        
        ServiceReference configAdminServiceRef =  
            getBundleContext().getServiceReference(ConfigurationAdmin.class.getName()); 
        
        if ( configAdminServiceRef != null )
        {
            configAdminSvc = (ConfigurationAdmin)getBundleContext().getService(configAdminServiceRef);
        }
        return configAdminSvc;
    }

    /**
     * If the configuration is not defined in the config admin service, then
     * look for the initial configuration file and load those properties.
     * 
     * The properties from the initial configuration file override the 
     * properties being passed in ( these are the interceptor service properties )
     * 
     * @param bundle - the bundle in which the interceptor class and configuration
     *                 is packaged
     * @param method - the interceptor Method instance
     * @param properties - the service properties
     * @paran isRuntimeRegistered - a boolean indicating whether the
     *                 interceptor is registered by RT or not. In the later case
     *                 the service properties are not overriden. 
     * @return the merged property set. 
     */
    public static Hashtable getInitialConfiguration(Bundle bundle,
            Method method, Hashtable properties, boolean isRuntimeRegistered )
    {
        // Initial configuration properties from the bundle
        Properties props = new Properties();

        /** If there is name in the service properties, use that to get the
         *  path to the initial configuration file. If the name is not defined, 
         *  then the folder name is based on the package+classname+method
         */
        String interceptorName = (String)properties.get(ServiceProperty.NAME.toString());
        
        String interceptorConfigPath = getInterceptorPropertiesPath(interceptorName, method);
       
        // See if properties file is included
        java.net.URL resURL = bundle.getEntry(interceptorConfigPath); 

        if ( resURL != null )
        {
            try
            {
                // load the properties
                java.io.InputStream ios = resURL.openStream();
                props.load(ios);
                ios.close();
                log_.finest("Read initial configuration from file " + interceptorConfigPath + " and the loaded properties are " + props.toString());
            }
            catch ( java.io.IOException ioex )
            {
                log_.log(Level.FINE, "Could not read interceptor external configuration file: " 
                        + interceptorConfigPath, ioex);
            }
        }
        else
        {
            log_.finest("Missing interceptor configuration file " 
                    + interceptorConfigPath);
        }

        Hashtable primary, secondary;
        
        if (isRuntimeRegistered)
        {
            primary = props;
            secondary = properties;
        }
        else
        {
            primary = properties;
            secondary = props;
        }
        
        // Merge the properties: config file overrides service properties
        java.util.Enumeration keys = primary.keys();
        while ( keys.hasMoreElements())
        {
            Object key = keys.nextElement();
            secondary.put(key, primary.get(key));
        }
        
        // If the name is not there in the service properties, extract it from 
        // the config file, if one is included in the bundle
        if ( resURL != null )
        {
            String parentDir = new File(interceptorConfigPath).getParentFile().getName();
            secondary.put(ServiceProperty.NAME.toString(), parentDir);
        }
        return secondary;
    }
    
    /**
     * @param intProps - the properties to be filtered
     * @param all - if true all properties are filtered. This falg is
     *              is set to true when properties are filtered to be 
     *              passed onto a user interceptor.
     * @return the filtered property set, system properties are removed from this set 
     */
    public static Hashtable filterOutSystemProperties(Hashtable intProps, boolean all)
    {
        // System properties like service.id, service.pid are internal 
        // and not meant to be exposed for configuration
        Hashtable intConfig = new Hashtable();
        ArrayList systemProps = getSystemProperties(all);
        java.util.Enumeration keys = intProps.keys();
        while ( keys.hasMoreElements() )
        {
            String key = (String) keys.nextElement();
            
            if ( !systemProps.contains(key) )
            {
                intConfig.put(key, intProps.get(key));
            }
            
        }
        return intConfig;
    }
    
    /**
     * Delete the configuration for the bundle. 
     * 
     * @param bundleId - identification of a bundle whose configuration is
     *                   to be deleted.
     */
    public static void deleteConfiguration(long bundleId)
    {
        ConfigurationAdmin cfgAdmin = ConfigurationHelper.getConfigurationAdminService();

        if ( cfgAdmin != null )
        {
            try
            {
                try
                {
                    String pidQuery = ServicePID.generateQuery(com.sun.jbi.configuration.Constants.INTERCEPTOR_TYPE, null );
                    
                    // Get a list of all interceptor configurations
                    Configuration[] configs = cfgAdmin.listConfigurations(pidQuery);
                
                    if ( configs != null )
                    {
                        for ( Configuration config : configs )
                        {
                            // If the bundle id in the configuration matches bundleId, delete it
                            java.util.Dictionary configProps = config.getProperties();
                            Long configBundleId = Long.parseLong(configProps.get(ServiceProperty.BUNDLE_ID).toString());
                            
                            // Don't delete dynamicaly created instances based on interceptors defined in the bundle
                            boolean isDynamic = false;
                            Object dynProp = configProps.get(ServiceProperty.IS_DYNAMIC_INSTANCE);
                            if (dynProp != null){
                                isDynamic = Boolean.parseBoolean(dynProp.toString());
                            }
                            
                            if ( configBundleId.longValue() == bundleId && !isDynamic){
                                config.delete();
                                log_.fine("Deleted configuration for bundle  " + bundleId);
                            }
                        }
                    }
                } catch (org.osgi.framework.InvalidSyntaxException ex)
                {
                    // No need to I18N this, since this will be deleted
                    log_.log(Level.WARNING, "Failed to delete the configuration.", ex);
                }
               
            }
            catch ( java.io.IOException ioex )
            {
                String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_FAILED_DELETE_CONFIG, 
                        bundleId);
                log_.log(Level.WARNING, errMsg, ioex);
            }
        }
    }
    
    /**
     * Delete a specifi configuration by name.
     */
    public static void deleteConfiguration(String name){
        ConfigurationAdmin cfgAdmin = ConfigurationHelper.getConfigurationAdminService();
        Configuration cfg = null;
        
        if ( cfgAdmin != null )
        {
            try
            {
                try
                {
                    String pidQuery = ServicePID.generateQuery(com.sun.jbi.configuration.Constants.INTERCEPTOR_TYPE, null );
                    
                    // Get a list of all interceptor configurations
                    Configuration[] configs = cfgAdmin.listConfigurations(pidQuery);
                    
                    if ( configs != null )
                    {
                        int i=0;
                        for ( Configuration config : configs )
                        {
                            java.util.Dictionary configProps = config.getProperties();
                            
                            String cfgName = (String) configProps.get(ServiceProperty.NAME);

                            if ( cfgName.equals(name) ){
                                cfg = config;
                                break;
                            }
                        }
                        cfg.delete();
                    }
                } catch (org.osgi.framework.InvalidSyntaxException ex)
                {
                    log_.log(Level.WARNING, "Failed to delete  configuration.", ex);
                }
            }
            catch ( java.io.IOException ioex )
            {
                log_.log(Level.WARNING, ioex.toString(), ioex);
            }
        }
        
    }
    
    /**
     * 
     * 
     * @return configurations corresponding to all dynamic aspects
     */
    public static java.util.Dictionary[] getDynamicConfigurations()
    {
        ConfigurationAdmin cfgAdmin = ConfigurationHelper.getConfigurationAdminService();
        java.util.Dictionary[] dynConfigs = new java.util.Dictionary[0];
        
        if ( cfgAdmin != null )
        {
            try
            {
                try
                {
                    String pidQuery = ServicePID.generateQuery(com.sun.jbi.configuration.Constants.INTERCEPTOR_TYPE, null );
                    
                    // Get a list of all interceptor configurations
                    Configuration[] configs = cfgAdmin.listConfigurations(pidQuery);
                    
                    if ( configs != null )
                    {
                        dynConfigs = new java.util.Dictionary[configs.length];
                        int i=0;
                        for ( Configuration config : configs )
                        {
                            java.util.Dictionary configProps = config.getProperties();
                            
                            // Don't delete dynamicaly created instances based on interceptors defined in the bundle
                            boolean isDynamic = false;
                            Object dynProp = configProps.get(ServiceProperty.IS_DYNAMIC_INSTANCE);
                            if (dynProp != null){
                                isDynamic = Boolean.parseBoolean(dynProp.toString());
                            }
                            
                            if (isDynamic){
                                dynConfigs[i++] = config.getProperties();
                            }
                        }
                    }
                } catch (org.osgi.framework.InvalidSyntaxException ex)
                {
                    // No need to I18N this, since this will be deleted
                    log_.log(Level.WARNING, "Failed to get dynamic configuration.", ex);
                }
            }
            catch ( java.io.IOException ioex )
            {
                log_.log(Level.WARNING, ioex.toString(), ioex);
            }
        }
        return dynConfigs;
    }
    /**
     * Get the interceptor configuration based on the service reference. 
     * 
     * @param svcRef - interceptor service reference
     */
    public static Properties getConfiguration(ServiceReference svcRef)
    {
        Properties props = new Properties();
        String servicePID = getServicePID(svcRef, null);
        ConfigurationAdmin cfgAdmin = ConfigurationHelper.getConfigurationAdminService();
        
        if ( cfgAdmin != null )
        {
            try
            {
                Configuration config = cfgAdmin.getConfiguration(servicePID);
                if ( config != null )
                {

                    java.util.Dictionary dict = config.getProperties();
                    if ( dict != null )
                    {
                        java.util.Enumeration keys = dict.keys();
                        while ( keys.hasMoreElements() )
                        {
                            String key = keys.nextElement().toString();
                            props.setProperty(key, dict.get(key).toString());
                        }
                    }
                }
                else
                {
                    String infoMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_WITHOUT_CONFIG, 
                        servicePID);
                   log_.fine(infoMsg);
                }
            }
            catch ( Exception ex )
            {
                String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_FAILED_GET_CONFIG, 
                        servicePID);
                log_.log(Level.WARNING, errMsg, ex);
            }
        }
        return props;
    }
    
    /**
     * Create a new Configuration in the Configuration Admin Service. This operation
     * is to be invoked only if the configuration does not exist for the interceptor
     * 
     */
    public static void createConfiguration(ServiceReference svcRef)
    {    
        // For independently registered interceptors
        Object name =  svcRef.getProperty(ServiceProperty.NAME.toString());
        
        Method method = (Method) svcRef.getProperty(ServiceProperty.METHOD.toString());
        
        if ( name == null )
        {
            name = InterceptionHelper.generateInterceptorName(method);
        }
        
        String servicePID = getServicePID(svcRef, name.toString());
        
        Hashtable intProps = getServiceProperties(svcRef);
        
        intProps.put(org.osgi.framework.Constants.SERVICE_PID, servicePID);
        intProps.put(ServiceProperty.NAME.toString(), name.toString());

        // Filter out internal system properties
        Hashtable intConfig = filterOutSystemProperties(intProps, false);
        
        ConfigurationAdmin cfgAdmin = getConfigurationAdminService();
        
        if ( cfgAdmin != null )
        {
            try
            {
                // Right now enclosing the service pid in quotes, otherwise
                // Properties interprets the key=value pairs in the string incorrectly
                Configuration config = cfgAdmin.getConfiguration(servicePID);
                config.update(intConfig);
                log_.finest("Registered Configuration for interceptor with PID " + servicePID + " config = " + intConfig.toString() );
            }
            catch ( java.io.IOException ioex )
            {
                String errMsg = translator_.getString(
                        LocalStringKeys.INTERCEPTOR_FAILED_INITIAL_UPDATE_CONFIG, 
                        servicePID);
                log_.log(Level.WARNING, errMsg, ioex);
            }
        }
    }
    
    /**
     * @return true if configuration exists for interceptor in Configuration Admin Service 
     * 
     */
    public static boolean configExists(Method method, String name)
    {   
        boolean isConfigThere = false;
        String pidQuery = ServicePID.generateQuery(com.sun.jbi.configuration.Constants.INTERCEPTOR_TYPE, name);
        
        // If the configuration exists in the Config Admin use that, 
        // else get it from the packaged configuration file, if one exists
        ConfigurationAdmin configAdmin = getConfigurationAdminService();
        if ( configAdmin != null )
        {
            try
            {
                Configuration[] config = configAdmin.listConfigurations(pidQuery);
                if ( config != null && config.length > 0 )
                {
                    isConfigThere = true;
                }
            }
            catch (Exception ex)
            {
                // What else can we do here
                log_.log(Level.INFO, "Could not determine if interceptor configuration exists for method : " 
                            + method.getName() + " assuming false.", ex);
            }
        }
        return isConfigThere;
    }

    /**
     * 
     * @return list of system property names
     */
    public static ArrayList<String> getSystemProperties(boolean all)
    {
        ArrayList<String> list = new ArrayList();
        
        // OSGi service stuff
        list.add(org.osgi.framework.Constants.SERVICE_ID);
        //list.add(org.osgi.framework.Constants.SERVICE_PID);
        list.add(org.osgi.framework.Constants.OBJECTCLASS);
        
        // Stuff the runtime adds
        //list.add(ServiceProperty.METHOD.toString());
        list.add(ServiceProperty.RUNTIME_REGISTERED.toString());
        //list.add(ServiceProperty.BUNDLE_ID.toString());
        
        if ( all )
        {
            list.add(org.osgi.framework.Constants.SERVICE_PID);
            list.add(ServiceProperty.METHOD.toString());
        }
        
        return list;
    }
    
    /**
     * 
     * 
     * @param svcRef
     * @return
     */
    private static String getInterceptorPropertiesPath(String name, Method method)
    {
        StringBuffer path = new StringBuffer();
        final String fwdSlash = "/";
        
        path.append(fwdSlash);
        path.append(com.sun.jbi.configuration.Constants.META_INF);
        path.append(fwdSlash);
        path.append(com.sun.jbi.configuration.Constants.INTERCEPTOR_DIR_NAME);
        path.append(fwdSlash);
         
        if ( name != null )
        {
            path.append(name);
            
        }
        else
        {
            // Package/Classname
            path.append(method.getDeclaringClass().getName());
            path.append(".");

            // Method
            path.append(method.getName());
        }
        
        path.append(fwdSlash);
        path.append(Constants.INTERCEPTOR_CONFIG_FILENAME);
        return path.toString();  
    }
}
