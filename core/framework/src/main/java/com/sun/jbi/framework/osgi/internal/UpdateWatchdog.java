/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UpdateWatchdog.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import java.io.File;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

/**
 * Watchdog class which samples the file system at a specified interval to 
 * detect changes in bundle artifacts (jars).  If the timestamp on the jar 
 * is later (i.e. newer) than our recorded value, the watchdog calls update()
 * on the bundle to trigger OSGi update processing.
 * @author kcbabo
 */
public class UpdateWatchdog implements Runnable {
    
    // Default sample interval in milliseconds
    private static final long DEFAULT_SAMPLE_INTERVAL = 2000;
    
    // Bundle header used to enable dynamic module update
    public static final String DYNAMIC_UPDATE = "Dynamic-Update";
    
    // Sampling interval for changes to a bundle jar
    private long sampleInterval_;
    
    // Collection of watched bundles
    private ConcurrentHashMap<Long, WatchedBundle> watchedBundles_ = 
            new ConcurrentHashMap<Long, WatchedBundle>();
    
    private Logger logger_ = Logger.getLogger(
            UpdateWatchdog.class.getPackage().getName());
    
    // What we use to schedule the watchdog thread
    private ScheduledExecutorService scheduler_;
    
    /**
     * Create a new UpdateWatchdog using the default interval.
     */
    public UpdateWatchdog() {
        this(DEFAULT_SAMPLE_INTERVAL);
    }
    
    /**
     * Create a new UpdateWatchdog using the specified interval.
     * @param interval sampling interval in milliseconds
     */
    public UpdateWatchdog(long interval) {
        sampleInterval_ = interval;
    }

    /**
     * Iterate through our watched bundles, updating when necessary.
     */
    public void run() {
        for (WatchedBundle watch : watchedBundles_.values()) {
            if (watch.updateRequired()) {
                try {
                    watch.getBundle().update();
                }
                catch (Exception ex) {
                    logger_.log(Level.WARNING, "Auto-update of bundle failed.", ex);
                }
            }
        }
    }
    
    /**
     * Adds a bundle to the watchdog interest list.
     * @param bundle bundle to watch
     * @return true if the boolean can be watched, false otherwise
     */
    public synchronized boolean watch(Bundle bundle) {
        boolean watched = false;
        String updatePath = (String)bundle.getHeaders().get(
                Constants.BUNDLE_UPDATELOCATION);
        Boolean dynamicUpdate = Boolean.valueOf((String)bundle.getHeaders().get(
                DYNAMIC_UPDATE));
        
        if (updatePath != null && dynamicUpdate) {
            
            try {
                File updateFile = new File(new URL(updatePath).getFile());
                if (updateFile.exists() && updateFile.isFile()) {
                    watchedBundles_.put(bundle.getBundleId(), 
                            new WatchedBundle(bundle, updateFile));
                    watched = true;
                }
            }
            catch (java.net.MalformedURLException urlEx) {
                logger_.log(Level.FINE, "Unable to watch URL for bundle " + 
                        bundle.getSymbolicName(), urlEx);
            }
        }
        
        // kick off the scheduler if this is our first watched bundle
        if (watched && scheduler_ == null) {
            start();
        }
        
        return watched;
    }
    
    /**
     * Removes a bundle from the watchdog interest list
     * @param bundle bundle to stop watching
     */
    public void unwatch(Bundle bundle) {
        watchedBundles_.remove(bundle.getBundleId());
        
        // kill the scheduler if this was our last watched bundle
        if (watchedBundles_.isEmpty()) {
            stop();
        }
    }
    
    /**
     * Stop watching everything.
     */
    public void unwatchAll() {
        watchedBundles_.clear();
        // kill the scheduler if this was our last watched bundle
        stop();
    }
    
    /**
     * Provides a reference to the scheduler used by this watchdog instance.
     * @return scheduler
     */
    ExecutorService getScheduler() {
        return scheduler_;
    }
    
    /**
     * Start the scheduler.
     */
    synchronized void start() {
        scheduler_ = Executors.newSingleThreadScheduledExecutor();
        scheduler_.scheduleAtFixedRate(this, sampleInterval_, 
                sampleInterval_, TimeUnit.MILLISECONDS);
    }
    
    /**
     * Stop the scheduler.
     */
    synchronized void stop() {
        if (scheduler_ != null) {
            scheduler_.shutdown();
            scheduler_ = null;
        }
    }
    
    /**
     * Handy little data structure for keeping track of watched bundles.
     */
    class WatchedBundle {
        private Bundle bundle_;
        private File updatePath_;
        private long lastUpdate_;
        
        WatchedBundle(Bundle bundle, File updatePath) {
            bundle_     = bundle;
            updatePath_ = updatePath;
            lastUpdate_ = updatePath_.lastModified();
        }
        
        Bundle getBundle() {
            return bundle_;
        }
        
        /** Returns true if an update is required for the watched entry. */
        boolean updateRequired() {
            if (lastUpdate_ < updatePath_.lastModified()) {
                lastUpdate_ = updatePath_.lastModified();
                return true;
            }
            else {
                return false;
            }
        }
    }
}
