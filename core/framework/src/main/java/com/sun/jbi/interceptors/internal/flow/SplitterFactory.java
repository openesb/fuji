/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitterFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.Map;

import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.framework.osgi.internal.BundleUtil;

import org.glassfish.openesb.api.eip.split.Splitter;
import org.osgi.framework.Bundle;

import javax.xml.namespace.NamespaceContext;

/**
 * Interface for a splitter
 * 
 * @author Sun Microsystems, Inc.
 */
public class SplitterFactory { 
    
    static java.util.logging.Logger log_ = java.util.logging.Logger.getLogger("com.sun.jbi.interceptors");
    /**
     * Singleton
     * 
     */
    private static SplitterFactory factory_;
    
    private SplitterFactory(){};
    
    /**
     * Get an instance of the factory.
     * 
     */
    public static SplitterFactory getInstance()
    {
        if ( factory_ == null)
        {
            factory_ = new SplitterFactory();
        }
        return factory_;
    }
    
    /**
     * Create a splitter based on the split information
     * 
     * @param type - Split type
     * @param config - Map containing the split configuration. 
     */
    public Splitter createSplitter(String appNamespace, SplitType type, Map config, Bundle bundle)
        throws Exception
    {
        
        Splitter splitter = null;
        switch (type) {
            case XPATH:
                {         
                    HashMap namespaces = ConfigurationUtil.getNamespaces(config);
                    NamespaceContext namespaceCtx = new NamespaceContextImpl(appNamespace, namespaces);
                    splitter = new XpathSplitter();
                    // Check for context annotations
                    injectContext(new HashMap(config), bundle, namespaceCtx, splitter);
                    break;
                }
            case REGEX:
                {
                    splitter = new RegexSplitter();
                    // Check for context annotations
                    injectContext(new HashMap(config), bundle, null, splitter);
                    break;
                }
            case XSL:
                {
                    splitter = new XSLSplitter();
                    // Check for context annotations
                    injectContext(new HashMap(config), bundle, null, splitter);
                    break;
                }
            case JAVA:
                {
                    HashMap namespaces = ConfigurationUtil.getNamespaces(config);
                    NamespaceContext namespaceCtx = new NamespaceContextImpl(appNamespace, namespaces);
                    splitter = instantiateJavaClass(config, bundle, namespaceCtx);
                    injectContext(new HashMap(config), bundle, namespaceCtx, splitter);
                    break;
                }
            case HEADER:
                {
                    splitter = new HeaderSplitter();
                    // Check for context annotations
                    injectContext(new HashMap(config), bundle, null, splitter);
                    break;
                }
            default:
                {
                    throw new Exception("Unsupported split type " + type.toString());
                }
        }
        return splitter;
    }
        
        
    /**
     * The classname and packagename are always there in the configuration, 
     * instantiate this class from the bundle, make sure it implements
     * the Aggregate Type interface, return the instance
     */
    private static Splitter instantiateJavaClass(Map config, Bundle bundle, NamespaceContext namespaceCtx)
        throws Exception {
        
        String className   = (String)config.get(Flow.JavaConfigKeys.CLASSNAME.toString());
        String packageName = (String)config.get(Flow.JavaConfigKeys.PACKAGENAME.toString());
        
        String qualifiedName = packageName + "." + className;
        Class extClass = bundle.loadClass(qualifiedName);
        
        if ( BundleUtil.implementsInterface(extClass, Splitter.class) ){
            Splitter splitter = (Splitter)extClass.newInstance();
            
            return splitter;
        } else {
            throw new Exception("Class " + qualifiedName + " does not implement interface: org.glassfish.openesb.api.eip.split.Splitter");
        }
    }
    
    
    private void injectContext(HashMap config, Bundle bundle, NamespaceContext namespaceCtx, Object instance)
    {
            java.lang.reflect.Field[] all = instance.getClass().getFields();

            for(java.lang.reflect.Field field : all )
            {
                if ( field.isAnnotationPresent(org.glassfish.openesb.api.eip.Resource.class) )
                {
                    boolean isAccesible = field.isAccessible();
                    if (!isAccesible){
                        field.setAccessible(true);
                    }
                    // Set the context only if type of field is a subtype of BundleContext
                    if ( org.osgi.framework.Bundle.class.isAssignableFrom(field.getType())){
                        // set the bundle context
                        try{
                            field.set(instance, bundle);
                        }
                        catch(Exception ex)
                        {
                            log_.log(Level.FINE, "Failed to inject the Bundle Context", ex);
                        }
                    }
                    else if (Map.class.isAssignableFrom(field.getType())){
                        // set the configuration
                        try{
                            log_.fine("Setting the split configuration " + config);
                            field.set(instance, config);
                        }
                        catch(Exception ex)
                        {
                            log_.log(Level.FINE, "Failed to inject the flow configuration", ex);
                        }
                    }
                    else if (NamespaceContext.class.isAssignableFrom(field.getType())){
                        // set the namespace ctx
                        try{
                            field.set(instance, namespaceCtx);
                        }
                        catch(Exception ex)
                        {
                            log_.log(Level.FINE, "Failed to inject the namespace context", ex);
                        }
                    }
                    else
                    {
                        String warning = new String("Annotated field " + field.getName() + " in class " +
                            instance.getClass().getName() + " is of an unsupported context type " + field.getType().getName());
                        log_.warning(warning);
                    }
                    if (!isAccesible)
                    {
                        field.setAccessible(false);
                    }
                    
                }
            }
    }
}
