/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.clientservices.eip;

/** i18n Message Keys.
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{    
    
    public static String EIP_CFG_MISSING_CONFIG_ID = "EIP_CFG_MISSING_CONFIG_ID";
    public static String EIP_CFG_MISSING_EIP_NAME  = "EIP_CFG_MISSING_EIP_NAME";
    public static String EIP_NOT_DYNAMIC           = "EIP_NOT_DYNAMIC";
    public static String EIP_INSTANCE_NOT_FOUND    = "EIP_INSTANCE_NOT_FOUND";
    public static String EIP_CFG_DUPLICATE         = "EIP_CFG_DUPLICATE";
    public static String EIP_CFG_MISSING           = "EIP_CFG_MISSING";
    public static String EIP_INSTANCE_NEEDS_QUALIFICATION  = "EIP_INSTANCE_NEEDS_QUALIFICATION";
    public static String EIP_TEMPLATE_GEN_NO_INSTANCE = "EIP_TEMPLATE_GEN_NO_INSTANCE";
}
