/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.InstallationContext;
import com.sun.jbi.framework.descriptor.Jbi;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.component.Component;
import javax.jbi.component.Bootstrap;

import javax.jbi.component.ComponentLifeCycle;
import org.osgi.framework.Bundle;

/**
 * OSGi bundle representing a JBI component.
 * @author kcbabo
 */
public class ComponentBundle extends StatefulJBIBundle {
    
    private Component component_;
    private Bootstrap bootstrap_;
    private ComponentContext context_;
    private Logger logger_ = Logger.getLogger(
            ComponentBundle.class.getPackage().getName());
    
    private ClassLoader classLoader;
    
    /**
     * Creates a new ComponentBundle.
     * @param descriptor jbi.xml
     * @param bundle containing OSGi bundle
     */
    public ComponentBundle(Jbi descriptor, Bundle bundle) {
        super(descriptor, bundle);
        try{
            bootstrap_ = getBootstrap();
        } catch (Exception ex){
            logger_.severe(ex.getMessage());
        }
    }
    
    /**
     * I'm a component.
     * @return component type
     */
    public BundleType getType() {
        return BundleType.COMPONENT;
    }

    /**
     * Returns component name.
     * @return component name
     */
    public String getName() {
        return getDescriptor().getComponent().getIdentification().getName();
    }
    
    /**
     * Returns component type.
     * @return component type
     */
    public String getComponentType() {
        return getDescriptor().getComponent().getType();
    }
    
    /**
     * Returns the JBI component context for this component bundle.
     * @return component context
     */
    public ComponentContext getComponentContext() {
        return context_;
    }
    
    /**
     * Sets the component content for a component bundle.
     * @param context component context
     */
    void setComponentContext(ComponentContext context) {
        context_ = context;
    }

    /**
     * Returns the component's implementation of javax.jbi.Component.
     * @return instance of Component interface
     * @throws java.lang.Exception failed to load or create the component's 
     *  implementation of javax.jbi.Component
     */
    public synchronized Component getComponent() throws Exception {
        if (component_ == null) {
            Class componentClass = getBundle().loadClass(
                    getDescriptor().getComponent().getComponentClassName());
            component_ = (Component)componentClass.newInstance();
            
        }
        
        return component_;
    }

    

    /**
     * this will initiate the component ClassLoader , If running within
     * GlassFish and the component wants leverage the AppServer infrastructure,
     * else the behavior is same as that of module ClassLoader which loads the
     * module
     * 
     * @param componentClass
     */
    private void initComponentClassLoader(Class<?> componentClass) {

	if (classLoader != null) {
	    return ;
	}
	
	ClassLoader moduleCL = componentClass.getClassLoader();
	ClassLoader connectorCL = null;

	try {

	    Class<?> gClass = moduleCL.loadClass("org.glassfish.internal.api.Globals");
	    Method getMethod = gClass.getMethod("get", Class.class);

	    Class<?> clhClass = moduleCL.loadClass("org.glassfish.internal.api.ClassLoaderHierarchy");

	    Object clhInst = getMethod.invoke(null, clhClass);

	    Method getConnectorCLMethod = clhClass.getMethod("getConnectorClassLoader",String.class);

	    Object o = getConnectorCLMethod.invoke(clhInst,(Object)null);

	    connectorCL = (ClassLoader) o;

	} catch (Exception e) {
	    logger_.fine("Glassfish Appserver specific Class could not be loaded: "+e.getMessage());
	}

	if (connectorCL != null) {

	    ComponentClassLoader compCL = new ComponentClassLoader(moduleCL);
	    compCL.addClassLoader(connectorCL);

	    classLoader = compCL;

	} else {
	    classLoader = moduleCL;
	}

	
    }
    

    /**
     * Returns the component's implementation of javax.jbi.Component.Bootstrap
     * @return instance of Component Bootstrap
     * @throws java.lang.Exception failed to load or create the component's 
     *  implementation of javax.jbi.Component
     */
    public synchronized Bootstrap getBootstrap() throws Exception {
        if (bootstrap_ == null) {
            Class installerClazz;
            try {
                installerClazz = getBundle().loadClass(
                    getDescriptor().getComponent().getBootstrapClassName()); 
            }
            catch(Throwable ex) {
                throw new javax.jbi.JBIException(
                    "Unable to load component bootstrap class", ex);
            }
            try {
                bootstrap_ = (Bootstrap)installerClazz.newInstance();
            }
            catch(Throwable ex) {
                throw new javax.jbi.JBIException(
                    "Unable to create component bootstrap instance", ex);
            }
        }

        
        return bootstrap_;
    }
    
    /**
     * Recover a component bundle and call the component's 208 lifecycle SPI.
     * Calling start on an OSGi bundle translates to init() + start() for a JBI
     * component. This method normally would be called in response to an OSGi
     * bundle STARTED event.
     */
    public void recover()
            throws Exception {

        if ( getState() != JBIState.RECOVERING )
        {
            ComponentLifeCycle lifeCycle;
            logger_.info("Starting component " + this.getName());
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try {
                lifeCycle = this.getComponent().getLifeCycle();

                /**
                 * initialize the component ClassLoader,
                 */
                initComponentClassLoader(lifeCycle.getClass());


                // Set the context class loader just in case the component needs it
                Thread.currentThread().setContextClassLoader(this.getClassLoader());

                // Fire 'er up!
                lifeCycle.init(this.getComponentContext());
                setState(JBIState.RECOVERING);
            }catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to start component " +
                        this.getName(), ex);
                throw ex;
            }finally {
                // Always restore the original context class loader
                Thread.currentThread().setContextClassLoader(cl);
            }
        }
    }
        
    /**
     * Start a component bundle and call the component's 208 lifecycle SPI.
     * Calling start on an OSGi bundle translates to init() + start() for a JBI
     * component. This method normally would be called in response to an OSGi
     * bundle STARTED event.
     */
    public void start() 
            throws Exception {
        
        if ( getState() != JBIState.STARTED)
        {
            ComponentLifeCycle lifeCycle;
            logger_.info("Starting component " + this.getName());
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try {
                lifeCycle = this.getComponent().getLifeCycle();
                if (getState() != JBIState.RECOVERING) {
                    /**
                     * initialize the component ClassLoader,
                     */
                    initComponentClassLoader(lifeCycle.getClass());


                    // Set the context class loader just in case the component needs it
                    Thread.currentThread().setContextClassLoader(this.getClassLoader());

                    // Fire 'er up!
                    lifeCycle.init(this.getComponentContext());
                }
                lifeCycle.start();
                setState(JBIState.STARTED);
            }catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to start component " + 
                        this.getName(), ex);
                throw ex;
            }finally {
                // Always restore the original context class loader
                Thread.currentThread().setContextClassLoader(cl);
            }
        }
    }

    /**
     * Stop a component bundle and call the component's 208 lifecycle
     * SPI.  Calling stop on an OSGi bundle translates to stop() + shutDown() for
     * a JBI component.  This method normally would be called in response to an
     * OSGi bundle STOPPED event.
     */
    public void stop() throws Exception {
        
        if ( getState() != JBIState.STOPPED || getState() != JBIState.SHUTDOWN )
        {
            ComponentLifeCycle lifeCycle;
            logger_.info("Stopping component " + this.getName());
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try {
                lifeCycle = this.getComponent().getLifeCycle();
                // Set the context class loader just in case the component needs it
                Thread.currentThread().setContextClassLoader(this.getClassLoader());
                lifeCycle.stop();
                setState(JBIState.STOPPED);

            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to stop component " 
                        + this.getName(), ex);
                throw ex;
            } finally {
                // Always restore the original context class loader
                Thread.currentThread().setContextClassLoader(cl);

            }
        }
    }
    
    /**
     * Shut down a component bundle and call the component's 208 lifecycle
     * SPI.  Calling stop on an OSGi bundle translates to stop() + shutDown() for
     * a JBI component.  This method normally would be called in response to an
     * OSGi bundle STOPPED event.
     */
    public void shutDown() throws Exception {
        
        if ( getState() != JBIState.SHUTDOWN )
        {
            ComponentLifeCycle lifeCycle;
            logger_.info("Shutting down component " + this.getName());
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            try {
                lifeCycle = this.getComponent().getLifeCycle();
                // Set the context class loader just in case the component needs it
                Thread.currentThread().setContextClassLoader(this.getClassLoader());
                lifeCycle.shutDown();
                setState(JBIState.SHUTDOWN);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to shutDown component " 
                        + this.getName(), ex);
                throw ex;
            } finally {
                // Always restore the original context class loader
                Thread.currentThread().setContextClassLoader(cl);
            }
        }
    }
    
    /**
     * Uninstall a component bundle and call the component's 208 boostrap 
     * SPI to uninstall the component.
     */
    public void uninstall() 
            throws Exception {
        
            Bootstrap bootstrap;
            logger_.info("Uninstalling component " + this.getName());
            try {
                bootstrap = this.getBootstrap();
                // Create an Installation context
                InstallationContext ctx = new InstallationContext(this, false);
                bootstrap.init(ctx);
                bootstrap.onUninstall();
                bootstrap.cleanUp();
            }catch (Exception ex) {
                logger_.log(Level.WARNING, "Failed to uninstall component " + 
                        this.getName(), ex);
                throw ex;
            }  
    }

    /**
     * @return the classLoader
     */
    public ClassLoader getClassLoader() {
        return classLoader;
    }

}
