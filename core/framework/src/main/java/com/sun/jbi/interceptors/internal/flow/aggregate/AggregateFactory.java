/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AggregateFactory.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow.aggregate;

import org.osgi.framework.Bundle;

import java.util.Properties;
import java.util.logging.Logger;

import com.sun.jbi.fuji.ifl.Flow;
import com.sun.jbi.framework.osgi.internal.BundleUtil;

import org.glassfish.openesb.api.eip.aggregate.Aggregate;


/**
 * Factory which creates an appropriate aggregator based on type
 * 
 * @author Sun Microsystems, Inc.
 */
public class AggregateFactory  {
    private Logger    log_ = Logger.getLogger("com.sun.jbi.interceptors");
    
    
    
    /**
     * Create an aggregate type extension based on the type and configuration
     * 
     * @param type - extension type
     * @param config - map containing configuration
     */
    public static Aggregate createAggregator(AggregateType type, Properties config, Bundle bundle)
        throws Exception{
        
        Aggregate typeExt = null;
        switch ( type ) {
            
            case SET:
                
                typeExt = new SetAggregate();
                break;
                
            case JAVA:
                
                typeExt = instantiateJavaClass(config, bundle);
                break;
        }
        
        return typeExt;
    };

     
    /**
     * The classname and packagename are always there in the configuration, 
     * instantiate this class from the bundle, make sure it implements
     * the Aggregate Type interface, return the instance
     */
    private static Aggregate instantiateJavaClass(Properties config, Bundle bundle)
        throws Exception {
        
        String className   = (String)config.get(Flow.JavaConfigKeys.CLASSNAME.toString());
        String packageName = (String)config.get(Flow.JavaConfigKeys.PACKAGENAME.toString());
        
        String qualifiedName = packageName + "." + className;
        Class extClass = bundle.loadClass(qualifiedName);
        
        if ( BundleUtil.implementsInterface(extClass, Aggregate.class) ){
            return (Aggregate)extClass.newInstance();
        } else {
            throw new Exception("Class " + qualifiedName + " does not implement interface: org.glassfish.openesb.api.eip.Aggregate");
        }
    }

    private AggregateFactory(){};
}
