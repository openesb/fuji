/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLoggerConfigurationService.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.logging;

import java.util.logging.Level;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import com.sun.jbi.configuration.Constants;
import com.sun.jbi.framework.osgi.internal.ComponentContext;

/**
 * This is a managed service that is registered to provide a configuration interface
 * to the admin tools. This class is used to provide a service to configure various 
 * component loggers.
 */
public class ComponentLoggerConfigurationService extends LoggerConfigurationService {
    
    
    /**
     * Constructor
     * This constructor creates an instance of the LoggerConfigurationService for the
     * provided component.
     * @param bundleCtx component bundle context
     * @param componentCtx component context
     */
    public ComponentLoggerConfigurationService(ComponentContext componentCtx) {
        super(
            Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR + "component-logger" + LoggingService.SEPARATOR + componentCtx.getComponentName(),
            Constants.CONFIG_DOMAIN + LoggingService.SEPARATOR + "component-logger-display" + LoggingService.SEPARATOR + componentCtx.getComponentName(),
            componentCtx.getLoggingService().getLoggers());
    }
     
    
    /**
     * This method is used to delete configurations maintaiend for the given component
     */
    public void deleteConfigurations() {
        
        try {
            
            ConfigurationAdmin configAdmin;
            if ((configAdmin = getConfigurationAdminService()) != null) {
                                      

                StringBuffer filter = new StringBuffer();
                filter.append("(");
                filter.append(org.osgi.framework.Constants.SERVICE_PID);
                filter.append("=");
                filter.append(com.sun.jbi.configuration.Constants.class.getPackage().getName());
                filter.append(":");
                filter.append(logDisplayServicePID_);
                filter.append(")");


                Configuration[] configs = configAdmin.listConfigurations(filter.toString()); 

                if (configs != null && configs.length > 0) {
                    //delete the first one that matched  
                    configs[0].delete();
                }

                filter = new StringBuffer();
                filter.append("(");
                filter.append(org.osgi.framework.Constants.SERVICE_PID);
                filter.append("=");
                filter.append(com.sun.jbi.configuration.Constants.class.getPackage().getName());
                filter.append(":");
                filter.append(logConfigServicePID_);
                filter.append(")");
                
                configs = configAdmin.listConfigurations(filter.toString()); 
                if (configs != null && configs.length > 0) {
                    //delete the first one that matched  
                    configs[0].delete();
                }
                
             }
            
         } catch (Exception ex) {
            log_.log(Level.WARNING, translator_.getString(LocalStringKeys.CONFIG_DELETE_FAILED), ex);
         }         
         
     }
       

}
