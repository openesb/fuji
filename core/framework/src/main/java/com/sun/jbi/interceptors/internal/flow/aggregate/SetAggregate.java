/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Aggregate.java - Last published on Mar 31, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow.aggregate;

import org.glassfish.openesb.api.service.ServiceMessage;
import org.glassfish.openesb.api.eip.aggregate.Aggregate;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.glassfish.openesb.api.message.MessageProps;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.PriorityQueue;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;


/**
 * This interface defines the SPI for an Aggregate type
 *
 * @author Sun Microsystems
 */
public class SetAggregate implements Aggregate{
    
    /** Ordered Message Payloads */
    private class OrderedPayload {
        
        // Low order is higher priority
        long ord_ = 0;
        
        // Payload
        Object payload_;
        
        /**
         * 
         * @param order
         * @param payload
         */
        public OrderedPayload(long order, Object payload){
            ord_ = order;
            payload_ = payload;
        }
        
        /**
         * 
         * @return the payload
         */
        public Object getPayload(){
            return payload_;
        }
        
        /**
         * 
         * @return the order
         */
        public long getOrder(){
            return ord_;
        }
        
    }
    
    private enum Config
    {
        COUNT      ("count"),
        SIZE       ("size"),
        ORDER      ("order"),
        HEADER     ("header"),
        TRAILER    ("trailer");

        private String config_;

        Config(String config) {
            config_ = config;
        }

        @Override
        public String toString() {
            return config_;
        }
    }
    
    private enum Order
    {
        // Order based on Message Exchange creation time
        CREATE    ("create"),
        
        // Order based on the time the Message was intercepted
        RECEIPT   ("receipt"),
        
        // Order based on message sequence number
        SEQUENCE  ("sequence"),
        
        // no order
        DEFAULT ("default");

        private String value_;

        Order(String value) {
            value_ = value;
        }

        @Override
        public String toString() {
            return value_;
        }
    }

    private class GroupInfo {
        private Object                  corrId_;
        private long                    size_;

        GroupInfo(String id, long size) {
            corrId_ = id;
            size_ = size;
        }

        void setSize(long size)
        {
            size_ = size;
        }
    }
    
    // Configuration
    private int       count_;
    private int       size_;
    private String    header_;
    private String    trailer_;   
    private Order     order_;
    
    private String    DEF_XML_HEADER  = "<aggregated_messages>";
    private String    DEF_XML_TRAILER = "</aggregated_messages>";
    private String    XML_HEADER_PATTERN = "\\x3c[\\w]*\\x3E";
    private String    XML_TRAILER_PATTERN = "\\x3c\\x2F[\\w]*\\x3E";
    private Map<Object,GroupInfo>           correlations_;

    
    // State
    private int       messageCount_;
    
    // Message Handling Stuff
    private Transformer transformer_;
    private Comparator comparator_;
    private DocumentBuilder     builder_;
    
    // Default Correlation Id
    private static final String DEFAULT_CORRELATION_ID = "id";
    
    // Misc
    private Logger              log_        = Logger.getLogger(AggregateController.class.getPackage().getName());
    
    public SetAggregate()
            throws Exception
    {
        builder_ = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        transformer_ = TransformerFactory.newInstance().newTransformer();
        transformer_.setOutputProperty (
                javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");
        correlations_ = new HashMap();
        correlations_.put(DEFAULT_CORRELATION_ID, new GroupInfo(DEFAULT_CORRELATION_ID, 0));
    }
    
    /*-----------------------------------------------------------------------*\
                                Aggregat Type SPI implementation 
    \*-----------------------------------------------------------------------*/
    
    /**
     * Set the configuration for the type
     * 
     * @param config
     */
    public void setConfiguration(Properties config)
        throws Exception {
        // default config
        messageCount_  = 0;
        order_ = Order.DEFAULT; // DEFAULT = all messages are same, no specific order
        
        // actual config
        initConfig(config);
    }
    
    /**
     * 
     * @return the Correlation ID and other information from the message
     */
    public Object getCorrelationID(ServiceMessage message) {
        
        String      corrId = (String)message.getProperty(MessageProps.GROUP_ID.toString());
        String      count = (String)message.getProperty(MessageProps.MESSAGE_CT.toString());

        GroupInfo   group;

        if ( corrId != null ) {
            group = correlations_.get(corrId);
            if (group == null) {
                long size = 0;

                if (count != null) {
                    size = Long.valueOf(count);
                }
                correlations_.put(corrId, group = new GroupInfo(corrId, size));
            }
        } else {
            corrId = DEFAULT_CORRELATION_ID;
        }
        
        return corrId;
    }

    /**
     *  @return true if all the conditions required to aggregate the message have
     *          been satisfied.
     */
    public boolean isAggregateComplete(Object correlationId, ServiceMessage[] serviceMessages){

        boolean     send = false;
        GroupInfo   group = correlations_.get(correlationId);
        
        if ( serviceMessages.length == count_  ||
                (group != null && group.size_ == serviceMessages.length)) {
            correlations_.remove(correlationId);
            send = true;
        }
        
        return send;
    }
        
    
    /**
     * Aggregate the messages being passed in, into one payload
     * 
     * @param corrId  - correlation ID 
     * @param serviceMessages - the messages to aggregate
     */
    public Object aggregateMessages(Object corrId, ServiceMessage[] serviceMessages)
        throws Exception{
      
        return mergeMessages(serviceMessages);
    }
    
        
    /**
     * Order the messages
     * 
     * @param svcMessages 
     */
    private PriorityQueue<OrderedPayload> orderMessages(ServiceMessage[] svcMessages)
            throws Exception
    {
                
        PriorityQueue<OrderedPayload>    messageCache = null;
        
        if ( messageCache == null )
        {
            messageCache = new PriorityQueue(5, getOrderedPayloadComparator());
        };
        
        for( ServiceMessage svcMsg : svcMessages)
        {
        
            Object payload = svcMsg.getPayload();
            long ord = getMessageOrder(svcMsg);
            OrderedPayload orderedMsg = new OrderedPayload(ord, payload);
            messageCache.add(orderedMsg);
        }
        
        return messageCache;
    }
    /**
     * Aggregate all the cached messages into one and then create a Source out of it. 
     * 
     * @return the aggregated message
     */
    private Object mergeMessages(ServiceMessage[] svcMessages)
        throws Exception
    {
        Object document = null;
        StringWriter writer = null;
        boolean isXmlPayload = false;
        
        // Order the messages and then combine into one
        PriorityQueue<OrderedPayload> messageCache = orderMessages(svcMessages);
        
        while ( messageCache.peek() != null)
        {
            OrderedPayload msg = messageCache.poll();
            Object obj = msg.getPayload();
            if ( writer == null )
            {
                writer = new StringWriter();  
                if ( obj instanceof Node ){
                    validateAndUpdateXmlRootElements();
                }
                if ( header_ != null )
                {
                    writer.append(header_);
                }
            }
            
            if ( obj instanceof Node )
            {
                Node doc = (Node) obj;
                
                // Now append the node content as string
                writer.append(NodeToString(doc));
                isXmlPayload = true;
            }
            else if ( obj instanceof String )
            {
                writer.append((String)obj);
            }
                
        }
        
        // Finally add the trailer if one exists
        if ( trailer_ != null )
        {
            writer.append(trailer_);
        }

        if (log_.isLoggable(Level.FINER)) {
            log_.finer("The aggregated document being sent is : " + writer.toString());
        }
        
        try
        {
            document = createDocument(writer, isXmlPayload); 
        }catch(org.xml.sax.SAXParseException ex)
        {
            String errMsg = "Aggregate failed to send message \n" 
                    + writer.toString() 
                    + "\n The  message is not a valid XML document";
            log_.severe(errMsg );
            throw new Exception(errMsg);
        }
        
        return document;
        
    }
        
    /**
     * Parse the aggregate configuration and save the information. 
     * 
     * @param config - aggregate configuration properties in a string
     */
    void initConfig(Properties config)
        throws Exception
    {
        if ( config != null )
        {
            Enumeration keySet = config.propertyNames();

            while (keySet.hasMoreElements()) {
                String key  = (String)keySet.nextElement();
                String value = (String)config.getProperty(key);
                
                if ( !"".equals(value) )
                {
                    if ( key.equalsIgnoreCase(Config.COUNT.toString()))
                    {
                        count_ = Integer.parseInt(value);

                        if ( count_ <= 0 )
                        {
                            throw new  Exception("Invalid Aggregate configuration, count cannot be zero or negative : " + count_ );
                        }
                    }
                    else if ( key.equalsIgnoreCase(Config.SIZE.toString()))
                    {
                        // TODO: take care of size at end and convert to bytes
                        size_ = Integer.parseInt(value);
                    }
                    else if ( key.equalsIgnoreCase(Config.ORDER.toString()))
                    {   
                        for ( Order o: Order.values() )
                        {
                            if (o.toString().equals(value)){
                                order_ = o;
                            }
                        }
                    }
                    else if ( key.equalsIgnoreCase(Config.HEADER.toString()))
                    {
                        header_ = value;
                    }
                    else if ( key.equalsIgnoreCase(Config.TRAILER.toString()))
                    {
                        trailer_ = value;
                    }
                }
            }
        }
    }
    
     
    /**
     * In case of XML elements, the header and trailer should match.
     * - validate the syntax of the header and trailer
     * - If one is specified the other should be inferred
     * - If none are specified then use defaults
     */
    private void validateAndUpdateXmlRootElements(){
        
        if ( header_== null && trailer_== null){
            header_  = DEF_XML_HEADER;
            trailer_ = DEF_XML_TRAILER;
            return;
        }
        
        String headerTxt = getElementText(header_);
        String trailerTxt = getElementText(trailer_);
        
        if ( headerTxt != null && trailerTxt != null ){
            // They should be equal
            if (!headerTxt.equals(trailerTxt)){
                log_.warning("The XML header " + header_ + " and trailer " + trailer_ + 
                        " for the aggregated messages don't match, adjusting the trailer to be the same as the header ");
                trailerTxt = headerTxt;
            }
        }
        
        if ( headerTxt == null && trailerTxt != null ){
            headerTxt = trailerTxt;
        } else if ( headerTxt != null && trailerTxt == null ){
            trailerTxt = headerTxt;
        }
        
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("<"); 
        strBuf.append(headerTxt);
        strBuf.append(">");
        header_ = strBuf.toString();
        
        strBuf = new StringBuffer();
        strBuf.append("<");
        strBuf.append("/");
        strBuf.append(trailerTxt);
        strBuf.append(">");
        trailer_ = strBuf.toString();
        
    }
    
    /**
     * @return the text name of the element
     * 
     */
    public String getElementText(String elementStr){
        
        if ( elementStr == null ){
            return null;
        }
        
        String txt = null;
        int i1 = elementStr.indexOf("<");
        int i2 = elementStr.indexOf(">");
        if ( i2 == -1 ) { i2 = elementStr.length();}
        
        String tmp = elementStr.substring(i1+1, i2);
        int i11 = tmp.indexOf("/");
        if ( i11 == -1 ){
            txt = tmp;
        } else {
            txt = tmp.substring(i11+1, tmp.length());
        }
        
        return txt;
    }
    
     /**
      * Create a comparator for the ordered messages
      * 
      * @return a new comparator instance for the ordered messages
      */
     private Comparator createOrderedPayloadComparator()
     {
         Comparator<OrderedPayload> comp = new Comparator<OrderedPayload> () 
         {
            public int compare(OrderedPayload msg1, OrderedPayload msg2) 
            {
                Long ordMsg1 = msg1.getOrder();
                Long ordMsg2 = msg2.getOrder();
                
                return ordMsg1.compareTo(ordMsg2);
            }
        };
        
        return comp;
     }
     
     /**
      * @return the comparator to be used for ordering messages being sent after aggregation
      */
     private Comparator getOrderedPayloadComparator()
     {
         return ( (comparator_ == null) ?  createOrderedPayloadComparator() : comparator_);
     }
     
        /**
     * Convert an element to String
     *
     * @param element - the element to convert to String
     */
    private String NodeToString(Node node)
        throws Exception
    {
        String nodeAsString = null;
        if ( node.getNodeType() == Node.ELEMENT_NODE)
        {
            // xsd:anyType
            StringWriter sw = new StringWriter();
            transformer_.transform(new DOMSource(node), new StreamResult(sw));
            nodeAsString = sw.toString();
        }
        else if ( node.getNodeType() == Node.TEXT_NODE )
        {
            // xsd:string
            nodeAsString = node.getNodeValue();
        }
        
        return nodeAsString;
    }
    
    /**
     * Create a message object from the combined message. If the isXML flag is 
     * true, the message content is XML
     * 
     * @param writer - StringWriter which has the message string 
     * @param isXml - flag if set to true it indicates that the message is an XML document
     */
    private Object createDocument(StringWriter writer, boolean isXml)
        throws Exception
    {
        String stringPayload = writer.toString();
        
        if ( isXml )
        {
            return builder_.parse(new InputSource(new StringReader(stringPayload)));
        }
        else
        {
            return stringPayload;
        }
    }
    
     /**
      * @return the order of message, based on
      *         the aggregate configuration i.e. the order of the aggregated
      *         messages could be {create, receipt, sequence}
      */
     private long getMessageOrder(ServiceMessage svcMsg)
     {
         long order = 0;
         switch (order_)
         {
             
             case CREATE: {                
                 String msgOrder = (String)svcMsg.getProperty(MessageProps.MESSAGE_TS.toString());
                 if ( msgOrder != null ){
                     try {
                        order = Long.valueOf(msgOrder.trim()).longValue();
                     } catch ( NumberFormatException ex){
                         log_.log(Level.FINE, 
                                  "Aggregate failed to get message timestamp for message " + svcMsg.toString(), 
                                  ex);
                     }
                 }
                 break;
             }
             
             case RECEIPT:
                 order = System.currentTimeMillis();
                 break;
                 
             case SEQUENCE:{
                 String msgOrder = (String)svcMsg.getProperty(MessageProps.MESSAGE_ID.toString());
                 if ( msgOrder != null )
                 {
                     try
                     {
                        order = Long.valueOf(msgOrder.trim()).longValue();
                     } catch ( NumberFormatException ex)
                     {
                         log_.log(Level.FINE, 
                                 "Aggregate failed to get message id for message " + svcMsg.toString(), 
                                 ex);
                     }
                 }
                 break;
             }
             
             default:
                 break;
         }
         return order;
     }  
}
