/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StringTranslator.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi;

/**
 * This is the interface to the String Translator, which provides services
 * for internationalization of messages to all services running inside the
 * JBI environment.
 *
 * @author Sun Microsystems, Inc.
 */
public interface StringTranslator
{
   /**
    * Get a localized string using the specified resource key.
    * @param key the key to the localized string in the resource bundle.
    * @return the localized string.
    */
    String getString(
        String key);

   /**
    * Get a localized string using the specified resource key. Handle one
    * message insert.
    * @param key the key to the localized string in the resource bundle.
    * @param insert1 the message insert.
    * @return the localized string formatted with the message insert.
    */
    String getString(
        String key,
        Object insert1);

   /**
    * Get a localized string using the specified resource key. Handle two
    * message inserts.
    * @param key the key to the localized string in the resource bundle.
    * @param insert1 the first message insert.
    * @param insert2 the second message insert.
    * @return the localized string formatted with the message inserts.
    */
    String getString(
        String key,
        Object insert1,
        Object insert2);

   /**
    * Get a localized string using the specified resource key. Handle three
    * message inserts.
    * @param key the key to the localized string in the resource bundle.
    * @param insert1 the first message insert.
    * @param insert2 the second message insert.
    * @param insert3 the third message insert.
    * @return the localized string formatted with the message inserts.
    */
    String getString(
        String key,
        Object insert1,
        Object insert2,
        Object insert3);

   /**
    * Get a localized string using the specified resource key. Handle four
    * message inserts.
    * @param key the key to the localized string in the resource bundle.
    * @param insert1 the first message insert.
    * @param insert2 the second message insert.
    * @param insert3 the third message insert.
    * @param insert4 the fourth message insert.
    * @return the localized string formatted with the message inserts.
    */
    String getString(
        String key,
        Object insert1,
        Object insert2,
        Object insert3,
        Object insert4);

   /**
    * Get a localized string using the specified resource key. Handle any
    * number of message inserts.
    * @param key the key to the localized string in the resource bundle.
    * @param inserts the array of message inserts.
    * @return the localized string formatted with the message inserts.
    */
    String getString(
        String key,
        Object[] inserts);

}
