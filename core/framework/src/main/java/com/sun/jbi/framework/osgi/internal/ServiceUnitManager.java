/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitManager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.result.ComponentTaskResult;
import com.sun.jbi.framework.result.ResultException;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.management.DeploymentException;

import org.osgi.framework.Bundle;

/**
 * Manages service unit bundles installed in the OSGi framework.
 * @author kcbabo
 */
public class ServiceUnitManager {
    
    // Service assemblies are exploded into this directory
    private static final String SU_DIR = "service-units";
    private Logger logger_ = Logger.getLogger(
            ServiceUnitManager.class.getPackage().getName());
    // References to SU bundles that we know about
    private Map<Long, ServiceUnitBundle> bundles_ =
            new ConcurrentHashMap<Long, ServiceUnitBundle>();

    private static ServiceUnitManager suManager_;
    
    /**
     * There is only one instance of this class
     * 
     */
    private ServiceUnitManager(){};
    
    /**
     * @return the instance of this class.
     */
    public static ServiceUnitManager getInstance() {
        if ( suManager_ == null ){
            suManager_ = new ServiceUnitManager();
        }
        
        return suManager_;
    }
    
    /**
     * Indicates whether a given bundle is known to the SU manager.
     * @param bundleId OSGi bundle id
     * @return true if the bundle has been registered with the SU manager, false
     * otherwise.
     */
    public boolean isRegistered(Long bundleId) {
        return bundles_.containsKey(bundleId);
    }

    /**
     * Returns a ServiceUnitBundle reference for the given bundle id.
     * @param bundleId bundle id
     * @return service unit bundle if the bundle has been registered, 
     * otherwise null.
     */
    public ServiceUnitBundle getServiceUnit(Long bundleId) {
        return bundles_.get(bundleId);
    }

    /**
     * Returns a ServiceUnitBundle for the given service unit name.  
     * Be careful when using this method when multiple versions of an SU may
     * exist.
     * @param name service unit name
     * @return service unit bundle if the bundle has been registered, 
     * otherwise null.
     */
    public ServiceUnitBundle getServiceUnit(String name) {
        ServiceUnitBundle serviceUnit = null;

        for (ServiceUnitBundle su : bundles_.values()) {
            if (su.getName().equals(name)) {
                serviceUnit = su;
                break;
            }
        }

        return serviceUnit;
    }

    /**
     * Returns a list of all service unit bundles that have been registered.
     * @return all registered SU bundles
     */
    public Collection<ServiceUnitBundle> getServiceUnits() {
        return bundles_.values();
    }
    
    
    /**
     * Invokes the appropriate component SPIs to start a service unit 
     * contained in an SU bundle.
     * @param bundleId bundle id for the service unit
     */
    public void startServiceUnit(Long bundleId) {
        if (isRegistered(bundleId)) {
            ServiceUnitBundle su = bundles_.get(bundleId);
            logger_.info("Starting JBI service unit " + su.getName());

            try {
                startServiceUnit(su);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Start of service unit failed", ex);
            }
        }
    }
    
    /**
     * Invokes the appropriate component SPIs to shut down a service unit 
     * contained in an SU bundle.
     * @param bundleId bundle id for the service unit
     */
    public void shutDownServiceUnit(Long bundleId) {
        if (isRegistered(bundleId)) {
            ServiceUnitBundle su = bundles_.get(bundleId);
            logger_.info("Shutting down JBI service unit " + su.getName());

            try {
                shutDownServiceUnit(su);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Shutdown of service unit failed", ex);
            }
        }
    }
    
    
    /**
     * Invokes the appropriate component SPIs to undeploy a service unit 
     * contained in an SU bundle.
     * @param bundleId bundle id for the service unit
     */
    public void undeployServiceUnit(Long bundleId) {
        if (isRegistered(bundleId)) {
            ServiceUnitBundle su = bundles_.get(bundleId);
            logger_.info("Undeploying JBI service unit " + su.getName());

            try {
                undeployServiceUnit(su);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, "Undeployment of service unit failed", ex);
            }
        }
    }
    
    
    /**
     * Invokes the appropriate component SPIs to deploy a service unit 
     * contained in an SU bundle.
     * @param bundle service unit bundle
     * @param jbiXml su descriptor
     */
    public void deployServiceUnit(Bundle bundle, Jbi jbiXml) {
        
        ServiceUnitBundle su = new ServiceUnitBundle(jbiXml, bundle);
        logger_.info("Deploying JBI service unit " + su.getName());

        try {
            registerServiceUnit(su);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            logger_.log(Level.WARNING, ex.getMessage());
            logger_.log(Level.FINE, ex.getMessage(), ex);
            try
            {
                bundle.uninstall();
            }
            catch(org.osgi.framework.BundleException bex)
            {
                logger_.log(Level.WARNING, "Failed to rollback failed deploy for service unit " + su.getName(), bex);
            }
        }
    }
    
    /**
     * Undeploy the service unit with the specified bundle id.
     * @param bundleId id of the SU bundle
     */
    void undeployServiceUnit(ServiceUnitBundle su) throws Exception {
        
        ComponentBundle component = Environment.getComponentManager().
                getComponent(su.getTargetComponent());

        // Start the target component if it's not already active
        if (component.getBundle().getState() != Bundle.ACTIVE) {
            component.getBundle().start();
        }

        try {
            // Invoke deploy on the component's ServiceUnitManager impl
            javax.jbi.component.ServiceUnitManager sum = 
                    component.getComponent().getServiceUnitManager();
            String result = sum.undeploy(su.getName(), 
                    getServiceUnitDirectory(su.getName()).getAbsolutePath());
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(result);
        }
        catch (ResultException rex) {
            throw rex;  // result of inspection
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            unregisterServiceUnit(su, true);
        }
    }
    
    
    /**
     * Invokes the appropriate component SPIs to shut down a service unit 
     * contained in an SU bundle.
     * @param component component targeted by the service unit
     * @param sa service unit bundle containing the service unit
     * @param su service unit descriptor
     */
    void shutDownServiceUnit(ServiceUnitBundle su) throws Exception {

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            // Find the targeted component bundle
            ComponentBundle component = Environment.getComponentManager().
                        getComponent(su.getTargetComponent());
            // Get a reference to the component's service unit manager
            javax.jbi.component.ServiceUnitManager sum = 
                    component.getComponent().getServiceUnitManager();
            // Set the context class loader just in case the component needs it
            Thread.currentThread().setContextClassLoader(component.getClassLoader());
            // Invoke stop/shutDown on the component's ServiceUnitManager impl
            sum.stop(su.getName());
            sum.shutDown(su.getName());
        } 
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }
    
    
    /**
     * Invokes the appropriate component SPIs to start a service unit contained
     * in an SU bundle.
     * @param component component targeted by the service unit
     * @param sa service unit bundle containing the service unit
     * @param su service unit descriptor
     */
    void startServiceUnit(ServiceUnitBundle su) throws Exception {

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            // Find the targeted component bundle
            ComponentBundle component = Environment.getComponentManager().
                        getComponent(su.getTargetComponent());
            
            // Start the target component if it's not already active
            if (component.getBundle().getState() != Bundle.ACTIVE) {
                component.getBundle().start();
            }
            
            // Get a reference to the component's service unit manager
            javax.jbi.component.ServiceUnitManager sum = 
                    component.getComponent().getServiceUnitManager();
            // Set the context class loader just in case the component needs it
            Thread.currentThread().setContextClassLoader(component.getClassLoader());
            // Invoke init/start on the component's ServiceUnitManager impl
            sum.init(su.getName(), 
                    getServiceUnitDirectory(su.getName()).getAbsolutePath());
            sum.start(su.getName());
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        } 
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }

    /**
     * Registers a service unit bundle with the SU Manager.  The SU is 
     * recorded in the SU manager registry and the service unit is
     * deployed to its target component.
     * @param su bundle to register
     * @throws java.lang.Exception failed to create deployment root or the
     * service unit deployment failed.
     */
    void registerServiceUnit(ServiceUnitBundle su) throws Exception {

        File rootDir = null;
        
        // Check for the deployment root
        try {
            if (!getServiceUnitDirectory(su.getName()).exists()) {
                rootDir = createDeploymentRoot(su);
                // If the deployment root didn't exist, that means the SU has
                // not been deployed
                deployServiceUnit(su);
            }
        }
        catch (Exception ex) {
            if (rootDir != null) {
                FileUtil.cleanDirectory(rootDir);
                rootDir.delete();
            }
            
            throw ex;
        }

        // Add the service unit to our registry
        bundles_.put(su.getBundle().getBundleId(), su);
    }

    /**
     * Deploys the service unit to its targeted component.
     * @param su service unit bundle to deploy
     * @throws java.lang.Exception service unit failed to deploy
     */
    void deployServiceUnit(ServiceUnitBundle su) throws Exception {
        
        ComponentBundle component = Environment.getComponentManager().
                getComponent(su.getTargetComponent());
        
        // Check to see if the target component actually exists
        if (component == null) {
            throw new Exception("Deployment of service unit failed. Stand-alone service units must have the target component header."); 
            // throw new Exception("Component " + su.getTargetComponent() + 
            //         " is not installed.");
        }

        // Start the target component if it's not already active
        if (component.getBundle().getState() != Bundle.ACTIVE) {
            component.getBundle().start();
        }

        // Invoke deploy on the component's ServiceUnitManager impl
        javax.jbi.component.ServiceUnitManager sum = 
                component.getComponent().getServiceUnitManager();
        try {
            String result = sum.deploy(su.getName(), 
                    getServiceUnitDirectory(su.getName()).getAbsolutePath());
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(result);
        }
        catch (ResultException rex) {
            throw rex;  // pass on, result of inspection
        }
        catch (DeploymentException dex) {
            // will throw ResultException wrapper if message is task result xml
            ComponentTaskResult.inspectResult(dex.getMessage());
            throw dex;
        }
    }

    /**
     * Create a deployment root for the specified service unit.
     * @param su service unit bundle in need of a deployment root
     * @throws java.lang.Exception failed to extract service unit into
     * deployment root.
     */
    File createDeploymentRoot(ServiceUnitBundle su) throws Exception {
        // Create the service unit directory
        File suDir = getServiceUnitDirectory(su.getName());
        suDir.mkdirs();
        
        // Extract content into directory
        BundleUtil.explodeContents(su.getBundle(), suDir);
        
        return suDir;
    }

    /**
     * Retruns the deployment root for the specified service unit.
     * @param saName service unit name
     * @return deployment root directory
     */
    File getServiceUnitDirectory(String suName) {
        return Environment.getFrameworkBundleContext().getDataFile(
                SU_DIR + File.separator + suName);
    }
    
    /**
     * Removes the service assembly bundle from the SA manager registry and
     * optionally deletes the deployment root.  The deployment root should only
     * be deleted when the service assembly is being undeployed.
     * @param sa service assembly to unregister
     * @param delete true to delete the deployment root, false otherwise
     */
    void unregisterServiceUnit(ServiceUnitBundle su, boolean delete) {

        if (delete) {
            // blow up the deployment root
            File saDir = getServiceUnitDirectory(su.getName());
            FileUtil.cleanDirectory(saDir);
            saDir.delete();
        }

        // Remove the service unit from our registry
        bundles_.remove(su.getBundle().getBundleId());
    }


}
