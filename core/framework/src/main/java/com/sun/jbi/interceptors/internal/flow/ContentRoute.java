 /*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ContentRoute.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.interceptors.internal.flow;

import com.sun.jbi.fuji.ifl.IFLModel;
import com.sun.jbi.fuji.ifl.Flow;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;

/**
 * This class provides support for content based routing in an IFL route definition.
 *
 * @author Sun Microsystems, Inc.
 */
public class ContentRoute extends AbstractFlow implements DynamicEIP {

    /** Default operation used for service filters */
    private static String FILTER_OPERATION_NAME = "requestReply";
    /** Default operation used for service provider */
    private static String PROVIDER_OPERATION_NAME = "oneWay";

    private Logger log_ = Logger.getLogger(ContentRoute.class.getPackage().getName());
    
    // List of services that form the route path
    private Map<String, Object> contentInfo_;
    private Map<String, String> whenInfo_;
    private String              contentName_;
    private MessageFilter       filter_;
    private String              elseRoute;
    private DynamicEIP          dynamic_;
    private boolean             valueBased_;

    public ContentRoute(){};
    /**
     * Creates a new content router.
     * @param filterServices list of services that serve as filters
     */
    public ContentRoute(String name, String namespace, Map<String, Object> contentInfo, IFLModel ifl)
        throws Exception {
        
        super(namespace, name, com.sun.jbi.fuji.ifl.Flow.SELECT);
        
        //
        //  Create the requested filter.
        //
        contentInfo_ = contentInfo;
        String type = (String)contentInfo_.get(Flow.Keys.TYPE.toString());
        config_ = (Properties)contentInfo_.get(Flow.Keys.CONFIG.toString());
        valueBased_ = ((String)contentInfo.get(Flow.Keys.SELECT.toString())).equals("value");
        log_.fine("ContentRoute(" + name + ") Value( " + contentInfo + ")");
        
        //
        //  Determine type of select.
        //
        if (valueBased_) {
            whenInfo_ = new HashMap();
            List<List<String>> whenList = (List)contentInfo.get("WHEN");
            for (List<String> whenClause : whenList) {
                whenInfo_.put(whenClause.get(1), whenClause.get(2));
            }
        } else {
            List<List<String>> whenList = (List)contentInfo.get("WHEN");
            StringBuffer    sb = new StringBuffer();
            sb.append("config-id: defaultRules\n");
            sb.append("app-ns: " + namespace + "\n");
            sb.append("config: \n");
            for (List<String> whenClause : whenList) {
                sb.append(" - type: " + whenClause.get(0) + "\n");
                sb.append("   condition: \"" + whenClause.get(1) + "\"\n");
                sb.append("   to: \"" + whenClause.get(2) + "\"\n");
            }
            if (config_ == null) {
                config_ = new Properties();
                config_.setProperty("defaultRules", sb.toString());
            } else if (!config_.containsKey("defaultRules")) {
                config_.setProperty("defaultRules", sb.toString());
            }
        }
        // The config is a set of properties and these filters expect exp
        if (config_ == null || config_.containsKey("defaultRules")) {
            filter_ = MessageFilterFactory.getInstance().createFilterCollection(name, namespace, type, config_);
        } else {
            filter_ = MessageFilterFactory.getInstance().createFilter(name, namespace, type, config_);
        }
        elseRoute = (String)contentInfo.get("ELSE");
        if (filter_ instanceof DynamicEIP) {
            dynamic_ = ((DynamicEIP)filter_);
        }

        //
        // register a single endpoint in the NMR endpoint registry for 
        // a ContentRouter endpoint
        //
        contentName_ = name;
        activateEndpoint(getServiceQName(contentName_), contentName_ + "_endpoint", false);
    }
    
    /**
     * @return true if the flow is dynamic
     * 
     */
    public boolean isDynamic(){
       return (dynamic_ != null);
    }
    protected boolean acceptEvent(int id) { return (false); }

    protected void onRequest(AbstractFlow.InboundContext ic) {
        MessageExchange     request = ic.getExchange();

        log_.fine("EIP: CBR(" + contentName_ + "," + request.getExchangeId() +") Started");
        if (log_.isLoggable(Level.FINER)) {
            log_.finer("EIP: CBR(" + contentName_ + ") Exchange: \n" + request.toString());
        }

        try {
            String result = filterExchange(request);

            if ( result != null )
            {
                List<String>    routePath = (List<String>)request.getProperty(Route.ROUTE_PATH);

                if (result != null) {
                    log_.fine("EIP: CBR(" + contentName_ + "," + request.getExchangeId() +
                            ") Selected Route(" + result + ")");
                    routePath.add(0, result);
                    if (request instanceof InOut) {
                        request.setMessage(request.getMessage("in"), "out");
                        reply(ic);
                    } else if (request instanceof InOnly) {
                        request.setStatus(ExchangeStatus.DONE);
                        status(ic);
                    }
                } else {
                   request.setError(new EipException(
                           "Select can't find match and no else clause found for (" + contentName_ +
                           ") with value (" + result + ")"));
                   status(ic);
                }
            } else {
                status(ic);
            }
        } catch (Exception e) {
            ByteArrayOutputStream   b = new ByteArrayOutputStream();
            PrintStream             ps = new PrintStream(b);
            e.printStackTrace(ps);
            ps.flush();
            log_.warning("EIP: CBR(" + contentName_ +"," +  request.getExchangeId() + ") Exception: " + b.toString() + toString());
        }

    }

    protected void onStatus(AbstractFlow.InboundContext ic) {
        log_.fine("EIP: CBR(" + contentName_ + "," + ic.getExchange().getExchangeId() + ") Complete");
    }

    /**
     * Filter the active message in the exchange based on the filter configuration
     * 
     * @param exchange - intercepted exchange.
     */
    private String filterExchange(MessageExchange exchange)
        throws Exception
    {   
        try {
            String value = filter_.execute(exchange);

            //
            //  If this is value based request, use the whenInfo to translate to
            //  a route.
            //
            if (valueBased_) {
                if (value == null) {
                    value = "false";
                }
                value = whenInfo_.get(value);
            }

            //
            //  If we have a value try looking it up as a route.
            //
            if (value == null || getEndpoint(value) == null) {
                 log_.fine("EIP: CBR(" + contentName_ + "," + exchange.getExchangeId() + ") Use ELSE");
                 value = elseRoute;
            }
            return (value);
        }
        catch (Exception ex) {
            // If there is an error in processing, set Exchange status as Error 
            // and short-circuit it
            exchange.setError(ex);
            return null;
        }
    }
    
    //
    //--------------------DynamicEIP------------------------------
    //

    /**
    * Add to the existing configuration
    * @param config - configuration data
    */
    public void add(Object config) throws Exception {
        dynamic_.add(config);
    }

    /**
    * Remove the specified configuration
    * @param configId - configuration id
    */
    public void remove(String configId) throws Exception {
        dynamic_.remove(configId);
    }

    /**
    * Update existing configuration
    * @param config - configuration data
    */
    public void modify(Object config) throws Exception {
        dynamic_.modify(config);
    }

    /**
    * Generate a configuration template which can be used to add/modify
    * a configuration. The template is specific to each eip.
    * @return
    */
    public Object generateTemplate() throws Exception {
        return (dynamic_.generateTemplate());
    }

    /**
    *
    * @param config
    * @return
    */
    public Object get(String config) throws Exception {
        return (dynamic_.get(config));
    }

    /**
    * List identifiers for all existing configuration
    */
    public List<String> listConfigs() throws Exception {
        return (dynamic_.listConfigs());
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("        CBR(" + contentName_ + ")\n");
        return (sb.toString());
    }

}
 

