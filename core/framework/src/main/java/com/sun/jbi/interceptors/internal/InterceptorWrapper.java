/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptorWrapper.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.interceptors.Intercept;
import com.sun.jbi.interceptors.Interceptor;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;
import javax.jbi.messaging.MessageExchange;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class InterceptorWrapper 
        implements Interceptor { 
    
             
    protected Object mInterceptor;

    public InterceptorWrapper(Object interceptor )
    {
        mInterceptor = interceptor;
    }
    
    public Object getInterceptor()
    {
        return mInterceptor;
    }

    public Method[] getMessageExchangeInterceptors()
    {
        Method[] annotatedMethods = getInterceptorMethods();
        Class msgExClass = MessageExchange.class;
        
        return filterByParameterType(annotatedMethods, msgExClass);
    }

    public Method[] getMessageInterceptors()
    {
        Method[] annotatedMethods = getInterceptorMethods();
        Class msgClass = ServiceMessage.class;
        
        return filterByParameterType(annotatedMethods, msgClass);
    }
    
    
    /**
     * @return an array of all the annotated methods in the class
     */
    public Method[] getInterceptorMethods()
    {
        List<Method> annotatedMethods = new ArrayList();
        if ( mInterceptor != null )
        {
            Method[] all = mInterceptor.getClass().getDeclaredMethods();
            
            for(Method method : all )
            {
                if ( method.isAnnotationPresent(com.sun.jbi.interceptors.Intercept.class) )
                {
                    annotatedMethods.add(method);
                }
            }
        }
        
        return  annotatedMethods.toArray(new Method[annotatedMethods.size()]);
    }
    
    /**
     * @return the filtered array of methods which accepts the specific 
     *         parameter, this should be the first parameter in the list.
     */
    public Method[] filterByParameterType(Method[] allMethods, Class type)
    {
        List<Method> filteredMethods = new ArrayList();
        
        for(Method method : allMethods )
        {
            Class[] types = method.getParameterTypes();
            if ( types.length > 0 )
            {
                Class firstParamType = types[0];
                
                if ( type.equals(firstParamType) )
                {
                    filteredMethods.add(method);
                }
            }
        }
        
        return  filteredMethods.toArray(new Method[filteredMethods.size()]);
    }
}
