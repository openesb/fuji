/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterfaceConnectionEvent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.events;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.xml.namespace.QName;
import org.osgi.service.event.Event;

public class InterfaceConnectionEvent 
        extends AbstractEvent 
        implements java.io.Serializable
{
   public static final String ADDED_EVENT_TOPIC = "com/sun/jbi/messaging/events/InterfaceConnectionEvent/ADD";
   public static final String REMOVED_EVENT_TOPIC = "com/sun/jbi/messaging/events/InterfaceConnectionEvent/REMOVE";
   public static final String FROM_INTERFACE = "from_interface";
   public static final String TO_ENDPOINT = "to_endpoint";
   public static final String TO_SERVICE  = "to_service";
   private QName        _toServiceName;
   private String       _toEndpointName;
   private QName        _fromInterfaceName;
   private ACTION       _action;
   
    static public boolean isInterfaceConnectionEvent(org.osgi.service.event.Event event)
    {
        String	topic = (String)event.getTopic();

	if (topic.equals(ADDED_EVENT_TOPIC) || topic.equals(REMOVED_EVENT_TOPIC))
        {
	    return (true);
        }
        return (false);
    }
    
    static public InterfaceConnectionEvent valueOf(org.osgi.service.event.Event event)
    {
        InterfaceConnectionEvent   ee = new InterfaceConnectionEvent();
        
        ee._toServiceName = (QName)event.getProperty(TO_SERVICE);
        ee._toEndpointName = (String)event.getProperty(TO_ENDPOINT);
        ee._fromInterfaceName = (QName)event.getProperty(FROM_INTERFACE);
        ee._owner = (String)event.getProperty(OWNER);
        if (event.getTopic().equals(ADDED_EVENT_TOPIC))
        {
            ee._action = ACTION.ADDED;
        }
        else
        {
            ee._action = ACTION.REMOVED;
        }
        return (ee);
    }
    
    static public InterfaceConnectionEvent valueOf(Dictionary props)
    {
        InterfaceConnectionEvent   ee = new InterfaceConnectionEvent();
        
        ee._toServiceName = (QName)props.get(TO_SERVICE);
        ee._toEndpointName = (String)props.get(TO_ENDPOINT);
        ee._fromInterfaceName = (QName)props.get(FROM_INTERFACE);
        ee._owner = (String)props.get(OWNER);
        return (ee);
    }
    
    static public InterfaceConnectionEvent valueOf(ACTION action, String ownerId, QName fromInterface,
            QName toService, String toEndpoint)
    {
        InterfaceConnectionEvent   ee = new InterfaceConnectionEvent();
        
        ee._owner = ownerId;
        ee._toServiceName = toService;
        ee._toEndpointName = toEndpoint;
        ee._fromInterfaceName = fromInterface;
        ee._action = action;
        return (ee);
    }
    
    public AbstractEvent getOpposite()
    {
        InterfaceConnectionEvent  event = 
                InterfaceConnectionEvent.valueOf(_action == ACTION.ADDED ?
                    ACTION.REMOVED : ACTION.ADDED, _owner, _fromInterfaceName,
                    _toServiceName, _toEndpointName);
        return (event);
    }

    public Dictionary getProperties()
    {
        return (null);
    }
 
    public Event getEvent()
    {
        Event       e;
        Dictionary  d = (Dictionary)new Hashtable();
        
        d.put(TO_SERVICE, _toServiceName);
        d.put(TO_ENDPOINT, _toEndpointName);
        d.put(FROM_INTERFACE, _fromInterfaceName);
        d.put(OWNER, _owner);
        e = new Event(_action == ACTION.ADDED 
                ? ADDED_EVENT_TOPIC : REMOVED_EVENT_TOPIC, d);
        return (e);        
    }
    
    public ACTION getAction()
    {
        return (_action);
    }
    
    public QName getToServiceName()
    {
        return (_toServiceName);
    }
    
    public String getToEndpointName()
    {
        return (_toEndpointName);
    }
    
    public QName getFromInterfaceName()
    {
        return (_fromInterfaceName);
    }
}

