/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AspectManager.java - Last published on May 12, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.clientservices.interceptor;

import java.util.HashMap;

import com.sun.jbi.interceptors.internal.aspects.AspectHelper;

import org.osgi.framework.BundleContext;

/**
 *
 * @author nikki
 */
public class AspectManagerImpl implements AspectManager{

    private AspectHelper aspectHelper_;
    
    public AspectManagerImpl(){
        aspectHelper_ = AspectHelper.getInstance();
    }
    
    /**
     * Create a new aspect instance
     * 
     * @param config - the aspect configuration
     */
    public String create(HashMap config) throws Exception{
        
        // Get the name and type from the config
        //  name should be unique
        //  from the type get the class and method name.
        // Look for registered Interceptor classes with same class/method name.
        // Get Bundle Id, create another instance of the class and register 
        // it as an interceptor with new config. 
        // Maintain a cache of [ aspect name | aspect registration ] 
        
        // TODO:
        // If name is null generate one
        // Persist created aspects, to be re created on restart
        
        return aspectHelper_.create(config);
    }
            

    /**
     * Delete an aspect instance
     * 
     * @param name - aspect instance name
     */
    public void delete(String name) throws Exception{
        
        // Lookup the service registration and delete the service registration
        aspectHelper_.delete(name);
    }

}
