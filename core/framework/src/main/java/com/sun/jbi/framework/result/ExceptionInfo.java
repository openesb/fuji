/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExceptionInfo.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import org.w3c.dom.Element;

/**
 * Object model for <code>exception-info</code> element in a JBI 
 * management result message.
 * @author kcbabo
 */
public class ExceptionInfo extends ResultElement {
    
//    public static final String ELEMENT_NAME = 
//            "exception-info";
//    
//    private static final String NESTING_LEVEL =
//            "nesting-level";
//    
//    private static final String MSG_LOC_INFO =
//            "msg-loc-info";
//    
//    private static final String STACK_TRACE =
//            "stack-trace";
    
    public ExceptionInfo(Element element) {
        super(element);
    }
    
    public LocalizedMessage getLocalizedMessage() {
        Element mli = getChildElement(TaskElem.msg_loc_info);
        return mli != null ? new LocalizedMessage(mli) : null;
    }
    
    public String getNestingLevel() {
        return getChildElement(TaskElem.nesting_level).getTextContent();
    }
    
    public String getStackTrace() {
        return getChildElement(TaskElem.stack_trace).getTextContent();
    }
    
    public void setNestingLevel(String nestingLevel) {
        getChildElement(TaskElem.nesting_level).setTextContent(nestingLevel);
    }
    
    public void setStackTrace(String stackTrace) {
        getChildElement(TaskElem.stack_trace).setTextContent(stackTrace);
    }

    @Override
    protected void initialize() {
        createChildElement(TaskElem.nesting_level);
        createChildElement(TaskElem.msg_loc_info);
        createChildElement(TaskElem.stack_trace);
    }
    
}
