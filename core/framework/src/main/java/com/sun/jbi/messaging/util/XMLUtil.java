/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XMLUtil.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/** Utility class used to encapsulate XML-related functionality.
 * @author Sun Microsystems, Inc.
 */
public final class XMLUtil
{
    /** Singleton reference. */
    private static XMLUtil sMe;    
    /** JAXP XSL transformer. */
    private Transformer     mTransform;    
    /** JAXP DOM builder. */
    private DocumentBuilder mBuilder;
    
    /** Private constructor. 
     * @throws javax.jbi.messaging.MessagingException initialization failed
     */
    public XMLUtil()
        throws javax.jbi.messaging.MessagingException
    {
        initBuilder();
        initTransform();
    }
        
    /** Returns an initialized instance of XMLUtil.
     * @return xml utility object
     * @throws javax.jbi.messaging.MessagingException initialization failed
     */
    public static XMLUtil getInstance()
        throws javax.jbi.messaging.MessagingException
    {
        if (sMe == null)
        {
            sMe = new XMLUtil();
        }
        
        return sMe;
    }
    
       
    /** Creates a new DOM object.
     * @return DOM object
     */
    public Document newDocument()
    {
        // builder frowns on concurrent access :-(
        synchronized (mBuilder)
        {
            return mBuilder.newDocument();
        }
    }
           
    /** Parses the XML input into a DOM.
     * @param in input stream containing XML
     * @return DOM object
     * @throws java.io.IOException failed to read document from stream
     */
    public Document readDocument(InputStream in)
        throws java.io.IOException
    {
        try
        {
            // builder frowns on concurrent access :-(
            synchronized (mBuilder)
            {
                return mBuilder.parse(in);
            }
        }
        catch (org.xml.sax.SAXException saxEx)
        {
            throw new java.io.IOException(saxEx.getMessage());
        }
    }
    
     /** Serializes a DOM object to the specified stream.
     * @param doc DOM reference
     * @param out output stream
     * @throws java.io.IOException failed to write document to stream
     */
    public void writeDocument(Document doc, OutputStream out)
        throws java.io.IOException
    {
        DOMSource       source;
        StreamResult    result;
        
        source = new DOMSource(doc);
        result = new StreamResult(out);
        
        try
        {
            // tranformer frowns on concurrent access :-(
            synchronized (mTransform)
            {
                mTransform.transform(source, result);
            }
        }
        catch (javax.xml.transform.TransformerException tEx)
        {
            throw new java.io.IOException(tEx.getMessage());
        }
    }
    
    public String asString(Document doc)
        throws javax.jbi.messaging.MessagingException
    {
        DOMSource       source;
        StringWriter    writer;
        StreamResult    result;
        
        source = new DOMSource(doc);
        writer = new StringWriter();
        result = new StreamResult(writer);
        
        try
        {
            // tranformer frowns on concurrent access :-(
            synchronized (mTransform)
            {
                mTransform.transform(source, result);
            }
        }
        catch (javax.xml.transform.TransformerException tEx)
        {
            throw new javax.jbi.messaging.MessagingException(tEx.getMessage());
        }
        
        return writer.toString();
    }
    
    /** Initialize XSL transformer.
     * @throws javax.jbi.messaging.MessagingException initialization failed
     */
    private void initTransform() 
        throws javax.jbi.messaging.MessagingException
    {
        TransformerFactory tf;
        
        try
        {
            // initialize transformer details
            tf          = TransformerFactory.newInstance();            
            mTransform  = tf.newTransformer();
            mTransform.setOutputProperty (OutputKeys.METHOD, "xml");
            mTransform.setOutputProperty (OutputKeys.INDENT, "yes");
        }
        catch (javax.xml.transform.TransformerFactoryConfigurationError tfcEx)
        {
            throw new javax.jbi.messaging.MessagingException(tfcEx);
        }
        catch (javax.xml.transform.TransformerConfigurationException cfgEx)
        {
            throw new javax.jbi.messaging.MessagingException(cfgEx);
        }
    }
    
    /** Initialize DOM builder.
     * @throws javax.jbi.messaging.MessagingException initialization failed
     */
    private void initBuilder()
        throws javax.jbi.messaging.MessagingException
    {
        DocumentBuilderFactory dbf;
        
        try
        {
            // initialize builder details
            dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            dbf.setValidating(false);
            
            mBuilder = dbf.newDocumentBuilder();
        }
        catch (javax.xml.parsers.ParserConfigurationException pEx)
        {
            throw new javax.jbi.messaging.MessagingException(pEx);
        }
        catch (javax.xml.parsers.FactoryConfigurationError fEx)
        {
            throw new javax.jbi.messaging.MessagingException(fEx);
        }
        
    }
}
