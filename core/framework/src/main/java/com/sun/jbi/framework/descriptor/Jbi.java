/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Jbi.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.descriptor;

import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Simple object representation of jbi.xml.
 * @author kcbabo
 */
public class Jbi extends Descriptor {
    
    private static final String DEFAULT_VERSION =
            "1.0";
    private static final String JBI =
            "jbi";
    private static final String VERSION =
            "version";
    
    private static DocumentBuilder docBuilder;  
    
    
    static {
        try {
            // initialize DOM
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            docBuilder = dbf.newDocumentBuilder();
            
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public Jbi(Element element) throws Exception {
        super(element);
    }
    
    public static synchronized Jbi newJbi(InputStream input) throws Exception {
        try {
            Document doc = docBuilder.parse(input);
            return new Jbi(doc.getDocumentElement());
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (Exception ex) { ; }
        }
    }
    
    public static Jbi newServiceAssemblyDescriptor() {
        Jbi jbi = newJbi();
        jbi.addServiceAssembly();
        return jbi;
    }
    
    public static Jbi newServiceUnitDescriptor() {
        Jbi jbi = newJbi();
        jbi.addServices();
        return jbi;
    }
    
    public Component getComponent() {
        Element comp = getChildElement(Component.ELEMENT_NAME);
        return comp != null ? new Component(comp) : null;
    }
    
    public SharedLibrary getSharedLibrary() {
        Element sl = getChildElement(SharedLibrary.ELEMENT_NAME);
        return sl != null ? new SharedLibrary(sl) : null;
    }
    
    public ServiceAssembly getServiceAssembly() {
        Element sa = getChildElement(ServiceAssembly.ELEMENT_NAME);
        return sa != null ? new ServiceAssembly(sa) : null;
    }
    
    public Services getServices() {
        Element svcs = getChildElement(Services.ELEMENT_NAME);
        return svcs != null ? new Services(svcs) : null;
    }
    
    @Override
    protected void initialize() {
        setVersion(DEFAULT_VERSION);
    }
    
    /**
     * Returns the version of the management message.
     * @return management message version
     */
    public String getVersion() {
        return element_.getAttribute(VERSION);
    }
    
    public void setVersion(String version) {
        element_.setAttribute(VERSION, version);
    }
    
    Services addServices() {
        return new Services(createChildElement(Services.ELEMENT_NAME));
    }
    
    ServiceAssembly addServiceAssembly() {
        return new ServiceAssembly(createChildElement(ServiceAssembly.ELEMENT_NAME));
    }
    
    private static synchronized Jbi newJbi() throws RuntimeException {
        
        try {
            Document doc = docBuilder.newDocument();
            Jbi jbi = new Jbi(doc.createElementNS(NS_URI, JBI));
            doc.appendChild(jbi.getElement());
            return jbi;
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
