/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeUtil.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import com.sun.jbi.interceptors.*;
import org.glassfish.openesb.api.service.ServiceMessageImpl;
import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.messaging.MessageExchangeProxy;

import java.util.Properties;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;


/**
 * Utility class for getting Message Exchange details.
 */
public abstract class MessageExchangeUtil 
{
    /**
     * Based on the Message Status return the active normalized message reference.
     *
     * @return the active normalized message reference, returns null if the 
     *         Message Exchange status is DONE or is in ERROR 
     * @param exchange - message exchange being intercepted
     */
    private static String getActiveMessageReference(MessageExchange exchange)
    {
        String activeMessageRef = null;
        ExchangeStatus currState = exchange.getStatus();
        
        if ( !(ExchangeStatus.DONE == currState || ExchangeStatus.ERROR == currState ) )
        {
            if ( exchange.getMessage(MessageName.OUT.toString()) != null )
            {
                activeMessageRef = MessageName.OUT.toString();
            }
            else if ( exchange.getMessage(MessageName.FAULT.toString()) != null )
            {
                activeMessageRef = MessageName.FAULT.toString();
            }
            else if ( exchange.getMessage(MessageName.IN.toString()) != null )
            {
                activeMessageRef = MessageName.IN.toString();
            }
            
        }
        return activeMessageRef;
    }

    /**
     * @return the current active message in the exchange as a ServiceMessage.
     *         null, if the active message reference is null.
     */
    public static ServiceMessage getActiveMessage(MessageExchange exchange)
            throws javax.jbi.messaging.MessagingException

    {
        NormalizedMessage normalMsg = getActiveNormalizedMessage(exchange);

        if (normalMsg instanceof ServiceMessage) {
            return ((ServiceMessage)normalMsg);
        }
        return new ServiceMessageImpl(normalMsg);
    }

    public static ServiceMessage getInMessage(MessageExchange exchange)
            throws javax.jbi.messaging.MessagingException

    {
        NormalizedMessage   nm = exchange.getMessage(MessageName.IN.toString());
        ServiceMessage      sm = null;

        if (nm != null)
        {
            if (nm  instanceof ServiceMessage) {
                sm = (ServiceMessage)nm;
            } else {
                sm = new ServiceMessageImpl(nm);
            }
        }
        return (sm);
    }

    public static ServiceMessage getOutMessage(MessageExchange exchange)
            throws javax.jbi.messaging.MessagingException
    {
        NormalizedMessage   nm = exchange.getMessage(MessageName.OUT.toString());
        ServiceMessage      sm = null;

        if (nm != null)
        {
            if (nm  instanceof ServiceMessage) {
                sm = (ServiceMessage)nm;
            } else {
                sm = new ServiceMessageImpl(nm);
            }
        }
        return (sm);
    }

    public static ServiceMessage getFaultMessage(MessageExchange exchange)
            throws javax.jbi.messaging.MessagingException
    {
        NormalizedMessage   nm = exchange.getFault();
        ServiceMessage      sm = null;

        if (nm != null)
        {
            if (nm  instanceof ServiceMessage) {
                sm = (ServiceMessage)nm;
            } else {
                sm = new ServiceMessageImpl(nm);
            }
        }
        return (sm);
    }

    public static ServiceMessage getPrototype(MessageExchange exchange, ServiceMessage msg)
           throws javax.jbi.messaging.MessagingException
    {
        ServiceMessage      sm;

        if (msg instanceof ServiceMessageImpl) {
             sm = ((ServiceMessageImpl)msg).getPrototype(exchange);
        } else {
            sm = new ServiceMessageImpl((NormalizedMessage)msg);
        }
        return (sm);
    }

   /**
     * @return the current active message in the exchange as a ServiceMessage.
     *         null, if the active message reference is null.
     */
    private static NormalizedMessage getActiveNormalizedMessage(MessageExchange exchange)
    {
        String activeMessageRef = getActiveMessageReference(exchange);
        NormalizedMessage normalMsg = null;
        
        if ( activeMessageRef != null )
        {
            normalMsg = exchange.getMessage(activeMessageRef);          
        }
        return normalMsg;
    } 
    
    /**
     *
     */
    public static Properties getExchangeProperties(MessageExchange exchange)
        throws Exception
    {
        Properties exProps = new Properties();
        
        String msg =  ( getActiveMessageReference(exchange) == null ? "null" : getActiveMessageReference(exchange));
        exProps.setProperty(FilterProperty.MESSAGE.toString(), msg);
        
        // -- If this is the Consumer Exchange, then the endpoint returned is the
        //    endpoint link, we want the actual endpoint, which we can get
        //    from the Proxy by calling getActualEndpoint()
        if ( exchange instanceof MessageExchangeProxy )
        {
            MessageExchangeProxy proxy = (MessageExchangeProxy) exchange;
            ServiceEndpoint ep = proxy.getActualEndpoint(); 

            if ( ep != null )
            {
                exProps.setProperty(FilterProperty.SERVICE.toString(), 
                        ep.getServiceName().toString());
                exProps.setProperty(FilterProperty.ENDPOINT.toString(), 
                        ep.getEndpointName());
            }
            
            exProps.setProperty(FilterProperty.CONSUMER.toString(), 
                    proxy.getSourceComponent());
            exProps.setProperty(FilterProperty.PROVIDER.toString(), 
                    proxy.getTargetComponent());
            
            ServiceEndpoint epLink = proxy.getEndpointLink();
            
            if ( epLink != null )
            {
                exProps.setProperty(FilterProperty.ENDPOINT_LINK.toString(),
                        epLink.getEndpointName());
                exProps.setProperty(FilterProperty.SERVICE_CONNECTION.toString(),
                        epLink.getServiceName().toString());
            }
            exProps.setProperty(FilterProperty.STATUS.toString(), 
                    proxy.getStatus().toString());            
        }
        
        return exProps;
    }
    
}
