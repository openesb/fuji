/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkTaskResult.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import org.w3c.dom.Element;

/**
 * Object model for <code>frmwk-task-result</code> element in a JBI 
 * management result message.
 * @author kcbabo
 */
public class FrameworkTaskResult extends ResultElement {
    
//    public static final String ELEMENT_NAME =
//            "frmwk-task-result";
//    
//    private static final String IS_CAUSE_FRAMEWORK = 
//            "is-cause-framework";
    
    public enum IsCauseFramework {YES, NO};
    
    public FrameworkTaskResult(Element element) {
        super(element);
    }
    
    public FrameworkTaskResultDetails getFrameworkTaskResultDetails() {
        Element ftrd = getChildElement(TaskElem.frmwk_task_result_details);
        return ftrd != null ? new FrameworkTaskResultDetails(ftrd) : null;
    }
    
    public IsCauseFramework getIsCauseFramework() {
        Element icf = getChildElement(TaskElem.is_cause_framework);
        
        if (icf != null && icf.getTextContent() != null) {
            return IsCauseFramework.valueOf(icf.getTextContent());
        }
        else {
            return null;
        }
    }
    
    public void setIsCauseFramework(IsCauseFramework cause) {
        if (getChildElement(TaskElem.is_cause_framework) == null) {
            createChildElement(TaskElem.is_cause_framework);
        }
        getChildElement(TaskElem.is_cause_framework).setTextContent(cause.toString());
    }

    @Override
    protected void initialize() {
        createChildElement(TaskElem.frmwk_task_result_details);
    }
    
}
