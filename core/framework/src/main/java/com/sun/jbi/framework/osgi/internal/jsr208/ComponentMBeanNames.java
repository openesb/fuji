/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentMBeanNames.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.ObjectName;

/**
 *
 * @author kcbabo
 */
public class ComponentMBeanNames implements javax.jbi.management.MBeanNames {
    
    private static final String DOMAIN_ROOT = "com.sun.jbi";
    private static final String TYPE_KEY = "type";
    private static final String TYPE_VAL = "component";
    private static final String COMPONENT_NAME_KEY = "componentName";
    private static final String CONTROL_TYPE_KEY = "controlType";
    private static final String CUSTOM_NAME_KEY = "customName";

    public enum ControlType {LifeCycle,Installer};
    
    private String componentName_;
    private Logger logger_;
    
    public ComponentMBeanNames(String componentName) {
        componentName_ = componentName;
        logger_ = Logger.getLogger(this.getClass().getPackage().getName());
    }

    public ObjectName createComponentMBeanName(ControlType controlType) {
        ObjectName mbeanName = null;
        Hashtable props = createPropertySet();
        props.put(CONTROL_TYPE_KEY, controlType.toString());

        try {
            mbeanName = new ObjectName(DOMAIN_ROOT, props);
        } catch (javax.management.MalformedObjectNameException ex) {
            logger_.log(Level.WARNING, "Failed to create MBean name", ex);
        }

        return mbeanName;
    }

    public ObjectName createCustomComponentMBeanName(String customName) {
        ObjectName mbeanName = null;
        Hashtable props = createPropertySet();
        props.put(CUSTOM_NAME_KEY, customName);
        
        try {
            mbeanName = new ObjectName(DOMAIN_ROOT, props);
        } catch (javax.management.MalformedObjectNameException ex) {
            logger_.log(Level.WARNING, "Failed to create custom MBean name", ex);
        }
        
        return mbeanName;
    }

    public String getJmxDomainName() {
        return DOMAIN_ROOT;
    }
    
    Hashtable createPropertySet() {
        Hashtable props = new Hashtable<String, String>();
        props.put(TYPE_KEY, TYPE_VAL);
        props.put(COMPONENT_NAME_KEY, componentName_);
        
        return props;
    }
}
