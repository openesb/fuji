/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageImpl.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.activation.DataHandler;
import javax.security.auth.Subject;
import javax.xml.transform.Source;
import javax.xml.namespace.QName;


/** Implementation of JBI <code>NormalizedMessage</code> interface.
 * @author Sun Microsystems, Inc.
 */
public class MessageImpl 
        implements javax.jbi.messaging.NormalizedMessage,
                   java.lang.Cloneable
{
    /** Attachment map */
    private HashMap     mAttachments;
    /** Property map */
    private HashMap     mProperties;
    /** Message content */
    private Source      mContent;
    
    /** Creates a new MessageImpl without content.
     * @throws javax.jbi.messaging.MessagingException failed to init message.
     */
    public MessageImpl()
    {
        this(null, new HashMap());
    }
    
    /** Create a new MessageImpl with the specified content and properties.
     *  @param content message content
     *  @param properties message properties
     *  @throws javax.jbi.messaging.MessagingException initialization failed
     */
    MessageImpl(Source content, HashMap properties)
    {
        mContent    = content;
        mProperties = properties;
        
        mAttachments = new HashMap();
    }

    public Object clone()
    {
        MessageImpl     msg = new MessageImpl(null, null);

        msg.mProperties = (HashMap)mProperties.clone();
        msg.mAttachments = (HashMap)mAttachments.clone();
        return (msg);
    }
    
    /** Returns a Source representation of message content.
     *  @return message content as Source object
     */
    public Source getContent()
    {
        return mContent;
    }
    
    /** Specifies the content of the message.
     *  @param content message content as Source object
     */
    public void setContent(Source content)
    {
        mContent = content;
    }
     
    public Object getProperty(String name)
    {
        return mProperties.get(name);
    }
    
    HashMap getProperties()
    {
        return (mProperties);
    }
    
    void setProperties(HashMap map)
    {
        mProperties = map;
    }
    
    public Set getPropertyNames()
    {
        return (mProperties.keySet());
    }
    
    public void setProperty(String name, Object value) 
    {
        mProperties.put(name, value);
    }
 
    public void setSecuritySubject(Subject subject)
    {
        setProperty("javax.jbi.security.subject", subject);
    }
    
    public Subject getSecuritySubject()
    {
        return ((Subject)getProperty("javax.jbi.security.subject"));
    }
    
    public void addAttachment(String id, DataHandler content)
        throws javax.jbi.messaging.MessagingException
    {
        mAttachments.put(id, content);
    }
    
    public javax.activation.DataHandler getAttachment(String id)
    {
        return (DataHandler)mAttachments.get(id);
    }
    
    public Set getAttachmentNames()
    {
        return (mAttachments.keySet());
    }
    
    public void removeAttachment(String id) 
        throws javax.jbi.messaging.MessagingException
    {
        mAttachments.remove(id);
    }
    
    void setAttachmentMap(HashMap attachments)
    {
        mAttachments = attachments;
    }    
     
    HashMap getAttachmentMap()
    {
        return mAttachments;
    }
    
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("            Message Content(Type): ");
        sb.append(mContent == null ? "null" : mContent.getClass().getName());
        sb.append("\n            Properties Count:   ");
        sb.append(mProperties.size());
        sb.append("\n");
        for (Iterator p = mProperties.entrySet().iterator(); p.hasNext(); )
        {
            Map.Entry   me = (Map.Entry)p.next();
            sb.append("              Name: ");
            sb.append((String)me.getKey());
            if (me.getValue() instanceof String)
            {
                sb.append("\n                Value(String): ");
                sb.append((String)me.getValue());
            }
            else if (me.getValue() instanceof QName)
            {
                sb.append("\n            Value(QName): ");
                sb.append((QName)me.getValue());
            }
            else if (me.getValue() instanceof Long)
            {
                sb.append("\n                Value(Long): ");
                sb.append((Long)me.getValue());
            }
            else if (me.getValue() != null)
            {  
                sb.append("\n                Value(Type): ");
                sb.append(me.getValue().getClass().getName());
            }
            sb.append("\n");
        }
        sb.append("            Attachments Count:  ");
        sb.append(mAttachments.size());
        sb.append("\n");
        for (Iterator p = mAttachments.entrySet().iterator(); p.hasNext(); )
        {
            Map.Entry   me = (Map.Entry)p.next();
            sb.append("              Name: ");
            sb.append((String)me.getKey());
            if (me.getValue() != null)
            {
                sb.append("\n                DataSource(Type) : ");
                sb.append(((DataHandler)me.getValue()).getDataSource().getClass().getName());
                sb.append("\n");
            }
            else
            {
                sb.append("\n                DataHandler: null\n");
            }
        }
        return (sb.toString());
    }
}
