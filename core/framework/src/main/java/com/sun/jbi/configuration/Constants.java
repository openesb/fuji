/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java - Last published on Nov 14, 2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

/** The Configuration domain suffix is based on this package name, i.e.
 *  the service pid's of all fuji Configurations will start with the
 *  suffix "{package_name}:", for example, "com.sun.jbi.configuration:"
 */
package com.sun.jbi.configuration;

/**
 * Constants used in fuji configurations
 * 
 * @author nikki
 */
public class Constants {
    
    /** Interceptor type specified in the fuji configuration pid **/
    public static String INTERCEPTOR_TYPE = "interceptor";
    
    /** fuji configuration domain pid **/
    public static String CONFIG_DOMAIN = "fuji-configuration";
    
    /** Interceptor directory name **/
    public static String INTERCEPTOR_DIR_NAME = "interceptor";

    /** Properties file extension **/
    public static String CONFIG_FILE_TYPE = ".properties";

    public static String META_INF = "META-INF";

    /** Interceptor Config Filename **/
    public static String INTERCEPTOR_CONFIG_FILENAME = INTERCEPTOR_DIR_NAME + CONFIG_FILE_TYPE;
}
