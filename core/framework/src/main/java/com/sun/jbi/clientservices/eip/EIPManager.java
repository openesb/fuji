/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPManager.java - Last published on May 6, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.clientservices.eip;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.HashMap;

/**
 *
 * @author Sun Microsystems
 */
public interface EIPManager {

    /** Augment an existing eip with new configuration
     * 
     * @param name
     * @param config
     * @throws java.lang.Exception
     * @deprecated 
     */
    public void add(QName name, Object config)
            throws Exception;
    
    /** Augment an existing eip with new configuration
     * 
     * @param config
     * @throws java.lang.Exception
     */
    public void add(HashMap config)
            throws Exception;

    /**
     * Remove identified configuration from the eip. 
     * 
     * @param name
     * @param configIdentifiers
     * @throws java.lang.Exception
     */
    public void remove(QName name, String configId)
            throws Exception;

    /**
     * Update existing eip configuration.
     * 
     * @param name
     * @param config
     * @throws java.lang.Exception
     * @deprecated
     */
    public void modify(QName name, Object config) throws Exception;

    /**
     * Update existing eip configuration.
     * 
     * @param config
     * @throws java.lang.Exception
     */
    public void modify(HashMap config) throws Exception;
    
    /**
     * List all the eip instance of a specific type.
     * 
     * @param name
     * @param type - eip type {select}
     * @return
     * @throws java.lang.Exception
     */
    public List<QName> list(String type) throws Exception;
    
    /**
     * List all the configuration identifiers for the eip instance
     * 
     * @param name - eip instance name
     * @return a list of configuration ids
     * @throws java.lang.Exception
     */
    public List<String> listConfigIds(QName name) throws Exception;
    
    /**
     * List all the configuration identifiers for the eip instance
     * 
     * @param name - eip instance name
     * @return YAML data for the eip instance and the configuration ids.
     * @throws java.lang.Exception
     */
    public HashMap listConfigIdsInfo(QName name) throws Exception;

    /**
     * Get the specified configuration for a  specific eip instance. 
     * A YAML object containing all pertinent configuration
     * information is returned.
     * 
     * @param name
     * @param configId - identifies a configuration
     * @return
     * @throws java.lang.Exception
     */
    public Object get(QName name, String configId) throws Exception;
    
     /**
      * Generate a configuration template which can be used to add/modify
      * a configuration. The template is specific to each eip.
      * @param type - eip type {select} 
      * @return
      */
     public Object generateTemplate(String type) throws Exception;
}
