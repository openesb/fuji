/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OSGiFramework.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.clientservices.eip.EIPManager;
import com.sun.jbi.clientservices.eip.EIPManagerImpl;
import com.sun.jbi.clientservices.interceptor.AspectManager;
import com.sun.jbi.clientservices.interceptor.AspectManagerImpl;
import com.sun.jbi.framework.osgi.internal.jsr208.AdminService;
import com.sun.jbi.framework.osgi.internal.jsr208.DeploymentService;
import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.interceptors.internal.aspects.AspectHelper;
import com.sun.jbi.interceptors.internal.InterceptionHelper;
import com.sun.jbi.interceptors.internal.InterceptorServiceListener;
import com.sun.jbi.interceptors.internal.ConfigurationHelper;
import com.sun.jbi.messaging.MessageService;
import com.sun.jbi.logging.RuntimeLoggerConfigurationService;
import com.sun.jbi.logging.RuntimeLoggerDisplayService;
import com.sun.jbi.logging.RuntimeLoggingService;


import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.SynchronousBundleListener;
import org.osgi.service.cm.ManagedService;

/**
 * Control center for the JBI/OSGI framework implementation.  This class provides
 * the bundle activator to bootstrap the JBI runtime (NMR, MBeans, services, etc.).
 * The framework bundle also acts as a bundle event listener to keep track of
 * JBI bundle activity in the OSGI framework.
 * @author kcbabo
 * @author mwhite
 */
public class OSGiFramework 
        implements BundleActivator, SynchronousBundleListener {
    
    private Logger logger_;
    
    /** Bundle context for our framework bundle. */
    private BundleContext bundleContext_;
    /** JSR 208 Normalized Message Service. */
    private MessageService messageService_;
    /** Manages JBI service assemblies. */
    private ServiceAssemblyManager saManager_;
    /** Manages stand-alone JBI service units. */
    private ServiceUnitManager suManager_;
    /** Manages JBI components. */
    private ComponentManager compManager_;
    /** Creates instances of JSR 208 ComponentContext */
    private ComponentContextFactory contextFactoryService_;
    /** Set of JSR 208 Admin MBeans. */
    private Set<ObjectName> adminMBeans_ = new HashSet<ObjectName>();
    /** Keeps track of interceptor bundles that are registered in the OSGi
     *  service registry. */
    private ConcurrentHashMap<String, InterceptorBundle> interceptorBundles_ = 
            new ConcurrentHashMap<String, InterceptorBundle>();
    /** JMX Connector Server which allows remote JMX clients to connect to us. */
    private JMXConnector jmxConnector_;
    /** Keeps track of service providers outside the JBI environment. */
    private ServiceProviderTracker serviceTracker_;
    /** Interceptor Service Listener **/
    private InterceptorServiceListener intSvcLsnr_;
    
     /** Runtime Logger Configuration Service Reference**/
    ServiceRegistration runtimeLoggerServiceRef_;

    /** Runtime Logger Display Service Reference**/
    ServiceRegistration runtimeLoggerDisplayServiceRef_;
    
    /** Client Service Registrations */
    List<ServiceRegistration> clientServices_;

    
    /** 
     * Create a new instance of the JBI OSGi Framework.
     */
    public OSGiFramework() {
        logger_ = Logger.getLogger(this.getClass().getPackage().getName());
        compManager_ = new ComponentManager();
        saManager_ = ServiceAssemblyManager.getInstance();
        suManager_ = ServiceUnitManager.getInstance();
        
        Environment.setComponentManager(compManager_);
        Environment.setServiceAssemblyManager(saManager_);
        
      
    }
    
    /** 
     * Observe bundle events to see if JBI bundles are changing state.
     * @param event the bundle event
     */
    public void bundleChanged(BundleEvent event) {
        switch (event.getType()) {
            case BundleEvent.INSTALLED :
                installBundle(event.getBundle());
                break;
            case BundleEvent.UNINSTALLED :
                uninstallBundle(event.getBundle());
                break;
            case BundleEvent.STARTED :
                startBundle(event.getBundle());
                break;
            case BundleEvent.STOPPED :
                stopBundle(event.getBundle());
                break;
            case BundleEvent.UPDATED :
                updateBundle(event.getBundle());
                break;
        }            
    }
    
    /** 
     * Start the JBI framework bundle.
     * @param context our bundle context
     * @throws java.lang.Exception 
     */
    public void start(BundleContext context) throws Exception {
        bundleContext_ = context;
        Environment.setFrameworkBundleContext(bundleContext_);
        
        //initialize the runtime logging service to create the root logger and other key loggers. 
        RuntimeLoggingService.initializeService();  
        registerLogServices();
  
        
        bundleContext_.addBundleListener(this);
         
        messageService_ = new MessageService();
        messageService_.setBundleContext(bundleContext_);
        messageService_.startService();
        Environment.setMessageService(messageService_);
        
        registerAdminMBeans();
        
        // register OSGi services
        contextFactoryService_ = new ComponentContextFactory();
        bundleContext_.registerService("javax.jbi.component.ComponentContext", 
                contextFactoryService_, null);

        MessageServiceFactory messageServiceFactory_ = new MessageServiceFactory();
        bundleContext_.registerService("com.sun.jbi.messaging.MessageService",
                messageServiceFactory_ ,null);
        
        // Register a listener for Interceptor Service's 
        intSvcLsnr_ = new InterceptorServiceListener();
        context.addServiceListener(intSvcLsnr_, getInterceptorClassFliter() );
        
        // Create an instance of our service tracker
        serviceTracker_ = new ServiceProviderTracker(contextFactoryService_);
     //   serviceTracker_.addingService((ServiceReference) messageServiceFactory_);

        serviceTracker_.start();

        
        
        registerJBIBundles();

        jmxConnector_ = new JMXConnector(context);
        jmxConnector_.start();
        
        registerClientServices();
        
        AspectHelper.getInstance().registerDynamicAspects(true);
        
    }
    
    /**
     * This method is used to register services used to set and list logger levels
     * This method registers a service that is used to set logger levels and another
     * service to display effective logger levels
     */
     private void registerLogServices() {
    
        //register the logger configuration service
        //this will be used by admin tools to configure log levels
        RuntimeLoggerConfigurationService runtimeLogConfigService = 
                new RuntimeLoggerConfigurationService(bundleContext_);
        runtimeLoggerServiceRef_ = bundleContext_.registerService(
                ManagedService.class.getName(),
                runtimeLogConfigService,
                runtimeLogConfigService.getConfiguration()); 
        
        //register the logger configuration service
        //this will be used by admin tools to display log levels
        RuntimeLoggerDisplayService runtimeLogDisplayService = 
                new RuntimeLoggerDisplayService(bundleContext_);
        runtimeLoggerDisplayServiceRef_ = bundleContext_.registerService(
                ManagedService.class.getName(),
                runtimeLogDisplayService,
                runtimeLogDisplayService.getConfiguration());
    }

    /** 
     * Stop the framework bundle.  Tear down MBeans and unregister services.
     * @param context
     * @throws java.lang.Exception
     */
    public void stop(BundleContext context) throws Exception {
        
        jmxConnector_.stop();
        serviceTracker_.stop();

        unregisterJBIBundles();
        
        bundleContext_.removeBundleListener(this);
        
        // Remove the interceptor Service listener
        bundleContext_.removeServiceListener(intSvcLsnr_);
        
        unregisterAdminMBeans();
        messageService_.stopService();
        
        runtimeLoggerServiceRef_.unregister();
        runtimeLoggerDisplayServiceRef_.unregister();
        
        unregisterClientServices();
    }
    
    void registerJBIBundles() {
        ArrayList<ComponentBundle> components = 
                    new ArrayList<ComponentBundle>();
        ArrayList<ServiceAssemblyBundle> assemblies = 
                new ArrayList<ServiceAssemblyBundle>();
        ArrayList<ServiceUnitBundle> serviceUnits = 
                new ArrayList<ServiceUnitBundle>();

        // Build a list of component and SA bundles we need to tweak
        for (Bundle bundle : bundleContext_.getBundles()) {
            Jbi jbiXml;
            if ((jbiXml = BundleUtil.getJbiXml(bundle)) != null) {
                if (jbiXml.getComponent() != null) {
                    components.add(new ComponentBundle(jbiXml, bundle));
                }
                else if (jbiXml.getServiceAssembly() != null) {
                    assemblies.add(new ServiceAssemblyBundle(jbiXml, bundle));
                }
                else if (jbiXml.getServices() != null) {
                    serviceUnits.add(new ServiceUnitBundle(jbiXml, bundle));
                }
            }
        }

        //
        //  Find components to register and prepare for recovery.
        //
        for (ComponentBundle comp : components) {
            try {
                compManager_.registerComponent(comp);
                if (comp.getBundle().getState() == Bundle.ACTIVE) {
                    compManager_.recoverComponent(comp.getBundle().getBundleId());
                }
            } catch (Exception ex) {
                logger_.log(Level.WARNING,
                        "Startup processing failed for component bundle " +
                        comp.getName(), ex);
            }
        }

        //
        // Start components
        //
        for (ComponentBundle comp : components) {
            try {
                if (comp.getBundle().getState() == Bundle.ACTIVE) {
                    compManager_.startComponent(comp.getBundle().getBundleId());
                }
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Startup processing failed for component bundle " + 
                        comp.getName(), ex);
            }
        }

        // Register and start assemblies
        for (ServiceAssemblyBundle sa : assemblies) {
            try {
                saManager_.registerServiceAssembly(sa);
                if (sa.getBundle().getState() == Bundle.ACTIVE) {
                    saManager_.startServiceAssembly(sa.getBundle().getBundleId());
                }
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Startup processing failed for service assembly bundle " + 
                        sa.getName(), ex);
            }
        }
        
        // Register and start stand-alone service units
        for (ServiceUnitBundle su : serviceUnits) {
            try {
                suManager_.registerServiceUnit(su);
                if (su.getBundle().getState() == Bundle.ACTIVE) {
                    suManager_.startServiceUnit(su.getBundle().getBundleId());
                }
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Startup processing failed for service unit bundle " + 
                        su.getName(), ex);
            }
        }
    }
    
    void unregisterJBIBundles() {
        // Stop and unregister assemblies
        for (ServiceAssemblyBundle sa : saManager_.getServiceAssemblies()) {
            try {
                if (sa.getBundle().getState() == Bundle.ACTIVE ){
                    stopBundle(sa.getBundle());
                }
                saManager_.unregisterServiceAssembly(sa, false);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Shutdown processing failed for service assembly bundle " + 
                        sa.getName(), ex);
            }
        }
        
        // Stop and unregister stand-alone service units
        for (ServiceUnitBundle su : suManager_.getServiceUnits()) {
            try {
                if ( su.getBundle().getState() == Bundle.ACTIVE ){
                    stopBundle(su.getBundle());
                }
                suManager_.unregisterServiceUnit(su, false);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Shutdown processing failed for service unit bundle " + 
                        su.getName(), ex);
            }
        }

        // Stop and unregister components
        for (ComponentBundle comp : compManager_.getComponents()) {
            try {
                if ( comp.getBundle().getState() == Bundle.ACTIVE ){
                    stopBundle(comp.getBundle());
                }
                compManager_.unregisterComponent(comp, false);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Shutdown processing failed for component bundle " + 
                        comp.getName(), ex);
            }
        }

    }
    
    /** 
     * Install a JBI OSGI bundle.
     * @param bundle a JBI component, service assembly, or shared library bundle.
     */
    void installBundle(Bundle bundle) {
        Jbi jbiXml = BundleUtil.getJbiXml(bundle);
        
        // Register aspects in any bundle
        AspectHelper.getInstance().createAspectsInBundle(bundle);
        
        // If there's no jbi.xml, we don't care about it
        if (jbiXml == null) {
            return;
        }
        
        if (jbiXml.getComponent() != null) {
            // It's a component
            compManager_.installComponent(bundle, jbiXml);
        }
        else if (jbiXml.getServiceAssembly() != null) {
            // It's a service assembly
            saManager_.deployServiceAssembly(bundle, jbiXml);
        }
        else if (jbiXml.getServices() != null) {
            // It's a service unit
            suManager_.deployServiceUnit(bundle, jbiXml);
        }
    }
    
    /** 
     * Uninstall a JBI OSGi bundle.
     * @param bundle a JBI component, service assembly, or shared library bundle.
     */
    void uninstallBundle(Bundle bundle) {
        
                        
        // Unregister aspects which are defined in the service assembly
        AspectHelper.getInstance().deleteAspectsInBundle(bundle.getBundleId());
        
        if (compManager_.isRegistered(bundle.getBundleId())) {
            compManager_.uninstallComponent(bundle.getBundleId());
        }
        else if (saManager_.isRegistered(bundle.getBundleId())) {
            saManager_.undeployServiceAssembly(bundle.getBundleId());
        }
        else if (suManager_.isRegistered(bundle.getBundleId())) {
            suManager_.undeployServiceUnit(bundle.getBundleId());
        }
        else {
            logger_.finer("Ignoring non-JBI bundle: " + bundle.getBundleId());
        }
                
        // If there is any configuration for this bundle, delete it
        ConfigurationHelper.deleteConfiguration(bundle.getBundleId());
    }
    
    /**
     * Update a JBI OSGi bundle.  This method is called by the OSGi framework
     * when an OSGi bundle has been successfully updated.  We need to make sure
     * the update is reflected in the underlying JBI runtime.
     * @param bundle a JBI component, service assembly, or shared library bundle
     */
    void updateBundle(Bundle bundle) {
        
        // We only support update of service assemblies at the moment
        if (saManager_.isRegistered(bundle.getBundleId())) {
            saManager_.updateServiceAssembly(bundle);
        }
    }
    
    /** 
     * Start a JBI OSGi bundle.
     * @param bundle a JBI component or service assembly.
     */ 
    void startBundle(Bundle bundle) {
        if (compManager_.isRegistered(bundle.getBundleId())) {
            compManager_.startComponent(bundle.getBundleId());
        }
        else if (saManager_.isRegistered(bundle.getBundleId())) {
            saManager_.startServiceAssembly(bundle.getBundleId());
        }
        else if (suManager_.isRegistered(bundle.getBundleId())) {
            suManager_.startServiceUnit(bundle.getBundleId());
        }
        else if (InterceptionHelper.isInterceptorBundle(bundle)) {
            startInterceptor(bundle);
        }
        else {
            logger_.finer("Ignoring non-JBI bundle: " + bundle.getBundleId());
        }
        
        // If there are any dynamic aspects enabled by this bundle register those
        try{
            AspectHelper.getInstance().registerDynamicAspects(false);
        } catch(Exception ex){
            logger_.log(Level.WARNING, "Failed to register all dynamic aspect instances", ex);
        }
    }
    
    /** 
     * Stop a JBI OSGi bundle.
     * @param bundlea JBI component or service assembly
     */
    void stopBundle(Bundle bundle) {
        
        if (compManager_.isRegistered(bundle.getBundleId())) {
            compManager_.shutDownComponent(bundle.getBundleId());
        }
        else if (saManager_.isRegistered(bundle.getBundleId())) {
            saManager_.shutDownServiceAssembly(bundle.getBundleId());
        }
        else if (suManager_.isRegistered(bundle.getBundleId())) {
            suManager_.shutDownServiceUnit(bundle.getBundleId());
        }
        else if (InterceptionHelper.isInterceptorBundle(bundle)) {
            stopInterceptor(bundle);
        }
        else {
            logger_.finer("Ignoring non-JBI bundle: " + bundle.getBundleId());
        }
    }
    
    /** 
     * Register 208 management MBeans. 
     */
    void registerAdminMBeans() {
        // register JBI MBeans
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        try {
            StandardMBean asMBean = new StandardMBean(
                    new AdminService(), javax.jbi.management.AdminServiceMBean.class);
            ObjectName asMBeanName = SystemMBeanNames.createSystemMBeanName(
                    "AdminService", "AdminService");
            mbs.registerMBean(asMBean, asMBeanName);
            adminMBeans_.add(asMBeanName);
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Failed to register admin service MBean", ex);
        }
        try {
            StandardMBean dsMBean = new StandardMBean(
                    new DeploymentService(), com.sun.jbi.management.DeploymentServiceMBean.class);
            ObjectName dsMBeanName = SystemMBeanNames.createSystemMBeanName(
                    "DeploymentService", "DeploymentService");
            mbs.registerMBean(dsMBean, dsMBeanName);
            adminMBeans_.add(dsMBeanName);
        } catch (Exception ex) {
            logger_.log(Level.WARNING, "Failed to register deployment service MBean", ex);
        }
    }
    
    /** 
     * Unregister 208 management MBeans.
     */
    void unregisterAdminMBeans() {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        
        for (ObjectName mbean : adminMBeans_) {
            try {
                mbs.unregisterMBean(mbean);
            } catch (Exception ex) {
                logger_.log(Level.WARNING, 
                        "Failed to unregister admin MBean: " + mbean, ex);
            }
        }
    }
    
    /**
     * Register all the client services
     */
    void registerClientServices(){
        
        EIPManager eipManager = new EIPManagerImpl();
        
        ServiceRegistration eipSvc = bundleContext_.registerService(
                EIPManager.class.getName(),
                eipManager,
                null);
        
        getClientServicesList().add(eipSvc);
        
        AspectManager aspectManager = new AspectManagerImpl();
        
        ServiceRegistration  aspSvc = bundleContext_.registerService(
                AspectManager.class.getName(),
                aspectManager,
                null);
        
        getClientServicesList().add(aspSvc);
    }
    
    /**
     * Unregister all client services
     */
    void unregisterClientServices(){
        
        for ( ServiceRegistration svcReg: getClientServicesList()){
            svcReg.unregister();;
        }
    }
    
    /** 
     * Register all interceptors in the interceptor bundle.
     * @param bundle
     * @throws java.lang.Exception
     */
    void startInterceptor(Bundle bundle) {
        InterceptorBundle interceptorBundle = new InterceptorBundle(bundle);
        
        try {
            for (ServiceRegistration svc : 
                InterceptionHelper.registerInterceptorsInBundle(bundle)) {
                    interceptorBundle.addService(svc);
            }
        } catch (Exception ex) {
            logger_.log(Level.WARNING, 
                    "Failed to register interceptors in bundle " + 
                    bundle.getSymbolicName(), ex);
        }
        interceptorBundles_.put(interceptorBundle.getName(), interceptorBundle);
    }
    
    /**
     * Unregister all interceptors in the interceptor bundle.
     * @param bundle
     */
    void stopInterceptor(Bundle bundle) {
        InterceptorBundle interceptorBundle = 
                interceptorBundles_.get(bundle.getSymbolicName());
        
        if (interceptorBundle != null) {
            interceptorBundle.unregisterServices();
        }
    }
    
    /**
     * Get the Interceptor interface name as a LDAP Query filter string :
     *   (objectClass=com.sun.jbi.interceptors.Interceptor)
     * 
     * @return the filter string for the interceptor service listener
     */ 
    private String getInterceptorClassFliter()
     {
        StringBuffer intSvcLsnrFilter = new StringBuffer();
        intSvcLsnrFilter.append("(");
        intSvcLsnrFilter.append(org.osgi.framework.Constants.OBJECTCLASS);
        intSvcLsnrFilter.append("=");
        intSvcLsnrFilter.append("com.sun.jbi.interceptors.Interceptor");
        intSvcLsnrFilter.append(")");
        return intSvcLsnrFilter.toString();
     }
    
    /**
     * 
     * @return the client servcies list
     */
    private List<ServiceRegistration> getClientServicesList(){
        if (clientServices_ == null){
            clientServices_ = new java.util.ArrayList();
        }
        return clientServices_;
    }
            
}
