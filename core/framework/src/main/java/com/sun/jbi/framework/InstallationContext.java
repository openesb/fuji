/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationContext.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework;

import com.sun.jbi.framework.descriptor.Component;
import com.sun.jbi.framework.osgi.internal.ComponentBundle;
import java.util.Arrays;
import java.util.List;
import javax.jbi.component.ComponentContext;
import org.w3c.dom.DocumentFragment;

/**
 *
 * @author kcbabo
 */
public class InstallationContext 
        implements javax.jbi.component.InstallationContext {
    
    private ComponentBundle component_;
    private Component descriptor_;
    private boolean isInstall_;
    
    public InstallationContext(ComponentBundle component, boolean isInstall) {
        component_ = component;
        descriptor_ = component.getDescriptor().getComponent();
        isInstall_ = isInstall;
    }

    public List getClassPathElements() {
        return Arrays.asList(
                descriptor_.getBootstrapClassPath().getPathElements());
    }

    public String getComponentClassName() {
        return descriptor_.getComponentClassName();
    }

    public String getComponentName() {
        return descriptor_.getIdentification().getName();
    }

    public ComponentContext getContext() {
        return component_.getComponentContext();
    }

    public String getInstallRoot() {
        return component_.getComponentContext().getInstallRoot();
    }

    public DocumentFragment getInstallationDescriptorExtension() {
        return descriptor_.getExtensionData();
    }

    public boolean isInstall() {
        return isInstall_;
    }

    public void setClassPathElements(List classPathElements) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
