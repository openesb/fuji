/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LinkedEndpoint.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import com.sun.jbi.messaging.stats.METimestamps;
import javax.xml.namespace.QName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;

/** 
 * @author Sun Microsystems, Inc.
 */
public class LinkedEndpoint extends RegisteredEndpoint
{
    /** Linked endpoints are global in scope and are activated as a result of
     *  service connection information in a service assembly.  Therefore they 
     *  are not 'owned' by a single component.  This constant is provided so
     *  to avoid a null ownerId var in RegisteredEndpoint.
     */
    private static final String OWNER_ID = "linked";
    
    private QName   mServiceLink;
    private String  mEndpointLink;
    private Link    mLinkType;

    /** Creates a new MappedEndpoint.
     *  @param service service name
     *  @param endpoint endpoint name
     *  @param linkType link type for connection mapping
     *  @param reference endpoint reference for this endpoint
     *  @param type endpoint type
     */
    public LinkedEndpoint(
        QName fromService, String fromEndpoint,
        QName toService, String toEndpoint,
        Link linkType)
    {
        super(fromService, fromEndpoint, OWNER_ID);
        
        mLinkType       = linkType;
        mServiceLink    = toService;
        mEndpointLink   = toEndpoint;
    }
    
    public int getType() 
    {
        return LINKED;
    }
    
    public String getLinkType()
    {
        return mLinkType.toString();
    }
    
    public String getEndpointLink()
    {
        return mEndpointLink;
    }
    
    public QName getServiceLink()
    {
        return mServiceLink;
    }

    public void setInUse(String ownerId)
    {
        mActiveExchanges.incrementAndGet();
        if (mOwnerId.equals(OWNER_ID))
        {
            mOwnerId = ownerId;
        }
    }

        synchronized void updateStatistics(MessageExchangeProxy me)
    {
        int             mask = me.getPhaseMask();
        METimestamps    ts = me.getTimestamps();

        if ((mask & MessageExchangeProxy.PM_RECEIVE_REPLY) != 0)
        {
            mReceiveReply++;
        }
        if ((mask & MessageExchangeProxy.PM_SEND_FAULT) != 0)
        {
            mSendFault++;
            mLastFaultTime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_SEND_DONE) != 0)
        {
            mSendDONE++;
            mLastDONETime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_SEND_ERROR) != 0)
        {
            mSendERROR++;
            mLastERRORTime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_SEND_REQUEST) != 0)
        {
            mSendRequest++;
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_FAULT) != 0)
        {
            mReceiveFault++;
            mLastFaultTime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_DONE) != 0)
        {
            mReceiveDONE++;
            mLastDONETime = System.currentTimeMillis();
        }
        if ((mask & MessageExchangeProxy.PM_RECEIVE_ERROR) != 0)
        {
            mReceiveERROR++;
            mLastERRORTime = System.currentTimeMillis();
        }
        if (ts != null)
        {
            mStatusTime.addSample(ts.mStatusTime);
            mNMRTime.addSample(ts.mNMRTime);
            mChannelTime.addSample(ts.mConsumerChannelTime);
            mComponentTime.addSample(ts.mConsumerTime);
        }
    }

    //-------------------------EndpointStatistics-------------------------------

    public String getName()
    {
        return (toExternalName());
    }

    /**
     * List of item names for CompositeData construction.
     */
    private static final int ITEMS_BASE = 13;
    private static final int ITEMS_EXTRA = 16;

    private static final String[] ITEM_NAMES = {
        "OwningChannel",
        "ActiveExchanges",
        "SendRequest",
        "ReceiveReply",
        "SendFault",
        "ReceiveFault",
        "LastFaultTime",
        "SendDONE",
        "ReceiveDONE",
        "LastDONETime",
        "SendERROR",
        "ReceiveERROR",
        "LastERRORTime",
        "StatusTimeMin (ns)",
        "StatusTimeAvg (ns)",
        "StatusTimeMax (ns)",
        "StatusTimeStd (ns)",
        "NMRTimeMin (ns)",
        "NMRTimeAvg (ns)",
        "NMRTimeMax (ns)",
        "NMRTimeStd (ns)",
        "ComponentTimeMin (ns)",
        "ComponentTimeAvg (ns)",
        "ComponentTimeMax (ns)",
        "ComponentTimeStd (ns)",
        "ChannelTimeMin (ns)",
        "ChannelTimeAvg (ns)",
        "ChannelTimeMax (ns)",
        "ChannelTimeStd (ns)"
    };

    /**
     * List of descriptions of items for ComponsiteData construction.
     */
    private static final String ITEM_DESCRIPTIONS[] = {
        "Owning DeliveryChannel",
        "Active Exchanges",
        "Number of requests sent",
        "Number of replies received",
        "Number of faults sent",
        "Number of faults received",
        "Timestamp of last fault",
        "Number of DONE requests sent",
        "Number of DONE requests received",
        "Timestamp of last DONE",
        "Number of ERROR requests sent",
        "Number of ERROR requests received",
        "Timestamp of last ERROR",
        "Status Time Min",
        "Status Time Avg",
        "Status Time Max",
        "Status Time Std",
        "NMR Time Min",
        "NMR Time Avg",
        "NMR Time Max",
        "NMR Time Std",
        "Component Time Min",
        "Component Time Avg",
        "Component Time Max",
        "Component Time Std",
        "Channel Time Min",
        "Channel Time Avg",
        "Channel Time Max",
        "Channel Time Std"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType ITEM_TYPES[] = {
         SimpleType.STRING,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG
    };

    public CompositeData        getStatistics()
    {
        try
        {
            Object      values[];
            String      names[];
            String      descs[];
            OpenType    types[];
            boolean     enabled = // EndpointRegistry.getInstance().statisticsEnabled() ||
                                  mChannelTime.getCount() != 0;

            if (enabled)
            {
                values = new Object[ITEMS_BASE + ITEMS_EXTRA];
                types = ITEM_TYPES;
                names = ITEM_NAMES;
                descs = ITEM_DESCRIPTIONS;
            }
            else
            {
                values = new Object[ITEMS_BASE];
                types = new OpenType[ITEMS_BASE];
                System.arraycopy(ITEM_TYPES, 0, types, 0, ITEMS_BASE);
                names = new String[ITEMS_BASE];
                System.arraycopy(ITEM_NAMES, 0, names, 0, ITEMS_BASE);
                descs = new String[ITEMS_BASE];
                System.arraycopy(ITEM_DESCRIPTIONS, 0, descs, 0, ITEMS_BASE);
            }


            values[0] = mOwnerId;
            values[1] = mActiveExchanges.get();
            values[2] = mSendRequest;
            values[3] = mReceiveReply;
            values[4] = mSendFault;
            values[5] = mReceiveFault;
            values[6] = mLastFaultTime;
            values[7] = mSendDONE;
            values[8] = mReceiveDONE;
            values[9] = mLastDONETime;
            values[10] = mSendERROR;
            values[11] = mReceiveERROR;
            values[12] = mLastERRORTime;
            if (enabled)
            {
                values[13] = mStatusTime.getMin();
                values[14] = (long)mStatusTime.getAverage();
                values[15] = mStatusTime.getMax();
                values[16] = (long)mStatusTime.getSd();
                values[17] = mNMRTime.getMin();
                values[18] = (long)mNMRTime.getAverage();
                values[19] = mNMRTime.getMax();
                values[20] = (long)mNMRTime.getSd();
                values[21] = mComponentTime.getMin();
                values[22] = (long)mComponentTime.getAverage();
                values[23] = mComponentTime.getMax();
                values[24] = (long)mComponentTime.getSd();
                values[25] = mChannelTime.getMin();
                values[26] = (long)mChannelTime.getAverage();
                values[27] = mChannelTime.getMax();
                values[28] = (long)mChannelTime.getSd();
            }

            return new CompositeDataSupport(
                new CompositeType(
                    "EndpointStatistics",
                    "Endpoint statistics",
                    names, descs, types), names, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            ; // ignore this for now
        }
        return (null);
    }

    /** Compares two LinkedEndpoint instances for equality. */
    public boolean equals(Object obj)
    {
        LinkedEndpoint le;
        boolean isEqual = false;
        
        if (obj instanceof LinkedEndpoint)
        {
            le = (LinkedEndpoint)obj;
            isEqual = (mLinkType == le.mLinkType) &&
                    (mEndpointLink.equals(le.mEndpointLink)) &&
                    (mServiceLink.equals(le.mServiceLink)) &&
                    (getEndpointName().equals(le.getEndpointName())) &&                    
                    (getServiceName().equals(le.getServiceName()));
                   
        }
        
        return isEqual;
    }
    
    /** Using to->service, to->endpoint, and from->service to produce hashcode.
     */
    public int hashCode()
    {
        return (mServiceLink.hashCode() ^ mEndpointLink.hashCode() ^ super.hashCode());    
    }
    
    public String toStringBrief()
    {
        StringBuilder   sb = new StringBuilder();

        sb.append("        EndPoint Type: Linked\n");
        sb.append(super.toStringBrief());
        sb.append("          LinkedService:  ");
        sb.append(mServiceLink == null ? "Null" : mServiceLink);
        sb.append(",\n          LinkedEndpoint: ");
        sb.append(mEndpointLink == null ? "Null" : mEndpointLink);
        sb.append("\n");
        return (sb.toString());
    }
}
