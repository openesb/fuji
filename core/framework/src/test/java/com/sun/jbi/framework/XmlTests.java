/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XmlTests.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework;

import com.sun.jbi.framework.result.*;
import java.io.StringReader;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import junit.framework.TestCase;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

/**
 * As most components do not return management messages with the
 * correct namespace URI, it's decided that handling of management
 * message xml will NOT be namespace aware.
 *
 * @author kcbabo
 */
public class XmlTests extends TestCase {
    
    protected static String MGMT_MSG_SCHEMA = 
            "schemas/managementMessage.xsd";
    
    protected static String JBI_XML_SCHEMA = 
            "schemas/jbi.xsd";
    
    // Schema used to validate XML fragments
    private String schemaLocation_;
    
    private DocumentBuilder docBuilder;
    
    public XmlTests(String testName, String schemaLocation) {
        super(testName);
        
        schemaLocation_ = schemaLocation;
        
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // see class comments above
//            dbf.setNamespaceAware(true);
            docBuilder = dbf.newDocumentBuilder();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public Element stringToElement(String xml) throws Exception {
        return docBuilder.parse(new InputSource(
                new StringReader(xml))).getDocumentElement();
    }
    
    public Element createElement(TaskElem name) throws Exception {
        return docBuilder.newDocument().createElement(name.toString());
    }
    
    public void validate(Element e) throws Exception {
     // see class comments above
        /*
        SchemaFactory sf = SchemaFactory.newInstance(
                XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new StreamSource(
                getClass().getClassLoader().getResourceAsStream(schemaLocation_)));
        Validator validator = schema.newValidator();
        
        validator.validate(new DOMSource(e.getOwnerDocument()));
        */
    }
}
