/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkTaskResultDetailsTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.XmlTests;


/**
 * Unit tests for FrameworkTaskResultDetails class.
 * @author kcbabo
 */
public class FrameworkTaskResultDetailsTest extends XmlTests {
    
    private static final String LOCALE = "es";
    
    private static final String TEST_ELEMENT = 
            "<frmwk-task-result-details xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<task-result-details></task-result-details> " +
                "<locale>" + LOCALE + "</locale>" +
             "</frmwk-task-result-details>";
    
    public FrameworkTaskResultDetailsTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }            

    /**
     * Test of getTaskResultDetails method, of class FrameworkTaskResultDetails.
     */
    public void testGetTaskResultDetails() throws Exception {
        FrameworkTaskResultDetails ftrd = new FrameworkTaskResultDetails(
                stringToElement(TEST_ELEMENT));
        assertNotNull(ftrd.getTaskResultDetails());
    }

    /**
     * Test of getLocale method, of class FrameworkTaskResultDetails.
     */
    public void testGetLocale() throws Exception {
        FrameworkTaskResultDetails ftrd = new FrameworkTaskResultDetails(
                stringToElement(TEST_ELEMENT));
        assertEquals(LOCALE, ftrd.getLocale());
    }

    /**
     * Test of setLocale method, of class FrameworkTaskResultDetails.
     */
    public void testSetLocale() throws Exception {
        FrameworkTaskResultDetails ftrd = new FrameworkTaskResultDetails(
                createElement(TaskElem.frmwk_task_result_details));
        
        ftrd.setLocale(LOCALE);
        assertEquals(LOCALE, ftrd.getLocale());
    }
    
    
    /**
     * Test creating a new FrameworkTaskResultDetails element from scratch.
     */
    public void testCreateNewFrameworkTaskResultDetails() throws Exception {
        FrameworkTaskResultDetails ftrd = new FrameworkTaskResultDetails(
                createElement(TaskElem.frmwk_task_result_details));
        
        assertNotNull(ftrd.getTaskResultDetails());
    }
}
