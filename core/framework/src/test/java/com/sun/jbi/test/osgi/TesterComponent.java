/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TesterComponent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.test.osgi;

import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.InstallationContext;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.DeploymentException;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.management.ObjectName;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

/**
 *
 * @author kcbabo
 */
public class TesterComponent implements 
        javax.jbi.component.Component, 
        javax.jbi.component.Bootstrap, 
        javax.jbi.component.ComponentLifeCycle, 
        javax.jbi.component.ServiceUnitManager {

    /******************** Component SPI *********************/
    public ComponentLifeCycle getLifeCycle() {
        return this;
    }

    public Document getServiceDescription(ServiceEndpoint arg0) {
        return null;
    }

    public ServiceUnitManager getServiceUnitManager() {
        return this;
    }

    public boolean isExchangeWithConsumerOkay(ServiceEndpoint arg0, MessageExchange arg1) {
        return true;
    }

    public boolean isExchangeWithProviderOkay(ServiceEndpoint arg0, MessageExchange arg1) {
        return true;
    }

    public ServiceEndpoint resolveEndpointReference(DocumentFragment arg0) {
        return null;
    }
    
    /******************** Bootstrap SPI *********************/
    
    public void cleanUp() throws JBIException {
    }

    public ObjectName getExtensionMBeanName() {
        ObjectName on = null;
        try {
            on = new ObjectName("com.sun.jbi:Type=Extension");
        }
        catch (javax.management.MalformedObjectNameException ex) {
        }
        return on;
    }

    public void init(InstallationContext arg0) throws JBIException {
        
    }

    public void onInstall() throws JBIException {
        
    }

    public void onUninstall() throws JBIException {
        
    }

    /******************* Life Cycle SPI ********************/

    public void init(ComponentContext arg0) throws JBIException {
        
    }

    public void shutDown() throws JBIException {
        
    }

    public void start() throws JBIException {
        
    }

    public void stop() throws JBIException {
        
    }

    /**************** ServiceUnitManager SPI ****************/

    public String deploy(String arg0, String arg1) throws DeploymentException {
        return null;
    }

    public void init(String arg0, String arg1) throws DeploymentException {
        
    }

    public void shutDown(String arg0) throws DeploymentException {
        
    }

    public void start(String arg0) throws DeploymentException {
        
    }

    public void stop(String arg0) throws DeploymentException {
        
    }

    public String undeploy(String arg0, String arg1) throws DeploymentException {
        return null;
    }
    
}
