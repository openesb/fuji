/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRobustInOnly.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;


import javax.xml.namespace.QName;
/**
 * Test RobustInOnly Message Exchange.
 * @author Sun Microsystems, Inc.
 */
public class TestRobustInOnly extends junit.framework.TestCase
{
    private static final QName  SERVICE  = new QName("RobustInOnlyService");
    private static final String ENDPOINT = "RobustInOnlyEndpoint";
    private static final QName  OPERATION = new QName("foobar");
    
    private MessageService          mMsgSvc;
    private MessageExchangeFactory  mFactory;
     /** NMR Environment Context */
    private NMRContext mContext;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestRobustInOnly(String aTestName) throws Exception
    {
        super(aTestName);
        mMsgSvc = new MessageService();
        mFactory = new ExchangeFactory(mMsgSvc);
        mContext = new NMRContext(mMsgSvc);
    }

    /**
     * Setup for the test. 
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mMsgSvc.startService();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mMsgSvc.stopService();
        mContext.reset();
    }
    
// =============================  test methods ================================
 
    /**
     * Happy path test for an RobustInOnly exchange with fault.
     * @throws Exception test failed
     */
    public void testExchangeGood()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.ROBUST_IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for errors
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an RobustInOnly exchange with status.
     * @throws Exception test failed
     */
    public void testExchangeGood2()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding2(mMsgSvc);
        engine  = new HappyEngine2(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.ROBUST_IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for errors
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an RobustInOnly exchange with fault.
     * @throws Exception test failed
     */
    public void testExchangeBindingSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.ROBUST_IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for errors
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an RobustInOnly exchange with fault.
     * @throws Exception test failed
     */
    public void testExchangeEngineSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.ROBUST_IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for errors
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an RobustInOnly exchange with fault.
     * @throws Exception test failed
     */
    public void testExchangeBothSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.ROBUST_IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for errors
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */    
    public void testExchangeFailure()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new BadBinding(mMsgSvc);
        engine  = new BadEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.ROBUST_IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    
    
// ============================ internal stuff ================================
    
    class HappyBinding extends Binding
    {
        HappyBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   outMsg;
            Fault               fault;
            RobustInOnly      robInOnly;

            // create the exchange
            robInOnly = mFactory.createRobustInOnlyExchange();
            outMsg  = robInOnly.createMessage();
            assertEquals(robInOnly.getPattern().toString(), ExchangePattern.ROBUST_IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, robInOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());

            // lookup the endpoint reference and set on exchange
            robInOnly.setEndpoint(mEndpoint);
            robInOnly.setOperation(OPERATION);

            // set the payload
            Payload.setPayload(outMsg);
            
            // set the out message
            robInOnly.setInMessage(outMsg);

            // send the exchange
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());

            // Check that settings are ignored while ownership is elsewhere.

            robInOnly.setEndpoint(null);
            assertEquals(mEndpoint, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertEquals(OPERATION, robInOnly.getOperation());

            // receive the response
            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());
            
            // check for a fault
            fault = robInOnly.getFault();
            assertTrue(fault != null);
            
            robInOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());
            
            // Check that settings are ignored when DONE.

            robInOnly.setEndpoint(null);
            assertEquals(mEndpoint, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertEquals(OPERATION, robInOnly.getOperation());
        }
    }
    
    class HappyBinding2 extends Binding
    {
        HappyBinding2(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   outMsg;
            Fault               fault;
            RobustInOnly        robInOnly;

            // create the exchange
            robInOnly = mFactory.createRobustInOnlyExchange();
            outMsg  = robInOnly.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());

            // lookup the endpoint reference and set on exchange
            robInOnly.setEndpoint(mEndpoint);
            robInOnly.setOperation(OPERATION);

            // set the payload
            Payload.setPayload(outMsg);
            
            // set the out message
            robInOnly.setInMessage(outMsg);

            // send the exchange
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());

            // receive the response
            robInOnly = (RobustInOnly)mChannel.accept();
            
            // check for a fault
            fault = robInOnly.getFault();
            assertTrue(fault == null);
            
            // Check that a Status was returned.
            assertTrue(robInOnly.getStatus() != ExchangeStatus.ACTIVE);

            // Check that we can't set the status.
            try
            {
                robInOnly.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set Status after DONE.");
            }
            catch (Exception ex) {};

            // Check that we can't send the status.
            try
            {
                mChannel.send(robInOnly);
                setFailure("Able to send Status after DONE.");
            }
            catch (Exception ex) {};

        }
    }
    
    class HappySynchBinding extends Binding
    {
        HappySynchBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   outMsg;
            Fault               fault;
            RobustInOnly      robInOnly;

            // create the exchange
            robInOnly = mFactory.createRobustInOnlyExchange();
            outMsg  = robInOnly.createMessage();
            assertEquals(robInOnly.getPattern().toString(), ExchangePattern.ROBUST_IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, robInOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());

            // lookup the endpoint reference and set on exchange
            robInOnly.setEndpoint(mEndpoint);
            robInOnly.setOperation(OPERATION);

            // set the payload
            Payload.setPayload(outMsg);
            
            // set the out message
            robInOnly.setInMessage(outMsg);

            // send the exchange
            assertTrue(mChannel.sendSync(robInOnly));
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());
            
            // check for a fault
            fault = robInOnly.getFault();
            assertTrue(fault != null);
            
            robInOnly.setStatus(ExchangeStatus.DONE);
            // Try sendSync
            try
            {
                mChannel.sendSync(robInOnly);
                setFailure("Able to sendSync when not legal");
            }
            catch (Exception ex) {};
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());
            
            // Check that settings are ignored when DONE.

            robInOnly.setEndpoint(null);
            assertEquals(mEndpoint, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertEquals(OPERATION, robInOnly.getOperation());
        }
    }
    
    
    class BadBinding extends Binding
    {
        BadBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   outMsg;
            Fault               fault;
            RobustInOnly       robInOnly;

            // create the exchange
            robInOnly = mFactory.createRobustInOnlyExchange();
            outMsg  = robInOnly.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());
            
            // lookup the endpoint reference and set on exchange
            robInOnly.setEndpoint(mEndpoint);
            robInOnly.setOperation(OPERATION);

            // set the payload
            Payload.setPayload(outMsg);
            
            // FAILURE CASE 1 := set fault as the first step
            fault = robInOnly.createFault();
            try
            {
                robInOnly.setFault(fault);
                setFailure("Able to set fault as first message in exchange");
            }
            catch (Exception ex) {}
            
            // FAILURE CASE 2 := set status as first step
            try
            {
                robInOnly.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status to done as first step in exchange");
            }
            catch (Exception ex) {}
            
            // Now we can proceed normally            
            mChannel.send(robInOnly);

            // receive the response
            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(MessageExchange.Role.CONSUMER, robInOnly.getRole());
            
            // check for a fault
            fault = robInOnly.getFault();
            assertTrue(fault == null);
            
            // FAILURE CASE 3 := set status after DONE
            try
            {
                robInOnly.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status to done after DONE.");
            }
            catch (Exception ex) {}

            // FAILURE CASE 4 := send after DONE.
            try
            {
                mChannel.send(robInOnly);
                setFailure("Able to send after DONE.");
            }
            catch (Exception ex) {}

        }
    }

    class HappyEngine extends Engine
    {
        HappyEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            Fault           fault;
            RobustInOnly    robInOnly;

            // accept the message exchange
            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(robInOnly.getPattern().toString(), ExchangePattern.ROBUST_IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            robInOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, robInOnly.getOperation());

            // check the out message
            if (robInOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            // generate a fault
            fault = robInOnly.createFault();
            robInOnly.setFault(fault);
            
            // send the exchange with fault
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
            
            // wait for status from engine
            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(ExchangeStatus.DONE, robInOnly.getStatus());
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());

            // Check that settings are ignored after message is DONE.

            robInOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, robInOnly.getOperation());
        }
    }    
    
    class HappySynchEngine extends Engine
    {
        HappySynchEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            Fault           fault;
            RobustInOnly    robInOnly;

            // accept the message exchange
            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(robInOnly.getPattern().toString(), ExchangePattern.ROBUST_IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            robInOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, robInOnly.getOperation());

            // check the out message
            if (robInOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            // generate a fault
            fault = robInOnly.createFault();
            robInOnly.setFault(fault);
            
            // send the exchange with fault
            assertTrue(mChannel.sendSync(robInOnly));
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
            
            assertEquals(ExchangeStatus.DONE, robInOnly.getStatus());

            // Check that settings are ignored after message is DONE.

            robInOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, robInOnly.getEndpoint());

            robInOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, robInOnly.getOperation());
        }
    }   
    
    class HappyEngine2 extends Engine
    {
        HappyEngine2(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            Fault           fault;
            RobustInOnly   robInOnly;

            // accept the message exchange
            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
            
            // check the out message
            if (robInOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            // generate a fault
            robInOnly.setStatus(ExchangeStatus.DONE);
            
            // Try and create a fault after decided to set Status.
            try
            {
                fault = robInOnly.createFault();
                setFailure("Able to create a fault after setting status.");
            }
            catch (Exception ex) {}

            // Try and set a fault after decided to set Status.
            try
            {
                robInOnly.setFault(null);
                setFailure("Able to set a fault after setting status.");
            }
            catch (Exception ex) {}
            
            // Send status to Enging.
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());

            // wait for status from engine
            assertEquals(ExchangeStatus.DONE, robInOnly.getStatus());
        }
    }    
    
    class BadEngine extends Engine
    {
        BadEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            RobustInOnly       robInOnly;
            NormalizedMessage   msg;

            robInOnly = (RobustInOnly)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
                        
            // Try and set out message again
            try
            {
                msg = robInOnly.createMessage();
                robInOnly.setInMessage(msg);
                setFailure("Able to set in message from binding");
                return;
            }
            catch (Exception ex1) {};
                        
            // complete the exchange normally
            robInOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(robInOnly);
            assertEquals(MessageExchange.Role.PROVIDER, robInOnly.getRole());
        }
    }
}
