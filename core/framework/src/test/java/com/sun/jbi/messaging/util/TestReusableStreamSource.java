/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestReusableStreamSource.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.Reader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

/**
 *
 * @author
 */
public class TestReusableStreamSource extends junit.framework.TestCase
{
    private Document            mDocument;
    private Transformer         mTransform;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestReusableStreamSource(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

            try
            {
                // initialize transformer details
                mTransform = TransformerFactory.newInstance().newTransformer();            
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
                factory.setNamespaceAware(true);
                mDocument = factory.newDocumentBuilder().newDocument();
            }
            catch (javax.xml.transform.TransformerFactoryConfigurationError tfcEx)
            {
                throw new java.lang.Exception("Can't create Transformer");
            }
            catch (javax.xml.transform.TransformerConfigurationException cfgEx)
            {
                throw new java.lang.Exception("Can't create Transformer");
            }
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
    }

// =============================  test methods ================================

    /**
     *  Test single operation case for service
     */
    public void testBasicStreamOperations()
        throws Exception
    {
        Element     e = mDocument.createElement("MyElement");
        Text        t = mDocument.createTextNode("MyText");
        DOMSource   ds = new DOMSource();
        StreamResult    sr;
        ByteArrayOutputStream   baos;
        ByteArrayInputStream    bais;
        StreamSource   ss;
        
        e.appendChild(t);
        ds.setNode(e);
        
        ReusableStreamSource   rss;

        //
        // Try with an InputStream.
        //
        mTransform.transform(ds, sr = new StreamResult(baos = new ByteArrayOutputStream()));
        ss = new StreamSource(bais = new ByteArrayInputStream(baos.toByteArray()));
        bais.reset();
        int bs = bais.available();
        
        //
        //  Check that they are the same to start.
        //
        rss = new ReusableStreamSource(ss);
        assertTrue(rss.getSource() == ss);
        assertTrue(rss.getPublicId() == ss.getPublicId());
        assertTrue(rss.getSystemId() == ss.getSystemId());
        
        //
        //  Once we ask for a Stream, they are seperate but repeatable
        //
        InputStream is = rss.getInputStream();
        assertTrue(rss.getSource() != ss);
        assertEquals(is.available(), bs);
        assertTrue(rss.getInputStream() != is);
        assertEquals(rss.getInputStream().available(), bs);

        //
        //  Try with a Reader.
        //
        bais.reset();
        ss = new StreamSource(new InputStreamReader(bais));

        //
        //  Check that they are the same to start.
        //
        rss = new ReusableStreamSource(ss);
        assertTrue(rss.getSource() == ss);
        assertTrue(rss.getPublicId() == ss.getPublicId());
        assertTrue(rss.getSystemId() == ss.getSystemId());

        //
        //  Once we ask for a Stream, they are seperate but repeatable
        //
        char[]  chars = new char[256];
        Reader r = rss.getReader();
        assertTrue(rss.getSource() != ss);
        assertTrue(rss.getReader() != r);
        assertEquals(r.read(chars),bs);
        assertEquals(rss.getReader().read(chars), bs);

    }
    

}
