/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptorWrapperTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import org.glassfish.openesb.api.service.ServiceMessage;
import com.sun.jbi.interceptors.Intercept;


import java.lang.reflect.Method;
import java.util.Properties;
import javax.jbi.messaging.MessageExchange;
import junit.framework.TestCase;

/**
 * Unit tests for Intereceptor Wrapper class.
 * @author nkaps
 */
public class InterceptorWrapperTest extends TestCase {


    
    public InterceptorWrapperTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
       
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test getInterceptorMethods method, of InterceptorWrapper
     */
    public void testGetInterceptorMethods() throws Exception {
        
        Object obj = new SomeClass();
        InterceptorWrapper wrapper = new InterceptorWrapper(obj);
        Method[] annotatedMethods = wrapper.getInterceptorMethods();
        assertTrue(annotatedMethods.length == 3);
        
    }
    
    /**
     * Test getMessageExchangeInterceptors method, of InterceptorWrapper
     */
    public void testGetMessageExchangeInterceptors() throws Exception {
        
        Object obj = new SomeClass();
        InterceptorWrapper wrapper = new InterceptorWrapper(obj);
        Method[] annotatedMethods = wrapper.getMessageExchangeInterceptors();
        assertTrue(annotatedMethods.length == 1);
        
    }

    /**
     * Test getMessageInterceptors method, of InterceptorWrapper
     */
    public void testGetMessageInterceptors() throws Exception {
        
        Object obj = new SomeClass();
        InterceptorWrapper wrapper = new InterceptorWrapper(obj);
        Method[] annotatedMethods = wrapper.getMessageInterceptors();
        assertTrue(annotatedMethods.length == 1);
        
    }    
   
    
    private class SomeClass
    {
        @Intercept()
        public void interceptor1(ServiceMessage msg)
        {
            
        }
        
        @Intercept()
        public void interceptor2(MessageExchange exchange)
        {
            
        }
        
                
        @Intercept()
        public void interceptor4()
        {
            
        }
    }
}
