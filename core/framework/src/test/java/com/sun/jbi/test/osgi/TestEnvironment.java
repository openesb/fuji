/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEnvironment.java - Last published on 4/10/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.test.osgi;

import com.sun.jbi.framework.osgi.internal.ComponentManager;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.osgi.internal.ServiceAssemblyManager;
import com.sun.jbi.messaging.MessageService;
import org.osgi.framework.BundleContext;

/**
 * Test subclass of Environment to allow tests to access protected
 * setter methods.
 * 
 * @author mwhite
 */
public class TestEnvironment extends Environment {

    public static void setMessageService(MessageService messageService) {
        Environment.setMessageService(messageService);
    }
    
    public static void setServiceAssemblyManager(ServiceAssemblyManager saManager) {
        Environment.setServiceAssemblyManager(saManager);
    }
    
    public static void setComponentManager(ComponentManager compManager) {
        Environment.setComponentManager(compManager);
    }
    
    public static void setFrameworkBundleContext(BundleContext bundleContext) {
        Environment.setFrameworkBundleContext(bundleContext);
    }
    
}
