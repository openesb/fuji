/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServicePID.java - Last published on Jun 30, 2009
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.configuration;

/**
 *
 * @author Sun Microsystems
 */
public class TestServicePID extends junit.framework.TestCase {
    
    public TestServicePID(String aTestName){
        super(aTestName);
    }
    
    public void setUp()
        throws Exception{
        super.setUp();
    }
    
    public void tearDown()
        throws Exception{
        super.tearDown();
    }
    
    public void testGetName(){
        ServicePID svcPID = new ServicePID("fuji-configuration.interceptor.myInterceptor");
        assertEquals("myInterceptor", svcPID.getName()); 
    }

    public void testGetName2(){
        ServicePID svcPID = new ServicePID("fuji-configuration.interceptor.my.Interceptor.One");
        assertEquals("my.Interceptor.One", svcPID.getName()); 
    }
        
    public void testGetType(){
        ServicePID svcPID = new ServicePID("fuji-configuration.interceptor.myInterceptor");
        assertEquals("interceptor", svcPID.getType()); 
    }
    
    public void testGetDomain(){
        ServicePID svcPID = new ServicePID("fuji-configuration.interceptor.myInterceptor");
        assertEquals( "fuji-configuration", svcPID.getDomain()); 
    }

}
