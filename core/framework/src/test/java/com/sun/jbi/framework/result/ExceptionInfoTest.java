/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExceptionInfoTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.XmlTests;


/**
 * Unit tests for ExceptionInfo class.
 * @author kcbabo
 */
public class ExceptionInfoTest extends XmlTests {
    
    private static final String STACK_TRACE = "stack trace";
    private static final String NESTING_LEVEL = "1";
    
    private static final String TEST_ELEMENT = 
            "<exception-info xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<nesting-level>" + NESTING_LEVEL + "</nesting-level>" +
                "<msg-loc-info></msg-loc-info> " +
                "<stack-trace>" + STACK_TRACE + "</stack-trace>" +
             "</exception-info>";
    
    
    public ExceptionInfoTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }

    /**
     * Test of getLocalizedMessage method, of class ExceptionInfo.
     */
    public void testGetLocalizedMessage() throws Exception {
        ExceptionInfo ei = new ExceptionInfo(stringToElement(TEST_ELEMENT));
        assertNotNull(ei.getLocalizedMessage());
    }

    /**
     * Test of getNestingLevel method, of class ExceptionInfo.
     */
    public void testGetNestingLevel() throws Exception {
        ExceptionInfo ei = new ExceptionInfo(stringToElement(TEST_ELEMENT));
        assertEquals(ei.getNestingLevel(), NESTING_LEVEL);
    }

    /**
     * Test of getStackTrace method, of class ExceptionInfo.
     */
    public void testGetStackTrace() throws Exception {
        ExceptionInfo ei = new ExceptionInfo(stringToElement(TEST_ELEMENT));
        assertEquals(ei.getStackTrace(), STACK_TRACE);
    }
    
    /**
     * Test of setMessage method, of class ExceptionInfo.
     */
    public void testSetNestingLevel() throws Exception {
        ExceptionInfo ei = new ExceptionInfo(
                createElement(TaskElem.exception_info));
        ei.setNestingLevel(NESTING_LEVEL);
        assertEquals(NESTING_LEVEL, ei.getNestingLevel());
    }
    
    /**
     * Test of setMessage method, of class ExceptionInfo.
     */
    public void testSetStackTrace() throws Exception {
        ExceptionInfo ei = new ExceptionInfo(
                createElement(TaskElem.exception_info));
        ei.setStackTrace(STACK_TRACE);
        assertEquals(STACK_TRACE, ei.getStackTrace());
    }

}
