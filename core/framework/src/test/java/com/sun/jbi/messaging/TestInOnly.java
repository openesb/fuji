/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestInOnly.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;


import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;


import javax.transaction.xa.XAResource;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import javax.xml.namespace.QName;
/**
 * Test InOnly Message Exchange.
 * @author Sun Microsystems, Inc.
 */
public class TestInOnly extends junit.framework.TestCase
{
    private static final QName  SERVICE  = new QName("InOnlyService");
    private static final String ENDPOINT = "InOnlyEndpoint";
    private static final QName  OPERATION = new QName("fooOperation");
    private static final String SENDSYNC_PROPERTY_NAME = "javax.jbi.messaging.sendSync";
    private TransactionManager  mTM;
    private FakeXAResource      mXAR;
    private FakeXAResource      mXAR2;
    
    private MessageService          mMsgSvc;
     /** NMR Environment Context */
    private NMRContext mContext;
    private MessageExchangeFactory  mFactory;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestInOnly(String aTestName)
        throws Exception
    {
        super(aTestName);
        mMsgSvc = new MessageService();
        mContext = new NMRContext(mMsgSvc);
    }

    /**
     * Setup for the test. 
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();        
        
        mMsgSvc.startService();
        mFactory = new ExchangeFactory(mMsgSvc);
        
        //
        //  When running as a LifeCycleListener, one can use the Context that is
        //  that is past at startup, cast it to the internal implementation:
        //  ((com.sun.jbi.framework.EnvironmentContext)ctx).getTransactionManager();
        //  NOTE: the TransactionManager isn't available at this early time, soyou have
        //  save the ctx until after processing has started.
        //
        mTM = FakeTransactionManager.getTM();
        mMsgSvc.setTransactionManager(mTM);
        mXAR = new FakeXAResource();
        mXAR2 = new FakeXAResource();
        
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mMsgSvc.stopService();
        mContext.reset();
    }
    
// =============================  test methods ================================
 
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeGood()
        throws Exception
    {
        System.out.println("testExchangeGood");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        mContext.addComponentInstance("engine", engine);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
 
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeBindingSynch()
        throws Exception
    {
        System.out.println("testExchangeBindingSynch");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeEngineSynch()
        throws Exception
    {
        System.out.println("testExchangeEngineSynch");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);        
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeBothSynch()
        throws Exception
    {
        System.out.println("testExchangeBothSynch");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeBindingCheck()
        throws Exception
    {
        Binding binding;
        Engine  engine;

        // Engine is provider, binding is consumer

        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappySynchEngineCheck(mMsgSvc);

        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);

        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeGoodTimeout()
        throws Exception
    {
        System.out.println("testExchangeGoodTimeout");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new HappyTimeoutBinding(mMsgSvc);
        engine  = new HappyTimeoutEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
     
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeGoodSynchTimeout()
        throws Exception
    {
        System.out.println("testExchangeGoodSynchTimeout");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new HappySynchTimeoutBinding(mMsgSvc);
        engine  = new HappyTimeoutEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeBadSynchTimeout()
        throws Exception
    {
        System.out.println("testExchangeBadSynchTimeout");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer
        
        binding = new BadSynchTimeoutBinding(mMsgSvc);
        engine  = new BadTimeoutEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }

    /**
     * Attempt to do bad things with an InOnly exchange.
     * @throws Exception test failed
     */
    public void testExchangeFailure()
        throws Exception
    {
        System.out.println("testExchangeFailure");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer

        binding = new HappyBinding(mMsgSvc);
        engine  = new BadEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Attempt to do bad things with an InOnly exchange.
     * @throws Exception test failed
     */
    public void testServiceEndpointFailure()
        throws Exception
    {
        System.out.println("testServiceEndpointFailure");
        Binding binding;
        Engine  engine;
        
        // Engine is provider, binding is consumer

        binding = new BadServiceEndpointBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
     /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */
    public void testXactGood()
        throws Exception
    {
        System.out.println("testXactGood");
        Binding binding;
        Engine  engine;

        binding = new XactBinding(mMsgSvc);
        engine  = new XactEngine(mMsgSvc);

        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);

        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    }

    /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */
    public void testXactAbort()
        throws Exception
    {
        System.out.println("testXactAbort");
        Binding binding;
        Engine  engine;

        binding = new XactBindingAbort(mMsgSvc);
        engine  = new XactEngine(mMsgSvc);

        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);

        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    }  
    
    /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */
    public void testTransactional()
        throws Exception
    {
        System.out.println("testTransactional");
        Binding binding;
        Engine  engine;

        binding = new XactTransactionalBinding(mMsgSvc);
        engine  = new XactTransactionalEngine(mMsgSvc);

        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);

        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    } 
        
    /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */
    public void testAcceptorClose()
        throws Exception
    {
        System.out.println("testAcceptorClose");
        Binding binding;
        Engine  engine;
        Sequencer   sequencer = new Sequencer();
        
        binding = new  CloseBinding(mMsgSvc);
        engine  = new CloseEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        engine.setSequencer(sequencer);
        binding.setSequencer(sequencer);
        
        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    }  

    /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */
    public void testSenderClose()
        throws Exception
    {
        System.out.println("testSenderClose");
        Binding binding;
        Engine  engine;
        Sequencer   sequencer = new Sequencer();
        
        binding = new  Close2Binding(mMsgSvc);
        engine  = new Close2Engine(mMsgSvc);

        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        engine.setSequencer(sequencer);
        binding.setSequencer(sequencer);
        
        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    } 
    
    /**
     * Attempt to do bad things with an OutOnly exchange.
     * @throws Exception test failed
     */
    public void testsendSyncClose()
        throws Exception
    {
        System.out.println("testsendSyncClose");
        Binding binding;
        Engine  engine;
        Sequencer   sequencer = new Sequencer();
        
        binding = new  Close3Binding(mMsgSvc);
        engine  = new Close2Engine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        engine.setSequencer(sequencer);
        binding.setSequencer(sequencer);
        
        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    } 
    
    /**
     * Attempt lookup an EPR and use it for addressing.
     * @throws Exception test failed
     */
    public void testsendEPR()
        throws Exception
    {
        System.out.println("testSendEPR");
        Binding binding;
        Engine  engine;
        Sequencer   sequencer = new Sequencer();
        
        binding = new  EPRBinding(mMsgSvc);
        engine  = new EPREngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_ONLY.toString());
        binding.init(SERVICE);
        engine.setSequencer(sequencer);
        binding.setSequencer(sequencer);
        
        Framework.runTest(binding, engine);

        // check for binding or engine failure
        binding.checkError();
        engine.checkError();

        binding.stop();
        engine.stop();
    }  

// ============================ internal stuff ================================
    
    class HappyBinding extends Binding
    {
        HappyBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(mFactory.createExchange(SERVICE, OPERATION).getPattern().toString(), ExchangePattern.IN_ONLY.toString());
           
            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            // Check that settings are ignored while ownership is elsewhere.
            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());
            
            // receive the response            
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
            assertTrue(inOnly.getProperty(SENDSYNC_PROPERTY_NAME) == null);
            assertTrue(!inOnly.getPropertyNames().contains(SENDSYNC_PROPERTY_NAME));
        }
    }

    class HappyTimeoutBinding extends Binding
    {
        HappyTimeoutBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            //  Delay to check accept timeout.
            
            synchronized (this)
            {
                this.wait(1000);
            }
            
            // send the exchange
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            // Check that settings are ignored while ownership is elsewhere.
            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());
            
            // receive the response            
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }
    
    class HappySynchBinding extends Binding
    {
        HappySynchBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange and wait for response
            assertTrue(mChannel.sendSync(inOnly));
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }

    class HappySynchTimeoutBinding extends Binding
    {
        HappySynchTimeoutBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange and wait for response
            assertTrue(mChannel.sendSync(inOnly, 2000));
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }

    class BadSynchTimeoutBinding extends Binding
    {
        BadSynchTimeoutBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange and wait for response
            assertTrue(!mChannel.sendSync(inOnly, 500));
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            // Check that settings are ignored after ERROR.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }

    class BadTimeoutBinding extends Binding
    {
        BadTimeoutBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            // Check that settings are ignored while ownership is elsewhere.
            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());
            
            // receive the response            
            inOnly = (InOnly)mChannel.accept(500);
            assertTrue(inOnly == null);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // Check that settings are ignored after ERROR.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }

    class BadServiceEndpointBinding extends Binding
    {
        BadServiceEndpointBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            
            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // Try missing service/endpoint.
            try
            {
                mChannel.send(inOnly);
                setFailure("Expected exception for service/endpoint missing");
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
            
            }
            
            // send the exchange
            inOnly.setEndpoint(mEndpoint);
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            // Check that settings are ignored while ownership is elsewhere.
            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());
            
            // receive the response            
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }

    class XactBinding extends Binding
    {
        XactBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            Transaction         xact;

            // create the exchange
            inOnly = mFactory.createInOnlyExchange();
            inMsg  = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know
            inOnly.setOperation(OPERATION);

            // lookup the endpoint reference and set on exchange
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            // set the payload
            Payload.setPayload(inMsg);

            // set the message on the exchange
            inOnly.setInMessage(inMsg);

            // send the exchange
            mTM.begin();
            xact = mTM.getTransaction();
            xact.enlistResource(mXAR);
            inOnly.setProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME, xact);
            inMsg.setProperty("Value", new Integer(1234));
            mXAR.setValue(new Integer(1234));
            try
            {
                 mChannel.send(inOnly);
                 setFailure("Didn't throw exception for active transaction.");
            }
            catch (javax.jbi.JBIException jEx)
            {
            }
            mTM.suspend();
            mChannel.send(inOnly);

            // Check that settings are ignored while ownership is elsewhere.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());

            // receive the response
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

           // Check that settings are ignored when DONE.

           inOnly.setEndpoint(null);
           assertEquals(mEndpoint, inOnly.getEndpoint());

           inOnly.setOperation(null);
           assertEquals(OPERATION, inOnly.getOperation());

           xact.delistResource(mXAR, XAResource.TMSUCCESS);
           xact.commit();
           assertEquals(mXAR.getValue(), mXAR2.getValue());
           assertEquals(1234, mXAR.getValue());
        }
    }
    
    class XactBindingAbort extends Binding
    {
        XactBindingAbort(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            Transaction         xact;

            // create the exchange
            inOnly = mFactory.createInOnlyExchange();
            inMsg  = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know
            inOnly.setOperation(OPERATION);

            // lookup the endpoint reference and set on exchange
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            // set the payload
            Payload.setPayload(inMsg);

            // set the message on the exchange
            inOnly.setInMessage(inMsg);

            // send the exchange
            mTM.begin();
            xact = mTM.getTransaction();
            xact.enlistResource(mXAR);
            inOnly.setProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME, xact);
            inMsg.setProperty("Value", new Integer(1234));
            mXAR.setValue(new Integer(1234));
            try
            {
                 mChannel.send(inOnly);
                 setFailure("Didn't throw exception for active transaction.");
            }
            catch (javax.jbi.JBIException jEx)
            {
            }
            mTM.suspend();
            mChannel.send(inOnly);

            // Check that settings are ignored while ownership is elsewhere.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());

            // receive the response
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

           // Check that settings are ignored when DONE.

           inOnly.setEndpoint(null);
           assertEquals(mEndpoint, inOnly.getEndpoint());

           inOnly.setOperation(null);
           assertEquals(OPERATION, inOnly.getOperation());

           xact.delistResource(mXAR, XAResource.TMSUCCESS);
           xact.rollback();
           assertEquals(mXAR.getValue(), mXAR2.getValue());
           assertEquals(0, mXAR.getValue());
        }
    }

    class XactTransactionalBinding extends Binding
    {
        XactTransactionalBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            Transaction         xact;

            // create the exchange
            inOnly = mFactory.createInOnlyExchange();
            inMsg  = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());

            // set the stuff we know
            inOnly.setOperation(OPERATION);

            // lookup the endpoint reference and set on exchange
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            // set the payload
            Payload.setPayload(inMsg);

            // set the message on the exchange
            inOnly.setInMessage(inMsg);

            // send the exchange
            mTM.begin();
            xact = mTM.getTransaction();
            xact.enlistResource(mXAR);
            inOnly.setProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME, xact);
            inMsg.setProperty("Value", new Integer(1234));
            mXAR.setValue(new Integer(1234));
            try
            {
                 mChannel.send(inOnly);
                 setFailure("Didn't throw exception for active transaction.");
            }
            catch (javax.jbi.JBIException jEx)
            {
            }
            mTM.suspend();
            mChannel.send(inOnly);

            // Check that settings are ignored while ownership is elsewhere.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());

            // receive the response
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

           // Check that settings are ignored when DONE.

           inOnly.setEndpoint(null);
           assertEquals(mEndpoint, inOnly.getEndpoint());

           inOnly.setOperation(null);
           assertEquals(OPERATION, inOnly.getOperation());
           mTM.resume(xact);
           xact.delistResource(mXAR, XAResource.TMSUCCESS);
           xact.commit();
           assertEquals(0, mXAR2.getValue());
           assertEquals(1234, mXAR.getValue());
        }
    }
    
    class CloseBinding extends Binding
    {
        CloseBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(mFactory.createExchange(SERVICE, OPERATION).getPattern().toString(), ExchangePattern.IN_ONLY.toString());
           
            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            mChannel.close();     
            
            getSequencer().reached(1);
        }
    }

    class Close2Binding extends Binding
    {
        Close2Binding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(mFactory.createExchange(SERVICE, OPERATION).getPattern().toString(), ExchangePattern.IN_ONLY.toString());
           
            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            getSequencer().stallUntil(1);

            // receive the response            
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.ERROR, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }
    
    class Close3Binding extends Binding
    {
        Close3Binding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(mFactory.createExchange(SERVICE, OPERATION).getPattern().toString(), ExchangePattern.IN_ONLY.toString());
           
            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOnly.getEndpoint());

            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange
            mChannel.sendSync(inOnly);
        }
    }

    class EPRBinding extends Binding
    {
        EPRBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            
            // create the exchange
            inOnly  = mFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOnly.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());
            assertEquals(mFactory.createExchange(SERVICE, OPERATION).getPattern().toString(), ExchangePattern.IN_ONLY.toString());
           
            // set the stuff we know & check that they are set.
            inOnly.setEndpoint(mMsgSvc.resolveEndpointReference(null));
            assertTrue(inOnly.getEndpoint() != null);
            assertTrue(inOnly.getEndpoint() != mEndpoint);
            inOnly.setOperation(OPERATION);
            assertEquals(OPERATION, inOnly.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            assertEquals(inMsg, inOnly.getInMessage());

            // send the exchange
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.CONSUMER, inOnly.getRole());

            // Check that settings are ignored while ownership is elsewhere.
            inOnly.setEndpoint(null);
            assertTrue(inOnly.getEndpoint() != null);

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());
            
            // receive the response            
            inOnly = (InOnly)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOnly.getStatus());

            // Check that settings are ignored when DONE.

            inOnly.setEndpoint(null);
            assertTrue(inOnly.getEndpoint() != null);

            inOnly.setOperation(null);
            assertEquals(OPERATION, inOnly.getOperation());           
        }
    }
    
    class HappyEngine extends Engine
    {
        HappyEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);
            
            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            inOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());

            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());           
        }
    }
    
    class HappySynchEngine extends Engine
    {
        HappySynchEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);

            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            inOnly.setStatus(ExchangeStatus.DONE);
            try
            {
                mChannel.sendSync(inOnly);
                setFailure("Able to sendSync when not legal");
            }
            catch (Exception ex) {};

            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());           
        }
    }
        class HappySynchEngineCheck extends Engine
    {
        HappySynchEngineCheck(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertTrue(inOnly.getProperty(SENDSYNC_PROPERTY_NAME) != null);
            assertTrue(inOnly.getPropertyNames().contains(SENDSYNC_PROPERTY_NAME));
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);

            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());

            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }

            inOnly.setStatus(ExchangeStatus.DONE);
            try
            {
                mChannel.sendSync(inOnly);
                setFailure("Able to sendSync when not legal");
            }
            catch (Exception ex) {};

            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());
        }
    } 
        
    class HappyTimeoutEngine extends Engine
    {
        HappyTimeoutEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);

            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            // Delay to help force timeouts.
            synchronized (this)
            {
                this.wait(1000);
            }
            
            inOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());

            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());           
        }
    }

    class XactEngine extends Engine
    {
        XactEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly              inOnly;
            Transaction         xact;
            NormalizedMessage   inMsg;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertTrue(inOnly.getEndpoint() != null);

            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());


            // verify out message is present
            if ((inMsg = inOnly.getInMessage()) == null)
            {
                setFailure("In message is null!");
                return;
            }

            xact = (Transaction)
                inOnly.getProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME);
            assertNotSame("XACT shouldn't be null", null,  xact);
            xact.enlistResource(mXAR2);
            mXAR2.setValue((Integer)inMsg.getProperty("Value"));
            xact.delistResource(mXAR2, XAResource.TMSUCCESS);
            inOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOnly);

            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());
                        
        }
    }

    class XactTransactionalEngine extends Engine
    {
        XactTransactionalEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly              inOnly;
            Transaction         xact;
            NormalizedMessage   inMsg;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertTrue(inOnly.getEndpoint() != null);

            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());

            // verify out message is present
            if ((inMsg = inOnly.getInMessage()) == null)
            {
                setFailure("In message is null!");
                return;
            }

            xact = (Transaction)inOnly.getProperty(
                MessageExchange.JTA_TRANSACTION_PROPERTY_NAME);
            assertNotSame("XACT should be null", null,  xact);
            inOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOnly);

            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());
                        
        }
    }

    class BadTimeoutEngine extends Engine
    {
        BadTimeoutEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);

            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            // Delay to help force timeouts.
            synchronized (this)
            {
                this.wait(1000);
            }
            
            // Should be able to set the status (other side shouldn't affect us.
            inOnly.setStatus(ExchangeStatus.DONE);
            
            try
            {
                mChannel.send(inOnly);
                setFailure("Can send after timeout.");
            }
            catch (javax.jbi.messaging.MessagingException e)
            {
            }
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            
            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());           
        }
    }
    class BadEngine extends Engine
    {
        BadEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly              inOnly;
            Fault               fault;
            NormalizedMessage   msg;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);
            
            // Try to create a fault
            try
            {
                fault  = inOnly.createFault();
                setFailure("Able to create fault on InOnly exchange");
                return;
            }
            catch (Exception ex1) {};
            
            // Try to set a fault
            try
            {
                inOnly.setFault(null);
                setFailure("Able to set a fault on InOnly exchange");
                return;
            }
            catch (Exception ex1) {};

            // Try to set in message from engine
            try
            {
                msg = inOnly.createMessage();
                inOnly.setInMessage(msg);
                setFailure("Able to set in message from engine");
                return;
            }
            catch (Exception ex1) {};         
            
            // complete the exchange normally
            inOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());

            // Try to set in after DONE.
            try
            {
                msg = inOnly.createMessage();
                inOnly.setInMessage(msg);
                setFailure("Able to set in message from engine");
                return;
            }
            catch (Exception ex1) {};         
        }
    }

    class CloseEngine extends Engine
    {
        CloseEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);
            
            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            getSequencer().stallUntil(1);

            // Should be able to set the status (other side shouldn't affect us.
            inOnly.setStatus(ExchangeStatus.DONE);
            
            try
            {
                mChannel.send(inOnly);
                setFailure("Send didn't get error for closed channel");
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
            }
        }
    }
    
    class Close2Engine extends Engine
    {
        Close2Engine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertTrue(inOnly.getEndpoint() != null);
            
            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            mChannel.close();
            
            getSequencer().reached(1);
        }
    }
    
    class EPREngine extends Engine
    {
        EPREngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOnly inOnly;

            inOnly = (InOnly)mChannel.accept();
            assertEquals(inOnly.getPattern().toString(), ExchangePattern.IN_ONLY.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());
            assertSame("EPR endpoint didn't resolve to SE", inOnly.getEndpoint(),getEndpoint());
            
            // Check that settings are ignored after message is ACTIVE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOnly.getOperation());
            
            // verify out message is present
            if (inOnly.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            inOnly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOnly);
            assertEquals(MessageExchange.Role.PROVIDER, inOnly.getRole());

            // Check that settings are ignored after message is DONE.

            inOnly.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOnly.getEndpoint());

            inOnly.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOnly.getOperation());           
        }
    }

}
