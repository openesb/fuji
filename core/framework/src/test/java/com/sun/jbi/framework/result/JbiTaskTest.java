/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiTaskTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.sun.jbi.framework.XmlTests;

/**
 * Unit tests for JbiTask class.
 * @author kcbabo
 */
public class JbiTaskTest extends XmlTests {
    
    private static final String FRMWK_RESULT_MSG = 
            "Something happened in the framework.";
    private static final String COMP_RESULT_MSG = 
            "Something happened in a component.";
    private static final String COMPONENT_NAME =
            "sun-service-engine";
    private static final String VERSION = "1.0";
    
    private static final String COMP_RESULT_ELEMENT = 
            "<component-task-result xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<component-name>" + COMPONENT_NAME + "</component-name> " +
                "<component-task-result-details>" +
                "<task-result-details>" +
                "<task-id>deploy</task-id>" +
                "<task-result>FAILED</task-result>" +
                "<task-status-msg>" +
                "<msg-loc-info>" +
                "<loc-token>SEMSG001</loc-token>" +
                "<loc-message>" + COMP_RESULT_MSG +"</loc-message>" +
                "</msg-loc-info>" +
                "</task-status-msg>" +
                "</task-result-details>" +
                "</component-task-result-details>" +
             "</component-task-result>";
    private static final String TEST_ELEMENT = 
            "<jbi-task xmlns=\"" + ResultElement.NS_URI + "\" version=\"" + VERSION + "\">" + 
                "<jbi-task-result></jbi-task-result> " +
             "</jbi-task>";
    
    public JbiTaskTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }            

    /**
     * Test of newJbiTask method, of class JbiTask.
     */
    public void testNewJbiTaskFromStream() throws Exception {
        JbiTask jt = JbiTask.newJbiTask(
                new ByteArrayInputStream(TEST_ELEMENT.getBytes()));
        assertNotNull(jt.getVersion());
    }
    
    /**
     * Test of newJbiTask method, of class JbiTask.
     */
    public void testNewJbiTaskFromScratch() throws Exception {
        JbiTask jt = JbiTask.newJbiTask();
        assertNotNull(jt.getVersion());
    }

    /**
     * Test of getVersion method, of class JbiTask.
     */
    public void testGetVersion() throws Exception {
        JbiTask jt = new JbiTask(stringToElement(TEST_ELEMENT));
        assertEquals(VERSION, jt.getVersion());
    }

    /**
     * Test of getJbiTaskResult method, of class JbiTask.
     */
    public void testGetJbiTaskResult() throws Exception {
        JbiTask jt = new JbiTask(stringToElement(TEST_ELEMENT));
        assertNotNull(jt.getJbiTaskResult());
    }
    
    /**
     * Test of createTaskResult method, of class JbiTask.
     */
    public void testCreateTaskResultComponentFailed() throws Exception {
        JbiTask jt = JbiTask.createTaskResult(FRMWK_RESULT_MSG, COMP_RESULT_ELEMENT);
        FrameworkTaskResult ftr = jt.getJbiTaskResult().getFrameworkTaskResult();
        assertEquals(FrameworkTaskResult.IsCauseFramework.NO,
                ftr.getIsCauseFramework());
        List<LocalizedMessage> msgs = ftr.getFrameworkTaskResultDetails().
                getTaskResultDetails().getTaskStatusMsgs();
        assertTrue(msgs.size() == 1);
        assertEquals(FRMWK_RESULT_MSG, msgs.get(0).getMessage());
        
        List<ComponentTaskResult> compResults = 
                jt.getJbiTaskResult().getComponentTaskResults();
        assertTrue(compResults.size() == 1);
        List<LocalizedMessage> compMsgs = 
                compResults.get(0).getTaskResultDetails().getTaskStatusMsgs();
        assertTrue(compMsgs.size() == 1);
        assertEquals(COMP_RESULT_MSG, compMsgs.get(0).getMessage());
        
        validate(jt.getElement());
    }

    /**
     * Test of createTaskResult method, of class JbiTask.
     */
    public void testCreateTaskResultSuccess() throws Exception {
        JbiTask jt = JbiTask.createTaskResult(FRMWK_RESULT_MSG, true);
        FrameworkTaskResult ftr = jt.getJbiTaskResult().getFrameworkTaskResult();
        assertNull(ftr.getIsCauseFramework());
        List<LocalizedMessage> msgs = ftr.getFrameworkTaskResultDetails().
                getTaskResultDetails().getTaskStatusMsgs();
        assertTrue(msgs.size() == 1);
        assertEquals(FRMWK_RESULT_MSG, msgs.get(0).getMessage());
        
        validate(jt.getElement());
    }
    
    /**
     * Test of createTaskResult method, of class JbiTask.
     */
    public void testCreateTaskResultFailed() throws Exception {
        JbiTask jt = JbiTask.createTaskResult(FRMWK_RESULT_MSG, false);
        FrameworkTaskResult ftr = jt.getJbiTaskResult().getFrameworkTaskResult();
        assertEquals(FrameworkTaskResult.IsCauseFramework.YES,
                ftr.getIsCauseFramework());
        List<LocalizedMessage> msgs = ftr.getFrameworkTaskResultDetails().
                getTaskResultDetails().getTaskStatusMsgs();
        assertTrue(msgs.size() == 1);
        assertEquals(FRMWK_RESULT_MSG, msgs.get(0).getMessage());
        
        validate(jt.getElement());
    }
    
    /**
     * This method verifies that a management result message created with 
     * the result api is actually schema valid.
     * @throws java.lang.Exception
     */
    public void testSchemaCompliance() throws Exception {
        // Build a full message and then schema validate
        JbiTask jt = JbiTask.newJbiTask();
        FrameworkTaskResult ftr = jt.getJbiTaskResult().getFrameworkTaskResult();
        ftr.setIsCauseFramework(FrameworkTaskResult.IsCauseFramework.NO);
        FrameworkTaskResultDetails ftrd = ftr.getFrameworkTaskResultDetails();
        ftrd.setLocale("es");
        TaskResultDetails trd = ftrd.getTaskResultDetails();
        trd.setTaskId("taskId");
        trd.setMessageType(TaskResultDetails.MessageType.ERROR);
        trd.setTaskResult(TaskResultDetails.TaskResult.FAILED);
        LocalizedMessage tsm = trd.addTaskStatusMsg();
        tsm.setMessage("status-msg");
        tsm.setToken("status-token");
        tsm.addParameter("foo");
        tsm.addParameter("bar");
        ExceptionInfo ei = trd.addExceptionInfo();
        ei.setNestingLevel("1");
        ei.setStackTrace("stack trace");
        ei.getLocalizedMessage().setToken("error-token");
        ei.getLocalizedMessage().setMessage("error-message");
        ComponentTaskResult ctr = jt.getJbiTaskResult().addComponentTaskResult();
        ctr.setComponentName("sun-binding");
        TaskResultDetails ctrd = ctr.getTaskResultDetails();
        ctrd.setMessageType(TaskResultDetails.MessageType.ERROR);
        ctrd.setTaskId("erorrOperation");
        ctrd.setTaskResult(TaskResultDetails.TaskResult.FAILED);
        
        validate(jt.getElement());
        
    }
}
