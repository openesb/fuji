/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.descriptor;

import junit.framework.TestCase;

/**
 *
 * @author kcbabo
 */
public class ServiceUnitTest extends TestCase {
    
    public ServiceUnitTest(String testName) {
        super(testName);
    }

    /**
     * Test of getArtifactsZip method, of class ServiceUnit.
     */
    public void testGetArtifactsZip() {
    }

    /**
     * Test of getComponentName method, of class ServiceUnit.
     */
    public void testGetComponentName() {
    }

    /**
     * Test of getIdentification method, of class ServiceUnit.
     */
    public void testGetIdentification() {
    }

}
