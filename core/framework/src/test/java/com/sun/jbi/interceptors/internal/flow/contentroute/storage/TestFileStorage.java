/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestFileStorage.java
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal.flow.contentroute.storage;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import com.sun.jbi.interceptors.internal.flow.contentroute.Storage;
import com.sun.jbi.interceptors.internal.flow.contentroute.storage.FileStorage;

/**
 *
 * @author
 */
public class TestFileStorage extends junit.framework.TestCase
{
    File    tempDirectory;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestFileStorage(String aTestName)
    {
        super(aTestName);
    }


    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        tempDirectory = File.createTempFile("temp", Long.toString(System.nanoTime()));
        if(!(tempDirectory.delete()))
        {
            throw new java.io.IOException("Could not delete temp file: " +
                    tempDirectory.getAbsolutePath());
        }
        if(!(tempDirectory.mkdir()))
        {
            throw new java.io.IOException("Could not create temp directory: " +
                    tempDirectory.getAbsolutePath());
        }
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();

        for (File f : tempDirectory.listFiles()) {
            f.delete();
        }
        tempDirectory.delete();
    }

// =============================  test methods ================================

    /**
     *  Test single operation case for service
     */
    public void testBasics()
        throws Exception
    {
        Storage     store = new FileStorage(tempDirectory);
        String[]    contents;
        Map         map = new HashMap();
        List        list = new ArrayList();

        //
        //  Check that we start with an empty store.
        //
        contents = store.listNames();
        assertEquals(contents.length, 0);

        //
        //  Create a Yaml document
        //
        list.add("Hello");
        list.add(1);
        map.put("name", "value");
        list.add(map);

        //
        //  Add an item and check its the only thing in the store and has some content.
        //
        store.addName("abc",list);
        contents = store.listNames();
        assertEquals(contents.length, 1);
        assertEquals(contents[0],"abc");
        Object content  = store.getName("abc");
        list = (List)content;
        assertEquals(((String)list.get(0)), "Hello");
        assertEquals(((Integer)list.get(1)).intValue(), 1);
        map = (Map)list.get(2);
        assertEquals(map.get("name"), "value");
        store.addName("abc",list);

        //
        //  Remove an item and check that its empty again.
        //
        store.removeName("abc");
        contents = store.listNames();
        assertEquals(contents.length, 0);
        try {
            store.getName("abc");
            fail("Expected exception for non-existant file during get");
        } catch (java.io.IOException ioEx) {

        }

        //
        // Check for exception on remove of non-existant file.
        //
        try {
            store.removeName("abc");
            fail("Expected exception for non-existant file during delete");
        } catch (java.io.IOException ioEx) {

        }

    }


}
