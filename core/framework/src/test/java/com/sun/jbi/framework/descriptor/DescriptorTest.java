/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DescriptorTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.descriptor;

import junit.framework.TestCase;

/**
 *
 * @author kcbabo
 */
public class DescriptorTest extends TestCase {
    
    public DescriptorTest(String testName) {
        super(testName);
    }

    /**
     * Test of getChildElement method, of class Descriptor.
     */
    public void testGetChildElement() {
    }

    /**
     * Test of getChildElements method, of class Descriptor.
     */
    public void testGetChildElements() {
    }

    /**
     * Test of getExtensionData method, of class Descriptor.
     */
    public void testGetExtensionData() {
    }

    /**
     * Test of writeTo method, of class Descriptor.
     */
    public void testWriteTo() throws Exception {
    }

    /**
     * Test of asString method, of class Descriptor.
     */
    public void testAsString() throws Exception {
    }

}
