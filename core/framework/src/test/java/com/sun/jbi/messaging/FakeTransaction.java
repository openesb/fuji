/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FakeTransaction.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.util.IdentityHashMap;
import java.util.Iterator;

import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

/** Simple utility class that implements a XAResource for testing.
 *  binding tests in independent threads.
 * @author Sun Microsystems, Inc.
 */
public class FakeTransaction
    implements javax.transaction.Transaction,
                javax.transaction.xa.Xid
{
    private     byte[]              mXid;
    private     int                 mStatus;
    private     TransactionManager  mTM;
    private     IdentityHashMap     mXARs;
    
    FakeTransaction(TransactionManager tm, byte[] id)
    {
        mTM = tm;
        mXid = id;
        mStatus = Status.STATUS_ACTIVE;
        mXARs = new IdentityHashMap();
    }
    
    public void commit() 
        throws javax.transaction.RollbackException, 
                javax.transaction.HeuristicMixedException, 
                javax.transaction.HeuristicRollbackException, 
                java.lang.SecurityException, 
                javax.transaction.SystemException 
    {
        boolean     mustAbort = false;
        
        if ( mStatus == Status.STATUS_NO_TRANSACTION)
        {
            throw new java.lang.IllegalStateException();
        }
        if (mStatus == Status.STATUS_ROLLEDBACK)
        {
            throw new javax.transaction.RollbackException();
        }
        mStatus = Status.STATUS_PREPARING;
        Iterator    i = mXARs.keySet().iterator();
        while (i.hasNext())
        {
           XAResource       xar;
           
           xar = (XAResource)i.next();
           try
           {
                xar.prepare(this);
           }
           catch (javax.transaction.xa.XAException xae)
           {
               mustAbort = true;
           }
        }
        mStatus = Status.STATUS_PREPARED;        
        if (mustAbort)
        {
            mStatus = Status.STATUS_ROLLING_BACK;
        }
        else
        {
            mStatus = Status.STATUS_COMMITTING;        
        }
        i = mXARs.keySet().iterator();
        while (i.hasNext())
        {
           XAResource       xar;
           
           xar = (XAResource)i.next();
           try
           {
                if (mustAbort)
                {
                    xar.rollback(this);
                }
                else
                {
                    xar.commit(this, false);
                }
           }
           catch (javax.transaction.xa.XAException xae)
           {
           }
        }
        if (mustAbort)
        {
            mStatus = Status.STATUS_ROLLEDBACK;
        }
        else
        {
            mStatus = Status.STATUS_COMMITTED;        
        }
        mStatus = Status.STATUS_NO_TRANSACTION;
     }
    
    public boolean delistResource(javax.transaction.xa.XAResource xAResource, int param) 
        throws java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        if (mXARs.get(xAResource) != this ||
            mStatus == Status.STATUS_PREPARED ||
            mStatus == Status.STATUS_NO_TRANSACTION)
        {
            throw new java.lang.IllegalStateException();
        }
        return (true);
    }
    
    public boolean enlistResource(javax.transaction.xa.XAResource xAResource) 
        throws javax.transaction.RollbackException, 
                java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        if (mStatus == Status.STATUS_PREPARED ||
            mStatus == Status.STATUS_NO_TRANSACTION)
        {
            throw new java.lang.IllegalStateException();
        }
        if (mStatus == Status.STATUS_MARKED_ROLLBACK)
        {
            throw new javax.transaction.RollbackException();
        }
        try
        {
            if (mXARs.get(xAResource) == null)
            {
                mXARs.put(xAResource, this);
                xAResource.start(this, XAResource.TMJOIN);
            }
            else
            {
                 xAResource.start(this, XAResource.TMRESUME);           
            }
        }
        catch (javax.transaction.xa.XAException xae)
        {
            throw new javax.transaction.SystemException();
        }
        return (true);
    }
    
    public int getStatus() 
        throws javax.transaction.SystemException 
    {
        return (mStatus);
    }
    
    public void registerSynchronization(javax.transaction.Synchronization synchronization) 
        throws javax.transaction.RollbackException, 
                java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
    }
    
    public void rollback() 
        throws java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        if ( mStatus == Status.STATUS_NO_TRANSACTION)
        {
            throw new java.lang.IllegalStateException();
        }
        if (mStatus == Status.STATUS_ROLLEDBACK)
        {
            throw new java.lang.IllegalStateException();
        }
        mStatus = Status.STATUS_ROLLING_BACK;
        Iterator    i = mXARs.keySet().iterator();
        while (i.hasNext())
        {
           XAResource       xar;
           
           xar = (XAResource)i.next();
           try
           {
                xar.rollback(this);
           }
           catch (javax.transaction.xa.XAException xae)
           {
           }
        }
        mStatus = Status.STATUS_ROLLEDBACK;
        mStatus = Status.STATUS_NO_TRANSACTION;
    }
    
    public void setRollbackOnly() 
        throws java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        if (mStatus == Status.STATUS_NO_TRANSACTION)
        {
            throw new java.lang.IllegalStateException();
        }
        mStatus = Status.STATUS_MARKED_ROLLBACK;
    }
    
    public byte[] getBranchQualifier() 
    {
        return (null);
    }
    
    public int getFormatId() 
    {
        return (0);
    }
    
    public byte[] getGlobalTransactionId() 
    {
        return (mXid);
    }
}
