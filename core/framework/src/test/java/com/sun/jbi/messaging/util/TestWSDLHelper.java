/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWSDLHelper.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging.util;

import com.sun.jbi.messaging.WsdlDocument;
import com.sun.jbi.messaging.util.XMLUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import javax.xml.namespace.QName;
import com.sun.jbi.messaging.ExchangePattern;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * Tests for the TestWSDLHelper class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestWSDLHelper extends junit.framework.TestCase implements EntityResolver
{
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestWSDLHelper(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
    }

// =============================  test methods ================================

    /**
     *  Test single operation case for service
     */
    public void testGetOperationForServiceSingle()
        throws Exception
    {
        Document desc;
        List<QName>                     ifs = new LinkedList();
        Map<QName,Map<String,QName>>    ops = new HashMap();
        
        desc = WsdlDocument.readDefaultDocument();
        WSDLHelper.getOperationsAndInterfaces(desc, WsdlDocument.STOCK_SERVICE_Q, null, ops, ifs);
        
        // only one operation for this service
        assertTrue(ops.size() == 1);
        // make sure the operation name is correct
        assertTrue(ops.containsKey(WsdlDocument.STOCK_OPERATION_Q));
    }
    
    /**
     *  Test multiple operation case for service
     */
    public void testGetOperationsForServiceMultiple()
        throws Exception
    {
        Document desc;
        List<QName>                     ifs = new LinkedList();
        Map<QName,Map<String,QName>>    ops = new HashMap();

        desc = WsdlDocument.readDefaultDocument();
        WSDLHelper.getOperationsAndInterfaces(desc, WsdlDocument.HELLO_SERVICE_Q, null, ops, ifs);
        
        // three operation for this service
        assertTrue(ops.size() == 3);
        // make sure the operation name is correct
        assertTrue(ops.containsKey(WsdlDocument.HELLO_C_OPERATION_1_Q));
        assertTrue(ops.containsKey(WsdlDocument.HELLO_W_OPERATION_Q));
    }
    
    /**
     *  Test single operation case for service
     */
    public void testGetInterfacesForServiceSingle()
        throws Exception
    {
        Document desc;
        List<QName>                     ifs = new LinkedList();
        Map<QName,Map<String,QName>>    ops = new HashMap();

        desc = WsdlDocument.readDefaultDocument();
        WSDLHelper.getOperationsAndInterfaces(desc, WsdlDocument.STOCK_SERVICE_Q, null, ops, ifs);


        assertTrue(ifs.size() == 1);
        assertTrue(ifs.get(0).equals(WsdlDocument.STOCK_INTERFACE_Q));
    }
    
    /**
     *  Test multiple operation case for service
     */
    public void testGetInterfacesForServiceMultiple()
        throws Exception
    {
        Document desc;
        List<QName>                     ifs = new LinkedList();
        Map<QName,Map<String,QName>>    ops = new HashMap();

        desc = WsdlDocument.readDefaultDocument();
        WSDLHelper.getOperationsAndInterfaces(desc, WsdlDocument.HELLO_SERVICE_Q, null, ops, ifs);


        assertTrue(ifs.size() == 2);
    }

    public void testCreateBasicWSDL()
            throws Exception
    {
        String NS = "http://fuji.dev.java.net/application/app";
        Map<QName,List<QName>>  interfaces = new HashMap();
        Map<QName,String>       operations = new HashMap();
        List<QName>             opnames = new LinkedList();
        Document                doc;
        QName[]                 ifaces;
        List<QName>                     ifs = new LinkedList();
        Map<QName,Map<String,QName>>    ops = new HashMap();

        opnames.add(new QName(NS, "oneWay"));
        opnames.add(new QName(NS, "requestReply"));
        operations.put(new QName(NS, "oneWay"), ExchangePattern.IN_ONLY.toString());
        operations.put(new QName(NS, "requestReply"), ExchangePattern.IN_OUT.toString());
        interfaces.put(new QName(NS, "interface_1"), opnames);

        doc = WSDLHelper.buildBasicWSDL(WSDLHelper.WSDL_11, NS,
            "myService", "myEndpoint", interfaces, operations);
        WSDLHelper.getOperationsAndInterfaces(doc, new QName(NS, "myService"), null, ops, ifs);
        assertEquals(interfaces.size(), ifs.size());
        for (QName i : ifs)
        {
            assertTrue(interfaces.get(i) != null);
        }
        assertEquals(operations.size(), ops.size());
        for (Object i : ops.keySet())
        {
            QName qn = (QName)i;
            assertTrue(operations.get(qn) != null);
        }
    }

    public void testCreate2PartWSDL()
            throws Exception
    {
        String NS = "http://fuji.dev.java.net/application/app";
        Map<QName,List<QName>>  interfaces = new HashMap();
        Map<QName,String>       operations = new HashMap();
        List<QName>             opnames = new LinkedList();
        Document                doc;
        QName[]                 ifaces;
        HashMap                 ops = new HashMap();
        List<QName>             ifs = new LinkedList();

        opnames.add(new QName(NS, "oneWay"));
        opnames.add(new QName(NS, "requestReply"));
        operations.put(new QName(NS, "oneWay"), ExchangePattern.IN_ONLY.toString());
        operations.put(new QName(NS, "requestReply"), ExchangePattern.IN_OUT.toString());
        interfaces.put(new QName(NS, "interface_1"), opnames);

        doc = WSDLHelper.buildBindingWSDL(WSDLHelper.WSDL_11, NS,
            "myService", "myEndpoint", interfaces, operations);
        setResolver(WSDLHelper.buildInterfaceWSDL(WSDLHelper.WSDL_11, NS,
            "myService", "myEndpoint", interfaces, operations));
        WSDLHelper.getOperationsAndInterfaces(doc, new QName(NS, "myService"), this, ops, ifs);
        assertEquals(interfaces.size(), ifs.size());
        assertEquals(operations.size(), ops.size());
        for (Object i : ops.keySet())
        {
            QName qn = (QName)i;
            assertTrue(operations.get(qn) != null);
        }
    }
    
   private String entity_;

   public void setResolver(Document entity) {
       try
       {
           TransformerFactory tf = TransformerFactory.newInstance();
           Transformer transformer = tf.newTransformer();
           StringWriter sw = new StringWriter();
           transformer.transform(new DOMSource(entity.getDocumentElement()), new StreamResult(sw));
           entity_ = sw.toString();
       } catch (Exception ignore) {
            entity = null;
       }
    }

    // ------------------- org.xml.sax.EntityResolver -------------------------

    public InputSource resolveEntity (String publicId, String systemId)
    {
        if (systemId.equals("interface.wsdl")) {
            return (new InputSource(new ByteArrayInputStream(entity_.getBytes())));
        }
        return (null);
    }

}
