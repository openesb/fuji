/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TesterBundle.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.test.osgi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;

/**
 * Dummy bundle implementation for testing.
 * @author kcbabo
 */
public class TesterBundle implements org.osgi.framework.Bundle {
    
    private static long bundleIdx_ = 0;
    
    private long bundleId_;
    private Hashtable headers_;
    private int state_;
    private File bundleDir_;
    private long bornOnDate_;
    private String symbolicName_;
    
    public TesterBundle() {
        bornOnDate_ = System.currentTimeMillis();
        state_ = Bundle.INSTALLED;
        bundleId_ = ++bundleIdx_;
        headers_ = new Hashtable();
    }
    
    public TesterBundle(long bundleId, String symbolicName, Hashtable headers, 
            File bundleDir) {
        this();
        bundleId_ = bundleId;
        headers_ = headers; 
        bundleDir_ = bundleDir;
        symbolicName_ = symbolicName;
    }

    /** 
     * Stupid implementation of findEntries.  If the file name matches the 
     * pattern exactly, we return it.
     */
    public Enumeration findEntries(String path, String filePattern, boolean recurse) {
        Enumeration entries = null;
        
        File filePath = new File(path);
        
        try {
            for (File file : filePath.listFiles()) {
                if (file.getName().equals(filePattern)) {
                    Vector v = new Vector();
                    v.add(file.toURL());
                    entries = v.elements();
                }
            }
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        
        return entries;
    }

    public long getBundleId() {
        return bundleId_;
    }

    public URL getEntry(String arg0) {
       try {
           return new File(bundleDir_, arg0).toURL();
       }
       catch (Exception ex) {
           throw new RuntimeException(ex);
       }
    }

    public BundleContext getBundleContext(){
        return null;
    }
    
    public Enumeration getEntryPaths(String arg0) {
        String[] paths = new File(bundleDir_, arg0).list();
        
        // Make sure all directory paths have a '/' at the end
        for (String path : paths) {
            if (new File(path).isDirectory() && !path.endsWith(File.separator)) {
                path = path + '/';
            }
        }
        
        return Collections.enumeration(Arrays.asList(paths));
    }

    public Dictionary getHeaders() {
        return headers_;
    }

    public Dictionary getHeaders(String arg0) {
        return headers_;
    }

    public long getLastModified() {
        return bornOnDate_;
    }

    public String getLocation() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ServiceReference[] getRegisteredServices() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public URL getResource(String name) {
        return getClass().getClassLoader().getResource(name);
    }

    public Enumeration getResources(String name) throws IOException {
        return getClass().getClassLoader().getResources(name);
    }

    public ServiceReference[] getServicesInUse() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getState() {
        return state_;
    }

    public String getSymbolicName() {
        return symbolicName_;
    }

    public boolean hasPermission(Object arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Class loadClass(String name) throws ClassNotFoundException {
        return getClass().getClassLoader().loadClass(name);
    }

    public void start() throws BundleException {
        state_ = Bundle.ACTIVE;
    }

    public void stop() throws BundleException {
        state_ = Bundle.RESOLVED;
    }

    public void start(int options) throws BundleException {
        this.start();
    }

    public void stop(int options) throws BundleException {
        this.stop();
    }
    
    public void uninstall() throws BundleException {
        state_ = Bundle.UNINSTALLED;       
    }

    public void update() throws BundleException {
        
    }

    public void update(InputStream arg0) throws BundleException {
        
    }
    
    public void setBundleDir(File bundleDir) {
        bundleDir_ = bundleDir;
    }
    
    public File getBundleDir() {
        return bundleDir_;
    }

    public void addHeader(String name, String value) {
        headers_.put(name, value);
    }
    
    public void removeHeader(String name) {
        headers_.remove(name);
    }
}
