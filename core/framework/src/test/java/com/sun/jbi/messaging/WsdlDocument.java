/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlDocument.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/** This class generates the default WSDL definition used by NMR tests.  
 *  Constants are supplied to protect individual tests from name changes
 *  in the underlying WSDL document.
 * @author Sun Microsystems, Inc.
 */
public class WsdlDocument
{
    /** Path to default WSDL definition. */
    public static final String WSDL_20_DEFINITION = "regress20.wsdl";
    
    public static final String WSDL_11_DEFINITION = "regress11.wsdl";
    
    public static final String NS_URI = "http://example.com/wsdl";
    /** NCNames */
    public static final String HELLO_C_INTERFACE    = "hellocity";
    public static final String HELLO_W_INTERFACE    = "helloworld";
    public static final String HELLO_C_OPERATION_1  = "helloPhoenix";
    public static final String HELLO_C_OPERATION_2  = "helloTempe";
    public static final String HELLO_W_OPERATION    = "helloEarth";
    public static final String HELLO_BINDING        = "hellobinding";
    public static final String HELLO_SERVICE        = "helloservice";
    public static final String HELLO_ENDPOINT_1     = "helloendpoint1";
    public static final String HELLO_ENDPOINT_2     = "helloendpoint2";
    
    public static final String STOCK_INTERFACE      = "StockQuote";
    public static final String STOCK_OPERATION      = "GetLastTradePrice";
    public static final String STOCK_BINDING        = "StockQuoteBinding";
    public static final String STOCK_SERVICE        = "StockQuoteService";
    public static final String STOCK_ENDPOINT       = "StockQuotePort";
    
    /** QNames */
    public static final QName HELLO_C_INTERFACE_Q 
        = new QName(NS_URI, HELLO_C_INTERFACE);
    public static final QName HELLO_W_INTERFACE_Q 
        = new QName(NS_URI, HELLO_W_INTERFACE);
    public static final QName STOCK_INTERFACE_Q 
        = new QName(NS_URI, STOCK_INTERFACE);
    public static final QName HELLO_C_OPERATION_1_Q 
        = new QName(NS_URI, HELLO_C_OPERATION_1);
    public static final QName HELLO_C_OPERATION_2_Q 
        = new QName(NS_URI, HELLO_C_OPERATION_2);
    public static final QName HELLO_W_OPERATION_Q 
        = new QName(NS_URI, HELLO_W_OPERATION);
    public static final QName STOCK_OPERATION_Q 
        = new QName(NS_URI, STOCK_OPERATION);
    public static final QName HELLO_SERVICE_Q 
        = new QName(NS_URI, HELLO_SERVICE);
    public static final QName STOCK_SERVICE_Q 
        = new QName(NS_URI, STOCK_SERVICE);
    
    public static Document readDocument(String path)
        throws Exception
    {
        DocumentBuilderFactory  dbf;
        DocumentBuilder         db;
        Document                d;
        
        dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        db  = dbf.newDocumentBuilder();
        d   = db.parse(WsdlDocument.class.getClassLoader().getResourceAsStream(path));
        
        return d;
    }
    
    public static Document readDefaultDocument()
        throws Exception
    {
        return readDocument(WSDL_20_DEFINITION);
    }
}
