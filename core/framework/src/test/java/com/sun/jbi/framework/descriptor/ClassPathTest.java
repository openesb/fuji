/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ClassPathTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.descriptor;

import com.sun.jbi.test.osgi.XmlTests;

/**
 * Unit tests for ClassPath class.
 * @author kcbabo
 */
public class ClassPathTest extends XmlTests {
    
    private static final String ELEMENT_1 = 
            ".";
    private static final String ELEMENT_2 = 
            "foo.jar";
    private static final String[] ELEMENTS = 
            new String[] {ELEMENT_1, ELEMENT_2};
    
    private static final String NODE = 
            "<class-path xmlns=\"" + Descriptor.NS_URI + "\">" +
            "<path-element>" + ELEMENT_1 + "</path-element>" +
            "<path-element>" + ELEMENT_2 + "</path-element>" +
            "</class-path>";
    
    
    public ClassPathTest(String testName) {
        super(testName, JBI_XML_SCHEMA);
    }

    /**
     * Test of getPathElements method, of class ClassPath.
     */
    public void testGetPathElements() throws Exception {
        ClassPath cp = new ClassPath(stringToElement(NODE));
        String[] pe = cp.getPathElements();
        
        assertTrue(pe.length == 2);
        assertEquals(pe[0], ELEMENT_1);
        assertEquals(pe[1], ELEMENT_2);
    }

}
