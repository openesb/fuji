/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalizedMessageTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.XmlTests;


/**
 * Unit tests for LocalizedMessage class.
 * @author kcbabo
 */
public class LocalizedMessageTest extends XmlTests {
    
    private static final String LOC_TOKEN = "JBI1009";
    private static final String LOC_MESSAGE = "The winning lotto numbers are {0} {1} {2}";
    private static final String LOC_PARAM_1 = "10";
    private static final String LOC_PARAM_2 = "20";
    private static final String LOC_PARAM_3 = "30";
    
    private static final String TEST_ELEMENT = 
            "<msg-loc-info xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<loc-token>" + LOC_TOKEN + "</loc-token>" +
                "<loc-message>" + LOC_MESSAGE + "</loc-message>" +
                "<loc-param>" + LOC_PARAM_1 + "</loc-param>" +
                "<loc-param>" + LOC_PARAM_2 + "</loc-param>" +
                "<loc-param>" + LOC_PARAM_3 + "</loc-param>" +
             "</msg-loc-info>";
    
    public LocalizedMessageTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }            

    /**
     * Test of getToken method, of class LocalizedMessage.
     */
    public void testGetToken() throws Exception {
        LocalizedMessage lm = new LocalizedMessage(stringToElement(TEST_ELEMENT));
        assertEquals(LOC_TOKEN, lm.getToken());
    }

    /**
     * Test of getMessage method, of class LocalizedMessage.
     */
    public void testGetMessage() throws Exception {
        LocalizedMessage lm = new LocalizedMessage(stringToElement(TEST_ELEMENT));
        assertEquals(LOC_MESSAGE, lm.getMessage());
    }

    /**
     * Test of getParam method, of class LocalizedMessage.
     */
    public void testGetParam() throws Exception {
        LocalizedMessage lm = new LocalizedMessage(stringToElement(TEST_ELEMENT));
        assertTrue(lm.getParameters().size() == 3);
    }
    
    /**
     * Test of setMessage method, of class LocalizedMessage.
     */
    public void testSetMessage() throws Exception {
        LocalizedMessage lm = new LocalizedMessage(createElement(TaskElem.msg_loc_info));
        lm.setMessage(LOC_MESSAGE);
        assertEquals(LOC_MESSAGE, lm.getMessage());
    }
    
    /**
     * Test of setToken method, of class LocalizedMessage.
     */
    public void testSetToken() throws Exception {
        LocalizedMessage lm = new LocalizedMessage(createElement(TaskElem.msg_loc_info));
        lm.setToken(LOC_TOKEN);
        assertEquals(LOC_TOKEN, lm.getToken());
    }
    
    /**
     * Test of addParam method, of class LocalizedMessage.
     */
    public void testAddParam() throws Exception {
        LocalizedMessage lm = new LocalizedMessage(createElement(TaskElem.msg_loc_info));
        lm.addParameter(LOC_PARAM_1);
        lm.addParameter(LOC_PARAM_2);
        assertTrue(lm.getParameters().size() == 2);
        assertEquals(lm.getParameters().get(0), LOC_PARAM_1);
    }

}
