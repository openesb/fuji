/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentManagerTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import ComponentManagerTest.TestComponent;
import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.messaging.MessageService;
import com.sun.jbi.test.osgi.TesterBundle;
import com.sun.jbi.test.osgi.TesterBundleContext;
import java.io.File;
import junit.framework.TestCase;

/**
 * Unit tests for ComponentManager class.
 * @author kcbabo
 */
public class ComponentManagerTest extends TestCase {
    
    private static final String FRAMEWORK_DIR =
        "target/test-classes/ComponentManagerTest/framework";
    private static final String WORK_DIR = "target/test-classes/ComponentManagerTest/tmp";
    private static final String DESCRIPTOR_PATH = "ComponentManagerTest/META-INF/jbi.xml";
    
    private File testDir_;
    private File fwTestDir_;
    private TesterBundle fwTestBundle_;
    private TesterBundle testBundle_;
    private ComponentBundle compBundle_;
    private ComponentManager compManager_;
    private TesterBundleContext bundleContext_;
    
    public ComponentManagerTest(String testName) {
        super(testName);
        fwTestDir_ = new File(FRAMEWORK_DIR);
        testDir_ = new File(WORK_DIR);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        // clean up, just in case we had a bad exit last time
        if (testDir_.exists()) {
            FileUtil.cleanDirectory(testDir_);
            testDir_.delete();
        }
        if (fwTestDir_.exists()) {
            FileUtil.cleanDirectory(fwTestDir_);
            fwTestDir_.delete();
        }
        
        fwTestDir_.mkdirs();
        testDir_.mkdirs();
        
        compManager_ = new ComponentManager();
        testBundle_ = new TesterBundle();
        testBundle_.setBundleDir(testDir_);
        compBundle_ = new ComponentBundle(Jbi.newJbi(
                getClass().getClassLoader().getResourceAsStream(DESCRIPTOR_PATH)),
                testBundle_);
        
        fwTestBundle_ = new TesterBundle();
        fwTestBundle_.setBundleDir(fwTestDir_);
        
        bundleContext_ = new TesterBundleContext(fwTestBundle_);
        
        // initialize env
        Environment.setMessageService(new MessageService());
        Environment.setComponentManager(compManager_);
        Environment.setFrameworkBundleContext(bundleContext_);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of isRegistered method, of class ComponentManager.
     */
    public void testIsRegistered() throws Exception {
        assertFalse(compManager_.isRegistered(compBundle_.getBundle().getBundleId()));
        compManager_.registerComponent(compBundle_);
        assertTrue(compManager_.isRegistered(compBundle_.getBundle().getBundleId()));
    }

    /**
     * Test of getComponent method, of class ComponentManager.
     */
    public void testGetComponent() throws Exception {
        assertNull(compManager_.getComponent(compBundle_.getName()));
        compManager_.registerComponent(compBundle_);
        assertEquals(compBundle_, compManager_.getComponent(compBundle_.getName()));
    }

    /**
     * Test of getComponents method, of class ComponentManager.
     */
    public void testGetComponents() throws Exception {
        assertTrue(compManager_.getComponents().size() == 0);
        compManager_.registerComponent(compBundle_);
        assertTrue(compManager_.getComponents().size() == 1);
    }

    /**
     * Test of installComponent method, of class ComponentManager.
     */
    public void testInstallComponent() throws Exception {
        compManager_.installComponent(compBundle_.getBundle(), 
                compBundle_.getDescriptor());
        TestComponent tc = (TestComponent)compManager_.getComponent(
                compBundle_.getBundle().getBundleId()).getComponent();
        assertTrue(compManager_.isRegistered(compBundle_.getBundle().getBundleId()));
    }

    /**
     * Test of uninstallComponent method, of class ComponentManager.
     */
    public void testUninstallComponent() throws Exception {
        compManager_.installComponent(compBundle_.getBundle(), 
                compBundle_.getDescriptor());
        TestComponent tc = (TestComponent)compManager_.getComponent(
                compBundle_.getBundle().getBundleId()).getComponent();
        compManager_.uninstallComponent(compBundle_.getBundle().getBundleId());
        assertFalse(compManager_.isRegistered(compBundle_.getBundle().getBundleId()));
    }

    /**
     * Test of startComponent method, of class ComponentManager.
     */
    public void testStartComponent() throws Exception {
        compManager_.installComponent(compBundle_.getBundle(), 
                compBundle_.getDescriptor());
        TestComponent tc = (TestComponent)compManager_.getComponent(
                compBundle_.getBundle().getBundleId()).getComponent();
        compManager_.startComponent(compBundle_.getBundle().getBundleId());
        assertTrue(tc.lifeCycleInitCalled);
        assertTrue(tc.startCalled);
    }

    /**
     * Test of shutDownComponent method, of class ComponentManager.
     */
    public void testShutDownComponent() throws Exception {
        compManager_.installComponent(compBundle_.getBundle(), 
                compBundle_.getDescriptor());
        TestComponent tc = (TestComponent)compManager_.getComponent(
                compBundle_.getBundle().getBundleId()).getComponent();
        compManager_.shutDownComponent(compBundle_.getBundle().getBundleId());
        assertTrue(tc.stopCalled);
        assertTrue(tc.shutDownCalled);
    }

    /**
     * Test of registerComponent method, of class ComponentManager.
     */
    public void testRegisterComponent() throws Exception {
        compManager_.registerComponent(compBundle_);
        assertTrue(compManager_.isRegistered(compBundle_.getBundle().getBundleId()));
    }

    /**
     * Test of unregisterComponent method, of class ComponentManager.
     */
    public void testUnregisterComponent() throws Exception {
        compManager_.registerComponent(compBundle_);
        compManager_.unregisterComponent(compBundle_, false);
        assertFalse(compManager_.isRegistered(compBundle_.getBundle().getBundleId()));
    }
}
