/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallerTest.java - Last published on 4/21/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.osgi.internal.ComponentBundle;
import com.sun.jbi.framework.osgi.internal.ComponentManager;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.osgi.internal.FileUtil;
import com.sun.jbi.messaging.MessageService;
import com.sun.jbi.test.osgi.TestEnvironment;
import com.sun.jbi.test.osgi.TesterBundle;
import com.sun.jbi.test.osgi.TesterBundleContext;

import java.io.File;
import javax.management.ObjectName;

import junit.framework.TestCase;

/**
 * Unit tests for Installer class.
 * @author mwhite
 */
public class InstallerTest extends TestCase {
    
    private static final String COMP_WORK_DIR =
        "target/test-classes/InstallerTest/component";
    private static final String FRAMEWORK_DIR =
        "target/test-classes/InstallerTest/framework";
    private static final String COMP_DESCRIPTOR_PATH =
        "InstallerTest/META-INF/jbi.xml";
    
    private File compTestDir_;
    private File fwTestDir_;
    private TesterBundle compTestBundle_;
    private TesterBundle fwTestBundle_;
    private ComponentBundle compBundle_;
    private ComponentManager compManager_;
    private TesterBundleContext bundleContext_;
    private Installer installer_;
    
    public InstallerTest(String testName) {
        super(testName);
        compTestDir_ = new File(COMP_WORK_DIR);
        fwTestDir_ = new File(FRAMEWORK_DIR);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        // clean up, just in case we had a bad exit last time
        if (compTestDir_.exists()) {
            FileUtil.cleanDirectory(compTestDir_);
            compTestDir_.delete();
        }
        if (fwTestDir_.exists()) {
            FileUtil.cleanDirectory(fwTestDir_);
            fwTestDir_.delete();
        }
        
        compTestDir_.mkdirs();
        fwTestDir_.mkdirs();
        
        compManager_ = new ComponentManager();
        compTestBundle_ = new TesterBundle();
        fwTestBundle_ = new TesterBundle();
        compTestBundle_.setBundleDir(compTestDir_);
        fwTestBundle_.setBundleDir(fwTestDir_);
        compBundle_ = new ComponentBundle(Jbi.newJbi(
            getClass().getClassLoader().getResourceAsStream(COMP_DESCRIPTOR_PATH)),
            compTestBundle_);

        bundleContext_ = new TesterBundleContext(fwTestBundle_);

        installer_ = new Installer(compBundle_);
        
        // initialize env
        TestEnvironment.setComponentManager(compManager_);
        TestEnvironment.setFrameworkBundleContext(bundleContext_);
        TestEnvironment.setMessageService(new MessageService());

        // register test component
        compManager_.registerComponent(compBundle_);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getInstallerConfigurationMBean method, of class Installer.
     *
     * @throws Exception on error.
     */
    public void testGetInstallerConfigurationMBean() throws Exception {
        ObjectName on = installer_.getInstallerConfigurationMBean();
        assertNotNull(on);
        assertTrue(-1 < on.toString().indexOf("Extension"));
    }

    /**
     * Test of getInstallRoot method, of class Installer.
     *
     * @throws Exception on error.
     */
    public void testGetInstallRoot() throws Exception {
        // Returns non-null for existing component
        String root = installer_.getInstallRoot();
        assertNotNull(root);
        assertTrue(-1 < root.indexOf("test-binding"));
    }

    /**
     * Test of install method, of class Installer.
     *
     * @throws Exception on error.
     */
    public void testInstall() throws Exception {
        ObjectName on = installer_.install();
        assertNotNull(on);
        assertTrue(-1 < on.toString().indexOf("test-binding"));
    }

    /**
     * Test of isInstalled method, of class Installer.
     * 
     * @throws Exception on error.
     */
    public void testIsInstalled() throws Exception {
        // Test with component installed
        assertTrue(installer_.isInstalled());
        // Test with component uninstalled
        compBundle_.getBundle().uninstall();
        assertFalse(installer_.isInstalled());
    }

    /**
     * Test of uninstall method, of class Installer.
     *
     * @throws Exception on error.
     */
    public void testUninstall() throws Exception {
        installer_.uninstall();
        assertFalse(installer_.isInstalled());
    }
}
