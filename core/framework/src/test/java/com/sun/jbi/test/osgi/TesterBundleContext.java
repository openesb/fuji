/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TesterBundleContext.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.test.osgi;

import java.io.File;
import java.io.InputStream;
import java.util.Dictionary;
import java.util.ArrayList;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 *
 * @author kcbabo
 */
public class TesterBundleContext implements org.osgi.framework.BundleContext {
    
    private TesterBundle bundle_;
        
    public TesterBundleContext(TesterBundle bundle) {
        bundle_ = bundle;
    }

    public void addBundleListener(BundleListener arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addFrameworkListener(FrameworkListener arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addServiceListener(ServiceListener arg0, String arg1) throws InvalidSyntaxException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addServiceListener(ServiceListener arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Filter createFilter(String arg0) throws InvalidSyntaxException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ServiceReference[] getAllServiceReferences(String arg0, String arg1) throws InvalidSyntaxException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Bundle getBundle() {
        return bundle_;
    }

    public Bundle getBundle(long arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Bundle[] getBundles() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public File getDataFile(String arg0) {
        return new File(bundle_.getBundleDir(), arg0);
    }

    public String getProperty(String arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Object getService(ServiceReference arg0) {
        return null;
    }

    public ServiceReference getServiceReference(String arg0) {
        return new ServiceReference() {
            public int 	compareTo(java.lang.Object reference)  {
                return 1;
            }
            
            public Bundle getBundle() {
                return null;
            }
            
            public Object getProperty(java.lang.String key) {
                return null;
            }
            
            public java.lang.String[] getPropertyKeys() {
                return null;
            }

            public Bundle[] getUsingBundles() {
                return null;
            }

            public boolean isAssignableTo(Bundle bundle, java.lang.String className) {
                return true;
            }
        };
    }

    public ServiceReference[] getServiceReferences(String arg0, String arg1) throws InvalidSyntaxException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Bundle installBundle(String arg0) throws BundleException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Bundle installBundle(String arg0, InputStream arg1) throws BundleException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ServiceRegistration registerService(String[] arg0, Object arg1, Dictionary arg2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ServiceRegistration registerService(String arg0, Object arg1, Dictionary arg2) {
        return new ServiceRegistration() {
            
            public ServiceReference getReference() {
                return null;
            }
            public void setProperties(java.util.Dictionary properties) {
            }
            public void unregister() {
            }
        };
    }

    public void removeBundleListener(BundleListener arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeFrameworkListener(FrameworkListener arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeServiceListener(ServiceListener arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean ungetService(ServiceReference arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
