/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FakeXAResource.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.util.HashMap;

import javax.transaction.xa.XAResource;

/** Simple utility class that implements a XAResource for testing.
 *  binding tests in independent threads.
 * @author Sun Microsystems, Inc.
 */
public class FakeXAResource
    implements XAResource
{
    private HashMap mXIDs;
    private byte[]  mXid;
    private int     mValue;
    
    FakeXAResource()
    {
        mXIDs = new HashMap();
        mValue = 0;
    }
    
    public void setValue(Integer value)
    {
        if (mXid == null)
        {
            throw new java.lang.IllegalStateException();
        }
        if (mXIDs.get(mXid) == null)
        {
            mXIDs.put(mXid, new Integer(mValue));
        }
        mValue = value.intValue();
    }
    
    public int getValue()
    {
        return (mValue);
    }
    
    public void commit(javax.transaction.xa.Xid xid, boolean param) 
        throws javax.transaction.xa.XAException 
    {
        if (mXIDs.get(xid.getGlobalTransactionId()) != null)
        {
            mXIDs.remove(xid.getGlobalTransactionId());
        }
    }
    
    public void end(javax.transaction.xa.Xid xid, int param) 
        throws javax.transaction.xa.XAException 
    {
        if (xid.getGlobalTransactionId().equals(mXid))
        {
            mXid = null;
        }
        else
        {
            throw new javax.transaction.xa.XAException();
        }
    }
    
    public void forget(javax.transaction.xa.Xid xid) 
        throws javax.transaction.xa.XAException 
    {
        mXIDs.remove(xid.getGlobalTransactionId());
    }
    
    public int getTransactionTimeout() 
        throws javax.transaction.xa.XAException 
    {
        return (0);
    }
    
    public boolean isSameRM(javax.transaction.xa.XAResource xAResource) 
        throws javax.transaction.xa.XAException 
    {
        return (true);
    }
    
    public int prepare(javax.transaction.xa.Xid xid) 
        throws javax.transaction.xa.XAException 
    {
        if (mXIDs.get(xid.getGlobalTransactionId()) == null)
        {       
            return (XA_RDONLY);
        }
        return (XA_OK);
    }
    
    public javax.transaction.xa.Xid[] recover(int param) 
        throws javax.transaction.xa.XAException 
    {
        return (null);
    }
    
    public void rollback(javax.transaction.xa.Xid xid) 
        throws javax.transaction.xa.XAException 
    {
        Object      o;
        
        if ((o = mXIDs.get(xid.getGlobalTransactionId())) != null)
        {
            mValue = ((Integer)o).intValue();
            mXIDs.remove(xid.getGlobalTransactionId());
        }
    }
    
    public boolean setTransactionTimeout(int param) 
        throws javax.transaction.xa.XAException 
    {
        return (false);
    }
    
    public void start(javax.transaction.xa.Xid xid, int param) 
        throws javax.transaction.xa.XAException 
    {
         if (mXid == null)
        {
            mXid = xid.getGlobalTransactionId();
            if (mXIDs.get(mXid) == null)
            {
                mXIDs.put(mXid, null);
            }
        }
        else
        {
            throw new javax.transaction.xa.XAException();
        }
   }
}
