/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.io.StringReader;
import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/** Generic engine skeleton for use with NMS junit/regress tests.
 * @author Sun Microsystems, Inc.
 */
public abstract class Engine 
        implements Runnable, Component
{
    private static final String COMPONENT_NAME = "engine";
    
    private   Exception             mError;
    private   String                mFailTxt;
    protected boolean               mIsProvider;
    protected DeliveryChannelImpl   mChannel;
    protected ServiceEndpoint       mEndpoint;
    protected Document              mDocument;
    protected String                mPattern;
    protected String                mEndpointName;
    protected QName                 mServiceName;
    protected Sequencer             mSequencer;
    
    protected Engine(MessageService msgSvc)
    {
        try {
            mChannel = msgSvc.activateChannel(COMPONENT_NAME, this);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /** Used for provider cases. */
    public void init(QName service, String endpoint, String pattern)
        throws Exception
    {
        mPattern        = pattern;
        mEndpointName   = endpoint;
        mServiceName    = service;
        mEndpoint = mChannel.activateEndpoint(service, endpoint);
        mIsProvider = true;
    }    
    
    /** Used in consumer cases. */
    public void init(QName service)
        throws Exception
    {
        ServiceEndpoint[] refs;
        
        refs = mChannel.getEndpointsForService(service);
        if (refs.length == 0)
        {
            throw new Exception("No endpoints found for service: " + service.getLocalPart());
        }
        
        mIsProvider = false;
        mEndpoint   = refs[0];
    }

    public void setSequencer(Sequencer sequencer)
    {
        mSequencer = sequencer;
    }
    
    public Sequencer getSequencer()
    {
        return (mSequencer);
    }
    
    public ServiceEndpoint getEndpoint()
    {
        return (mEndpoint);
    }
     
    public void run()
    {
        try
        {
            start();
        }
        catch (Exception ex)
        {
            mError = ex;
        }
    }
    
    public void stop()
        throws Exception
    {
        if (mIsProvider)
        {
            mChannel.deactivateEndpoint(mEndpoint);
        }
        
        mChannel.close();
    }
    
    public abstract void start()
        throws Exception;
    
    public void checkError()
        throws Exception
    {
        if (mError != null)
        {
            throw mError;
        }
        else if (mFailTxt != null)
        {
            throw new Exception(mFailTxt);
        }
    }
    
    public void setFailure(String txt)
    {
        mFailTxt = txt;
    }
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle()
    {
        return null;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        return null;        
    }
    
    public Document getServiceDescription(ServiceEndpoint endpoint)
    {
        String wsdl =
            "<wsdl:description targetNamespace=\"" + mServiceName.getNamespaceURI() + "\" " +
            "xmlns:wsdl=\"http://www.w3.org/ns/wsdl\">" +
            "<wsdl:interface name=\"fooInterface\">" +
            "<wsdl:operation name=\"fooOperation\" pattern=\"" + mPattern + "\"/>" +
            "</wsdl:interface>" +
            "<wsdl:service interface=\"fooInterface\" name=\"" + mServiceName.getLocalPart() + "\">" +
            "<wsdl:endpoint binding=\"binding1\" name=\"" + mEndpointName + "\"/>" +
            "</wsdl:service>" +
            "</wsdl:description>";

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            return dbf.newDocumentBuilder().parse(new InputSource(new StringReader(wsdl)));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return (null);
        }
    }    
    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        return mEndpoint;
    }
}
