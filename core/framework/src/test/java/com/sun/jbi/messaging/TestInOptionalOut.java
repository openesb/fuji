/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestInOptionalOut.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;


import javax.xml.namespace.QName;
/**
 * Test InOptionalOut Message Exchange.
 * @author Sun Microsystems, Inc.
 */
public class TestInOptionalOut extends junit.framework.TestCase
{
    private static final QName  SERVICE  = new QName("InOptionalOutService");
    private static final String ENDPOINT = "InOptionalOutEndpoint";
    private static final QName  OPERATION = new QName("foobar");
    
    private MessageService          mMsgSvc;
    private MessageExchangeFactory  mFactory;
     /** NMR Environment Context */
    private NMRContext mContext;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestInOptionalOut(String aTestName)
        throws Exception
    {
        super(aTestName);
        mMsgSvc = new MessageService();
        mContext = new NMRContext(mMsgSvc);
    }

    /**
     * Setup for the test. 
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();        
        
        mMsgSvc.startService();
        
        mFactory = new ExchangeFactory(mMsgSvc); 
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mMsgSvc.stopService();
        mContext.reset();
    }
    
// =============================  test methods ================================
 
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeGood()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with fault
     * @throws Exception test failed
     */
    public void testExchangeGood2()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding2(mMsgSvc);
        engine  = new HappyEngine2(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with out and status
     * @throws Exception test failed
     */
    public void testExchangeGood3()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding3(mMsgSvc);
        engine  = new HappyEngine3(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with out and fault
     * @throws Exception test failed
     */
    public void testExchangeGood4()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding4(mMsgSvc);
        engine  = new HappyEngine4(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeBindingSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeEngineSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeBothSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeBindingSynch4()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding4(mMsgSvc);
        engine  = new HappyEngine4(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeEngineSynch4()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding4(mMsgSvc);
        engine  = new HappySynchEngine4(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOptionalOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeBothSynch4()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding4(mMsgSvc);
        engine  = new HappySynchEngine4(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }

    /**
     * Attempt to do bad things with an InOptionalOut exchange.
     * @throws Exception test failed
     */
    public void testExchangeFailure()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding3(mMsgSvc);
        engine  = new BadEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OPTIONAL_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    
// ============================ internal stuff ================================
    
    class HappyBinding extends Binding
    {
        HappyBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOptionalOut       inOptOut;

            // create the exchange
            inOptOut  = mFactory.createInOptionalOutExchange();
            inMsg   = inOptOut.createMessage();
            assertEquals(inOptOut.getPattern().toString(), ExchangePattern.IN_OPTIONAL_OUT.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // set the stuff we know & check that they are set.
            inOptOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOptOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOptOut.setInMessage(inMsg);
            assertEquals(inMsg, inOptOut.getInMessage());

            // send the exchange
            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while ownership is elsewhere.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());
            
            // receive the response            
            inOptOut = (InOptionalOut)mChannel.accept();

            assertEquals(ExchangeStatus.DONE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() == null);
            assertTrue(inOptOut.getOutMessage() == null);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while DONE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());            
        }
    }

    class HappyBinding2 extends Binding
    {
        HappyBinding2(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOptionalOut              inOptOut;

            // create the exchange
            inOptOut  = mFactory.createInOptionalOutExchange();
            inMsg   = inOptOut.createMessage();

            // set the stuff we know & check that they are set.
            inOptOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOptOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOptOut.setInMessage(inMsg);
            assertEquals(inMsg, inOptOut.getInMessage());

            // send the exchange
            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while ownership is elsewhere.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());

            // receive the response            
            inOptOut = (InOptionalOut)mChannel.accept();

            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() != null);
    
            // Check that settings are ignored when DONE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());
            
            // Try and set a fault.
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to create/set fault after fault.");
            }
            catch (Exception ex1) {};         

            inOptOut.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while still ACTIVE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());           
        }
    }

    class HappyBinding3 extends Binding
    {
        HappyBinding3(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOptionalOut       inOptOut;

            // create the exchange
            inOptOut  = mFactory.createInOptionalOutExchange();
            inMsg   = inOptOut.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // set the stuff we know & check that they are set.
            inOptOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOptOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOptOut.setInMessage(inMsg);
            assertEquals(inMsg, inOptOut.getInMessage());

            // send the exchange
            mChannel.send(inOptOut);

            // Check that settings are ignored while ownership is elsewhere.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());
            
            // receive the response            
            inOptOut = (InOptionalOut)mChannel.accept();

            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() == null);
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored when DONE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());

            inOptOut.setStatus(ExchangeStatus.DONE);

            // Try and set a fault
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to create/set fault after setting status.");
            }
            catch (Exception ex1) {};         

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while still ACTIVE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());           
        }
    }

    class HappyBinding4 extends Binding
    {
        HappyBinding4(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOptionalOut       inOptOut;

            // create the exchange
            inOptOut  = mFactory.createInOptionalOutExchange();
            inMsg   = inOptOut.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // set the stuff we know & check that they are set.
            inOptOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOptOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOptOut.setInMessage(inMsg);
            assertEquals(inMsg, inOptOut.getInMessage());

            // send the exchange
            mChannel.send(inOptOut);

            // Check that settings are ignored while ownership is elsewhere.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());

            // receive the response            
            inOptOut = (InOptionalOut)mChannel.accept();

            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() == null);
            assertTrue(inOptOut.getOutMessage() != null);

            // Check that settings are ignored when DONE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());

            inOptOut.setFault(inOptOut.createFault());

            // Try and set Status 
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after setting fault.");
            }
            catch (Exception ex1) {};         

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while still ACTIVE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());

            // receive the response            
            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());
            assertEquals(ExchangeStatus.DONE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() != null);
            assertTrue(inOptOut.getOutMessage() != null);
        }
    }
    
    class HappySynchBinding extends Binding
    {
        HappySynchBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOptionalOut       inOptOut;

            // create the exchange
            inOptOut  = mFactory.createInOptionalOutExchange();
            inMsg   = inOptOut.createMessage();
            assertEquals(inOptOut.getPattern().toString(), ExchangePattern.IN_OPTIONAL_OUT.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // set the stuff we know & check that they are set.
            inOptOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOptOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOptOut.setInMessage(inMsg);
            assertEquals(inMsg, inOptOut.getInMessage());

            // send the exchange
            assertTrue(mChannel.sendSync(inOptOut));
            assertEquals(ExchangeStatus.DONE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() == null);
            assertTrue(inOptOut.getOutMessage() == null);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored while DONE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());            
        }
    }

    class HappySynchBinding4 extends Binding
    {
        HappySynchBinding4(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOptionalOut       inOptOut;

            // create the exchange
            inOptOut  = mFactory.createInOptionalOutExchange();
            inMsg   = inOptOut.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // set the stuff we know & check that they are set.
            inOptOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOptOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOptOut.setInMessage(inMsg);
            assertEquals(inMsg, inOptOut.getInMessage());

            // send the exchange
            assertTrue(mChannel.sendSync(inOptOut));
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() == null);
            assertTrue(inOptOut.getOutMessage() != null);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());

            // Check that settings are ignored when DONE.

            inOptOut.setEndpoint(null);
            assertEquals(mEndpoint, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertEquals(OPERATION, inOptOut.getOperation());

            inOptOut.setFault(inOptOut.createFault());

            // Try and set Status 
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after setting fault.");
            }
            catch (Exception ex1) {};         

            assertTrue(mChannel.sendSync(inOptOut));
            assertEquals(ExchangeStatus.DONE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() != null);
            assertTrue(inOptOut.getOutMessage() != null);
            assertEquals(MessageExchange.Role.CONSUMER, inOptOut.getRole());
        }
    }

    class HappyEngine extends Engine
    {
        HappyEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut inOptOut;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(inOptOut.getPattern().toString(), ExchangePattern.IN_OPTIONAL_OUT.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOptOut.getOperation());

            inOptOut.setStatus(ExchangeStatus.DONE);

            // Try and set out message after status set.
            try
            {
                inOptOut.setOutMessage(inOptOut.createMessage());
                setFailure("Able to set out message after status");
            }
            catch (Exception ex) {};

            // Try and create/set fault after status is set.
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to set/create fault after status");
            }
            catch (Exception ex) {};

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOptOut.getOperation());            
        }
    }
    
    class HappyEngine2 extends Engine
    {
        HappyEngine2(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut inOptOut;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOptOut.getOperation());

            inOptOut.setFault(inOptOut.createFault());

            // Try and set out message after fault
            try
            {
                inOptOut.setOutMessage(inOptOut.createMessage());
                setFailure("Able to set out message after fault");
            }
            catch (Exception ex) {};

            // Try and set out status after fault
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after fault");
            }
            catch (Exception ex) {};

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOptOut.getOperation());

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.DONE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);

            // Try and set out status after fault has been sent.
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after fault is sent");
            }
            catch (Exception ex) {};
        }
    }
    
    
    class HappyEngine3 extends Engine
    {
        HappyEngine3(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut inOptOut;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOptOut.getOperation());

            inOptOut.setOutMessage(inOptOut.createMessage());

            // Try and set fault after out message
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to set fault after out message");
            }
            catch (Exception ex) {};

            // Try and set out status after out message
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after out message");
            }
            catch (Exception ex) {};

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOptOut.getOperation());
            
            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.DONE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() == null);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
        }
    }
    
    class HappyEngine4 extends Engine
    {
        HappyEngine4(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut inOptOut;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOptOut.getOperation());

            inOptOut.setOutMessage(inOptOut.createMessage());

            // Try and set fault after out message
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to set fault after out message");
            }
            catch (Exception ex) {};

            // Try and set out status after out message
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after out message");
            }
            catch (Exception ex) {};

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOptOut.getOperation());
            
            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() != null);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());

            // Try and set out message after fault
            try
            {
                inOptOut.setOutMessage(inOptOut.createMessage());
                setFailure("Able to set out message after fault");
            }
            catch (Exception ex) {};

            inOptOut.setStatus(ExchangeStatus.DONE);
            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
        }
    }
    
    class HappySynchEngine extends Engine
    {
        HappySynchEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut inOptOut;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(inOptOut.getPattern().toString(), ExchangePattern.IN_OPTIONAL_OUT.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOptOut.getOperation());

            inOptOut.setStatus(ExchangeStatus.DONE);

            // Try and set out message after status set.
            try
            {
                inOptOut.setOutMessage(inOptOut.createMessage());
                setFailure("Able to set out message after status");
            }
            catch (Exception ex) {};

            // Try and create/set fault after status is set.
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to set/create fault after status");
            }
            catch (Exception ex) {};

            // Try sendSync
            try
            {
                mChannel.sendSync(inOptOut);
                setFailure("Able to sendSync when not legal");
            }
            catch (Exception ex) {};

            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is DONE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOptOut.getOperation());            
        }
    }

    class HappySynchEngine4 extends Engine
    {
        HappySynchEngine4(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut inOptOut;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getInMessage() != null);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOptOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOptOut.getEndpoint());

            inOptOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOptOut.getOperation());

            inOptOut.setOutMessage(inOptOut.createMessage());

            // Try and set fault after out message
            try
            {
                inOptOut.setFault(inOptOut.createFault());
                setFailure("Able to set fault after out message");
            }
            catch (Exception ex) {};

            // Try and set out status after out message
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status after out message");
            }
            catch (Exception ex) {};

            assertTrue(mChannel.sendSync(inOptOut));
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            assertEquals(ExchangeStatus.ACTIVE, inOptOut.getStatus());
            assertTrue(inOptOut.getFault() != null);

            // Try and set out message after fault
            try
            {
                inOptOut.setOutMessage(inOptOut.createMessage());
                setFailure("Able to set out message after fault");
            }
            catch (Exception ex) {};

            inOptOut.setStatus(ExchangeStatus.DONE);
            // Try sendSync
            try
            {
                mChannel.sendSync(inOptOut);
                setFailure("Able to sendSync when not legal");
            }
            catch (Exception ex) {};
            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
        }
    }
    
     class BadEngine extends Engine
    {
        BadEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOptionalOut              inOptOut;
            Fault               fault;
            NormalizedMessage   msg;

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
            
            // Try and set in message from engine
            try
            {
                msg = inOptOut.createMessage();
                inOptOut.setInMessage(msg);
                setFailure("Able to set in message from engine");
            }
            catch (Exception ex1) {};         
            

            // complete the exchange normally
            msg = inOptOut.createMessage();
            inOptOut.setOutMessage(msg);
            mChannel.send(inOptOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());

            // Try and send after DONE.
            try
            {
                msg = inOptOut.createMessage();
                inOptOut.setInMessage(msg);
                setFailure("Able to set status.");
            }
            catch (Exception ex1) {};         

            // Try and set a status.
            try
            {
                inOptOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status.");
            }
            catch (Exception ex1) {};         

            inOptOut = (InOptionalOut)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOptOut.getRole());
        }
    }
}
