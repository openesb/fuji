/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkTaskResultTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.XmlTests;


/**
 * Unit tests for FrameworkTaskResult class.
 * @author kcbabo
 */
public class FrameworkTaskResultTest extends XmlTests {
    
    private static final String CAUSE_FRAMEWORK = "YES";
    
    private static final String TEST_ELEMENT = 
            "<frmwk-task-result xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<frmwk-task-result-details></frmwk-task-result-details> " +
                "<is-cause-framework>" + CAUSE_FRAMEWORK + "</is-cause-framework>" +
             "</frmwk-task-result>";
    
    public FrameworkTaskResultTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }            

    /**
     * Test of getIsCauseFramework method, of class FrameworkTaskResult.
     */
    public void testGetIsCauseFramework() throws Exception {
        FrameworkTaskResult ftr = new FrameworkTaskResult(
                stringToElement(TEST_ELEMENT));
        assertEquals(CAUSE_FRAMEWORK, ftr.getIsCauseFramework().toString());
    }
    
    /**
     * Test of getTaskResultDetails method, of class FrameworkTaskResult.
     */
    public void testGetFrameworkTaskResultDetails() throws Exception {
        FrameworkTaskResult ftr = new FrameworkTaskResult(
                stringToElement(TEST_ELEMENT));
        assertNotNull(ftr.getFrameworkTaskResultDetails());
    }
    
    /**
     * Test of setIsCauseFramework method, of class FrameworkTaskResult.
     */
    public void testSetIsCauseFramework() throws Exception {
        FrameworkTaskResult ftr = new FrameworkTaskResult(
                createElement(TaskElem.frmwk_task_result));
        
        ftr.setIsCauseFramework(FrameworkTaskResult.IsCauseFramework.YES);
        assertEquals(FrameworkTaskResult.IsCauseFramework.YES, 
                ftr.getIsCauseFramework());
        
        ftr.setIsCauseFramework(FrameworkTaskResult.IsCauseFramework.NO);
        assertEquals(FrameworkTaskResult.IsCauseFramework.NO, 
                ftr.getIsCauseFramework());
    }
    
    /**
     * Test creating a new FrameworkTaskResult element from scratch.
     */
    public void testCreateNewFrameworkTaskResult() throws Exception {
        FrameworkTaskResult ftr = new FrameworkTaskResult(
                createElement(TaskElem.frmwk_task_result));
        
        assertNotNull(ftr.getFrameworkTaskResultDetails());
        assertNull(ftr.getIsCauseFramework());
    }

}
