package com.sun.jbi.interceptors.internal.flow.aggregate;

import junit.framework.TestCase;

/**
 *
 * @author nkaps
 */
public class TestSetAggregate extends TestCase {
    
    
    public TestSetAggregate(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
       
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test getting the element txt
     */
    public void testGetElemetText1() throws Exception {
        SetAggregate sa = new SetAggregate();
        
        String text = sa.getElementText("</goodText>");
        System.out.println(text);
        assertEquals("goodText", text);
    }
    
    /**
     * Test getting the element txt
     */
    public void testGetElemetText2() throws Exception {
        SetAggregate sa = new SetAggregate();
        
        String text = sa.getElementText("/goodText>");
        System.out.println(text);
        assertEquals("goodText", text);
    }
    
    /**
     * Test getting the element txt
     */
    public void testGetElemetText3() throws Exception {
        SetAggregate sa = new SetAggregate();
        
        String text = sa.getElementText("/goodText");
        System.out.println(text);
        assertEquals("goodText", text);
    }
    
    /**
     * Test getting the element txt
     */
    public void testGetElemetText4() throws Exception {
        SetAggregate sa = new SetAggregate();
        
        String text = sa.getElementText("goodText");
        System.out.println(text);
        assertEquals("goodText", text);
    }
    
    /**
     * Test getting the element txt
     */
    public void testGetElemetText5() throws Exception {
        SetAggregate sa = new SetAggregate();
        
        String text = sa.getElementText("<goodText");
        System.out.println(text);
        assertEquals("goodText", text);
    }
    
    /**
     * Test getting the element txt
     */
    public void testGetElemetText6() throws Exception {
        SetAggregate sa = new SetAggregate();
        
        String text = sa.getElementText("goodText>");
        System.out.println(text);
        assertEquals("goodText", text);
    }    
}