/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRContext.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.util.HashMap;
import java.util.Vector;
import javax.management.MBeanServerFactory;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/** Simple NMR implementation of Framework contexts.
 * @author Sun Microsystems, Inc.
 */
public class NMRContext 
{
    public static final String ROOT_PATH = "bld";
    
    private HashMap                 mComponents;    
    private HashMap                 mContexts;
    private MessageService          mMsgSvc;
    private Vector                  mEmptyVec;
    
    public NMRContext(MessageService msgSvc)
    {
        mComponents = new HashMap();        
        mContexts   = new HashMap();
        mEmptyVec   = new Vector();
        mMsgSvc     = msgSvc;
    }
    
    
    public void reset()
    {
        mComponents.clear();
        mContexts.clear();
    }
    
    public ComponentContext getComponentContext(String componentId)
    {
        return (ComponentContext)mContexts.get(componentId);
    }
    
    /*******************************************************************
     *                  ComponentManager Methods
     ******************************************************************/
    
    public void addComponentInstance(String id, Component component)
        throws Exception
    {
        mComponents.put(id, component);
        mContexts.put(id, new ComponentContextImpl(mMsgSvc.activateChannel(id, component)));
    }
    
    
    public Component getComponentInstance(String str)
    {
        return (Component)mComponents.get(str);
    }

    /*******************************************************************
     *                EnvironmentContext Methods
     ******************************************************************/
    
    /**
     * Indicates whether or not the JBI framework has been fully started.  This
     * method provides clients with a way of determining if the JBI framework
     * started up in passive mode as a result of on-demand initialization.  
     * The 'start' parameter instructs the framework to
     * start completely if it has not already done so.  If the framework has
     * already been started, the request to start again is ignored.
     * @param start requests that the framework start completely before
     *  returning.
     * @return true if the framework is completely started, false otherwise.
     */
    public boolean isFrameworkReady(boolean start)
    {
        return true;
    }
    
    public String getAppServerInstallRoot()
    {
        return ROOT_PATH;
    }
    
    public String getAppServerInstanceRoot()
    {
        return ROOT_PATH;
    }
    
    public String getJbiInstallRoot()
    {
        return ROOT_PATH;
    }
    
    public String getJbiInstanceRoot()
    {
        return ROOT_PATH;
    }
    
    public javax.management.MBeanServer getMBeanServer()
    {
        return MBeanServerFactory.createMBeanServer("com.sun.jbi");
    }
    
    /**
     * This method is used to scaffold EnvironmentContext.isStartOnDeployEnabled()
     * This method is used only for testing and always returns true
     */
    public boolean isStartOnDeployEnabled()
    {
        return true;
    }
    
    /**
     * This method is used to find out if start-onverify is enabled.
     * When this is enabled components are started automatically when 
     * an application has to be verified for them 
     * This is controlled by the property com.sun.jbi.startOnVerify.
     * By default start-on-verify is enabled. 
     * It is disabled only if com.sun.jbi.startOnVerify=false.
     */
    public boolean isStartOnVerifyEnabled()
    {
        return true;
    }    
    
           
    
    /** Little impl of ComponentContext. */
    class ComponentContextImpl implements ComponentContext
    {
        private DeliveryChannelImpl mChannel;

        ComponentContextImpl(DeliveryChannelImpl channel)
        {
            mChannel = channel;
        }

        /*******************************************************************
         *                  ComponentContext Methods
         ******************************************************************/

        public ServiceEndpoint activateEndpoint(QName serviceName, String endpointName) 
            throws javax.jbi.JBIException
        {
            return mChannel.activateEndpoint(serviceName, endpointName);
        }

        public void deactivateEndpoint(ServiceEndpoint endpoint) 
            throws javax.jbi.JBIException
        {
            mChannel.deactivateEndpoint(endpoint);
        }

        public void deregisterExternalEndpoint(ServiceEndpoint externalEndpoint)
            throws javax.jbi.JBIException
        {

        }

        public String getComponentName()
        {
            return mChannel.getChannelId();
        }

        public javax.jbi.messaging.DeliveryChannel getDeliveryChannel() 
            throws javax.jbi.messaging.MessagingException
        {
            return mChannel;
        }

        public ServiceEndpoint getEndpoint(javax.xml.namespace.QName service, String name)
        {
            return null;
        }

        public org.w3c.dom.Document getEndpointDescriptor(ServiceEndpoint endpoint) 
            throws javax.jbi.JBIException
        {
            return mChannel.getEndpointDescriptor(endpoint, false);
        }

        public ServiceEndpoint[] getEndpointsForService(javax.xml.namespace.QName serviceName)
        {
            return mChannel.getEndpointsForService(serviceName);
        }

        public ServiceEndpoint[] getExternalEndpoints(javax.xml.namespace.QName interfaceName) 
        {
            return null;
        }

        public ServiceEndpoint[] getExternalEndpointsForService(javax.xml.namespace.QName serviceName) 
        {
            return null;
        }

        public String getInstallRoot()
        {
            return NMRContext.ROOT_PATH;
        }

        public java.util.logging.Logger getLogger(String suffix, String resourceBundleName) 
            throws java.util.MissingResourceException, javax.jbi.JBIException
        {
            return null;
        }

        public String getWorkspaceRoot()
        {
            return NMRContext.ROOT_PATH;
        }

        public void registerExternalEndpoint(ServiceEndpoint externalEndpoint)
        {

        }

        public ServiceEndpoint resolveEndpointReference(
            org.w3c.dom.DocumentFragment endpointReference)
        {
            return null;
        }

        public javax.jbi.management.MBeanNames getMBeanNames()
        {
            return null;
        }

        public javax.management.MBeanServer getMBeanServer()
        {
            return null;
        }

        public javax.naming.InitialContext getNamingContext()
        {
            return null;
        }

        public Object getTransactionManager()
        {
            return null;
        }
        
        public ServiceEndpoint[] getEndpoints(QName interfaceName) 
        {
            return mChannel.getEndpoints(interfaceName);
        }
       
    }
    
}
