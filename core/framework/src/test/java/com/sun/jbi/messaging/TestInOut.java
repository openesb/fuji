/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestInOut.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;


import javax.xml.namespace.QName;
/**
 * Test InOut Message Exchange.
 * @author Sun Microsystems, Inc.
 */
public class TestInOut extends junit.framework.TestCase
{
    private static final QName  SERVICE  = new QName("InOutService");
    private static final String ENDPOINT = "InOutEndpoint";
    private static final QName  OPERATION = new QName("foobar");
    
    private MessageService          mMsgSvc;
    private MessageExchangeFactory  mFactory;
     /** NMR Environment Context */
    private NMRContext mContext;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestInOut(String aTestName)
        throws Exception
    {
        super(aTestName);
        mMsgSvc = new MessageService();
        mFactory = new ExchangeFactory(mMsgSvc);
        mContext = new NMRContext(mMsgSvc);
    }

    /**
     * Setup for the test. 
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();        
        
        mMsgSvc.startService();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mMsgSvc.stopService();
        mContext.reset();
    }
    
// =============================  test methods ================================
 
    /**
     * Happy path test for an InOut exchange with status
     * @throws Exception test failed
     */
    public void testExchangeGood()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
         
   /**
     * Happy path test for an InOut exchange with fault
     * @throws Exception test failed
     */
    public void testExchangeGood2()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding2(mMsgSvc);
        engine  = new HappyEngine2(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    /**
     * Happy path test for an InOut exchange with error status
     * @throws Exception test failed
     */
    public void testExchangeGood3()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding3(mMsgSvc);
        engine  = new HappyEngine3(mMsgSvc);
                
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }

    /**
     * Happy path test for an InOut exchange with error status
     * @throws Exception test failed
     */
    public void testExchangeBindingSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappyEngine(mMsgSvc);
                
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    public void testExchangeEngineSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
   
    public void testExchangeGoodBothSynch()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappySynchBinding(mMsgSvc);
        engine  = new HappySynchEngine(mMsgSvc);
        
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }

    /**
     * Attempt to do bad things with an InOut exchange.
     * @throws Exception test failed
     */
    public void testExchangeFailure()
        throws Exception
    {
        Binding binding;
        Engine  engine;
        
        binding = new HappyBinding(mMsgSvc);
        engine  = new BadEngine(mMsgSvc);
                
        engine.init(SERVICE, ENDPOINT, ExchangePattern.IN_OUT.toString());
        binding.init(SERVICE);
        
        Framework.runTest(binding, engine);
        
        // check for binding or engine failure
        binding.checkError();
        engine.checkError();
        
        binding.stop();
        engine.stop();
    }
    
    
// ============================ internal stuff ================================
    
    class HappyBinding extends Binding
    {
        HappyBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOut               inOut;

            // create the exchange
            inOut  = mFactory.createInOutExchange();
            inMsg   = inOut.createMessage();
            assertEquals(inOut.getPattern().toString(), ExchangePattern.IN_OUT.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOut.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // set the stuff we know & check that they are set.
            inOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOut.setInMessage(inMsg);
            assertEquals(inMsg, inOut.getInMessage());

            // send the exchange
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // Check that settings are ignored while ownership is elsewhere.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());

            // receive the response            
            inOut = (InOut)mChannel.accept();
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            assertEquals(ExchangeStatus.ACTIVE, inOut.getStatus());
            assertTrue(inOut.getFault() == null);

            // Check that settings are ignored while still ACTIVE.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());
            
            inOut.setStatus(ExchangeStatus.DONE);
            
            // Try create a fault after status has been set.
            try
            {
                inOut.createFault();
                setFailure("Able to create a fault after status set.");
            }
            catch (Exception ex) {}

            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // Check that settings are ignored while still ACTIVE.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());            
        }
    }

    class HappyBinding2 extends Binding
    {
        HappyBinding2(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOut              inOut;

            // create the exchange
            inOut  = mFactory.createInOutExchange();
            inMsg   = inOut.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // set the stuff we know & check that they are set.
            inOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOut.setInMessage(inMsg);
            assertEquals(inMsg, inOut.getInMessage());

            // send the exchange
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // Check that settings are ignored while ownership is elsewhere.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());
           
            // receive the response            
            inOut = (InOut)mChannel.accept();

            assertEquals(ExchangeStatus.ACTIVE, inOut.getStatus());
            assertTrue(inOut.getFault() != null);
    
            // Check that settings are ignored when ACTIVE.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());
            
            inOut.setStatus(ExchangeStatus.ERROR);
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // Check that settings are ignored while still ERROR.
            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());           
        }
    }

    class HappyBinding3 extends Binding
    {
        HappyBinding3(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOut              inOut;

            // create the exchange
            inOut  = mFactory.createInOutExchange();
            inMsg   = inOut.createMessage();
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // set the stuff we know & check that they are set.
            inOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOut.setInMessage(inMsg);
            assertEquals(inMsg, inOut.getInMessage());

            // send the exchange
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());
    
            // Check that settings are ignored while ownership is elsewhere.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());
            
            // receive the response            
            inOut = (InOut)mChannel.accept();
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());
            assertEquals(ExchangeStatus.ERROR, inOut.getStatus());            
        }
    }

    class HappySynchBinding extends Binding
    {
        HappySynchBinding(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            NormalizedMessage   inMsg;
            InOut               inOut;

            // create the exchange
            inOut  = mFactory.createInOutExchange();
            inMsg   = inOut.createMessage();
            assertEquals(inOut.getPattern().toString(), ExchangePattern.IN_OUT.toString());
            assertEquals(ExchangeStatus.ACTIVE, inOut.getStatus());
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // set the stuff we know & check that they are set.
            inOut.setEndpoint(mEndpoint);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(OPERATION);
            assertEquals(OPERATION, inOut.getOperation());

            // set the payload
            Payload.setPayload(inMsg);
            
            // set message on exchange
            inOut.setInMessage(inMsg);
            assertEquals(inMsg, inOut.getInMessage());

            // send the exchange and wait for response
            assertTrue(mChannel.sendSync(inOut));
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());
            assertEquals(ExchangeStatus.ACTIVE, inOut.getStatus());
            assertTrue(inOut.getFault() == null);

            // Check that settings are ignored while still ACTIVE.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());
            
            inOut.setStatus(ExchangeStatus.DONE);
            
            // Try create a fault after status has been set.
            try
            {
                inOut.createFault();
                setFailure("Able to create a fault after status set.");
            }
            catch (Exception ex) {}

            // Try sendSync
            try
            {
                mChannel.sendSync(inOut);
                setFailure("Able to sendSync when not legal");
            }
            catch (Exception ex) {};
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.CONSUMER, inOut.getRole());

            // Check that settings are ignored while still ACTIVE.

            inOut.setEndpoint(null);
            assertEquals(mEndpoint, inOut.getEndpoint());

            inOut.setOperation(null);
            assertEquals(OPERATION, inOut.getOperation());            
        }
    }
   
    class HappyEngine extends Engine
    {
        HappyEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOut inOut;
            NormalizedMessage   outMsg;

            inOut = (InOut)mChannel.accept();
            assertEquals(inOut.getPattern().toString(), ExchangePattern.IN_OUT.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
            outMsg   = inOut.createMessage();
            
            // Check that settings are ignored after message is ACTIVE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOut.getOperation());

            // Check that we can't set DONE status.
            try
            {
                inOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set DONE on InOut Exchange as provider.");
            }
            catch (Exception Ex){}
            
            // verify in message is present
            if (inOut.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            inOut.setOutMessage(outMsg);
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOut.getOperation());
            
            inOut = (InOut)mChannel.accept();
            assertEquals(ExchangeStatus.DONE, inOut.getStatus());
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
        }
    }
    
    class HappySynchEngine extends Engine
    {
        HappySynchEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOut inOut;
            NormalizedMessage   outMsg;

            inOut = (InOut)mChannel.accept();
            assertEquals(inOut.getPattern().toString(), ExchangePattern.IN_OUT.toString());
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
            outMsg   = inOut.createMessage();
            
            // Check that settings are ignored after message is ACTIVE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOut.getOperation());

            // verify in message is present
            if (inOut.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            inOut.setOutMessage(outMsg);
            assertTrue(mChannel.sendSync(inOut));
            assertEquals(ExchangeStatus.DONE, inOut.getStatus());
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
        }
    }
    
    class HappyEngine2 extends Engine
    {
        HappyEngine2(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOut inOut;

            inOut = (InOut)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOut.getOperation());

            // verify in message is present
            if (inOut.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            inOut.setFault(inOut.createFault());
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOut.getOperation());
            
            inOut = (InOut)mChannel.accept();
            assertEquals(ExchangeStatus.ERROR, inOut.getStatus());
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
        }
    }
    
    class HappyEngine3 extends Engine
    {
        HappyEngine3(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOut inOut;
            NormalizedMessage   outMsg;

            inOut = (InOut)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
            
            // Check that settings are ignored after message is ACTIVE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after ACTIVE", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after ACTIVE", null, inOut.getOperation());

            // verify in message is present
            if (inOut.getInMessage() == null)
            {
                setFailure("In message is null!");
                return;
            }
            
            // Check that Error status can be set after an OUT message has been set and that
            // the OUT message is null'ed after ERROR is set.'
            
            outMsg   = inOut.createMessage();
            inOut.setOutMessage(outMsg);
            inOut.setStatus(ExchangeStatus.ERROR);
            assertSame("Out message not null after ERROR set", inOut.getOutMessage(), null);
            try
            {
                inOut.setOutMessage(outMsg);
                setFailure("Can set OUT message after ERROR has been set.");
            } catch (Exception e) {}
            
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());

            // Check that settings are ignored after message is DONE.

            inOut.setEndpoint(null);
            assertNotSame("Allowed to set endpoint after done", null, inOut.getEndpoint());

            inOut.setOperation(null);
            assertNotSame("Allowed to set operation after done", null, inOut.getOperation());
        }
    }
    
    class BadEngine extends Engine
    {
        BadEngine(MessageService msgService)
        {
            super(msgService);
        }

        public void start()
            throws Exception
        {
            InOut              inOut;
            Fault               fault;
            NormalizedMessage   msg;

            inOut = (InOut)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
            
            // Try and set in message from engine
            try
            {
                msg = inOut.createMessage();
                inOut.setInMessage(msg);
                setFailure("Able to set in message from engine");
                return;
            }
            catch (Exception ex1) {};         
            

            // complete the exchange normally
            msg = inOut.createMessage();
            inOut.setOutMessage(msg);
            mChannel.send(inOut);
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());

            // Try and set out after DONE.
            try
            {
                msg = inOut.createMessage();
                inOut.setOutMessage(msg);
                setFailure("Able to set out message after send from engine.");
                return;
            }
            catch (Exception ex1) {};         

            // Try and set a status.
            try
            {
                inOut.setStatus(ExchangeStatus.DONE);
                setFailure("Able to set status.");
            }
            catch (Exception ex1) {};         

            inOut = (InOut)mChannel.accept();
            assertEquals(MessageExchange.Role.PROVIDER, inOut.getRole());
        }
    }
}
