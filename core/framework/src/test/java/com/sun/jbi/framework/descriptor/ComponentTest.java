/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.descriptor;

import com.sun.jbi.test.osgi.XmlTests;


/**
 * Unit tests for Component class.
 * @author kcbabo
 */
public class ComponentTest extends XmlTests {
    
    private static final String TYPE =
            "service-engine";
    
    private static final String BCL_DELEGATION =
            "self-first";
    
    private static final String CCL_DELEGATION =
            "parent-first";
    
    private static final String COMPONENT_CLASS_NAME =
            "com.sun.foo.MyEngine";
    
    private static final String BOOTSTRAP_CLASS_NAME =
            "com.sun.foo.MyInstaller";
    
    private static final String NODE =
            "<component xmlns=\"" + Descriptor.NS_URI + "\" type=\"" + TYPE + 
            "\" component-class-loader-delegation=\"" + CCL_DELEGATION + 
            "\" bootstrap-class-loader-delegation=\"" + BCL_DELEGATION + "\">" +
            "<identification/>" +
            "<component-class-name>" + COMPONENT_CLASS_NAME + "</component-class-name>" +
            "<component-class-path/>" +
            "<bootstrap-class-name>" + BOOTSTRAP_CLASS_NAME + "</bootstrap-class-name>" +
            "<bootstrap-class-path/>" +
            "<shared-library/>" +
            "<shared-library/>" +
            "<shared-library/>" +
            "</component>";
    
    private Component component_;
    
    public ComponentTest(String testName) {
        super(testName, JBI_XML_SCHEMA);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        component_ = new Component(stringToElement(NODE));
    }

    /**
     * Test of getType method, of class Component.
     */
    public void testGetType() throws Exception  {
        assertEquals(component_.getType(), TYPE);
    }

    /**
     * Test of getComponentClassLoaderDelegation method, of class Component.
     */
    public void testGetComponentClassLoaderDelegation() throws Exception  {
        assertEquals(component_.getComponentClassLoaderDelegation(), CCL_DELEGATION);
    }

    /**
     * Test of getBootstrapClassLoaderDelegation method, of class Component.
     */
    public void testGetBootstrapClassLoaderDelegation() throws Exception  {
        assertEquals(component_.getBootstrapClassLoaderDelegation(), BCL_DELEGATION);
    }

    /**
     * Test of getBootstrapClassName method, of class Component.
     */
    public void testGetBootstrapClassName() throws Exception  {
        assertEquals(component_.getBootstrapClassName(), BOOTSTRAP_CLASS_NAME);
    }

    /**
     * Test of getComponentClassName method, of class Component.
     */
    public void testGetComponentClassName() throws Exception  {
        assertEquals(component_.getComponentClassName(), COMPONENT_CLASS_NAME);
    }

    /**
     * Test of getIdentification method, of class Component.
     */
    public void testGetIdentification() throws Exception  {
        assertNotNull(component_.getIdentification());
    }

    /**
     * Test of getComponentClassPath method, of class Component.
     */
    public void testGetComponentClassPath() throws Exception  {
        assertNotNull(component_.getComponentClassPath());
    }

    /**
     * Test of getBootstrapClassPath method, of class Component.
     */
    public void testGetBootstrapClassPath() throws Exception  {
        assertNotNull(component_.getBootstrapClassPath());
    }

}
