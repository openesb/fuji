/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TaskResultDetailsTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.XmlTests;


/**
 * Unit tests for TaskResultDetails class.
 * @author kcbabo
 */
public class TaskResultDetailsTest extends XmlTests {
    
    private static final String TASK_ID = "deploy";
    private static final String TASK_RESULT = "SUCCESS";
    private static final String MESSAGE_TYPE = "INFO";
    
    private static final String TEST_ELEMENT = 
            "<task-result-details xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<task-id>" + TASK_ID + "</task-id>" +
                "<task-result>" + TASK_RESULT + "</task-result>" +
                "<message-type>" + MESSAGE_TYPE + "</message-type>" +
                "<task-status-msg><msg-loc-info/></task-status-msg> " +
                "<task-status-msg><msg-loc-info/></task-status-msg> " +
                "<exception-info></exception-info> " +
                "<exception-info></exception-info> " +
             "</task-result-details>";
    
    public TaskResultDetailsTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }            

    /**
     * Test of getTaskId method, of class TaskResultDetails.
     */
    public void testGetTaskId() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(stringToElement(TEST_ELEMENT));
        assertEquals(TASK_ID, trd.getTaskId());
    }

    /**
     * Test of getTaskResult method, of class TaskResultDetails.
     */
    public void testGetTaskResult() throws Exception  {
        TaskResultDetails trd = new TaskResultDetails(stringToElement(TEST_ELEMENT));
        assertEquals(TASK_RESULT, trd.getTaskResult().toString());
    }

    /**
     * Test of getMessageType method, of class TaskResultDetails.
     */
    public void testGetMessageType() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(stringToElement(TEST_ELEMENT));
        assertEquals(MESSAGE_TYPE, trd.getMessageType().toString());
    }

    /**
     * Test of getTaskStatusMsgs method, of class TaskResultDetails.
     */
    public void testGetTaskStatusMsgs() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(stringToElement(TEST_ELEMENT));
        assertTrue(trd.getTaskStatusMsgs().size() == 2);
    }

    /**
     * Test of getExceptionInfos method, of class TaskResultDetails.
     */
    public void testGetExceptionInfos() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(stringToElement(TEST_ELEMENT));
        assertTrue(trd.getExceptionInfos().size() == 2);
    }
    
    /**
     * Test of setTaskId method, of class TaskResultDetails.
     */
    public void testSetTaskId() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(
                createElement(TaskElem.task_result_details));
        
        trd.setTaskId(TASK_ID);
        assertEquals(TASK_ID, trd.getTaskId());
    }
    
    /**
     * Test of setMessageType method, of class TaskResultDetails.
     */
    public void testSetMessageType() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(
                createElement(TaskElem.task_result_details));
        
        trd.setMessageType(TaskResultDetails.MessageType.INFO);
        assertEquals(TaskResultDetails.MessageType.INFO, trd.getMessageType());
    }
    
    /**
     * Test of setTaskResult method, of class TaskResultDetails.
     */
    public void testSetTaskResult() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(
                createElement(TaskElem.task_result_details));
        
        trd.setTaskResult(TaskResultDetails.TaskResult.SUCCESS);
        assertEquals(TaskResultDetails.TaskResult.SUCCESS, trd.getTaskResult());
    }
    
    /**
     * Test of addTaskStatusMsg method, of class TaskResultDetails.
     */
    public void testAddTaskStatusMsg() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(
                createElement(TaskElem.task_result_details));
        
        assertTrue(trd.getTaskStatusMsgs().size() == 0);
        trd.addTaskStatusMsg();
        assertTrue(trd.getTaskStatusMsgs().size() == 1);
        trd.addTaskStatusMsg();
        assertTrue(trd.getTaskStatusMsgs().size() == 2);
    }
    
    /**
     * Test of addExceptionInfo method, of class TaskResultDetails.
     */
    public void testAddExceptionInfo() throws Exception {
        TaskResultDetails trd = new TaskResultDetails(
                createElement(TaskElem.task_result_details));
        
        assertTrue(trd.getExceptionInfos().size() == 0);
        trd.addExceptionInfo();
        assertTrue(trd.getExceptionInfos().size() == 1);
        trd.addExceptionInfo();
        assertTrue(trd.getExceptionInfos().size() == 2);
    }

}
