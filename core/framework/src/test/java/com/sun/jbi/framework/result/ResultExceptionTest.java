/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ResultExceptionTest.java
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import junit.framework.TestCase;

import com.sun.jbi.framework.result.TaskResultDetails.TaskId;

/**
 * @author ksimpson
 *
 */
public class ResultExceptionTest extends TestCase {

    /**
     * 
     */
    public ResultExceptionTest() {
    }

    /**
     * @param name
     */
    public ResultExceptionTest(String name) {
        super(name);
    }

    public void testCreateResult() throws Exception {
        String mmxml = getMMXml();
        ComponentTaskResult result = JbiTask.createComponentTaskResult(mmxml);
        evaluateResult(result);
    }
    
    public void testInspect() throws Exception {
        runInspectTest("testInspect", false, "This is not an MMXml message!");
        runInspectTest("testInspect", true, getMMXml());
    }
    
    public void testSuccess() throws Exception {
        String mmxml = (new MockComp()).createSuccessMessage(TaskId.init);
        runInspectTest("testSuccess", false, mmxml);
    }

    public void testException() throws Exception {
        String mmxml = (new MockComp()).createExceptionMessage(
                TaskId.init, null, "COMPTK_INIT_1", "COMPTK-6007: Failed init message: TRANSL-6053: Nested failure:");
        runInspectTest("testException", true, mmxml);
    }

    public void testFailed() throws Exception {
        String mmxml = (new MockComp()).createFailedMessage(
                TaskId.init, "COMPTK_INIT_1", "COMPTK-6007: Failed init message: TRANSL-6053: Nested failure:");
        runInspectTest("testSuccess", true, mmxml);
    }

    protected void runInspectTest(String mthd, boolean shouldThrow, String mmxml) throws Exception {
        if (shouldThrow) {
            try {
                ComponentTaskResult.inspectResult(mmxml);
                fail(mthd +": A DeploymentException should have been thrown!");
            }
            catch (Exception e) {
                assertTrue(mthd +": wrong exception type", e instanceof ResultException);
                ResultException rex = (ResultException) e;
                evaluateResult(rex.getComponentTaskResult());
            }
        }
        else {
            try { ComponentTaskResult.inspectResult(mmxml); }
            catch (Exception e) { fail(mthd +": Should NOT have failed inspection!"); }
        }
    }
    
    protected void evaluateResult(ComponentTaskResult result) {
        assertNotNull("result is null", result);
        assertEquals("wrong component name", 
                     "sun-xslt-engine", result.getComponentName());
        assertEquals("wrong task id", 
                     "init", result.getTaskResultDetails().getTaskId());
        assertNotNull("wrong message type", result.getTaskResultDetails().getMessageType());
        assertTrue("no messages", 0 < result.getTaskResultDetails().getTaskStatusMsgs().size());
        assertEquals("wrong component name", 
                     "COMPTK_INIT_1", 
                     result.getTaskResultDetails().getTaskStatusMsgs().get(0).getToken());
        String msg = result.getTaskResultDetails().getTaskStatusMsgs().get(0).getMessage();
        assertNotNull("null message", msg);
        assertTrue("wrong message 1", msg.startsWith("COMPTK-6007:"));
        assertTrue("wrong message 2", msg.contains("TRANSL-6053:"));
    }

    protected String getMMXml() throws Exception {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"+
                "<component-name>sun-xslt-engine</component-name>"+
                "<component-task-result-details><task-result-details>"+
                "<task-id>init</task-id><task-result>FAILED</task-result>"+
                "<message-type>WARNING</message-type><task-status-msg>"+
                "<msg-loc-info><loc-token>COMPTK_INIT_1</loc-token>"+
                "<loc-message>COMPTK-6007: Service unit \"fuji2-sun-xslt-engine\""+
                " failed to start provider endpoints: TRANSL-6053: sun-xslt-engine"+
                " EndpointInfo[name=genHtml_endpoint,service={http://fuji.dev.java.net/application/fuji2}genHtml,provides]"+
                " is missing from descriptor in service unit: fuji2-sun-xslt-engine</loc-message>"+
                "</msg-loc-info></task-status-msg></task-result-details>"+
                "</component-task-result-details></component-task-result>";
    }
    
    private static class MockComp {
        public String createExceptionMessage(TaskId taskId, Throwable exception, 
                                             String locToken, String locMessage,
                                             Object... locParams) {
            return ComponentTaskResult.createExceptionMessage(getComponentName(),
                    taskId, exception, locToken, locMessage, locParams);
        }

        protected String createFailedMessage(TaskId taskId, String locToken,
                                             String locMessage, Object... locParams) {
            return ComponentTaskResult.createFailedMessage(
                    getComponentName(), taskId, locToken, locMessage, locParams);
        }

        public String createSuccessMessage(TaskId taskId) {
            return ComponentTaskResult.createSuccessMessage(getComponentName(), taskId);
        }

        protected String getComponentName() {
            return "sun-xslt-engine";
        }
    }
}
