/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentTaskResultTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import com.sun.jbi.framework.XmlTests;


/**
 * Unit tests for ComponentTaskResult class.
 * @author kcbabo
 */
public class ComponentTaskResultTest extends XmlTests {
    
    private static final String COMPONENT_NAME = "sun-binding-component";
    
    private static final String TEST_ELEMENT = 
            "<component-task-result xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<component-name>" + COMPONENT_NAME + "</component-name>" +
                "<component-task-result-details>" +
                "<task-result-details></task-result-details> " +
                "</component-task-result-details>" +
             "</component-task-result>";
    
    public ComponentTaskResultTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }            

    /**
     * Test of getComponentName method, of class ComponentTaskResult.
     */
    public void testGetComponentName() throws Exception {
        ComponentTaskResult ctr = new ComponentTaskResult(
                stringToElement(TEST_ELEMENT));
        assertEquals(COMPONENT_NAME, ctr.getComponentName());
    }
    
    /**
     * Test of getTaskResultDetails method, of class ComponentTaskResult.
     */
    public void testGetTaskResultDetails() throws Exception {
        ComponentTaskResult ctr = new ComponentTaskResult(
                stringToElement(TEST_ELEMENT));
        assertNotNull(ctr.getTaskResultDetails());
    }
    
    /**
     * Test of setComponentName method, of class ComponentTaskResult.
     */
    public void testSetComponentName() throws Exception {
        ComponentTaskResult ctr = new ComponentTaskResult(
                createElement(TaskElem.component_task_result));
        ctr.setComponentName(COMPONENT_NAME);
        assertEquals(COMPONENT_NAME, ctr.getComponentName());
    }
}
