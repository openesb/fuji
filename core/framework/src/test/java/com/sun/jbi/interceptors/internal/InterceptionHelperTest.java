/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptionHelperTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.interceptors.internal;

import junit.framework.TestCase;
import java.util.Properties;

/**
 * Unit tests for Intereceptor Helper class.
 * @author nkaps
 */
public class InterceptionHelperTest extends TestCase {


    
    public InterceptionHelperTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
       
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test getInterceptorMethods method, of InterceptorWrapper
     */
    public void testCompareServiceNamesLocalPart() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/MyTestApp}outbound";
        String svcName2 = "outbound";
        
        assertTrue(InterceptionHelper.compareServiceNames(svcName1, svcName2));
        
    }
    
    public void testCompareServiceNamesQualifiedName() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/MyTestApp}outbound";
        String svcName2 = "{http://fuji.dev.java.net/application/MyTestApp}outbound";
        
        assertTrue(InterceptionHelper.compareServiceNames(svcName1, svcName2));
        
    }
    
    public void testCompareServiceNamesWithDifferentNS() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/MyDemoApp}outbound";
        String svcName2 = "{http://fuji.dev.java.net/application/MyTestApp}outbound";
        
        assertFalse(InterceptionHelper.compareServiceNames(svcName1, svcName2));
        
    }
    
    public void testCompareServiceNamesWithDifferentLocalPart() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/MyTestApp}inbound";
        String svcName2 = "{http://fuji.dev.java.net/application/MyTestApp}outbound";
        
        assertFalse(InterceptionHelper.compareServiceNames(svcName1, svcName2));
        
    }
    
    public void testCompareServiceNamesDiff() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/MyDemoApp}inbound";
        String svcName2 = "{http://fuji.dev.java.net/application/MyTestApp}outbound";
        
        assertFalse(InterceptionHelper.compareServiceNames(svcName1, svcName2)); 
    }
    
    public void testCompareServiceNamesWithRegex() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/MyDemoApp}inbound";
        String svcName2 = "{http://fuji.dev.java.net/application/My[a-z]*}in[a-z]*";
        
        assertFalse(InterceptionHelper.compareServiceNames(svcName1, svcName2)); 
    }

    public void testCompareServiceNamesWithNonAplha() throws Exception {
        
        String svcName1 = "{http://fuji.dev.java.net/application/M2DemoApp}split$1";
        String svcName2 = "{http://fuji.dev.java.net/application/M2DemoApp}split$1";
        
        assertTrue(InterceptionHelper.compareServiceNames(svcName1, svcName2)); 
    }
    
    public void testMatchWithEndpointLink() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "{http://fuji.dev.java.net/application/MyTestApp}inbound");
        intProps.setProperty(FilterProperty.ENDPOINT.toString(), "inbound_endpoint");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    }
    
    public void testMatchWithEndpoint() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "{http://fuji.dev.java.net/application/MyTestApp}outbound");
        intProps.setProperty(FilterProperty.ENDPOINT.toString(), "outbound_endpoint");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    }
    
    public void testMatchWithMixedEndpointInfo() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "{http://fuji.dev.java.net/application/MyTestApp}inbound");
        intProps.setProperty(FilterProperty.ENDPOINT.toString(), "outbound_endpoint");
        
        assertFalse(InterceptionHelper.match(exProps, intProps)); 
    }
    
    public void testMatchWithEndpointOnly() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.ENDPOINT.toString(), "outbound_endpoint");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    } 
    
    public void testMatchWithLinkedEndpointOnly() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.ENDPOINT.toString(), "inbound_endpoint");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    } 
    
    public void testMatchWithServiceNameOnly() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "outbound");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    } 
    
    public void testMatchWithServiceConnectionNameOnly() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "inbound");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    } 
    
        public void testMatchWithServiceQNameOnly() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "{http://fuji.dev.java.net/application/MyTestApp}outbound");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    } 
    
    public void testMatchWithServiceConnectionQNameOnly() throws Exception {
        
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.SERVICE.toString(), "{http://fuji.dev.java.net/application/MyTestApp}inbound");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    }
    
    public void testMatchProviderAndConsumer() throws Exception
    {
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.PROVIDER.toString(), "provider");
        intProps.setProperty(FilterProperty.CONSUMER.toString(), "consumer");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    }
    
        
    public void testDefaultExclusion() throws Exception
    {
        Properties exProps = getExchangeProperties(true);
        
        assertFalse(InterceptionHelper.match(exProps, new Properties())); 
    }
    
    /**
     * Test using regular expression in the service name
     * 
     * @throws java.lang.Exception
     */
    public void testMatchProvider() throws Exception
    {
        Properties exProps = getExchangeProperties(true);
        Properties intProps = new Properties();
        
        intProps.setProperty(FilterProperty.PROVIDER.toString(), "provi[a-z]*");
        
        assertTrue(InterceptionHelper.match(exProps, intProps)); 
    }
    
    /**
     * 
     * @param withLink - indicates that the link information is to be included.
     * @return the exchange properties
     */
    private Properties getExchangeProperties(boolean withLink)
    {
        Properties exProps = new Properties();
        
        exProps.setProperty(FilterProperty.SERVICE.toString(), "{http://fuji.dev.java.net/application/MyTestApp}outbound");
        exProps.setProperty(FilterProperty.ENDPOINT.toString(), "outbound_endpoint");
        
        if ( withLink )
        {
            exProps.setProperty(FilterProperty.SERVICE_CONNECTION.toString(), "{http://fuji.dev.java.net/application/MyTestApp}inbound");
            exProps.setProperty(FilterProperty.ENDPOINT_LINK.toString(), "inbound_endpoint");
        }
        
        exProps.setProperty(FilterProperty.PROVIDER.toString(), "provider");
        exProps.setProperty(FilterProperty.CONSUMER.toString(), "consumer");
        return exProps;
    }
}
