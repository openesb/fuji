/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiTaskResultTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.result;

import java.util.List;

import com.sun.jbi.framework.XmlTests;

/**
 * Unit tests for JbiTaskResult class.
 * @author kcbabo
 */
public class JbiTaskResultTest extends XmlTests {
    
    private static final String COMPONENT_NAME_1 = "sun-abc-binding";
    private static final String COMPONENT_NAME_2 = "sun-xyz-engine";
    private static final String COMPONENT_NAME_3 = "third-wheel-engine";
    
    private static final String TEST_ELEMENT = 
            "<jbi-task-result xmlns=\"" + ResultElement.NS_URI + "\">" + 
                "<frmwk-task-result></frmwk-task-result> " +
                "<component-task-result>" +
                   "<component-name>" + COMPONENT_NAME_1 + "</component-name>" +
                   "<component-task-result-details/>" +
                "</component-task-result>" +
                "<component-task-result>" +
                   "<component-name>" + COMPONENT_NAME_2 + "</component-name>" +
                   "<component-task-result-details/>" +
                "</component-task-result>" +
             "</jbi-task-result>";
    
    private static final String COMPONENT_RESULT =
                "<component-task-result xmlns=\"" + ResultElement.NS_URI + "\">" +
                   "<component-name>" + COMPONENT_NAME_3 + "</component-name>" +
                   "<component-task-result-details/>" +
                "</component-task-result>";
    
    public JbiTaskResultTest(String testName) {
        super(testName, MGMT_MSG_SCHEMA);
    }
    
    /**
     * Test of getFrameworkTaskResult method, of class JbiTaskResult.
     */
    public void testGetFrameworkTaskResult() throws Exception {
        JbiTaskResult jtr = new JbiTaskResult(
                stringToElement(TEST_ELEMENT));
        assertNotNull(jtr.getFrameworkTaskResult());
    }
    
    /**
     * Test of getComponentTaskResults method, of class JbiTaskResult.
     */
    public void testGetComponentTaskResults() throws Exception {
        JbiTaskResult jtr = new JbiTaskResult(
                stringToElement(TEST_ELEMENT));
        List<ComponentTaskResult> results = jtr.getComponentTaskResults();
        
        assertTrue(results.size() == 2);
        assertEquals(COMPONENT_NAME_1, results.get(0).getComponentName());
        assertEquals(COMPONENT_NAME_2, results.get(1).getComponentName());
    }
    
    /**
     * Test creating a new JbiTaskResult element from scratch.
     */
    public void testCreateNewJbiTaskResult() throws Exception {
        JbiTaskResult jtr = new JbiTaskResult(
                createElement(TaskElem.jbi_task_result));
        
        assertNotNull(jtr.getFrameworkTaskResult());
        assertTrue(jtr.getComponentTaskResults().size() == 0);
    }
    
    /**
     * Test of addComponentTaskResult method, of class JbiTaskResult.
     */
    public void testAddComponentTaskResult() throws Exception {
        JbiTaskResult jtr = new JbiTaskResult(
                createElement(TaskElem.jbi_task_result));
        
        jtr.addComponentTaskResult();
        assertTrue(jtr.getComponentTaskResults().size() == 1);
        jtr.addComponentTaskResult();
        assertTrue(jtr.getComponentTaskResults().size() == 2);
    }
    
    /**
     * Test adding an existing element to task result.
     */
    public void testAddComponentTaskResult2() throws Exception {
        JbiTaskResult jtr = new JbiTaskResult(
                createElement(TaskElem.jbi_task_result));
        // stick a dummy one in the result
        jtr.addComponentTaskResult();
        // now add the component response
        jtr.addComponentTaskResult(new ComponentTaskResult(
                stringToElement(COMPONENT_RESULT)));
        assertTrue(jtr.getComponentTaskResults().size() == 2);
    }
}
