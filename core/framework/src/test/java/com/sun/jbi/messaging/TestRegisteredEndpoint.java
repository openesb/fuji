/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRegisteredEndpoint.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import javax.xml.namespace.QName;
import org.w3c.dom.Element;
import org.w3c.dom.DocumentFragment;

/**
 * Tests for the RegisteredEndpoint class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestRegisteredEndpoint extends junit.framework.TestCase
{
    private final String OWNER_ID = "owner";
    
    private RegisteredEndpoint mEndpoint;
    private RegisteredEndpoint mEndpoint2;
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestRegisteredEndpoint(String aTestName)
    {
        super(aTestName);
        
        mEndpoint = new InternalEndpoint(
            WsdlDocument.HELLO_SERVICE_Q,
            WsdlDocument.HELLO_ENDPOINT_1,
            OWNER_ID);
        mEndpoint2 = new InternalEndpoint(
            WsdlDocument.HELLO_SERVICE_Q,
            WsdlDocument.HELLO_ENDPOINT_1,
            OWNER_ID);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();        
    }

// =============================  test methods ================================

    /**
     *  Sanity check constructor behavior.
     */
    public void testRegisteredEndpoint()
        throws Exception
    {
        assertEquals(mEndpoint.getServiceName(), WsdlDocument.HELLO_SERVICE_Q);
        assertEquals(mEndpoint.getEndpointName(), WsdlDocument.HELLO_ENDPOINT_1);
        assertEquals(mEndpoint.getOwnerId(), OWNER_ID);
	assertEquals(mEndpoint.hashCode(), mEndpoint2.hashCode());
	assertTrue(mEndpoint.equals(mEndpoint2));
    }
    
    /**
     *  Check to see if appropriate interfaces
     */
    public void testImplementsInterface()
        throws Exception
    {
        mEndpoint.parseDescriptor(WsdlDocument.readDefaultDocument(), null);
        assertTrue(mEndpoint.implementsInterface(WsdlDocument.HELLO_C_INTERFACE_Q));
        assertTrue(mEndpoint.implementsInterface(WsdlDocument.HELLO_W_INTERFACE_Q));
    }
    
    /**
     *  Sanity check constructor behavior.
     */
    public void testGetAsReference()
        throws Exception
    {
        DocumentFragment    frag;
        Element             root;
        String              service;
        String              endpoint;
        
        frag = mEndpoint.getAsReference(null);
        root = (Element)frag.getFirstChild();
        
        // verify service name (ignoring QName prefix)
        service = root.getAttributeNS(EndpointReference.NS_URI_JBI, 
                                      EndpointReference.ATTR_SERVICE_NAME);
        service = service.substring(service.indexOf(":") + 1);
        assertEquals(service, mEndpoint.getServiceName().getLocalPart());
                       
        // verify endpoint name
        endpoint = root.getAttributeNS(EndpointReference.NS_URI_JBI, 
                                       EndpointReference.ATTR_ENDPOINT_NAME);
        assertEquals(endpoint, mEndpoint.getEndpointName());
    }
   
}
