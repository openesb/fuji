/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRComponent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;


import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;


import org.w3c.dom.Document;

/** Abstract implementation of Component interface used by test components
 *  in the NMR regression suite.
 * @author Sun Microsystems, Inc.
 */
public class NMRComponent implements Component
{
    public static final int WSDL_11 = 1;
    public static final int WSDL_20 = 2;
    
    private int mType;
    private boolean mExchangeOkay;
    private Document mServiceDescription;
    
    public NMRComponent()
    {
        this(WSDL_20, false);
    }
    
    public NMRComponent(int type)
    {
        this(type, false);
    }
    
    public NMRComponent(int type, boolean isExchangeOkay)
    {
        mType           = type;
        mExchangeOkay   = isExchangeOkay;
    }
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle()
    {
        return null;
    }
        
    /** Default behavior is to return the WSDL found at DEFAULT_DOC_PATH.
     */
    public Document getServiceDescription(ServiceEndpoint endpoint) 
    {
        Document doc = null;
        
        try
        {
            // check to see if a specific service description has been set
            if (mServiceDescription != null) 
            {
                doc = mServiceDescription;
            }
            // looks like we're creating a default description
            else if (mType == WSDL_20)
            {
                doc = WsdlDocument.readDocument(WsdlDocument.WSDL_20_DEFINITION);
            }
            else if (mType == WSDL_11)
            {
                doc = WsdlDocument.readDocument(WsdlDocument.WSDL_11_DEFINITION);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
        return doc;
    }
    
    /**
     * Used to specify which service description document is returned from 
     * calls to getServiceDescription().
     * @param description WSDL document
     */
    public void setDescription(Document description) {
        mServiceDescription = description;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager() 
    {
        return null;
    }
    
    public boolean isExchangeWithConsumerOkay(
        ServiceEndpoint endpoint, 
        javax.jbi.messaging.MessageExchange exchange) 
    {
        return mExchangeOkay;
    }
    
    public boolean isExchangeWithProviderOkay(
        ServiceEndpoint endpoint, 
        javax.jbi.messaging.MessageExchange exchange) 
    {
        return mExchangeOkay;
    }
    public ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        return null;
    }
}
