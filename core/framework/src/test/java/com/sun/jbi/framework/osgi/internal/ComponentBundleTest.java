/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentBundleTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.test.osgi.TesterBundle;
import javax.jbi.component.Component;
import junit.framework.TestCase;

/**
 *
 * @author kcbabo
 */
public class ComponentBundleTest extends TestCase {
    
    private static final String DESCRIPTOR_PATH = "ComponentBundleTest/META-INF/jbi.xml";
    private static final String COMPONENT_NAME = "test-component";
    
    private ComponentBundle compBundle_;
    
    public ComponentBundleTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        compBundle_ = new ComponentBundle(Jbi.newJbi(
                getClass().getClassLoader().getResourceAsStream(DESCRIPTOR_PATH)),
                new TesterBundle());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getType method, of class ComponentBundle.
     */
    public void testGetType() {
        assertEquals(BundleType.COMPONENT, compBundle_.getType());
    }

    /**
     * Test of getName method, of class ComponentBundle.
     */
    public void testGetName() {
        assertEquals(COMPONENT_NAME, compBundle_.getName());
    }

    /**
     * Test of getComponentContext method, of class ComponentBundle.
     */
    public void testGetSetComponentContext() {
        assertNull(compBundle_.getComponentContext());
        ComponentContext cc = new ComponentContext(null, null, null);
        compBundle_.setComponentContext(cc);
        assertEquals(cc, compBundle_.getComponentContext());
    }

    /**
     * Test of getComponent method, of class ComponentBundle.
     */
    public void testGetComponent() throws Exception {
        Component comp = compBundle_.getComponent();
        assertNotNull(comp);
    }

}
