/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminServiceTest.java - Last published on 4/10/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal.jsr208;

import com.sun.jbi.framework.descriptor.Jbi;
import com.sun.jbi.framework.osgi.internal.ComponentBundle;
import com.sun.jbi.framework.osgi.internal.ComponentManager;
import com.sun.jbi.framework.osgi.internal.Environment;
import com.sun.jbi.framework.osgi.internal.FileUtil;
import com.sun.jbi.messaging.MessageService;
import com.sun.jbi.test.osgi.TestEnvironment;
import com.sun.jbi.test.osgi.TesterBundle;
import com.sun.jbi.test.osgi.TesterBundleContext;

import java.io.File;
import javax.management.ObjectName;

import junit.framework.TestCase;

/**
 * Unit tests for AdminService class.
 * @author mwhite
 */
public class AdminServiceTest extends TestCase {
    
    private static final String FRAMEWORK_DIR =
        "target/test-classes/AdminServiceTest/framework";
    private static final String BINDING_WORK_DIR =
        "target/test-classes/AdminServiceTest/tmp1";
    private static final String ENGINE_WORK_DIR =
        "target/test-classes/AdminServiceTest/tmp2";
    private static final String BINDING_DESCRIPTOR_PATH =
        "AdminServiceTest/binding/META-INF/jbi.xml";
    private static final String ENGINE_DESCRIPTOR_PATH =
        "AdminServiceTest/engine/META-INF/jbi.xml";
    
    private File bindingTestDir_;
    private File engineTestDir_;
    private File fwTestDir_;
    private TesterBundle fwTestBundle_;
    private TesterBundle bindingTestBundle_;
    private TesterBundle engineTestBundle_;
    private ComponentBundle bindingBundle_;
    private ComponentBundle engineBundle_;
    private ComponentManager compManager_;
    private TesterBundleContext bundleContext_;
    private AdminService adminService_;
    
    public AdminServiceTest(String testName) {
        super(testName);
        fwTestDir_ = new File(FRAMEWORK_DIR);
        bindingTestDir_ = new File(BINDING_WORK_DIR);
        engineTestDir_ = new File(ENGINE_WORK_DIR);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        // clean up, just in case we had a bad exit last time
        if (bindingTestDir_.exists()) {
            FileUtil.cleanDirectory(bindingTestDir_);
            bindingTestDir_.delete();
        }
        if (engineTestDir_.exists()) {
            FileUtil.cleanDirectory(engineTestDir_);
            engineTestDir_.delete();
        }
        if (fwTestDir_.exists()) {
            FileUtil.cleanDirectory(fwTestDir_);
            fwTestDir_.delete();
        }
        
        fwTestDir_.mkdirs();
        bindingTestDir_.mkdirs();
        engineTestDir_.mkdirs();
        
        adminService_ = new AdminService();
        compManager_ = new ComponentManager();
        bindingTestBundle_ = new TesterBundle();
        bindingTestBundle_.setBundleDir(bindingTestDir_);
        bindingBundle_ = new ComponentBundle(Jbi.newJbi(
            getClass().getClassLoader().getResourceAsStream(BINDING_DESCRIPTOR_PATH)),
            bindingTestBundle_);

        engineTestBundle_ = new TesterBundle();
        engineTestBundle_.setBundleDir(engineTestDir_);
        engineBundle_ = new ComponentBundle(Jbi.newJbi(
            getClass().getClassLoader().getResourceAsStream(ENGINE_DESCRIPTOR_PATH)),
            engineTestBundle_);
        
        fwTestBundle_ = new TesterBundle();
        fwTestBundle_.setBundleDir(fwTestDir_);

        bundleContext_ = new TesterBundleContext(fwTestBundle_);
        
        // initialize env
        TestEnvironment.setComponentManager(compManager_);
        TestEnvironment.setFrameworkBundleContext(bundleContext_);
        TestEnvironment.setMessageService(new MessageService());

        // register test components
        compManager_.registerComponent(bindingBundle_);
        compManager_.registerComponent(engineBundle_);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getBindingComponents method, of class AdminService.
     *
     * @throws Exception on error.
     */
    public void testGetBindingComponents() throws Exception {
        ObjectName[] on = adminService_.getBindingComponents();
        assertEquals(1, on.length);
        assertNotNull(on[0]);
        assertTrue(-1 < on[0].toString().indexOf("test-binding"));
    }

    /**
     * Test of getComponentByName method, of class AdminService.
     *
     * @throws Exception on error.
     */
    public void testGetComponentByName() throws Exception {
        // Returns null for non-existent component
        assertNull(adminService_.getComponentByName("nonexistent"));

        // Returns non-null for existing component, ObjectName contains
        // component name
        ObjectName on = adminService_.getComponentByName("test-binding");
        assertNotNull(on);
        assertTrue(-1 < on.toString().indexOf("test-binding"));
    }

    /**
     * Test of getEngineComponents method, of class AdminService.
     *
     * @throws Exception on error.
     *         installed service engines; must be non-null; may be empty.
     */
    public void testGetEngineComponents() throws Exception {
        ObjectName[] on = adminService_.getEngineComponents();
        assertEquals(1, on.length);
        assertNotNull(on[0]);
        assertTrue(-1 < on[0].toString().indexOf("test-engine"));
    }

    /**
     * Test of getSystemInfo method, of class AdminService.
     * 
     * @throws Exception on error.
     */
    public void testGetSystemInfo() throws Exception {
        assertNotNull(adminService_.getSystemInfo());
        assertTrue(adminService_.getSystemInfo().length() > 0);
    }

    /**
     * Test of getSystemService method, of class AdminService.
     *
     * @throws Exception on error.
     */
    public void testGetSystemService() throws Exception {
        // Returns null for non-existent service
        assertNull(adminService_.getSystemService("nonexistent"));

        // Returns non-null for existing system service, ObjectName contains
        // service name. However, at this point there are no system services
        // defined with a life cycle, so nothing is tested yet.
        // ObjectName on = adminService_.getComponentByName("future");
        // assertNotNull(on);
        //assertTrue(-1 < on.toString().indexOf("future"));
    }

    /**
     * Test of getSystemServices method, of class AdminService.
     *
     * @throws Exception on error.
     */
    public void testGetSystemServices() throws Exception {
        // Until system services are defined, this always returns an
        // empty array.
        ObjectName[] on = adminService_.getSystemServices();
        assertEquals(0, on.length);
    }

    /**
     * Test of isBinding method, of class AdminService.
     *
     * @throws Exception on error.
     */
    public void testIsBinding() throws Exception {
        assertTrue(adminService_.isBinding("test-binding"));
        assertFalse(adminService_.isBinding("test-engine"));
    }

    /**
     * Test of isEngine method, of AdminService class.
     *
     * @throws Exception on error.
     */
    public void testIsEngine() throws Exception {
        assertTrue(adminService_.isEngine("test-engine"));
        assertFalse(adminService_.isEngine("test-binding"));
    }
}
