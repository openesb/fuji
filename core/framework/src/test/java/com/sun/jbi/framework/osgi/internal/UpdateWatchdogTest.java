/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UpdateWatchdogTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.framework.osgi.internal;

import com.sun.jbi.test.osgi.TesterBundle;
import java.io.File;
import java.util.concurrent.ExecutorService;
import junit.framework.TestCase;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

/**
 *
 * @author kcbabo
 */
public class UpdateWatchdogTest extends TestCase {
    
    static final String BUNDLE_DIR = "target/test-classes/UpdateWatchdogTest/bundles";
    
    private UpdateWatchdog updateWatchdog_;
    
    public UpdateWatchdogTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        // create a new watchdog with a 200ms interval
        updateWatchdog_ = new UpdateWatchdog(200);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        // just in case
        updateWatchdog_.stop();
    }

    /**
     * Test of watch method, of class UpdateWatchdog.
     */
    public void testWatch() throws Exception  {
        UpdateBundle bundle = new UpdateBundle(true);
        updateWatchdog_.watch(bundle);
        assertTrue(bundle.getUpdateCount() == 0);
        bundle.getUpdateFile().setLastModified(System.currentTimeMillis() + 1000);
        waitFor(800);
        // make sure we updated
        assertTrue(bundle.getUpdateCount() == 1);
        waitFor(800);
        // make sure update isn't getting called repeatedly
        assertTrue(bundle.getUpdateCount() == 1);
        
    }
    
    
    /**
     * Test of watch method, of class UpdateWatchdog.
     */
    public void testWatchDisabled() throws Exception  {
        UpdateBundle bundle = new UpdateBundle(false);
        updateWatchdog_.watch(bundle);
        assertTrue(bundle.getUpdateCount() == 0);
        bundle.getUpdateFile().setLastModified(System.currentTimeMillis() + 1000);
        waitFor(800);
        // make sure we were *not* updated
        assertTrue(bundle.getUpdateCount() == 0);
    }

    /**
     * Test of unwatch method, of class UpdateWatchdog.
     */
    public void testUnwatch() throws Exception  {
        UpdateBundle bundle = new UpdateBundle(true);
        updateWatchdog_.watch(bundle);
        assertTrue(bundle.getUpdateCount() == 0);
        bundle.getUpdateFile().setLastModified(System.currentTimeMillis() + 1000);
        waitFor(800);
        // make sure we updated
        assertTrue(bundle.getUpdateCount() == 1);
        
        // now unwatch the bundle, update the timestamp, and make sure it's not
        // picked up by the watchdog
        updateWatchdog_.unwatch(bundle);
        bundle.getUpdateFile().setLastModified(System.currentTimeMillis() + 1000);
        waitFor(800);
        // make sure we updated
        assertTrue(bundle.getUpdateCount() == 1);
    }
    
    
    /**
     * Test to see if a deleted watched artifact causes issues.
     */
    public void testDeletedArtifact() throws Exception  {
        UpdateBundle bundle = new UpdateBundle(true);
        updateWatchdog_.watch(bundle);
        assertTrue(bundle.getUpdateFile().delete());
        // basically wait for something bad to happen
        waitFor(800);
    }

    /**
     * Test of start method, of class UpdateWatchdog.
     */
    public void testStart() throws Exception  {
        updateWatchdog_.start();
        // check to make sure we're started
        assertFalse(updateWatchdog_.getScheduler().isShutdown());
        
    }

    /**
     * Test of stop method, of class UpdateWatchdog.
     */
    public void testStop() throws Exception  {
        updateWatchdog_.start();
        waitFor(400);
        ExecutorService scheduler = updateWatchdog_.getScheduler();
        updateWatchdog_.stop();
        // check to make sure we're stopped
        assertTrue(scheduler.isShutdown());
        waitFor(800);
        assertTrue(scheduler.isTerminated());
    }

    /** Causes the current thread to wait for timeToWait milliseconds. */
    void waitFor(long timeToWait) {
        Object o = new Object();
        try {
            synchronized (o) {
                o.wait(timeToWait);
            }
        }
        catch (InterruptedException iex) {
            return;
        }
    }
}

class UpdateBundle extends TesterBundle {
    
    private File updateFile_;
    private int updateCount_;
    
    public UpdateBundle(boolean updateEnabled) throws Exception {
        super();
        File bundleDir = new File(
                UpdateWatchdogTest.BUNDLE_DIR + "/" + getBundleId());
        bundleDir.mkdirs();
        setBundleDir(bundleDir);
        updateFile_ = File.createTempFile("bundle", "jar", bundleDir);
        addHeader(UpdateWatchdog.DYNAMIC_UPDATE, String.valueOf(updateEnabled));
        addHeader(Constants.BUNDLE_UPDATELOCATION, updateFile_.toURL().toString());
    }
    
    @Override
    public void update() throws BundleException {
        ++updateCount_;
    }
    
    public int getUpdateCount() {
        return updateCount_;
    }
    
    public File getUpdateFile() {
        return updateFile_;
    }
}
