/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestExchangePattern.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.messaging;

import java.net.URI;

/**
 * The point of this test
 *
 * @author Sun Microsystems, Inc.
 */
public class TestExchangePattern extends junit.framework.TestCase
{    
    /** In Only MEP. */
    public static final String REF_IN_ONLY = 
        "http://www.w3.org/2004/08/wsdl/in-only";
    
    /** In Out MEP. */
    public static final String REF_IN_OUT =
        "http://www.w3.org/2004/08/wsdl/in-out";
    
    /** In Optional Out MEP. */
    public static final String REF_IN_OPTIONAL_OUT =
        "http://www.w3.org/2004/08/wsdl/in-opt-out";
    
    /** Robust In Only MEP. */
    public static final String REF_ROBUST_IN_ONLY =
        "http://www.w3.org/2004/08/wsdl/robust-in-only";
    
    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestExchangePattern(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. 
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
    }

// =============================  test methods ================================

    /**
     *  Test to make sure URI strings returned from match our reference values.
     */
    public void testExchangePatternStrings()
        throws Exception
    {
        assertEquals(ExchangePattern.IN_ONLY.toString(), REF_IN_ONLY);
        assertEquals(ExchangePattern.IN_OUT.toString(), REF_IN_OUT);
        assertEquals(ExchangePattern.IN_OPTIONAL_OUT.toString(), REF_IN_OPTIONAL_OUT);
        assertEquals(ExchangePattern.ROBUST_IN_ONLY.toString(), REF_ROBUST_IN_ONLY);
    }
    
    /**
     *  Test to make sure URIs returned from match our reference values.
     */
    public void testExchangePatternURIs()
        throws Exception
    {
        assertEquals(ExchangePattern.IN_ONLY.getURI(), new URI(REF_IN_ONLY));
        assertEquals(ExchangePattern.IN_OUT.getURI(), new URI(REF_IN_OUT));
        assertEquals(ExchangePattern.IN_OPTIONAL_OUT.getURI(), new URI(REF_IN_OPTIONAL_OUT));
        assertEquals(ExchangePattern.ROBUST_IN_ONLY.getURI(), new URI(REF_ROBUST_IN_ONLY));
    }
    
    
}
