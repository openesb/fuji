/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponent.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package ComponentManagerTest;

import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.InstallationContext;

/**
 *
 * @author kcbabo
 */
public class TestComponent extends com.sun.jbi.test.osgi.TesterComponent {
    
    public boolean cleanUpCalled;
    public boolean lifeCycleInitCalled;
    public boolean componentInitCalled;
    public boolean onInstallCalled;
    public boolean onUninstallCalled;
    public boolean shutDownCalled;
    public boolean startCalled;
    public boolean stopCalled;

    @Override
    public void cleanUp() throws JBIException {
        super.cleanUp();
        cleanUpCalled = true;
    }

    @Override
    public void init(InstallationContext arg0) throws JBIException {
        super.init(arg0);
        componentInitCalled = true;
    }

    @Override
    public void init(ComponentContext arg0) throws JBIException {
        super.init(arg0);
        lifeCycleInitCalled = true;
    }

    @Override
    public void onInstall() throws JBIException {
        super.onInstall();
        onInstallCalled = true;
    }

    @Override
    public void onUninstall() throws JBIException {
        super.onUninstall();
        onUninstallCalled = true;
    }

    @Override
    public void shutDown() throws JBIException {
        super.shutDown();
        shutDownCalled = true;
    }

    @Override
    public void start() throws JBIException {
        super.start();
        startCalled = true;
    }

    @Override
    public void stop() throws JBIException {
        super.stop();
        stopCalled = true;
    }
    
}
