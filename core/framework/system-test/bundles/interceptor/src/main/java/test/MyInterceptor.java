/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MyInterceptor.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package test;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.MessageExchange;

import com.sun.jbi.interceptors.Context;
import com.sun.jbi.interceptors.Intercept;

/**
 * @author Sun Microsystems
 *
 */
public class MyInterceptor {
    
        @Context
        private org.osgi.framework.BundleContext mCtx;
        
        @Context
        private Long mLong;
        
	/**
	 * 
	 */
	public MyInterceptor() 
        {
	}

        /**
	 * A read-only message exchange interceptor
	 * 
	 * @param exchange - MessageExchange
	 */
	@Intercept(priority=200, status="Active")
	public boolean anInterceptor(MessageExchange exchange) 
        {
            System.out.println("External Message Exchange Interceptor  [anInterceptor ] intercepted exchange :"
                    + exchange.toString());
            
            if ( mCtx != null )
            {
                System.out.println("Bundle context injected");
            }
            else
            {
                System.out.println("Bundle context not injected");
                return false;
            }
            
            if ( mLong != null )
            {
                System.out.println("Bundle context injected in a wrong type");
                return false;
            }
            
            return true;
            
        }
        
        /**
	 * A read-only message exchange interceptor
	 * 
	 * @param exchange - MessageExchange
	 */
	@Intercept(priority=201, status="Active")
	public boolean aReadOnlyMessageExchangeInterceptor(MessageExchange exchange) 
        {
            
            System.out.println("External Message Exchange Interceptor  [aReadOnlyMessageExchangeInterceptor ] intercepted exchange :"
                    + exchange.toString());

            NormalizedMessage msg = exchange.getMessage("in");

            if ( msg != null )
            {
                msg.setProperty("Fuji", "is the best");
            }
            return true;
            
        }
        
        /**
	 * A read-write message exchange interceptor
	 * 
	 * @param exchange
	 */
	@Intercept(priority=203, status="Active")
	public boolean aReadWriteOnlyMessageExchangeInterceptor(MessageExchange exchange) 
            throws Exception
        {
            System.out.println("External Message Exchange Interceptor  [aReadWriteOnlyMessageExchangeInterceptor ] intercepted exchange :"
                    + exchange.toString());

            NormalizedMessage msg = exchange.getMessage("in");

            if ( msg != null )
            {
                if ( !msg.getProperty("Fuji").equals("is the future") )
                {
                    throw new Exception("Message Property Fuji not set as expected, actual value : "
                            + msg.getProperty("Fuji") );
                }

                msg.setProperty("Fuji", "is the future");
            }
                            

            exchange.setStatus(ExchangeStatus.DONE );

            return false;
            
        }
        
        /**
	 * A read-only message exchange interceptor
	 * 
	 * @param exchange
	 */
	@Intercept(priority=202, status="Active")
	public void anotherReadOnlyMessageExchangeInterceptor(MessageExchange exchange) 
            throws Exception
        {
                    
            System.out.println("External Message Exchange Interceptor  [anotherReadOnlyMessageExchangeInterceptor ] intercepted exchange :"
                    + exchange.toString());
            
            NormalizedMessage msg = exchange.getMessage("in");

            if ( msg != null )
            {
                if ( !msg.getProperty("Fuji").equals("is the best") )
                {
                    throw new Exception("Message Property Fuji not set as expected, actual value : "
                                + msg.getProperty("Fuji") );
                };
                
                msg.setProperty("Fuji", "is the future");
            }
        }


}
