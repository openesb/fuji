/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponent1.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package test;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.JBIException;

import com.sun.jbi.fuji.component.base.BaseComponent;
import javax.jbi.management.DeploymentException;

/**
 * @author Sun Microsystems
 *
 */
public class TestComponent1 extends BaseComponent {
    
    private String COMP_START_FAIL     = "component.fail.start";
    private String COMP_INSTALL_FAIL   = "component.fail.install";
    
    // -- Component Life Cycle Operations 
    @Override 
    public void shutDown() throws JBIException {
    }

    @Override
    public void start() throws JBIException {
        
        boolean fail = Boolean.parseBoolean(System.getProperty(COMP_START_FAIL, "false"));
        if (fail)
        {
            throw new JBIException("Test component " + context_.getComponentName() + " failed to start. This failure is configured and is expected.");
        }
    }

    @Override
    public void stop() throws JBIException {

    }
    
    // -- Component Bootstrap stuff
    public void onInstall() throws JBIException {
        
        boolean fail = Boolean.parseBoolean(System.getProperty(COMP_INSTALL_FAIL, "false"));
        if (fail)
        {
            throw new JBIException("Failed to install test component " + context_.getComponentName() + "This failure is configured and is expected.");
        }        
    }
        
    // -- Service Assembly Life Cycle Operations
    @Override
    public void shutDown(String suName) throws DeploymentException {
        super.shutDown(suName);
    }

    @Override
    public void start(String suName) throws DeploymentException {
        super.start(suName);
        
    }

    @Override
    public void stop(String suName) throws DeploymentException {
        super.stop(suName);
    }
    
    // -- Service Unit Manager
    @Override
    public String deploy(String arg0, String arg1) throws DeploymentException {
        return super.deploy(arg0, arg1);
    }
    
    @Override   
    public void onUninstall() throws JBIException {
        System.out.println(" TestComponent onUninstall invoked ");
    }
    
    @Override   
    public void cleanUp() throws JBIException {
        System.out.println(" TestComponent cleanUp invoked ");
    }
}
