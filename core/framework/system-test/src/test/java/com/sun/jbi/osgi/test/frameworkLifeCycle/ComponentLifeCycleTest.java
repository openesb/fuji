/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLifeCycleTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.osgi.test.frameworkLifeCycle;

import org.osgi.framework.Bundle;


/**
 * Tests Message Exchange interception
 * @author nikita
 */
public class ComponentLifeCycleTest 
        extends com.sun.jbi.osgi.test.AbstractIntegrationTests
{
    private String testComponentPath = "target/bundles/test-component-1.jar";
    
    private String COMP_START_FAIL     = "component.fail.start";
    private String COMP_INSTALL_FAIL   = "component.fail.install";
    
    
    Bundle mComponent;
    
    public ComponentLifeCycleTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
     
        super.setUp();

    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    /**   
     * Test the case where a component fails to start, the state of the component
     * should be RESOLVED
     */ 
    public void testComponentStartupFailure() throws Exception { 
        
        // Configure the component to fail on start.
        System.setProperty(COMP_START_FAIL, "true");
        
        // Install the test interceptor bundle
        java.io.File file = new java.io.File(testComponentPath);
        mComponent  = felix_.getBundleContext().installBundle(file.toURL().toExternalForm());
        mComponent.start();
        
        assertEquals(mComponent.getState(),(Bundle.RESOLVED));
        mComponent.uninstall();
        System.setProperty(COMP_START_FAIL, "false");
    }
    
     
    /**
     * 
     * @throws java.lang.Exception
     *
    public void testComponentInstallFailure() throws Exception { 
        
        // Configure the component to fail on start.
        System.setProperty(COMP_INSTALL_FAIL, "true");
        
        // Install the test interceptor bundle
        java.io.File file = new java.io.File(testComponentPath);
        mComponent  = felix_.getBundleContext().installBundle(file.toURL().toExternalForm());

        assertEquals(null, mComponent);
        System.setProperty(COMP_INSTALL_FAIL, "false");
    }*/
}
