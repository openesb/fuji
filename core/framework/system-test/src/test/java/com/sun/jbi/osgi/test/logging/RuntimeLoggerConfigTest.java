/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeLoggerConfigTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.osgi.test.logging;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Bundle;
import java.util.Properties;
import org.apache.felix.cm.impl.ConfigurationAdminImpl;
import java.lang.reflect.Method;
import java.util.Dictionary;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Vector;

/**
 * Tests Runtime Logger Configuration and Component Logger Configuration
 */
public class RuntimeLoggerConfigTest
        extends com.sun.jbi.osgi.test.AbstractIntegrationTests
{
    
    /**
     *  test component
     */
    private String testComponnet = "target/bundles/logger-test-component.jar";
    
    /**
     * bundle for the test component
     */
    Bundle component_;
    
    /**
     * Constructor
     * @param testName
     */
    public RuntimeLoggerConfigTest(String testName) {
        super(testName);
    }
    
    /**
     * Setup
     */
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * teardown
     * @throws java.lang.Exception
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
       
    /**
     * This method tests listing of RuntimeLoggers
     * 
     * This method talks to the service
     * com.sun.jbi.configuration:runtime-logger-display
     * and asserts the pre-determined runtime loggers are listed
     * 
     * @throws java.lang.Exception
     */
    public void testRuntimeLogConfig() throws Exception {
        
        /*using reflection as junit gives a classcastexception
         The class returned - org.apache.felix.cm.impl.ConfigurationAdminImpl        
	Caused an ERROR
        org.apache.felix.cm.impl.ConfigurationAdminImpl
        java.lang.ClassCastException: org.apache.felix.cm.impl.ConfigurationAdminImpl */
        
        Object configAdmin = getConfigurationAdminService();
        Class configAdminClass = configAdmin.getClass();
        Method getConfigMthd = configAdminClass.getMethod("getConfiguration", 
                new Class[]{String.class, String.class});
        
        Object config = getConfigMthd.invoke(configAdmin, 
                "fuji-configuration.runtime-logger.runtime-logger", null);
        Class configClass = config.getClass();
        Method getPropsMthd = configClass.getMethod("getProperties");
        
        Dictionary props = (Dictionary)getPropsMthd.invoke(config, null);
        Vector<String> loggers = (Vector<String>)props.get("loggers");
        //1. check the default values
        assertTrue(loggers.contains("com.sun.jbi=INFO"));
        assertTrue(loggers.contains("com.sun.jbi.framework=DEFAULT"));
        assertTrue(loggers.contains("com.sun.jbi.fuji=DEFAULT"));
        
        //2. Change the values
        Vector<String> changedLoggers = new Vector<String>();
        changedLoggers.add("com.sun.jbi=FINE");
        changedLoggers.add("com.sun.jbi.framework=DEFAULT");
        changedLoggers.add("com.sun.jbi.fuji=DEFAULT");

        props.put("loggers", changedLoggers);
        Method updateMthd = configClass.getMethod("update", new Class[]{Dictionary.class});
        updateMthd.invoke(config, props);
        Thread.sleep(200); //wait for the async update call to finish
        
        //3.verify if the values changed in the display service
        config = getConfigMthd.invoke(configAdmin, 
                "fuji-configuration.runtime-logger-display.runtime-logger-display", null);
        configClass = config.getClass();
        getPropsMthd = configClass.getMethod("getProperties");
        
        props = (Dictionary)getPropsMthd.invoke(config, null);
        loggers = (Vector<String>)props.get("loggers");
        
        assertTrue(loggers.contains("com.sun.jbi=FINE"));
        assertTrue(loggers.contains("com.sun.jbi.framework=FINE"));
        assertTrue(loggers.contains("com.sun.jbi.fuji=FINE"));
        
        //3. Verify if the values actually changed
        assertEquals(Logger.getLogger("com.sun.jbi").getLevel(), Level.FINE);
        
        //disabling the following test for now. This leads to a permission issue inside junit
        //outside of junit env it works fine. - Needs more investigation
        /*java.lang.NullPointerException
	at org.apache.felix.cm.impl.ConfigurationAdminImpl.hasPermission(ConfigurationAdminImpl.java:158)
	at org.apache.felix.cm.impl.ConfigurationAdminImpl.checkPermission(ConfigurationAdminImpl.java:172)
	at org.apache.felix.cm.impl.ConfigurationAdminImpl.getConfiguration(ConfigurationAdminImpl.java:116)
	at com.sun.jbi.logging.RuntimeLoggerConfigurationService.updateAllComponentLoggerConfigurationServices(RuntimeLoggerConfigurationService.java:108)
	at com.sun.jbi.logging.RuntimeLoggerConfigurationService.updated(RuntimeLoggerConfigurationService.java:78)
	at org.apache.felix.cm.impl.ConfigurationManager$UpdateConfiguration.run(ConfigurationManager.java:1142)
	at org.apache.felix.cm.impl.UpdateThread.run(UpdateThread.java:90)
        
        //4. Verify if a change to the root logger impacted the component logger as expected
        assertEquals(Logger.getLogger("com.sun.jbi.test-binding-component").getLevel(), null);
        config = getConfigMthd.invoke(configAdmin, 
                "com.sun.jbi.configuration:test-binding-component-logger-display", null);
        configClass = config.getClass();
        getPropsMthd = configClass.getMethod("getProperties");       
        getPropsMthd = configClass.getMethod("getProperties");
        loggers = (Dictionary)getPropsMthd.invoke(config, null);
        assertEquals(loggers.get("com.sun.jbi.test-binding-component"), "FINE");*/

    }
    
     /**
     * This method is used to get a reference to the config admin service
     */
    private Object getConfigurationAdminService() {
        
        Object configAdmin = null;
        if (felix_.getBundleContext() != null) {
            ServiceReference  configAdminRef = 
               felix_.getBundleContext().getServiceReference(ConfigurationAdmin.class.getName());
 
            if (configAdminRef != null) {
                configAdmin = felix_.getBundleContext().getService(configAdminRef);  
            }
        }
        return configAdmin;
    }
    
}