/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractIntegrationTests.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.osgi.test;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import junit.framework.TestCase;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.util.FelixConstants;
import org.apache.felix.main.AutoActivator;
import org.codehaus.plexus.util.FileUtils;
import org.osgi.framework.Bundle;

/**
 *
 * @author kcbabo
 */
public abstract class AbstractIntegrationTests extends TestCase {

    // JBI Framework bundle name
    private static final String JBI_FRAMEWORK_BUNDLE_NAME = "org.glassfish.openesb.jbi-framework";
    // Config Admin Bundle Name
    private static final String CONFIG_ADMIN_BUNDLE_NAME = "org.apache.felix.configadmin";
    // Felix root dir
    private static final String FELIX_ROOT = "target/felix";
    // Felix cache config key
    private static final String FELIX_CACHE = "org.osgi.framework.storage";
    // FElix Cache Rootdir
    private static final String FELIX_CACHE_ROOTDIR = "felix.cache.rootdir";
    
    // Default cache dir name
    private static final String FELIX_CACHE_DIR_NAME = "cache";
    // We install all the bundles in this directory
    private static final String BUNDLE_DIR_NAME = "target/bundles";
    // Relative path to felix config file
    private static final String FELIX_CONFIG = "felix/conf/config.properties";
    
    protected File felixDir_ = new File(FELIX_ROOT);
    protected File cacheDir_;
    protected File bundlesDir_ = new File(BUNDLE_DIR_NAME);
    protected File configFile_ = new File("test-classes/", FELIX_CONFIG);
    
    // Felix OSGi runtime instance
    protected Felix felix_;
    
    protected Map<String, Bundle> bundles_ = new HashMap<String, Bundle>();
    
    protected AbstractIntegrationTests(String testName) {
        super(testName);
        cacheDir_ = new File(felixDir_, testName + "/" + FELIX_CACHE_DIR_NAME);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        cleanUp();
        createFelix();
        
        felix_.start();
        
        
        // install every bundle
        for (URL url : getBundlesToInstall()) {
            Bundle b = felix_.getBundleContext().installBundle(url.toString());
            bundles_.put(b.getSymbolicName(), b);
        }
        

        
        // Start the Config Admin Bundle
        if ( getConfigAdminBundle() != null )
        {
            getConfigAdminBundle().start();
        }
        
        getFrameworkBundle().start();        
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        
        // Stop and uninstall the config admin bundle
        if ( getConfigAdminBundle() != null )
        {
            getConfigAdminBundle().stop();
            getConfigAdminBundle().uninstall();
        }
        
        // Stop and uninstall the framework bundle
        if ( getFrameworkBundle() != null )
        {
            getFrameworkBundle().stop();
            getFrameworkBundle().uninstall();
        }
        
        //felix_.waitForStop(30000);
    }
    
    
    private void cleanUp() throws Exception {
        // Clean up felix install with every run
        if (cacheDir_.exists()) {
            FileUtils.deleteDirectory(cacheDir_);
            cacheDir_.mkdir();
        }
    }
    
    protected Bundle getFrameworkBundle() {
        return bundles_.get(JBI_FRAMEWORK_BUNDLE_NAME);
    }
    
    protected Bundle getConfigAdminBundle() {
        return bundles_.get(CONFIG_ADMIN_BUNDLE_NAME);
    }
    
    /**
     * Returns a list containing the absolute path to each bundle that should
     * be installed.
     * @return
     */
    protected List<URL> getBundlesToInstall() throws Exception {
        ArrayList<URL> list = new ArrayList<URL>();
        for (File f : bundlesDir_.listFiles()) {
            if (f.isFile()) {
                list.add(f.toURL());
            }
        }
        return list;
    }
    
    private void createFelix() throws Exception {
        
        // make our root directory
        felixDir_.mkdir();
        // init config
        Properties configProps = new Properties();
        configProps.load(getClass().getClassLoader().getResourceAsStream(FELIX_CONFIG));
        configProps.setProperty(FELIX_CACHE, cacheDir_.getAbsolutePath());
        configProps.setProperty(FELIX_CACHE_ROOTDIR, felixDir_.getAbsolutePath());

        //unset URLStreamHandlerFactory, which causes big problems in NB
        URL url = new URL("file://tmp");
        Field handlerFactory = url.getClass().getDeclaredField("factory");
        handlerFactory.setAccessible(true);
        handlerFactory.set(null, null);
        // ditto for URLConnection
        handlerFactory = URLConnection.class.getDeclaredField("factory");
        handlerFactory.setAccessible(true);
        handlerFactory.set(null, null);

        // Create a list for custom framework activators and
        // add an instance of the auto-activator it for processing
        // auto-install and auto-start properties. Add this list
        // to the configuration properties.
        List list = new ArrayList();
        list.add(new AutoActivator(configProps));
        configProps.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, list);

        /**
        List list = new ArrayList();
        list.add(new AutoActivator(configProps));
        // (8) Create a case-insensitive property map.
        Map configMap = new StringMap(configProps, false);
         * */
        // (9) Create an instance of the framework.
        felix_ = new Felix(configProps);
    }
}
