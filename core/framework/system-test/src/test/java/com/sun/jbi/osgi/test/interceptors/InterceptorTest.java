/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterceptorTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.osgi.test.interceptors;

import com.sun.jbi.interceptors.Intercept;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.NormalizedMessage;

import java.util.Properties;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;


/**
 * Tests Message Exchange interception
 * @author nikita
 */
public class InterceptorTest 
        extends com.sun.jbi.osgi.test.AbstractIntegrationTests
{
    private String testBundlePath = "target/bundles/test-interceptor.jar";
    
    Bundle mInterceptor;
    
    public InterceptorTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
     
        super.setUp();
        // Install the test interceptor bundle
        java.io.File file = new java.io.File(testBundlePath);
        mInterceptor  = felix_.getBundleContext().installBundle(file.toURL().toExternalForm());
        mInterceptor.start();
    }
    
    @Override
    protected void tearDown() throws Exception {
        
        mInterceptor.stop();
        super.tearDown();
    }
        
    public void testSimpleInterception() throws Exception {
        
        ServiceRegistration meInterceproteService;
        
        ServiceReference componentContext;
        
        ComponentContext componentCtx;
        DeliveryChannel  channel;
        ServiceEndpoint  endpoint;
        QName            serviceName = new QName("ProviderService"); 
        String           endpointName = "ProviderEndpoint"; 
        
        
        
        // Make sure we actually resolved and started
        assertTrue(getFrameworkBundle().getState() == Bundle.ACTIVE);
        

        
        // Get the ComponentContext
        ServiceReference serviceRef = 
                felix_.getBundleContext().getServiceReference(ComponentContext.class.getName());
        componentCtx = (ComponentContext)
                felix_.getBundleContext().getService(serviceRef);

        // activate an  endpoint as Provider
        channel = componentCtx.getDeliveryChannel();
        endpoint = componentCtx.activateEndpoint(serviceName, endpointName);
        
        
        // Send the ME and wait for a response
        ServiceEndpoint consumerEndpoint = componentCtx.getEndpoint(serviceName, endpointName);
        sendExchange(channel, endpoint);
        acceptExchange(channel);
        
        // Deactivate endpoint and close the channel
        componentCtx.deactivateEndpoint(endpoint);
        channel.close();
    }
  
     
    private void sendExchange(DeliveryChannel channel, ServiceEndpoint endpoint)
        throws Exception
    {
            NormalizedMessage   inMsg;
            InOnly              inOnly;
            MessageExchangeFactory     exchangeFactory;
            
            // create the exchange
            exchangeFactory = channel.createExchangeFactory(endpoint);
            inOnly  = exchangeFactory.createInOnlyExchange();
            inMsg   = inOnly.createMessage();
           
            // set some property
            inMsg.setProperty("Fuji", "Rocks");

           
            inOnly.setEndpoint(endpoint);
            inOnly.setOperation(new QName("hello"));
            
            // set message on exchange
            inOnly.setInMessage(inMsg);
            
            System.out.println("Consumer sending Message Exchange : \n" +
                        inOnly.toString() );
            
            // send the exchange
            channel.send(inOnly);
    }
    
    private void acceptExchange(DeliveryChannel channel)
        throws Exception
    {
            MessageExchange me = channel.accept();
            if ( me != null )
            {
                System.out.println("Consumer got message : " + me.toString());
                
                NormalizedMessage msg = me.getMessage("in");

                // Verify
                if ( msg != null )
                {
                    assertTrue(msg.getProperty("Fuji").equals("is the future"));

                    assertTrue(me.getStatus().equals(ExchangeStatus.DONE));
                }
            }
    }

}
