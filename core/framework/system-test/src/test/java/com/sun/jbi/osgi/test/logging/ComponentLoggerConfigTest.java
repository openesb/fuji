/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLoggerConfigTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.osgi.test.logging;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Bundle;
import java.util.Properties;
import java.util.Vector;
import org.apache.felix.cm.impl.ConfigurationAdminImpl;
import java.lang.reflect.Method;
import java.util.Dictionary;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.Enumeration;
/**
 * Tests Runtime Logger Configuration and Component Logger Configuration
 */
public class ComponentLoggerConfigTest
        extends com.sun.jbi.osgi.test.AbstractIntegrationTests
{
    
    
    /**
     * Constructor
     * @param testName
     */
    public ComponentLoggerConfigTest(String testName) {
        super(testName);
    }
    
    /**
     * Setup
     */
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    /**
     * teardown
     * @throws java.lang.Exception
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
       
    /**
     * This method tests listing of RuntimeLoggers
     * 
     * This method talks to the service
     * com.sun.jbi.configuration:test-binding-component-logger
     * and asserts the pre-determined runtime loggers are listed
     * 
     * @throws java.lang.Exception
     */
    public void testComponentLogConfig() throws Exception {
        
        /*using reflection as junit gives a classcastexception
         The class returned - org.apache.felix.cm.impl.ConfigurationAdminImpl        
	Caused an ERROR
        org.apache.felix.cm.impl.ConfigurationAdminImpl
        java.lang.ClassCastException: org.apache.felix.cm.impl.ConfigurationAdminImpl */
        
        Object configAdmin = getConfigurationAdminService();
        Class configAdminClass = configAdmin.getClass();
        Method getConfigMthd = configAdminClass.getMethod("getConfiguration", 
                new Class[]{String.class, String.class});
        
        Object config = getConfigMthd.invoke(configAdmin, 
                "fuji-configuration.component-logger.test-binding-component", null);
        Class configClass = config.getClass();
		System.out.println("=======================");
		System.out.println(configClass.getCanonicalName());
		System.out.println("=======================");
        Method getPropsMthd = configClass.getMethod("getProperties");
        
        Dictionary props = (Dictionary)getPropsMthd.invoke(config, null);
        Vector loggers = (Vector<String>)props.get("loggers");
        //1.Verify the default level
        assertTrue(loggers.contains("com.sun.jbi.test-binding-component=DEFAULT"));
        
               
        //2. Change the component logger level
        loggers.remove("com.sun.jbi.test-binding-component=DEFAULT");
        loggers.add("com.sun.jbi.test-binding-component=FINER");
        props.put("loggers", loggers);
        
        Method updateMthd = configClass.getMethod("update", new Class[]{Dictionary.class});
        updateMthd.invoke(config, props);
        Thread.sleep(200); //wait for the async update call to finish
        
        //3. Verify if the values changed in the display service
        config = getConfigMthd.invoke(configAdmin, 
                "fuji-configuration.component-logger-display.test-binding-component", null);
        configClass = config.getClass();
        getPropsMthd = configClass.getMethod("getProperties");
        
        props = (Dictionary)getPropsMthd.invoke(config, null);
        loggers = (Vector<String>)props.get("loggers");
        assertTrue(loggers.contains("com.sun.jbi.test-binding-component=FINER"));
        
        //4. Verify if the actual logger changed
        assertEquals(Logger.getLogger("com.sun.jbi.test-binding-component").getLevel(), Level.FINER);

    }
    
     /**
     * This method is used to get a reference to the config admin service
     */
    private Object getConfigurationAdminService() {
        
        Object configAdmin = null;
        if (felix_.getBundleContext() != null) {
            ServiceReference  configAdminRef = 
               felix_.getBundleContext().getServiceReference(ConfigurationAdmin.class.getName());
 
            if (configAdminRef != null) {
                configAdmin = felix_.getBundleContext().getService(configAdminRef);  
            }
        }
        return configAdmin;
    }
    
}