/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StartupShutdownTest.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.osgi.test.frameworkLifeCycle;

import java.io.File;
import javax.jbi.component.ComponentContext;
import org.osgi.framework.Bundle;


/**
 * Tests of full framework life cycle.  This test also covers automatic startup
 * and shutdown of components and service assemblies.
 * @author kcbabo
 */
public class StartupShutdownTest 
        extends com.sun.jbi.osgi.test.AbstractIntegrationTests {
    
    public StartupShutdownTest(String testName) {
        super(testName);
    }
    
    public void testEmptyFrameworkLifeCycle() throws Exception {
        // Get a handle to the framework and start it
        getFrameworkBundle().start();
        
        // Make sure we actually resolved and started
        assertTrue(getFrameworkBundle().getState() == Bundle.ACTIVE);
        
        // Sanity check for a service registered by the JBI framework
        assertNotNull(felix_.getBundleContext().getServiceReference(
                ComponentContext.class.getName()));
        
        // Stop the framework
        getFrameworkBundle().stop();
        
        // Make sure we actually stopped
        assertTrue(getFrameworkBundle().getState() == Bundle.RESOLVED);
        
        // No lingering services!
        assertNull(felix_.getBundleContext().getServiceReference(
                ComponentContext.class.getName()));
    }
    
    /*
    public void testReinstall() throws Exception {
        // Uninstall the framework bundle added in setup()
        getFrameworkBundle().uninstall();
        
        // Verify it's gone
        assertNull(getBundleBySymbolicName(FRAMEWORK_BUNDLE_NAME));
        
        // Reinstall, start, and stop
        Bundle newFwBundle = felix_.getBundleContext().installBundle(
                new File(FRAMEWORK_BUNDLE_PATH).toURL().toString());
        assertNotNull(getBundleBySymbolicName(FRAMEWORK_BUNDLE_NAME));
        newFwBundle.start();
        
        // Make sure we actually resolved and started
        assertTrue(newFwBundle.getState() == Bundle.ACTIVE);
        newFwBundle.stop();
    }
     */
}
