/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventInfoFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;


//import com.sun.jbi.bundle.proxy.jms.JMSConnectionManager;

import java.lang.reflect.Constructor;
import java.util.HashMap;

/**
 * Factory for different remote connection managers.
 * @author Sun Microsystems, Inc
 */
public class EventInfoFactory 
{
    /** 
     * ConnectionManagerFactory singleton.
     */  
    private static EventInfoFactory sCMF;
    private HashMap                 mEventInfo;
    
    /** 
     * Constructor
     * Private constructor to facilitate a singleton.
     */
    private EventInfoFactory() 
    {
        mEventInfo = new HashMap();
    }
    
    /**
     * Returns a singleton instance of the ConnectionManagerFactory class.
     * @return The instance of the ConnectionManagerFactory 
     */
    public static EventInfoFactory getInstance() 
    { 
        if (null == sCMF)
        {
            sCMF = new EventInfoFactory(); 
        }
        return sCMF;
    }
 
    public void registerEventInfo(Class implementingClass, String eventName)
            throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        try
        {
            Constructor c = implementingClass.getDeclaredConstructor(new Class[]  { Event.class });
            mEventInfo.put(eventName, c);
        }
        catch (java.lang.NoSuchMethodException nsmEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.ConnectionException(nsmEx);
        }
    }
    
    public EventInfo newInstance(Event e)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            Constructor c = (Constructor)mEventInfo.get(e.getEventName());
            if (c != null)
            {
                EventInfo   ei = (EventInfo)c.newInstance(new Object[] { e });
                return (ei);
            }
            return (null);
        }
        catch (java.lang.InstantiationException iEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(iEx);
        }
        catch (java.lang.IllegalAccessException iaEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(iaEx);
        }
        catch (java.lang.reflect.InvocationTargetException itEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(itEx);
        }
    }
}
