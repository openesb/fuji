/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSServerConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.gms;

import com.sun.enterprise.ee.cms.core.MessageSignal;

import com.sun.jbi.bundle.proxy.Localizer;

import com.sun.jbi.bundle.proxy.connection.ServerConnection;
import com.sun.jbi.bundle.proxy.connection.ClientConnection;

import java.util.concurrent.LinkedBlockingQueue;

import java.util.logging.Logger;
import java.util.logging.Level;


/**
 * Connection as a JMS Server.
 * @author Sun Microsystems, Inc
 */
public class GMSServerConnection 
        implements ServerConnection
{
    private Localizer               mLoc;
    private GMSConnectionManager    mGCM;
    private LinkedBlockingQueue     mMessageQueue;
    
    /** 
     * Creates a new instance of JMSServerConnection
     * @param cm that owns this server connection
     * @param session that should be used to create any JMS resources
     * @param instance id for this server connection.
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    GMSServerConnection(GMSConnectionManager cm) 
    {
        mGCM = cm;
        mLoc = Localizer.get();
        mMessageQueue = new LinkedBlockingQueue();
    }
    
    /**
     * Accept a new client connection.
     * @return The ClientConnection.
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problem.
     */
    public ClientConnection accept()
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        ClientConnection    cc= null;
        
        try
        {
            GMSMessage   m = (GMSMessage)mMessageQueue.take();
            
            cc = mGCM.getClientConnection(m);
            return (cc);
        }
        catch (java.lang.InterruptedException ieEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.ConnectionException(
                mLoc.t("0068: Exception accepting on ServerConnection: {0}", ieEx.toString()));
        }
    }
    
    
    void postMessage(MessageSignal    ms)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mMessageQueue.put(new GMSMessage(ms.getMemberToken(), ms.getMessage()));
        }
        catch (java.lang.InterruptedException ieEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ieEx);
        }
        
    }
    
    /**
     * Return the instance of the client.
     * @return The instanceId of the client.
     */
    public String getInstanceId()
    {
        return (mGCM.getInstanceId());
    }
}
