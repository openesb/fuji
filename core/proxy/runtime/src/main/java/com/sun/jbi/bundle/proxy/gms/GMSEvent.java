/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSEvent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.gms;

import com.sun.enterprise.ee.cms.core.MessageSignal;

import com.sun.jbi.bundle.proxy.RegistrationInfo;

import com.sun.jbi.bundle.proxy.connection.Event;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;

/**
 * Connection as a JMS Client.
 * @author Sun Microsystems, Inc
 */
public class GMSEvent
        implements Event
{
    private String                  mEventName;
    private String                  mSender;
    private ObjectInputStream       mOis;
    private ObjectOutputStream      mOos;
    private ByteArrayOutputStream   mBaos;
    private static final int        VERSION_1 = 1;
    
    GMSEvent(MessageSignal ms)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        byte[]      bytes = ms.getMessage();
        
        try
        {
            mOis = new ObjectInputStream(new ByteArrayInputStream(bytes));
            mSender = ms.getMemberToken();
            if (mOis.readInt() == VERSION_1)
            {
                mEventName = mOis.readUTF();
            }
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);            
        }
    }
    
    GMSEvent(String eventName)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mOos = new ObjectOutputStream(mBaos = new ByteArrayOutputStream());
            mOos.writeInt(VERSION_1);
            mOos.writeUTF(eventName);
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);                        
        }
    }
    
    public String getSender()
    {
        return (mSender);
    }
    
    public String getEventName()
    {
        return (mEventName);
    }
    
    public byte[] getMessage()
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mOos.close();
            return (mBaos.toByteArray());
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);                        
            
        }
    }
    
    public String getString()
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            return (mOis.readUTF());
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);
        }
    }
    
    public long getLong()
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            return (mOis.readLong());
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);
        }
    }
        
    public void putString(String value)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mOos.writeUTF(value);
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);            
        }
    }
    
    public void putLong(long value)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mOos.writeLong(value);
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);            
        }
    }

    public Object getObject()
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            ByteArrayInputStream    bais;
            ObjectInputStream       ois;
            int                     size;
            int                     offset = 0;
            byte[]                  bytes;
            Object                  object = null;
            
            size = mOis.readInt();
            bytes = new byte[size];
            while (offset < size)
            {
                int count = mOis.read(bytes, offset, size - offset);
                if (count == -1)
                {
                    break;
                }
                offset += count;
            }
            if (size != offset)
            {
                throw new java.io.IOException();                                                             
            }
            ois = new ObjectInputStream(bais = new ByteArrayInputStream(bytes));
            object = ois.readObject();
            ois.close();
            return (object);
        }
        catch (java.lang.ClassNotFoundException cnfEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(cnfEx);                                             
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);                                  
        }
    }
    
    public void putObject(Object value)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            ByteArrayOutputStream   baos;
            ObjectOutputStream      oos;
            byte[]                  bytes;
            
            oos = new ObjectOutputStream(baos = new ByteArrayOutputStream());
            oos.writeObject(value);
            oos.flush();
            oos.close();
            bytes = baos.toByteArray();
            mOos.writeInt(bytes.length);
            mOos.write(bytes);
        }
        catch (java.io.IOException ioEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(ioEx);                                
        }
    }
    
}
