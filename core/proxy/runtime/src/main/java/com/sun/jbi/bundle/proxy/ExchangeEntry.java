/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExchangeEntry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy;

import com.sun.jbi.bundle.proxy.connection.ClientConnection;

import java.net.URI;

import java.util.Date;

import java.text.SimpleDateFormat;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;

import javax.xml.namespace.QName;

/**
 * Records information about MessageExchanges that are in progress.
 * @author Sun Microsystems, Inc
 */
public class ExchangeEntry 
{
    private String              mId;
    private MessageExchange     mME;
    private MessageExchange     mRelatedME;
    private ClientConnection    mCC;
    private QName               mService;
    private String              mEndpoint;
    private QName               mOperation;
    private String              mConnection;
    private URI                 mPattern;
    private long                mTime;
    private boolean             mConsumer;
    private long                mBytesSent;
    private long                mBytesReceived;
    private int                 mMessagesSent;
    private int                 mMessagesReceived;
    private boolean             mFaulted;
    private ExchangeStatus      mMEStatus;
    private int                 mState;
    public final static int         STATE_FIRST = 1;
    public final static int         STATE_ONGOING = 2;
    public final static int         STATE_LAST = 3;
    private int                 mStatus;
    public final static int         STATUS_FIRSTSENT = 1;
    public final static int         STATUS_INSENT = 2;
    public final static int         STATUS_OUTSENT = 4;
    public final static int         STATUS_FAULTSENT = 8;
    public final static int         STATUS_ERRORSENT = 16;
    public final static int         STATUS_STATUSSENT = 32;
    
    public final static int       FIELD_SERVICE = 1;
    public final static int       FIELD_ENDPOINT = 2;
    public final static int       FIELD_SERVICE_AND_ENDPOINT = 3;
    public final static int       FIELD_OPERATION = 4;
    public final static int       FIELD_CONNECTION = 5;
    public final static int       FIELD_PATTERN = 6;
    public final static int       FIELD_STATUS = 7;
    public final static int       FIELD_BYTESSENT = 8;
    public final static int       FIELD_BYTESRECEIVED = 9;
    public final static int       FIELD_MESSAGESSENT = 10;
    public final static int       FIELD_MESSAGESRECEIVED = 11;
    public final static int       FIELD_DURATION = 12;
    
    /** Creates a new instance of ExchangeEntry */
    ExchangeEntry(String id, MessageExchange me, boolean consumer) 
    {
        mId = id;
        mME = me;
        mState = STATE_FIRST;
        mStatus = 0;
        mTime = System.currentTimeMillis();
        mOperation = me.getOperation();
        mService = me.getEndpoint().getServiceName();
        mEndpoint = me.getEndpoint().getEndpointName();
        mPattern = me.getPattern();
        mConsumer = consumer;
    }
    
    public MessageExchange getMessageExchange()
    {
        return (mME);
    }
    
    public void setClientConnection(ClientConnection cc)
    {
        mCC = cc;
        mConnection = mCC.getInstanceId();
    }
    
    ClientConnection getClientConnection()
    {
        return (mCC);
    }
    
    public void sendBytes(int bytes)
    {
        mBytesSent += bytes;
        mMessagesSent++;
    }
    
    public void receivedBytes(int bytes)
    {
        mBytesReceived += bytes;
        mMessagesReceived++;
    }
    
    public long getBytesSent()
    {
        return (mBytesSent);
    }
    
    public long getBytesReceived()
    {
        return (mBytesReceived);
    }
    
    public int getMessagesSent()
    {
        return (mMessagesSent);
    }
    
    public int getMessagesReceived()
    {
        return (mMessagesReceived);
    }
    
    public MessageExchange getRelatedExchange()
    {
        return (mRelatedME);
    }
    
    void setRelatedExchange(MessageExchange me)
    {
        mRelatedME = me;
    }
    
    void updateExchange(MessageExchange me)
    {
        mME = me;
    }
    
    QName getService()
    {
        return (mService);
    }
    
    QName getOperation()
    {
        return (mOperation);
    }
    
    long getTime()
    {
        return (mTime);
    }
    
    boolean isConsumer()
    {
        return (mConsumer);
    }
    
    boolean isFaulted()
    {
        boolean     fault = mFaulted;
        
        if (mME != null)
        {
            fault = mME.getFault() != null;
        }
        return (fault);
    }
    
    boolean isError()
    {
        ExchangeStatus      status = mMEStatus;
        
        if (mME != null)
        {
            status = mME.getStatus();
        }
        return (status == ExchangeStatus.ERROR);
    }
    
    boolean isDone()
    {
        ExchangeStatus      status = mMEStatus;
        
        if (mME != null)
        {
            status = mME.getStatus();
        }
        return (status == ExchangeStatus.DONE);
    }
    
    public void setState(int state)
    {
        if ((mState = state) == STATE_LAST)
        {
            mTime = System.currentTimeMillis() - mTime;
            mFaulted = mME.getFault() != null;
            mMEStatus = mME.getStatus();
            mME = null;
            mCC = null;
        }
    }
    
    public void setStatus(int status)
    {
        mStatus |= status;
    }
    
    public void setService(QName service)
    {
        mService = service;
    }
    
    public boolean checkStatus(int status)
    {
        return ((mStatus & status) != 0);
    }
    
    public boolean checkState(int state)
    {
        return (mState == state);
    }

    public Object getField(int name)
    {
        switch (name)
        {
            case FIELD_SERVICE:
                return (mService.toString());                
            case FIELD_ENDPOINT:
                return (mEndpoint);
            case FIELD_SERVICE_AND_ENDPOINT:
                return (mService.toString() + mEndpoint);
            case FIELD_OPERATION:
                return (mOperation.toString());
            case FIELD_CONNECTION:
                return (mConnection);
            case FIELD_PATTERN:
                return (mPattern);
            case FIELD_STATUS:
                return (mMEStatus.toString());                
            case FIELD_BYTESSENT:
                return (new Long(mBytesSent));
            case FIELD_BYTESRECEIVED:
                return (new Long(mBytesReceived));
            case FIELD_MESSAGESSENT:
                return (new Long(mMessagesSent));
            case FIELD_MESSAGESRECEIVED:
                return (new Long(mMessagesReceived));
            case FIELD_DURATION:
                return (new Long(mTime));
        }
        return (null);
    }
    
    public String getStringField(int name)
    {
        switch (name)
        {
            case FIELD_SERVICE:
                return (mService.toString());                
            case FIELD_ENDPOINT:
                return (mEndpoint);
            case FIELD_SERVICE_AND_ENDPOINT:
                return (mService.toString() + mEndpoint);
            case FIELD_OPERATION:
                return (mOperation.toString());
            case FIELD_CONNECTION:
                return (mConnection);
            case FIELD_PATTERN:
                return (mPattern.toString());
            case FIELD_STATUS:
                return (mMEStatus.toString());                
        }
        
        return (Integer.toString(name));
    }
    
    public long getLongField(int name)
    {
        switch (name)
        {
            case FIELD_BYTESSENT:
                return (mBytesSent);
            case FIELD_BYTESRECEIVED:
                return (mBytesReceived);
            case FIELD_MESSAGESSENT:
                return (mMessagesSent);
            case FIELD_MESSAGESRECEIVED:
                return (mMessagesReceived);
            case FIELD_DURATION:
                return (mTime);
        }
        return (0);
    }
    
    public static String fieldToString(int name)
    {
        switch (name)
        {
            case FIELD_SERVICE:
                return ("Service");
            case FIELD_ENDPOINT:
                return ("Endpoint");
            case FIELD_SERVICE_AND_ENDPOINT:
                return ("Service&Endpoint");
            case FIELD_OPERATION:
                return ("Operation");
            case FIELD_CONNECTION:
                return ("Connection");
            case FIELD_PATTERN:
                return ("Pattern");
            case FIELD_STATUS:
                return ("Status");
            case FIELD_BYTESSENT:
                return ("BytesSent       ");
            case FIELD_BYTESRECEIVED:
                return ("BytesReceived   ");
            case FIELD_MESSAGESSENT:
                return ("MessagesSent    ");
            case FIELD_MESSAGESRECEIVED:
                return ("MessagesReceived");
            case FIELD_DURATION:
                return ("Duration-ms     ");
        }
        return (Integer.toString(name));
    }
    
    public String toString()
    {
        StringBuilder        sb = new StringBuilder(200);
        
        sb.append("    MessageExchange Id(");
        sb.append(mId);
        sb.append(")\n      Pattern    (");
        sb.append(mPattern.toString());
        sb.append(")\n      Service    (");
        sb.append(mService.toString());
        sb.append(")\n      Endpoint   (");
        sb.append(mEndpoint);
        sb.append(")\n      Operation  (");
        sb.append(mOperation.toString());
        if (mRelatedME != null)
        {
            sb.append(")\n      RelatedId  (");
            sb.append(mRelatedME.getExchangeId());
        }
        sb.append(")\n      Status     (");
        sb.append(isDone() ? "DONE" : (isError() ? "ERROR" : "ACTIVE"));
        sb.append(")    Role(");
        sb.append(mConsumer ? "Consumer" : "Provider");
        sb.append(")    Faulted(");
        sb.append(isFaulted());
        sb.append(")\n      Messages   (");
        sb.append(mMessagesSent + mMessagesReceived);
        sb.append(")    Bytes Sent(");
        sb.append(mBytesSent);
        sb.append(") Received(");
        sb.append(mBytesReceived);
        sb.append(")\n      State      (");
        sb.append(mState == STATE_LAST ? "LAST" : (mState == STATE_FIRST ? "FIRST" : "ONGOING"));
        if (mState != STATE_LAST)
        {
            sb.append(")    StartTime  (");
            sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date(mTime)));
        }
        else
        {
            sb.append(")    LifeTimeMs (");
            sb.append(mTime);
        }
        sb.append(")\n      Connection (");
        sb.append(mConnection);
        sb.append(")\n");
        return (sb.toString());
    }
    
    
}
