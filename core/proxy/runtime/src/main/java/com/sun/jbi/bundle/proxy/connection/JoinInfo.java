/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JoinInfo.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

import java.util.LinkedList;

/**
 * Acknowledgement that a instance with the following endpoints has joined.
 * @author Sun Microsystems, Inc
 */
public class JoinInfo
        extends EventInfo
{
    private final String                    mInstanceId;
    private LinkedList<Object>              mEndpoints;
    public static final String              EVENTNAME = "Join";    
    
    /**
     * Constructor.
     */
    public JoinInfo(String instanceId, LinkedList<Object> eps)
    {
        mInstanceId = instanceId;
        mEndpoints = eps;
    }

    public String getEventName()
    {
        return (EVENTNAME);
    }    
    
    /**
     * Accessor for InstanceId.
     * @return String containing the InstanceId.
     */
    public String getInstanceId()
    {
        return (mInstanceId);
    }

    /**
     * Accessor for Endpoints.
     * @return String containing the InstanceId.
     */
    public LinkedList<Object> getEndpoints()
    {
        return (mEndpoints);
    }
    
    /**
     * Setter for Endpoints.
     * @return String containing the InstanceId.
     */
    public void setEndpoints(LinkedList eps)
    {
        mEndpoints = eps;
    }
    
    public JoinInfo(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        mInstanceId = event.getString();
        mEndpoints = (LinkedList)event.getObject();
    }
    
    public void encodeEvent(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        event.putString(mInstanceId);
        event.putObject(mEndpoints);
    }
}
