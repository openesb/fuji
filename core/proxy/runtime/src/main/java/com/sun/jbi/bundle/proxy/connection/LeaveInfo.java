/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LeaveInfo.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

/**
 * Represents a instance that leaves a Proxy group
 * @author Sun Microsystems, Inc
 */
public class LeaveInfo
        extends EventInfo
{
    private final String            mInstanceId;
    private final long              mDeathtime;
    
    public static final String      EVENTNAME = "Leave";    
    
    /**
     * Constructor.
     */
    public LeaveInfo(String instanceId, long deathtime)
    {
        mInstanceId = instanceId;
        mDeathtime = deathtime;
    }

    public String getEventName()
    {
        return (EVENTNAME);
    }
    
    /**
     * Accessor for Birthtime.
     * @return long containing the Birthtime.
     */
    public long getDeathtime()
    {
        return (mDeathtime);
    }
    
    /**
     * Accessor for InstanceId.
     * @return String containing the InstanceId.
     */
    public String getInstanceId()
    {
        return (mInstanceId);
    }

    public LeaveInfo(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        mInstanceId = event.getString();
        mDeathtime = event.getLong();
    }
    
    public void encodeEvent(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        event.putString(mInstanceId);
        event.putLong(mDeathtime);
    }
}
