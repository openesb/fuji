/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProxyBinding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy;

import com.sun.jbi.bundle.proxy.connection.ConnectionManager;
import com.sun.jbi.bundle.proxy.connection.ConnectionManagerFactory;

import com.sun.jbi.bundle.proxy.stats.Aggregate;
import com.sun.jbi.bundle.proxy.stats.Calculate;
import com.sun.jbi.bundle.proxy.stats.StatBase;

import com.sun.jbi.messaging.events.AbstractEvent;
import com.sun.jbi.messaging.events.EndpointEvent;
import com.sun.jbi.messaging.events.ServiceConnectionEvent;

import javax.jbi.component.ComponentContext;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import java.text.SimpleDateFormat;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

/**
 * Performs BC-related work for ProxyBinding.
 * @author Sun Microsystems, Inc
 */
public class ProxyBinding 
        implements BundleActivator
{
    /**
     * Out LifeCycle
     */
    private ComponentContext        mContext;
    
    /**
     * Our BUndle Context
     * 
     */
    private BundleContext           mBundle;
    
    /**
     * Managers the connection between ProxyBinding instances.
     */
    private ConnectionManager       mCM;
    
    /**
     * The connection to the local NMR.
     */
    private DeliveryChannel         mChannel;
    
    /**
     * Our endpoint for ProxyBinding exposed services.
     */
    private ServiceEndpoint         mService;
    
    /**
     * Thread that is the primary interface to the NMR.
     */
    private Thread                  mNMRThread;
    private NMRProcessor            mNMRProcessor;
    
    /**
     * Thread that is the primary interface to remote communications.
     */
    private Thread                  mRemoteThread;
    private RemoteProcessor         mRemoteProcessor;
    
    /**
     * Thread that is the primary interface to remote events.
     */
    private Thread                  mEventThread;
    private EventProcessor          mEventProcessor;
    
    /**
     * Set of active MessageExchanges.
     */
    private HashMap                 mExchanges;
    
    /**
     * Our logger.
     */
    private Logger                  mLog;
    
    /**
     * Our logger.
     */
    private Localizer               mLoc;

    /**
     * Remote endpoints that we proxy, and local endpoints that we can service.
     */
    private HashMap<RegistrationInfo, ServiceInfo>              mRemoteEndpoints;
    private HashMap<RegistrationInfo, RegistrationInfo>         mLocalEndpoints;
    private HashMap<ConnectionsInfo, ConnectionsInfo>           mLocalConnections;
    private HashMap<ConnectionsInfo, ConnectionsInfo>           mRemoteConnections;
    private long                                               mSerialNumber;
    
    /**
     * Our unique instance identifier.
     */
    private String                  mId;
    
    /**
     * Our current run state.
     */
    private boolean                 mRunning;
    private boolean                 mStarted;
    private Exception               mStartException;
    
    /**
     * The name of our service.
     */
    static final String             SUNPROXYBINDINGSERVICE = "SunProxyBindingBundle";
    String                          mBundleId;
    
    // internal stats tree
    private StatBase                mStatistics;
    
    /**
     * Basic statistics collection counters.
     */
    private long                    mExchangesSent;
    private long                    mExchangesReceived;
    private long                    mBytesSent;
    private long                    mBytesReceived;
    private long                    mMessagesSent;
    private long                    mMessagesReceived;
    private long                    mFaults;
    private long                    mError;
    private long                    mDone;
   
    /**
     * Constructor for a ProxyBinding.
     * @param channel that should be used.
     * @param instanceId to be assigned.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    public ProxyBinding()
        throws javax.jbi.JBIException
    {
        mExchanges = new HashMap();
        mRemoteEndpoints = new HashMap();
        mLocalEndpoints = new HashMap();
        mLocalConnections = new HashMap();
        mRemoteConnections = new HashMap();
        try
        {
            mId = java.net.InetAddress.getLocalHost().getCanonicalHostName();
        }
        catch (Exception ignore)
        {
            mId = "localhost";
        }
        mId = mId + "$" +  java.lang.management.ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
        mId = mId.replace(".", "_");
        mId = mId.replace("-", "_");
    }
    
    /**
     * Start the ProxyBinding machinery. This involves starting the three threads that 
     * listen for NMR traffic, Communications traffic and Events.
     */
    public void start(BundleContext bc)
            throws Exception
    {
        StatBase            sb;
        StatBase            sb2;
        
        mBundle = bc;
        mBundleId = String.valueOf(bc.getBundle().getBundleId());
        
        // Use the service registry to obtain a ComponentContext reference
        ServiceReference serviceRef = bc.getServiceReference("javax.jbi.component.ComponentContext");

        // Cast the service reference into the contract we expect
        mContext = (ComponentContext)bc.getService(serviceRef);
        mLog = getLogger("base");
        mLoc = Localizer.get();
        mLog.info(mLoc.t("0041: Proxy binding Id({0})\n", mId));
        mChannel = mContext.getDeliveryChannel();
        mCM = ConnectionManagerFactory.getInstance(mContext.getNamingContext()).getConnectionManager(mId);
        mService = mContext.activateEndpoint(new QName(SUNPROXYBINDINGSERVICE), mId);  

        //
        //  Setup statistics collection aggregated by Service and Operation.
        //
        mStatistics = new Aggregate(ExchangeEntry.FIELD_SERVICE);
        mStatistics.addChild(sb2 = sb = new Aggregate(ExchangeEntry.FIELD_OPERATION));
        sb.addChild(sb = new Calculate(ExchangeEntry.FIELD_BYTESSENT));
        sb.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESRECEIVED));
        sb.addPeer(new Calculate(ExchangeEntry.FIELD_DURATION, Calculate.MIN | Calculate.MAX | Calculate.AVG | Calculate.STD));
        sb2.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESSENT));
        sb.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESRECEIVED));
        sb.addPeer(new Calculate(ExchangeEntry.FIELD_DURATION, Calculate.MIN | Calculate.MAX | Calculate.AVG | Calculate.STD));
        mStatistics.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESSENT));
        sb.addPeer(sb = new Calculate(ExchangeEntry.FIELD_BYTESRECEIVED));
        sb.addPeer(new Calculate(ExchangeEntry.FIELD_DURATION, Calculate.MIN | Calculate.MAX | Calculate.AVG | Calculate.STD));
        try
        {
            mEventProcessor = new EventProcessor(this);
            mNMRProcessor = new NMRProcessor(this);
            mRemoteProcessor = new RemoteProcessor(this);
        }
        catch (com.sun.jbi.bundle.proxy.connection.ConnectionException cEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0042: PB:ProxyBinding start ConnectionException: {0}",
                            cEx.getMessage()), cEx);
            return;            
        }
        
        mRunning = true;
        mEventThread = new Thread (mEventProcessor);
        mEventThread.setName("JBI-ProxyBinding-EventProcessor");
        mEventThread.setDaemon(true);
        mEventThread.start();
        mNMRThread = new Thread (mNMRProcessor);
        mNMRThread.setName("JBI-ProxyBinding-NMRProcessor");
        mNMRThread.setDaemon(true);
        mNMRThread.start();
        mRemoteThread = new Thread (mRemoteProcessor);
        mRemoteThread.setName("JBI-ProxyBinding-RemoteProcessor");
        mRemoteThread.setDaemon(true);
        mRemoteThread.start();
        
//        //
//        //  Wait for the event processor to resolve membership.
//        //
//        synchronized (this)
//        {
//            if (!mStarted)
//            {
//                try
//                {
//                    this.wait();
//                    if (!mStarted)
//                    {
//                        stop();
//                        throw mStartException;
//                    }
//                }
//                catch (java.lang.InterruptedException iEx)
//                {
//
//                }
//            }
//        }
	
    }
    
    synchronized void isStarted()
    {
        //
        //  Wait for the event processor to resolve membership.
        //
        synchronized (this)
        {
            while (mRunning && !mStarted)
            {
                try
                {
                    this.wait();
                }
                catch (java.lang.InterruptedException iEx)
                {
                    break;
                }
            }
        
        }
    }
    //
    //  Used to signal start() that it can complete.
    //
    synchronized void startComplete(Exception e)
    {
        mStarted = e == null;
        mStartException = e;
        if (e != null)
        {
            stop();
        }
        this.notifyAll();
    }
    
    /**
     * Stop the ProxyBinding machinery. This involves getting the NMR and Communications threads
     * to terminate.
     */
    public void stop(BundleContext bc)
    {
        stop();
    }
    
    synchronized public void stop()
    {
        try
        {
            if (mRunning)
            {
                mLog.info(this.toString());
                mRunning = false;
                ((com.sun.jbi.ext.DeliveryChannel)mChannel).addTimeoutListener(null);
                mContext.deactivateEndpoint(mService);  
                mNMRProcessor.stop();
                mNMRThread.interrupt();
                mRemoteProcessor.stop();
                mRemoteThread.interrupt();
                mEventProcessor.stop();
                mEventThread.interrupt();
                mCM.closeServerConnection();
                mCM.closeClientConnection();
                mCM.closeEventConnection();
                mCM.stop();
                mChannel.close();
                mLog.info(this.toString());
            }
        }
        catch (Exception ignore)
        {
            
        }
    }
 
//
// ------------------------ Accessors for interesting info  -----------------------
//   
    
    BundleContext   getBundleContext()
    {
        return (mBundle);
    }
    
    ConnectionManager getConnectionManager()
    {
        return (mCM);
    }
    
    public DeliveryChannel getDeliveryChannel()
    {
        return (mChannel);
    }

    ServiceEndpoint getService()
    {
        return (mService);
    }
    
    Document getServiceDescription(ServiceEndpoint endpoint)
    {
        return (mNMRProcessor.getDescription(endpoint));
    }
    
    ServiceEndpoint resolveEndpointReference(DocumentFragment document)
    {
        //
        //  We may be entered via recursion involving multiple instances. To avoid
        //  this we check to see if the source of the request is the NMRProcessor thread.
        //  This would only be true if the NMRProcessor if currently handling a remote 
        //  resolveEndpointReference.
        //
        if (!Thread.currentThread().equals(mNMRThread))
        {
            return (mNMRProcessor.resolveEndpointReference(document));
        }
        return (null);
            
    }
    
    boolean isExchangeWithConsumerOkay(ServiceEndpoint endpoint, javax.jbi.messaging.MessageExchange exchange)
    {
        return (mNMRProcessor.isExchangeOkay(endpoint, exchange));
    }
    
    ComponentContext getComponentContext()
    {
        return (mContext);
    }
    
    public Logger getLogger(String suffix)
    {
        try
        {
            return (mContext.getLogger(suffix, null));
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            return (Logger.getLogger("com.sun.jbi.component.SunProxyBinding." + suffix));
        }
    }
    
    String  getInstanceId()
    {
        return (mId);
    }
    
    HashMap getInstances()
    
    {
        return (mEventProcessor.getInstances());
    }
    
//
// ------------------------ Methods used for tracking state of Exchanges -----------------------
//   

    synchronized public ExchangeEntry trackExchange(String id, MessageExchange me, boolean consumer)
    {
        ExchangeEntry   ee;
        
        mExchanges.put(id, ee = new ExchangeEntry(id, me, consumer));        
        return (ee);
    }

    synchronized public ExchangeEntry getExchangeEntry(String id)
    {       
        return ((ExchangeEntry)mExchanges.get(id));
    }
        
    void purgeExchange(String id)
    {
        ExchangeEntry   ee;
        boolean         isLast = false;
        
        synchronized (this)
        {
            ee = (ExchangeEntry)mExchanges.get(id);
            if (ee != null)
            {
		ServiceEndpoint ep = ee.getMessageExchange().getEndpoint();
                
                if (ep != null)
                {
                    returnInstanceForEndpoint(ep, ee.getClientConnection().getInstanceId());
                }
                ee.setState(ExchangeEntry.STATE_LAST);
                mExchanges.remove(id);
                isLast = true;
            }
        }
        
        if (isLast)
        {
            updateStatistics(ee);
        }
    }
    
//
// ------------------------ Methods for tracking remote RegistrationInfo ----------------
//      
   
    synchronized void processRemoteEndpoint(RegistrationInfo ri)
        throws javax.jbi.JBIException
    {
        ServiceInfo         si = (ServiceInfo)mRemoteEndpoints.get(ri);
        EndpointEvent       e = ri.getEndpoint();
        boolean             add = ri.getAction().equals(RegistrationInfo.ACTION_ADD);

        mSerialNumber++;
        ri.setSerial(mSerialNumber);
        ((com.sun.jbi.ext.DeliveryChannel)mChannel).handleEndpointEvent(e);
        if (e.getAction() == AbstractEvent.ACTION.ADDED)
        {
            mLog.fine("PB:receive add registration (" +
                ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
        }
        else
        {
            mLog.fine("PB:receive remove registration (" +
                ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
        }
        if (add)
        {
            if (si == null)
            {
                si = new ServiceInfo(e);
            }
            si.addInstance(ri);
            mRemoteEndpoints.put(ri, si);
        }
        else
        {
            if (si != null)
            {
                if (si.removeInstance(ri.getInstanceId()))
                {
                    mRemoteEndpoints.remove(ri);
                }
            }            
        }
    }
    
    synchronized void purgeRemoteEndpointsForInstance(String instanceId)
    {
        mSerialNumber++;
        for (Iterator i = mRemoteEndpoints.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry      m = (Map.Entry)i.next();
            ServiceInfo    si = (ServiceInfo)m.getValue();
            
            if (si.removeInstance(instanceId))
            {
                try
                {
                    ((com.sun.jbi.ext.DeliveryChannel)mChannel)
                        .handleEndpointEvent(si.getEndpoint().getOpposite());
                }
                catch (javax.jbi.JBIException jEx)
                {
                    
                }
                i.remove();                
            }
        }
    }
        
    synchronized String getInstanceForEndpoint(ServiceEndpoint se)
    {
        RegistrationInfo        info = new RegistrationInfo(se.getServiceName(), se.getEndpointName(), "", "");
        ServiceInfo             si;
        String                  id = null;
        
        si = (ServiceInfo)mRemoteEndpoints.get(info);
        if (si != null)
        {
            id = si.getInstance();
        }
        return (id);
    }

    synchronized void returnInstanceForEndpoint(ServiceEndpoint se, String instance)
    {
        RegistrationInfo        info = new RegistrationInfo(se.getServiceName(), se.getEndpointName(), "", "");
        ServiceInfo             si;
        
        si = (ServiceInfo)mRemoteEndpoints.get(info);
        if (si != null)
        {
            si.returnInstance(instance);
        }       
    }
    
//
// ------------------------ Methods for tracking remote ConnectionInfo ----------------
//      
    
    synchronized void processRemoteConnection(ConnectionsInfo ci)
        throws javax.jbi.JBIException
    {
        ServiceConnectionEvent      e = ci.getEndpoint();
        boolean                     add = ci.getAction().equals(ConnectionsInfo.ACTION_ADD);
        
        mSerialNumber++;
        ci.setSerial(mSerialNumber);
        ((com.sun.jbi.ext.DeliveryChannel)mChannel).handleEndpointEvent(ci.getEndpoint());
        if (add)
        {
            mLog.fine("PB:receive add service connection (" + 
                ci.getServiceName() + "," + ci.getEndpointName() + "," +
                ci.getServiceLinkName() + "," + ci.getEndpointLinkName() + ")");
        }
        else
        {
            mLog.fine("PB:receive remove service connection (" +
                ci.getServiceName() + "," + ci.getEndpointName() + "," +
                ci.getServiceLinkName() + "," + ci.getEndpointLinkName() + ")");
        }
        if (add)
        {
            mRemoteConnections.put(ci, ci);
        }
        else
        {
            mRemoteConnections.remove(ci);            
        }
    }
    
    synchronized void purgeRemoteConnectionsForInstance(String instanceId)
        throws javax.jbi.JBIException
    {
        mSerialNumber++;
        for (Iterator i = mRemoteConnections.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry           m = (Map.Entry)i.next();
            ConnectionsInfo     ci = (ConnectionsInfo)m.getValue();
            
            if (ci.getInstanceId().equals(instanceId))
            {
                ((com.sun.jbi.ext.DeliveryChannel)mChannel)
                    .handleEndpointEvent(ci.getEndpoint().getOpposite());
                i.remove();                
            }
        }
    }
//
// ------------------------ Methods operating on local RegistrationInfo -----------------------
//
    /** 
     * Locally process an EndpointEvent
     * @param endpoint to be removed from Registry.
     * @return RegistrationInfo created.
     * @throws javax.jbi.JBIException for any JMS problems.
     */
    public void handleEndpointEvent(AbstractEvent endpoint)
//        throws javax.jbi.JBIException
    {        
        mSerialNumber++;
	if (endpoint instanceof ServiceConnectionEvent)
        {
	    ServiceConnectionEvent	sce = (ServiceConnectionEvent)endpoint;
            ConnectionsInfo ci;
        
            ci = new ConnectionsInfo(sce, mCM.getInstanceId());  
            ci.setSerial(mSerialNumber);
            if (endpoint.getAction() == AbstractEvent.ACTION.ADDED)
            {
                mLocalConnections.put(ci, ci);
                mLog.fine("PB:send add service connection (" + 
                    sce.getFromServiceName() + "," + sce.getFromEndpointName() + "," +
                    sce.getToServiceName() + "," + sce.getToEndpointName() +
                    "," + sce.getType() + ")");
            }
            else
            {
                mLocalConnections.remove(ci);
                mLog.fine("PB:send remove service connection (" +
                    sce.getFromServiceName() + "," + sce.getFromEndpointName() + "," +
                    sce.getToServiceName() + "," + sce.getToEndpointName() + ")");               
            }
            mEventProcessor.sendEvent(ci);
        }
	else
        {
	    EndpointEvent   ee = (EndpointEvent)endpoint;
            RegistrationInfo ri;
        
            ri = new RegistrationInfo(ee, mCM.getInstanceId());
            ri.setSerial(mSerialNumber);
            if (endpoint.getAction() == AbstractEvent.ACTION.ADDED)
            {
                mLocalEndpoints.put(ri, ri);
                mLog.fine("PB:send add registration (" +
                    ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
            }
            else
            {
                mLocalEndpoints.remove(ri);
                mLog.fine("PB:send remove registration (" +
                    ri.getServiceName() + "," + ri.getEndpointName() + "," + ri.getInstanceId() + ")");
            }
	    mEventProcessor.sendEvent(ri);
	}
    }
    
    synchronized LinkedList<Object> getLocalEndpoints()
    {
        LinkedList<Object>          ll = new LinkedList();
        
        for (RegistrationInfo ri : mLocalEndpoints.values())
        {
            ll.add(ri);
        }
        for (ConnectionsInfo ci : mLocalConnections.values())
        {
            ll.add(ci);
        }
        ll.add(Long.valueOf(mSerialNumber));
        return (ll);
    }
    
//
// ------------------- Methods for statistics collection/display  -------------------
//
    
    private void updateStatistics(ExchangeEntry ee)
    {
        mBytesSent += ee.getBytesSent();
        mBytesReceived += ee.getBytesReceived();
        mMessagesSent +=  ee.getMessagesSent();
        mMessagesReceived += ee.getMessagesReceived();
        if (ee.isConsumer())
        {
            mExchangesSent++;
        }
        else
        {
            mExchangesReceived++;
        }
        if (ee.isFaulted())
        {
            mFaults++;
	}
        if (ee.isDone())
        {
            mDone++;
	}
        if (ee.isError())
        {
            mError++;
	}

        // update internal stats
        if (mStatistics != null)
        {
            mStatistics.apply(ee);
        }
    }


//
// ------------------------ Methods for debugging/information  -----------------------
//
        
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        
        sb.append("ProxyBinding state at ");
        sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date(System.currentTimeMillis())));
        sb.append("\n");
        sb.append(mEventProcessor.toString());
        sb.append("  Exchanges          Sent(");
        sb.append(mExchangesSent);
        sb.append(")  Received(");
        sb.append(mExchangesReceived);
        sb.append(")\n");
        sb.append("  Messages           Sent(");
        sb.append(mMessagesSent);
        sb.append(")  Received(");
        sb.append(mMessagesReceived);
        sb.append(")\n");
        sb.append("  Bytes              Sent(");
        sb.append(mBytesSent);
        sb.append(")  Received(");
        sb.append(mBytesReceived);
        sb.append(")\n");
        sb.append("  Result             DONE(");
        sb.append(mDone);
        sb.append(")  ERROR(");
        sb.append(mError);
        sb.append(")  Faulted(");
        sb.append(mFaults);
        sb.append(")\n  Internal Exchanges Sent(");
        sb.append(mNMRProcessor.mOperationsSent);
        sb.append(")  Received(");
        sb.append(mNMRProcessor.mOperationsReceived);
        sb.append(")\n");
        sb.append("  Active Exchanges   Count(");
        sb.append(mExchanges.size());
        sb.append(")\n");
        for (Iterator i = mExchanges.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Local Services Count(");
        sb.append(mLocalEndpoints.size());
        sb.append(")\n");
        for (Iterator i = mLocalEndpoints.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Local Service Connections Count(");
        sb.append(mLocalConnections.size());
        sb.append(")\n");
        for (Iterator i = mLocalConnections.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Remote Services Count(");
        sb.append(mRemoteEndpoints.size());
        sb.append(")\n");
        for (Iterator i = mRemoteEndpoints.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        sb.append("  Remote Service Connections Count(");
        sb.append(mRemoteConnections.size());
        sb.append(")\n");
        for (Iterator i = mRemoteConnections.values().iterator(); i.hasNext();)
        {
            sb.append(i.next().toString());
        }
        if (mStatistics != null)
        {
            sb.append("  Active Statistics:\n");
            sb.append(mStatistics.report(2));
        }
        return (sb.toString());
    }

}
