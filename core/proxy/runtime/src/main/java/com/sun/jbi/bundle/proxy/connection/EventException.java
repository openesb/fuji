/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

/**
 * EventException is thrown by Event when a problem is found.
 * @author Sun Microsystems, Inc
 */
public class EventException 
        extends javax.jbi.JBIException
{
    /** 
     * Create a new EventException.
     * @param msg error detail
     */
    public EventException(String msg)
    {
        super(msg);
    }
    
    /** 
     * Create a new EventException with the specified cause and error text.
     * @param msg error detail
     * @param cause underlying error
     */
    public EventException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    /** 
     * Create a new ConnectionException with the specified cause.
     * @param cause underlying error
     */   
    public EventException(Throwable cause)
    {
        super(cause);
    }
}
