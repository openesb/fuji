/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

/**
 * ConnectionException is thrown by instances of ConnectionManagers when a problem is found.
 * @author Sun Microsystems, Inc
 */
public class ConnectionException 
        extends javax.jbi.messaging.MessagingException
{
    /** 
     * Create a new ConnectionException.
     * @param msg error detail
     */
    public ConnectionException(String msg)
    {
        super(msg);
    }
    
    /** 
     * Create a new ConnectionException with the specified cause and error text.
     * @param msg error detail
     * @param cause underlying error
     */
    public ConnectionException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    /** 
     * Create a new ConnectionException with the specified cause.
     * @param cause underlying error
     */   
    public ConnectionException(Throwable cause)
    {
        super(cause);
    }
}
