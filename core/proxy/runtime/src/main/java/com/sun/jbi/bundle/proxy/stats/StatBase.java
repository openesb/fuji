/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StatBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.stats;

import com.sun.jbi.bundle.proxy.ExchangeEntry;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class StatBase 
{
    protected StatBase    mChild;
    protected StatBase    mPeer;

    public Object clone()
    {
        try
        {
            StatBase  sb = (StatBase)super.clone();
            
            if (sb.mChild != null)
            {
                sb.mChild = (StatBase)sb.mChild.clone();
            }
            if (sb.mPeer != null)
            {
                sb.mPeer = (StatBase)sb.mPeer.clone();
            }
            return (sb);
        }
        catch (java.lang.CloneNotSupportedException cnsEx)
        {
            return (null);
        }
    }
    
    public StatBase getPeer()
    {
        return (mPeer);
    }
    
    public StatBase getChild()
    {
        return (mChild);
    }
    
    public void addChild(StatBase child)
    {
        mChild = child;
    }
    
    public void addPeer(StatBase peer)
    {
        mPeer = peer;
    }
    
    public void apply(ExchangeEntry entry)
    {
        if (mChild != null)
        {
            mChild.apply(entry);
        }
        if (mPeer != null)
        {
            mPeer.apply(entry);
        }
    }

    public void clear()
    {
        if (mChild == null)
        {
            mChild.clear();
        }
        if (mPeer == null)
        {
            mPeer.clear();
        }
    }
   
    public abstract String report(int depth);
}
