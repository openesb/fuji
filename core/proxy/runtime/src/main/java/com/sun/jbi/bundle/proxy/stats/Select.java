/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Select.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.stats;

import com.sun.jbi.bundle.proxy.ExchangeEntry;

public class Select
        extends StatBase
        implements java.lang.Cloneable
{
    int                     mField;
    int                     mOperation;
    public final static int    OP_EQ   = 1;
    public final static int    OP_NE   = 2;
    public final static int    OP_LE   = 4;
    public final static int    OP_LT   = 8;
    public final static int    OP_GT   = 16;
    public final static int    OP_GE   = 32;
    Object                  mValue;
        
    Select(int field, int operation, long value)
    {
        mField = field;
        mOperation = operation;
        mValue = new Long(value);
    }
    
    Select (int field, int operation, String value)
    {
        mField = field;
        mOperation = operation;
        mValue = value;
    }
    
    public void apply(ExchangeEntry entry)
    {
        Comparable  value = (Comparable)entry.getField(mField);
        int         result;
        
        result = value.compareTo(mValue);
        if ((result < 0 && (mOperation & (OP_LE | OP_LT | OP_NE)) != 0) ||
            (result == 0 && (mOperation & (OP_EQ | OP_LE | OP_GE)) != 0) ||
            (result > 0 && (mOperation & (OP_GT | OP_GE | OP_NE)) != 0))
        {
            super.apply(entry);
        }         
    }
 
    public String report(int depth)
    {
        StringBuffer    sb = new StringBuffer();
        
        if (mChild != null)
        {
            sb.append(mChild.report(depth + 1));
        }
        if (mPeer != null)
        {
            sb.append(mPeer.report(depth));
        }
        return (sb.toString());
    }
        
}
