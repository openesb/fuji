package com.sun.jbi.bundle.proxy;

public class Localizer extends net.java.hulp.i18n.LocalizationSupport {
    public Localizer() {
        super("ESBPRX-");
    }

    private static final Localizer s = new Localizer();

    public static Localizer get() {
        return s;
    }
}
