/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistrationInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy;

import com.sun.jbi.bundle.proxy.connection.Event;
import com.sun.jbi.bundle.proxy.connection.EventInfo;

import com.sun.jbi.messaging.events.AbstractEvent;
import com.sun.jbi.messaging.events.EndpointEvent;

import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.namespace.QName;


/**
 * Represents a service or endpoint registration in the distributed registry.
 * @author Sun Microsystems, Inc
 */
public class RegistrationInfo
        extends EventInfo
        implements java.io.Serializable
{
    private transient EndpointEvent     mEndpoint;
    private QName                       mServiceName;
    private String                      mEndpointName;
    private String                      mInstanceId;
    private transient String            mAction;
    
    public static final String          EVENTNAME = "Registration";    
    public static final String          ACTION_ADD  = "Add";
    public static final String          ACTION_REMOVE = "Remove";
        
    /**
     * Constructor.
     */
    public RegistrationInfo(EndpointEvent ee, String instanceId)
    {
        mEndpoint = ee;
        mServiceName = ee.getServiceName();
        mEndpointName = ee.getEndpointName();
        mAction = ee.getAction() == AbstractEvent.ACTION.ADDED ? ACTION_ADD : ACTION_REMOVE;
        mInstanceId = instanceId;
    }
    
    public RegistrationInfo(QName serviceName, String endpointName, String action, String instanceId)
    {
        mServiceName = serviceName;
        mEndpointName = endpointName;
        mAction = action;
        mInstanceId = instanceId;
    }
    
    public EndpointEvent getEndpoint()
    {
        return (mEndpoint);
    }
    
    public String getEventName()
    {
        return (EVENTNAME);
    }
    
    /**
     * Accessor for ServiceName.
     * @return QName containing the ServiceName.
     */
    public QName getServiceName()
    {
        return (mServiceName);
    }
    
    /**
     * Accessor for EndpointName.
     * @return String containing the EndpointName.
     */
    public String getEndpointName()
    {
        return (mEndpointName);
    }
    
    /**
     * Accessor for InstanceId.
     * @return String containing the InstanceId.
     */
    public String getInstanceId()
    {
        return (mInstanceId);
    }

    /**
     * Accessor for Action.
     * @return String containing the Action.
     */
    public String getAction()
    {
        return (mAction);
    }
    
    public RegistrationInfo(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        String          namespace;
        String          local;
        
        setSerial(event.getLong());
        mAction = event.getString();
        mInstanceId = event.getString();
        namespace = event.getString();
        local = event.getString();
        mServiceName = new QName(namespace, local);
        mEndpointName = event.getString();
        mEndpoint = EndpointEvent.valueOf(
                mAction.equals(ACTION_ADD) 
                    ? AbstractEvent.ACTION.ADDED : AbstractEvent.ACTION.REMOVED, "",
                mServiceName, mEndpointName);
    }
    
    public void encodeEvent(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        event.putLong(getSerial());
        event.putString(mAction);
        event.putString(mInstanceId);
        event.putString(mServiceName.getNamespaceURI());
        event.putString(mServiceName.getLocalPart());
        event.putString(mEndpointName);
    }
    
    public int hashCode()
    {
        int     hash = 0;
        String  value;
        
        if (mServiceName != null)
        {
            if ((value = mServiceName.getNamespaceURI()) != null)
            {
                hash ^= value.hashCode();
            }
            if ((value = mServiceName.getLocalPart()) != null)
            {
                hash ^= value.hashCode();
            }
        }
        if (mEndpointName != null)
        {
            hash ^= mEndpointName.hashCode();
        }
        return (hash);
    }
    
    public boolean equals(Object equalTo)
    {
        RegistrationInfo        ri;
        
        if (equalTo instanceof RegistrationInfo)
        {
            ri = (RegistrationInfo)equalTo;
            if (mServiceName == null)
            {
                if (ri.getServiceName() != null)
                {
                    return (false);
                }
            }
            else
            {
                if (!mServiceName.getNamespaceURI().equals(ri.getServiceName().getNamespaceURI()) ||
                    !mServiceName.getLocalPart().equals(ri.getServiceName().getLocalPart()))
                {
                    return (false);
                }
            }
            if (mEndpointName == null)
            {
                if (ri.getEndpointName() != null)
                {
                    return (false);
                }
            }
            else
            {
                if (!mEndpointName.equals(ri.getEndpointName()))
                {
                    return (false);
                }
            }
                

            return (true);
        }
        return (false);
    }
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        
        sb.append("        InstanceId    (");
        sb.append(mInstanceId);
        sb.append(")\n          Serial  (");
        sb.append(getSerial());
        sb.append(")\n          Service  (");
        sb.append(mServiceName.toString());
        sb.append(")\n          Endpoint (");
        sb.append(mEndpointName);
        sb.append(")\n");
        return (sb.toString());
    }
}
