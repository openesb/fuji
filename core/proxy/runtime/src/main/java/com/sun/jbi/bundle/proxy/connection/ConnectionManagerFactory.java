/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionManagerFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

import com.sun.jbi.bundle.proxy.Localizer;

//import com.sun.jbi.bundle.proxy.jms.JMSConnectionManager;
//import com.sun.jbi.bundle.proxy.jms.IMQConnectionHelper;
//import com.sun.jbi.bundle.proxy.jms.JMSConnectionHelper;
//import com.sun.jbi.bundle.proxy.jms.JBossMQConnectionHelper;
import com.sun.jbi.bundle.proxy.gms.GMSConnectionManager;

import javax.naming.InitialContext;

/**
 * Factory for different remote connection managers.
 * @author Sun Microsystems, Inc
 */
public class ConnectionManagerFactory 
{
    /** 
     * ConnectionManagerFactory singleton.
     */  
    private static ConnectionManagerFactory sCMF;
    private final InitialContext            mContext;
    private Localizer                       mLoc;

    /** 
     * Constructor
     * Private constructor to facilitate a singleton.
     */
    private ConnectionManagerFactory(InitialContext context) 
    { 
        mContext = context;
        mLoc = Localizer.get();
    }
    
    /**
     * Returns a singleton instance of the ConnectionManagerFactory class.
     * @return The instance of the ConnectionManagerFactory 
     */
    public static ConnectionManagerFactory getInstance(InitialContext context) 
    { 
        if (null == sCMF)
        {
            sCMF = new ConnectionManagerFactory(context); 
        }
        return sCMF;
    }

    /**
     * Returns an ConnectionManager instance using the the named provider.
     * @param id to assign to this connection
     * @return The requested ConnectionManager.
     * @throws ConnectionException if provider is not found or failure to create.
     */
    public ConnectionManager getConnectionManager(String id)
        throws ConnectionException
    {
        String          provider;
        String[]        pieces;
        
        provider = System.getProperty("com.sun.jbi.binding.proxy.connection", "GMS::");
        pieces = provider.split(":");
//        if (pieces.length == 3)
//        {
//            if (pieces[0].equals("IMQ"))
//            {
//                //
//                // Hard code this for the moment.
//                //
//                return (new JMSConnectionManager(id, new IMQConnectionHelper(pieces[1], pieces[2])));
//            }
//            else if (pieces[0].equals("JBOSSMQ"))
//            {
//                return (new JMSConnectionManager(id, new JBossMQConnectionHelper(pieces[1], pieces[2])));
//            }
//                
//        }
//        else 
        if (pieces.length == 1)
        {
//            if (pieces[0].equals("JMS"))
//            {
//                return (new JMSConnectionManager(id, new JMSConnectionHelper(mContext)));
//            }
//            else if (pieces[0].equals("GMS"))
            {
                return (new GMSConnectionManager(id));
            }
        }            
        throw new ConnectionException(
            mLoc.t("0090: No connection for provider type ({0}), ProxyBinding will not be started.", provider));

    }
}
