/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy;

import com.sun.jbi.bundle.proxy.connection.ConnectionManager;
import com.sun.jbi.bundle.proxy.connection.ClientConnection;

import com.sun.jbi.bundle.proxy.util.MEPOutputStream;

import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NodeList;

/**
 *
 * Runnable class that implements the NMR receiver. All traffic from the NMR that is associated
 * with endpoints for which the ProxyBinding is acting as a proxy, are processed here. Also, the
 * ProxyBinding exposes a handful of services used locally and remotely, and these are handled
 * here. 
 * The basic flow is:
 *      Decide if message is for us or for a proxy.
 *      If the exchange is for us, process it.
 *      Else, lookup the instance that hosts the service.
 *      Send the exchange to the remote instance that is hosting the service.
 *      Remember the exchange if new, forget the exchange is this is the last send.
 *
 * @author Sun Microsystems, Inc
 */
class NMRProcessor
        implements  java.lang.Runnable,
            com.sun.jbi.ext.TimeoutListener
{
    private Logger                  mLog;
    private Localizer               mLoc;
    private ProxyBinding            mPB;
    private ConnectionManager       mCM;
    private boolean                 mRunning;
    private DeliveryChannel         mChannel;
    private MessageExchangeFactory  mFactory;
    private MEPOutputStream         mMOS;
    
    /**
     * Operation names.
     */
    static final String             EVENT = "Event";
    static final String             SERVICE_DESCRIPTION = "ServiceDescription";
    static final String             RESOLVE_ENDPOINT = "ResolveEndpoint";
    static final String             ISEXCHANGEOKAY = "IsExchangeOkay";
    static final String             ISEXCHANGEOKAY2 = "IsExchangeOkay2";
    static final String             TIMEOUT = "Timeout";

    /**
     * Property names used in above operations.
     */
    static final String             EXCHANGEID = "ExchangeId";
    static final String             TIMEOUTCHECK = "TimeoutCheck";
    static final String             SERVICENAME = "ServiceName";
    static final String             SERVICELINKNAME = "ServiceLinkName";
    static final String             ENDPOINTNAME = "EndpointName";
    static final String             ENDPOINTLINKNAME = "EndpointLinkName";
    static final String             LINKTYPE = "LinkType";
    static final String             INSTANCENAME = "InstanceName";
    static final String             EXCHANGE = "Exchange";
    static final String             EXCHANGEOKAY = "ExchangeOkay";
    
    int                             mOperationsReceived;
    int                             mOperationsSent;

    NMRProcessor(ProxyBinding proxyBinding)
    {
         mPB = proxyBinding;
         mLog = mPB.getLogger("nmr");        
         mChannel = mPB.getDeliveryChannel();
         mFactory = mChannel.createExchangeFactoryForService(mPB.getService().getServiceName());
         mCM = mPB.getConnectionManager();
         mRunning = true;
         ((com.sun.jbi.ext.DeliveryChannel)mChannel).addTimeoutListener(this);
    }
    
    void stop()
    {
        mRunning = false;
    }
    
    public void run() 
    {
        MessageExchange         me = null;
        
        mLog.info(mLoc.t("0018: PB-NP: starting."));
        mPB.isStarted();
        try
        {
            mMOS = new MEPOutputStream(mPB);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0019: PB-NP: Init MessagingException: {0}", mEx.getMessage()), mEx);
        }
        
        for (;mRunning;)
        {
            try
            {
                ExchangeEntry           ee;
                ServiceEndpoint         se;
                
                //
                //  Wait for a message exchange.
                //
                me = null;
                me = (MessageExchange)mChannel.accept();
                if (me == null)
                {
                    continue;
                }
//                mLog.info(mLoc.t("0020: PB-NP: Accept Id({0})", me.getExchangeId()));
//                mLog.info(mLoc.t("0021: PB-NP: ME is\n {0}", me.toString()));

                //
                //  See if the ProxyBinding itself is the target of the request.
                //
                if ((se = me.getEndpoint()).getServiceName().getLocalPart().equals(mPB.SUNPROXYBINDINGSERVICE))
                {
                    processProxyServiceRequest(me);
                }
                else
                {
                    //
                    //  See if this is a new or known message exchange.
                    //  A new message is tracked for future use.
                    //
                    String          id = me.getExchangeId();
                    
                    ee = mPB.getExchangeEntry(id);
                    if (ee == null)
                    { 
                        ClientConnection       cc;
                        String                 target;

                        target = mPB.getInstanceForEndpoint(se);
                        if (target == null)
                        {
                            mLog.info(mLoc.t("0022: Proxy Binding: {0}", mPB.toString()));
                            throw new javax.jbi.messaging.MessagingException(
                                mLoc.t("0091: Endpoint not found for service ({0})", se.toString()));
                        }                    
                        cc = mCM.getClientConnection(target);                    
                        ee = mPB.trackExchange(id, me, true);
                        ee.setClientConnection(cc);
                        ee.setState(ExchangeEntry.STATE_ONGOING);
//                        mLog.info(mLoc.t("0023: PB-NP: New Id({0}) To ({1})", me.getExchangeId(), cc.getInstanceId()));
                    }
                    else
                    {
                        ee.updateExchange(me);
//                        mLog.info(mLoc.t("0024: PB-NP: Ongoing Id({0})", me.getExchangeId()));
                    }

                    //
                    //  Send the MEP to its target PB.
                    //
                    writeMEP(ee);
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
                //
                //  We will get MessagingException from an InterruptedException at shutdown.
                //
                if (mRunning)
                {
                    logExceptionInfo("PB-NP: MessagingException.", mEx);
                    if (me != null)
                    {
                        handleError(me, mEx);
                    }
                    else
                    {
                        mPB.stop();
                    }
                }
            }
            catch (Exception e)
            {
                logExceptionInfo("PB-NP: Unexpected Exception.", e);
                
                //
                //  Fail fast in the face of unanticipated problems.
                //
                mLog.log(Level.INFO,
                         mLoc.t("0022: Proxy Binding: {0}", mPB.toString()), e);
                mPB.stop();
            }
        }
    }
    
    void handleError(MessageExchange me, Exception e)
    {
        String              id = me.getExchangeId();
        ExchangeEntry       ee = mPB.getExchangeEntry(id);
        
        //
        //  Ignore this message if we know nothing about it. It could be a stale message from a
        //  restart.
        //
        if (ee != null)
        {
            if (((com.sun.jbi.ext.DeliveryChannel)mChannel).isActive(me))
            {
                //
                //  Send error indication back to the local NMR based component.
                //  This is only possible if the ME is still active (I.E. final send() hasn't happened.)
                //
                try
                {
                    me.setError(e);
                    mChannel.send(me);
                }
                catch (javax.jbi.JBIException jEx)
                {

                }
            }
        
            //
            //  Send error indication back to remote NMR based component.
            //  This is only possible if we have an ongoing conversation with this ME.
            //
            ClientConnection    cc = ee.getClientConnection();

            try
            {
                cc.send(new MEPOutputStream(mPB).writeException(id, e));
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
        mLog.info(mLoc.t("0026: PB-NP: drop Id({0}) Reason({1})", id, e.toString()));
        mPB.purgeExchange(id);
    }
    
    void logExceptionInfo(String reason, Throwable t)
    {
        StringBuffer                    sb = new StringBuffer();
        java.io.ByteArrayOutputStream   b;
        Throwable                       nextT = t;
        
        sb.append(reason);
        while (t != null)
        {
            if (nextT != null)
            {
                java.io.PrintStream ps = new java.io.PrintStream(b = new java.io.ByteArrayOutputStream());
                t.printStackTrace(ps);
                sb.append(mLoc.t("0027: Exception ({0})", b.toString()));
            }
            nextT = t.getCause();
            if (nextT != null)
            {
                sb.append(mLoc.t("0028: Caused by: "));
            }
            t = nextT;
        }
        
        mLog.warning(sb.toString());        
    }
    
    void writeMEP(ExchangeEntry ee)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me = ee.getMessageExchange();
        ClientConnection    cc = ee.getClientConnection();
        String              id = me.getExchangeId();
        byte[]             bytes;
        
        //
        //  Send message exchange to target.
        //
        bytes = mMOS.writeMEP(me, ee);
//        mLog.info(mLoc.t("0028: PB-NP: Write Id({0}) Target({1}) Bytes({2})", id, cc.getInstanceId(), bytes.length));
        cc.send(bytes);

        //
        //  Check to see if the Channel has completed all processing
        //  on this message exchange.
        //
        if (!((com.sun.jbi.ext.DeliveryChannel)mChannel).isActive(me))
        {
            //mLog.info(mLoc.t("0029: PB-NP: forget Id({0})", id));
            mPB.purgeExchange(id);
        }

    }
     
    void writeIsMEPOk(ExchangeEntry ee)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me = ee.getMessageExchange();
        ClientConnection    cc = ee.getClientConnection();
        String              id = me.getExchangeId();
        byte[]             bytes;
        
        //
        //  Send message exchange to target.
        //
        bytes = mMOS.writeIsMEPOk(ee);
//        mLog.info(mLoc.t("0030: PB-NP: IsMEPOk send Id({0}) Target({1}) Bytes({2})", ee.getRelatedExchange().getExchangeId(), cc.getInstanceId(), bytes.length));
        cc.send(bytes);

//        //
//        //  Check to see if the Channel has completed all processing
//        //  on this message exchange.
//        //
//        if (!mChannel.activeReference(me))
//        {
//            mLog.info(mLoc.t("0031: PB:NMRProcessor forget Id({0})", id));
//            mPB.purgeExchange(id);
//        }

    }
    
    void writeMEPOk(ExchangeEntry ee, boolean okay)
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me = ee.getMessageExchange();
        ClientConnection    cc = ee.getClientConnection();
        String              id = me.getExchangeId();
        byte[]             bytes;
        
        //
        //  Send message exchange to target.
        //
        bytes = mMOS.writeMEPOk(id, okay);
//        mLog.info(sLoc_.t("0032: PB-NP: MEPOk send Id({0}) Target({1}) Bytes({1})",
//                          id,
//                          cc.getInstanceId(),
//                          bytes.length));
        cc.send(bytes);
        if (!okay)
        {
//            mLog.info(mLoc.t("0033: PB-NP: writeMEPOk forget Id({0})", id));
            mPB.purgeExchange(id);           
        }
    }


//
// ------------------- Methods that deal with ProxyBinding services  ------------------
//

    /**
     * Handle any operations that are targeted at the service exposed by the ProxyBinding.
     * @param me containing the messageExchange that should be handled.
     */
    void processProxyServiceRequest(MessageExchange me)
    {
        String  operation = me.getOperation().getLocalPart();
        
//        mLog.info(mLoc.t("0034: PB-NP: Requested Operation({0})", operation));
        mOperationsReceived++;
        
        //
        //  Determine action needed.
        //
        if (operation.equals(TIMEOUT))
        {
            onTimeout(me);            
        }
        else if (operation.equals(SERVICE_DESCRIPTION))
        {
            doServiceDescription(me);
        }
        else if (operation.equals(RESOLVE_ENDPOINT))
        {
            doResolveEndpoint(me);
        }
        else if (operation.equals(ISEXCHANGEOKAY))
        {
            doIsExchangeOkay(me);
        }
        else if (operation.equals(ISEXCHANGEOKAY2))
        {
            doIsExchangeOkay2(me);
        }
        else
        {
            try
            {
                mLog.info(mLoc.t("0035: PB-NP: Unknown Operation({0})", operation));
                {
                    me.setStatus(ExchangeStatus.ERROR);
                    mChannel.send(me);
                }
            }
            catch (javax.jbi.messaging.MessagingException ignore)
            {
                
            }
        }
    }

    public void onTimeout(MessageExchange me)
    {
        String              id = (String)me.getProperty(EXCHANGEID);
        ExchangeEntry       ee = mPB.getExchangeEntry(id);
        
        try
        {
            if (((com.sun.jbi.ext.MessageExchange)me).isRemoteInvocation())
            {                    
                if (me.getRole().equals(MessageExchange.Role.PROVIDER))
                {
                    MessageExchange     oldme;
                    boolean             timedout;

                    mLog.info(mLoc.t("0036: PB-NP: Timeout check Id({0})", id));
                    if (ee != null)
                    {
                        oldme = ee.getMessageExchange();
                        if (oldme != null)
                        {
                            me.setProperty(TIMEOUTCHECK,
                                new Boolean(timedout = ((com.sun.jbi.ext.MessageExchange)oldme).checkTimeout()));
                            if (timedout)
                            {
                                //
                                //  Cleanup any ExchangeEntry for the message that has timed out.
                                //
                                mLog.info(mLoc.t("0037: PB-NP: Timeout terminate Id({0})", id));
                                ((com.sun.jbi.ext.MessageExchange)oldme).terminate(null);
                                mPB.purgeExchange(oldme.getExchangeId());
                            }
                        }
                    }
                    me.setStatus(ExchangeStatus.DONE);
                    mChannel.send(me);
                }
                else
                {
                    ee = mPB.getExchangeEntry(me.getExchangeId());
                    if (ee != null)
                    {
                        mLog.info(mLoc.t("0038: PB-NP: Timeout remote  Id({0}) Target({1})",
                                         id,
                                         ee.getClientConnection().getInstanceId()));
                        writeMEP(ee);
                    }
                }
            }
            else
            {
                ExchangeEntry       tee;

                //
                //  We use the instance determined by the exchange information. This allows the
                //  timeout message to be sent to the same instance as the original request
                //  instead of being subject to a load-balancing decision.
                //
                tee = mPB.trackExchange(me.getExchangeId(), me, true);
                tee.setClientConnection(ee.getClientConnection());
                tee.setState(ExchangeEntry.STATE_ONGOING);
                mLog.info(mLoc.t("0039: PB-NP: Timeout local Id({0}) Target({1})",
                                id,
                                ee.getClientConnection().getInstanceId()));
                writeMEP(tee);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.log(Level.INFO,
                         mLoc.t("0040: PB-NP: Timeout Id({0}) Exception({0})",
                                id, jEx.getMessage()), jEx);
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doServiceDescription(MessageExchange me)
    {
        try
        {
            QName               service = (QName)me.getProperty(SERVICENAME);
            String              endpoint = (String)me.getProperty(ENDPOINTNAME);
            ServiceEndpoint     se = ((com.sun.jbi.ext.DeliveryChannel)mChannel).createEndpoint(service, endpoint);

            if (((com.sun.jbi.ext.MessageExchange)me).isRemoteInvocation())
            {                    
               if (me.getRole().equals(MessageExchange.Role.PROVIDER))
               {
                    //
                    //  This is an InOut request. We should get a DONE/ERROR back when the request is
                    //  acknowledged. There is nothing to do with the acknowledgement so just return
                    //  (and swallow the message.)
                    //
                    if (me.getStatus().equals(ExchangeStatus.ACTIVE))
                    {
                        Document            document;
                        NormalizedMessage   nm;

                        document = mPB.getComponentContext().getEndpointDescriptor(se);
                        nm = me.createMessage();
                        nm.setContent(new DOMSource(document));
                        ((InOut)me).setOutMessage(nm);
                        mChannel.send(me);
                    }
                }
                else
                {
                    ExchangeEntry   ee = mPB.getExchangeEntry(me.getExchangeId());
                    if (ee != null)
                    {
                        mLog.fine("PB-NP: ServiceDescription Target(" + ee.getClientConnection().getInstanceId() + ")");
                        writeMEP(ee);
                    }
                }
            }
            else                
            {
                ExchangeEntry       ee = mPB.getExchangeEntry(me.getExchangeId());

                //
                //  Select the Instance based on the Service+Endpoint information.
                //  Note: This ME is InOut so we have to correctly handle the DONE message
                //  that most go back to the provoder.
                //
                if (ee == null)
                {
                    ee = mPB.trackExchange(me.getExchangeId(), me, true);
                    ee.setClientConnection(mCM.getClientConnection(mPB.getInstanceForEndpoint(se)));
                    ee.setState(ExchangeEntry.STATE_ONGOING);
                }
                mLog.fine("PB-NP: ServiceDescription Target(" + ee.getClientConnection().getInstanceId() + ")");
                writeMEP(ee);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB-NP: ServiceDescription Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
    }
    
    void doResolveEndpoint(MessageExchange me)
    {
        try
        {
            if (((com.sun.jbi.ext.MessageExchange)me).isRemoteInvocation())
            {                    
                if (me.getRole().equals(MessageExchange.Role.PROVIDER))
                {
                    Document            document = (Document)((DOMSource)((InOnly)me).getInMessage().getContent()).getNode();
                    ServiceEndpoint     se;
                    DocumentFragment    df = document.createDocumentFragment();
                    NodeList            nodes = document.getChildNodes();
                    
                    for (int i = 0; i < nodes.getLength(); )
                    {
                        df.appendChild(nodes.item(i));
                    }
                    se = mPB.getComponentContext().resolveEndpointReference(df);
                    me.setProperty(SERVICENAME, se.getServiceName());
                    me.setProperty(ENDPOINTNAME, se.getEndpointName());
                    me.setStatus(ExchangeStatus.DONE);
                    mChannel.send(me);                    
                }
                else
                {
                    ExchangeEntry   ee = mPB.getExchangeEntry(me.getExchangeId());
                    if (ee != null)
                    {
                        mLog.fine("PB-NP: ResolveEndpoint Target(" + ee.getClientConnection().getInstanceId() + ")");
                        writeMEP(ee);
                    }
                }
            }
            else                
            {
                ExchangeEntry       ee;
                
                ee = mPB.trackExchange(me.getExchangeId(), me, true);
                ee.setClientConnection(mCM.getClientConnection((String)me.getProperty(INSTANCENAME)));
                ee.setState(ExchangeEntry.STATE_ONGOING);
                mLog.fine("PB-NP: ResolveEndpoint Target(" + ee.getClientConnection().getInstanceId() + ")");
                writeMEP(ee);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB-NP: ResolveEndpoint Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
   }
    
   void doIsExchangeOkay(MessageExchange me)
   {
        try
        {
            MessageExchange     rme = (MessageExchange)me.getProperty(EXCHANGE);
            ExchangeEntry       ee;
            ClientConnection    cc;

            cc = mCM.getClientConnection((String)me.getProperty(INSTANCENAME));  

            ee = mPB.trackExchange(rme.getExchangeId(), rme, true);
            ee.setRelatedExchange(me);
            ee.setClientConnection(cc);
            ee.setState(ExchangeEntry.STATE_ONGOING);
            mLog.fine("PB-NP: IsExchangeOkay Target(" + ee.getClientConnection().getInstanceId() + ")");
            writeIsMEPOk(ee);
        }
        catch (javax.jbi.JBIException jEx)
        {
        }
  }

   void doIsExchangeOkay2(MessageExchange me)
   {
        try
        {
            if (me.getStatus().equals(ExchangeStatus.ACTIVE))
            {
                MessageExchange     rme = (MessageExchange)me.getProperty(EXCHANGE);
                ExchangeEntry       ee = mPB.getExchangeEntry(rme.getExchangeId());
                
                writeMEPOk(ee, ((com.sun.jbi.ext.DeliveryChannel)mChannel).isExchangeOkay(rme));
                me.setStatus(ExchangeStatus.DONE);
                mChannel.send(me);
            }
        }
        catch (javax.jbi.JBIException jEx)
        {
            try
            {
                mLog.fine("PB-NP: IsExchangeOkay2 Exception(" + jEx + ")");
                me.setStatus(ExchangeStatus.ERROR);
                mChannel.send(me);  
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }
  }
 
//
// ---------------- Methods defined in com.sun.jbi.messaging.TimeoutListener --------------
//

    /**
     * A message exchange has timed out on a sendSynch operation. We use an InOnly message exchange
     * to convey the request to the ProxyBinding (remember we are running in the context of a
     * component that is waiting in sendSynch().)
     * @param channel that is performing a timeout on an endpoint.
     * @param endpoint that is being timed out.
     */
    public boolean checkTimeout(DeliveryChannel channel, javax.jbi.messaging.MessageExchange exchange)
    {
        InOnly          io;
        boolean         timedout = false;
        
        try
        {
            io = mFactory.createInOnlyExchange();
            io.setOperation(new QName(TIMEOUT));
            io.setProperty(EXCHANGEID, exchange.getExchangeId());
            mOperationsSent++;
            channel.sendSync(io);
            if (io.getStatus().equals(ExchangeStatus.DONE))
            {
                timedout = ((Boolean)io.getProperty(TIMEOUTCHECK)).booleanValue();
                if (timedout)
                {
                    ((com.sun.jbi.ext.MessageExchange)exchange).terminate(null);
                    mPB.purgeExchange(exchange.getExchangeId());
                }
            }
            else
            {
                mLog.fine("PB-NP: Timeout failure");
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            
        }
        
        return (timedout);
    }


    /**
     * A service description has been requested for a remote service. We process this by sending a
     * InOnly message exchange to the ProxyBinding (remember we are running in the context of a
     * component that asking for the service description) with the endpoint information.
     * @param endpoint that is being queried for its description
     */
    public Document getDescription(ServiceEndpoint endpoint)
    {
        InOut       io;
        Document    document = null;
        try
        {
            io = mFactory.createInOutExchange();
            io.setOperation(new QName(SERVICE_DESCRIPTION));
            io.setProperty(SERVICENAME, endpoint.getServiceName());
            io.setProperty(ENDPOINTNAME, endpoint.getEndpointName());
            mOperationsSent++;
            mChannel.sendSync(io);
            if (!io.getStatus().equals(ExchangeStatus.ACTIVE))
            {
                mLog.fine("PB-NP: serviceDescription failure");
            }
            else
            {
                document = (Document)((DOMSource)((InOut)io).getOutMessage().getContent()).getNode();
                io.setStatus(ExchangeStatus.DONE);
                mChannel.send(io);
            }
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            
        }
        
        return (document);
    }
    
    /**
     * An endpoint reference needs to be resolved.  We send this to each known instance and wait for an
     * answer. We process this by sending a InOnly message exchange to the remote ProxyBinding 
     * (remember we are running in the context of a component that is asking for the endpoint resolution)
     * with the endpoint information.
     * @param endpoint that is being queried for its description
     */
    ServiceEndpoint resolveEndpointReference(DocumentFragment document)
    {
        InOnly       io;
        ServiceEndpoint se = null;
        HashMap     instances = mPB.getInstances();
        
        for (Iterator i = instances.keySet().iterator(); i.hasNext(); )
        {
            String      instance = (String)i.next();
            
            //
            //  Ignore ourselves...
            //
            if (instance.equals(mPB.getInstanceId()))
            {
                continue;
            }
            
            try
            {
                NormalizedMessage       nm;
                io = mFactory.createInOnlyExchange();
                nm = io.createMessage();
                nm.setContent(new DOMSource(document));
                io.setInMessage(nm);
                io.setOperation(new QName(RESOLVE_ENDPOINT));
                io.setProperty(INSTANCENAME, instance);
                mOperationsSent++;
                mChannel.sendSync(io);
                if (!io.getStatus().equals(ExchangeStatus.DONE))
                {
                    mLog.fine("PB-NP: resolveEndpoint failure");
                }
                else
                {
                    if (io.getProperty(SERVICENAME) != null)
                    {
                        se = ((com.sun.jbi.ext.DeliveryChannel)mChannel).createEndpoint((QName)io.getProperty(SERVICENAME),
                            (String)io.getProperty(ENDPOINTNAME));
                    }
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
            if (se != null)
            {
                break;
            }
        }

        return (se);
    }
    
    /**
     * A provider of a service needs to be queried about the suitability of the consumer request.
     * We process this by sending a InOnly message exchange to the remote ProxyBinding 
     * (remember we are running in the context of a component that is attempting to perform a send to the
     * provider)
     * @param endpoint that is being queried for its description
     */
    boolean isExchangeOkay(ServiceEndpoint endpoint, javax.jbi.messaging.MessageExchange exchange)
    {
        InOnly       io;
        ServiceEndpoint se = null;
        HashMap     instances = mPB.getInstances();
        Boolean     okay;
        
        for (Iterator i = instances.keySet().iterator(); i.hasNext(); )
        {
            String      instance = (String)i.next();
            
            //
            //  Ignore ourselves...
            //
            if (instance.equals(mPB.getInstanceId()))
            {
                continue;
            }
            
            try
            {
                io = mFactory.createInOnlyExchange();
                io.setOperation(new QName(ISEXCHANGEOKAY));
                io.setProperty(INSTANCENAME, instance);
                io.setProperty(EXCHANGE, exchange);
                mOperationsSent++;
                mChannel.sendSync(io);
                if (!io.getStatus().equals(ExchangeStatus.DONE))
                {
                    mLog.fine("PB-NP: resolveEndpoint failure");
                }
                else
                {
                    if ((okay = (Boolean)io.getProperty(EXCHANGEOKAY)) != null)
                    {
                        return (okay.booleanValue());
                    }
                }
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {

            }
        }

        return (false);
    }
 
}
