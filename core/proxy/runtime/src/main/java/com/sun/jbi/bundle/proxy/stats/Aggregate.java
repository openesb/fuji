/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Aggregate.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.stats;

import com.sun.jbi.bundle.proxy.ExchangeEntry;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Aggregate
        extends StatBase
        implements java.lang.Cloneable
{
    class Entry
    {            
        int             mCount;
        StatBase        mItem;

        Entry (StatBase item)
        {
            mItem = item;
        }
    }
    
    private int         mField;
    private HashMap     mMap;
    
    /** Creates a new instance of Aggregate */
    public Aggregate(int field) 
    {
        mField = field;
        mMap = new HashMap();
    }
    
    public Object clone()
    {
        Aggregate a = (Aggregate)super.clone();
        
        a.mMap = new HashMap();
        return (a);
    }
    
    public void apply(ExchangeEntry entry)
    {
        String      name = (String)entry.getStringField(mField);
        Entry       e;
        StatBase   value;
        
        e = (Entry)mMap.get(name);
        if (e == null)
        {
            if (mChild == null)
            {
                e = new Entry(null);
            }
            else
            {
                e = new Entry((StatBase)mChild.clone());
            }
            mMap.put(name, e);
        }
        e.mCount++;
        if (e.mItem != null)
        {
            e.mItem.apply(entry);
        }
        if (mPeer != null)
        {
            mPeer.apply(entry);
        }
     }
    
    public void clear()
    {
        mMap = new HashMap();
        super.clear();
    }
    
    public String report(int depth)
    {        
        String              name;
        Long                value;
        StringBuffer        sb = new StringBuffer();
        long                total = 0;
        
        for (Iterator i = mMap.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry   me = (Map.Entry)i.next();
            Entry       e;
            
            for (int j = 0; j < depth; j++)
            {
                sb.append("  ");
            }
            sb.append(ExchangeEntry.fieldToString(mField));
            sb.append(" (");
            sb.append((String)me.getKey());
            e = (Entry)me.getValue();
            sb.append(") Count(");
            sb.append(e.mCount);
            total += e.mCount;
            sb.append(")\n");
            if (e.mItem != null)
            {
                sb.append(e.mItem.report(depth + 1));
            }
        }
        for (int j = 0; j < depth; j++)
        {
            sb.append("  ");
        }
        sb.append("Total (");
        sb.append(total);
        sb.append(")\n");
        if (mPeer != null)
        {
            sb.append(mPeer.report(depth));
        }
        return (sb.toString());
    }
}
