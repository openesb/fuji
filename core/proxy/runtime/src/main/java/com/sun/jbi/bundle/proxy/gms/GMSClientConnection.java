/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSClientConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.gms;

import com.sun.jbi.bundle.proxy.connection.ClientConnection;


/**
 * Connection as a JMS Client.
 * @author Sun Microsystems, Inc
 */
public class GMSClientConnection 
        implements ClientConnection
{
    private GMSConnectionManager    mGCM;
    private GMSMessage              mMessage;
    private String                  mId;
    private int                     mByteCount;
    
    /** 
     * Creates a new instance of GMSClientConnection
     * @param id to be assigned to this connection.
     * @param cm GMC ConnectionManager
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    GMSClientConnection(String id, GMSConnectionManager cm) 
    {        
        mId = id;
        mGCM = cm;       
    }
    
    /**
     * Send array of bytes on the JMS connection.
     * @param bytes to be sent
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public void send(byte[] bytes)
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        mGCM.sendMessage(new GMSMessage(mId, bytes));
    }
    
    /**
     * Receive array of bytes on the JMS connection.
     * @return bytes to be returned
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public byte[] receive()
    {
        byte[]      bytes = mMessage.getMessage();
        
        mByteCount = bytes.length;
        return (bytes);        
    }
    
    /**
     * Return the instanceId of this connection.
     * @return String containing instanceId.
     */
    public String getInstanceId()
    {
        return (mId);
    }

    void setMessage(GMSMessage message)
    {
        mMessage = message;
    }   
    
    public int receivedByteCount()
    {
        return (mByteCount);
    }
}
