/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)HelloInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

import java.util.LinkedList;

/**
 * Presents an instance joining a Proxy group
 * @author Sun Microsystems, Inc
 */
public class HelloInfo
        extends EventInfo
{
    private final String                mInstanceId;
    private final long                  mBirthtime;
    private final LinkedList<Object>    mQueuedEvents;
    private long                        mSerialNumber;
    
    public static final String          EVENTNAME = "Hello";    
    
    /**
     * Constructor.
     */
    public HelloInfo(String instanceId, long birthtime)
    {
        mInstanceId = instanceId;
        mBirthtime = birthtime;
        mSerialNumber = 0;
        mQueuedEvents = new LinkedList();
    }

    public String getEventName()
    {
        return (EVENTNAME);
    }
    
    public long getSerialNumber()
    {
        return (mSerialNumber);
    }
    
    synchronized public void setSerialNumber(long sn)
    {
        mSerialNumber = sn;
    }
    
    synchronized public boolean addQueuedEvent(Object o, long sn)
    {
        if (mSerialNumber == 0 || mQueuedEvents.size() > 0)
        {
            mQueuedEvents.add(o);
            return (true);
        }
        return (false);
    }
    
    synchronized public Object getQueuedEvent()
    {
        if (!mQueuedEvents.isEmpty())
        {
            return (mQueuedEvents.getFirst());
        }
        return (null);
    }
    
    /**
     * Accessor for Birthtime.
     * @return long containing the Birthtime.
     */
    public long getBirthtime()
    {
        return (mBirthtime);
    }
    
    /**
     * Accessor for InstanceId.
     * @return String containing the InstanceId.
     */
    public String getInstanceId()
    {
        return (mInstanceId);
    }


    public HelloInfo(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        mInstanceId = event.getString();
        mBirthtime = event.getLong();
        mSerialNumber = 0;
        mQueuedEvents = new LinkedList();
    }
    
    public void encodeEvent(Event event)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        event.putString(mInstanceId);
        event.putLong(mBirthtime);
    }
}
