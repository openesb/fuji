/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RemoteProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy;

import com.sun.jbi.bundle.proxy.connection.ConnectionManager;
import com.sun.jbi.bundle.proxy.connection.ConnectionManagerFactory;
import com.sun.jbi.bundle.proxy.connection.ClientConnection;
import com.sun.jbi.bundle.proxy.connection.EventConnection;
import com.sun.jbi.bundle.proxy.connection.ServerConnection;
import com.sun.jbi.bundle.proxy.connection.EventInfo;
import com.sun.jbi.bundle.proxy.connection.EventInfoFactory;

import com.sun.jbi.bundle.proxy.util.MEPInputStream;
import com.sun.jbi.bundle.proxy.util.MEPOutputStream;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;

import javax.xml.namespace.QName;

/**
 * Runnable class that implements the Remote receiver. All traffic from any remote instance is
 * first handled here. The remote traffic is either directed at this ProxyBinding instance or at 
 * an endpoint for a local component.
 * The basic flow is:
 *      Decide if message is for us or for local compoent.
 *      If the exchange is for us, process it.
 *      Else, lookup the local compoent that hosts the service.
 *      Send the to the NMR for local routing
 *      Remember the exchange if new, forget the exchange is this is the last send.
 *
 * @author Sun Microsystems, Inc
 */
class RemoteProcessor
        implements  java.lang.Runnable                    
{
    private Logger                      mLog;
    private Localizer                   mLoc;
    private ProxyBinding                mPB;
    private ConnectionManager           mCM;
    private boolean                     mRunning;
    private DeliveryChannel             mChannel;
    private MEPInputStream              mMIS;
    private MessageExchangeFactory      mFactory;
    
    RemoteProcessor(ProxyBinding proxyBinding)
    {
         mPB = proxyBinding;
         mLog = mPB.getLogger("remote");        
         mLoc = Localizer.get();
         mChannel = mPB.getDeliveryChannel();
         mCM = mPB.getConnectionManager();
         mRunning = true;
         mFactory = mPB.getDeliveryChannel().createExchangeFactory();
    }
    
    void stop()
    {
        mRunning = false;
    }
    
    public void run() 
    {
        ServerConnection        sc;
        
        //
        // Set up JMS Queue based on our instance-id for inbound messages.
        //
        try
        {
            sc = mCM.getServerConnection();
            mLog.info(mLoc.t("0043: PB-RP: starting as ({0})", sc.getInstanceId()));
            mMIS = new MEPInputStream(mPB);
            mPB.isStarted();
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.log(Level.WARNING,
                         mLoc.t("0044: PB-RP: Init MessagingException: {0}", mEx.getMessage()),
                         mEx);
            return;
        }
        catch (Exception ex)
        {
            mLog.log(Level.WARNING,
                         mLoc.t("0045: PB-RP: Init Exception: {0}", ex.getMessage()),
                         ex);
            return;                               
        }


        //
        // Basic processing loop.
        //
        for (;mRunning;)
        {                
            try
            {
                ClientConnection        cc = null;
                int                     msgType;
            
                //
                // Accept a new client connection.
                //
                cc = sc.accept();
                if (cc == null)
                {   
                    continue;
                }

                //
                //  Read the message from the connection.
                //
                msgType = mMIS.readMessage(cc);
                
                //
                //  Process based on type of message.
                //
                if (msgType == MEPInputStream.TYPE_MEP)
                {
                    doMEP();
                }
                else if (msgType == MEPInputStream.TYPE_EXCEPTION)
                {
                    doException();
                }
                else if (msgType == MEPInputStream.TYPE_ISMEPOK)
                {
                    doISMEPOk();
                }
                else if (msgType == MEPInputStream.TYPE_MEPOK)
                {
                    doMEPOK();                 
                }
                else
                {
                }
                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
                if (mRunning)
                {
                    logExceptionInfo("PB-RP: MessagingException.", mEx);
                    handleError(mEx);
                }
            }
            catch (Exception ex)
            {
                logExceptionInfo("PB-RP: Unexpected Exception.", ex);

                //
                //  Fail fast in the face of unanticipated problems.
                //
                mPB.stop();
            }
         }
    } 
    
    void doException()
    {
        mLog.info(mLoc.t("0046: PB-RP: Exception Id({0})",
                         mMIS.getExchangeId()));
        handleError(mMIS.getException());
    }
    
    void doISMEPOk()
    {
        ExchangeEntry      ee = mMIS.getExchangeEntry();
        MessageExchange    me = ee.getMessageExchange();
        InOnly             io;
        
//        mLog.fine("PB-RP: IsMEPOk Id(" + me.getExchangeId() + ") From (" 
//            + ee.getClientConnection().getInstanceId() +") Bytes(" + ee.getBytesReceived() + ")");
        try
        {
            io = mFactory.createInOnlyExchange();
            io.setService(mPB.getService().getServiceName());
            io.setOperation(new QName(NMRProcessor.ISEXCHANGEOKAY2));
            io.setProperty(NMRProcessor.EXCHANGE, me);
            mChannel.send(io);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0047: PB-RP: doISMEPOk MessagingException: {0}", mEx.getMessage()),
                     mEx);
        }
    }
    
    void doMEPOK()
    {
        String             id = mMIS.getExchangeId();
        ExchangeEntry      ee = mPB.getExchangeEntry(id);
        MessageExchange    me = ee.getRelatedExchange();
        boolean            ok = mMIS.getMEPOk();
        
        //
        //  Forward MEP OK status to requestor.
        //
        //mLog.info(Level.INFO, mLoc.t("0048: PB-RP: MEPOk Id({0}) From ({1}) Bytes({2})", id, ee.getClientConnection().getInstanceId(), ee.getBytesReceived()));
        try
        {
            me.setProperty(NMRProcessor.EXCHANGEOKAY, Boolean.valueOf(ok));
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0049: PB-RP: doMEPOk MessagingException: {0}", mEx.getMessage()),
                     mEx);            
        }
        
        //
        //  If the exchange is going to fail, we need to cleanup allocated resources.
        //
        if (!ok)
        {
            mPB.purgeExchange(id);
        }
    }
    
    void doMEP()
        throws javax.jbi.messaging.MessagingException
    {
        ExchangeEntry      ee = mMIS.getExchangeEntry();
        MessageExchange    me = ee.getMessageExchange();
        ClientConnection   cc = ee.getClientConnection();
        String             id = me.getExchangeId();
        long               byteCount = ee.getBytesReceived();
        boolean            last;
        
        //
        // Perform any first time processing on the exchange.
        //
//        mLog.log(Level.INFO,
//                  mLoc.t("0050: PB-RP: Read Id({0}) From ({1}) Bytes({2})", id, cc.getInstanceId(), byteCount);
//        mLog.log(Level.INFO, mLoc.t("0051: PB-RP: ME is {0}\n", me.toString()));
        if (ee.checkState(ExchangeEntry.STATE_FIRST))
        {
            //mLog.log(Level.INFO, mLoc.t("0052: PB-RP: New Id({0}) From ({1})", id, cc.getInstanceId()));
            if (me.getEndpoint() == null)
            {
                throw new javax.jbi.messaging.MessagingException(
                    mLoc.t("0091: Endpoint not found for service ({0})", ee.getService()));
            }
            ee.setState(ExchangeEntry.STATE_ONGOING);
        }
        else
        {
             //mLog.info(mLoc.t("0053: PB-RP: Ongoing Id({0})", id));                  
        }                

        //
        // Check if the send() will cause the NMR to stop tracking the exchange.
        //
//        mLog.info(mLoc.t("0054: PB-RP: Send Id({0})", id));
        last = !me.getStatus().equals(ExchangeStatus.ACTIVE);
        try
        {
            mChannel.send(me);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            //
            // Status may have changed from ACTIVE or ERROR so check if me need
            // to cleanup.
            //
            if (!((com.sun.jbi.ext.DeliveryChannel)mChannel).isActive(me))
            {
                mPB.purgeExchange(id);
                throw mEx;
            }
        }
        
        //
        //  Cleanup exchange that isn't being tracked by the NMR anymore.
        //
        if (last)
        {
//            mLog.info(mLoc.t("0055: PB-RP: forget Id({0})", id));
            mPB.purgeExchange(id);
        }
    }
    
    void handleError(Exception e)
    {
        ExchangeEntry       ee;
        ClientConnection    cc;
        String              id;
        
        id = mMIS.getExchangeId();
        ee = mPB.getExchangeEntry(id);
        
        //
        //  Ignore this message if we don't know anything about it. Could be a stale message from
        //  the most recent restart.
        //
        if (ee != null)
        {
            cc = ee.getClientConnection();
            if (cc != null)
            {
                //
                //  Send error indication back to the remote NMR based component.
                //
                try
                {
                    mLog.warning(mLoc.t("0056: PB-RP: forward exception Id({0}) Reason({1})", id, e.toString()));
                    cc.send(new MEPOutputStream(mPB).writeException(id, e));
                }
                catch (javax.jbi.JBIException jEx)
                {
                    //
                    //  Ignore problems sending.
                    //
                }
            }
        
            //
            //  Send error indication back to local NMR based component.
            //  This is only possible if we have an ongoing conversation with this ME.
            //
            try
            {
                MessageExchange     me = ee.getMessageExchange();
                
                mLog.warning(mLoc.t("0057: PB-RP: set exception Id({0}) Reason({1})", id, e.toString()));
                if (me != null)
                {
                    me.setError(e);
                    mChannel.send(me);
                }
            }
            catch (javax.jbi.JBIException jEx)
            {
                //
                //  Ignore problems sending.
                //
                mLog.warning(mLoc.t("0058: PB-RP: ignore exception Id({0}) Reason({1})",
                                    id, jEx.toString()));
            }
            catch (java.lang.IllegalStateException isEx)
            {
                //
                //  Ignore problems sending.
                //
                mLog.warning(mLoc.t("0058: PB-RP: ignore exception Id({0}) Reason({1})", id, isEx.toString()));
            }
        }
        mLog.warning(mLoc.t("0059: PB-RP: dropping exchange Id({0}) Reason({1})", id, e.toString()));
        mPB.purgeExchange(id);
    }
    
    void logExceptionInfo(String reason, Throwable t)
    {
        StringBuffer                    sb = new StringBuffer();
        java.io.ByteArrayOutputStream   b;
        Throwable                       nextT = t;
        
        sb.append(reason);
        while (t != null)
        {
            if (nextT != null)
            {
                java.io.PrintStream ps = new java.io.PrintStream(b = new java.io.ByteArrayOutputStream());
                t.printStackTrace(ps);
                sb.append(" Exception (");
                sb.append(b.toString());
                sb.append(") ");    
            }
            nextT = t.getCause();
            if (nextT != null)
            {
                sb.append("Caused by: ");
            }
            t = nextT;
        }
        
        mLog.warning(sb.toString());        
    }

}
 
