/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

import com.sun.jbi.bundle.proxy.connection.EventInfo;

/**
 * Connection as a Client.
 * @author Sun Microsystems, Inc
 */
public interface EventConnection 
{
    void sendEvent(EventInfo e)
        throws com.sun.jbi.bundle.proxy.connection.EventException;
    
    void sendEventTo(EventInfo e, String id)
        throws com.sun.jbi.bundle.proxy.connection.EventException;
    
    EventInfo receiveEvent(long timeout)
        throws  com.sun.jbi.bundle.proxy.connection.EventException;
}
