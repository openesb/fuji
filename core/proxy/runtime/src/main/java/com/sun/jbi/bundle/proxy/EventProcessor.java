/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy;

import com.sun.jbi.bundle.proxy.connection.ConnectionManager;
import com.sun.jbi.bundle.proxy.connection.EventConnection;
import com.sun.jbi.bundle.proxy.connection.EventInfo;
import com.sun.jbi.bundle.proxy.connection.EventInfoFactory;
import com.sun.jbi.bundle.proxy.connection.HelloInfo;
import com.sun.jbi.bundle.proxy.connection.JoinInfo;
import com.sun.jbi.bundle.proxy.connection.LeaveInfo;

import com.sun.jbi.messaging.events.AbstractEvent;
import com.sun.jbi.messaging.events.EndpointEvent;
import com.sun.jbi.messaging.events.ExternalEndpointEvent;
import com.sun.jbi.messaging.events.ServiceConnectionEvent;
import com.sun.jbi.messaging.events.InterfaceConnectionEvent;

import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.text.SimpleDateFormat;

import javax.jbi.servicedesc.ServiceEndpoint;

import  org.osgi.framework.ServiceRegistration;
import  org.osgi.service.event.Event;
import  org.osgi.service.event.EventConstants;
import  org.osgi.service.event.EventHandler;

/**
 * Performs processing of events exchanged by the Proxy Bindings.
 * @author Sun Microsystems, Inc
 */
class EventProcessor
        implements  java.lang.Runnable,
                     EventHandler
{
    private HashMap<String,HelloInfo>  mInstances;
    private String                     mId;
    private String                     mMasterId;
    private LinkedList<AbstractEvent>   mQueuedEvents;
    
    /**
     * Our logger.
     */
    private Logger                  mLog;
    private Localizer               mLoc;
    private ServiceRegistration     mEventSR;    
    private ProxyBinding            mPB;
    private ConnectionManager       mCM;
    private ServiceEndpoint         mService;
    private EventConnection         mEC;
    private boolean                 mRunning = true;
    private boolean                 mPrepared = false;
    int                             mEventsReceived;
    int                             mEventsSent;
    
    /**
     * Name of our ACTIVATE endpoint operation.
     */
    static final String             ACTIVATE = "Activate";
    
    /**
     * Name of our DEACTIVATE endpoint operation.
     */
    static final String             DEACTIVATE = "Deactivate";

    /**
     * Constructor for the EventProcessor.
     * @param proxyBinding that we are running under
     */
    EventProcessor(ProxyBinding proxyBinding)
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        EventInfoFactory            efi;
        
        mPB = proxyBinding;
        mLog = mPB.getLogger("event");        
        mService = proxyBinding.getService();
        mCM = proxyBinding.getConnectionManager();
        mEC = mCM.getEventConnection();
        mInstances = new HashMap();
       
        //
        //  Register the events that we can accept.
        //
        efi = EventInfoFactory.getInstance();
        efi.registerEventInfo(RegistrationInfo.class, RegistrationInfo.EVENTNAME);
        efi.registerEventInfo(JoinInfo.class, JoinInfo.EVENTNAME);
        efi.registerEventInfo(LeaveInfo.class, LeaveInfo.EVENTNAME);
        efi.registerEventInfo(ConnectionsInfo.class, ConnectionsInfo.EVENTNAME);
        
    }
    
    private long getProperty(String prop, String defaultValue)
    {
        long    value;
        
        try
        {
            value = new Integer(System.getProperty(prop, defaultValue)).longValue();
        }
        catch (java.lang.NumberFormatException nfEx)
        {
            value = new Integer(defaultValue).longValue();
        }
        return (value);
    }
    
    /**
     * Stop the event processor on the next event or timeout.
     */
    void stop()
    {
        mRunning = false;
        mEventSR.unregister();
    }
    
    HashMap getInstances()
    {
        return ((HashMap)mInstances.clone());
    }
    
    void setupEventHandler()
    {
        String [] topics = new String[] { AbstractEvent.ALL_TOPICS };
        Dictionary d = new Hashtable();

        d.put(EventConstants.EVENT_TOPIC, topics );
        mEventSR = mPB.getBundleContext().registerService( EventHandler.class.getName(),
                this, d );        
    }
    
    public void handleEvent(Event e)
    {
        AbstractEvent   ae = null;

        if (EndpointEvent.isEndpointEvent(e))
        {
           ae = EndpointEvent.valueOf(e); 
        }
        else if (ExternalEndpointEvent.isExternalEndpointEvent(e))
        {
           ae = ExternalEndpointEvent.valueOf(e); 
        }
        else if (ServiceConnectionEvent.isServiceConnectionEvent(e))
        {
           ae = ServiceConnectionEvent.valueOf(e); 
        }
        else if (InterfaceConnectionEvent.isInterfaceConnectionEvent(e))
        {
           ae = InterfaceConnectionEvent.valueOf(e); 
        }
        if (ae != null && 
                !ae.getOwner().equals(ProxyBinding.SUNPROXYBINDINGSERVICE) &&
                !ae.getOwner().equals(mPB.mBundleId))
        { 
            //
            //  Atomically decide to queue or process events.
            //
            synchronized (this)
            {
                if (mQueuedEvents != null)
                {
                    mQueuedEvents.add(ae);
                    ae = null;
                }
            }
            if (ae != null)
            {
                mPB.handleEndpointEvent(ae);
            }
        }
    }
    
    Exception prepare()
    {
        AbstractEvent[]                 aes; 
        com.sun.jbi.ext.DeliveryChannel dc = (com.sun.jbi.ext.DeliveryChannel)mPB.getDeliveryChannel();
        
        try
        {
            //
            //  Before we enable Event's to come streaming in, prepare to queue them.
            //
            mQueuedEvents = new LinkedList();
            
            //
            //  Start the event handler.
            //
            setupEventHandler();
            
            //
            //  Copy all NMR Endpoint Events into the Proxy Binding.
            //
            aes = dc.getAllEndpointEvents();
            for (AbstractEvent ae : aes)
            {
                mPB.handleEndpointEvent(ae);
            }
            
            //
            //  Copy Endpoint Events queued during this window into the Proxy Binding.
            //
            for (;;)
            {
                AbstractEvent       ae;

                //
                //  Atomically pull from queue until empty.
                //
                synchronized (this)
                {
                    ae = mQueuedEvents.poll();                   
                    if (ae == null)
                    {
                        mQueuedEvents = null;
                        break;
                    }
                    ae = mQueuedEvents.getFirst();
                }
                mPB.handleEndpointEvent(ae);                
            }

            mCM.start(mPB);
            mPrepared = true;
        }
        catch (Exception eX)
        {
            return (eX);
        }
        return (null);
    }
    
    /**
     * Main processing loop
     * Basic flow:
     *      Read next write event or timeout.
     *      Process event (if available)
     *      Handle timeout (if happened)
     */
    public void run() 
    {
        mLog.info(mLoc.t("0001: PB-EP:EventProcessor starting."));
        mPB.startComplete(prepare());

        //
        // Basic processing loop: Wait for next event...
        //
        for (;mRunning;)
        {                
            try
            {
                EventInfo               ei = null;        
                
                //
                //  Wait for next event or timeout.
                //
                ei = mEC.receiveEvent(0);
                
                //
                //  Process event if we got one.
                //
                if (ei != null)
                {   
                    mEventsReceived++;
                    processEvent(ei);
                }
                
            }
            catch (com.sun.jbi.bundle.proxy.connection.EventException eEx)
            {
                mLog.log(Level.INFO,
                         mLoc.t("0002: PB-EP: EventException: {0}", eEx.getMessage()), eEx);
                mPB.stop();
            }
            catch (Exception ex)
            {
                mLog.log(Level.INFO,
                         mLoc.t("0003: PB-EP: Exception: {0}", ex.getMessage()), ex);
                ex.printStackTrace();
            }
        }
    }    
    
//
// ------------------- Methods handling event generation and processing  ------------------
//    
       
    void processEvent(EventInfo ei)
    {
        String      sender = ei.getInstanceId();
//        mLog.info(mLoc.t("0004: PB-EP:EventProcessor processEvent: {0} from {1}", ei.getEventName(), sender));
       
        //
        //  Skip any events that we sent.
        //
        if (sender == null || !sender.equals(mId))
        {
            if (ei instanceof RegistrationInfo)
            {
                handleRegistrationEvent((RegistrationInfo)ei);
            }
            else if (ei instanceof ConnectionsInfo)
            {
                handleConnectionsEvent((ConnectionsInfo)ei);
            }
            else if (ei instanceof HelloInfo)
            {
                handleHelloEvent((HelloInfo)ei);
            }
            else if (ei instanceof JoinInfo)
            {
                handleJoinEvent((JoinInfo)ei);
            }
            else if (ei instanceof LeaveInfo)
            {
                handleLeaveEvent((LeaveInfo)ei);
            }
            mLog.info(mLoc.t("0005: PB-EP: {0}", mPB.toString()));
        }
    }
    
   
    void handleRegistrationEvent(RegistrationInfo ri)
    {
        String      instance = ri.getInstanceId();
        HelloInfo   hi = mInstances.get(instance);
        
        try
        {
//            mLog.info(mLoc.t("0006: PB-EP:Registration {0} : {1},{2},{3}", ri.getAction(), ri.getServiceName(), ri.getEndpointName(), ri.getInstanceId()));
            if (!hi.addQueuedEvent(ri, ri.getSerial()))
            {
                mPB.processRemoteEndpoint(ri);
            }
        }
        catch (javax.jbi.JBIException mEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0007: PB-EP:error handling registration event: {0}", mEx.getMessage()), mEx);
        }
    }
    
    void handleConnectionsEvent(ConnectionsInfo ci)
    {
        String      instance = ci.getInstanceId();
        HelloInfo   hi = mInstances.get(instance);
        
        try
        {
//            mLog.info(mLoc.t("0008: PB-EP:Service connection {0} : {1},{2},{3},{4}", ci.getAction(), ci.getServiceName(), ci.getEndpointName(), ci.getServiceLinkName(), ci.getEndpointLinkName(), ci.getLink()));
            if (!hi.addQueuedEvent(ci, ci.getSerial()))
            {
                mPB.processRemoteConnection(ci);
            }
        }
        catch (javax.jbi.JBIException mEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0009: PB-EP:error handling connection event: {0}", mEx.getMessage()), mEx);
        }
    }
    
    void handleHelloEvent(HelloInfo hi)
    {
        LinkedList          eps = mPB.getLocalEndpoints();
        LinkedList          events = new LinkedList();
        
        mLog.info(mLoc.t("0010: PB-EP:Hello from {0} \n", hi.getInstanceId()));
        
        //
        //  Extract the Event information.
        //
        for (Object o : eps)
        {
            if (o instanceof RegistrationInfo)
            {
                events.add(((RegistrationInfo)o).getEndpoint());
            }
            else if (o instanceof ConnectionsInfo)
            {
                events.add(((ConnectionsInfo)o).getEndpoint());
            }
        }
        
        mInstances.put(hi.getInstanceId(), hi);
        JoinInfo   ji = new JoinInfo(mPB.getInstanceId(), events);
        sendEventTo(ji, hi.getInstanceId());
    }
    
    void handleJoinEvent(JoinInfo ji)
    {
        String          instance = ji.getInstanceId();
        HelloInfo       hi = mInstances.get(instance);
        long            serialNumber = 0;
        
        try
        {
            mLog.info(mLoc.t("0011: PB-EP:Join from {0} \n", instance));

            LinkedList<Object> eps = ji.getEndpoints();

            //
            //  Process set of Endpoint sent with Join.
            //
            for (Object o : eps)
            {
                if (o instanceof EndpointEvent)
                {
                    mPB.processRemoteEndpoint(new RegistrationInfo((EndpointEvent)o, instance));
                }
                else if (o instanceof ServiceConnectionEvent)
                {
                    mPB.processRemoteConnection(new ConnectionsInfo((ServiceConnectionEvent)o, instance));
                }
                else if (o instanceof Long)
                {
                    serialNumber = ((Long)o).longValue();
                }                    
            }
            
            //
            //  Process any Endpoints that appeared since the Join.
            //
            hi.setSerialNumber(serialNumber);
            for (Object o;;)
            {
                o = hi.getQueuedEvent();
                if (o == null)
                {
                    break;
                }
                if (o instanceof RegistrationInfo)
                {
                    mPB.processRemoteEndpoint((RegistrationInfo)o);
                }
                else if (o instanceof ConnectionsInfo)
                {
                    mPB.processRemoteConnection((ConnectionsInfo)o);
                }
            }
        }
        catch (javax.jbi.JBIException mEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0012: PB-EP:error handling join event: {0}", mEx.getMessage()), mEx);
        }
    }
    
    void handleLeaveEvent(LeaveInfo li)
    {
        mLog.info(mLoc.t("0013: PB-EP:Leave from {0} \n", li.getInstanceId()));
	try
	{
            mInstances.remove(li.getInstanceId());
	    mPB.purgeRemoteEndpointsForInstance(li.getInstanceId());
	    mPB.purgeRemoteConnectionsForInstance(li.getInstanceId());
	}
	catch (javax.jbi.JBIException ignore)
        {
        }
    }
    
    void sendEvent(EventInfo ei)
    {
        try
        {
//            mLog.info(mLoc.t("0014: PB-EP:Send event {0} \n", ei.getEventName());

            if (mPrepared)
            {
                mEventsSent++;
                mEC.sendEvent(ei);
            }
        }
        catch (com.sun.jbi.bundle.proxy.connection.EventException eEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0015: PB-EP:sendEvent failed: {0}", eEx.getMessage()), eEx);            
        }
    }
    
    void sendEventTo(EventInfo ei, String instance)
    {
        try
        {
//            mLog.info(mLoc.t("0016: PB-EP:Send event {0} To {1}\n", ei.getEventName(), instance));
            if (mPrepared)
            {
                mEventsSent++;
                mEC.sendEventTo(ei, instance);
            }
        }
        catch (com.sun.jbi.bundle.proxy.connection.EventException eEx)
        {
            mLog.log(Level.INFO,
                     mLoc.t("0017: PB-EP:sendEventTo failed: {0}", eEx.getMessage()), eEx);            
        }
    }
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        SimpleDateFormat    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        
        sb.append("  Event Processor\n");
        sb.append("    Events         Sent(");
        sb.append(mEventsSent);
        sb.append(")  Received(");
        sb.append(mEventsReceived);
        sb.append(")\n    Instances Count(");
        sb.append(mInstances.size());
        sb.append(")\n");
        for (HelloInfo hi : mInstances.values())
        {
            sb.append("      " + sdf.format(new Date(hi.getBirthtime())));
            sb.append("    " + hi.getInstanceId() + "\n");
        }
        return (sb.toString());
    }
}
