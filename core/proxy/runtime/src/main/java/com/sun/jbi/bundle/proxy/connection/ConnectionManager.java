/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.connection;

import com.sun.jbi.bundle.proxy.ProxyBinding;

/**
 * This interface specifies the interactions between the proxy binding and a 
 * connection manager that manages the connection between JBI instances.
 *
 * @author Sun Microsystems, Inc
 */
public interface ConnectionManager 
{
    /**
     * Start the ConnectionManager.
     */
    void start(ProxyBinding pb)
        throws ConnectionException;
    
    /**
     * Stop the ConnectionManager.
     */
    void stop();
    
    /**
     * Prepare the ConnectionManager for a new Thread.
     * 
     */
    void prepare()
        throws ConnectionException;
    
    /**
     * Creates a ServerConnection.
     * @param instanceId a unique id for this connection.
     * @return ServerConnection to use.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    ServerConnection getServerConnection()
        throws ConnectionException;
 
    /**
     * Get a ClientConnection targetted at a particular instance.
     * @param instanceId a unique id for this JBI instance.
     * @return ClientConnection to use.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    ClientConnection getClientConnection(String instanceId)
        throws ConnectionException;

    /**
     * Get an EventConnection.
     * @param instanceId a unique id for this JBI instance.
     * @return ClientConnection to use.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    EventConnection getEventConnection()
        throws ConnectionException;

    /**
     * Close the EventConnection.
     */
    void closeServerConnection();
    
    /**
     * Close the EventConnection.
     */
    void closeClientConnection();
    
    /**
     * Close the EventConnection.
     */
    void closeEventConnection();
    
    /**
     * Gets the instance id of this connection.,
     * @return String containing instance id.
     */
    String getInstanceId();   
    
    /**
     * Gets the external instance id of this connection.,
     * @return String containing instance id.
     */
    String getExternalInstanceId();   

}
