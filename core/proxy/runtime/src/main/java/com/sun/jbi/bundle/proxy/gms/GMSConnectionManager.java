/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSConnectionManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.gms;

import com.sun.jbi.bundle.proxy.ProxyBinding;

import com.sun.jbi.bundle.proxy.connection.ConnectionManager;
import com.sun.jbi.bundle.proxy.connection.ClientConnection;
import com.sun.jbi.bundle.proxy.connection.EventConnection;
import com.sun.jbi.bundle.proxy.connection.EventInfo;
import com.sun.jbi.bundle.proxy.connection.HelloInfo;
import com.sun.jbi.bundle.proxy.connection.JoinInfo;
import com.sun.jbi.bundle.proxy.connection.LeaveInfo;
import com.sun.jbi.bundle.proxy.connection.ServerConnection;

import com.sun.jbi.bundle.proxy.Localizer;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.HashMap;

import com.sun.enterprise.ee.cms.core.SignalAcquireException;
import com.sun.enterprise.ee.cms.core.SignalReleaseException;
import com.sun.enterprise.ee.cms.core.GroupManagementService;
import com.sun.enterprise.ee.cms.core.GMSConstants;
import com.sun.enterprise.ee.cms.core.GMSFactory;
import com.sun.enterprise.ee.cms.impl.client.JoinNotificationActionFactoryImpl;
import com.sun.enterprise.ee.cms.core.JoinNotificationSignal;
import com.sun.enterprise.ee.cms.impl.client.PlannedShutdownActionFactoryImpl;
import com.sun.enterprise.ee.cms.core.PlannedShutdownSignal;
import com.sun.enterprise.ee.cms.impl.client.FailureSuspectedActionFactoryImpl;
import com.sun.enterprise.ee.cms.core.FailureSuspectedSignal;
import com.sun.enterprise.ee.cms.impl.client.FailureNotificationActionFactoryImpl;
import com.sun.enterprise.ee.cms.core.FailureNotificationSignal;
import com.sun.enterprise.ee.cms.core.Signal;
import com.sun.enterprise.ee.cms.core.CallBack;
import com.sun.enterprise.ee.cms.impl.client.MessageActionFactoryImpl;
import com.sun.enterprise.ee.cms.core.MessageSignal;
import com.sun.enterprise.ee.cms.core.GMSException;

/**
 * Implements a JMS instance of the ConnectionManager contract.
 * @author Sun Microsystems, Inc
 */
public class GMSConnectionManager
        implements ConnectionManager, CallBack, Runnable
{
    private Logger    mLog;
    private Localizer mLoc;
    
    private GroupManagementService  mGMS;
    private GMSEventConnection      mEC;
    private GMSServerConnection     mSC;
    private HashMap                 mClientConnections;
    private String                  mId;
    private String                  mExternalId;
    private static final String     sGroupName = "SunJBI-GMS-PB";
    private Thread                  mThread;
    
    //
    //  Version of Message format
    //
    private static final int        VERSION_1 = 1;
    
    //
    //  Version_1 Message Types
    //
    private static final int        MS_EVENT = 1;
    private static final int        MS_MESSAGE = 2;
    
    /**
     * Creates a new connection manager.
     * Four connections to the JMS resource are used:
     *      The SendEvent connection is used to send events (Many synchronized).
     *      The ReceiveEvent connection is used to receive events(EventProcessor)
     *      The Server connection is used to receive messages (RemoteProcessor).
     *      The Client connection is used to send messages (NMRProcessor).
     * Each connection is used by a single thread (see-above).
     * Three connections are used due to a restriction on connection when using the JMS RA.
     *
     * @param id with unique instance id
     * @param host of our JMS resource
     * @param port on the host of our JMS resource
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public GMSConnectionManager(String id) 
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException 
    {
        mId = mExternalId = id;
        mEC = new GMSEventConnection(this);
        mSC = new GMSServerConnection(this);
        mClientConnections = new HashMap();
//        mId = sanitizeName(id);
    }
    
    public void start(ProxyBinding pb)
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException 
    {
        mLog = pb.getLogger("gms");
        mLoc = Localizer.get();
        mLog.info(mLoc.t("0060: PB-GMS:Star GMS connection manager: ExternalId({0}) InternalId({1})", mExternalId, mId));
        
        try
        {
            mGMS = (GroupManagementService)GMSFactory.startGMSModule(mId, sGroupName,
                GroupManagementService.MemberType.CORE, null);
            
            //register for group events

            //register to receive notification when a process joins the group
            mGMS.addActionFactory(new JoinNotificationActionFactoryImpl(this));

            //register to receive notification when a group member leaves on a planned shutdown
            mGMS.addActionFactory(new PlannedShutdownActionFactoryImpl(this));

            //register to receive notification when a group member is suspected to have failed
            mGMS.addActionFactory(new FailureSuspectedActionFactoryImpl(this));

            //register to receive notification when a group member is confirmed failed
            mGMS.addActionFactory(new FailureNotificationActionFactoryImpl(this));

            //register to receive messages from other group members to this registered component
            mGMS.addActionFactory(new MessageActionFactoryImpl(this), "EventProcessor");
            mGMS.addActionFactory(new MessageActionFactoryImpl(this), "RemoteProcessor");

            mThread = new Thread(this);
            mThread.isDaemon();
            mThread.start();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void stop()
    {      
        //leaves the group gracefully
        mGMS.shutdown(GMSConstants.shutdownType.INSTANCE_SHUTDOWN); 
    }
    
    public void prepare()
    {
    }

    public void run()
    {
        //joins the group
        try 
        {
           mGMS.join();
        } 
        catch (GMSException e) 
        { 
             e.printStackTrace();
        } 
    }
    
    
    //------------------- Callback ----------------------------------
    
    public void processNotification(Signal signal)
    {
        try
        {
            signal.acquire();
            if (signal instanceof MessageSignal) 
            {
                MessageSignal       ms = (MessageSignal)signal;
                
                String  component = ms.getTargetComponent();

                if (component.equals("EventProcessor"))
                {
                    // Post event to EventProcessor
//                    mLog.info(mLoc.t("0061: PB-GMS:Event received from: {0}", signal.getMemberToken()));
                    mEC.postEvent(ms);
                }
                else if (component.equals("RemoteProcessor"))
                {
                    // Post event to RemoteProcessor
//                    mLog.info(mLoc.t("0062: PB-GMS:Message received from: {0}",
//                        signal.getMemberToken()));
                    mSC.postMessage(ms);
                }
                else
                {
                    mLog.info(mLoc.t("0063: PB-GMS:Unknown component ({0})", component));
                }
            }
            else if (signal instanceof JoinNotificationSignal)
            {
                mLog.info(mLoc.t("0064: PB-GMS: Join notification received from: {0}", signal.getMemberToken()));
                if (!signal.getMemberToken().equals(mId))
                {
                    HelloInfo   hi = new HelloInfo(signal.getMemberToken(),
                                            System.currentTimeMillis());
                    mEC.queueEvent(hi);
                }
            }
            else if (signal instanceof PlannedShutdownSignal)
            {
                mLog.info(mLoc.t("0065: PB-GMS: Shutdown received from: {0}", signal.getMemberToken()));
                if (!signal.getMemberToken().equals(mId))
                {
                    LeaveInfo   li = new LeaveInfo(signal.getMemberToken(),
                                            System.currentTimeMillis());
                    mEC.queueEvent(li);
                }
            }
            else if (signal instanceof FailureSuspectedSignal)
            {

            }
            else if (signal instanceof FailureNotificationSignal)
            {
                mLog.info(mLoc.t("0066: PB-GMS: Failure received from: {0}", signal.getMemberToken()));
                if (!signal.getMemberToken().equals(mId))
                {
                    LeaveInfo   li = new LeaveInfo(signal.getMemberToken(),
                                            System.currentTimeMillis());
                    mEC.queueEvent(li);
                }
            }
            signal.release();
        }
        catch (Exception e)
        {
            
        }
        
    }
    
    Logger getLogger()
    {
        return (mLog);
    }
    
    /**
     * Create a JMS ServerConnection.
     * @return ServerConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public synchronized ServerConnection getServerConnection()
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        return (mSC);
    }
    
    /**
     * Get a ClientConnection. This may be a recycled connection or a new connection.
     * @param id for this connection
     * @return ClientConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public ClientConnection getClientConnection(String id)
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        GMSClientConnection        cc;
        
        cc = (GMSClientConnection)mClientConnections.get(id);
        if (cc == null)
        {
            cc = new GMSClientConnection(id, this);
            mClientConnections.put(id, cc);
        }
        return (cc);      
    }

    /**
     * Get a ClientConnection. This may be a recycled connection or a new connection.
     * @param id for this connection
     * @return ClientConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public ClientConnection getClientConnection(GMSMessage message)
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        GMSClientConnection        cc;
        
        cc = (GMSClientConnection)mClientConnections.get(message.getSender());
        if (cc == null)
        {
            cc = new GMSClientConnection(message.getSender(), this);
            mClientConnections.put(message.getSender(), cc);
        }
        cc.setMessage(message);
        return (cc);      
    }
    
   /**
     * Get an EventConnection.
     * @return EventConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public synchronized EventConnection getEventConnection()
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        return (mEC);      
    }

    void sendEvent(GMSEvent e)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mGMS.getGroupHandle().sendMessage("EventProcessor", e.getMessage());
            //mLog.info(mLoc.t("0067: PB-GMS:Event broadcast"));
        }
        catch (com.sun.enterprise.ee.cms.core.GMSException gEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(gEx);
        }
    }
    
    void sendEventTo(GMSEvent e, String target)
        throws com.sun.jbi.bundle.proxy.connection.EventException
    {
        try
        {
            mGMS.getGroupHandle().sendMessage(target, "EventProcessor", e.getMessage());
            //mLog.info(mLoc.t("0068: PB-GMS:Event sent to: {0}", target));
        }
        catch (com.sun.enterprise.ee.cms.core.GMSException gEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.EventException(gEx);
        }
    }

    void sendMessage(GMSMessage m)
        throws com.sun.jbi.bundle.proxy.connection.ConnectionException
    {
        try
        {
            mGMS.getGroupHandle().sendMessage(m.getSender(), "RemoteProcessor", m.getMessage());
//            mLog.info(mLoc.t("0069: PB-GMS:Message sent to: {0}", m.getSender()));
        }
        catch (com.sun.enterprise.ee.cms.core.GMSException gEx)
        {
            throw new com.sun.jbi.bundle.proxy.connection.ConnectionException(gEx);
        }
    }

    /**
     * Close the EventConnection.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    public void closeServerConnection()
    {
        mSC = null;
    }
    
    /**
     * Close the EventConnection.
     */
    public void closeClientConnection()
    {
        mClientConnections.clear();
    }
    
    /**
     * Close the EventConnection.
     */
    public void closeEventConnection()
    {
        mEC = null;
    }
      
    /**
     * Gets the instance id of this connection.,
     * @return String containing instance id.
     */
    public String getInstanceId()
    {
        return (mId);
    }
    
    /**
     * Gets the instance id of this connection.,
     * @return String containing instance id.
     */
    public String getExternalInstanceId()
    {
        return (mExternalId);
    }
    
    /**
     * Takes an external name and converts it into a viable JMS Queue name.
     * @param is to be sanitized
     * @return String containing the sanitized id.
     */
    String sanitizeName(String id)
    {
        StringBuffer        sb = new StringBuffer();
        
        for (int i = 0; i < id.length(); i++)
        {
            char    ch = id.charAt(i);
            
            if (ch == '@')
            {
                sb.append("_at_");
            }
            else if (ch == '_')
            {
                sb.append("__");
            }
            else if (ch == '.')
            {
                sb.append("_dot_");
            }
            else if (ch == ':')
            {
                sb.append("_colon_");
            }
            else if (ch == '-')
            {
                sb.append("_minus_");
            }
            else
            {
                sb.append(ch);
            }
        }
        
        return (sb.toString());
    }
}
