/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Calculate.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.bundle.proxy.stats;

import com.sun.jbi.bundle.proxy.ExchangeEntry;

public class Calculate
        extends StatBase
        implements java.lang.Cloneable
{
    private int     mField;
    private int     mDisplay;
    private long    count;
    private long    min;
    private long    max;
    private double  sum;
    private double  squaredsum;
    
    public static final int CNT = 1;
    public static final int MIN = 2;
    public static final int MAX = 4;
    public static final int SUM = 8;
    public static final int AVG = 16;
    public static final int STD = 32;

    /** Creates a new instance of Calculate */
    public Calculate(int field) 
    {
        mField = field;
        mDisplay = MIN | MAX | SUM | AVG;
    }
    
    public Calculate(int field, int display) 
    {
        mField = field;
        mDisplay = display;
    }
    
    public void apply(ExchangeEntry entry)
    {
        long    value;
        
        count++;
        
        if (mField != 0)
        {
            value = entry.getLongField(mField);

            sum += value;
            if ((mDisplay & STD) != 0)
            {
                squaredsum += (double)value * (double)value;
            }
            if (count == 1)
            {
                min = max = value;
            }
            else
            {
                if (value < min)
                {
                    min = value;
                }
                else if (value > max)
                {
                    max = value;
                }
            }
        }
        super.apply(entry);
    }
    
    public void clear()
    {
        count = 0;
        min = 0;
        max = 0;
        sum = 0.0;
        squaredsum = 0.0;
        super.clear();
    }
    
    public String report(int depth)
    {
        StringBuffer    sb = new StringBuffer();
        
        for (int j = 0; j < depth; j++)
        {
            sb.append("  ");
        }
        if (mField != 0)
        {
            sb.append(ExchangeEntry.fieldToString(mField));
        }
        if ((mDisplay & CNT) != 0)
        {
            sb.append(" C(");
            sb.append(count);
            sb.append(")");
        }
        if ((mDisplay & MIN) != 0)
        {
            sb.append(" M(");
            sb.append(min);
            sb.append(")");
        }
        if ((mDisplay & MAX) != 0)
        {
            sb.append(" X(");
            sb.append(max);
            sb.append(")");
        }
        if ((mDisplay & SUM) != 0)
        {
            sb.append(" T(");
            sb.append(sum);
            sb.append(")");
        }
        if ((mDisplay & AVG) != 0)
        {
            sb.append(" V(");
            sb.append((double)((long)(sum/count * 100)) / 100.);
            sb.append(")");
        }
        if ((mDisplay & STD) != 0)
        {
            sb.append(") S(");
            sb.append((double)((long)(Math.sqrt((1.0 / (count - 1)) * (squaredsum - (sum * sum / count))) * 100)) / 100.);
            sb.append(")");
        }
        sb.append("\n");
        if (mChild != null)
        {
            sb.append(mChild.report(depth + 1));
        }
        if (mPeer != null)
        {
            sb.append(mPeer.report(depth));
        }
        return (sb.toString());
    }
    
}
