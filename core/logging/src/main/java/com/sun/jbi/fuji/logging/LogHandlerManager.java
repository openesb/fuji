/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LogHandlerManager.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.logging;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.SimpleFormatter;

import org.osgi.framework.BundleContext;


/** 
 * This class is used to create a custom log handler for Fuji.
 */
public class LogHandlerManager {
    

    /**
     * reference to the custom log handler
     */
    private static Handler customLogHandler_;
    
    /**
     * logger instance
     */
    private static Logger logger_ = Logger.getLogger(LogHandlerManager.class.getPackage().getName());
   
    /**
     * localizer instance
     */
    private static Localizer sLoc_ = Localizer.get();

    /**
     * the system property for configuring the log dir 
     */
    private static final String FUJI_LOG_DIR = "fuji.log.dir";
    
    /**
     * the system property for configuring the log file
     */
    private static final String FUJI_LOG_FILE = "fuji.log.file";
    
    /**
     * the default log dir
     */
    private static final String DEFAULT_LOG_DIR = System.getProperty("user.dir") + File.separator + "logs";
                  
    /**
     * singleton LogHandlerManager
     */
    private static LogHandlerManager logHandlerManager_;
    
    
    /**
     * reference to bundlecontext
     */
    private static BundleContext bundleCtx_;
    
    /**
     * constructor 
     */
    public LogHandlerManager(BundleContext bundleCtx) {
        bundleCtx_ = bundleCtx;
    }
    
    
    /**
     * This method is used to get the singletone LogHandlerManager
     */
    public static LogHandlerManager getLogHandlerManager(BundleContext bundleContext) {
        
        if (logHandlerManager_ == null) {
            logHandlerManager_ = new LogHandlerManager(bundleContext);
        }
        return logHandlerManager_;    
        
    }
            
            
    
    /**
     * This method is used to add a custom file handler to the root logger
     */
    public static void addCustomHandler() {
        
        
        //no-op if the property fuji.log.file is not set. log goes to console
        if (!isFileHandlerRequested()) {
            return;
        }

        
        
        Logger rootLogger = Logger.getLogger("");
                              
        Handler handlers[] = rootLogger.getHandlers();
        for (Handler handler:handlers) {   
           rootLogger.removeHandler(handler); 
        }  
                         
        //create the custom handler and store a reference for removal when needed
        Handler customLogHandler_ = createCustomFileHandler();        
        
        //add the custom handler
        if (customLogHandler_ != null) {
            rootLogger.addHandler(customLogHandler_);
        }
        
        
    }
    
    
    
    /**
     * This method is used to create a custom file handler that allows log entries
     * of ALL levels to be written to a file.
     * @return Handler a custom log handler
     */
    public static Handler createCustomFileHandler() {
                                
        try {
            
            File logFile = locateLogFile();
            
            //create a filehandler with nolimit and append=true
            FileHandler fileHandler = new FileHandler(
                    logFile.getAbsolutePath(),
                    0,1,true);
            
            fileHandler.setLevel(Level.ALL);
            fileHandler.setFormatter(new SimpleFormatter());
            return fileHandler;
        }
        catch (Exception ex)
        {
            logger_.log(Level.WARNING,
                        sLoc_.t("0001:  Failed to create a custom log handler."), ex);
            return null;
        }

    }
    
    /**
     * This method is used to get the location of the log file
     */
    private static File locateLogFile() {
        
        String logDirName = bundleCtx_.getProperty(FUJI_LOG_DIR);
        if (logDirName == null || logDirName.trim().equals("")) {
            logDirName = DEFAULT_LOG_DIR;
        }
        File logDir = new File(logDirName);
                
        if (!logDir.exists()) {
            logDir.mkdirs();          
        }
        
        String logFile = bundleCtx_.getProperty(FUJI_LOG_FILE);
        return new File(logDir, logFile);
        
    }
    
    /**
     * This method is used to remove the custom log handler from the root logger
     */
    public static void removeCustomHandler() {
        
        Logger rootLogger = Logger.getLogger("");

        if (customLogHandler_ != null) {
            rootLogger.removeHandler(customLogHandler_);
        }   
    }
        
    
    /**
     * This method is used to determine if logs should be shown in console or
     * redirected to file. If the property fuji.log.file is not set then the
     * log entries are shown in the console
     */
    private static boolean isFileHandlerRequested() {
        
        String logFileName = bundleCtx_.getProperty(FUJI_LOG_FILE);
        
        if (logFileName == null || logFileName.trim().equals("")) {
            return false;
        } 
        return true;
        
    }
            
                
}




