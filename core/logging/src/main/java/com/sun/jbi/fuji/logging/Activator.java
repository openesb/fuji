/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Activator.java - Last published on 3/13/2008
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.logging;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;

/**
 * This activator is used to register a managed service for managing the
 * log level of various loggers in Fuji.
 */
public class Activator implements BundleActivator {
    
    
    /**
     * reference to the LogService ServiceRegistration
     */
    ServiceRegistration logLevelConfigServiceRef_;
    
    
    /**
     * This method is used to add custom log handlers and to
     * register the log level config service.
     * @param bundleCtx the bundle context
     */
    public void start(BundleContext bundleCtx) {
        
        //1.add custom log handlers
        LogHandlerManager.getLogHandlerManager(bundleCtx).addCustomHandler();
        

    }
       
    /**
     * This method is used to remove the custom log handlers and to 
     * unregister the loglevelconfig service.
     * @param bundleCtx the bundle context
     */    
    public void stop(BundleContext bundleCtx) {
        
        //remove custom log handlers        
        LogHandlerManager.getLogHandlerManager(bundleCtx).removeCustomHandler();
        
                    
    }

}
