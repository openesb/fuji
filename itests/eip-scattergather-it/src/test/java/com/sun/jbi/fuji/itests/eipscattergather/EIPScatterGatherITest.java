/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPScatterGatherITest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.eipscattergather;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.Properties;
import org.apache.maven.it.Verifier;

/**
 * Integration test for ScatterGather EIP
 * @author chikkala
 */
public class EIPScatterGatherITest
        extends BaseITest {
    Verifier    verifier;
    String      savedMEgroupid;
    String      savedNMgroupid;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EIPScatterGatherITest(String testName) {
        super(testName, "eipScatterGatherTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "in");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "r1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "r2");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "r3");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "r3e");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out");

        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/hol/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();
        String fileArchiveTestDir = (new File(fileTestDir, "archive")).getAbsolutePath();

        updateFileServiceConfigProperties("in", null, "file-%d.xml", "true", fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("r1", null, "r1-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("r2", null, "r2-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("r3", null, "r3-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("r3e", null, "r3e-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("out", null, "out-%d.xml", "true", fileArchiveTestDir, null, null, null);

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        this.resetVerifier(verifier);
        this.executeDeployGoal(verifier);

        sleep(5000);

        // perfrom runtime test
        copyTestDataToProjectDir("file-testdata/file-1.xml", new File(fileInTestDir, "file-1.xml").getAbsolutePath());
        sleep(2000);
        verifier.assertFilePresent(new File(fileArchiveTestDir, "out-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r2-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r3-0.xml").getAbsolutePath());
        verifier.assertFileNotPresent(new File(fileArchiveTestDir, "r3e-0.xml").getAbsolutePath());

        copyTestDataToProjectDir("file-testdata/file-2.xml", new File(fileInTestDir, "file-2.xml").getAbsolutePath());
        sleep(2000);
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r1-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r2-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r3-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "r3e-0.xml").getAbsolutePath());
        verifier.assertFileNotPresent(new File(fileArchiveTestDir, "out-1.xml").getAbsolutePath());

        // undeploy the app
        this.resetVerifier(verifier);
        this.executeUnDeployGoal(verifier);

        this.resetVerifier(verifier);
    }

    void checkProperties(File f, int sequence) {
        verifier.assertFilePresent(f.getAbsolutePath());
        Properties p = extractProperties(f);
        String  seq = Integer.toString(sequence);
        String  mid = p.getProperty("MessageExchangeId");
        String  megroupid = p.getProperty("com.sun.jbi.messaging.groupid");
        String  memessageid = p.getProperty("com.sun.jbi.messaging.messageid");
        String  nmgroupid = p.getProperty("org.glassfish.openesb.messaging.groupid");
        String  nmmessageid = p.getProperty("org.glassfish.openesb.messaging.messageid");

        if (savedMEgroupid == null) {
            savedMEgroupid = megroupid;
        } else {
            if (megroupid.equals(savedMEgroupid)) {
            throw new RuntimeException("File: " + f.getAbsolutePath() + " Should have ME groupid: " + savedMEgroupid);

            }
        }
        if (savedNMgroupid == null) {
            savedNMgroupid = megroupid;
        } else {
            if (megroupid.equals(savedNMgroupid)) {
            throw new RuntimeException("File: " + f.getAbsolutePath() + " Should have ME groupid: " + savedNMgroupid);

            }
        }
        if (!megroupid.endsWith("-" + seq)) {
            throw new RuntimeException("File: " + f.getAbsolutePath() + " Should have ME groupid ending with: " + "-" + seq);
        }
        if (!memessageid.equals("1")) {
            throw new RuntimeException("File: " + f.getAbsolutePath() + " Should have ME messageid: " + "1");
        }
        if (!nmmessageid.equals(seq)) {
            throw new RuntimeException("File: " + f.getAbsolutePath() + " Should have NM messageid: " + seq);
        }
    }
}
