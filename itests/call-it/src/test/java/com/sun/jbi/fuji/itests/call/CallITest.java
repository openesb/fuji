/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.call;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import org.apache.maven.it.Verifier;

/**
 * Integration test for all eip's using file and jruby services
 * @author chikkala
 */
public class CallITest
        extends BaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CallITest(String testName) {
        super(testName, "callTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "in");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out");
        // assert pojo service is generated
        String pojoServiceFilePath = "src/main/java/pojo/" +  "Svcinvoker.java";
        verifier.assertFilePresent(pojoServiceFilePath);

        // overwrite the pojo service.
        copyTestDataToProjectDir("pojo/Svcinvoker.java", "src/main/java/pojo/Svcinvoker.java");
        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/hol/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();

        updateFileServiceConfigProperties("in", null, "file-in.xml", null, fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("out", null, "out.xml", null, fileOutTestDir, null, null, null);

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        this.resetVerifier(verifier);
        this.executeDeployGoal(verifier);

        sleep(5000);

        // perfrom runtime test
        copyTestDataToProjectDir("file-testdata/file-in.xml", new File(fileInTestDir, "file-in.xml").getAbsolutePath());

        sleep(10000);
        // validate test results
        verifier.assertFilePresent(new File(fileOutTestDir, "out.xml").getAbsolutePath());
        // undeploy the app
        this.resetVerifier(verifier);
        this.executeUnDeployGoal(verifier);

        this.resetVerifier(verifier);
    }
}
