package pojo;

import org.glassfish.openesb.pojose.api.Consumer;
import org.glassfish.openesb.pojose.api.ErrorMessage;
import org.glassfish.openesb.pojose.api.annotation.ConsumerEndpoint;
import org.glassfish.openesb.pojose.api.annotation.Provider;
import org.glassfish.openesb.pojose.api.annotation.Resource;
import org.glassfish.openesb.pojose.api.annotation.Operation;
import org.glassfish.openesb.pojose.api.res.Context;
import org.w3c.dom.Node;

@Provider(name="invoker_endpoint", serviceQN="{http://fuji.dev.java.net/application/callTest/private}invoker",
        interfaceQN="{http://fuji.dev.java.net/application/callTest}callTest_interface")
public class Svcinvoker {
    // POJO Context
    @Resource
    private Context jbiCtx;
    @ConsumerEndpoint(name="partnerA_endpoint",
            serviceQN="{http://fuji.dev.java.net/application/callTest/private}partnerA",
            interfaceQN="{http://fuji.dev.java.net/application/callTest}callTest_interface",
            operationQN="{http://fuji.dev.java.net/application/callTest}oneWay",
            inMessageTypeQN="{http://fuji.dev.java.net/application/callTest}anyMsg")
    private Consumer clientEp;
    public Svcinvoker() {
    }

    @Operation (outMessageTypeQN="{http://fuji.dev.java.net/application/callTest}anyMsg")
    public void receive(Node input) throws ErrorMessage {
        // Enter your code goes here.
        clientEp.sendInOnly(input);
    }
}

