/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SimpleITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.simpleit;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Integration test for a simple route 
 * @author chikkala
 */
public class SimpleITest
    extends BaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SimpleITest(String testName) {
        super(testName, "simpleTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);

        verifier.assertFilePresent( "src/main/config/file/f1/service.properties" );
        verifier.assertFilePresent( "src/main/config/file/f2/service.properties" );
        this.resetVerifier(verifier);

        this.executeDistGoal(verifier);
        verifier.assertFilePresent( "target/felix/bundle/simpleTest-" + fujiVersion + ".jar" );
        verifier.assertFilePresent( "target/felix/bundle/sun-encoder-library-" + fujiVersion +".jar" );
        verifier.assertFilePresent( "target/felix/bundle/sun-file-binding-" + fujiVersion + ".jar" );

        this.resetVerifier(verifier);
    }
}
