/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPSelectITest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.eipselect;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Integration test for Select EIP
 * @author chikkala
 */
public class EIPSelectITest
        extends BaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EIPSelectITest(String testName) {
        super(testName, "eipSelectTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "in");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "c1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "b1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "e1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "t1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "n1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "j1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "x1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out");

        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/hol/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();
        String fileArchiveTestDir = (new File(fileTestDir, "archive")).getAbsolutePath();

        updateFileServiceConfigProperties("in", null, "file-f%d.xml", "true", fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("out", null, "out-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("c1", null, "c1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("b1", null, "b1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("e1", null, "e1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("t1", null, "t1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("n1", null, "n1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("j1", null, "j1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("x1", null, "x1-%d.xml", "true", fileOutTestDir, null, null, null);

    //config-id:myConfig
    //app-ns: http:///fuji/.dev.java.net/application/my-maven-project
    //type: select
    //name: dynamic-select
    //config:
    //  - type: xpath
    //    condition: "//../...>10"
    //    to: endpoint-1
    //  - type: xpath
    //    condition: "//../...<10"
    //    to: named-route-x
	Properties p = new Properties();
        p.setProperty("defaultRules",
                "config-id:defaultConfig\n" +
                "app-ns: http://fuji.dev.java.net/application/ \n" +
                "type: select \n" +
                "name: named-select\n" +
                "config : \n" +
                " - type : xpath\n" +
                "   condition: \"/Trade/Type=\\\"BOND\\\"\" \n" +
                "   to: \"{http://fuji.dev.java.net/application/eipSelectTest/private}to-bond-1\" \n" +
                " - type : xpath\n" +
                "   condition: \"/Trade/Type=\\\"CASH\\\"\" \n" +
                "   to: \"{http://fuji.dev.java.net/application/eipSelectTest/private}to-cash-1\" \n" +
                " \n");
        updateFlowConfigProperties(SELECT_FLOW, "named-select", p);

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        this.resetVerifier(verifier);
        this.executeDeployGoal(verifier);

        sleep(5000);

        // perfrom runtime test
        copyTestDataToProjectDir("file-testdata/file-f1.xml", new File(fileInTestDir, "file-f1.xml").getAbsolutePath());
	sleep(500);
        copyTestDataToProjectDir("file-testdata/file-f2.xml", new File(fileInTestDir, "file-f2.xml").getAbsolutePath());
	sleep(500);
        copyTestDataToProjectDir("file-testdata/file-f3.xml", new File(fileInTestDir, "file-f3.xml").getAbsolutePath());
	sleep(500);
        copyTestDataToProjectDir("file-testdata/file-f4.xml", new File(fileInTestDir, "file-f4.xml").getAbsolutePath());
	sleep(500);
        copyTestDataToProjectDir("file-testdata/file-f5.xml", new File(fileInTestDir, "file-f5.xml").getAbsolutePath());
	sleep(500);
        copyTestDataToProjectDir("file-testdata/file-f6.xml", new File(fileInTestDir, "file-f6.xml").getAbsolutePath());
        sleep(10000);

        // validate test results
        verifier.assertFilePresent(new File(fileArchiveTestDir, "out-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "out-1.xml").getAbsolutePath());
        verifier.assertFileNotPresent(new File(fileArchiveTestDir, "out-2.xml").getAbsolutePath());

        verifier.assertFilePresent(new File(fileOutTestDir, "c1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "b1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "e1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "c1-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "b1-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "e1-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "t1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "t1-1.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "n1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "e1-2.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "e1-3.xml").getAbsolutePath());
        verifier.assertFileNotPresent(new File(fileOutTestDir, "e1-4.xml").getAbsolutePath());
        verifier.assertFileNotPresent(new File(fileOutTestDir, "j1-0.xml").getAbsolutePath());

        // undeploy the app
        this.resetVerifier(verifier);
        this.executeUnDeployGoal(verifier);

        this.resetVerifier(verifier);
    }
}
