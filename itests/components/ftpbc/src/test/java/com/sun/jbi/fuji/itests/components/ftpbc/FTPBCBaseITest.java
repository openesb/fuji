/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FTPBCBaseITest.java
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.ftpbc;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.util.Properties;
import org.apache.maven.it.Verifier;

/**
 * Base class for integration tests for FTP BC
 * @author jfu
 */
public abstract class FTPBCBaseITest extends BaseITest {

    public static final String FTP_SERVICE_TYPE = "ftp";

    public FTPBCBaseITest(String testName, String appName) {
        super(testName, appName);
    }

    protected void assertServiceConfigFile(Verifier verifier,
            String serviceType,
            String serviceName) throws Exception {

        if (FTP_SERVICE_TYPE.equals(serviceType)) {

            String configPath = "src/main/config";
            String configFile = "service.properties";

            configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;
            verifier.assertFilePresent(configPath);

        } else {
            super.assertServiceConfigFile(verifier, serviceType, serviceName);
        }
    }

    /**
     * for ftp:message
     * @param serviceName
     * @param messageRepository
     * @param messageNamePrefixIB
     * @param messageNamePrefixOB
     * @param messageName
     * @param user
     * @param passwd
     * @param host
     * @param port
     * @param dirListStyle
     * @param mode
     * @param pollInterval
     * @throws Exception
     */
    protected void updateFTPServiceConfigProperties(String serviceName,
            String messageRepository,
            String messageNamePrefixIB,
            String messageNamePrefixOB,
            String messageName,
            String user,
            String passwd,
            String host,
            String port,
            String dirListStyle,
            String mode,
            String pollInterval) throws Exception {

        Properties props = new Properties();
        // comment out ftp:transfer
        // use ftp:message
        props.setProperty("transfer", "false");
        if (messageRepository != null) {
            props.setProperty("ftp.messageRepository", messageRepository.trim());
        }
        if (messageNamePrefixIB != null) {
            props.setProperty("ftp.messageNamePrefixIB", messageNamePrefixIB.trim());
        }
        else {
            props.setProperty("ftp.messageNamePrefixIB", "req.");
        }
        if (messageNamePrefixOB != null) {
            props.setProperty("ftp.messageNamePrefixOB", messageNamePrefixOB.trim());
        }
        else {
            props.setProperty("ftp.messageNamePrefixOB", "resp.");
        }
        if (messageName != null) {
            props.setProperty("ftp.messageName", messageName.trim());
        }
        else {
            props.setProperty("ftp.messageName", "%u");
        }
        if (user != null) {
            props.setProperty("ftp.user", user.trim());
        }
        if (passwd != null) {
            props.setProperty("ftp.password", passwd.trim());
        }
        if (host != null) {
            props.setProperty("ftp.host", host.trim());
        } else {
            props.setProperty("ftp.host", "");
        }
        if (port != null) {
            props.setProperty("ftp.port", port.trim());
        }
        if (dirListStyle != null) {
            props.setProperty("ftp.dirListStyle", dirListStyle.trim());
        } else {
            props.setProperty("ftp.dirListStyle", "UNIX");
        }
        if (mode != null) {
            props.setProperty("ftp.mode", mode.trim());
        } else {
            props.setProperty("ftp.mode", "BINARY");
        }
        if (pollInterval != null) {
            props.setProperty("ftp.pollIntervalMillis", pollInterval.trim());
        } else {
            props.setProperty("ftp.pollIntervalMillis", "5000");
        }
        updateServiceConfigProperties(FTP_SERVICE_TYPE, serviceName, props);
    }
    /**
     * for ftp:transfer
     * @param serviceName
     * @param recvFrom
     * @param recvFromDir
     * @param isRegex
     * @param user
     * @param passwd
     * @param host
     * @param port
     * @param dirListStyle
     * @param mode
     * @param pollInterval
     * @throws Exception
     */
    protected void updateFTPServiceConfigProperties4TransferIB(String serviceName,
            String recvFrom,
            String recvFromDir,
            String isRegex,
            String user,
            String passwd,
            String host,
            String port,
            String dirListStyle,
            String mode,
            String pollInterval) throws Exception {

        Properties props = new Properties();
        props.setProperty("transfer", "true");
        if (recvFrom != null) {
            props.setProperty("ftp.receiveFrom", recvFrom.trim());
        }
        if (isRegex != null) {
            props.setProperty("ftp.receiveFromHasRegexs", isRegex.trim());
        }
        // post operation
        props.setProperty("ftp.postReceiveCommand", "RENAME");
        props.setProperty("ftp.postReceiveLocation", recvFromDir + "/SAVED/%f");
        props.setProperty("ftp.postReceiveLocationHasPatterns", "true");

        if (user != null) {
            props.setProperty("ftp.user", user.trim());
        }
        if (passwd != null) {
            props.setProperty("ftp.password", passwd.trim());
        }
        if (host != null) {
            props.setProperty("ftp.host", host.trim());
        } else {
            props.setProperty("ftp.host", "");
        }
        if (port != null) {
            props.setProperty("ftp.port", port.trim());
        }
        if (dirListStyle != null) {
            props.setProperty("ftp.dirListStyle", dirListStyle.trim());
        } else {
            props.setProperty("ftp.dirListStyle", "UNIX");
        }
        if (mode != null) {
            props.setProperty("ftp.mode", mode.trim());
        } else {
            props.setProperty("ftp.mode", "BINARY");
        }
        if (pollInterval != null) {
            props.setProperty("ftp.pollIntervalMillis", pollInterval.trim());
        } else {
            props.setProperty("ftp.pollIntervalMillis", "5000");
        }
        updateServiceConfigProperties(FTP_SERVICE_TYPE, serviceName, props);
    }

    /**
     * for ftp:transfer
     * @param serviceName
     * @param sendTo
     * @param sendToDir
     * @param isPattern
     * @param user
     * @param passwd
     * @param host
     * @param port
     * @param dirListStyle
     * @param mode
     * @throws Exception
     */
    protected void updateFTPServiceConfigProperties4TransferOB(String serviceName,
            String sendTo,
            String sendToDir,
            String isPattern,
            String user,
            String passwd,
            String host,
            String port,
            String dirListStyle,
            String mode) throws Exception {

        Properties props = new Properties();
        props.setProperty("transfer", "true");
        if (sendTo != null) {
            props.setProperty("ftp.sendTo", sendTo.trim());
        }
        if (isPattern != null) {
            props.setProperty("ftp.sendToHasPatterns", isPattern.trim());
        }
        // post operation
        props.setProperty("ftp.postSendCommand", "RENAME");
        props.setProperty("ftp.postSendLocation", sendToDir + "/%f");
        props.setProperty("ftp.postSendLocationHasPatterns", "true");

        if (user != null) {
            props.setProperty("ftp.user", user.trim());
        }
        if (passwd != null) {
            props.setProperty("ftp.password", passwd.trim());
        }
        if (host != null) {
            props.setProperty("ftp.host", host.trim());
        } else {
            props.setProperty("ftp.host", "");
        }
        if (port != null) {
            props.setProperty("ftp.port", port.trim());
        }
        if (dirListStyle != null) {
            props.setProperty("ftp.dirListStyle", dirListStyle.trim());
        } else {
            props.setProperty("ftp.dirListStyle", "UNIX");
        }
        if (mode != null) {
            props.setProperty("ftp.mode", mode.trim());
        } else {
            props.setProperty("ftp.mode", "BINARY");
        }
        updateServiceConfigProperties(FTP_SERVICE_TYPE, serviceName, props);
    }
}
