/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DBQueryTest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.ftpbc;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;

/**
 * FTP put and get test
 * @author jfu
 */
public class FTPTest extends FTPBCBaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FTPTest(String testName) {
        super(testName, "ftpServiceTest");
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {       
        super.tearDown();
    }
    

    protected void executeTestsOnGeneratedApp() throws Exception {
        /* Start server, if any */
        FtpServer server = startFTPServer();

        if ( server == null )
            throw new Exception("Failed to start FTP server ...");

        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);

        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "local_file_poll");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "local_file_write");
        this.assertServiceConfigFile(verifier, FTP_SERVICE_TYPE, "remote_ftp_put");
        this.assertServiceConfigFile(verifier, FTP_SERVICE_TYPE, "remote_ftp_poll");

        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(),
                                        "target/files")).getAbsolutePath();

        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();

        updateFileServiceConfigProperties("local_file_poll",
                                          null, 
                                          "test_data_%d.xml",
                                          "true",
                                          fileInTestDir, 
                                          null, 
                                          null, 
                                          null);

        updateFileServiceConfigProperties("local_file_write",
                                          null, 
                                          "output_file_%d.xml",
                                          "true", 
                                          fileOutTestDir, 
                                          null, 
                                          null, 
                                          null);

//        updateFTPServiceConfigProperties("remote_ftp_put",
//                                              "FUJI_FTP_ITEST_DIR",
//                                              "msg.",
//                                              "msg.",
//                                              "%u",
//                                              "anonymous",
//                                              "",
//                                              "localhost",
//                                              "9921",
//                                              "UNIX",
//                                              "BINARY",
//                                              "5000");
//
//        updateFTPServiceConfigProperties("remote_ftp_poll",
//                                              "FUJI_FTP_ITEST_DIR",
//                                              "msg.",
//                                              "msg.",
//                                              "%u",
//                                              "anonymous",
//                                              "",
//                                              "localhost",
//                                              "9921",
//                                              "UNIX",
//                                              "BINARY",
//                                              "5000");

        updateFTPServiceConfigProperties4TransferOB("remote_ftp_put",
                                              "FUJI_FTP_ITEST_DIR/STAGED/target%0.txt",
                                              "FUJI_FTP_ITEST_DIR",
                                              "true",
                                              "anonymous",
                                              "",
                                              "localhost",
                                              "9921",
                                              "UNIX",
                                              "BINARY");

        updateFTPServiceConfigProperties4TransferIB("remote_ftp_poll",
                                              "FUJI_FTP_ITEST_DIR/target[0-9]+\\.txt",
                                              "FUJI_FTP_ITEST_DIR",
                                              "true",
                                              "anonymous",
                                              "",
                                              "localhost",
                                              "9921",
                                              "UNIX",
                                              "BINARY",
                                              "5000");

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        
        // deploy
        try {
            
            this.resetVerifier(verifier);
            this.executeDeployGoal(verifier);

            sleep(5000);

            // perfrom runtime test
            // feed inbound messages
            copyTestFilesToProjectDir("inbound_messages",
                                     new File(fileInTestDir).getAbsolutePath());

            sleep(50000);
            // validate test results
            verifier.assertFilePresent(new File(fileOutTestDir, "output_file_0.xml").getAbsolutePath());
            verifier.assertFilePresent(new File(fileOutTestDir, "output_file_1.xml").getAbsolutePath());
            verifier.assertFilePresent(new File(fileOutTestDir, "output_file_2.xml").getAbsolutePath());

            this.resetVerifier(verifier);
            
        } finally {            
            // undeploy the app
            this.resetVerifier(verifier);
            this.executeUnDeployGoal(verifier);
            if ( server != null )
                server.stop();
        }
        
        /* Stop external server here, if any */
    }

    private FtpServer startFTPServer() throws Exception {
        FtpServerFactory serverFactory = new FtpServerFactory();

        ListenerFactory factory = new ListenerFactory();

        // set the port of the listener
        factory.setPort(9921);

//        // define SSL configuration
//        SslConfigurationFactory ssl = new SslConfigurationFactory();
//        ssl.setKeystoreFile(new File("src/test/resources/ftpserver.jks"));
//        ssl.setKeystorePassword("password");
//
//        // set the SSL configuration for the listener
//        factory.setSslConfiguration(ssl.createSslConfiguration());
//        factory.setImplicitSsl(true);

        // replace the default listener
        serverFactory.addListener("default", factory.createListener());
        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
        File userFile = new File("users.properties");
        if ( !userFile.exists() ) {
            throw new Exception("Missing ftp configuration: " + userFile.getName());
        }
        
        File ftpRoot = new File("ftproot");
        
        if ( ftpRoot.exists() ) {
            FileUtils.deleteDirectory(ftpRoot);
        }

        ftpRoot.mkdir();
        userManagerFactory.setFile(userFile);
        
        UserManager um = userManagerFactory.createUserManager();

//        BaseUser user = new BaseUser();
//        user.setName("ftptest");
//        user.setPassword("password");
//        user.setHomeDirectory("ftproot");
//        AuthorizationRequest request;
//        user.authorize(null);
//        um.save(user);

        serverFactory.setUserManager(um);

        // start the server
        FtpServer server = serverFactory.createServer();
        try {
            server.start();
        } catch (FtpException ex) {
            server = null;
            ex.printStackTrace();
            Logger.getLogger(FTPTest.class.getName()).log(Level.SEVERE, "Exception caught when starting server...", ex);
        }
        return server;
    }
}
