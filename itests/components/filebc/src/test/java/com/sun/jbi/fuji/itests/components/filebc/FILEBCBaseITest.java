/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FTPBCBaseITest.java
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.filebc;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.util.Properties;
import org.apache.maven.it.Verifier;

/**
 * Base class for integration tests for FILE BC
 * @author jfu
 */
public abstract class FILEBCBaseITest extends BaseITest {

    public static final String FILE_SERVICE_TYPE = "file";

    public FILEBCBaseITest(String testName, String appName) {
        super(testName, appName);
    }

    protected void assertServiceConfigFile(Verifier verifier,
            String serviceType,
            String serviceName) throws Exception {

        if (FILE_SERVICE_TYPE.equals(serviceType)) {

            String configPath = "src/main/config";
            String configFile = "service.properties";

            configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;
            verifier.assertFilePresent(configPath);

        } else {
            super.assertServiceConfigFile(verifier, serviceType, serviceName);
        }
    }
}
