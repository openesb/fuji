/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DBQueryTest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.filebc;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;

/**
 * FTP put and get test
 * @author jfu
 */
public class FILETest extends FILEBCBaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FILETest(String testName) {
        super(testName, "fileServiceTest");
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {       
        super.tearDown();
    }
    

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);

        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "file_in");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "file_out");

        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(),
                                        "target/files")).getAbsolutePath();

        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();

        // for now, just do a oneway in -> oneway out route
        // do on-demand and inout later
//    protected void updateInboundFileServiceConfigProperties(String serviceName,
//            String msgTypePrimitive,
//            String msgType,
//            String schemaLoc,
//            String use,
//            String encodingStyle,
//            String fileName,
//            String isPattern,
//            String isRegex,
//            String charSet,
//            String fileType,
//            String isMultiRecord,
//            String pollingInterval,
//            String recordDelimeter,
//            String forwardAsAttachment,
//            String removeEOL,
//            String maxBytesPerRecord,
//            String archive,
//            String archiveDir,
//            String archiveDirRelative,
//            // from this point on - mapped from file:address
//            String fileDirectory,
//            String isRelative,
//            String relativeTo,
//            String lockName,
//            String seqName,
//            String workArea,
//            String persistBaseDir) throws Exception {}
        updateInboundFileServiceConfigProperties("file_in",
                "true",
                "anyType",
                null,
                null,
                null,
                "test_data_%d.xml",
                "true",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                fileInTestDir,
                null,
                null,
                null,
                null,
                null,
                null);

//    protected void updateOutboundFileServiceConfigProperties(String serviceName,
//            String msgTypePrimitive,
//            String msgType,
//            String schemaLoc,
//            String use,
//            String encodingStyle,
//            String fileName,
//            String isPattern,
//            String charSet,
//            String fileType,
//            String addEOL,
//            String isMultiRecord,
//            String protect,
//            String protectDir,
//            String protectDirRelative,
//            String stage,
//            String stageDir,
//            String stageDirRelative,
//            // from this point on - mapped from file:address
//            String fileDirectory,
//            String isRelative,
//            String relativeTo,
//            String lockName,
//            String seqName,
//            String workArea,
//            String persistBaseDir) throws Exception {}
        updateOutboundFileServiceConfigProperties("file_out",
                null,
                null,
                "output_file_%d.xml",
                "true",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                fileOutTestDir,
                null,
                null,
                null,
                null,
                null,
                null);


        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        try {
            
            this.resetVerifier(verifier);
            this.executeDeployGoal(verifier);

            sleep(5000);

            // perfrom runtime test
            // feed inbound messages
            copyTestFilesToProjectDir("inbound_messages",
                                     new File(fileInTestDir).getAbsolutePath());

            sleep(50000);
            // validate test results
            verifier.assertFilePresent(new File(fileOutTestDir, "output_file_0.xml").getAbsolutePath());
            verifier.assertFilePresent(new File(fileOutTestDir, "output_file_1.xml").getAbsolutePath());
            verifier.assertFilePresent(new File(fileOutTestDir, "output_file_2.xml").getAbsolutePath());

            this.resetVerifier(verifier);
            
        } finally {            
            // undeploy the app
            this.resetVerifier(verifier);
            this.executeUnDeployGoal(verifier);
        }
        
        /* Stop external server here, if any */
    }
}
