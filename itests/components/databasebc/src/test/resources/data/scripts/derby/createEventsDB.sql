
connect 'jdbc:derby://localhost:1600/cluedin-events-db;create=true' user 'app' password 'app';

create table events (
  event_id INT NOT NULL GENERATED ALWAYS AS IDENTITY CONSTRAINT event_pk PRIMARY KEY,
  event_date DATE NOT NULL, 
  event_time TIME NOT NULL, 
  event_name VARCHAR(250) NOT NULL, 
  description VARCHAR(500) DEFAULT NULL,
  status INT NOT NULL DEFAULT 1);


insert INTO events (event_date,event_time,event_name,description) 
       VALUES(
         '05/24/2009', '18:00','Lakers Game 2','2009 Western conference finals');

insert INTO events (event_date,event_time,event_name,description) 
       VALUES(
         '05/24/2009', '12:30','Lakers Game 3','2009 Western conference finals');

insert INTO events (event_date,event_time,event_name,description) 
       VALUES(
         '05/26/2009', '18:00','Lakers Game 4','2009 Western conference finals');


