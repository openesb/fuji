/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DatabaseBCBaseITest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.databasebc;

import com.sun.jbi.fuji.itests.base.BaseITest;
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
import java.util.Properties;
import org.apache.maven.it.Verifier;
//import org.apache.maven.it.util.FileUtils;
//import org.apache.maven.it.util.ResourceExtractor;

/**
 * Base class for integration tests for Database BC
 * @author npiega
 */
public abstract class DatabaseBCBaseITest extends BaseITest {

    public static final String DATABASE_SERVICE_TYPE = "database";
    
    public DatabaseBCBaseITest(String testName, String appName) {
        super(testName, appName);
    }
    
    protected void assertServiceConfigFile(Verifier verifier, 
                                           String serviceType, 
                                           String serviceName) throws Exception {
        
        if (DATABASE_SERVICE_TYPE.equals(serviceType)) {
            
            String configPath = "src/main/config";
            String configFile = "service.properties";

            configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;
            verifier.assertFilePresent(configPath);
            
        } else {            
            super.assertServiceConfigFile(verifier, serviceType, serviceName);            
        }
    }
    

    /*
        jdbc.operationType=select
        #jndi handle for datasource
        jdbc.jndiName=jdbc/__defaultDS
        #sql statement, e.g. INSERT INTO TEST (NAME, ID) values (?,?)
        jdbc.sql=select event_name, event_time, description from events
        #jdbc.sql=select event_name, event_time, description from events where event_time           BETWEEN TIME(RTRIM(CAST (HOUR(CURRENT_TIME)+1 AS CHAR(2))) || ':00:00') AND           TIME(RTRIM(CAST (HOUR(CURRENT_TIME)+2 AS CHAR(2))) || ':00:00')
        #parameter order, e.g. NAME, ID
        jdbc.paramOrder=event_name,event_time,description
        #table name
        jdbc.TableName=events
    */
    protected void updateDatabaseServiceConfigProperties(String serviceName,
                                                         String operationType,
                                                         String jndiName,
                                                         String sql,
                                                         String paramOrder,
                                                         String tableName) throws Exception {
        Properties props = new Properties();

        if (operationType != null) {
            props.setProperty("jdbc.operationType", operationType);
        }
        if (jndiName != null) {
            props.setProperty("jdbc.jndiName", jndiName);
        }
        if (sql != null) {
            props.setProperty("jdbc.sql", sql);
        }
        if (paramOrder != null) {
            props.setProperty("jdbc.paramOrder", paramOrder);
        }
        if (tableName != null) {
            props.setProperty("jdbc.TableName", tableName);
        }
                
        updateServiceConfigProperties(DATABASE_SERVICE_TYPE, serviceName, props);
    }
    

}
