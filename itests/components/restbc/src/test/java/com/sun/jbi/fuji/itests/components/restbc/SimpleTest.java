/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.restbc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Simple integration test for RestBC
 * @author echou
 */
public class SimpleTest extends RestBCBaseITest {

    private static final int JERSERY_SERVER_PORT = 16000;

    private Server jerseyServer = null;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SimpleTest(String testName) {
        super(testName, "restbcSimpleTest");
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {       
        super.tearDown();
    }
    

    protected void executeTestsOnGeneratedApp() throws Exception {
        jerseyServer = new Server(JERSERY_SERVER_PORT);
        jerseyServer.start();
        
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "in_file");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out_file");
        this.assertServiceConfigFile(verifier, JRUBY_SERVICE_TYPE, "map_in");
        this.assertServiceConfigFile(verifier, JRUBY_SERVICE_TYPE, "map_out");
        this.assertServiceConfigFile(verifier, REST_SERVICE_TYPE, "rest_service");

        // update configuration.
        copyTestDataToProjectDir("jruby/map_in/service.rb", "src/main/jruby/map_in/service.rb");
        copyTestDataToProjectDir("jruby/map_out/service.rb", "src/main/jruby/map_out/service.rb");
        
        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();

        updateFileServiceConfigProperties("in_file", null, "in-file.xml", null, fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("out_file", null, "out-file-%d.xml", "true", fileOutTestDir, null, null, null);

        updateRestServiceConfigProperties(
            "rest_service",
            "http://localhost:" + JERSERY_SERVER_PORT + "/simple-resource",
            "GET",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        
        // deploy
        try {
            
            this.resetVerifier(verifier);
            this.executeDeployGoal(verifier);

            sleep(5000);

            // perfrom runtime test
            copyTestDataToProjectDir("file-testdata/in-file.xml", new File(fileInTestDir, "in-file.xml").getAbsolutePath());

            sleep(20000);
            // validate test results
            verifier.assertFilePresent(new File(fileOutTestDir, "out-file-0.xml").getAbsolutePath());


            this.resetVerifier(verifier);
            
        } finally {
            
            // undeploy the app
            this.resetVerifier(verifier);
            this.executeUnDeployGoal(verifier);
            
        }
        
        
        jerseyServer.stop();
        
    }
    

}
