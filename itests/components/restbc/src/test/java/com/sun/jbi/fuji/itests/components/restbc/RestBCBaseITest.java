/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.restbc;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Base class for integration tests for RestBC
 * @author echou
 */
public abstract class RestBCBaseITest extends BaseITest {

    public static final String REST_SERVICE_TYPE = "rest";
    
    public RestBCBaseITest(String testName, String appName) {
        super(testName, appName);
    }
    
    protected void assertServiceConfigFile(Verifier verifier, String serviceType, String serviceName) throws Exception {
        
        if (REST_SERVICE_TYPE.equals(serviceType)) {
            
            String configPath = "src/main/config";
            String configFile = "service.properties";

            configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;
            verifier.assertFilePresent(configPath);
            
        } else {
            
            super.assertServiceConfigFile(verifier, serviceType, serviceName);
            
        }
    }
    
    protected void updateRestServiceConfigProperties(String serviceName,
            String url,
            String method,
            String accept_types,
            String accept_languages,
            String content_type,
            String headers,
            String param_style,
            String params,
            String basic_auth_username,
            String basic_auth_password) throws Exception {
        Properties props = new Properties();

        if (url != null) {
            props.setProperty("http.resource-url", url);
        }
        if (method != null) {
            props.setProperty("http.method", method);
        }
        if (accept_types != null) {
            props.setProperty("http.accept-types", accept_types);
        }
        if (accept_languages != null) {
            props.setProperty("http.accept-languages", accept_languages);
        }
        if (content_type != null) {
            props.setProperty("http.content-type", content_type);
        }
        if (headers != null) {
            props.setProperty("http.headers", headers);
        }
        if (param_style != null) {
            props.setProperty("http.param-style", param_style);
        }
        if (params != null) {
            props.setProperty("http.params", params);
        }
        if (basic_auth_username != null) {
            props.setProperty("http.basic-auth-username", basic_auth_username);
        }
        if (basic_auth_password != null) {
            props.setProperty("http.basic-auth-password", basic_auth_password);
        }
                
        updateServiceConfigProperties(REST_SERVICE_TYPE, serviceName, props);
    }
    

}
