require 'java'
require 'XPath'
require 'ESB'
include_class "javax.xml.namespace.QName"

include_class "java.io.BufferedReader"
include_class "java.io.InputStreamReader"
include_class "javax.xml.parsers.DocumentBuilder"
include_class "javax.xml.parsers.DocumentBuilderFactory"
include_class "org.xml.sax.InputSource"
include_class "org.w3c.dom.Document"
include_class "org.w3c.dom.Node"

@@service = QName.new "http://fuji.dev.java.net/application/[app]", "[service]"

def process(inMsg)
  
  serviceResponse = inMsg.getAttachment("org.glassfish.openesb.rest.attachment")
  reader = BufferedReader.new(InputStreamReader.new(serviceResponse.getInputStream()))

  responseStr = ""
  s = reader.readLine()
  while s
    responseStr = responseStr + s
    s = reader.readLine()
  end
  
  puts responseStr
  
  xmlReader = java.io.StringReader.new(responseStr)
  xmlSource = InputSource.new(xmlReader)
  dbf = DocumentBuilderFactory.newInstance
  dbf.setNamespaceAware(true)
  db = dbf.newDocumentBuilder
  doc = db.parse(xmlSource)
  root = doc.getDocumentElement()
  
  inMsg.setPayload(root)
  
  inMsg
end