package com.sun.jbi.fuji.itests.components.restbc;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.container.grizzly.GrizzlyWebContainerFactory;


public class Server {
    
    private int port;
    private SelectorThread threadSelector;
    
    public Server(int port) {
        this.port = port;
    }
    
    public void start() throws Exception {
        URI baseURI = UriBuilder.fromUri("http://localhost/").port(port).build();
        
        final Map<String, String> initParams = new HashMap<String, String>();
        initParams.put("com.sun.jersey.config.property.packages", 
                "com.sun.jbi.fuji.itests.components.restbc.resources");
        threadSelector = GrizzlyWebContainerFactory.create(baseURI, initParams);
        
        System.out.println(String.format("Jersey server started at: " + baseURI));
        
    }
    
    public void stop() throws Exception {
        threadSelector.stopEndpoint();
    }
    
    
    public static void main(String[] args) {
        Server server = new Server(16000);
        
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
