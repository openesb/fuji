package com.sun.jbi.fuji.itests.components.restbc.resources;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;


/**
 * SimpleResource.java
 *
 * @author Edward Chou
 */
@Path("/simple-resource")
public class SimpleResource {

    @GET
    public String get() {
        return "<data>GET method content</data>";
    }
    
    @POST
    public String post(String s) {
        return "<data>POST method content</data>";
    }
    
}
