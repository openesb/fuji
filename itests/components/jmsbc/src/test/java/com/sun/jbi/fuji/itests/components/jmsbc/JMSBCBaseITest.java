/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DatabaseBCBaseITest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.jmsbc;

import java.util.Properties;

import org.apache.maven.it.Verifier;

import com.sun.jbi.fuji.itests.base.BaseITest;


public abstract class JMSBCBaseITest extends BaseITest {

    public static final String JMS_SERVICE_TYPE = "jms";
    
    public JMSBCBaseITest(String testName, String appName) {
        super(testName, appName);
    }
    
    protected void assertServiceConfigFile(Verifier verifier, 
                                           String serviceType, 
                                           String serviceName) throws Exception {
        
        if (JMS_SERVICE_TYPE.equals(serviceType)) {
            
            String configPath = "src/main/config";
            String configFile = "service.properties";

            configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;
            verifier.assertFilePresent(configPath);
            
        } else {            
            super.assertServiceConfigFile(verifier, serviceType, serviceName);            
        }
    }
    

   
    protected void updateJMSServiceConfigProperties(String serviceName,
                                                         String destinationType,
                                                         String destination,
                                                         String messageType,
                                                         String part,
                                                         String connectionURL) throws Exception {
        Properties props = new Properties();

        
            props.setProperty("jms.destinationType", destinationType);
       
            props.setProperty("jms.destination", destination);
        
            props.setProperty("jms.messageType",messageType);
       
            props.setProperty("jms.textPart",part);
        
            props.setProperty("jms.connectionURL", connectionURL);
            
            props.setProperty("jms.concurrencyMode","cc");            
            props.setProperty("jms.jms.maxConcurrentConsumers","1");
            props.setProperty("jms.transaction","NoTransaction");
            props.setProperty("jms.username","admin");
            props.setProperty("jms.password","admin");
            props.setProperty("jms.jndi.initialContextFactory","");
            props.setProperty("jms.jndi.providerURL","");
            props.setProperty("jms.jndi.securityPrincipal","");
            props.setProperty("jms.jndi.securityCredentials","");
            
            props.setProperty("jms.jmsjcaOptions","");
            


            
        
                
        updateServiceConfigProperties(JMS_SERVICE_TYPE, serviceName, props);
    }
    

}
