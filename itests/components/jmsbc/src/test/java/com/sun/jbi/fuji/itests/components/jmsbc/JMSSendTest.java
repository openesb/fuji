/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DBQueryTest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.components.jmsbc;

import java.io.File;

import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;

public class JMSSendTest extends JMSBCBaseITest {

    /**
     * Create the test case
     *
     * @param testName
     *                name of the test case
     */
    public JMSSendTest(String testName) {
	super(testName, "jmsTest");
    }

    protected void setUp() throws Exception {
	super.setUp();
    }

    protected void tearDown() throws Exception {
	super.tearDown();
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
	/* Start server, if any */

	Verifier verifier = this.createFujiAppVerifier();

	this.executeGenerateArtifactsGoal(verifier);

	// check if the service artifacts are generated
	this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "trigger_file");
	this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out_file");
	this.assertServiceConfigFile(verifier, JMS_SERVICE_TYPE, "jms_service");
	this.assertServiceConfigFile(verifier, JMS_SERVICE_TYPE, "jms_inbound");
	

	// update configuration.
	// e.g. copyTestDataToProjectDir("jruby/map_in/service.rb",
	// "src/main/jruby/map_in/service.rb");

	File prjDir = this.getFujiAppProjectDir();

	String rtTestFolder = (new File(prjDir.getParentFile(), "target/files")).getAbsolutePath();

	System.out.println(rtTestFolder);

	File fileTestDir = new File(rtTestFolder);
	
	//create file
	File inDir = new File(fileTestDir, "in"); inDir.mkdirs();
	
	File srcIn = new File(prjDir.getParentFile().getParentFile(), "data/file-testdata/in-file.xml");
	File dstIn = new File(inDir,"in-file.xml");dstIn.createNewFile();
	
	FileUtils.copyFile(srcIn, dstIn);
	
	
	String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
	String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();

	updateFileServiceConfigProperties("trigger_file", null, "in-file.xml", null, fileInTestDir, null, null, null);

	updateFileServiceConfigProperties("out_file", null, "out-file-%d.xml", "true", fileOutTestDir, null, null, null);

	updateJMSServiceConfigProperties("jms_service", 
		"Queue", "defaultQ", "TextMessage", 
		"part1", "mq://localhost:7676/");
	
	updateJMSServiceConfigProperties("jms_inbound", 
		"Queue", "defaultQ", "TextMessage", 
		"part1", "mq://localhost:7676/");
	
	

	this.resetVerifier(verifier);
	this.executeBuildGoal(verifier);

	this.assertServiceAsseblyFile(verifier);

	// deploy
	try {

	    this.resetVerifier(verifier);
	    this.executeDeployGoal(verifier);

	    sleep(5000);

	    // perfrom runtime test
	    copyTestDataToProjectDir("file-testdata/in-file.xml", new File(fileInTestDir, "in-file.xml").getAbsolutePath());

	    sleep(20000);
	    // validate test results
	    verifier.assertFilePresent(new File(fileOutTestDir, "out-file-0.xml").getAbsolutePath());

	    this.resetVerifier(verifier);

	} finally {
	    // undeploy the app
	    this.resetVerifier(verifier);
	    this.executeUnDeployGoal(verifier);
	}

	/* Stop external server here, if any */
    }
}
