/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPFilterITest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.eiptee;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Integration test for Filter EIP
 * @author chikkala
 */
public class EIPFilterITest
        extends BaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EIPFilterITest(String testName) {
        super(testName, "eipFilterTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "in");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "f1");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "f2");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out");

        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/hol/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();
        String fileArchiveTestDir = (new File(fileTestDir, "archive")).getAbsolutePath();

        updateFileServiceConfigProperties("in", null, "file-f%d.xml", "true", fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("out", null, "out-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("f1", null, "f1-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("f2", null, "f2-%d.xml", "true", fileOutTestDir, null, null, null);

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        this.resetVerifier(verifier);
        this.executeDeployGoal(verifier);

        sleep(5000);

        // perfrom runtime test
        copyTestDataToProjectDir("file-testdata/file-f1.xml", new File(fileInTestDir, "file-f1.xml").getAbsolutePath());
	sleep(2000);
        copyTestDataToProjectDir("file-testdata/file-f2.xml", new File(fileInTestDir, "file-f2.xml").getAbsolutePath());
        sleep(10000);

        // validate test results
        verifier.assertFilePresent(new File(fileArchiveTestDir, "out-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileArchiveTestDir, "out-1.xml").getAbsolutePath());

        verifier.assertFilePresent(new File(fileOutTestDir, "f1-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "f2-0.xml").getAbsolutePath());

        // undeploy the app
        this.resetVerifier(verifier);
        this.executeUnDeployGoal(verifier);

        this.resetVerifier(verifier);
    }
}
