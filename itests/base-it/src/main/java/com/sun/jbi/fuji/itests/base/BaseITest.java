/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BaseITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.base;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import junit.framework.TestCase;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Base Integration test 
 * @author chikkala
 */
public abstract class BaseITest
        extends TestCase {
    // service types

    public static final String FILE_SERVICE_TYPE = "file";
    public static final String JRUBY_SERVICE_TYPE = "jruby";
    // flow types
    public static final String FILTER_FLOW = "filter";
    public static final String SPLIT_FLOW = "split";
    public static final String AGGREGATE_FLOW = "aggregate";
    public static final String SELECT_FLOW = "select";
    public static final String TEE_FLOW = "tee";
    public static final String FUJI_VERSION_PROP = "fuji.currentVersion";
    private static final String BASE_GID = "open-esb.fuji.itests.";
    private static final String BASE_PKG = "com.sun.jbi.fuji.itests.";
    protected static final String APP_VERSION = "1.0-SNAPSHOT";
    protected String fujiVersion;
    protected String testGID;
    protected String testAID;
    protected String testPKG;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BaseITest(String testName, String appName) {
        super(testName);
        fujiVersion = System.getProperty(FUJI_VERSION_PROP);
        testGID = BASE_GID + appName;
        testAID = appName;
        testPKG = BASE_PKG + appName;
    }

    protected List getIAppArcheTypeOptions() {
        List options = new ArrayList();
        //  options.add("archetype:generate");

        options.add("-DarchetypeGroupId=open-esb.fuji");
        options.add("-DarchetypeArtifactId=app-archetype");
        options.add("-DarchetypeVersion=" + fujiVersion);

        options.add("-DgroupId=" + testGID);
        options.add("-DartifactId=" + testAID);
        options.add("-Dversion=" + fujiVersion);
        options.add("-Dpackage=" + testPKG);

        options.add("-Darchetype.interactive=false");

        return options;
    }

    /**
     * creates integrationapp from archetype
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeGenerateFromArchetype(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
//        cliOptions.add( "-N" );
        cliOptions = getIAppArcheTypeOptions();
        verifier.setCliOptions(cliOptions);
        verifier.executeGoal("archetype:generate");
    }

    /**
     *  generates the service configurations from IFL
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeGenerateArtifactsGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        verifier.executeGoal("fuji:generate-artifacts");
    }

    /**
     * build, package and install into local repo
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeCleanGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        verifier.executeGoal("clean");
    }

    /**
     * build, package and install into local repo
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeBuildGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        verifier.executeGoal("install");
    }

    /**
     * deploy and starts the app
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeDeployGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        List goals = new ArrayList();
        goals.add("install");
        goals.add("fuji:deploy-app");
        verifier.executeGoals(goals);
    }

    /**
     * deploy and starts the app
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeUnDeployGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        List goals = new ArrayList();
        // goals.add("install");
        goals.add("fuji:undeploy-app");
        verifier.executeGoals(goals);
    }

    /**
     * standalone application goals
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeDistGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        cliOptions.add("-Pdist");
        verifier.setCliOptions(cliOptions);
        verifier.executeGoal("fuji:dist");
    }

    /**
     * standalone application run goal
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void executeRunGoal(Verifier verifier) throws Exception {
        List cliOptions = new ArrayList();
        verifier.setCliOptions(cliOptions);
        verifier.executeGoal("fuji:run");
    }

    protected File getFujiAppProjectDir() throws Exception {
        File prjDir = ResourceExtractor.simpleExtractResources(getClass(), "/app/" + testAID);
        return prjDir;
    }

    protected File getTestDataDir() throws Exception {
        File dataDir = ResourceExtractor.simpleExtractResources(getClass(), "/data");
        return dataDir;
    }

    /**
     * creates the fuji app verifier and copies the ifl file from test data directory
     * to the ifl directory in the project.
     * @return
     * @throws java.lang.Exception
     */
    protected Verifier createFujiAppVerifier() throws Exception {
        File prjDir = getFujiAppProjectDir();
        Verifier verifier = null;
        verifier = new Verifier(prjDir.getAbsolutePath(), true);

        File iflDest = new File(prjDir, "src/main/ifl/app.ifl");
        File iflSrc = ResourceExtractor.simpleExtractResources(getClass(), "/data/app.ifl");
        FileUtils.copyFile(iflSrc, iflDest);

        return verifier;
    }

    protected void copyTestDataToProjectDir(String srcPath, String destPath) throws Exception {
        File prjDir = getFujiAppProjectDir();
        File destFile = new File(destPath);
        if (!destFile.isAbsolute()) {
            destFile = new File(prjDir, destPath);
        }
        File srcFile = new File(srcPath);
        if (!srcFile.isAbsolute()) {
            srcFile = ResourceExtractor.simpleExtractResources(getClass(), "/data/" + srcPath);
        }
        FileUtils.copyFile(srcFile, destFile);
    }

    protected void copyTestFilesToProjectDir(String srcPath, String destPath) throws Exception {
        File prjDir = getFujiAppProjectDir();
        File destFile = new File(destPath);
        if (!destFile.isAbsolute()) {
            destFile = new File(prjDir, destPath);
        }
        File srcFile = new File(srcPath);
        if (!srcFile.isAbsolute()) {
            srcFile = ResourceExtractor.simpleExtractResources(getClass(), "/data/" + srcPath);
        }
        FileUtils.copyDirectory(srcFile, destFile);
    }

    protected void updateProperties(File propFile, Properties props) throws IOException {
        Properties updatedProps = new Properties();
        FileInputStream fileIn = null;
        FileOutputStream fileOut = null;
        try {
            fileIn = new FileInputStream(propFile);
            updatedProps.load(fileIn);
            fileIn.close();
            fileIn = null;
            for (Enumeration propEnum = props.propertyNames(); propEnum.hasMoreElements();) {
                String name = (String) propEnum.nextElement();
                updatedProps.setProperty(name, props.getProperty(name));
            }
            fileOut = new FileOutputStream(propFile);
            updatedProps.store(fileOut, "Updated properties from integration test");
            fileOut.close();
            fileOut = null;
        } finally {
            if (fileIn != null) {
                try {
                    fileIn.close();
                } catch (IOException ex) {
                    // ex.printStackTrace();
                }
            }
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException ex) {
                    // ex.printStackTrace();
                }
            }
        }
    }

    protected void updateServiceConfigProperties(String serviceType, String serviceName, Properties props) throws Exception {
        String configPath = "src/main/config";
        String configFile = "service.properties";
        if ("jruby".equals(serviceType) || "xslt".equals(serviceType)) {
            throw new Exception("Invalid Service type for updating the configuration properties service type=" + serviceType + " name=" + serviceName);
        }
        configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;

        File prjDir = getFujiAppProjectDir();
        updateProperties(new File(prjDir, configPath), props);
    }

    protected void updateFlowConfigProperties(String flowType, String flowName, Properties props) throws Exception {
        String flowPath = "src/main";
        String flowFile = "flow.properties";
        flowPath += "/" + flowType + "/" + flowName + "/" + flowFile;
        File prjDir = getFujiAppProjectDir();
        updateProperties(new File(prjDir, flowPath), props);
    }

    protected void updateFileServiceConfigProperties(String serviceName,
            String use,
            String fileName,
            String isPattern,
            String fileDirectory,
            String isMultiRecord,
            String pollingInterval,
            String recordDelimeter) throws Exception {
        Properties props = new Properties();

        // set default msg part type
//        props.setProperty("file.msg.type.simple.xsd", "true");
//        props.setProperty("file.msg.type", "anyType");
//        props.setProperty("file.msg.schema.loc", "message.xsd");
        if (use != null) {
            props.setProperty("file.use", use);
        }
        if (fileName != null) {
            props.setProperty("file.fileName", fileName);
        }
        if (pollingInterval != null) {
            props.setProperty("file.pollingInterval", pollingInterval);
        }
        if (fileDirectory != null) {
            props.setProperty("file.fileDirectory", fileDirectory);
        }
        if (isPattern != null) {
            props.setProperty("file.fileNameIsPattern", isPattern);
        }
        if (isMultiRecord != null) {
            props.setProperty("file.multipleRecordsPerFile", isMultiRecord);
        }
        if (recordDelimeter != null) {
            props.setProperty("file.recordDelimiter", recordDelimeter);
        }
        updateServiceConfigProperties(FILE_SERVICE_TYPE, serviceName, props);
    }

    protected void updateInboundFileServiceConfigProperties(String serviceName,
            String msgTypePrimitive,
            String msgType,
            String schemaLoc,
            String use,
            String encodingStyle,
            String fileName,
            String isPattern,
            String isRegex,
            String charSet,
            String fileType,
            String isMultiRecord,
            String pollingInterval,
            String recordDelimeter,
            String forwardAsAttachment,
            String removeEOL,
            String maxBytesPerRecord,
            String archive,
            String archiveDir,
            String archiveDirRelative,
            // from this point on - mapped from file:address
            String fileDirectory,
            String isRelative,
            String relativeTo,
            String lockName,
            String seqName,
            String workArea,
            String persistBaseDir) throws Exception {
        Properties props = new Properties();
// service parameters applicable to file oneway inbound
//file.msg.type.primitive=true
//file.msg.type=string
//file.msg.schema.loc=message.xsd
//file.fileName=file.xml
//file.fileNameIsPattern=false
//file.fileNameIsRegex=false
//file.charset=
//file.fileType=text
//file.use=literal
//file.encodingStyle=
//file.pollingInterval=1000
//file.multipleRecordsPerFile=false
//file.recordDelimiter=
//file.forwardAsAttachment=false
//file.removeEOL=false
//file.maxBytesPerRecord=0
//file.archive=true
//file.archiveDirectory=archive
//file.archiveDirIsRelative=false

        if (use != null) {
            props.setProperty("file.use", use);
        }
        if (fileName != null) {
            props.setProperty("file.fileName", fileName);
        }
        if (pollingInterval != null) {
            props.setProperty("file.pollingInterval", pollingInterval);
        }
        if (isPattern != null) {
            props.setProperty("file.fileNameIsPattern", isPattern);
        }
        if (isMultiRecord != null) {
            props.setProperty("file.multipleRecordsPerFile", isMultiRecord);
        }
        if (recordDelimeter != null) {
            props.setProperty("file.recordDelimiter", recordDelimeter);
        }

        if (msgTypePrimitive != null) {
            props.setProperty("file.msg.type.simple.xsd", msgTypePrimitive);
        }
        if (msgType != null) {
            props.setProperty("file.msg.type", msgType);
        }
        if (schemaLoc != null) {
            props.setProperty("file.msg.schema.loc", schemaLoc);
        }
        if (encodingStyle != null) {
            props.setProperty("file.encodingStyle", encodingStyle);
        }
        if (isRegex != null) {
            props.setProperty("file.fileNameIsRegex", isRegex);
        }
        if (charSet != null) {
            props.setProperty("file.charset", charSet);
        }
        if (fileType != null) {
            props.setProperty("file.fileType", fileType);
        }
        if (pollingInterval != null) {
            props.setProperty("file.pollingInterval", pollingInterval);
        }
        if (forwardAsAttachment != null) {
            props.setProperty("file.forwardAsAttachment", forwardAsAttachment);
        }
        if (removeEOL != null) {
            props.setProperty("file.removeEOL", removeEOL);
        }
        if (maxBytesPerRecord != null) {
            props.setProperty("file.maxBytesPerRecord", maxBytesPerRecord);
        }
        if (archive != null) {
            props.setProperty("file.archive", archive);
        }
        if (archiveDir != null) {
            props.setProperty("file.archiveDirectory", archiveDir);
        }
        if (archiveDirRelative != null) {
            props.setProperty("file.archiveDirIsRelative", archiveDirRelative);
        }
        if (fileDirectory != null) {
            props.setProperty("file.fileDirectory", fileDirectory);
        }
        if (isRelative != null) {
            props.setProperty("file.relativePath", isRelative);
        }
        if (relativeTo != null) {
            props.setProperty("file.pathRelativeTo", relativeTo);
        }
        if (lockName != null) {
            props.setProperty("file.lockName", lockName);
        }
        if (seqName != null) {
            props.setProperty("file.seqName", seqName);
        }
        if (workArea != null) {
            props.setProperty("file.workArea", workArea);
        }
        if (persistBaseDir != null) {
            props.setProperty("file.persistenceBaseLoc", persistBaseDir);
        }

        updateServiceConfigProperties(FILE_SERVICE_TYPE, serviceName, props);
    }

    protected void updateOutboundFileServiceConfigProperties(String serviceName,
            String use,
            String encodingStyle,
            String fileName,
            String isPattern,
            String charSet,
            String fileType,
            String addEOL,
            String isMultiRecord,
            String protect,
            String protectDir,
            String protectDirRelative,
            String stage,
            String stageDir,
            String stageDirRelative,
            // from this point on - mapped from file:address
            String fileDirectory,
            String isRelative,
            String relativeTo,
            String lockName,
            String seqName,
            String workArea,
            String persistBaseDir) throws Exception {
// service parameters applicable to file oneway outbound
//file.msg.type.primitive=true
//file.msg.type=string
//file.msg.schema.loc=message.xsd
//file.fileName=file.xml
//file.fileNameIsPattern=false
//file.charset=
//file.fileType=text
//file.use=literal
//file.encodingStyle=
//file.multipleRecordsPerFile=false
//file.addEOL=false
//file.protect=false
//file.protectDirectory=protect
//file.protectDirIsRelative=false
//file.stage=false
//file.stageDirectory=stage
//file.stageDirIsRelative=false
        Properties props = new Properties();

        if (use != null) {
            props.setProperty("file.use", use);
        }
        if (fileName != null) {
            props.setProperty("file.fileName", fileName);
        }
        if (isPattern != null) {
            props.setProperty("file.fileNameIsPattern", isPattern);
        }
        if (isMultiRecord != null) {
            props.setProperty("file.multipleRecordsPerFile", isMultiRecord);
        }

//        if (msgTypePrimitive != null) {
//            props.setProperty("file.msg.type.simple.xsd", msgTypePrimitive);
//        }
//        if (msgType != null) {
//            props.setProperty("file.msg.type", msgType);
//        }
//        if (schemaLoc != null) {
//            props.setProperty("file.msg.schema.loc", schemaLoc);
//        }
        if (encodingStyle != null) {
            props.setProperty("file.encodingStyle", encodingStyle);
        }
        if (charSet != null) {
            props.setProperty("file.charset", charSet);
        }
        if (fileType != null) {
            props.setProperty("file.fileType", fileType);
        }

        if (protect != null) {
            props.setProperty("file.protect", protect);
        }
        if (protectDir != null) {
            props.setProperty("file.protectDirectory", protectDir);
        }
        if (protectDirRelative != null) {
            props.setProperty("file.protectDirIsRelative", protectDirRelative);
        }

        if (stage != null) {
            props.setProperty("file.stage", stage);
        }
        if (stageDir != null) {
            props.setProperty("file.stageDirectory", stageDir);
        }
        if (stageDirRelative != null) {
            props.setProperty("file.stageDirIsRelative", stageDirRelative);
        }

        if (fileDirectory != null) {
            props.setProperty("file.fileDirectory", fileDirectory);
        }
        if (isRelative != null) {
            props.setProperty("file.relativePath", isRelative);
        }
        if (relativeTo != null) {
            props.setProperty("file.pathRelativeTo", relativeTo);
        }
        if (lockName != null) {
            props.setProperty("file.lockName", lockName);
        }
        if (seqName != null) {
            props.setProperty("file.seqName", seqName);
        }
        if (workArea != null) {
            props.setProperty("file.workArea", workArea);
        }
        if (persistBaseDir != null) {
            props.setProperty("file.persistenceBaseLoc", persistBaseDir);
        }
        updateServiceConfigProperties(FILE_SERVICE_TYPE, serviceName, props);
    }

//
//    <!-- MessageExchangeId: 246503845275256-4672-134704232329320246 -->
//    <!--   MEprop Name: com.sun.jbi.messaging.messageid -->
//    <!--    Value: 1 -->
//
    protected Properties extractProperties(File file) {
        FileInputStream     fis = null;
        InputStreamReader   isr = null;
        Properties          p = new Properties();

        try {
            StringBuffer    sb = new StringBuffer();
            char[]          chars = new char[1024];


            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            for (;;) {
                int count = isr.read(chars);

                if (count <= 0) {
                    break;
                }
                sb.append(chars, 0, count);
            }
            String      buffer = sb.toString();
            Matcher     m = Pattern.compile("<!--   ..prop: (.*) -->\\s+<!--    Value: (.*) -->").matcher(buffer);

            for (;;) {
                if (!m.find()) {
                    break;
                }
                p.setProperty(m.group(1), m.group(2));
            }
            m.reset(buffer);
            m.usePattern(Pattern.compile("<!-- MessageExchangeId: (.*) -->"));
            if (m.find()) {
                p.setProperty("MessageExchangeId", m.group(1));
            }
        } catch (java.io.IOException ignore) {

        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (java.io.IOException ignore) {

                }
            }
        }
        return (p);
    }

    protected void assertServiceConfigFile(Verifier verifier, String serviceType, String serviceName) throws Exception {
        String configPath = "src/main/config";
        String configFile = "service.properties";
        if ("jruby".equals(serviceType)) {
            configPath = "src/main";
            configFile = "service.rb";
        } else if ("xslt".equals(serviceType)) {
            configPath = "src/main";
            configFile = "transform.xsl";
        }
        configPath += "/" + serviceType + "/" + serviceName + "/" + configFile;
        verifier.assertFilePresent(configPath);
    }

    protected void assertFlowConfigFileInSourceDir(Verifier verifier, String flow, String name) throws Exception {
        String flowConfigPath = "src/main/" + flow + "/" + name + "/flow.properties";
        verifier.assertFilePresent(flowConfigPath);
    }

    protected void assertFlowConfigFileInBuildDir(Verifier verifier, String flow, String name) throws Exception {
        String flowConfigPath = "target/classes/META-INF/flow/" + flow + "/" + name + "/flow.properties";
        verifier.assertFilePresent(flowConfigPath);
    }

    protected void assertServiceAsseblyFile(Verifier verifier) throws Exception {
        verifier.assertFilePresent("target/" + testAID + "-" + fujiVersion + ".jar");
    }

    /**
     * verifies for error log and resets the streams.
     * @param verifier
     * @throws java.lang.Exception
     */
    protected void resetVerifier(Verifier verifier) throws Exception {
        /*
         * This is the simplest way to check a build
         * succeeded. It is also the simplest way to create
         * an IT test: make the build pass when the test
         * should pass, and make the build fail when the
         * test should fail. There are other methods
         * supported by the verifier. They can be seen here:
         * http://maven.apache.org/shared/maven-verifier/apidocs/index.html
         */
        verifier.verifyErrorFreeLog();
        /*
         * Reset the streams before executing the verifier
         * again.
         */
        verifier.resetStreams();
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true); //TODO: add tests for the top level project artifacts, if needed.
    }

    /**
     * main test that first creates the fuji app from archetype and verfies it.
     * @throws java.lang.Exception
     */
    public void testFujiApp() throws Exception {
        // Check in your dummy Maven project in /src/test/resources/...
        // The testdir is computed from the location of this
        // file.
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/app");
        // System.out.println("### TestDir " + testDir.getAbsolutePath());
        Verifier verifier;
        /*
         * We must first make sure that any artifact created
         * by this test has been removed from the local
         * repository. Failing to do this could cause
         * unstable test results. Fortunately, the verifier
         * makes it easy to do this.
         */
        verifier = new Verifier(testDir.getAbsolutePath(), true);
        verifier.deleteArtifact(testGID, "app", APP_VERSION, "pom");
        verifier.deleteArtifact(testGID, testAID, fujiVersion, "jar");

        executeGenerateFromArchetype(verifier);

        verifier.assertFilePresent(testAID + "/pom.xml");
        verifier.assertFilePresent(testAID + "/src/main/ifl/app.ifl");

        resetVerifier(verifier);

        executeTestsOnGeneratedApp(); // override this in extended tests to add additional test code
    //TODO: when we have a test sequence, we ca really separate the tests. For time being
    // add other tests under this hood and not as a junit tests (i.e. don't start with testXXY)
    }

    protected void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception ex) {
            // ignore
        }
    }

    /**
     * called from main test testFujiApp. implement this to add more testing of the fuji app
     * or just return noop.
     * @throws java.lang.Exception
     */
    protected abstract void executeTestsOnGeneratedApp() throws Exception;
}
