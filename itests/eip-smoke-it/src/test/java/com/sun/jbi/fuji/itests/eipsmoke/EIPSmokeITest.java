/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.eipsmoke;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Integration test for all eip's using file and jruby services
 * @author chikkala
 */
public class EIPSmokeITest
        extends BaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EIPSmokeITest(String testName) {
        super(testName, "eipSmokeTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "source_file");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "archive_file");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "bond_file");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "cash_file");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "other_file");
        this.assertServiceConfigFile(verifier, JRUBY_SERVICE_TYPE, "enrich_service");

        // check if the flow configurations are generated in src
        // assertFlowConfigFileInSourceDir(verifier, SPLIT_FLOW, "myxpath");

        // update configuration.
        copyTestDataToProjectDir("jruby/service.rb", "src/main/jruby/enrich_service/service.rb");
        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/hol/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();
        String fileArchiveTestDir = (new File(fileTestDir, "archive")).getAbsolutePath();

        updateFileServiceConfigProperties("source_file", null, "batchtrades-in.xml", null, fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("archive_file", null, "archive-%d.xml", "true", fileArchiveTestDir, null, null, null);
        updateFileServiceConfigProperties("bond_file", null, "bond-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("cash_file", null, "cash-%d.xml", "true", fileOutTestDir, null, null, null);
        updateFileServiceConfigProperties("other_file", null, "other-%d.xml", "true", fileOutTestDir, null, null, null);

        this.resetVerifier(verifier);
        this.executeBuildGoal(verifier);

        // check if the split configuration is generated at packaging
        assertFlowConfigFileInBuildDir(verifier, AGGREGATE_FLOW, AGGREGATE_FLOW + "$1"); //inline flow
        assertFlowConfigFileInBuildDir(verifier, AGGREGATE_FLOW, AGGREGATE_FLOW + "$2");  //inline flow
        assertFlowConfigFileInBuildDir(verifier, AGGREGATE_FLOW, AGGREGATE_FLOW + "$3");  //inline flow

        assertFlowConfigFileInBuildDir(verifier, FILTER_FLOW, FILTER_FLOW + "$1");  //inline flow

        assertFlowConfigFileInBuildDir(verifier, SPLIT_FLOW, SPLIT_FLOW + "$1");

        assertFlowConfigFileInBuildDir(verifier, SELECT_FLOW, SELECT_FLOW + "$1");        

        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        this.resetVerifier(verifier);
        this.executeDeployGoal(verifier);

        sleep(5000);

        // perfrom runtime test
        copyTestDataToProjectDir("file-testdata/batchtrades-in.xml", new File(fileInTestDir, "batchtrades-in.xml").getAbsolutePath());

        sleep(10000);
        // validate test results
        verifier.assertFilePresent(new File(fileArchiveTestDir, "archive-0.xml").getAbsolutePath());

        verifier.assertFilePresent(new File(fileOutTestDir, "bond-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "cash-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "other-0.xml").getAbsolutePath());
        // undeploy the app
        this.resetVerifier(verifier);
        this.executeUnDeployGoal(verifier);

        this.resetVerifier(verifier);
    }
}
