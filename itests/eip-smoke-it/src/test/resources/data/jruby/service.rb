require 'java'
require 'XPath'
require 'ESB'
include_class "javax.xml.namespace.QName"

@@service = QName.new "http://fuji.dev.java.net/application/[app]", "[service]"

def process(inMsg)
  # payload is a java org.w3c.dom.Node object
  payload = inMsg.getPayload

  puts "Got message payload"
  # get the trade
  trade = XPath.findNode(payload, "//Trade")
  if trade
    local_ccy = trade.getOwnerDocument.createElement("LocalCurrency")
    local_ccy.setTextContent("USD")
    trade.appendChild(local_ccy)
  else
    puts "Could not find Trade node in message"
  end

  inMsg
end