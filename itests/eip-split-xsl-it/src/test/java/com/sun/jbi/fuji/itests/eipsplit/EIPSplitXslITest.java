/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EIPSplitITest.java 
 *
 * Copyright 2009 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.eiptee;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.Properties;
import org.apache.maven.it.Verifier;

/**
 * Integration test for Split EIP
 * @author chikkala
 */
public class EIPSplitXslITest
        extends BaseITest {
    Verifier    verifier;
    String      savedMEgroupid;
    String      savedNMgroupid;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EIPSplitXslITest(String testName) {
        super(testName, "eipSplitXslTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "in");
        this.assertServiceConfigFile(verifier, FILE_SERVICE_TYPE, "out");

        File prjDir = this.getFujiAppProjectDir();

        String rtTestFolder = (new File(prjDir.getParentFile(), "target/hol/files")).getAbsolutePath();
        System.out.println(rtTestFolder);

        File fileTestDir = new File(rtTestFolder);
        String fileInTestDir = (new File(fileTestDir, "in")).getAbsolutePath();
        String fileOutTestDir = (new File(fileTestDir, "out")).getAbsolutePath();
        //String fileArchiveTestDir = (new File(fileTestDir, "archive")).getAbsolutePath();

        updateFileServiceConfigProperties("in", null, "in.xml", "false", fileInTestDir, null, null, null);
        updateFileServiceConfigProperties("out", null, "out-%d.xml", "true", fileOutTestDir, null, null, null);

        // Copy the xsl file to the right location
        File splitConfigFolder = new File(prjDir.getParentFile(), "eipSplitXslTest/src/main/resources/META-INF/flow/split/split$1/");
        splitConfigFolder.mkdirs();
        System.out.println("The split config folder is === "+splitConfigFolder.getAbsolutePath());
        
        this.resetVerifier(verifier);
        copyTestDataToProjectDir("order-split.xsl", new File(splitConfigFolder, "order-split.xsl").getAbsolutePath());
        this.executeBuildGoal(verifier);
        
        
        this.assertServiceAsseblyFile(verifier);
        
        // deploy
        this.resetVerifier(verifier);
        this.executeDeployGoal(verifier);

        sleep(5000);

        
        // perfrom runtime test
        copyTestDataToProjectDir("file-testdata/order.xml", new File(fileInTestDir, "in.xml").getAbsolutePath());
        sleep(10000);

        
        // verify files were split correctly
        verifier.assertFilePresent(new File(fileOutTestDir, "out-0.xml").getAbsolutePath());
        verifier.assertFilePresent(new File(fileOutTestDir, "out-1.xml").getAbsolutePath());
         
        
        // undeploy the app
        this.resetVerifier(verifier);
        this.executeUnDeployGoal(verifier);

        this.resetVerifier(verifier);
    }
}
