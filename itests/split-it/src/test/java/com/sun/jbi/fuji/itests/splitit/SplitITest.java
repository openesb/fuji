/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SplitITest.java 
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.fuji.itests.splitit;

import com.sun.jbi.fuji.itests.base.BaseITest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;

/**
 * Integration test for split 
 * @author chikkala
 */
public class SplitITest
    extends BaseITest {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SplitITest(String testName) {
        super(testName, "splitTest");
    }

    protected void executeTestsOnGeneratedApp() throws Exception {
        Verifier verifier = this.createFujiAppVerifier();

        this.executeGenerateArtifactsGoal(verifier);
        // check if the service artifacts are generated
        verifier.assertFilePresent( "src/main/config/file/f1/service.properties" );
        verifier.assertFilePresent( "src/main/config/file/f2/service.properties" );
        verifier.assertFilePresent( "src/main/config/file/f3/service.properties" );
        verifier.assertFilePresent( "src/main/config/file/f4/service.properties" );
        verifier.assertFilePresent( "src/main/config/file/f5/service.properties" );
        verifier.assertFilePresent( "src/main/config/file/f6/service.properties" );
        // check if the split configuration is generated in src
        verifier.assertFilePresent( "src/main/split/my/flow.properties" );

        this.resetVerifier(verifier);

        this.executeDistGoal(verifier);

        // check if the split configuration is generated at packaging
        verifier.assertFilePresent( "target/classes/META-INF/flow/split/my/flow.properties" );
        verifier.assertFilePresent( "target/classes/META-INF/flow/split/split$2/flow.properties" );
        verifier.assertFilePresent( "target/classes/META-INF/flow/split/split$3/flow.properties" );

        verifier.assertFilePresent( "target/felix/bundle/splitTest-" + fujiVersion + ".jar" );
        verifier.assertFilePresent( "target/felix/bundle/sun-encoder-library-" + fujiVersion +".jar" );
        verifier.assertFilePresent( "target/felix/bundle/sun-file-binding-" + fujiVersion + ".jar" );

        this.resetVerifier(verifier);
    }

}
