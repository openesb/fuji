/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NetworkServerActivator.java
 *
 * Copyright 2008 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.fuji.derby;

import java.io.PrintWriter;
import java.net.InetAddress;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.apache.derby.drda.NetworkServerControl;

/**
 * This bundle activator is used to control the lifecycle of 
 * derby network server. 
 * 
 */
public class NetworkServerActivator implements BundleActivator {
    
   
    /**
     * constant for localhost
     */
    private static final String LOCALHOST = "localhost";
    
    /**
     * constant for default port
     */
    private static final int DEFAULT_PORT = 1600;

    /**
     * property to configure network server port
     */
    private static final String PORT_PROPERTY="com.sun.jbi.fuji.derby.port";
    
    /**
     * NetworkServerControl
     */
    private static NetworkServerControl mServerControl;

    /**
     * This method is used to start the network server.
     * The network server port is could be configured using the system
     * property com.sun.jbi.fuji.derby.port.
     * @param context
     * @throws java.lang.Exception
     */
    public void start(BundleContext context) throws Exception {
        

        int port = DEFAULT_PORT;
        String portString = context.getProperty(PORT_PROPERTY);
        if (portString != null) {
            port = Integer.parseInt(portString);
        }
        
        mServerControl = 
                new NetworkServerControl(
                    InetAddress.getByName(LOCALHOST), 
                    port);

        mServerControl.start(new PrintWriter(System.out));


    }

    public void stop(BundleContext context) throws Exception {
        mServerControl.shutdown();

    }
}
